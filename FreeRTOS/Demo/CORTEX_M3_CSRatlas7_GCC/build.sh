#! /bin/sh
#
# Copyright (c) [2015-2016], The Linux Foundation
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


# build entry for M3 related firmware:
#  1. Tinyloader
#  2. Recover
#  3. Wakeup
#  4. Application(FreeRTOS)

# set build compiler environment
export CROSS_COMPILE=arm-linux-gnueabihf-
# tool configuration
poky=~/workspace/stepb
SIGN=$poky/poky/sources/linux-x86/devtools/sign/sign
M3KEY=$poky/poky/sources/linux-x86/devtools/sign/M3_key.bin
SOCTEST=~/workspace/soctest

UBOOT=$SOCTEST/out/atlas7cb/u-boot.csr

CC=${CROSS_COMPILE}gcc
BIN=${CROSS_COMPILE}objcopy
FREERTOS_RECOVER_CANTP_ARCCORE_TRAIL_VERSION="no"

# by default build for dinependent mode
l_isIndependentMode=1
l_isM3BackupImage=0
l_isTestMode=0

check_build_result()
{
	if [ $? -ne 0 ]; then
		exit;
	fi
}



if [ $# -eq 2 ] ; then
	if [ $1 = 'i' ]; then
		l_isIndependentMode=1
	elif [ $1 = 'd' ]; then	# switch to dependent mode
		l_isIndependentMode=0
	elif [ $1 = 'b' ]; then	# switch to independent mode backup image
		l_isIndependentMode=1
		l_isM3BackupImage=1
	elif [ $1 = 't' ]; then	# build boot time test image at once
		l_isIndependentMode=1
		l_isTestMode=1
	fi

	readonly removable=$(cat /sys/block/$2/removable)
	if [ ! "$removable" = "1" ]; then
		echo -e "'$2' is not your SDCARD or NAND disk! Please be careful!\a"
		exit 1
	fi
else
	echo "need 2 params, example ./build.sh mode sd_name"
	exit 1
fi

echo
echo "  >> DIR: `pwd`"
if [ $l_isIndependentMode -eq 1 ] ; then
	echo "  >> BUILD FOR INDENPENDENT MODE: COMPILER=$CC"
	export M3_MODE=independent
	if [ $l_isM3BackupImage -eq 1 ] ; then
		M3_BASE=0x18100240
		export M3_BACKUP=yes
		echo "  >> M3 BACKUP IMAGE @$M3_BASE"
	else
		M3_BASE=0x18000240
		export M3_BACKUP=no
	fi

else
	echo "  >> BUILD FOR DENPENDENT MODE: COMPILER=$CC"
	export M3_MODE=dependent
	M3_BASE=0x05400000
fi
echo
if [ ${FREERTOS_RECOVER_CANTP_ARCCORE_TRAIL_VERSION} == "yes" ]
then
	if ! [ -e 9c57d86f66be.tar.bz2 ] ; then
		wget http://my.arccore.com/hg/arc-stable/archive/9c57d86f66be.tar.bz2
		bzip2 -dvk 9c57d86f66be.tar.bz2
		tar -xvf 9c57d86f66be.tar
		rm 9c57d86f66be.tar
	fi

	if ! [ -d arc-stable-9c57d86f66be ] ; then
		bzip2 -dvk 9c57d86f66be.tar.bz2
		tar -xvf 9c57d86f66be.tar
		rm 9c57d86f66be.tar
	fi

	echo "  >> Prepare ARCCORE CANTP stack files"
	cp -v arc-stable-9c57d86f66be/include/CanTp.h recover/include/
	cp -v arc-stable-9c57d86f66be/include/CanTp_Cbk.h recover/include/
	cp -v arc-stable-9c57d86f66be/include/CanTp_Types.h recover/include/
	cp -v arc-stable-9c57d86f66be/include/ComStack_Types.h recover/include/
	cp -v arc-stable-9c57d86f66be/include/Modules.h recover/include/
	cp -v arc-stable-9c57d86f66be/communication/CanTp/CanTp.c recover/source/
	echo "/* empty stub */" > recover/include/Platform_Types.h
	echo "/* empty stub */" > recover/include/Det.h
	echo "/* empty stub */" > recover/include/SchM_CanTp.h
	echo "/* empty stub */" > recover/include/PduR_CanTp.h
	sed -i "462 i\		rxRuntime->iso15765.BS = computedBs;	/* add by parai as bug */" recover/source/CanTp.c
else
  if ! [ -e recover/include/CanTp.h ] ; then
  	echo "  >> generate AUTOSAR V3.1 CANTP stub files"
	echo "#ifndef CAN_TP_H" > recover/include/CanTp.h
	echo "#define CAN_TP_H" >> recover/include/CanTp.h
	echo "#include \"ComStack_Types.h\"" >> recover/include/CanTp.h
	echo "#include \"CanTp_Cfg.h\"" >> recover/include/CanTp.h
	echo "void CanTp_TxConfirmation(PduIdType CanTpTxPduId);" >> recover/include/CanTp.h
	echo "void CanTp_RxIndication(PduIdType CanTpRxPduId,const PduInfoType *CanTpRxPduPtr);" >> recover/include/CanTp.h
	echo "Std_ReturnType CanTp_Transmit(PduIdType CanTpTxSduId,const PduInfoType *CanTpTxInfoPtr);" >> recover/include/CanTp.h
	echo "void CanTp_MainFunction(void);" >> recover/include/CanTp.h
	echo "void CanTp_Init(void);" >> recover/include/CanTp.h
	echo "#endif"  >> recover/include/CanTp.h
	echo "" > recover/include/CanTp_Cbk.h
	echo "" > recover/include/CanTp_Types.h
	echo "#ifndef COMSTACK_TYPES_H" > recover/include/ComStack_Types.h
	echo "#define COMSTACK_TYPES_H" >> recover/include/ComStack_Types.h
	echo "#define RECOVER_STUB_AUTOSAR_CANTP" >> recover/include/ComStack_Types.h
	echo "typedef unsigned int PduIdType;" >> recover/include/ComStack_Types.h
	echo "typedef unsigned int PduLengthType;" >> recover/include/ComStack_Types.h
	echo "typedef struct {" >> recover/include/ComStack_Types.h
	echo "    unsigned char *SduDataPtr;" >> recover/include/ComStack_Types.h
	echo "    PduLengthType SduLength;" >> recover/include/ComStack_Types.h
	echo "} PduInfoType;" >> recover/include/ComStack_Types.h
	echo "typedef enum {" >> recover/include/ComStack_Types.h
	echo "    BUFREQ_OK=0," >> recover/include/ComStack_Types.h
	echo "    BUFREQ_NOT_OK," >> recover/include/ComStack_Types.h
	echo "    BUFREQ_BUSY," >> recover/include/ComStack_Types.h
	echo "    BUFREQ_OVFL" >> recover/include/ComStack_Types.h
	echo "} BufReq_ReturnType;" >> recover/include/ComStack_Types.h
	echo "typedef unsigned char NotifResultType;" >> recover/include/ComStack_Types.h
	echo "#define NTFRSLT_OK						0x00" >> recover/include/ComStack_Types.h
	echo "typedef int CanTp_ConfigType;" >> recover/include/ComStack_Types.h
	echo "typedef int CanTp_NSduType;" >> recover/include/ComStack_Types.h
	echo "typedef int CanTp_RxIdType;" >> recover/include/ComStack_Types.h
	echo "#endif" >> recover/include/ComStack_Types.h
	echo "" > recover/include/Modules.h
	echo "#include \"CanTp.h\"" > recover/source/CanTp.c
	echo "void CanTp_TxConfirmation(PduIdType CanTpTxPduId) {}" >> recover/source/CanTp.c
	echo "void CanTp_RxIndication(PduIdType CanTpRxPduId,const PduInfoType *CanTpRxPduPtr){}" >> recover/source/CanTp.c
	echo "Std_ReturnType CanTp_Transmit(PduIdType CanTpTxSduId,const PduInfoType *CanTpTxInfoPtr){return 0;}" >> recover/source/CanTp.c
	echo "void CanTp_MainFunction(void){}" >> recover/source/CanTp.c
	echo "void CanTp_Init(void){}" >> recover/source/CanTp.c
	echo "/* empty stub */" > recover/include/Platform_Types.h
	echo "/* empty stub */" > recover/include/Det.h
	echo "/* empty stub */" > recover/include/SchM_CanTp.h
	echo "/* empty stub */" > recover/include/PduR_CanTp.h
  fi
fi

cd bootloader
make all BOARD_VER=atlas7b ROMAPI_VER=atlas7b2 DEBUG=no
check_build_result

cd ../recover
make all BOARD_VER=atlas7b DEBUG=yes
check_build_result

cd tool-util
make all
check_build_result

cd ../..
ln -fs recover/tool-util/loaderserver.exe loaderserver.exe

cd wakeup
make all BOARD_VER=atlas7b DEBUG=no
check_build_result

cd ..
make all M3_TEST_ENABLE=yes BOARD_VER=atlas7b TINY_LOADER_USED=yes M3_LOADER_ENABLE=no STRIP_UNUSED=no FREERTOS_DEBUG=yes
check_build_result

make all TARGET=freertos-dep M3_MODE=dependent M3_BASE=0x05400000 M3_TEST_ENABLE=yes BOARD_VER=atlas7b TINY_LOADER_USED=no M3_LOADER_ENABLE=no STRIP_UNUSED=no FREERTOS_DEBUG=yes
check_build_result

if [ $l_isIndependentMode -eq 1 ] ; then
if [ $l_isTestMode -eq 1 ] ; then
	make all TARGET=freertos-nt M3_TEST_ENABLE=yes BOARD_VER=atlas7b TINY_LOADER_USED=no M3_LOADER_ENABLE=no STRIP_UNUSED=no FREERTOS_DEBUG=yes
fi
fi

mkdir -p release

if [ $l_isIndependentMode -eq 1 ] ; then
	$SIGN -r bootloader/out/bootloader.bin $M3KEY release/bootloader.bin 0 0
	$SIGN -r recover/out/recover.bin $M3KEY release/recover.bin 0 0
	$SIGN -r wakeup/out/wakeup.bin $M3KEY release/wakeup.bin 0 0
	$SIGN -r gcc/RTOSDemo.bin $M3KEY release/freertos.bin 0 0

	$SIGN -r bootloader/out/bootloader.bin $M3KEY release/bootloader0.bin 0 0
	$SIGN -r recover/out/recover.bin $M3KEY release/recover0.bin 0 0
	$SIGN -r wakeup/out/wakeup.bin $M3KEY release/wakeup0.bin 0 0
	$SIGN -r gcc/RTOSDemo.bin $M3KEY release/freertos0.bin 0 0
	cp gcc/freertos-dep.bin release/freertos-dep.bin

	cd release

	cp -v $poky/poky/build/tmp/work/atlas7_arm-poky-linux-gnueabi/kalimba/1.0-r0/prebuilts/atlas7/kalimba/m3/kalimba.fw ./kalimba.fw
	cp -v $poky/poky/build/tmp/work/atlas7_arm-poky-linux-gnueabi/kalimba/1.0-r0/prebuilts/atlas7/kalimba/m3/audio.raw ./audio.raw

	echo "unsigned long image_header[][3] = {" > image_header.c
	echo "    { 0 /* bootlader.bin */, $M3_BASE+0 , `wc -c ../bootloader/out/bootloader.bin | cut -d \" \" -f 1` }," >> image_header.c
	echo "    { 1 /* recover.bin   */, $M3_BASE+32*1024 , `wc -c ../recover/out/recover.bin    | cut -d \" \" -f 1` }," >> image_header.c
	echo "    { 2 /* freertos.bin  */, $M3_BASE+64*1024 , `wc -c ../gcc/RTOSDemo.bin   | cut -d \" \" -f 1` }," >> image_header.c
	if [ $l_isTestMode -eq 1 ] ; then
		echo "    { 3 /* wakeup.bin    */, $M3_BASE+(2048-32)*1024, `wc -c ../wakeup/out/wakeup.bin     | cut -d \" \" -f 1` }," >> image_header.c
		echo "    { 4 /* uboot.csr     */, $M3_BASE+2048*1024-0x240, `wc -c $UBOOT     | cut -d \" \" -f 1` }," >> image_header.c
	else
		echo "    { 3 /* wakeup.bin    */, $M3_BASE+(1024-32)*1024, `wc -c ../wakeup/out/wakeup.bin     | cut -d \" \" -f 1` }," >> image_header.c
		# echo "    { 4 /* uboot.csr     */, $M3_BASE+1024*1024-0x240, `wc -c $UBOOT     | cut -d \" \" -f 1` }," >> image_header.c
        # no uboot for early-audio version
		echo "    { 0xFFFFFFFF, 0xFFFFFFFF , 0xFFFFFFFF }," >> image_header.c
		echo "    { 5 /* kalimba.fw    */, $M3_BASE+1024*1024-0x240, `wc -c kalimba.fw	   | cut -d \" \" -f 1` }," >> image_header.c
		echo "    { 6 /* audio.raw     */, $M3_BASE+(1024+512)*1024-0x240, `wc -c audio.raw	| cut -d \" \" -f 1` }," >> image_header.c
		echo "    { 7 /* freertos-dep     */, $M3_BASE+(64+672)*1024-0x240, `wc -c freertos-dep.bin    | cut -d \" \" -f 1` }," >> image_header.c
	fi


	echo "};" >> image_header.c
	$CC -mthumb -mcpu=cortex-m3 -O3 -c image_header.c -o image_header.o
	$BIN -O binary image_header.o image_header.bin
	truncate -s 28k bootloader.bin
	truncate -s  4k image_header.bin
	truncate -s 32k recover.bin
	if [ $l_isTestMode -eq 1 ] ; then
		truncate -s 1952k freertos.bin
	else
		truncate -s 672k freertos.bin
	fi
	truncate -s 256k freertos-dep.bin
	truncate -s 32k wakeup.bin
	cat bootloader.bin image_header.bin recover.bin freertos.bin freertos-dep.bin wakeup.bin kalimba.fw audio.raw > freertos-ind.bin
	#cat bootloader.bin image_header.bin recover.bin freertos.bin freertos-dep.bin wakeup.bin $UBOOT > freertos-ind-with-boot.bin
  	if [ $l_isTestMode -eq 1 ] ; then
		# for test of the boot time saving
		truncate -s 250k ../gcc/RTOSDemo.bin
		$SIGN -r ../gcc/RTOSDemo.bin $M3KEY freertos.bin 0 0
		truncate -s 1952k freertos.bin
		cat bootloader.bin image_header.bin recover.bin freertos.bin wakeup.bin > freertos-ind-250k.bin

		truncate -s 500k ../gcc/RTOSDemo.bin
		$SIGN -r ../gcc/RTOSDemo.bin $M3KEY freertos.bin 0 0
		truncate -s 1952k freertos.bin
		cat bootloader.bin image_header.bin recover.bin freertos.bin wakeup.bin > freertos-ind-500k.bin

		truncate -s 1000k ../gcc/RTOSDemo.bin
		$SIGN -r ../gcc/RTOSDemo.bin $M3KEY freertos.bin 0 0
		truncate -s 1952k freertos.bin
		cat bootloader.bin image_header.bin recover.bin freertos.bin wakeup.bin > freertos-ind-1000k.bin

		truncate -s 1500k ../gcc/RTOSDemo.bin
		$SIGN -r ../gcc/RTOSDemo.bin $M3KEY freertos.bin 0 0
		truncate -s 1952k freertos.bin
		cat bootloader.bin image_header.bin recover.bin freertos.bin wakeup.bin > freertos-ind-1500k.bin

		truncate -s 250k ../gcc/freertos-nt.bin
		$SIGN -r ../gcc/freertos-nt.bin $M3KEY freertos-ind-nt-250k.bin 0 0

		truncate -s 500k ../gcc/freertos-nt.bin
		$SIGN -r ../gcc/freertos-nt.bin $M3KEY freertos-ind-nt-500k.bin 0 0

		truncate -s 1000k ../gcc/freertos-nt.bin
		$SIGN -r ../gcc/freertos-nt.bin $M3KEY freertos-ind-nt-1000k.bin 0 0

		truncate -s 1500k ../gcc/freertos-nt.bin
		$SIGN -r ../gcc/freertos-nt.bin $M3KEY freertos-ind-nt-1500k.bin 0 0
  	fi
	cat image_header.c

	if [ $l_isM3BackupImage -eq 1 ] ; then
		cp freertos-ind.bin freertos-ind-bak.bin
		cp freertos-ind-with-boot.bin freertos-ind-bak-with-boot.bin
		cp bootloader0.bin bootloader0-bak.bin
		cp freertos0.bin freertos0-bak.bin
	fi
else
	cp bootloader/out/bootloader.bin release
	cp recover/out/recover.bin release
	cp wakeup/out/wakeup.bin release
	cp gcc/RTOSDemo.bin release/freertos.bin
	
	cd release
	echo "unsigned long image_header[][3] = {" > image_header.c
	echo "    { 0 /* bootlader.bin */, $M3_BASE+0 , `wc -c bootloader.bin | cut -d \" \" -f 1` }," >> image_header.c
	echo "    { 0xFFFFFFFF /* recover.bin   */, $M3_BASE+32*1024 , `wc -c recover.bin    | cut -d \" \" -f 1` }," >> image_header.c
	echo "    { 2 /* freertos.bin  */, $M3_BASE+64*1024 , `wc -c freertos.bin   | cut -d \" \" -f 1` }," >> image_header.c
	echo "    { 3 /* wakeup.bin    */, $M3_BASE+32*1024, `wc -c wakeup.bin     | cut -d \" \" -f 1` }," >> image_header.c
	echo "};" >> image_header.c
	$CC -mthumb -mcpu=cortex-m3 -O3 -c image_header.c -o image_header.o
	$BIN -O binary image_header.o image_header.bin
	truncate -s 28k bootloader.bin
	truncate -s  4k image_header.bin
	truncate -s 32k recover.bin
	truncate -s 32k wakeup.bin
	cat bootloader.bin image_header.bin wakeup.bin freertos.bin > freertos-dep.bin

	cat image_header.c
	
fi

chmod +x *.bin

ls -l
if [ $l_isIndependentMode -eq 1 ] ; then

	sudo cp -v freertos-ind*.bin /media/${USER}/root/home/root/

	echo "the last step when start the EVB with the SD card:"
	echo "    switch BOOT_SEL 4th pin to pin4, change to dependent mode;"
	echo "    qspi_loop.sh -f freertos.bin freertos.txt;"
	echo "    switch BOOT_SEL 4th pin to pin5, change to independent mode and then reboot."

	sudo umount -fl /media/${USER}/boot
	sudo umount -fl /media/${USER}/data
	sudo umount -fl /media/${USER}/user
	sudo umount -fl /media/${USER}/root
else
	cp -v freertos-dep.bin $SOCTEST/out/atlas7cb/RTOSDemo.bin
	cd $SOCTEST
	make uboot
	check_build_result
	cd $SOCTEST/out/atlas7cb
	sudo cp -v ../../scripts/flash.sh .
	sudo ./flash.sh $2 uboot
fi

