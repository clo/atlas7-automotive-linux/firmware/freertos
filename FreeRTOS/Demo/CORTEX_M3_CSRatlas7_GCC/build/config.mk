#
# Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

export SHELL = /bin/bash

#
# project workspace directory
#
export DIR_PROJECT = $(shell pwd -P)

#
# default board name, can be:
#   atlas7ca: atlas7 step-a core board
#   atlas7b: atlas7 step-b core board
#
# to make images for atlas7b:
#   $ make (or make BOARD_VER=atlas7b)
#
export BOARD_VER ?= atlas7b

# is backup image of M3 applicaiton
# offset is 0x10000(1Mb)
# only for step b
export M3_BACKUP ?= no

#
# Should we link to GCC libraries?
#
export LIBC_USED ?= NO

#
# only valid in independent mode
# if m3 mode is independent and loader enabled, M3 will load the
# u-boot from QSPI to DDRAM and then on A7 start from DDRAM
#
export M3_LOADER_ENABLE ?= no

# remove unused data and code
export STRIP_UNUSED ?= no

# only valid in independent mode
export TINY_LOADER_USED ?= no

# CAN ERROR interrupt tx abort test
export CAN_ERROR_TX_ABORT_TEST ?= no

#
# initialize variables for compiler
#
INC_DIR=
VPATH=
OBJS=

export SCRIPTS_DIR=${DIR_PROJECT}/scripts
export PXP_ENABLE ?= no
export PIC_ENABLE ?= no
export BENCHMARK_ENABLE ?= no
export FREERTOS_KAS_FW_ENABLE ?= no
export STACK_SZ ?= 0x800
export VECTOR_TBL_SZ ?= 0x300
export GOT_TBL_SZ ?= 0x1000
export DATA_BSS_SZ ?= 0x7000

#
# makedefs - Definitions common to all makefiles.
#
ifeq (${PIC_ENABLE}, yes)
	export PIC_FLAGS=-fpie -pie \
		-mno-pic-data-is-text-relative \
		-mpic-register=r9 -msingle-pic-base -mlong-calls \
		-D __PIC_ENABLE__
endif

ifeq (${BOARD_VER}, atlas7b)
	export BOARD_FLAGS=-D __ATLAS7_STEP_B__
endif

ifeq (${PXP_ENABLE}, yes)
	export PXP_FLAGS=-D __PXP_ENABLE__
endif

include build/makedefs

ifeq (${M3_MODE}, independent)
	CFLAGS+=-D __M3_INDEPENDENT__
endif

ifeq ($(M3_BACKUP),yes)
CFLAGS += -D__M3_BACKUP__
endif

#
# strip the un-used function and data
#
ifeq ($(STRIP_UNUSED),yes)
ifeq (${PIC_ENABLE}, yes)
# in this PIC mode, strip of data is not allowed.
else
AFLAGS += -fdata-sections
CFLAGS += -fdata-sections
endif
AFLAGS += -ffunction-sections
CFLAGS += -ffunction-sections
LDFLAGS+= --gc-sections
endif
