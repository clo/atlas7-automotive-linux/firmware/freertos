#
# Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

DIR_RTOS = $(shell readlink -f ${DIR_PROJECT}/../../Source)
DIR_RTOS_COMMON = $(shell readlink -f ${DIR_PROJECT}/../Common)
DIR_RTOS_PORTABLE = ${DIR_RTOS}/portable

# Include Directories
INC_DIR += -I ${DIR_RTOS}/include
INC_DIR += -I ${DIR_RTOS_PORTABLE}/GCC/ARM_CM3

# Virtual Path for Compiler
VPATH += ${DIR_RTOS}
VPATH += ${DIR_RTOS_PORTABLE}/GCC/ARM_CM3
VPATH += ${DIR_RTOS_PORTABLE}/MemMang

# Objects for FreeRTOS
OBJS += ${OBJDIR}/list.o
OBJS += ${OBJDIR}/queue.o
OBJS += ${OBJDIR}/tasks.o
OBJS += ${OBJDIR}/croutine.o
OBJS += ${OBJDIR}/heap_4.o
OBJS += ${OBJDIR}/event_groups.o
OBJS += ${OBJDIR}/timers.o
OBJS += ${OBJDIR}/i2c.o
