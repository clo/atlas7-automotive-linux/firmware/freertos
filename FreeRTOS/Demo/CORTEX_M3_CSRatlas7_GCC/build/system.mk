#
# Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

CFLAGS+=-D CONFIG_SYSRTC_IO_BRIDGE -D CONFIG_PWRC_IO_BRIDGE
CFLAGS+=-DCONFIG_ARCH_ATLAS7

# Virtual Path for Compiler
VPATH += ${DIR_PROJECT}/system

# Objects for device drivers
OBJS += ${OBJDIR}/atlas7_port.o
OBJS += ${OBJDIR}/handler.o
OBJS += ${OBJDIR}/sysrtc.o
OBJS += ${OBJDIR}/pm.o
OBJS += ${OBJDIR}/timer.o
OBJS += ${OBJDIR}/uart.o
OBJS += ${OBJDIR}/gpio.o
OBJS += ${OBJDIR}/iobridge.o
OBJS += ${OBJDIR}/qspi.o
OBJS += ${OBJDIR}/kas_keyhole.o
OBJS += ${OBJDIR}/tsadc.o
OBJS += ${OBJDIR}/cache.o
OBJS += ${OBJDIR}/gpt.o
OBJS += ${OBJDIR}/pinctrl.o
OBJS += ${OBJDIR}/pulsec.o
OBJS += ${OBJDIR}/ddr.o
OBJS += ${OBJDIR}/clock.o
OBJS += ${OBJDIR}/loader.o
OBJS += ${OBJDIR}/otp.o
OBJS += ${OBJDIR}/sirfdrive3.o

ifeq (${M3_MODE}, independent)
ifeq (${M3_LOADER_ENABLE}, yes)
	CFLAGS+=-D __M3_LOADER_ENABLE__ 
endif
endif

ifeq ($(TINY_LOADER_USED), yes)
	CFLAGS+=-D__M3_TINY_LOADER_USED__
	AFLAGS+=-D__M3_TINY_LOADER_USED__
endif
