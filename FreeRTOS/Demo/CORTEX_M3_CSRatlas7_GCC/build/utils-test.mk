#
# Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

CFLAGS+=-D CONFIG_ENABLE_TEST

# Virtual Path for Compiler
VPATH += ${DIR_PROJECT}/utils/test

# Objects for register access test
CFLAGS+=-D CONFIG_ENABLE_REGOPS_TEST
OBJS += ${OBJDIR}/regops_test.o

# Objects for sysrtc test
CFLAGS+=-D CONFIG_ENABLE_SYSRTC_TEST
OBJS += ${OBJDIR}/rtc_test.o

# Objects for ipc test
# CFLAGS+=-D CONFIG_ENABLE_IPC_TEST
# OBJS += ${OBJDIR}/ipc_test.o

# Objects for virtio CAN test
CFLAGS+=-D CONFIG_ENABLE_VCAN_TEST
OBJS += ${OBJDIR}/virtio_can_test.o

# Objects for pwrc test
CFLAGS+=-D CONFIG_ENABLE_PWRC_TEST
OBJS += ${OBJDIR}/pwrc_test.o

# Objects for watchdog test
CFLAGS+=-D CONFIG_ENABLE_WATCHDOG_TEST
OBJS += ${OBJDIR}/watchdog_test.o

# Objects for canbus test
CFLAGS+=-D CONFIG_ENABLE_CANBUS_TEST
#CFLAGS+=-D CONFIG_ENABLE_VIRTIO_CAN_TIME_TEST
#CFLAGS+=-D CONFIG_ENABLE_VIRTIO_CAN_TASK
#CFLAGS+=-D CONFIG_ENABLE_CAN_LOG
CFLAGS+=-D CONFIG_VIRTIO_CAN_TIME_TEST_ID=0x100
OBJS += ${OBJDIR}/canbus_test_lib.o

# Objects for canbus basic cli interface
CFLAGS+=-D CONFIG_ENABLE_CANBUS_BASIC_TEST
OBJS += ${OBJDIR}/canbus_basic_cli.o

# Objects for canbus demo
CFLAGS+=-D CONFIG_ENABLE_CANBUS_DEMO
OBJS += ${OBJDIR}/canbus_demo_lib.o
OBJS += ${OBJDIR}/canbus_demo.o

# Objects for canbus interface test
CFLAGS+=-D CONFIG_ENABLE_CANBUS_INTERFACE_TEST
OBJS += ${OBJDIR}/canbus_interface_test_lib.o
OBJS += ${OBJDIR}/canbus_interface_test.o

# Objects for canbus SW test
CFLAGS+=-D CONFIG_ENABLE_CANBUS_CORE_TEST
OBJS += ${OBJDIR}/canbus_core_test_lib.o
OBJS += ${OBJDIR}/canbus_core_test_basic.o
OBJS += ${OBJDIR}/canbus_core_test_link.o
OBJS += ${OBJDIR}/canbus_core_test_sst.o
OBJS += ${OBJDIR}/canbus_core_test_error_detect.o
OBJS += ${OBJDIR}/canbus_core_test_debug_mode.o
OBJS += ${OBJDIR}/canbus_core_test.o

# Objects for canbus KPI test
CFLAGS+=-D CONFIG_ENABLE_CANBUS_KPI_TEST
OBJS += ${OBJDIR}/canbus_load_test.o

# Objects for QSPI test
CFLAGS+=-D CONFIG_ENABLE_QSPI_TEST
OBJS += ${OBJDIR}/qspi_test_xip.o
OBJS += ${OBJDIR}/qspi_test.o

CFLAGS+=-D CONFIG_ENABLE_TSADC_TEST
OBJS += ${OBJDIR}/tsadc_test.o

# Objects for clock test
CFLAGS+=-D CONFIG_ENABLE_CLOCK_TEST
OBJS += ${OBJDIR}/m3_clock_test.o

# Objects for clock test
CFLAGS+=-D CONFIG_ENABLE_GPIO_TEST
OBJS += ${OBJDIR}/gpio_test.o

# Objects for GPT access test
CFLAGS+=-D CONFIG_ENABLE_GPT_TEST
OBJS += ${OBJDIR}/gpt_test.o

# Objects for CACHE test
CFLAGS+=-D CONFIG_ENABLE_CACHE_TEST
OBJS += ${OBJDIR}/cache_test.o

# Objects for AES test
CFLAGS+=-D CONFIG_ENABLE_AES_TEST
OBJS += ${OBJDIR}/aes_io.o

# Objects for NTFW test
CFLAGS+=-D CONFIG_ENABLE_NOC_NTFW_TEST
OBJS += ${OBJDIR}/noc_ntfw_test.o

# Objects for uboot loader test
CFLAGS+=-D CONFIG_ENABLE_LOADER_TEST
OBJS += ${OBJDIR}/loader_test.o

# Objects for pulse counter test
CFLAGS+=-D CONFIG_ENABLE_PULSEC_TEST
OBJS += ${OBJDIR}/pulsec_test.o

# CAN ERROR TX ABORT TEST ENABLE
ifeq (${CAN_ERROR_TX_ABORT_TEST},yes)
CFLAGS+=-D ENABLE_CAN_ERROR_TX_ABORT_TEST
endif

# Objects for recover firmware test
CFLAGS+=-D CONFIG_ENABLE_RECOVER_TEST
OBJS += ${OBJDIR}/recover_test.o
