/**************************************************************************/
/*   Title       : CANmod3 low-level driver, header file                  */
/*   Company     : INICORE INC., Newark, CA 94560                         */
/*   File        : CANmod3.h                                              */
/*   Author      : DL                                                     */
/*   Date        : April 5, 2010                                          */
/**************************************************************************/
/*                                                                        */
/*  Copyright (c) 2010-2015 INICORE INC., All Rights Reserved             */
/*                                                                        */
/**************************************************************************/

/* $InicoreDate: 2015/03/03 01:24:39 $ $InicoreAuthor: daniel $ $InicoreRevision: 1.8 $ $InicoreName: CANMOD3_DRIVER-REL-1_1_0 $*/

#ifndef CANmod3_H
#define CANmod3_H

#define CANmod3_RX_MAILBOX		32
#define CANmod3_TX_MAILBOX		32
/**
 * Manual setting with specified fields
 */
#define CANmod3_SPEED_MANUAL		0

/**
*      Configuration and Speed definitions
*/

#define CANmod3_PRESET                  (CAN_CONFIG_REG.l)0
#define CANmod3_SAMPLE_BOTH_EDGES       0x00000001
#define CANmod3_THREE_SAMPLES           0x00000002
#define CANmod3_SET_SJW(_sjw)           (_sjw<<2)
#define CANmod3_AUTO_RESTART            0x00000010
#define CANmod3_SET_TSEG2(_tseg2)       (_tseg2<<5)
#define CANmod3_SET_TSEG1(_tseg1)       (_tseg1<<8)
#define CANmod3_SET_BITRATE(_bitrate)   (_bitrate<<16)
#define CANmod3_ARB_ROUNDROBIN          0x00000000
#define CANmod3_ARB_FIXED_PRIO          0x00001000
#define CANmod3_BIG_ENDIAN              0x00000000
#define CANmod3_LITTLE_ENDIAN           0x00002000


// Error codes
#define CANmod3_OK			0
#define CANmod3_ERR			1
#define CANmod3_INIT_SPEED_ERR		2
#define CANmod3_NOT_INITIALIZED		3
#define CANmod3_NOT_STARTED		4
#define CANmod3_TXBUF_FULL		5
#define CANmod3_RXBUF_EMPTY		6
#define CANmod3_TSEG1_TOO_SMALL		7
#define CANmod3_TSEG2_TOO_SMALL		8
#define CANmod3_SJW_TOO_BIG		9
#define CANmod3_INVALID_REGISTER	10
#define CANmod3_BASIC_CAN_MAILBOX	11
#define CANmod3_NO_RTR_MAILBOX		12
#define CANmod3_INVALID_MAILBOX		13

//  Flag bits
#define CANmod3_NO_MSG			0x80
#define CANmod3_VALID_MSG		0x81

// A couple of definitions just to make the code more readable so we know what a 1 and 0 mean.
#define IDE_ON				1
#define IDE_OFF				0
#define RTR_ON				1
#define RTR_OFF				0


// Interrupt Bit Definitions
#define CANMOD3_INT_GLOBAL		1<<0    ///< Global interrupt
#define CANMOD3_INT_ARB_LOSS		1<<2    ///< Arbitration loss interrupt
#define CANMOD3_INT_OVR_LOAD		1<<3    ///< Overload interrupt
#define CANMOD3_INT_BIT_ERR		1<<4    ///< Bit error interrupt
#define CANMOD3_INT_STUFF_ERR		1<<5    ///< Bit stuffing error interrupt
#define CANMOD3_INT_ACK_ERR		1<<6    ///< Acknowledgement error interrupt
#define CANMOD3_INT_FORM_ERR		1<<7    ///< Format error interrupt
#define CANMOD3_INT_CRC_ERR		1<<8    ///< CRC error interrupt
#define CANMOD3_INT_BUS_OFF		1<<9    ///< Bus-off interrupt
#define CANMOD3_INT_RX_MSG_LOST		1<<10   ///< Rx message lost interrupt
#define CANMOD3_INT_TX_MSG		1<<11   ///< Tx message interupt
#define CANMOD3_INT_RX_MSG		1<<12   ///< Rx message interrupt
#define CANMOD3_INT_RTR_MSG		1<<13   ///< RTR message interrupt
#define CANMOD3_INT_STUCK_AT_0		1<<14   ///< Stuck-at-0 error interrupt
#define CANMOD3_INT_SST_FAILURE		1<<15   ///< Single-shot transmission error interrupt
#define CANMOD3_INT_ERROR_MASK		0xc7fc	///< all error interrupt
#define CANMOD3_INT_MASK		0xfffc	///< all interrupt expect global interrupt


// Tx Buffer
#define CANMOD3_TX_WPNH_EBL		1<<23
#define CANMOD3_TX_WPNL_EBL		1<<3
#define CANMOD3_TX_INT_EBL		1<<2
#define CANMOD3_TX_ABORT_REQ		1<<1
#define CANMOD3_TX_REQ			0x01


// Rx Buffer
#define CANMOD3_RX_WPNH_EBL		1<<23
#define CANMOD3_RX_WPNL_EBL		1<<7
#define CANMOD3_RX_LINK_EBL		1<<6
#define CANMOD3_RX_INT_EBL		1<<5
#define CANMOD3_RX_RTR_REPLY_EBL	1<<4
#define CANMOD3_RX_BUFFER_EBL		1<<3


//
//  CANmod3 driver version number
//
#define CANmod3_VERSION			0x0103

/*
* can send type: normal or sst(single shot)
*/
enum _can_send_type
{
	CAN_SEND_NORMAL = 0,
	CAN_SEND_SST,
};

/**
*  _CANmod3_msgobject contains a CAN message object. Following example code
*  shows different ways of using it.

@par Example:
@code
....
void example(){
CANmod3_MSGOBJECT msg;

msg.id = 0x123456;

// data as 32 bits, 16 bit at the time
msg.dataHigh = 0x11223344;
msg.dataLow = 0x55667788;

// or 8 bits at a time
msg.data[0] = 0xCC;

// set all flags at once
msg.l = 0x0;

// or one at a time
msg.dlc = 3;
msg.ide = 0;
msg.rtr = 0;
....
}
@endcode
*/
typedef struct _CANmod3_msgobject
{
	// CAN Message ID.
	volatile struct
	{
		UINT32 n_id:3;
		UINT32 id:29;
	};

	// CAN Message Data organized as two 32 bit words or 8 data bytes
	volatile union
	{
		struct
		{
			UINT32 dataHigh;
			UINT32 dataLow;
		};
		INT8 data[8];
	};

	// CAN Message flags and smaller values organized as one single 32 bit word or a number of bit fields.
	volatile union
	{
		UINT32 l;			// 32 bit flag

		struct
		{// Flags structure.

			UINT32 na0:16;		// [0..15] Message Valid Bit, 0 == Not valid.
			UINT32 dlc:4;		// [16..19] Data Length Code (number of bytes in a CAN message)
			UINT32 ide:1;		// [20] Extended Identifier Bit 1 == 29 bit identifier, 0 == 11 bit identifier.
			UINT32 rtr:1;		// [21] Remote Bit  0 == regular message
			UINT32 na1:9;		// [22..31]
		};
	};
} CANmod3_MSGOBJECT,*PCANmod3_MSGOBJECT;

/**
* _CANmod3_filterobject
*/

typedef struct _CANmod3_filterobject
{
	// Acceptance mask settings
	volatile union
	{
		UINT32 l;
		struct
		{
			UINT32 n_a:1;
			UINT32 rtr:1;
			UINT32 ide:1;
			UINT32 ext_id:18;
			UINT32 normal_id:11;
		};
	} amr;
	// Acceptance code settings
	volatile union
	{
		UINT32 l;
		struct
		{
			UINT32 n_a:1;
			UINT32 rtr:1;
			UINT32 ide:1;
			UINT32 ext_id:18;
			UINT32 normal_id:11;
		};
	} acr;
	// Acceptance mask and code settings for first two data bytes
	volatile union
	{
		UINT32 l;
		struct
		{
			UINT32 mask:16;
			UINT32 code:16;
		};
	} amcr_d;
}CANmod3_FILTEROBJECT,*PCANmod3_FILTEROBJECT;

/**
* _CANmod3_txmsgobject
*/

typedef struct _CANmod3_txmsgobject
{
	// CAN Message flags and smaller values organized as one single 32 bit word or a number of bit fields.
	volatile union
	{
		UINT32 l;               // 32 bit flag

		// Tx Flags structure.
		struct
		{
			UINT32 txReq:1;      // [0] TxReq
			UINT32 txAbort:1;    // [1] TxAbort
			UINT32 txIntEbl:1;   // [2] TxIntEbl
			UINT32 wpnl:1;       // [3] Write protect not for bit 2
			UINT32 na0:12;       // [4..15] Message Valid Bit, 0 == Not valid.
			UINT32 dlc:4;        // [16..19] Data Length Code (number of bytes in a CAN message)
			UINT32 ide:1;        // [20] Extended Identifier Bit 1 == 29 bit identifier, 0 == 11 bit identifier.
			UINT32 rtr:1;        // [21] Remote Bit  0 == regular message
			UINT32 na1:1;        // [22] Reserved
			UINT32 wpnh:1;       // [23] Write protect not for bits 21..16
			UINT32 na2:8;        // [24..31]
		};
	} txb;

	// CAN Message ID.
	volatile struct
	{
		UINT32 n_id:3;
		UINT32 id:29;
	};

	// CAN Message Data organized as two 32 bit words or 8 data bytes
	volatile union
	{
		struct
		{
			UINT32 dataHigh;
			UINT32 dataLow;
		};
		UINT8 data[8];
	};

} CANmod3_TXMSGOBJECT;

/**
* _CANmod3_rxmsgobject
*/

typedef struct _CANmod3_rxmsgobject
{
	// CAN Message flags and smaller values organized as one single 32 bit word or a number of bit fields.
	volatile union
	{
		UINT32 l;                 // 32 bit flag

		// Rx Flags structure.
		struct
		{
			UINT32 msgAv:1;         // [0] MsgAv
			UINT32 rtrReplyPend:1;  // [1] RTRreply_pending
			UINT32 rtrAbort:1;      // [2] RTRabort
			UINT32 bufferEbl:1;     // [3] BufferEnable
			UINT32 rtrReply:1;      // [4] RTRreply
			UINT32 rxIntEbl:1;      // [5] RxIntEbl
			UINT32 linkFlag:1;      // [6] LinkFlag
			UINT32 wpnl:1;          // [7] Write protect not for bit 6..3
			UINT32 na0:8;           // [8..15] Message Valid Bit, 0 == Not valid.
			UINT32 dlc:4;           // [16..19] Data Length Code (number of bytes in a CAN message)
			UINT32 ide:1;           // [20] Extended Identifier Bit 1 == 29 bit identifier, 0 == 11 bit identifier.
			UINT32 rtr:1;           // [21] Remote Bit  0 == regular message
			UINT32 na1:1;           // [22] Reserved
			UINT32 wpnh:1;          // [23] Write protect not for bits 21..16
			UINT32 na2:8;           // [24..31]
		};
	} rxb;

	// CAN Message ID.
	volatile struct
	{
		UINT32 n_id:3;
		UINT32 id:29;
	};

	// CAN Message Data organized as two 32 bit words or 8 data bytes
	volatile union
	{
		struct
		{
			UINT32 dataHigh;
			UINT32 dataLow;
		};
		UINT8 data[8];
	};

	// CAN Message Filter: Acceptance mask register
	volatile union
	{
		UINT32 l;
		struct
		{
			UINT32 n_a:1;
			UINT32 rtr:1;
			UINT32 ide:1;
			UINT32 id:29;
		};
	} amr;

	// CAN Message Filter: Acceptance code register
	volatile union
	{
		UINT32 l;
		struct
		{
			UINT32 n_a:1;
			UINT32 rtr:1;
			UINT32 ide:1;
			UINT32 id:29;
		};
	} acr;

	UINT32 amr_d;
	UINT32 acr_d;

} CANmod3_RXMSGOBJECT,*PCANmod3_RXMSGOBJECT;

/**
* Error status register
*/
typedef union error_status
{
	volatile UINT32 l;
	volatile struct
	{
		UINT32 tx_err_cnt:8;
		UINT32 rx_err_cnt:8;
		UINT32 error_stat:2;
		UINT32 txgte96:1;
		UINT32 rxgte96:1;
		UINT32 n_a:12;
	};
} ERROR_STATUS;

/**
* Buffer status register
* For canmod3x, this are two 32-bit registers. 
*/
typedef struct buffer_status
{
	UINT32 rxMsgAv;
	UINT32 txReq;
} BUFFER_STATUS;

/**
* Interrupt enable register
*/
typedef union int_enable
{
	volatile UINT32 l;
	volatile struct
	{
		UINT32 int_ebl:1;      //0
		UINT32 n_a0:1;         //1
		UINT32 arb_loss:1;     //2
		UINT32 ovr_load:1;     //3
		UINT32 bit_err:1;      //4
		UINT32 stuff_err:1;    //5
		UINT32 ack_err:1;      //6
		UINT32 form_err:1;     //7
		UINT32 crc_err:1;      //8
		UINT32 bus_off:1;      //9
		UINT32 rx_msg_loss:1;  //10
		UINT32 tx_msg:1;       //11
		UINT32 rx_msg:1;       //12
		UINT32 rtr_msg:1;      //13
		UINT32 stuck_at_0:1;   //14
		UINT32 sst_failure:1;  //15
		UINT32 n_a1:16;
	};
} INT_ENABLE, *PINT_ENABLE;

/**
* Interrupt status register
*/
typedef union int_status
{
	volatile UINT32 l;
	volatile struct
	{
		UINT32 n_a0:2;         //1..0
		UINT32 arb_loss:1;     //2
		UINT32 ovr_load:1;     //3
		UINT32 bit_err:1;      //4
		UINT32 stuff_err:1;    //5
		UINT32 ack_err:1;      //6
		UINT32 form_err:1;     //7
		UINT32 crc_err:1;      //8
		UINT32 bus_off:1;      //9
		UINT32 rx_msg_loss:1;  //10
		UINT32 tx_msg:1;       //11
		UINT32 rx_msg:1;       //12
		UINT32 rtr_msg:1;      //13
		UINT32 stuck_at_0:1;   //14
		UINT32 sst_failure:1;  //15
		UINT32 n_a1:16;
	};
} INT_STATUS,*PINT_STATUS;

/**
* Command register
*/
typedef union command_reg
{
	volatile UINT32 l;
	volatile struct
	{
		UINT32 run_stop:1;
		UINT32 listen_only:1;
		UINT32 loop_back:1;
		UINT32 sram_test:1;
		UINT32 sw_reset:1;
		UINT32 n_a:27;
	};
} COMMAND_REG;

/**
* Configuration register
*/
typedef union canmod3_config_reg
{
	volatile UINT32 l;
	volatile struct
	{
		UINT32 edge_mode:1;     //0
		UINT32 sampling_mode:1; //1
		UINT32 cfg_sjw:2;       //3..2
		UINT32 auto_restart:1;  //4
		UINT32 cfg_tseg2:3;     //7..5
		UINT32 cfg_tseg1:4;     //11..8
		UINT32 cfg_arbiter:1;   //12
		UINT32 endian:1;        //13
		UINT32 ecr_mode:1;      //14
		UINT32 n_a0:1;          //15
		UINT32 cfg_bitrate:15;  //30..16
		UINT32 n_a1:1;		//31
	};
} CANmod3_CONFIG_REG, *PCANmod3_CONFIG_REG;

/**
* Register mapping of CAN controller
*/
typedef struct CANmod3_device
{
	INT_STATUS		IntStatus;      ///< Interrupt status register
	INT_ENABLE		IntEbl;         ///< Interrupt enable register
	BUFFER_STATUS		BufferStatus;   ///< Buffer status indicators
	ERROR_STATUS		ErrorStatus;    ///< Error status
	COMMAND_REG		Command;        ///< CAN operating mode
	CANmod3_CONFIG_REG	Config;         ///< Configuration register
	UINT32			na;
	CANmod3_TXMSGOBJECT	TxMsg[CANmod3_TX_MAILBOX];   ///< Tx message buffers
	CANmod3_RXMSGOBJECT	RxMsg[CANmod3_RX_MAILBOX];   ///< Rx message buffers

} CANmod3_DEVICE;


#define CANmod3_PORT0				0
#define CANmod3_PORT1				1

#define CAN0_REGISTER_ADDRESS			((CANmod3_DEVICE *)TO_MCU_REG(A7DA_CAN0_MODULE_BASE))
#define CAN1_REGISTER_ADDRESS			((CANmod3_DEVICE *)TO_MCU_REG(A7DA_CAN1_MODULE_BASE))


#undef	ZONE_CAN_ISR
#define ZONE_CAN_ISR		0
#undef	ZONE_CAN_DEBUG
#define ZONE_CAN_DEBUG		0
#undef	ZONE_CAN_ERROR
#define ZONE_CAN_ERROR		1



/* Callback function of CAN Rx interrupts */
typedef void ( xCanIsrRx )( UINT8 Port, CANmod3_MSGOBJECT *pxCanMod3Msg, UINT32 BufferNum );

/* Callback function of CAN Tx interrupts */
typedef void ( xCanIsrTx )( UINT8 Port, UINT32 xTxMask);

/* Callback function of CAN Error interrupts */
typedef void ( xCanIsrError )( UINT8 Port , UINT32 xIntStatus );


/**
* Each CAN core instance has its own canmod3_instance_t structure that contains
* a register access pointer and proprietary data. The instance content should
* only be accessed by using the respective API functions.
* Each API function has a pointer to this instance as first argument. This way,
* implementations with several cores can be handled using the same low-level
* driver.
*/
typedef struct
{
	xCanIsrTx           *pxCanIsrTxCallback;
	xCanIsrRx 		    *pxCanIsrRxCallback;
	xCanIsrError 		*pxCanIsrErrCallback;
	UINT8			ucCanIntrNum;
	// Hardware related entries (pointer to device, interrupt number etc)
	CANmod3_DEVICE		*hw_reg;   /// Pointer to CAN registers.

	// Local data (eg pointer to local FIFO, irq number etc)
	UINT8  			basic_can_rx_mb; /// number of rx mailboxes used in basic CAN mode
	UINT8  			basic_can_tx_mb; /// number of tx mailboxes used in basic CAN mode
	UINT32 			tx_request_mask;
} canmod3_instance_t;


/**
* Implementations (API)
*/
INT32 CANmod3_RegisterTxCallback( UINT8 ucPort, xCanIsrTx *pxTxCallback );
INT32 CANmod3_RegisterRxCallback( UINT8 ucPort, xCanIsrRx *pxRxCallback );
INT32 CANmod3_RegisterErrCallback( UINT8 ucPort, xCanIsrError *pxErrCallback );
INT32 CANmod3_UnRegisterTxCallback( UINT8 ucPort );
INT32 CANmod3_UnRegisterRxCallback( UINT8 ucPort );
INT32 CANmod3_UnRegisterErrCallback( UINT8 ucPort );


UINT8  CANmod3_Init(canmod3_instance_t* this_can, UINT8 port_num, UINT32 bitRate, PCANmod3_CONFIG_REG pcan_config, UINT8 basic_can_rx_mb, UINT8 basic_can_tx_mb);
void   CANmod3_SetConfigReg(canmod3_instance_t* this_can, UINT32 cfg);
UINT8  CANmod3_Mode(canmod3_instance_t* this_can, UINT32 mode);
UINT8  CANmod3_Start(canmod3_instance_t* this_can);
UINT8  CANmod3_Stop(canmod3_instance_t* this_can);
UINT32 CANmod3_GetID(PCANmod3_MSGOBJECT pMsg);
UINT32 CANmod3_SetID(PCANmod3_MSGOBJECT pMsg);
UINT32 CANmod3_FormatMask( UINT32 id, UINT8 ide, UINT8 rtr);
UINT8 CANmod3_SetGlobalIntEbl(canmod3_instance_t* this_can);
UINT8 CANmod3_ClrGlobalIntEbl(canmod3_instance_t* this_can);
UINT8 CANmod3_SetIntEbl(canmod3_instance_t* this_can, UINT32 flag);
UINT32 CANmod3_GetGlobalIntEbl(canmod3_instance_t* this_can);
UINT32 CANmod3_GetIntEbl(canmod3_instance_t* this_can);
UINT8 CANmod3_ClearIntStatus(canmod3_instance_t* this_can, UINT32 flag);
UINT32 CANmod3_GetIntStatus(canmod3_instance_t* this_can);
UINT8 CANmod3_SetRTRMessageN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_MSGOBJECT pMsg);
UINT8  CANmod3_RTRMessageAbortN(canmod3_instance_t* this_can, UINT8 n);
UINT8 CANmod3_ConfigBuffer(canmod3_instance_t* this_can, PCANmod3_FILTEROBJECT pFilter, UINT32 RxCtrl);
UINT8  CANmod3_ConfigBufferN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_RXMSGOBJECT pMsg);
UINT8  CANmod3_GetMessageN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_MSGOBJECT pMsg);
UINT8  CANmod3_GetMessage(canmod3_instance_t* this_can, PCANmod3_MSGOBJECT pMsg);
UINT8  CANmod3_GetMessageAv(canmod3_instance_t* this_can);
UINT8  CANmod3_SendMessageN(canmod3_instance_t* this_can, UINT8 n, UINT32 bSst, PCANmod3_MSGOBJECT pMsg);
UINT8 CANmod3_TxMaiboxNReady(canmod3_instance_t* this_can, UINT8 n);
UINT8  CANmod3_SendMessageAbortN(canmod3_instance_t* this_can, UINT8 n);
UINT8  CANmod3_SendMessageReady(canmod3_instance_t* this_can);
UINT8  CANmod3_SendMessage(canmod3_instance_t* this_can, PCANmod3_MSGOBJECT pMsg);
UINT8  CANmod3_GetMaskN(canmod3_instance_t* this_can, UINT8 n, PUINT32 pamr, PUINT32 pacr, PUINT16 pdta_amr, PUINT16 pdta_acr);
UINT8  CANmod3_SetMaskN(canmod3_instance_t* this_can, UINT8 n, UINT32 amr, UINT32 acr, UINT16 dta_amr, UINT16 dta_acr);
UINT32 CANmod3_GetRxBufferStatus(canmod3_instance_t* this_can);
UINT32 CANmod3_GetTxBufferStatus(canmod3_instance_t* this_can);
UINT8  CANmod3_GetErrorStatus(canmod3_instance_t* this_can, PUINT32 stat);
UINT32 CANmod3_GetRxErrorCount(canmod3_instance_t* this_can);
UINT32 CANmod3_GetRxGTE96(canmod3_instance_t* this_can);
UINT32 CANmod3_GetTxErrorCount(canmod3_instance_t* this_can);
UINT32 CANmod3_GetTxGTE96(canmod3_instance_t* this_can);

#endif
