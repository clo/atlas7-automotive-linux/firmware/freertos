/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CANBUS_H
#define CANBUS_H
#ifndef __NO_FREERTOS__
#include "semphr.h"
#include "task.h"
#include "event_groups.h"
#else
#include "ctypes.h"
#endif
#include "CANmod3.h"

/**
*    debug message
*/
#undef	ZONE_CAN_DEBUG
#define ZONE_CAN_DEBUG		0
#undef	ZONE_CAN_ERROR
#define ZONE_CAN_ERROR		1

#define CAN_PORT0		CANmod3_PORT0
#define CAN_PORT1		CANmod3_PORT1

/**
*      Configuration and Speed definitions
*/
#define CAN_PRESET		CANmod3_PRESET
#define CAN_SAMPLE_BOTH_EDGES	CANmod3_SAMPLE_BOTH_EDGES
#define CAN_THREE_SAMPLES	CANmod3_THREE_SAMPLES
#define CAN_SET_SJW(_sjw)	CANmod3_SET_SJW( _sjw )
#define CAN_AUTO_RESTART	CANmod3_AUTO_RESTART
#define CAN_SET_TSEG2(_tseg2)	CANmod3_SET_TSEG2( _tseg2 )
#define CAN_SET_TSEG1(_tseg1)	CANmod3_SET_TSEG1( _tseg1 )
#define CAN_SET_BITRATE(_bitrate)	CANmod3_SET_BITRATE( _bitrate )
#define CAN_ARB_ROUNDROBIN	CANmod3_ARB_ROUNDROBIN
#define CAN_ARB_FIXED_PRIO	CANmod3_ARB_FIXED_PRIO
#define CAN_BIG_ENDIAN		CANmod3_BIG_ENDIAN
#define CAN_LITTLE_ENDIAN	CANmod3_LITTLE_ENDIAN

/**
* Manual setting with specified fields
*/
#define CAN_SPEED_MANUAL	CANmod3_SPEED_MANUAL


// Following constants are based on a CAN clock of 48MHz and 40MHz
// Bus length  Sample Point    Comments

// 5000m       81%  Sample bit three times
#define CAN_SPEED_48M_5K	CAN_SET_BITRATE(383)|CAN_SET_TSEG1(15)|CAN_SET_TSEG2(7)|CAN_SET_SJW(2)|CANmod3_THREE_SAMPLES
#define CAN_SPEED_40M_5K	CAN_SET_BITRATE(319)|CAN_SET_TSEG1(15)|CAN_SET_TSEG2(7)|CAN_SET_SJW(2)|CANmod3_THREE_SAMPLES

// 5000m       81%  Sample bit three times
#define CAN_SPEED_48M_10K	CAN_SET_BITRATE(299)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_10K	CAN_SET_BITRATE(249)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

//2500m       81%  Sample bit three times
#define CAN_SPEED_48M_20K	CAN_SET_BITRATE(149)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_20K	CAN_SET_BITRATE(124)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

// 1000m       87%
#define CAN_SPEED_48M_50K	CAN_SET_BITRATE(59)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_50K	CAN_SET_BITRATE(49)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

// 600m        87%
#define CAN_SPEED_48M_100K	CAN_SET_BITRATE(29)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_100K	CAN_SET_BITRATE(24)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

// 500m        87%
#define CAN_SPEED_48M_125K	CAN_SET_BITRATE(23)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_125K	CAN_SET_BITRATE(19)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(1)

// 250m        87%
#define CAN_SPEED_48M_250K	CAN_SET_BITRATE(11)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_250K	CAN_SET_BITRATE(9)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

//100m        75% @ 8M, 87% @ 16M
#define CAN_SPEED_48M_500K	CAN_SET_BITRATE(5)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_500K	CAN_SET_BITRATE(4)|CAN_SET_TSEG1(12)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

//50m         75%
#define CAN_SPEED_48M_800K	CAN_SET_BITRATE(5)|CAN_SET_TSEG1(6)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_800K	CAN_SET_BITRATE(4)|CAN_SET_TSEG1(6)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)

//25m         75%
#define CAN_SPEED_48M_1M	CAN_SET_BITRATE(5)|CAN_SET_TSEG1(4)|CAN_SET_TSEG2(1)|CAN_SET_SJW(0)
#define CAN_SPEED_40M_1M	CAN_SET_BITRATE(4)|CAN_SET_TSEG1(4)|CAN_SET_TSEG2(1)|CAN_SET_SJW(1)


// OPERATING Mode Definitions
#define CANOP_MODE_NORMAL		0x01   ///< Start controller
#define CANOP_MODE_LISTEN_ONLY		0x03   ///< Set listen-only mode and start controller
#define CANOP_MODE_INT_LOOPBACK		0x07   ///< Set internal loopback mode and start controller
#define CANOP_MODE_EXT_LOOPBACK		0x05   ///< Set external loopback mode and start controller
#define CANOP_SRAM_TEST_MODE		0x08   ///< Set SRAM test mode
#define CANOP_SW_RESET			0x10   ///< Perform software reset

/* Error codes */
#define CAN_OK				CANmod3_OK
#define CAN_ERR				CANmod3_ERR
#define CAN_INIT_SPEED_ERR		CANmod3_INIT_SPEED_ERR
#define CAN_NOT_INITIALIZED		CANmod3_NOT_INITIALIZED
#define CAN_NOT_STARTED			CANmod3_NOT_STARTED
#define CAN_TXBUF_FULL			CANmod3_TXBUF_FULL
#define CAN_RXBUF_EMPTY			CANmod3_RXBUF_EMPTY
#define CAN_TSEG1_TOO_SMALL		CANmod3_TSEG1_TOO_SMALL
#define CAN_TSEG2_TOO_SMALL		CANmod3_TSEG2_TOO_SMALL
#define CAN_SJW_TOO_BIG			CANmod3_SJW_TOO_BIG
#define CAN_INVALID_REGISTER		CANmod3_INVALID_REGISTER
#define CAN_BASIC_CAN_MAILBOX		CANmod3_BASIC_CAN_MAILBOX
#define CAN_NO_RTR_MAILBOX		CANmod3_NO_RTR_MAILBOX
#define CAN_INVALID_MAILBOX		CANmod3_INVALID_MAILBOX

#define CANBUS_ID_WAKEUP_M3		(0x1)
#define CANBUS_ID_WAKEUP_A7		(0x2)
#define CANBUS_ID_REARVIEW_START	(0x4)
#define CANBUS_ID_REARVIEW_STOP		(0x5)

#define CANBUS_TRIGGER_REARVIEW_MAGIC	(0x1314169)

#define CANBUS_BUFFER_IS_USED		0xff
#define CANBUS_BUFFER_IS_NOT_USED	0

#define CANBUS_DUMP_QUEUE_LENGH		32

#define CANBUS_DLC_MAX			8

typedef struct _CAN_msgobject
{
	bool	ide;    	// Extended Identifier Bit 1 == 29 bit identifier, 0 == 11 bit identifier
	bool	rtr;		// Remote Bit  0 == regular message
	UINT32	id;		// CAN Message ID.
	UINT32	dlc;		// Data Length Code (number of bytes in a CAN message)


	// CAN Message Data organized as two 32 bit words or 8 data bytes
	volatile union
	{
		struct
		{
			UINT32 dataHigh;
			UINT32 dataLow;
		};
		UINT8 data[8];
	};
} CAN_MSGOBJECT;
typedef CAN_MSGOBJECT * PCAN_MSGOBJECT;

typedef CANmod3_FILTEROBJECT	CAN_FILTEROBJECT;
typedef CANmod3_FILTEROBJECT *	PCAN_FILTEROBJECT;

typedef CANmod3_CONFIG_REG	CAN_CONFIG_REG ;
typedef CANmod3_CONFIG_REG * 	PCAN_CONFIG_REG ;

typedef canmod3_instance_t	can_handle;


#ifndef __NO_FREERTOS__
typedef struct _CAN_HANDLE
{
	// indicate which port it is.
	UINT8			ucCanPortNum;
	SemaphoreHandle_t       xRxBufferLock;
	QueueHandle_t		xRxChannels[CANmod3_RX_MAILBOX];
	QueueHandle_t		xVirtCanQueue;
	EventGroupHandle_t  xTxEventH;	/* for message box 16 ~ 31 */
	EventGroupHandle_t  xTxEventL;	/* for message box 0  ~ 15 */
	EventGroupHandle_t  xErrEvent;
	canmod3_instance_t 	*pxCanMod3Instance;
} CAN_HANDLE, *PCAN_HANDLE;

#endif

// Implementations (API)
void vCanSetQueueFullErrorEnabled(BOOL enable);

INT32 xCanInit( UINT8 ucPort, UINT32 uxBitRate,
			PCAN_CONFIG_REG pxCanConfig,
			UINT8 uxBasicCanRxMb,
			UINT8 uxBasicCanTxMb );

INT32 xCanDeinit( UINT8 ucPort );

INT32 xCanStart( UINT8 ucPort, UINT32 uxWorkMode );

INT32 xCanStop( UINT8 ucPort );

INT32 xCanReConfigCoreInRunning( UINT8 ucPort, UINT32 uxCfg );

#ifndef __NO_FREERTOS__
xQueueHandle xCanReceiveQueueCreate( UINT8 ucPort,
				PCAN_FILTEROBJECT pxFilter,
				UINT8 ucN,
				portBASE_TYPE uxQueueLength );

#endif
INT32 xCanReceiveQueueReConfig( UINT8 ucPort,
				PCAN_FILTEROBJECT pxFilter,
				UINT8 ucN);

INT32 xCanStopReceiveQueue( UINT8 ucPort, UINT8 ucN );

INT32 xCanDeleteReceiveQueue( UINT8 ucPort, UINT8 ucN );

#ifndef __NO_FREERTOS__
QueueHandle_t xGetVirtCanReceiveQueue( UINT8 ucPort );

EventGroupHandle_t xGetCanTransmitEventGroup( UINT8 ucPort, UINT8 ucHigh );

EventGroupHandle_t xGetCanErrorEventGroup( UINT8 ucPort );

#endif

INT32 xCanSend( UINT8 ucPort, PCAN_MSGOBJECT pxMsg, UINT8 ucN, UINT32 uxBsst);

INT32 xCanAbortTxBufferN( UINT8 ucPort, UINT8 ucN);

INT32 xCanRTRMessageNCreate( UINT8 ucPort,
				PCAN_MSGOBJECT pxMsg,
				PCAN_FILTEROBJECT pxFilter,
				UINT8 ucN );

INT32 xCanSetRTRMessageN( UINT8 ucPort, PCAN_MSGOBJECT pxMsg, UINT8 ucN);

INT32 xCanRTRMessageAbortN( UINT8 ucPort, UINT8 ucN );

INT32 xCanRTRMessageNDelete( UINT8 ucPort, UINT8 ucN );

INT32 xCanArbTestFunction( PCAN_MSGOBJECT pxMsg0, PCAN_MSGOBJECT pxMsg1,
				UINT32 ucN, UINT32 uxBsst );

INT32 xCanSuspend( UINT8 ucPort, PCAN_FILTEROBJECT pxFilter, UINT8 ucN );

void vCanRetainSend( UINT32 canid, UINT32 dataH, UINT32 dataL );

UINT32 xCanResume( void );

void vCanbusInitialize( UINT8 ucPort, UINT32 uxCanClk );


#endif
