/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __C_TYPES_H__
#define __C_TYPES_H__

#include <stdint.h>
#include "compiler.h"

#ifndef container_of
#define container_of(ptr, type, member) \
    (type *)((char *)(ptr) - (char *) &((type *)0)->member)
#endif

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

typedef long long INT64;
typedef long LONG;
typedef int INT32;
typedef short INT16;
typedef char INT8;

typedef long long * PINT64;
typedef long * PLONG;
typedef int * PINT32;
typedef short * PINT16;
typedef char * PINT8;

typedef unsigned long long UINT64;
typedef unsigned long ULONG;
typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;

typedef unsigned long long * PUINT64;
typedef unsigned long * PULONG;
typedef unsigned int * PUINT32;
typedef unsigned short * PUINT16;
typedef unsigned char * PUINT8;

typedef unsigned long long u64;
typedef unsigned long ulong;
typedef unsigned long ul;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef unsigned long dma_addr_t;

#ifndef __ssize_t_defined
typedef unsigned int size_t;
#define __ssize_t_defined
#endif

#ifndef __off_t_defined
typedef unsigned long off_t;
#define __off_t_defined
#endif

typedef volatile unsigned long ulv;
typedef volatile unsigned char u8v;
typedef volatile unsigned short u16v;

typedef enum {
	false = 0,
	true = 1,
} bool;

typedef enum {
	FALSE = 0,
	TRUE = 1,
} BOOL;

static inline int isspace (int ch)
{
	if ((ch == ' ') || (ch == '\r') ||
		(ch == '\n') || (ch == '\v') ||
		(ch == '\t') || (ch == '\f'))
		return TRUE;
	return FALSE;
}

static inline int isalnum (int ch)
{
	/* ASCII only */
	if (((ch >= '0') && (ch <= '9')) ||
		((ch >= 'A') && (ch <= 'Z')) ||
		((ch >= 'a') && (ch <= 'z')))
		return TRUE;
	return FALSE;
}

static inline int isdigit (int ch)
{
	/* ASCII only */
	if ((ch >= '0') && (ch <= '9'))
		return TRUE;
	return FALSE;
}

static inline int isupper (int ch)
{
	/* ASCII only */
	if ((ch >= 'A') && (ch <= 'Z'))
		return TRUE;
	return FALSE;
}


#endif /* __C_TYPES_H__ */
