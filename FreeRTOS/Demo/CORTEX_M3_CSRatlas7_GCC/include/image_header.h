/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _IMAGE_HEADER_H_
#define _IMAGE_HEADER_H_

#include "ctypes.h"
#include "pm.h"

#define IMAGE_ID_BOOTLOADER   0
#define IMAGE_ID_RECOVER      1
#define IMAGE_ID_FREERTOS     2
#define IMAGE_ID_WAKEUP       3
#define IMAGE_ID_UBOOT        4
#define IMAGE_ID_KALIMBA_FW	5
#define IMAGE_ID_AUDIO_FILE	6
#define IMAGE_ID_FREERTOS_DEP 7

#ifndef __M3_BACKUP__
#define IMAGE_HEADER(base)  ((image_header_t *)((base)+28*1024))
#else
#define IMAGE_HEADER(base)  ((image_header_t *)((base)+(1024+28)*1024))
#endif

typedef uint32_t image_header_t[3];

#define DECLARE_API_GET_IMAGE_ADDRESS()						\
uint32_t uxGetImageAddress(uint32_t uxId)					\
{															\
	uint32_t uxAddress;										\
	uint32_t uxBase;										\
	if (PM_BOOT_INDEPENDENT == uxPmCheckBootMode()) {		\
		uxBase = 0x18000000;								\
	}														\
	else													\
	{														\
		uxBase = 0x05400000;								\
	}														\
	if (uxId == IMAGE_HEADER(uxBase)[uxId][0]) {			\
		uxAddress = IMAGE_HEADER(uxBase)[uxId][1];			\
	} else {												\
		uxAddress = 0xFFFFFFFF;								\
	}														\
															\
	return uxAddress;										\
}

#define DECLARE_API_GET_IMAGE_SIZE()						\
uint32_t uxGetImageSize(uint32_t uxId)						\
{															\
	uint32_t uxSize;										\
	uint32_t uxBase;										\
	if (PM_BOOT_INDEPENDENT == uxPmCheckBootMode())	{		\
		uxBase = 0x18000000;								\
	}														\
	else													\
	{														\
		uxBase = 0x05400000;								\
	}														\
	if (uxId == IMAGE_HEADER(uxBase)[uxId][0]) {			\
		uxSize = IMAGE_HEADER(uxBase)[uxId][2];				\
	} else {												\
		uxSize = 0xFFFFFFFF;								\
	}														\
															\
	return uxSize;											\
}

#define DECLARE_API_GET_IMAGE_ADDRESS_AND_SIZE()						\
uint32_t uxGetImageAddressAndSize(uint32_t uxId, uint32_t *uxSize)		\
{																		\
	uint32_t uxAddress;													\
	uint32_t uxBase;													\
	if (PM_BOOT_INDEPENDENT == uxPmCheckBootMode()) {					\
		uxBase = 0x18000000;											\
	}																	\
	else																\
	{																	\
		uxBase = 0x05400000;											\
	}																	\
	if (uxId == IMAGE_HEADER(uxBase)[uxId][0]) {						\
		uxAddress = IMAGE_HEADER(uxBase)[uxId][1];						\
		*uxSize   = IMAGE_HEADER(uxBase)[uxId][2];						\
	} else {															\
		uxAddress = 0xFFFFFFFF;											\
	}																	\
																		\
	return uxAddress;													\
}

extern uint32_t uxGetImageAddress(uint32_t uxId);
extern uint32_t uxGetImageSize(uint32_t uxId);
extern uint32_t uxGetImageAddressAndSize(uint32_t uxId, uint32_t *uxSize);
#endif /* _IMAGE_HEADER_H_ */
