/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __IO_H__
#define __IO_H__

#include "ctypes.h"
#include "soc_mem.h"

/*
 * Definitions for register access
 */
/* Convert SoC Reg in MCU(M3) address space */
#define TO_MCU_REG(addr) (SOC_SYSTEM_MAP + addr)

/* Access MCU(M3) Registers */
#define REG(addr)	(*((volatile unsigned long *)(addr)))

/* Access SoC Registers, must use the SOC_SYSTEM_MAP offset */
#define SOC_REG(addr) (*((volatile unsigned long *)(TO_MCU_REG(addr))))

#define REG_UPDATE_BITS(reg, mask, val)  do { \
	volatile unsigned long _val = SOC_REG(reg); \
	_val &= (~(mask)); \
	_val |= (val); \
	SOC_REG(reg) = _val; \
} while(0)

/* Convert DRAM in SoC address space */
#define TO_SOC_DRAM(addr) (addr + SOC_SYSTEM_MAP)

/* Convert DRAM in MCU(M3) address space */
#define TO_MCU_DRAM(addr) (addr - SOC_SYSTEM_MAP)

/* Convert no cacheable MCU(M3) address to SoC address space */
#define SYSAHB_TO_SYSTEM_MAP(addr) (addr - SOC_SYSAHB_DDRAM + SOC_SYSTEM_MAP)

/* Convert DRAM in SYSTEM IO address space */
#ifdef __ATLAS7_STEP_B__
#define TO_MCU_IO_DRAM(addr)	(TO_MCU_DRAM(addr) + SOC_SYSAHB_DDRAM)
#else
#define TO_MCU_IO_DRAM(addr)	TO_MCU_DRAM(addr)
#endif

/* Access DRAM in System AHB */
#define DRAM_IO(addr)	(addr + SOC_SYSAHB_DDRAM)

/* Access memory in MCU address space */
#define MEM(addr) (*((volatile unsigned long *)(addr)))
#define CH8(addr) (*((volatile char *)(addr)))

/* Access DRAM in SoC address space */
#define SOC_DRAM(addr) (*((volatile unsigned long *)(TO_MCU_DRAM(addr))))

/* The Address in which area */
#define IS_IN_DRAM(x)	((x) < L2_MEMORY_DRAM_SZ)
#define IS_IN_SPRAM(x)	((x) >= L2_MEMORY_SPRAM && (x) < L2_MEMORY_RETAIN)
#define IS_IN_RETAIN(x)	((x) >= L2_MEMORY_RETAIN && (x) < L2_MEMORY_RESERVE_0)
#define IS_IN_QSPI(x)	((x) >= L2_MEMORY_QSPI && (x) < L2_MEMORY_QSPI_END)

/* Definitions for bit operation */
#define BITS_PER_BYTE	8
#define BITS_PER_LONG	(sizeof(long) * BITS_PER_BYTE)
#define BIT_WORD(nr)	((nr) / BITS_PER_LONG)
#define BIT_MASK(nr)	(1UL << ((nr) % BITS_PER_LONG))

static inline void __set_bit(int nr, volatile unsigned long *addr)
{
	unsigned long mask = BIT_MASK(nr);
	unsigned long *p = ((unsigned long *)addr) + BIT_WORD(nr);

	*p  |= mask;
}

static inline void __clear_bit(int nr, volatile unsigned long *addr)
{
	unsigned long mask = BIT_MASK(nr);
	unsigned long *p = ((unsigned long *)addr) + BIT_WORD(nr);

	*p &= ~mask;
}

static inline int __test_bit(int nr, const volatile unsigned long *addr)
{
        return 1UL & (addr[BIT_WORD(nr)] >> (nr & (BITS_PER_LONG-1)));
}

#define set_bit(nr, v)		__set_bit(nr, (volatile unsigned long *)&(v))
#define test_bit(nr, v)		__test_bit(nr, (volatile unsigned long *)&(v))
#define clear_bit(nr, v)	__clear_bit(nr, (volatile unsigned long *)&(v))

#define SHIFT_L(v, s)	((v) << (s))
#define SHIFT_R(v, s)	((v) >> (s))

#define ALIGN(x, a)	(((x) + (a) - 1) & ~((a) - 1))

/* definitions for page operation */
#define PAGE_SIZE 4096
#define PAGE_MASK (PAGE_SIZE - 1)

#define PAGE_ALIGN_UP(val)	ALIGN(val, PAGE_SIZE)
#define PAGE_ALIGN_DOWN(val)	((val) & ~(PAGE_MASK))
#define PAGE_ALIGN(val)		PAGE_ALIGN_UP(val)

/* bit type */
#define BIT0	0x00000001
#define BIT1	0x00000002
#define BIT2	0x00000004
#define BIT3	0x00000008
#define BIT4	0x00000010
#define BIT5	0x00000020
#define BIT6	0x00000040
#define BIT7	0x00000080
#define BIT8	0x00000100
#define BIT9	0x00000200
#define BIT10	0x00000400
#define BIT11	0x00000800
#define BIT12	0x00001000
#define BIT13	0x00002000
#define BIT14	0x00004000
#define BIT15	0x00008000
#define BIT16	0x00010000
#define BIT17	0x00020000
#define BIT18	0x00040000
#define BIT19	0x00080000
#define BIT20	0x00100000
#define BIT21	0x00200000
#define BIT22	0x00400000
#define BIT23	0x00800000
#define BIT24	0x01000000
#define BIT25	0x02000000
#define BIT26	0x04000000
#define BIT27	0x08000000
#define BIT28	0x10000000
#define BIT29	0x20000000
#define BIT30	0x40000000
#define BIT31	0x80000000

#define BIT(n)	(1 << (n))

#endif	/* __IO_H__ */
