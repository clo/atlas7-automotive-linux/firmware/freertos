/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __PM_H__
#define __PM_H__

enum {
	PM_BOOT_DEPENDENT = 0xE000,
	PM_BOOT_INDEPENDENT = 0xE001,
};

enum {
	PM_WAKE_COLD_BOOT,
	PM_WAKE_WARM_BOOT,
	PM_WAKE_CAN,
	PM_WAKE_WDOG,
};

#define PM_IPC_CODE_MASK	0xFF
enum {
	PM_HIBERNATION = 0,
	PM_DEEP_SLEEP,
	PM_RESET,
	PM_FORCE_SHUTDOWN,
	PM_HIBERNATION_M3,
	PM_DEEP_SLEEP_M3,
	PM_RTC_WAKEUP,
	PM_M3_NOC_DISCONNECT,
	PM_RESET_M3,
	PM_M3_RCV_HOLD,
	PM_NORMAL
};

enum {
	PM_CAN_PARTIAL = 0,
	PM_CAN_WAKEUP_LISTENER0,
	PM_CAN_WAKEUP_LISTENER1,
	PM_CAN_WAKEUP_CONTROLLER0,
	PM_CAN_WAKEUP_CONTROLLER1,
	PM_CAN_WAKEUP_CONTROLLER0_OR_1,
	PM_CAN_WAKEUP_TRANSCEIVER0,
	PM_CAN_WAKEUP_TRANSCEIVER1,
};

/* The DDRM valid flag for M3 dependent wakeup */
#define M3_WAKEUP_FLAG_VALID		0x5F8C6D3E
#define M3_CAN1_WAKUP_FLAG_VALID	0x45572189

#define PM_PARAM_ADDR			0x17E0FFC0
#define PM_ARG_BOOT_ENTRY 		(PM_PARAM_ADDR)
#define PM_ARG_QSPI_REG_CTRL		(PM_PARAM_ADDR + 0x4)
#define PM_ARG_QSPI_REG_RDC		(PM_PARAM_ADDR + 0x8)
#define PM_ARG_QSPI_REG_DEFMEM		(PM_PARAM_ADDR + 0xC)
#define PM_ARG_QSPI_REG_DELAY_LINE	(PM_PARAM_ADDR + 0x10)
#define PM_ARG_CAN_TRIGGER_REARVIEW	(PM_PARAM_ADDR + 0x14)

#define PM_M3_CAN1_WAKUP			(PM_PARAM_ADDR + 0x34)	/* Test of CAN1 wakeup support */
#define PM_BOOT_TIME_MEM  			(PM_PARAM_ADDR + 0x38)
#define PM_RECOVER_FLAG_MEM  		(PM_PARAM_ADDR + 0x3C)
#define PM_ARG_LAST_OFFSET			0x3C /* Could not exceed */

/* The canbus wake up mode for pm*/
#define PM_WAKEUP_CANBUS_MODE_GOTO_SLEEP	(0x0)
#define PM_WAKEUP_CANBUS_MODE_WAKEUP_M3		(0x1)
#define PM_WAKEUP_CANBUS_MODE_WAKEUP_A7		(0x2)
#define PM_WAKEUP_CANBUS_MODE_TRIGGER_REARVIEW	(0x3)

#define ENABLE_M3_CAN1_WAKUP() (MEM(PM_M3_CAN1_WAKUP)==M3_CAN1_WAKUP_FLAG_VALID)

void vChangeCpuClock( uint32_t uxClk );
uint32_t uxPmCheckBootMode( void );
uint32_t uxPmCheckWakeupSource( void );
void vPmClearWakeupSource( uint32_t uxVal );
uint32_t uxGetPmLevel(void);
void vGotoPowerSavingMode( uint32_t uxPmLevel );
void vPmEnableCanWakeup( uint32_t uxType );
void vNocClkReconnectAndNoWait( void );
void vNocClkDisconnect( void );
void vM3WakeupA7( void );
void vGotoPowerResetA7( void );
void vPmResetNorFlash(void);
void vPmPowerModeManager(void);
#endif /* __PM_H__ */
