/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __QSPI_H__
#define __QSPI_H__

/* QSPI control register defines */
#define QSPI_CLK_DELAY(x)	( ( ( x ) & 0xFF ) << 0 )
#define QSPI_RX_FIFO_THD(x)	( ( ( x ) & 0xFF ) << 8 )
#define QSPI_TX_FIFO_THD(x)	( ( ( x ) & 0xFF ) << 16 )
#define QSPI_ENTER_DPM		BIT24
/* SPI MODE: 1: CPOL=1, CPHA=1; 0: CPOL=0, CPHA=0 */
#define QSPI_SPI_MODE		BIT25
#define QSPI_SOFT_RESET		BIT26
#define QSPI_CLK_DIV_MASK	0xF
#define QSPI_CLK_DIV(x)		( ( ( x ) & QSPI_CLK_DIV_MASK ) << 28 )

/* QSPI status register defines */
#define QSPI_DATA_OUT_AV	BIT0
#define QSPI_DATA_IN_RDY	BIT1
#define QSPI_STATUS_DPM		BIT2
#define QSPI_REQUEST_RDY	BIT3
#define QSPI_FREAD_BUSY		BIT4
#define QSPI_DEVICE_SR_OFFSET	24
#define QSPI_DEVICE_SR_MASK	0xFF

/* QSPI default memory register defines */
#define QSPI_FAST_READ		0
#define QSPI_READ2O		1
#define QSPI_READ2IO		2
#define QSPI_READ4O		3
#define QSPI_READ4IO		4
#define QSPI_READ_OPCODE_MASK	0x7
#define QSPI_DEF_MEM_READ_OPCODE(x)	( ( ( x ) & QSPI_READ_OPCODE_MASK) << 0 )
#define QSPI_PP			0
#define QSPI_PP2O		1
#define QSPI_PP4O		2
#define QSPI_PP4IO		3
#define QSPI_WRITE_OPCODE_MASK	0x7
#define QSPI_WRITE_OPCODE_OFFSET 3
#define QSPI_DEF_MEM_WRITE_OPCODE(x)	( ( ( x ) & QSPI_WRITE_OPCODE_MASK) << 3 )
#define QSPI_DEF_MEM_ATTR_32	BIT6
#define QSPI_DEF_MEM_ATTR_DPM	BIT7
#define QSPI_DEF_MEM_CHIP_SELECT(x)	( ( ( x ) & 0x7) << 8 )
#define QSPI_DEF_MEM_AUTO_ID	BIT11

/* QSPI custom instruction setup register defines */
#define QSPI_CI_OPCODE(x)	( (  (x ) & 0xFF ) << 0 )
#define QSPI_CI_LENGTH(x)	( ( ( x + 1 ) & 0xF ) << 8 )
#define QSPI_CI_SPI_WPN		BIT( 12 )
#define QSPI_CI_SPI_HOLDN	BIT( 13 )
#define QSPI_CI_CHECK_WIP	BIT( 14 )
#define QSPI_CI_WREN		BIT( 15 )

/* QSPI read dummy cycles register defines */
#define QSPI_RX_DELAY_MAX	0x7
#define QSPI_RDC_READ2IO(x)	( ( ( x ) & 0xF ) << 0 )
#define QSPI_RDC_READ4IO(x)	( ( ( x ) & 0xF ) << 4 )
#define QSPI_RX_DELAY(x)	( ( ( x ) & 0x7 ) << 8 )

/* QSPI XOTF enable register defines */
#define QSPI_XOTF_DEACTIVATED	( 0x0 << 0 )
#define QSPI_XOTF_ACTIVATED	( 0x1 << 0 )

char* cQspiGetNorName( void );
void vQspiSetClock( UINT32 uxClk );
void vQspiSetMode(UINT32 uxMode);
void vQspiSetClkDelay( UINT32 uxClkDelay );
void vQspiSetWorkMode( UINT32 uxRdOp, UINT32 uxWrtOp, UINT32 uxExtAddr );
void vQspiSetDummy( UINT32 uxR2IO, UINT32 uxR4IO );
void vQspiSetRxDelay( UINT32 uxRxDelay );
void vQspiEnterDpm( void );
void vQspiLeaveDpm( void );
void vQspiEnter32addr( void );
void vQspiLeave32addr( void );
void vQspiSetXipBase( UINT32 uxBase);
void vQspiSwitchHost( void );
void vQspiSwitchXotf( void );
void vQspiInit( UINT32 uxDelayLine );
void vQspiEraseSector( UINT32 uxFlashAddr );
void vQspiEraseFlash( UINT32 uxFlashAddr, UINT32 uxSize);
void vQspiIoWrite( UINT32 uxFlashAddr, const UINT32 * pxSource, UINT32 uxSize );
void vQspiIoRead( UINT32 uxFlashAddr, UINT32 *pxDest, UINT32 uxSize );
void vQspiBoost( void );
void vQspiEnableXipFromWakeup( void );

#endif	/* __QSPI_H__ */
