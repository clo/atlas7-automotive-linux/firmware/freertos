/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_ARMM3_H__
#define __ATLAS7_ARMM3_H__

#define _ARMM3_REG_BASE		0x18830000
#define _ARMM3_NATIVE_REG_BASE	0x20000000

/* ARMM3 CPU registers */
#define ARMM3_NATIVE_ISP	(_ARMM3_NATIVE_REG_BASE + 0x4800)
#define ARMM3_NATIVE_IPC	(_ARMM3_NATIVE_REG_BASE + 0x4804)
#define ARMM3_NATIVE_ICC	(_ARMM3_NATIVE_REG_BASE + 0xA000)
#define ARMM3_NATIVE_ICM	(_ARMM3_NATIVE_REG_BASE + 0xA004)
#define ARMM3_NATIVE_IRES	(_ARMM3_NATIVE_REG_BASE + 0xA008)
#define ARMM3_NATIVE_ISTATUS	(_ARMM3_NATIVE_REG_BASE + 0xA00C)
#define ARMM3_NATIVE_DCC	(_ARMM3_NATIVE_REG_BASE + 0xA010)
#define ARMM3_NATIVE_DCM	(_ARMM3_NATIVE_REG_BASE + 0xA014)
#define ARMM3_NATIVE_DRES	(_ARMM3_NATIVE_REG_BASE + 0xA018)
#define ARMM3_NATIVE_DSTATUS	(_ARMM3_NATIVE_REG_BASE + 0xA01C)

/* ARMM3 CPU registers after NoC */
#define A7DA_ARMM3_ISP		(_ARMM3_REG_BASE + 0x4800)
#define A7DA_ARMM3_IPC		(_ARMM3_REG_BASE + 0x4804)
#define A7DA_ARMM3_ICC		(_ARMM3_REG_BASE + 0xA000)
#define A7DA_ARMM3_ICM		(_ARMM3_REG_BASE + 0xA004)
#define A7DA_ARMM3_IRES		(_ARMM3_REG_BASE + 0xA008)
#define A7DA_ARMM3_ISTATUS	(_ARMM3_REG_BASE + 0xA00C)
#define A7DA_ARMM3_DCC		(_ARMM3_REG_BASE + 0xA010)
#define A7DA_ARMM3_DCM		(_ARMM3_REG_BASE + 0xA014)
#define A7DA_ARMM3_DRES		(_ARMM3_REG_BASE + 0xA018)
#define A7DA_ARMM3_DSTATUS	(_ARMM3_REG_BASE + 0xA01C)

/* ICACHE Config Register Bits */
#define ICACHE_CONFIG_ENABLE	( BIT0 )
#define ICACHE_CONFIG_AV	( BIT1 )	/* Abort Cycle On Vector Fetch Enabled */

/* ICACHE Management Register Bits */
#define ICACHE_MGM_INVALIDATE	( BIT0 )
#define ICACHE_MGM_RATE_EN	( BIT2 )	/* Rate counter logic Enabled */
#define ICACHE_MGM_RATE_SEL	( BIT3 )	/* Rate counter output selection, Bit[4:3]*/
#define ICACHE_MGM_RATE_THR	( BIT5 )	/* Rate counter threshold, Bit[12:5]*/

/* ICACHE Status Register Bits */
#define ICACHE_STATUS_DUAL_HIT_RD_ERR	( BIT0 )
#define ICACHE_STATUS_DUAL_HIT_WR_ERR	( BIT1 )
#define ICACHE_STATUS_LINE_BUF_IDLE	( BIT16 )
#define ICACHE_STATUS_BCVI_IDLE		( BIT17 )

/* DCACHE Config Register Bits */
#define DCACHE_CONFIG_ENABLE		( BIT0 )

/* DCACHE Management Register Bits */
#define DCACHE_MGM_INVALIDATE	( BIT0 )
#define DCACHE_MGM_FLUSH	( BIT1 )
#define DCACHE_MGM_RATE_EN	( BIT2 )	/* Rate counter logic Enabled */
#define DCACHE_MGM_RATE_SEL	( BIT3 )	/* Rate counter output selection, Bit[4:3]*/
#define DCACHE_MGM_RATE_THR	( BIT5 )	/* Rate counter threshold, Bit[12:5]*/

/* DCACHE Status Register Bits */
#define DCACHE_STATUS_DUAL_HIT_RD_ERR	( BIT0 )
#define DCACHE_STATUS_DUAL_HIT_WR_ERR	( BIT1 )
#define DCACHE_STATUS_LINE_BUF_IDLE	( BIT16 )
#define DCACHE_STATUS_BCVI_IDLE		( BIT17 )

#endif /* __ATLAS7_ARMM3_H__ */
