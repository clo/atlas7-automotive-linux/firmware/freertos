/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SOC_CPU_H__
#define __SOC_CPU_H__

#define _CPU_REG_BASE 0x10230000

#define A7DA_CPU0_SRAM_CONFIG0		(_CPU_REG_BASE + 0x0000)
#define A7DA_CPU0_SRAM_CONFIG1		(_CPU_REG_BASE + 0x0004)
#define A7DA_CPU1_SRAM_CONFIG0		(_CPU_REG_BASE + 0x0008)
#define A7DA_CPU1_SRAM_CONFIG1		(_CPU_REG_BASE + 0x000C)
#define A7DA_L2_SRAM_CONFIG		(_CPU_REG_BASE + 0x0010)
#define A7DA_CPU_CONFIG			(_CPU_REG_BASE + 0x0018)
#define A7DA_CPU_BOOT_CFG		(_CPU_REG_BASE + 0x001C)
#define A7DA_CPU_STANDBY_STATUS		(_CPU_REG_BASE + 0x0020)
#define A7DA_CPU_CSSI_LPI_CTRL		(_CPU_REG_BASE + 0x0100)
#define A7DA_VERSION			(_CPU_REG_BASE + 0xFFFC)

#define M3_RESET_LATCH_REGISTER		A7DA_CPU_BOOT_CFG

#endif /* __SOC_CPU_H__ */
