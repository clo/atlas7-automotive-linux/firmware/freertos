/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_GPIO_H__
#define __ATLAS7_GPIO_H__

#include <io.h>

#define GPIO_MAX_PIN	32

typedef enum
{
	eGpioRtcm = 0,
	eGpioMediamLvds,
	eGpioMediamUartNand,
	eGpioVdifmGnss,
	eGpioVdifmLcdVip,
	eGpioVdifmSdioI2s,
	eGpioVdifmSpRgmii,
	/* End */
	eGpioChipMax,
} eGpioChipId;

typedef enum
{
	eGpioIrqTypeNone = 0,
	eGpioIrqTypeEdgeRising,
	eGpioIrqTypeEdgeFalling,
	eGpioIrqTypeEdgeBoth,
	eGpioIrqTypeLevelLow,
	eGpioIrqTypeLevelHigh,
} eGpioIrqType;

/* Chip definition of GPIO RTCM */
#define GPIO_RTCM_BASE			0x18890000
#define GPIO_RTCM_PINS			13

/* Definition of GPIO RTCM PINS */
#define GPIO_RTC_0	0	/* rtc_gpio_0 Both */
#define GPIO_RTC_1	1	/* rtc_gpio_1 Both */
#define GPIO_RTC_2	2	/* rtc_gpio_2 Both */
#define GPIO_RTC_3	3	/* rtc_gpio_3 Both */
#define GPIO_RTC_4	4	/* low_bat_ind_b Both */
#define GPIO_RTC_5	5	/* can0_tx Both */
#define GPIO_RTC_6	6	/* can0_rx Both */
#define GPIO_RTC_7	7	/* spi0_clk Both */
#define GPIO_RTC_8	8	/* spi0_cs_b Both */
#define GPIO_RTC_9	9	/* spi0_io_0 Both */
#define GPIO_RTC_10	10	/* spi0_io_1 Both */
#define GPIO_RTC_11	11	/* spi0_io_2 Both */
#define GPIO_RTC_12	12	/* spi0_io_3 Both */
#define GPIO_RTC_13	13	/* io_on */

/* Chip definition of GPIO MEDIAM LVDS */
#define GPIO_MEDIAM_LVDS_BASE		0x17040000
#define GPIO_MEDIAM_LVDS_PINS		10

/* Definition of GPIO MEDIAM LVDS PINS */
#define GPIO_LVDS_0	0	/* lvds_tx0d0p */
#define GPIO_LVDS_1	1	/* lvds_tx0d0n */
#define GPIO_LVDS_2	2	/* lvds_tx0d1p */
#define GPIO_LVDS_3	3	/* lvds_tx0d1n */
#define GPIO_LVDS_4	4	/* lvds_tx0d2p */
#define GPIO_LVDS_5	5	/* lvds_tx0d2n */
#define GPIO_LVDS_6	6	/* lvds_tx0d3p */
#define GPIO_LVDS_7	7	/* lvds_tx0d3n */
#define GPIO_LVDS_8	8	/* lvds_tx0d4p */
#define GPIO_LVDS_9	9	/* lvds_tx0d4n */

/* Chip definition of GPIO MEDIAM UART_NAND */
#define GPIO_MEDIAM_UART_NAND_BASE	0x17040100
#define GPIO_MEDIAM_UART_NAND_PINS	24

/* Definition of GPIO MEDIAM UART_NAND PINS */
#define GPIO_NAND_0	0	/* df_ad_0 */
#define GPIO_NAND_1	1	/* df_ad_1 */
#define GPIO_NAND_2	2	/* df_ad_2 */
#define GPIO_NAND_3	3	/* df_ad_3 */
#define GPIO_NAND_4	4	/* df_ad_4 */
#define GPIO_NAND_5	5	/* df_ad_5 */
#define GPIO_NAND_6	6	/* df_ad_6 */
#define GPIO_NAND_7	7	/* df_ad_7 */
#define GPIO_NAND_8	8	/* df_cle */
#define GPIO_NAND_9	9	/* df_ale */
#define GPIO_NAND_10	10	/* df_we_b */
#define GPIO_NAND_11	11	/* df_re_b */
#define GPIO_NAND_12	12	/* df_ry_by */
#define GPIO_NAND_13	13	/* df_cs_b_0 */
#define GPIO_NAND_14	14	/* df_cs_b_1 */
#define GPIO_NAND_15	15	/* df_dqs */
#define GPIO_UART_0	16	/* uart0_tx */
#define GPIO_UART_1	17	/* uart0_rx */
#define GPIO_UART_2	18	/* uart1_tx */
#define GPIO_UART_3	19	/* uart1_rx */
#define GPIO_UART_4	20	/* uart3_tx */
#define GPIO_UART_5	21	/* uart3_rx */
#define GPIO_UART_6	22	/* uart4_tx */
#define GPIO_UART_7	23	/* uart4_rx */
#define GPIO_JTAG_0	24	/* jtag_tdo */
#define GPIO_JTAG_1	25	/* jtag_tms */
#define GPIO_JTAG_2	26	/* jtag_tck */
#define GPIO_JTAG_3	27	/* jtag_tdi */
#define GPIO_JTAG_4	28	/* jtag_trstn */

/* Chip definition of GPIO VDIFM GNSS */
#define GPIO_VDIFM_GNSS_BASE		0x13300000
#define GPIO_VDIFM_GNSS_PINS		19

/* Definition of GPIO VDIFM GNSS PINS */
#define GPIO_0		0	/* gpio_0 */
#define GPIO_1		1	/* gpio_1 */
#define GPIO_2		2	/* gpio_2 */
#define GPIO_3		3	/* gpio_3 */
#define GPIO_4		4	/* gpio_4 */
#define GPIO_5		5	/* gpio_5 */
#define GPIO_6		6	/* gpio_6 */
#define GPIO_7		7	/* gpio_7 */
#define GPIO_8		8	/* sda_0 */
#define GPIO_9		9	/* scl_0 */
#define GPIO_GNSS_0	10	/* trg_spi_clk */
#define GPIO_GNSS_1	11	/* trg_spi_di */
#define GPIO_GNSS_2	12	/* trg_spi_do */
#define GPIO_GNSS_3	13	/* trg_spi_cs_b */
#define GPIO_GNSS_4	14	/* trg_acq_d1 */
#define GPIO_GNSS_5	15	/* trg_irq_b */
#define GPIO_GNSS_6	16	/* trg_acq_d0 */
#define GPIO_GNSS_7	17	/* trg_acq_clk */
#define GPIO_GNSS_8	18	/* trg_shutdown_b_out */

/* Chip definition of GPIO VDIFM LCD_VIP */
#define GPIO_VDIFM_LCD_VIP_BASE		0x13300100
#define GPIO_VDIFM_LCD_VIP_PINS		32

/* Definition of GPIO VDIFM LCD_VIP PINS */
#define GPIO_VIP_0	0	/* vip_0 */
#define GPIO_VIP_1	1	/* vip_1 */
#define GPIO_VIP_2	2	/* vip_2 */
#define GPIO_VIP_3	3	/* vip_3 */
#define GPIO_VIP_4	4	/* vip_4 */
#define GPIO_VIP_5	5	/* vip_5 */
#define GPIO_VIP_6	6	/* vip_6 */
#define GPIO_VIP_7	7	/* vip_7 */
#define GPIO_VIP_8	8	/* vip_pxclk */
#define GPIO_VIP_9	9	/* vip_hsync */
#define GPIO_VIP_10	10	/* vip_vsync */
#define GPIO_LCD_0	11	/* l_pclk */
#define GPIO_LCD_1	12	/* l_lck */
#define GPIO_LCD_2	13	/* l_fck */
#define GPIO_LCD_3	14	/* l_de */
#define GPIO_LCD_4	15	/* ldd_0 */
#define GPIO_LCD_5	16	/* ldd_1 */
#define GPIO_LCD_6	17	/* ldd_2 */
#define GPIO_LCD_7	18	/* ldd_3 */
#define GPIO_LCD_8	19	/* ldd_4 */
#define GPIO_LCD_9	20	/* ldd_5 */
#define GPIO_LCD_10	21	/* ldd_6 */
#define GPIO_LCD_11	22	/* ldd_7 */
#define GPIO_LCD_12	23	/* ldd_8 */
#define GPIO_LCD_13	24	/* ldd_9 */
#define GPIO_LCD_14	25	/* ldd_10 */
#define GPIO_LCD_15	26	/* ldd_11 */
#define GPIO_LCD_16	27	/* ldd_12 */
#define GPIO_LCD_17	28	/* ldd_13 */
#define GPIO_LCD_18	29	/* ldd_14 */
#define GPIO_LCD_19	30	/* ldd_15 */
#define GPIO_LCD_20	31	/* lcd_gpio_20 */

/* Chip definition of GPIO VDIFM SDIO_I2S */
#define GPIO_VDIFM_SDIO_I2S_BASE	0x13300200
#define GPIO_VDIFM_SDIO_I2S_PINS	29

/* Definition of GPIO VDIFM SDIO_I2S PINS */
#define GPIO_SDIO_0	0	/* sdio2_clk */
#define GPIO_SDIO_1	1	/* sdio2_cmd */
#define GPIO_SDIO_2	2	/* sdio2_dat_0 */
#define GPIO_SDIO_3	3	/* sdio2_dat_1 */
#define GPIO_SDIO_4	4	/* sdio2_dat_2 */
#define GPIO_SDIO_5	5	/* sdio2_dat_3 */
#define GPIO_SDIO3_0	6	/* sdio3_clk */
#define GPIO_SDIO3_1	7	/* sdio3_cmd */
#define GPIO_SDIO3_2	8	/* sdio3_dat_0 */
#define GPIO_SDIO3_3	9	/* sdio3_dat_1 */
#define GPIO_SDIO3_4	10	/* sdio3_dat_2 */
#define GPIO_SDIO3_5	11	/* sdio3_dat_3 */
#define GPIO_SDIO3_6	12	/* coex_pio_0 */
#define GPIO_SDIO3_7	13	/* coex_pio_1 */
#define GPIO_SDIO3_8	14	/* coex_pio_2 */
#define GPIO_SDIO3_9	15	/* coex_pio_3 */
#define GPIO_SDIO5_0	16	/* sdio5_clk */
#define GPIO_SDIO5_1	17	/* sdio5_cmd */
#define GPIO_SDIO5_2	18	/* sdio5_dat_0 */
#define GPIO_SDIO5_3	19	/* sdio5_dat_1 */
#define GPIO_SDIO5_4	20	/* sdio5_dat_2 */
#define GPIO_SDIO5_5	21	/* sdio5_dat_3 */
#define GPIO_I2S_0	22	/* i2s_mclk */
#define GPIO_I2S_1	23	/* i2s_bclk */
#define GPIO_I2S_2	24	/* i2s_ws */
#define GPIO_I2S_3	25	/* i2s_dout0 */
#define GPIO_I2S_4	26	/* i2s_dout1 */
#define GPIO_I2S_5	27	/* i2s_dout2 */
#define GPIO_I2S_6	28	/* i2s_din */

/* Chip definition of GPIO VDIFM SP_RGMII */
#define GPIO_VDIFM_SP_RGMII_BASE	0x13300300
#define GPIO_VDIFM_SP_RGMII_PINS	27

/* Definition of GPIO VDIFM SP_RGMII PINS */
#define GPIO_RGMII_0	0	/* rgmii_txd_0 */
#define GPIO_RGMII_1	1	/* rgmii_txd_1 */
#define GPIO_RGMII_2	2	/* rgmii_txd_2 */
#define GPIO_RGMII_3	3	/* rgmii_txd_3 */
#define GPIO_RGMII_4	4	/* rgmii_txclk */
#define GPIO_RGMII_5	5	/* rgmii_tx_ctl */
#define GPIO_RGMII_6	6	/* rgmii_rxd_0 */
#define GPIO_RGMII_7	7	/* rgmii_rxd_1 */
#define GPIO_RGMII_8	8	/* rgmii_rxd_2 */
#define GPIO_RGMII_9	9	/* rgmii_rxd_3 */
#define GPIO_RGMII_10	10	/* rgmii_rx_clk */
#define GPIO_RGMII_11	11	/* rgmii_rxc_ctl */
#define GPIO_RGMII_12	12	/* rgmii_mdio */
#define GPIO_RGMII_13	13	/* rgmii_mdc */
#define GPIO_RGMII_14	14	/* rgmii_intr_n */
#define GPIO_SP_0	15	/* spi1_en */
#define GPIO_SP_1	16	/* spi1_clk */
#define GPIO_SP_2	17	/* spi1_din */
#define GPIO_SP_3	18	/* spi1_dout */
#define GPIO_SP_4	19	/* usp0_clk */
#define GPIO_SP_5	20	/* usp0_tx */
#define GPIO_SP_6	21	/* usp0_rx */
#define GPIO_SP_7	22	/* usp0_fs */
#define GPIO_SP_8	23	/* usp1_clk */
#define GPIO_SP_9	24	/* usp1_tx */
#define GPIO_SP_10	25	/* usp1_rx */
#define GPIO_SP_11	26	/* usp1_fs */

/* Definition bits of GPIO Control Registers */
#define GPIO_BIT_INTR_LOW_MASK		BIT0
#define GPIO_BIT_INTR_HIGH_MASK		BIT1
#define GPIO_BIT_INTR_TYPE_MASK		BIT2
#define GPIO_BIT_INTR_EN_MASK		BIT3
#define GPIO_BIT_INTR_STATUS_MASK	BIT4
#define GPIO_BIT_OUT_EN_MASK		BIT5
#define GPIO_BIT_DATAOUT_MASK		BIT6
#define GPIO_BIT_DATAIN_MASK		BIT7

/* Gpio Chip initialize function */
int32_t xGpioChipInitialize( eGpioChipId eChipId );

/* Functions can be used in normal scenes */
void vGpioEnableInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx );
void vGpioDisableInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx );
void vGpioClearInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx );
void vGpioSetDirectionInput( eGpioChipId eChipId, uint8_t ucGpioIdx );
void vGpioSetDirectionOutput( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue );
void vGpioSetValue( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue );
uint32_t uxGpioGetValue( eGpioChipId eChipId, uint8_t ucGpioIdx );

/* Functions can be used in ISR and sleep wakeup */
void vRetainGpioSetDirectionInputUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx );
void vRetainGpioSetDirectionOutputUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue );
void vRetainGpioSetValueUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue );
uint32_t uxRetainGpioGetValueUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx );

/* Callback function of GPIO pin interrupts */
typedef void ( xGpioPinISR )( eGpioChipId eChipId, uint8_t ucGpioIdx, void *pxData );

/* Functions for GPIO Client to register/unregister interrupts */
int32_t xGpioRegisterInterrupt( xGpioPinISR *pxGpioIsr, eGpioChipId eChipId, uint8_t ucGpioIdx, eGpioIrqType eIntrType, void *pxData );
void vGpioUnRegisterInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx );

#endif /* __ATLAS7_GPIO_H__ */
