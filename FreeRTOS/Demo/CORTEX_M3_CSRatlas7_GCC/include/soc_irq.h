/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_INTERRUPT_H__
#define __ATLAS7_INTERRUPT_H__

typedef enum IRQn
{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
	NonMaskableInt_IRQn = -14,	/*!< 2 Non Maskable Interrupt                             */
	MemoryManagement_IRQn = -12,/*!< 4 Cortex-M3 Memory Management Interrupt              */
	BusFault_IRQn = -11,	/*!< 5 Cortex-M3 Bus Fault Interrupt                      */
	UsageFault_IRQn = -10,	/*!< 6 Cortex-M3 Usage Fault Interrupt                    */
	SVCall_IRQn = -5,		/*!< 11 Cortex-M3 SV Call Interrupt                       */
	DebugMonitor_IRQn = -4,	/*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
	PendSV_IRQn = -2,		/*!< 14 Cortex-M3 Pend SV Interrupt                       */
	SysTick_IRQn = -1,		/*!< 15 Cortex-M3 System Tick Interrupt                   */

/******  M3 specific Interrupt Numbers *********************************************************/
	INT_TIMER_0_IRQn = 0,
	INT_TIMER_1_IRQn = 1,
	INT_TIMER_2_IRQn = 2,
	INT_I2S1_IRQn = 3,
	INT_SPDIF_IRQn = 4,
	INT_MEDIA_IRQn = 5,
	INT_GRAPHIC_IRQn = 6,
	INT_VSS_VITERBI_IRQn = 7,
	INT_VSS_RD_DMAC_IRQn = 8,
	INT_VSS_WR_DMAC_IRQn = 9,
	INT_USB0_IRQn = 10,
	INT_USB1_IRQn = 11,
	INT_DMAC0_IRQn = 12,
	INT_GPIO_MEDIAM_0_IRQn = 13,
	INT_GPIO_MEDIAM_1_IRQn = 14,
	INT_SPI0_IRQn = 15,
	INT_SPI1_IRQn = 16,
	INT_UART0_IRQn = 17,
	INT_UART1_IRQn = 18,
	INT_UART2_IRQn = 19,
	INT_USP0_IRQn = 20,
	INT_USP1_IRQn = 21,
	INT_USP2_IRQn = 22,
	INT_SDIO23_IRQn = 23,
	INT_I2C0_IRQn = 24,
	INT_I2C1_IRQn = 25,
	INT_CC_SEC_IRQn = 26,
	INT_CC_PUB_IRQn = 27,
	INT_SYS2PCI_IRQn = 28,
	INT_CPU_PMU_IRQn = 29,
	INT_LCD0_IRQn = 30,
	INT_VPP0_IRQn = 31,
	INT_PWRC_IRQn = 32,
	INT_TSC_IRQn = 33,
	INT_TSCADC_IRQn = 34,
	INT_AC97_IRQn = 35,
	INT_ROM_IRQn = 36,
	INT_SYS2PCI2_IRQn = 37,
	INT_SDIO01_IRQn = 38,
	INT_SDIO45_IRQn = 39,
	INT_PCICOPY_IRQn = 40,
	INT_NAND_IRQn = 41,
	INT_SECURITY_IRQn = 42,
	INT_GPIO_VDIFM_0_IRQn = 43,
	INT_GPIO_VDIFM_1_IRQn = 44,
	INT_GPIO_VDIFM_2_IRQn = 45,
	INT_GPIO_VDIFM_3_IRQn = 46,
	INT_GPIO_RTCM_IRQn = 47,
	INT_PULSEC_IRQn = 48,
	INT_TIMER_3_IRQn = 49,
	INT_TIMER_4_IRQn = 50,
	INT_TIMER_5_IRQn = 51,
	INT_SYSRTC_ALARM0_IRQn = 52,
	INT_SYSRTC_TIC_IRQn = 53,
	INT_SYSRTC_ALARM1_IRQn = 54,
	INT_DMAC2_IRQn = 55,
	INT_DMAC3_IRQn = 56,
	INT_A7CA_WRAP_IRQn = 57,
	INT_TIMER_WATCHDOG_1ST_IRQn = 58,
	INT_GMAC_SBD_IRQn = 59,
	INT_IACC_IRQn = 60,
	INT_G2D_IRQn = 61,
	INT_LCD1_IRQn = 62,
	INT_VIP1_IRQn = 63,
	INT_LVDS_IRQn = 64,
	INT_VPP1_IRQn = 65,
	INT_UART3_IRQn = 66,
	INT_CBUS0_IRQn = 67,
	INT_CBUS1_IRQn = 68,
	INT_UART4_IRQn = 69,
	INT_GMAC_PMT_IRQn = 70,
	INT_UART5_IRQn = 71,
	INT_MEDIAE0_IRQn = 72,
	INT_MEDIAE1_IRQn = 73,
	INT_TIMER_6_IRQn = 74,
	INT_TIMER_7_IRQn = 75,
	INT_TIMER_8_IRQn = 76,
	INT_TIMER_9_IRQn = 77,
	INT_TIMER_10_IRQn = 78,
	INT_TIMER_11_IRQn = 79,
	INT_DMACN_USP0_IRQn = 80,
	INT_DMAC_NAND_IRQn = 81,
	INT_CPU_PMU1_IRQn = 82,
	INT_CPU_AXIERRIRQ_IRQn = 83,
	INT_DMACN_VIP1_IRQn = 84,
	INT_AFECVDVIP0_IRQn = 85,
	INT_DMAC0_SEC_IRQn = 86,
	INT_DMAC2_SEC_IRQn = 87,
	INT_DMAC3_SEC_IRQn = 88,
	INT_DMAC4_SEC_IRQn = 89,
	INT_LCD0_FRONT_IRQn = 90,
	INT_LCD1_FRONT_IRQn = 91,
	INT_M3_CTINTISR0_IRQn = 92,
	INT_M3_CTINTISR1_IRQn = 93,
	INT_ID_CPU_CTIIRQ0_IRQn = 94,
	INT_ID_CPU_CTIIRQ1_IRQn = 95,
	INT_DCU_IRQn = 96,
	INT_NOCFIFO_IRQn = 97,
	INT_SDIO67_IRQn = 98,
	INT_DMAC4_IRQn = 99,
	INT_UART6_IRQn = 100,
	INT_USP3_IRQn = 101,
	INT_NOC_AUDMSCM_INTR_IRQn = 102,
	INT_NOC_BTM_INTR_IRQn = 103,
	INT_NOC_CPUM_INTR_IRQn = 104,
	INT_NOC_DDRM_INTR_IRQn = 105,
	INT_NOC_GNSSM_INTR_IRQn = 106,
	INT_NOC_GPUM_INTR_IRQn = 107,
	INT_NOC_MEDIAM_INTR_IRQn = 108,
	INT_NOC_RTCM_INTR_IRQn = 109,
	INT_NOC_VDIFM_INTR_IRQn = 110,
	INT_M3_TIMER_0_IRQn = 111,
	INT_M3_TIMER_1_IRQn = 112,
	INT_M3_TIMER_2_IRQn = 113,
	INT_RESERVED5_IRQn = 114,
	INT_RESERVED6_IRQn = 115,
	INT_RESERVED7_IRQn = 116,
	INT_RESERVED8_IRQn = 117,
	INT_RESERVED9_IRQn = 118,
	INT_RESERVED10_IRQn = 119,
	INT_RESERVED11_IRQn = 120,
	INT_RESERVED12_IRQn = 121,
	INT_IPC_TRGT2_INIT0_INTR1_IRQn = 122,
	INT_IPC_TRGT2_INIT1_INTR1_IRQn = 123,
	INT_IPC_TRGT2_INIT3_INTR1_IRQn = 124,
	INT_IPC_TRGT2_INIT0_INTR2_IRQn = 125,
	INT_IPC_TRGT2_INIT1_INTR2_IRQn = 126,
	INT_IPC_TRGT2_INIT3_INTR2_IRQn = 127
} IRQn_Type;

/* The lowest interrupt priority for API safe CALL */
#define API_SAFE_LOWEST_INTR_PRIO	( configLOWEST_INTR_PRIO )
#define API_SAFE_INTR_PRIO_0		( API_SAFE_LOWEST_INTR_PRIO )
#define API_SAFE_INTR_PRIO_1		( API_SAFE_LOWEST_INTR_PRIO - 1 )
#define API_SAFE_INTR_PRIO_2		( API_SAFE_LOWEST_INTR_PRIO - 2 )
#define API_SAFE_INTR_PRIO_3		( API_SAFE_LOWEST_INTR_PRIO - 3 )
#define API_SAFE_INTR_PRIO_4		( API_SAFE_LOWEST_INTR_PRIO - 4 )
#define API_SAFE_INTR_PRIO_5		( API_SAFE_LOWEST_INTR_PRIO - 5 )
#define API_SAFE_INTR_PRIO_6		( API_SAFE_LOWEST_INTR_PRIO - 6 )
#define API_SAFE_INTR_PRIO_7		( API_SAFE_LOWEST_INTR_PRIO - 7 )
#define API_SAFE_INTR_PRIO_8		( API_SAFE_LOWEST_INTR_PRIO - 8 )
#define API_SAFE_INTR_PRIO_9		( API_SAFE_LOWEST_INTR_PRIO - 9 )
#define API_SAFE_INTR_PRIO_10		( API_SAFE_LOWEST_INTR_PRIO - 10 )

/* The lowest interrupt priority for API unsafe CALL */
#define API_UNSAFE_LOWEST_INTR_PRIO	( configBASEPRI_INTR_PRIO - 1 )
#define API_UNSAFE_INTR_PRIO_0		( API_UNSAFE_LOWEST_INTR_PRIO )
#define API_UNSAFE_INTR_PRIO_1		( API_UNSAFE_LOWEST_INTR_PRIO - 1 )
#define API_UNSAFE_INTR_PRIO_2		( API_UNSAFE_LOWEST_INTR_PRIO - 2 )
#define API_UNSAFE_INTR_PRIO_3		( API_UNSAFE_LOWEST_INTR_PRIO - 3 )
#define API_UNSAFE_INTR_PRIO_4		( API_UNSAFE_LOWEST_INTR_PRIO - 4 )

#endif /* __ATLAS7_INTERRUPT_H__ */
