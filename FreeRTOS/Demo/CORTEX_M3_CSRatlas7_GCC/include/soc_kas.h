/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_KAS_H__
#define __ATLAS7_KAS_H__

#define _KAS_REG_BASE	0x10C10000

/* KAS Parameters registers */
#define	A7DA_KAS_MODE				(_KAS_REG_BASE + 0x0)
#define	A7DA_KAS_TRANSLATE			(_KAS_REG_BASE + 0x10)
#define	A7DA_KAS_DMA_MODE			(_KAS_REG_BASE + 0x14)
#define	A7DA_KAS_DMA_BASE			(_KAS_REG_BASE + 0x18)
#define	A7DA_KAS_DMA_ADDR			(_KAS_REG_BASE + 0x1C)
#define	A7DA_KAS_DMA_MODULO			(_KAS_REG_BASE + 0x20)
#define	A7DA_KAS_DMA_INC			(_KAS_REG_BASE + 0x24)
#define	A7DA_KAS_DMA_STATUS			(_KAS_REG_BASE + 0x28)
#define	A7DA_KAS_DMA_CHAIN_LINE			(_KAS_REG_BASE + 0x2C)
#define	A7DA_KAS_HWBP_SETUP			(_KAS_REG_BASE + 0x30)
#define	A7DA_KAS_EFUSE				(_KAS_REG_BASE + 0x34)
#define	A7DA_KAS_TIMER				(_KAS_REG_BASE + 0x38)
#define	A7DA_KAS_RATE_COUNTER_0			(_KAS_REG_BASE + 0x50)
#define	A7DA_KAS_RATE_COUNTER_1			(_KAS_REG_BASE + 0x54)
#define	A7DA_KAS_RATE_COUNTER_2			(_KAS_REG_BASE + 0x58)
#define	A7DA_KAS_RATE_COUNTER_3			(_KAS_REG_BASE + 0x5C)
#define	A7DA_KAS_RATE_COUNTER_4			(_KAS_REG_BASE + 0x60)
#define	A7DA_KAS_RATE_COUNTER_5			(_KAS_REG_BASE + 0x64)
#define	A7DA_KAS_RATE_COUNTER_ENABLE		(_KAS_REG_BASE + 0x68)
#define	A7DA_KAS_INTR_BOUNCE			(_KAS_REG_BASE + 0x84)

/* KAS DMAC registers */
#define A7DA_KAS_DMAC_DMA_ADDR			(_KAS_REG_BASE + 0x400)
#define A7DA_KAS_DMAC_DMA_XLEN			(_KAS_REG_BASE + 0x404)
#define A7DA_KAS_DMAC_DMA_YLEN			(_KAS_REG_BASE + 0x408)
#define A7DA_KAS_DMAC_DMA_CTRL			(_KAS_REG_BASE + 0x40C)
#define A7DA_KAS_DMAC_DMA_WIDTH			(_KAS_REG_BASE + 0x410)
#define A7DA_KAS_DMAC_DMA_VALID			(_KAS_REG_BASE + 0x414)
#define A7DA_KAS_DMAC_DMA_INT			(_KAS_REG_BASE + 0x418)
#define A7DA_KAS_DMAC_DMA_INT_EN		(_KAS_REG_BASE + 0x41C)
#define A7DA_KAS_DMAC_DMA_LOOP_CTRL		(_KAS_REG_BASE + 0x420)
#define A7DA_KAS_DMAC_DMA_INT_CNT		(_KAS_REG_BASE + 0x424)
#define A7DA_KAS_DMAC_DMA_TIMEOUT_CNT		(_KAS_REG_BASE + 0x428)
#define A7DA_KAS_DMAC_DMA_PAU_TIME_CNT		(_KAS_REG_BASE + 0x42C)
#define A7DA_KAS_DMAC_DMA_CUR_TABLE_ADDR	(_KAS_REG_BASE + 0x430)
#define A7DA_KAS_DMAC_DMA_CUR_DATA_ADDR		(_KAS_REG_BASE + 0x434)
#define A7DA_KAS_DMAC_DMA_MUL			(_KAS_REG_BASE + 0x438)
#define A7DA_KAS_DMAC_DMA_STATE0		(_KAS_REG_BASE + 0x43C)
#define A7DA_KAS_DMAC_DMA_STATE1		(_KAS_REG_BASE + 0x440)

/* KAS CPU Keyhole registers */
#define A7DA_KAS_CPU_KEYHOLE_ADDR		(_KAS_REG_BASE + 0x504)
#define A7DA_KAS_CPU_KEYHOLE_DATA		(_KAS_REG_BASE + 0x508)
#define A7DA_KAS_CPU_KEYHOLE_MODE		(_KAS_REG_BASE + 0x50C)

#define A7DA_KAS_PM_SRAM_BYTES			(256 * 1024)
#define A7DA_KAS_DM1_SRAM_BYTES			(96 * 1024)
#define A7DA_KAS_DM2_SRAM_BYTES			(96 * 1024)

#define A7DA_KAS_PM_SRAM_SIZE			(A7DA_KAS_PM_SRAM_BYTES / 4)
#define A7DA_KAS_DM1_SRAM_SIZE			(A7DA_KAS_DM1_SRAM_BYTES / 4)
#define A7DA_KAS_DM2_SRAM_SIZE			(A7DA_KAS_DM2_SRAM_BYTES / 4)

/* Setup Kalimba DSP */
void prvSetupKas(void);

#endif /* __ATLAS7_KAS_H__ */
