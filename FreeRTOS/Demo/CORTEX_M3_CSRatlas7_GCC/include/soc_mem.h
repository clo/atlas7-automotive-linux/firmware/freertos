/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_MEM_H__
#define __ATLAS7_MEM_H__

#include <sizes.h>

/* "L2" Memory Mapping */
/*
 * SW cannot access addresses 0x0 and 0x4 in DRAM when Cache is enabled,
 * since it will be remapped to SP/PC).
 */
#define L2_MEMORY_DRAM			0x00000000
#define L2_MEMORY_DRAM_SZ		(SZ_1M * 382)
#define L2_MEMORY_SPRAM			0x17E00000
#define L2_MEMORY_SPRAM_SZ		SZ_32K
#define L2_MEMORY_RETAIN		0x17E08000
#define L2_MEMORY_RETAIN_SZ		SZ_32K
#define L2_MEMORY_RESERVE_0		0x17E10000
#define L2_MEMORY_RESERVE_0_SZ		SZ_64K
#define L2_MEMORY_ROM			0x17E20000
#define L2_MEMORY_ROM_SZ		(SZ_1K * 24)
#define L2_MEMORY_RESERVE_1		0x17E2C000
#define L2_MEMORY_RESERVE_1_SZ		(SZ_1K * 1872)
#define L2_MEMORY_QSPI			0x18000000
#define L2_MEMORY_QSPI_SZ		SZ_128M
#define L2_MEMORY_QSPI_END		(L2_MEMORY_QSPI + SZ_128M)

/* A7 core access QSPI MEMORY Address */
#define SOC_MEMORY_QSPI			0x20000000

/* Tight Coupled Memory */
/*
 * TCM hadn't been used in TCM.
 * TCMs are available only when a cache is disabled.
 */
#define L1_MEMORY_ITCM			0x17E26000
#define L1_MEMORY_ITCM_SZ		SZ_16K
#define L1_MEMORY_DTCM			0x17E2A000
#define L1_MEMORY_DTCM_SZ		SZ_16K

/* M3 registers (before the NoC) */
#define ARMM3_REGISTER_START		0x20000000
#define ARMM3_REGISTER_SIZE		SZ_1K

/* System, currently reserved */
#define SYSTEM_RESERVED_0		0x20000400
#define SYSTEM_RESERVED_0_SZ		(SZ_512M - SZ_1K)

/*
 * A7DA SoC (system map)
 * NIU APB should shift SystemCode address in 1GB backwards
 * (e.g. virtual 0x40000000 should be translated to system 0x00000000).
 */
#define SOC_SYSTEM_MAP			0x40000000
#define SOC_SYSTEM_MAP_SZ		SZ_2G

/* System AHB DDRAM address */
#define SOC_SYSAHB_DDRAM		0x80000000
#define SOC_SYSAHB_DDRAM_SZ		SZ_1G

/* System, currently reserved */
#define SYSTEM_CURRENT_RESERVED_1	0xC0000000
#define SYSTEM_CURRENT_RESERVED_1_SZ	SZ_512M

/* Cortex-M3 system reserved */
#define CORTEX_M3_SYSTEM_RESERVED	0xE0000000
#define CORTEX_M3_SYSTEM_RESERVED_SZ	SZ_512M

/* Cortex-M3 SP and PC Remap address when cache is enabled */
#define CORTEX_M3_REMAP_SP		0x00000000
#define CORTEX_M3_REMAP_PC		0x00000004

#endif	/* __ATLAS7_MEM_H__ */
