/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ARMM3_OTP_H__
#define __ARMM3_OTP_H__

#define _DS_OTP_BASE 0x18260000

#define OTP_BR_STATUS		(_DS_OTP_BASE + 0x0000)
#define OTP_BR_CONFIG		(_DS_OTP_BASE + 0x0004)
#define OTP_BR_LOCK_BITS0	(_DS_OTP_BASE + 0x0008)
#define OTP_BR_LOCK_BITS1	(_DS_OTP_BASE + 0x000C)
#define OTP_BR_LOCK_BITS2	(_DS_OTP_BASE + 0x0010)
#define OTP_BR_LOCK_BITS3	(_DS_OTP_BASE + 0x0014)
#define OTP_BR_BOOTSTRAP0	(_DS_OTP_BASE + 0x001C)
#define OTP_BR_BOOTSTRAP1	(_DS_OTP_BASE + 0x0020)
#define OTP_STATUS		(_DS_OTP_BASE + 0x0800)
#define OTP_CMD	CMD		(_DS_OTP_BASE + 0x0804)
#define OTP_TESTMODE		(_DS_OTP_BASE + 0x0808)
#define OTP_TIMING_GRAN		(_DS_OTP_BASE + 0x0818)

int read_OTP_word(unsigned long offset, unsigned long *val);
int write_OTP_word(unsigned long offset, unsigned long val);

#endif /* __ARMM3_OTP_H__ */
