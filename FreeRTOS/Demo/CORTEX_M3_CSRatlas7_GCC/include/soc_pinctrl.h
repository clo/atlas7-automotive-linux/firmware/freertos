/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_PINCTRL_H__
#define __ATLAS7_PINCTRL_H__

#include <io.h>

#define USP2_SHARE_PINS_WITH_SDIO5

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define I2C0_SDA_PIN					127
#define I2C0_SCL_PIN					128

#define I2S_MCLK_PIN					112
#define I2S_BCLK_PIN					113
#define I2S_WS_PIN					114
#define I2S_DOUT0_PIN					115
#define I2S_DOUT1_PIN					116
#define I2S_DOUT2_PIN					117
#define I2S_DIN_PIN					118

#define USP0_CLK_PIN					141
#define USP0_TX_PIN					142
#define USP0_RX_PIN					143
#define USP0_FS_PIN					144

#define USP1_CLK_PIN					145
#define USP1_TX_PIN					146
#define USP1_RX_PIN					147
#define USP1_FS_PIN					148

#ifdef USP2_SHARE_PINS_WITH_SDIO5
/*
 * share the pins with sdio5
 * sdio5_clk --> usp2_clk
 * sdio5_dat1 --> usp2_fs
 * sdio5_cmd --> usp2_tx
 * sdio5_dat0 --> usp2_rx
 */
#define USP2_CLK_PIN					91
#define USP2_TX_PIN					92
#define USP2_RX_PIN					93
#define USP2_FS_PIN					94
#define FUNC_USP2		3
#endif

#ifdef USP2_SHARE_PINS_WITH_SPI1
/*
 * share the pins with spi1
 * spi1_clk --> usp2_clk
 * spi1_en --> usp2_fs
 * spi1_dout --> usp2_tx
 * spi_din --> usp2_rx
 */
#define USP2_CLK_PIN					18
#define USP2_TX_PIN					19
#define USP2_RX_PIN					20
#define USP2_FS_PIN					21
#define FUNC_USP2		2
#endif

/* Definition of multiple function select register */
#define FUNC_CLEAR_MASK         0x7
#define FUNC_GPIO               0
#define FUNC_I2C		1
#define FUNC_I2S		1
#define FUNC_USP0		1
#define FUNC_USP1		1
#define FUNC_ANALOGUE           0x8
#define ANA_CLEAR_MASK          0x1

/* Clear Register offset */
#define CLR_REG(r)      ((r) + 0x04)
/* Raw value of Driver-Strength Bits */
#define DS3     BIT(3)
#define DS2     BIT(2)
#define DS1     BIT(1)
#define DS0     BIT(0)
#define DSZ     0

/* Drive-Strength Intermediate Values */
#define DS_NULL         -1
#define DS_1BIT_IM_VAL  DS0
#define DS_1BIT_MASK    0x1
#define DS_2BIT_IM_VAL  (DS1 | DS0)
#define DS_2BIT_MASK    0x3
#define DS_4BIT_IM_VAL  (DS3 | DS2 | DS1 | DS0)
#define DS_4BIT_MASK    0xf

/* The Drive-Strength of 4WE Pad                 DS1  0  CO */
#define DS_4WE_3   (DS1 | DS0)	/* 1  1  3  */
#define DS_4WE_2   (DS1)	/* 1  0  2  */
#define DS_4WE_1   (DS0)	/* 0  1  1  */
#define DS_4WE_0   (DSZ)	/* 0  0  0  */

/* The Drive-Strength of 16st Pad                DS3  2  1  0  CO */
#define DS_16ST_15  (DS3 | DS2 | DS1 | DS0)	/* 1  1  1  1  15 */
#define DS_16ST_14  (DS3 | DS2 | DS0)	/* 1  1  0  1  13 */
#define DS_16ST_13  (DS3 | DS2 | DS1)	/* 1  1  1  0  14 */
#define DS_16ST_12  (DS2 | DS1 | DS0)	/* 0  1  1  1  7  */
#define DS_16ST_11  (DS2 | DS0)	/* 0  1  0  1  5  */
#define DS_16ST_10  (DS3 | DS1 | DS0)	/* 1  0  1  1  11 */
#define DS_16ST_9   (DS3 | DS0)	/* 1  0  0  1  9  */
#define DS_16ST_8   (DS1 | DS0)	/* 0  0  1  1  3  */
#define DS_16ST_7   (DS2 | DS1)	/* 0  1  1  0  6  */
#define DS_16ST_6   (DS3 | DS2)	/* 1  1  0  0  12 */
#define DS_16ST_5   (DS2)	/* 0  1  0  0  4  */
#define DS_16ST_4   (DS3 | DS1)	/* 1  0  1  0  10 */
#define DS_16ST_3   (DS1)	/* 0  0  1  0  2  */
#define DS_16ST_2   (DS0)	/* 0  0  0  1  1  */
#define DS_16ST_1   (DSZ)	/* 0  0  0  0  0  */
#define DS_16ST_0   (DS3)	/* 1  0  0  0  8  */

/* The Drive-Strength of M31 Pad                 DS0  CO */
#define DS_M31_0   (DSZ)	/* 0  0  */
#define DS_M31_1   (DS0)	/* 1  1  */

/* Raw values of Pull Option Bits */
#define PUN     BIT(1)
#define PD      BIT(0)
#define PE      BIT(0)
#define PZ      0

/* Definition of Pull Types */
#define PULL_UP         0
#define HIGH_HYSTERESIS 1
#define HIGH_Z          2
#define PULL_DOWN       3
#define PULL_DISABLE    4
#define PULL_ENABLE     5
#define PULL_UNKNOWN    -1

/* Pull Options for 4WE Pad                       PUN  PD  CO */
#define P4WE_PULL_MASK          0x3
#define P4WE_PULL_DOWN          (PUN | PD)	/* 1   1   3  */
#define P4WE_HIGH_Z             (PUN)	/* 1   0   2  */
#define P4WE_HIGH_HYSTERESIS    (PD)	/* 0   1   1  */
#define P4WE_PULL_UP            (PZ)	/* 0   0   0  */

/* Pull Options for 16ST Pad                      PUN  PD  CO */
#define P16ST_PULL_MASK         0x3
#define P16ST_PULL_DOWN         (PUN | PD)	/* 1   1   3  */
#define P16ST_HIGH_Z            (PUN)	/* 1   0   2  */
#define P16ST_PULL_UP           (PZ)	/* 0   0   0  */

/* Pull Options for M31 Pad                       PE */
#define PM31_PULL_MASK          0x1
#define PM31_PULL_ENABLED       (PE)	/* 1 */
#define PM31_PULL_DISABLED      (PZ)	/* 0 */

/* Pull Options for A/D Pad                       PUN  PD  CO */
#define PANGD_PULL_MASK         0x3
#define PANGD_PULL_DOWN         (PUN | PD)	/* 1   1   3  */
#define PANGD_HIGH_Z            (PUN)	/* 1   0   2  */
#define PANGD_PULL_UP           (PZ)	/* 0   0   0  */

/* Definition of Input Disable */
#define DI_MASK         0x1
#define DI_DISABLE      0x1
#define DI_ENABLE       0x0

/* Definition of Input Disable Value */
#define DIV_MASK        0x1
#define DIV_DISABLE     0x1
#define DIV_ENABLE      0x0

/* The Atlas7's Pad Type List */
enum ePinctrlPadType {
	ePAD_T_4WE_PD = 0,	/* ZIO_PAD3V_4WE_PD */
	ePAD_T_4WE_PU,		/* ZIO_PAD3V_4WE_PD */
	ePAD_T_16ST,		/* ZIO_PAD3V_SDCLK_PD */
	ePAD_T_M31_0204_PD,	/* PRDW0204SDGZ_M311311_PD */
	ePAD_T_M31_0204_PU,	/* PRDW0204SDGZ_M311311_PU */
	ePAD_T_M31_0610_PD,	/* PRUW0610SDGZ_M311311_PD */
	ePAD_T_M31_0610_PU,	/* PRUW0610SDGZ_M311311_PU */
	ePAD_T_AD,		/* PRDWUWHW08SCDG_HZ */
};

/*
 * struct xPinctrlPinDesc - boards/machines provide information on their
 * pins, pads or other muxable units in this struct
 * @ucNumber: unique pin number from the global pin number space
 * @pcName: a name for this pin
 */
struct xPinctrlPinDesc {
	uint8_t ucNumber;
	const char *pcName;
};

/* Convenience macro to define a single named or anonymous pin descriptor */
#define PINCTRL_PIN(a, b) { .ucNumber = a, .pcName = b }

/**
 * struct xPinctrlPadConf - Atlas7 Pad Configuration
 * @uxId		The ID of this Pad.
 * @eType:		The type of this Pad.
 * @uxMuxReg:		The mux register offset.
 *			This register contains the mux.
 * @uxPupdReg:		The pull-up/down register offset.
 * @uxDrvstrReg:	The drive-strength register offset.
 * @uxAdCtrlReg:	The Analogue/Digital Control register.
 *
 * @ucMuxBit:		The start bit of mux register.
 * @ucPupdBit:		The start bit of pull-up/down register.
 * @ucDrvstrBit:	The start bit of drive-strength register.
 * @ucAdCtrlBit:	The start bit of analogue/digital register.
 */
struct xPinctrlPadConfig {
	const uint32_t uxId;
	enum ePinctrlPadType eType;
	uint32_t uxMuxReg;
	uint32_t uxPupdReg;
	uint32_t uxDrvstrReg;
	uint32_t uxAdCtrlReg;
	/* bits in register */
	uint8_t ucMuxBit;
	uint8_t ucPupdBit;
	uint8_t ucDrvstrBit;
	uint8_t ucAdCtrlBit;
};

#define PADCONF(pad, t, mr, pr, dsr, adr, mb, pb, dsb, adb)	\
	{							\
		.uxId = pad,					\
		.eType = t,					\
		.uxMuxReg = mr,					\
		.uxPupdReg = pr,					\
		.uxDrvstrReg = dsr,				\
		.uxAdCtrlReg = adr,				\
		.ucMuxBit = mb,					\
		.ucPupdBit = pb,					\
		.ucDrvstrBit = dsb,				\
		.ucAdCtrlBit = adb,				\
	}
#define ATLAS7_PINCTRL_BANK_0_PINS      18

/* Simple map data structure */
struct xPinctrlMapData {
	uint8_t ucIdx;
	uint8_t ucData;
};
/**
 * struct xPinctrlPullInfo - Atlas7 Pad pull info
 * @ucPadType:The type of this Pad.
 * @ucMask:The mas value of this pin's pull bits.
 * @pxV2s: The map of pull register value to pull status.
 * @pxS2v: The map of pull status to pull register value.
 */
struct xPinctrlPullInfo {
	uint8_t ucPadType;
	uint8_t ucMask;
	const struct xPinctrlMapData *pxV2s;
	const struct xPinctrlMapData *pxS2v;
};

/**
 * struct xPinctrlDsInfo - Atlas7 Pad DriveStrength info
 * @ucType:               The type of this Pad.
 * @ucMask:               The mask value of this pin's pull bits.
 * @ucImval:              The immediate value of drives strength register.
 */
struct xPinctrlDsInfo {
	uint8_t ucype;
	uint8_t ucMask;
	uint8_t ucImval;
	uint8_t ucReserved;
};

/**
 * struct xPinctrlDsMaInfo - Atlas7 Pad DriveStrength & currents info
 * @uxMa:         The Drive Strength in current value .
 * @uxDs16st:    The correspond raw value of 16st pad.
 * @uxDs4we:     The correspond raw value of 4we pad.
 * @uxDs0204m31: The correspond raw value of 0204m31 pad.
 * @uxDs0610m31: The correspond raw value of 0610m31 pad.
 */
struct xPinctrlDsMaInfo {
	uint32_t uxMa;
	uint32_t uxDs16st;
	uint32_t uxDs4we;
	uint32_t uxDs0204m31;
	uint32_t uxDs0610m31;
};

uint32_t uxPinctrlPinFuncSel(uint32_t uxPinIndex, uint32_t uxFuncIndex);
uint32_t uxPinctrlPinDsSel(uint32_t uxPinIndex, uint32_t uxMa);
uint32_t uxPinctrlPinPullSel(uint32_t uxPinIndex, uint32_t uxPullIndex);
#endif /* __ATLAS7_PINCTRL_H__ */
