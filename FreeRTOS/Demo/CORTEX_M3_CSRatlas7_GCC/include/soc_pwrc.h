/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_PWRC_H__
#define __ATLAS7_PWRC_H__

#ifdef CONFIG_PWRC_IO_BRIDGE
/* If PWRC connect to IO Bridge, we have to use the offset only */
#define _PWRC_BASE 0x0
#else
/* If PWRC connect to system bus, we can use the absolute address */
#define _PWRC_BASE 0x18840000
#endif

#define A7DA_PWRC_PDN_CTRL_SET		(_PWRC_BASE + 0x3000)
#define A7DA_PWRC_PDN_CTRL_CLR		(_PWRC_BASE + 0x3004)
#define A7DA_PWRC_PON_STATUS		(_PWRC_BASE + 0x3008)
#define A7DA_PWRC_TRIGGER_EN_SET	(_PWRC_BASE + 0x300C)
#define A7DA_PWRC_TRIGGER_EN_CLR	(_PWRC_BASE + 0x3010)
#define A7DA_PWRC_INT_MASK_SET		(_PWRC_BASE + 0x3014)
#define A7DA_PWRC_INT_MASK_CLR		(_PWRC_BASE + 0x3018)
#define A7DA_PWRC_INT_STATUS		(_PWRC_BASE + 0x301C)
#define A7DA_PWRC_PIN_STATUS		(_PWRC_BASE + 0x3020)
#define A7DA_PWRC_XINW_FMODE_CTRL	(_PWRC_BASE + 0x302C)
#define A7DA_PWRC_SPARE			(_PWRC_BASE + 0x3024)
#define A7DA_PWRC_RTC_PLL_CTRL		(_PWRC_BASE + 0x3028)
#define A7DA_PWRC_DDR_LDO_CONFIG	(_PWRC_BASE + 0x3030)
#define A7DA_PWRC_GPIO3_DEBUG		(_PWRC_BASE + 0x3034)
#define A7DA_PWRC_RTC_NOC_PWRCTL_SET	(_PWRC_BASE + 0x3038)
#define A7DA_PWRC_RTC_NOC_PWRCTL_CLR	(_PWRC_BASE + 0x303C)

#ifdef __ATLAS7_STEP_B__
#define A7DA_PWRC_XTAL_REG2		(_PWRC_BASE + 0x3040)
#define A7DA_PWRC_XTAL_MUX_SEL2		(_PWRC_BASE + 0x3044)
#endif

#define A7DA_PWRC_RTC_CAN_CTRL		(_PWRC_BASE + 0x3048)
#define A7DA_PWRC_RTC_CAN_STATUS	(_PWRC_BASE + 0x304C)
#define A7DA_PWRC_FSM_M3_CTRL		(_PWRC_BASE + 0x3050)
#define A7DA_PWRC_FSM_STATE		(_PWRC_BASE + 0x3054)
#define A7DA_PWRC_RTCLDO_REG		(_PWRC_BASE + 0x3058)
#define A7DA_PWRC_GNSS_CTRL		(_PWRC_BASE + 0x305C)
#define A7DA_PWRC_GNSS_STATUS		(_PWRC_BASE + 0x3060)
#define A7DA_PWRC_XTAL_REG		(_PWRC_BASE + 0x3064)
#define A7DA_PWRC_XTAL_LDO_MUX_SEL	(_PWRC_BASE + 0x3068)
#define A7DA_PWRC_RTC_SW_RSTC_SET	(_PWRC_BASE + 0x306C)
#define A7DA_PWRC_RTC_SW_RSTC_CLR	(_PWRC_BASE + 0x3070)
#define A7DA_PWRC_POWER_SW_CTRL_SET	(_PWRC_BASE + 0x3074)
#define A7DA_PWRC_POWER_SW_CTRL_CLR	(_PWRC_BASE + 0x3078)
#define A7DA_PWRC_RTC_DCOG		(_PWRC_BASE + 0x307C)
#define A7DA_PWRC_M3_MEMORIES		(_PWRC_BASE + 0x3080)
#define A7DA_PWRC_CAN0_MEMORY		(_PWRC_BASE + 0x3084)
#define A7DA_PWRC_RTC_GNSS_MEMORY	(_PWRC_BASE + 0x3088)
#define A7DA_PWRC_M3_CLK_EN		(_PWRC_BASE + 0x308C)
#define A7DA_PWRC_CAN0_CLK_EN		(_PWRC_BASE + 0x3090)
#define A7DA_PWRC_SPI0_CLK_EN		(_PWRC_BASE + 0x3094)
#define A7DA_PWRC_RTC_SEC_CLK_EN	(_PWRC_BASE + 0x3098)

#ifdef __ATLAS7_STEP_B__
#define A7DA_PWRC_RTC_CAN_TRANS_CTRL	(_PWRC_BASE + 0x309C)
#define A7DA_PWRC_M3_WD_CNTR		(_PWRC_BASE + 0x30A0)
#define A7DA_PWRC_A7_WD_CNTR		(_PWRC_BASE + 0x30A4)
#define A7DA_PWRC_RTC_WRITE_ONE		(_PWRC_BASE + 0x30A8)
#else
#define A7DA_PWRC_RTC_NOC_CLK_EN	(_PWRC_BASE + 0x309C)
#endif

/* The bits of A7DA_PWRC_PON_STATUS */
#define PWRC_RESET_M3		BIT31
#ifdef __ATLAS7_STEP_B__
/* Boot was triggered by a7_warm_reset_b event. */
#define PWRC_PON_A7_WARM_RST_BOOT	BIT17
/* Boot was triggered by m3_wd_rst_b event. */
#define PWRC_PON_A7_WDOG_SW_BOOT	BIT16

#define PWRC_PON_WATCH_DOG_BOOT_BITS	( PWRC_PON_A7_WARM_RST_BOOT | PWRC_PON_A7_WDOG_SW_BOOT )
#else
/* Boot was triggered by a7_warm_reset_b event. */
#define PWRC_PON_A7_WARM_RST_BOOT	BIT19
/* Boot was triggered by m3_wd_rst_b event. */
#define PWRC_PON_M3_WDOG_BOOT		BIT18
/* Boot was triggered by a7_wd_hw_rst_b event.*/
#define PWRC_PON_A7_WDOG_HW_BOOT	BIT17
/* Boot was triggered by a7_wd_sw_rst_b event. */
#define PWRC_PON_A7_WDOG_SW_BOOT	BIT16

#define PWRC_PON_WATCH_DOG_BOOT_BITS	( PWRC_PON_A7_WARM_RST_BOOT | PWRC_PON_M3_WDOG_BOOT | PWRC_PON_A7_WDOG_HW_BOOT | PWRC_PON_A7_WDOG_SW_BOOT )
#endif

/* Boot was triggered by the CAN request power ON event. */
#define PWRC_PON_CAN_BOOT		BIT15
/* Boot was triggered by the GNSS request power ON event. */
#define PWRC_PON_GNSS_BOOT		BIT14
/* Boot was triggered by X_REBOOT_B event. */
#define PWRC_PON_RTC_RST_BOOT		BIT11
/* Boot was triggered by X_GPIO[3:0] event. */
#define PWRC_PON_X_GPIO_3_BOOT		BIT10
#define PWRC_PON_X_GPIO_2_BOOT		BIT9
#define PWRC_PON_X_GPIO_1_BOOT		BIT8
#define PWRC_PON_X_GPIO_0_BOOT		BIT7
/* Boot was triggered by RTC alarm1 event. */
#define PWRC_PON_RTC_ALARM1_BOOT	BIT5
/* Boot was triggered by RTC alarm0 event. */
#define PWRC_PON_RTC_ALARM0_BOOT	BIT4
/* Boot was triggered by EXT_ON event. */
#define PWRC_PON_EXT_ON_BOOT		BIT3
/* Boot was triggered by ON_KEY event. */
#define PWRC_PON_ON_KEY_BOOT		BIT2
/*
 * This bit is used to indicate whether the system RTC has been
 * reset when the system is boot from Cold Boot Mode.
 * if the system RTC is reset, the software should write this bit
 * to 1 after the system is boot.
 * Then for the next cold boot, the software can use this bit to
 * judge whether the system RTC has been reset.
 * 0 : System RTC has been reset when the cold boot event
 *     happened.
 * 1 : Since the software writes this bit to 1, the system RTC is
 *     not reset.
 *
 */
#define PWRC_PON_SYSRTC_BEEN_RST	BIT1
/*
 * Warm boot indication.
 * 0 : Power-up from cold boot mode
 * 1 : Wakeup from power saving mode
 */
#define PWRC_PON_WARM_BOOT		BIT0


/* The bits of A7DA_PWRC_RTC_NOC_PWRCTL_SET/CLR */
#define PWRC_NOC_REGI_IDLE_REQ		0x0001
#define PWRC_NOC_REGI_IDLE_ACK		0x0002
#define PWRC_NOC_REGI_IDLE		0x0004
#define PWRC_NOC_REGT_IDLE_REQ		0x0008
#define PWRC_NOC_REGT_IDLE_ACK		0x0010
#define PWRC_NOC_REGT_IDLE		0x0020
#define PWRC_NOC_DRAMT_IDLE_REQ		0x0040
#define PWRC_NOC_DRAMT_IDLE_ACK		0x0080
#define PWRC_NOC_DRAMT_IDLE		0x0100
#define PWRC_NOC_SECURITY_IDLE_REQ	0x0200
#define PWRC_NOC_SECURITY_IDLE_ACK	0x0400
#define PWRC_NOC_SECURITY_IDLE		0x0800
#define PWRC_NOC_SPI0_IDLE_REQ		0x1000
#define PWRC_NOC_SPI0_IDLE_ACK		0x2000
#define PWRC_NOC_SPI0_IDLE		0x4000

/* The values of A7DA_PWRC_FSM_STATE */
#define PWRC_FSM_COLDBOOT_MODE		0x0
#define PWRC_FSM_BEGIN_POWER_ON		0x1
#define PWRC_FSM_MEM_ON			0x2
#define PWRC_FSM_IO_ON			0x3
#define PWRC_FSM_ANALOG_ON		0x4
#define PWRC_FSM_CORE_ON		0x5
#define PWRC_FSM_BOOT_RET_OFF		0x6
#define PWRC_FSM_BOOT_RESET_OFF		0x7
#define PWRC_FSM_NORMAL_MODE		0x8
#define PWRC_FSM_PREPARE_RET		0x9
#define PWRC_FSM_BEGIN_RET		0xA
#define PWRC_FSM_RET_DONE		0xB
#define PWRC_FSM_BEGIN_POWER_OFF	0xC
#define PWRC_FSM_ONE_OFF		0xD
#define PWRC_FSM_ANALOG_OFF		0xE
#define PWRC_FSM_IO_OFF			0xF
#define PWRC_FSM_MEM_OFF		0x10
#define PWRC_FSM_CORE_OFF		0x11
#define PWRC_FSM_BOOT_RESET_ON		0x12
#define PWRC_FSM_PSAVING_MODE		0x13
#define PWRC_FSM_PSAVING_MODE_M3	0x14
#define PWRC_FSM_M3_RESET_OFF		0x15
#define PWRC_FSM_M3_RESET_ON		0x16
#define PWRC_FSM_CAN_ACTIVE		0x17
#define PWRC_FSM_M3_ON			0x18
#define PWRC_FSM_M3_OFF			0x19
#define PWRC_FSM_START			0x1A

/* The values of A7DA_PWRC_RTC_CAN_CTRL */
#define PWRC_CAN_LIST0_EN		0x1
#define PWRC_CAN_LIST1_EN		(0x1<<1)
#define PWRC_CAN_LIST0_SEL(num)		(((num)&0x7)<<2)
#define PWRC_CAN_LIST1_SEL(num)		(((num)&0x7)<<5)
#define PWRC_CAN_LIST0_DELAY(val)	(((val)&0x7f)<<8)
#define PWRC_CAN_LIST1_DELAY(val)	(((val)&0x7f)<<15)
#define PWRC_CAN_PRE_WAKEUP_PARTIAL	(0<<22)
#define PWRC_CAN_PRE_WAKEUP_CAN0	(0x1<<22)
#define PWRC_CAN_PRE_WAKEUP_CAN1	(0x2<<22)
#define PWRC_CAN_PRE_WAKEUP_TRANSCEIVER	(0x4<<22)
#define PWRC_CAN_EXT_IRQ_CONNECT_CAN0	(0<<25)
#define PWRC_CAN_EXT_IRQ_CONNECT_CAN1	(0x1<<25)
#define PWRC_CAN_PRE_WAKEUP_MODE_CAN0	(0<<26)
#define PWRC_CAN_PRE_WAKEUP_MODE_M3PD	(0x1<<26)
#define PWRC_CAN_WAKEUP_EN		(0x1<<27)
#define PWRC_CAN_CAN0_SEL_TRANSCEIVER_0	(0<<28)
#define PWRC_CAN_CAN0_SEL_TRANSCEIVER_1	(0x1<<28)
#define PWRC_CAN_CAN0_SEL_TRANSCEIVER_AUTO	(0x2<<28)
#define PWRC_CAN_CAN0_SEL_AUTO		(0x2<<28)

/* The bits of A7DA_PWRC_RTC_SW_RSTC_SET and A7DA_PWRC_RTC_SW_RSTC_CLR */
#define PWRC_RTC_SW_RESTC_CORE_RESET	BIT0
#define PWRC_RTC_SW_RESTC_A7_WARM_RESET	BIT1
#define PWRC_RTC_SW_RESTC_MIX_RESET	BIT2
#define PWRC_RTC_SW_RESTC_SYSRTC_RESET	BIT3
#define PWRC_RTC_SW_RESTC_M3_POR_RESET	BIT4
#define PWRC_RTC_SW_RESTC_M3_SYS_RESET	BIT5
#define PWRC_RTC_SW_RESTC_M3_DAP_RESET	BIT6
#define PWRC_RTC_SW_RESTC_M3_CTI_RESET	BIT7
#define PWRC_RTC_SW_RESTC_CAN0_RESET	BIT8
#define PWRC_RTC_SW_RESTC_SPI0_RESET	BIT9
#define PWRC_RTC_SW_RESTC_RTC_SEURITY_RESET	BIT10
#define PWRC_RTC_SW_RESTC_IO_RTC_RESET	BIT11
#define PWRC_RTC_SW_RESTC_GPIO_RTC_RESET	BIT12
#define PWRC_RTC_SW_RESTC_RTC_NOC_IOB_RESET	BIT13
#define PWRC_RTC_SW_RESTC_CSSI_RST_RTCM	BIT14
#define PWRC_RTC_SW_RESTC_CAN_LIST0_RESET	BIT15
#define PWRC_RTC_SW_RESTC_CAN_LIST1_RESET	BIT16
#define PWRC_RTC_SW_RESTC_REBOOT	BIT17
#define PWRC_RTC_SW_RESTC_RTC_RST	BIT18
#define PWRC_RTC_SW_RESTC_RESET		BIT19
#define PWRC_RTC_SW_RESTC_M3_TIMER_RESET	BIT20
#define PWRC_RTC_SW_RESTC_M3_WDOG_RESET	BIT21

#define CPU_REG_BASE            0x10230000
#define CPU_REG(offset)			(CPU_REG_BASE + (offset))

#define CPU_BOOT_CFG            CPU_REG(0x001C)
#define M3_BOOT_TYPE_INDEP		0x10
#define M3_SRAM_BASE			0x17E00000
#define M3_ROM_BASE			0x17E20000

#define PWRC_FAST_DOMAIN_BASE           0x188D0000

#define PWRC_REG_F(offset)              (PWRC_FAST_DOMAIN_BASE + (offset))
#define PWRC_SCRATCH_PAD1_F             PWRC_REG_F(0x0000)
#define PWRC_SCRATCH_PAD2_F				PWRC_REG_F(0x0004)
#define PWRC_SCRATCH_PAD3_F				PWRC_REG_F(0x0008)
#define PWRC_SCRATCH_PAD4_F				PWRC_REG_F(0x000C)
#define PWRC_SCRATCH_PAD5_F				PWRC_REG_F(0x0010)
#define PWRC_SCRATCH_PAD6_F				PWRC_REG_F(0x0014)
#define PWRC_SCRATCH_PAD7_F				PWRC_REG_F(0x0018)
#define PWRC_SCRATCH_PAD8_F				PWRC_REG_F(0x001C)
#define PWRC_SCRATCH_PAD9_F				PWRC_REG_F(0x0020)
#define PWRC_SCRATCH_PAD10_F			PWRC_REG_F(0x0024)
#define PWRC_SCRATCH_PAD11_F			PWRC_REG_F(0x0028)
#define PWRC_SCRATCH_PAD12_F			PWRC_REG_F(0x002C)
#define PWRC_RTC_BOOTSTRAP0_F			PWRC_REG_F(0x0030)
#define PWRC_RTC_BOOTSTRAP1_F			PWRC_REG_F(0x0034)
#define PWRC_M3_HASH_0_F				PWRC_REG_F(0x0038)
#define PWRC_M3_HASH_1_F				PWRC_REG_F(0x003C)
#define PWRC_M3_HASH_2_F				PWRC_REG_F(0x0040)
#define PWRC_M3_HASH_3_F				PWRC_REG_F(0x0044)
#define PWRC_M3_HASH_4_F				PWRC_REG_F(0x0048)
#define PWRC_M3_HASH_5_F				PWRC_REG_F(0x004C)
#define PWRC_M3_HASH_6_F				PWRC_REG_F(0x0050)
#define PWRC_M3_HASH_7_F				PWRC_REG_F(0x0054)
#define PWRC_QSPI_PARAMS_F				PWRC_REG_F(0x0058)
#define PWRC_CSSI_LPI_CTRL_F			PWRC_REG_F(0x005C)
#define PWRC_BCRL_VAL_F					PWRC_REG_F(0x0060)
#define PWRC_RTCM_AMP_ADJUST_F			PWRC_REG_F(0x0064)
#define PWRC_KAS_SW_CONFIG_F			PWRC_REG_F(0x0068)
#define PWRC_RTC_GNSS_SD_F				PWRC_REG_F(0x006C)
#define PWRC_RTC_W1_DEBUG_F				PWRC_REG_F(0x0070)
#define PWRC_RTC_IOB_CTRL_F				PWRC_REG_F(0x0074)
#define PWRC_SPINLOCK_RD_DEBUG			PWRC_REG_F(0x0078)
#define PWRC_TESTANDSET_0_F				PWRC_REG_F(0x007C)
#define PWRC_WATCHDOG_CONFIG_F			PWRC_REG_F(0x0080)

#endif /* __ATLAS7_PWRC_H__ */
