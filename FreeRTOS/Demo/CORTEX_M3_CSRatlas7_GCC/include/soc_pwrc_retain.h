/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_PWRC_RETAIN_H__
#define __ATLAS7_PWRC_RETAIN_H__

#define _RETAIN_REG_BASE 0x188D0000

#define A7DA_PWRC_SCRATCH_PAD1		(_RETAIN_REG_BASE + 0x0000)
#define A7DA_PWRC_SCRATCH_PAD2		(_RETAIN_REG_BASE + 0x0004)
#define A7DA_PWRC_SCRATCH_PAD3		(_RETAIN_REG_BASE + 0x0008)
#define A7DA_PWRC_SCRATCH_PAD4		(_RETAIN_REG_BASE + 0x000C)
#define A7DA_PWRC_SCRATCH_PAD5		(_RETAIN_REG_BASE + 0x0010)
#define A7DA_PWRC_SCRATCH_PAD6		(_RETAIN_REG_BASE + 0x0014)
#define A7DA_PWRC_SCRATCH_PAD7		(_RETAIN_REG_BASE + 0x0018)
#define A7DA_PWRC_SCRATCH_PAD8		(_RETAIN_REG_BASE + 0x001C)
#define A7DA_PWRC_SCRATCH_PAD9		(_RETAIN_REG_BASE + 0x0020)
#define A7DA_PWRC_SCRATCH_PAD10		(_RETAIN_REG_BASE + 0x0024)
#define A7DA_PWRC_SCRATCH_PAD11		(_RETAIN_REG_BASE + 0x0028)
#define A7DA_PWRC_SCRATCH_PAD12		(_RETAIN_REG_BASE + 0x002C)
#define A7DA_PWRC_RTC_BOOTSTRAP0	(_RETAIN_REG_BASE + 0x0030)	/* rARMM3_RTC_BOOTSTRAP0 */
#define A7DA_PWRC_RTC_BOOTSTRAP1	(_RETAIN_REG_BASE + 0x0034)
#define A7DA_PWRC_M3_HASH_0		(_RETAIN_REG_BASE + 0x0038)	/* ARMM3_PWRC_HASH_BASE */
#define A7DA_PWRC_M3_HASH_1		(_RETAIN_REG_BASE + 0x003C)
#define A7DA_PWRC_M3_HASH_2		(_RETAIN_REG_BASE + 0x0040)
#define A7DA_PWRC_M3_HASH_3		(_RETAIN_REG_BASE + 0x0044)
#define A7DA_PWRC_M3_HASH_4		(_RETAIN_REG_BASE + 0x0048)
#define A7DA_PWRC_M3_HASH_5		(_RETAIN_REG_BASE + 0x004C)
#define A7DA_PWRC_M3_HASH_6		(_RETAIN_REG_BASE + 0x0050)
#define A7DA_PWRC_M3_HASH_7		(_RETAIN_REG_BASE + 0x0054)
#define A7DA_PWRC_QSPI_PARAMS		(_RETAIN_REG_BASE + 0x0058)
#define A7DA_PWRC_CSSI_LPI_CTRL		(_RETAIN_REG_BASE + 0x005C)
#define A7DA_PWRC_BCRL_VAL		(_RETAIN_REG_BASE + 0x0060)
#define A7DA_PWRC_RTCM_AMP_ADJUST	(_RETAIN_REG_BASE + 0x0064)
#define A7DA_PWRC_KAS_SW_CONFIG		(_RETAIN_REG_BASE + 0x0068)
#define A7DA_PWRC_RTC_GNSS_SD		(_RETAIN_REG_BASE + 0x006C)
#ifdef __ATLAS7_STEP_B__
#define A7DA_PWRC_RTC_IOB_CTRL		(_RETAIN_REG_BASE + 0x0070)
#define A7DA_PWRC_SPINLOCK_RD_DEBUG	(_RETAIN_REG_BASE + 0x0074)
#define A7DA_PWRC_TESTANDSET_0		(_RETAIN_REG_BASE + 0x0078)
#define A7DA_PWRC_TESTANDSET_1		(_RETAIN_REG_BASE + 0x007C)
#define A7DA_PWRC_TESTANDSET_2		(_RETAIN_REG_BASE + 0x0080)
#define A7DA_PWRC_TESTANDSET_3		(_RETAIN_REG_BASE + 0x0084)
#define A7DA_PWRC_TESTANDSET_4		(_RETAIN_REG_BASE + 0x0088)
#define A7DA_PWRC_TESTANDSET_5		(_RETAIN_REG_BASE + 0x008C)
#define A7DA_PWRC_TESTANDSET_6		(_RETAIN_REG_BASE + 0x0090)
#define A7DA_PWRC_TESTANDSET_7		(_RETAIN_REG_BASE + 0x0094)
#define A7DA_PWRC_MODE_TYPE		(_RETAIN_REG_BASE + 0x0098)
#define A7DA_PWRC_UUID_VAL0		(_RETAIN_REG_BASE + 0x009C)
#define A7DA_PWRC_UUID_VAL1		(_RETAIN_REG_BASE + 0x00A0)
#define A7DA_PWRC_UUID_VAL2		(_RETAIN_REG_BASE + 0x00A4)
#define A7DA_PWRC_UUID_VAL3		(_RETAIN_REG_BASE + 0x00A8)
#define A7DA_PWRC_HBK0			(_RETAIN_REG_BASE + 0x00AC)
#define A7DA_PWRC_HBK1			(_RETAIN_REG_BASE + 0x00B0)
#define A7DA_PWRC_HBK2			(_RETAIN_REG_BASE + 0x00B4)
#define A7DA_PWRC_HBK3			(_RETAIN_REG_BASE + 0x00B8)
#define A7DA_PWRC_M3_CPU_RESET		(_RETAIN_REG_BASE + 0x00BC)
#define A7DA_PWRC_SCRATCH_PAD1_W1	(_RETAIN_REG_BASE + 0x00C0)
#define A7DA_PWRC_SCRATCH_PAD2_W1	(_RETAIN_REG_BASE + 0x00C4)
#define A7DA_PWRC_SCRATCH_PAD3_W1	(_RETAIN_REG_BASE + 0x00C8)
#define A7DA_PWRC_SCRATCH_PAD4_W1	(_RETAIN_REG_BASE + 0x00CC)
#define A7DA_PWRC_SCRATCH_PAD5_W1	(_RETAIN_REG_BASE + 0x00D0)
#define A7DA_PWRC_M3_STATUS		(_RETAIN_REG_BASE + 0x00D4)
#define A7DA_PWRC_RTC_W1_DEBUG		(_RETAIN_REG_BASE + 0x00D8)
#else
#define A7DA_PWRC_RTC_W1_DEBUG		(_RETAIN_REG_BASE + 0x0070)
#define A7DA_PWRC_RTC_IOB_CTRL		(_RETAIN_REG_BASE + 0x0074)
#endif


/* Alias for PWRC registers */
/* rARMM3_WAKEUP_ADDR */
#define PWRC_M3_WAKEUP_ADDR		A7DA_PWRC_SCRATCH_PAD2
/* rARMM3_POWER_STATUS_SW */
#define PWRC_POWER_STATUS_SW		A7DA_PWRC_SCRATCH_PAD3

/* Reset Latch */
#ifdef __ATLAS7_STEP_B__
#define PWRC_RESET_LATCH_REG		A7DA_PWRC_BCRL_VAL
#else
#define PWRC_RESET_LATCH_REG		A7DA_PWRC_SCRATCH_PAD4
#endif

/* rARMM3_RTC_SP10 */
#define PWRC_M3_RTC_SP10		A7DA_PWRC_SCRATCH_PAD10
/* rARMM3_RTC_BOOTSTRAP0 */
#define PWRC_M3_RTC_BOOTSTRAP0		A7DA_PWRC_RTC_BOOTSTRAP0
/* A7 to M3 PM IPC operation Code register */
#define PWRC_PM_IPC_OPCODE		A7DA_PWRC_SCRATCH_PAD8

#ifdef __ATLAS7_STEP_B__
#define REQUEST_HWLOCK(x)	while( !SOC_REG( A7DA_PWRC_TESTANDSET_##x ) )
#define RELEASE_HWLOCK(x)	SOC_REG( A7DA_PWRC_TESTANDSET_##x ) = 0
#else
#define REQUEST_HWLOCK(x)	do {} while( 0 )
#define RELEASE_HWLOCK(x)	do {} while( 0 )
#endif

#define PWRC_IS_CLOCK_LATE_INITIALIZED()  		( 0 != (SOC_REG(A7DA_PWRC_SCRATCH_PAD11)&(BIT29)) )
#define PWRC_SET_CLOCK_LATE_INITIALIZED() 		{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) |= BIT29; }
#define PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG() { SOC_REG(A7DA_PWRC_SCRATCH_PAD11) &= ~(BIT29); }

#define PWRC_IS_CLOCK_INITIALIZED()  		( 0 != (SOC_REG(A7DA_PWRC_SCRATCH_PAD11)&(BIT30)) )
#define PWRC_SET_CLOCK_INITIALIZED() 		{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) |= BIT30; }
#define PWRC_CLEAR_CLOCK_INITIALIZED_FLAG() { SOC_REG(A7DA_PWRC_SCRATCH_PAD11) &= ~(BIT30); }

#define PWRC_IS_DDR_INITIALIZED()  			( 0 != (SOC_REG(A7DA_PWRC_SCRATCH_PAD11)&(BIT31)) )
#define PWRC_SET_DDR_INITIALIZED() 			{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) |= BIT31; }
#define PWRC_CLEAR_DDR_INITIALIZED_FLAG() 	{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) &= ~(BIT31);}

#define PWRC_SET_FREERTOS_VERSION(vv) 		{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) |= (((vv)&0xFF)<<21); }
#define PWRC_GET_FREERTOS_VERSION() 		( (SOC_REG(A7DA_PWRC_SCRATCH_PAD11)>>21)&0xFF )
#define PWRC_CLEAR_FREERTOS_VERSION() 		{ SOC_REG(A7DA_PWRC_SCRATCH_PAD11) &= (~(0xFF<<21)); }

#endif /* __ATLAS7_PWRC_RETAIN_H__ */
