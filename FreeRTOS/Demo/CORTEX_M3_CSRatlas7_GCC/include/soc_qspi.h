/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_QSPI_H__
#define __ATLAS7_QSPI_H__

#define _QSPI_REG_BASE 0x188B0000

/* QSPI MEM CTRL Registers */
/* Control Register */
#define A7DA_QSPI_CTRL		(_QSPI_REG_BASE + 0x0000)
/* Status Register */
#define A7DA_QSPI_STAT		(_QSPI_REG_BASE + 0x0004)
/* Access Request Register 0 */
#define A7DA_QSPI_ACCRR0	(_QSPI_REG_BASE + 0x0008)
/* Access Request Register 1 */
#define A7DA_QSPI_ACCRR1	(_QSPI_REG_BASE + 0x000C)
/* Access Request Register 2 */
#define A7DA_QSPI_ACCRR2	(_QSPI_REG_BASE + 0x0010)
/* Duration Deep Power-Down Mode */
#define A7DA_QSPI_DDPM		(_QSPI_REG_BASE + 0x0014)
/* Read/Write Data Register */
#define A7DA_QSPI_RWDATA	(_QSPI_REG_BASE + 0x0018)
/* FIFOs Status Registe */
#define A7DA_QSPI_FFSTAT	(_QSPI_REG_BASE + 0x001C)
/* Default Attributes of the Serial Flash Device */
#define A7DA_QSPI_DEFMEM	(_QSPI_REG_BASE + 0x0020)
/* Extended Addressing Mode */
#define A7DA_QSPI_EXADDR	(_QSPI_REG_BASE + 0x0024)
/* Memory Specification Register */
#define A7DA_QSPI_MEMSPEC	(_QSPI_REG_BASE + 0x0028)
/* Interrupt Mask Register */
#define A7DA_QSPI_INTMSK	(_QSPI_REG_BASE + 0x002C)
/* Interrupt Request Register */
#define A7DA_QSPI_INTREQ	(_QSPI_REG_BASE + 0x0030)
/* Custom Instruction Setup Register */
#define A7DA_QSPI_CICFG		(_QSPI_REG_BASE + 0x0034)
/* Custom Instruction Data Register 0 */
#define A7DA_QSPI_CIDR0		(_QSPI_REG_BASE + 0x0038)
/* Custom Instruction Data Register */
#define A7DA_QSPI_CIDR1		(_QSPI_REG_BASE + 0x003C)
/* Read Dummy Cycles Register */
#define A7DA_QSPI_RDC		(_QSPI_REG_BASE + 0x0040)

/* QSPI Configuration RAM */
#define A7DA_QSPI_CFGRAM_BASE	(_QSPI_REG_BASE + 0x0100)
#define A7DA_QSPI_CFGRAM_SIZE	0x0100

/* QSPI CORE Registers */
/* FCSPI DMA System Memory Source/Destination address */
#define A7DA_QSPI_DMA_SADDR	(_QSPI_REG_BASE + 0x0800)
/* FCSPI DMA FLASH Source/Destination address */
#define A7DA_QSPI_DMA_FADDR	(_QSPI_REG_BASE + 0x0804)
/* FCSPI DMA Block Length */
#define A7DA_QSPI_DMA_LEN	(_QSPI_REG_BASE + 0x0808)
/* FCSPI DMA Control/Status Register */
#define A7DA_QSPI_DMA_CST	(_QSPI_REG_BASE + 0x080C)
/* FCSPI Debug Register */
#define A7DA_QSPI_DEBUG		(_QSPI_REG_BASE + 0x0810)
/* FCSPI xotf_en Register */
#define A7DA_QSPI_XOTF_EN	(_QSPI_REG_BASE + 0x0814)
/* FCSPI xotf_base Register */
#define A7DA_QSPI_XOTF_BASE	(_QSPI_REG_BASE + 0x0818)
/* FCSPI delay_line Register */
#define A7DA_QSPI_DELAY_LINE	(_QSPI_REG_BASE + 0x081C)
/* FCSPI addr_extmode Register */
#define A7DA_QSPI_ADDR_EXTMODE	(_QSPI_REG_BASE + 0x0820)


#endif /* __ATLAS7_QSPI_H__ */
