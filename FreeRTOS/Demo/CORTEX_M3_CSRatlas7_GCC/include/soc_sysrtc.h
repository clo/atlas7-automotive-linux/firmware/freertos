/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_SYSRTC_H__
#define __ATLAS7_SYSRTC_H__

#ifdef CONFIG_SYSRTC_IO_BRIDGE
/* If SYSRTC connect to IO Bridge, we have to use the offset only */
#define _SYSRTC_REG_BASE 0x0
#else
/* If SYSRTC connect to system bus, we can use the absolute address */
#define _SYSRTC_REG_BASE 0x18840000
#endif


#define A7DA_SYSRTC_COUNTER		(_SYSRTC_REG_BASE + 0x2000)
#define A7DA_SYSRTC_ALARM0		(_SYSRTC_REG_BASE + 0x2004)
#define A7DA_SYSRTC_STATUS		(_SYSRTC_REG_BASE + 0x2008)
#define A7DA_SYSRTC_DIV			(_SYSRTC_REG_BASE + 0x200C)
#define A7DA_SYSRTC_ALARM1		(_SYSRTC_REG_BASE + 0x2018)
#define A7DA_SYSRTC_CLK_SWITCH		(_SYSRTC_REG_BASE + 0x201C)

void atlas7_m3_rtc_config(void);

#endif /* __ATLAS7_SYSRTC_H__ */
