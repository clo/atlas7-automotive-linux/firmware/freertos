/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_TIMER_H__
#define __ATLAS7_TIMER_H__

#include <io.h>

#define _TIMER_REG_BASE 0x10DC0000

/* Registers of TIMER */
#define A7DA_TIMER_32COUNTER_0_CTRL		(_TIMER_REG_BASE + 0x0000)
#define A7DA_TIMER_32COUNTER_1_CTRL		(_TIMER_REG_BASE + 0x0004)
#define A7DA_TIMER_32COUNTER_2_CTRL		(_TIMER_REG_BASE + 0x0008)
#define A7DA_TIMER_32COUNTER_3_CTRL		(_TIMER_REG_BASE + 0x000C)
#define A7DA_TIMER_32COUNTER_4_CTRL		(_TIMER_REG_BASE + 0x0010)
#define A7DA_TIMER_32COUNTER_5_CTRL		(_TIMER_REG_BASE + 0x0014)
#define A7DA_TIMER_MATCH_0			(_TIMER_REG_BASE + 0x0018)
#define A7DA_TIMER_MATCH_1			(_TIMER_REG_BASE + 0x001C)
#define A7DA_TIMER_MATCH_2			(_TIMER_REG_BASE + 0x0020)
#define A7DA_TIMER_MATCH_3			(_TIMER_REG_BASE + 0x0024)
#define A7DA_TIMER_MATCH_4			(_TIMER_REG_BASE + 0x0028)
#define A7DA_TIMER_MATCH_5			(_TIMER_REG_BASE + 0x002C)
#define A7DA_TIMER_32COUNTER_0_MATCH_NUM	(_TIMER_REG_BASE + 0x0030)
#define A7DA_TIMER_32COUNTER_1_MATCH_NUM	(_TIMER_REG_BASE + 0x0034)
#define A7DA_TIMER_32COUNTER_2_MATCH_NUM	(_TIMER_REG_BASE + 0x0038)
#define A7DA_TIMER_32COUNTER_3_MATCH_NUM	(_TIMER_REG_BASE + 0x003C)
#define A7DA_TIMER_32COUNTER_4_MATCH_NUM	(_TIMER_REG_BASE + 0x0040)
#define A7DA_TIMER_32COUNTER_5_MATCH_NUM	(_TIMER_REG_BASE + 0x0044)
#define A7DA_TIMER_COUNTER_0			(_TIMER_REG_BASE + 0x0048)
#define A7DA_TIMER_COUNTER_1			(_TIMER_REG_BASE + 0x004C)
#define A7DA_TIMER_COUNTER_2			(_TIMER_REG_BASE + 0x0050)
#define A7DA_TIMER_COUNTER_3			(_TIMER_REG_BASE + 0x0054)
#define A7DA_TIMER_COUNTER_4			(_TIMER_REG_BASE + 0x0058)
#define A7DA_TIMER_COUNTER_5			(_TIMER_REG_BASE + 0x005C)
#define A7DA_TIMER_INTR_STATUS			(_TIMER_REG_BASE + 0x0060)
#define A7DA_TIMER_WATCHDOG_EN			(_TIMER_REG_BASE + 0x0064)
#define A7DA_TIMER_64COUNTER_CTRL		(_TIMER_REG_BASE + 0x0068)
#define A7DA_TIMER_64COUNTER_LO			(_TIMER_REG_BASE + 0x006C)
#define A7DA_TIMER_64COUNTER_HI			(_TIMER_REG_BASE + 0x0070)
#define A7DA_TIMER_64COUNTER_LOAD_LO		(_TIMER_REG_BASE + 0x0074)
#define A7DA_TIMER_64COUNTER_LOAD_HI		(_TIMER_REG_BASE + 0x0078)
#define A7DA_TIMER_64COUNTER_RLATCHED_LO	(_TIMER_REG_BASE + 0x007C)
#define A7DA_TIMER_64COUNTER_RLATCHED_HI	(_TIMER_REG_BASE + 0x0080)

/* Registers in STEP-B */
#define A7DA_TIMER_COUNTER_WD		        (_TIMER_REG_BASE + 0x0084)
#define A7DA_TIMER_32COUNTER_WD_CTRL		(_TIMER_REG_BASE + 0x008C)
#define A7DA_TIMER_MATCH_WD		            (_TIMER_REG_BASE + 0x0090)
#define A7DA_TIMER_WATCHDOG_STATUS          (_TIMER_REG_BASE + 0x0094)

/* Registers of TIMERB */
#define A7DA_TIMERB_32COUNTER_0_CTRL		(_TIMER_REG_BASE + 0x8000)
#define A7DA_TIMERB_32COUNTER_1_CTRL		(_TIMER_REG_BASE + 0x8004)
#define A7DA_TIMERB_32COUNTER_2_CTRL		(_TIMER_REG_BASE + 0x8008)
#define A7DA_TIMERB_32COUNTER_3_CTRL		(_TIMER_REG_BASE + 0x800C)
#define A7DA_TIMERB_32COUNTER_4_CTRL		(_TIMER_REG_BASE + 0x8010)
#define A7DA_TIMERB_32COUNTER_5_CTRL		(_TIMER_REG_BASE + 0x8014)
#define A7DA_TIMERB_MATCH_0			(_TIMER_REG_BASE + 0x8018)
#define A7DA_TIMERB_MATCH_1			(_TIMER_REG_BASE + 0x801C)
#define A7DA_TIMERB_MATCH_2			(_TIMER_REG_BASE + 0x8020)
#define A7DA_TIMERB_MATCH_3			(_TIMER_REG_BASE + 0x8024)
#define A7DA_TIMERB_MATCH_4			(_TIMER_REG_BASE + 0x8028)
#define A7DA_TIMERB_MATCH_5			(_TIMER_REG_BASE + 0x802C)
#define A7DA_TIMERB_32COUNTER_0_MATCH_NUM	(_TIMER_REG_BASE + 0x8030)
#define A7DA_TIMERB_32COUNTER_1_MATCH_NUM	(_TIMER_REG_BASE + 0x8034)
#define A7DA_TIMERB_32COUNTER_2_MATCH_NUM	(_TIMER_REG_BASE + 0x8038)
#define A7DA_TIMERB_32COUNTER_3_MATCH_NUM	(_TIMER_REG_BASE + 0x803C)
#define A7DA_TIMERB_32COUNTER_4_MATCH_NUM	(_TIMER_REG_BASE + 0x8040)
#define A7DA_TIMERB_32COUNTER_5_MATCH_NUM	(_TIMER_REG_BASE + 0x8044)
#define A7DA_TIMERB_COUNTER_0			(_TIMER_REG_BASE + 0x8048)
#define A7DA_TIMERB_COUNTER_1			(_TIMER_REG_BASE + 0x804C)
#define A7DA_TIMERB_COUNTER_2			(_TIMER_REG_BASE + 0x8050)
#define A7DA_TIMERB_COUNTER_3			(_TIMER_REG_BASE + 0x8054)
#define A7DA_TIMERB_COUNTER_4			(_TIMER_REG_BASE + 0x8058)
#define A7DA_TIMERB_COUNTER_5			(_TIMER_REG_BASE + 0x805C)
#define A7DA_TIMERB_INTR_STATUS			(_TIMER_REG_BASE + 0x8060)
#define A7DA_TIMERB_WATCHDOG_EN			(_TIMER_REG_BASE + 0x8064)
#define A7DA_TIMERB_64COUNTER_CTRL		(_TIMER_REG_BASE + 0x8068)
#define A7DA_TIMERB_64COUNTER_LO		(_TIMER_REG_BASE + 0x806C)
#define A7DA_TIMERB_64COUNTER_HI		(_TIMER_REG_BASE + 0x8070)
#define A7DA_TIMERB_64COUNTER_LOAD_LO		(_TIMER_REG_BASE + 0x8074)
#define A7DA_TIMERB_64COUNTER_LOAD_HI		(_TIMER_REG_BASE + 0x8078)
#define A7DA_TIMERB_64COUNTER_RLATCHED_LO	(_TIMER_REG_BASE + 0x807C)
#define A7DA_TIMERB_64COUNTER_RLATCHED_HI	(_TIMER_REG_BASE + 0x8080)

/* Registers in STEP-B */
#define A7DA_TIMERB_COUNTER_WD               (_TIMER_REG_BASE + 0x8084)
#define A7DA_TIMERB_32COUNTER_WD_CTRL        (_TIMER_REG_BASE + 0x808C)
#define A7DA_TIMERB_MATCH_WD                 (_TIMER_REG_BASE + 0x8090)
#define A7DA_TIMERB_WATCHDOG_STATUS          (_TIMER_REG_BASE + 0x8094)

#define TIMER_FREQUENCY		(1 * 1000 * 1000)
#define TIMER_MATCH_COUNT	(0xffffffff)
#define TIMER_SAFE_LIMIT	(0xF0000000)
#define TIMER_32_EN	0x1
#define TIMER_32_LOOP	0x4
#define TIMER_64_LATCH	0x1
#define TIMER_64_LOAD	0x2

#define A7DA_M3_TIMER_32COUNTER_0_CTRL		0x18850004
#define A7DA_M3_TIMER_32COUNTER_1_CTRL		0x18850008
#define A7DA_M3_TIMER_32COUNTER_2_CTRL		0x1885000C
#define A7DA_M3_TIMER_32COUNTER_WD_CTRL		0x18850010
#define A7DA_M3_TIMER_MATCH_0			0x18850014
#define A7DA_M3_TIMER_MATCH_1			0x18850018
#define A7DA_M3_TIMER_MATCH_2			0x1885001C
#define A7DA_M3_TIMER_MATCH_WD			0x18850020
#define A7DA_M3_TIMER_32COUNTER_0_MATCH_NUM	0x18850024
#define A7DA_M3_TIMER_32COUNTER_1_MATCH_NUM	0x18850028
#define A7DA_M3_TIMER_32COUNTER_2_MATCH_NUM	0x1885002C
#define A7DA_M3_TIMER_COUNTER_0			0x18850030
#define A7DA_M3_TIMER_COUNTER_1			0x18850034
#define A7DA_M3_TIMER_COUNTER_2			0x18850038
#define A7DA_M3_TIMER_COUNTER_WD		0x1885003C
#define A7DA_M3_TIMER_WATCHDOG_STATUS		0x18850040
#define A7DA_M3_TIMER_WATCHDOG_EN		0x18850044
#define A7DA_M3_TIMER_INTR_STATUS		0x18850048

static inline void vOsTimerInit( uint32_t uxSysClk, uint32_t uxVal )
{
	/* Set DIV */
#ifdef __ATLAS7_STEP_B__
	SOC_REG(A7DA_M3_TIMER_32COUNTER_0_CTRL) = (uxSysClk / TIMER_FREQUENCY - 1) << 16;
	SOC_REG(A7DA_M3_TIMER_COUNTER_0) = uxVal;
	SOC_REG(A7DA_M3_TIMER_MATCH_0) = TIMER_MATCH_COUNT;
	SOC_REG(A7DA_M3_TIMER_32COUNTER_0_CTRL) |= (TIMER_32_EN | TIMER_32_LOOP);
#else
	SOC_REG(A7DA_TIMER_64COUNTER_CTRL) = (uxSysClk / TIMER_FREQUENCY - 1) << 16;
	SOC_REG(A7DA_TIMER_64COUNTER_LOAD_LO) = uxVal;
	SOC_REG(A7DA_TIMER_64COUNTER_LOAD_HI) = 0;
	SOC_REG(A7DA_TIMER_64COUNTER_CTRL) |= TIMER_64_LOAD;

#endif
}

static inline __on_retain uint32_t uxOsGetCurrentTick( void )
{
#ifdef __ATLAS7_STEP_B__
	return SOC_REG(A7DA_M3_TIMER_COUNTER_0);
#else
	SOC_REG(A7DA_TIMER_64COUNTER_CTRL) |= TIMER_64_LATCH;   //Latch

	return SOC_REG(A7DA_TIMER_64COUNTER_RLATCHED_LO);
#endif
}

void vUsWait( uint32_t uxUs );
/* microsecond  */
void vSetTimeout( uint32_t uxUs );
/* microsecond  */
uint32_t uxGetSpendTime( void );
uint32_t uxIsTimeout( void );

#endif /* __ATLAS7_TIMER_H__ */
