/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __TSADC_H__
#define __TSADC_H__

#define _TSADC_REG_BASE 0x10d80000

/* Registers of TSADC */
#define A7DA_TSADC_CTRL1		(_TSADC_REG_BASE + 0x0000)
#define A7DA_TSADC_CTRL2		(_TSADC_REG_BASE + 0x0004)
#define A7DA_TSADC_INTR_STATUS		(_TSADC_REG_BASE + 0x0010)
#define A7DA_TSADC_AUX1			(_TSADC_REG_BASE + 0x001c)
#define A7DA_TSADC_AUX2			(_TSADC_REG_BASE + 0x0020)
#define A7DA_TSADC_AUX3			(_TSADC_REG_BASE + 0x0024)
#define A7DA_TSADC_AUX4			(_TSADC_REG_BASE + 0x0028)
#define A7DA_TSADC_AUX5			(_TSADC_REG_BASE + 0x002c)
#define A7DA_TSADC_AUX6			(_TSADC_REG_BASE + 0x0030)
#define A7DA_TSADC_AUX7			(_TSADC_REG_BASE + 0x0034)
#define A7DA_TSADC_AUX8			(_TSADC_REG_BASE + 0x0038)
#define A7DA_TSADC_TEMP1		(_TSADC_REG_BASE + 0x004c)
#define A7DA_TSADC_TEMP2		(_TSADC_REG_BASE + 0x0050)
#define A7DA_TSADC_INTR_SET		(_TSADC_REG_BASE + 0x0054)
#define A7DA_TSADC_INTR_CLR		(_TSADC_REG_BASE + 0x0058)

/* INTR register defines */
#define A7DA_TSADC_PEN_INTR_EN		BIT5
#define A7DA_TSADC_DATA_INTR_EN		BIT4
#define A7DA_TSADC_PEN_INTR		BIT1
#define A7DA_TSADC_DATA_INTR		BIT0

/* DATA register defines */
#define A7DA_TSADC_DATA_VALID		BIT30
#define A7DA_TSADC_DATA_MASK		0x3FFF
#define A7DA_TSADC_CHANNEL_MASK		( 0x1F << 10 )

typedef enum {
	AUX1 = 0,
	AUX2,
	AUX3,
	AUX4,
	AUX5,
	AUX6,
	AUX7,
	AUX8,
	TEMP1,
	TEMP2,
} ChannelIds;

struct TscChannel {
	unsigned int ulMode;
	unsigned int ulValReg;
};

/* Functions of tsadc initialize */
int vTsadcInit( void );

void vTsadcEnable( void );
void vTsadcDisable( void );

/* get channel voltage */
unsigned int ulTsadcGetVoltage( ChannelIds eChannel );

#endif /* __TSADC_H__ */
