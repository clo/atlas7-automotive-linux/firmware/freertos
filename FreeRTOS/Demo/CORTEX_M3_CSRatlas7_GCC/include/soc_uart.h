/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ATLAS7_UART_H__
#define __ATLAS7_UART_H__

#define SOC_UART_NUMBER	7

/* Base address of UART */
#define _UART_REG_BASE 0x18010000

/* Registers of UART#0 */
#define A7DA_UART_LINE_CTRL		(_UART_REG_BASE + 0x0040)
#define A7DA_UART_TXRX_ENA_REG		(_UART_REG_BASE + 0x004C)
#define A7DA_UART_DIVISOR		(_UART_REG_BASE + 0x0050)
#define A7DA_UART_INT_ENABLE_SET	(_UART_REG_BASE + 0x0054)
#define A7DA_UART_INT_STATUS		(_UART_REG_BASE + 0x0058)
#define A7DA_UART_RISC_DSP_MODE		(_UART_REG_BASE + 0x005C)
#define A7DA_UART_INT_ENABLE_CLR	(_UART_REG_BASE + 0x0060)
#define A7DA_UART_TX_DMA_IO_CTRL	(_UART_REG_BASE + 0x0100)
#define A7DA_UART_TX_DMA_IO_LEN		(_UART_REG_BASE + 0x0104)
#define A7DA_UART_TXFIFO_CTRL		(_UART_REG_BASE + 0x0108)
#define A7DA_UART_TXFIFO_LEVEL_CHK	(_UART_REG_BASE + 0x010C)
#define A7DA_UART_TXFIFO_OP		(_UART_REG_BASE + 0x0110)
#define A7DA_UART_TXFIFO_STATUS		(_UART_REG_BASE + 0x0114)
#define A7DA_UART_TXFIFO_DATA		(_UART_REG_BASE + 0x0118)
#define A7DA_UART_RX_DMA_IO_CTRL	(_UART_REG_BASE + 0x0120)
#define A7DA_UART_RX_DMA_IO_LEN		(_UART_REG_BASE + 0x0124)
#define A7DA_UART_RXFIFO_CTRL		(_UART_REG_BASE + 0x0128)
#define A7DA_UART_RXFIFO_LEVEL_CHK	(_UART_REG_BASE + 0x012C)
#define A7DA_UART_RXFIFO_OP		(_UART_REG_BASE + 0x0130)
#define A7DA_UART_RXFIFO_STATUS		(_UART_REG_BASE + 0x0134)
#define A7DA_UART_RXFIFO_DATA		(_UART_REG_BASE + 0x0138)

/* Registers of UART#0 ADD */
#define A7DA_UART_AFC_CTRL		(_UART_REG_BASE + 0x0140)
#define A7DA_UART_RX_DMA_CNTR		(_UART_REG_BASE + 0x0144)
#define A7DA_UART_SWH_DMA_IO		(_UART_REG_BASE + 0x0148)

/* BITs of UART TX DMA I/O MODE Register */
#define UART_TX_IO_MODE		BIT0

/* BITs of UART RX DMA I/O MODE Register */
#define UART_RX_IO_MODE		BIT0

/* BITs of UART Transmit/Receive Enable Register */
#define UART_RX_EN		BIT0
#define UART_TX_EN		BIT1

/* BITs of UART TX FIFO Operation Register */
#define UART_TXFIFO_RESET	BIT0
#define UART_TXFIFO_START	BIT1

/* BITs of UART#0,2,3,4,5,6 TX FIFO Control register */
#define UART_TXFIFO_THD		BIT6

/* BITs of UART#1 TX FIFO Control register */
#define UART1_TXFIFO_THD	BIT4

/* UART#0,2,3,4,5,6 TX FIFO Status Register */
#define UART_TXFIFO_LEVEL	BIT6
#define UART_TXFIFO_FULL	BIT7
#define UART_TXFIFO_EMPTY	BIT8

/* UART#1 TX FIFO Status Register */
#define UART1_TXFIFO_LEVEL	BIT4
#define UART1_TXFIFO_FULL	BIT5
#define UART1_TXFIFO_EMPTY	BIT6

/* BITs of UART RX FIFO Operation Register */
#define UART_RXFIFO_RESET	BIT0
#define UART_RXFIFO_START	BIT1

/* BITs of UART#0,2,3,4,5,6 RX FIFO Control register */
#define UART_RXFIFO_THD		BIT6

/* BITs of UART#1 RX FIFO Control register */
#define UART1_RXFIFO_THD	BIT4

/* UART#0,2,3,4,5,6 RX FIFO Status Register */
#define UART_RXFIFO_LEVEL	BIT6
#define UART_RXFIFO_FULL	BIT7
#define UART_RXFIFO_EMPTY	BIT8

/* UART#1 RX FIFO Status Register */
#define UART1_RXFIFO_LEVEL	BIT4
#define UART1_RXFIFO_FULL	BIT5
#define UART1_RXFIFO_EMPTY	BIT6

/* Definitions of UART interrupt enable bits */
#define UART_RX_DONE_INT_EN_SET		BIT0
#define UART_TX_DONE_INT_EN_SET		BIT1
#define UART_RX_OFLOW_INT_EN_SET	BIT2
#define UART_TX_ALLOUT_INT_EN_SET	BIT3
#define UART_RX_IO_DMA_INT_EN_SET	BIT4
#define UART_TX_IO_DMA_INT_EN_SET	BIT5
#define UART_RXFIFO_FULL_INT_EN_SET	BIT6
#define UART_TXFIFO_EMPTY_INT_EN_SET	BIT7
#define UART_RXFIFO_THD_INT_EN_SET	BIT8
#define UART_TXFIFO_THD_INT_EN_SET	BIT9
#define UART_FRAME_ERR_INT_EN_SET	BIT10
#define UART_RXD_BREAK_INT_EN_SET	BIT11

/* Definitions of UART interrupt status bits */
#define UART_INT_MASK_ALL		0xFFFF
#define UART_INT_RX_DONE		BIT0
#define UART_INT_TX_DONE		BIT1
#define UART_INT_RX_OFLOW		BIT2
#define UART_INT_TX_ALL_EMPTY		BIT3
#define UART_INT_DMA_IO_RX_DONE		BIT4
#define UART_INT_DMA_IO_TX_DONE		BIT5
#define UART_INT_RXFIFO_FULL		BIT6
#define UART_INT_TXFIFO_EMPTY		BIT7
#define UART_INT_RXFIFO_THD_REACH	BIT8
#define UART_INT_TXFIFO_THD_REACH	BIT9
#define UART_INT_FRM_ERR		BIT10
#define UART_INT_RXD_BREAK		BIT11
#define UART_INT_UART_RX_TIMEOUT	BIT12
#define UART_INT_PARITY_ERR		BIT13
#define UART_INT_CTS_CHANGE		BIT14
#define UART_INT_RTS_CHANGE		BIT15
#define UART_INT_PLUG_IN		BIT16

/* Enumernate the Uart Devices */
enum {
	UART0 = 0,
	UART1,
	UART2,
	UART3,
	UART4,
	UART5,
	UART6,
};

/* Enumernate Uart Devices Working modes */
enum {
	UART_MODE_POLL = 0,
	UART_MODE_INTR,
};

/* Functions of uart initialize */
int xUartDeviceInitialize( uint8_t ucPort, uint8_t ucBlock, uint32_t uxBaudRate, uint32_t uxIoClk );
char uartReadByte(void);
void uartWriteByte(u8 ch);
void vUartDeviceReHwInitialize( uint8_t ucPort );

#endif /* __ATLAS7_UART_H__ */
