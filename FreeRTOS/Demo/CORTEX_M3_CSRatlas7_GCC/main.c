/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"

/* Library include files. */
#include "soc_irq.h"
#include "core_cm3.h"

/* platform include files */
#include "io.h"
#include "utils.h"
#include "pm.h"
#include "qspi.h"
#include "canbus.h"
#include "system/audio/audio.h"
#include "system/audio/kalimba_ipc.h"
#include "system/audio/kcm/kcm.h"
#include "gpt.h"
#include "sirfdrive3.h"

#include "soc_clkc.h"
#include "soc_timer.h"
#include "soc_uart.h"
#include "soc_gpio.h"
#include "soc_pwrc.h"
#include "soc_pwrc_retain.h"
#include "soc_iobridge.h"
#include "soc_ioc.h"

#include "misclib.h"
#include "debug.h"
#include "loader.h"
#if configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1
#include "trcUser.h"
#endif

#ifdef CONFIG_ENABLE_CANBUS_DEMO
#include "utils/test/canbus_demo_lib.h"
#endif

#include "image_header.h"

extern int _dram;
extern int _dram_load_offset;
extern int _dram_size;

extern void clock_init(void);
extern void clk_src_late_init(void);
extern int ddr_init(void);

DECLARE_API_GET_IMAGE_ADDRESS()
DECLARE_API_GET_IMAGE_SIZE()
DECLARE_API_GET_IMAGE_ADDRESS_AND_SIZE()

#ifdef __PIC_ENABLE__
#define RELOCATE_FLAG	( 0xFC << 24 )
#define RELOCATE_MASK	( 0xFF << 24 )
void vRelocateVectorTable( UBaseType_t uxPicOffset, UBaseType_t *pxVtbl, UBaseType_t uxTblSzInBytes )
{
	uint32_t uxIdx, uxTblSz;
	UBaseType_t *pxVectorEntry;

	uxTblSz = uxTblSzInBytes >> 2;
	for( uxIdx = 1; uxIdx < uxTblSz; uxIdx++ )
	{
		pxVectorEntry = pxVtbl + uxIdx;
		if( *pxVectorEntry != 0 )
		{
			*pxVectorEntry = ( *pxVectorEntry  & ~RELOCATE_MASK ) + uxPicOffset;
		}
	}
}

void vRelocateGlobalOffsetTable( UBaseType_t uxPicOffset, UBaseType_t *pxGotbl, UBaseType_t uxTblSzInBytes )
{
	uint32_t uxIdx, uxTblSz;
	UBaseType_t *pxGotEntry;

	uxTblSz = uxTblSzInBytes >> 2;
	for( uxIdx = 0; uxIdx < uxTblSz; uxIdx++ )
	{
		pxGotEntry = pxGotbl + uxIdx;
		if( (*pxGotEntry & RELOCATE_MASK ) == RELOCATE_FLAG )
		{
			*pxGotEntry = ( *pxGotEntry  & ~RELOCATE_MASK ) + uxPicOffset;
		}
	}
}

void vRelocateDataRel( UBaseType_t uxPicOffset, UBaseType_t *pxDataRel, UBaseType_t uxTblSzInBytes )
{
	uint32_t uxIdx, uxTblSz;
	UBaseType_t *pxDataEntry;

	uxTblSz = uxTblSzInBytes >> 2;
	for( uxIdx = 0; uxIdx < uxTblSz; uxIdx++ )
	{
		pxDataEntry = pxDataRel + uxIdx;
		if( (*pxDataEntry & RELOCATE_MASK ) == RELOCATE_FLAG )
		{
			*pxDataEntry = ( *pxDataEntry  & ~RELOCATE_MASK ) + uxPicOffset;
		}
	}
}
#endif


const static char *pcWakeSrcString[] = {
	"Cold", "Warm", "CAN", "Watchdog"
};
static uint32_t uxWakeSrc, uxBootMode;
static void prvCheckAndBoostIndepMode( void )
{
	uxWakeSrc = uxPmCheckWakeupSource();
	uxBootMode = uxPmCheckBootMode();

	if( PM_WAKE_WARM_BOOT == uxWakeSrc || PM_WAKE_CAN == uxWakeSrc )
		return;

	#ifndef __M3_TINY_LOADER_USED__  /* no need as already done by tinyloader */
	vChangeCpuClock( configCPU_CLOCK_HZ );
	#endif

	if( PM_BOOT_INDEPENDENT == uxBootMode )
	{
		#ifndef __M3_TINY_LOADER_USED__  /* no need as already done by tinyloader */
		vQspiBoost();
		#endif
		#if defined(__ATLAS7_STEP_B__) && defined(__M3_LOADER_ENABLE__)
		vLoaderStartup();
		#endif

	}
}

/*-----------------------------------------------------------*/
/* if configUSE_IDLE_HOOK == 1, IDLE will call this function*/
void vApplicationIdleHook( void )
{
#if configUSE_CO_ROUTINES == 1
	/* Only when configUSE_CO_ROUTINES == 1, The co-routines are
	executed in the idle task using the idle task hook. */
	for( ;; )
	{
		/* BUG: If you enable configUSE_CO_ROUTINE, but you hadn't
		created any CoRoutine, that means prvInitialiseCoRoutineLists()
		will not be called, it will cause vCoRoutineSchedule() halt
		at: prvCheckPendingReadyList() or prvCheckDelayedList(). */
		vCoRoutineSchedule();
	}
#endif
}

/*-----------------------------------------------------------*/
/* if configCHECK_FOR_STACK_OVERFLOW == 1, StackOverlow will call this function*/
void vApplicationStackOverflowHook( xTaskHandle pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	taskDISABLE_INTERRUPTS();
	DebugMsg("StackOverflow Taskname:%s\r\n", pcTaskName);
	for( ;; );
}

/*-----------------------------------------------------------*/
/* if configUSE_MALLOC_FAILED_HOOK == 1, Malloc Failed will call this function*/
void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c, heap_2.c or heap_4.c are used, then the
	size of the heap available to pvPortMalloc() is defined by
	configTOTAL_HEAP_SIZE in FreeRTOSConfig.h, and the xPortGetFreeHeapSize()
	API function can be used to query the size of free heap space that remains
	(although it does not provide information on how the remaining heap might
	be fragmented). */
	taskDISABLE_INTERRUPTS();
	DebugMsg("MallocFailed\r\n");
	for( ;; );
}

const unsigned long __stack_chk_guard = 0x000a0dff;

void __stack_chk_fail(void)
{
	DebugMsg("stack check fail\r\n");
	while(1);
}

/*-----------------------------------------------------------*/
extern unsigned int Vectors;
/*-----------------------------------------------------------*/
#if 0
static void vClkcCheckCa7ReadyFlags(void)
{
	while(SOC_REG(A7DA_CLKC_CLKC_SW_REG5) != BOOT_TEST_A7_READY)
	{
		vTaskDelay(1);
	}
}

static void vHandshakeWithCa7( void )
{
	/* tell A7, M3 is ready */
	clkc_set_m3_ready_flags();

	/* Wait A7 ready */
	vClkcCheckCa7ReadyFlags();

	/* clear flag register */
	clkc_clr_m3_ready_flags();
	clkc_clr_a7_ready_flags();
}
#endif

extern void vPmWdgHandler( eGptChipId eChipId,uint8_t ucGptIdx,void *pxData);
void prvSetupHardware( void )
{
	/* boost QSPI in independent mode */
	prvCheckAndBoostIndepMode();

#if (configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1)
	/* try to initialize clock, ignore the failed condition */
	clock_init();

	/* try to initialize ddr, ignore the failed condition */
	ddr_init();

	/* try to initialize clock late source, ignore the failed condition */
	clk_src_late_init();

	/* here DRAM is ready, init its section */
	memcpy(&_dram,(void*)(MEM(PM_ARG_BOOT_ENTRY)+(uint32_t)&_dram_load_offset),(size_t)&_dram_size);

	vTraceInitTraceData();

	/* Start or restart the trace. */
	vTraceStop();
	vTraceClear();
	vTraceStart();
#endif

	/* IPC need this clk */
	clkc_enable_vdifm_nocr();

	/* OTP need this clk */
	clkc_enable_gnssm_io();

	/* IOCTOP & TIMER need this clk */
	clkc_enable_audmscm_io();

	/* enable ioc_top */
	clkc_enable_ioc_top_io();

	/* enable timer */
	clkc_enable_timer_io();

	/* Write Vector table to SCB */
	SCB->VTOR = (uint32_t)( &Vectors );

	/* set priority grouping */
	NVIC_SetPriorityGrouping(0x0);

	xGptInitialize(eGptOsTimerA);
	xGptRegisterWdgInterrupt(vPmWdgHandler,eGptOsTimerA,NULL);

	xGptInitialize(eGptOsTimerM3);

	xGpioChipInitialize( eGpioRtcm );

#ifndef __PXP_ENABLE__
	/* Enable canbus wakeup */
	if(ENABLE_M3_CAN1_WAKUP())
	{
		ioc_enable_can1_mux();
	}
	vPmEnableCanWakeup( PM_CAN_WAKEUP_CONTROLLER0 );

	if( configCPU_CLOCK_HZ == 192000000 )
		vCanbusInitialize( CAN_PORT0, CAN_SPEED_48M_1M );
	else
		vCanbusInitialize( CAN_PORT0, CAN_SPEED_40M_1M );

#ifdef CONFIG_ENABLE_CANBUS_DEMO
	vCanbusDemoStart(CAN_PORT0);
#endif

#endif	/* __PXP_ENABLE__ */
}

void vPortException(int intno)
{
	DebugMsg(" # interrupt %d happened as exception, panic error!\r\n",intno);
	while(1);
}
static uint32_t uxStartupTime;
extern unsigned int debugRecorderDataPtr;
uint32_t uxChimePlayTime;
bool m3_audio_support = false;
static void prvStartupTask(void *pvParameters)
{
	/* M3 will initialize the clock and ddr if not initialized by A7 uboot
	 * so no need to handshake */
	/* vHandshakeWithCa7(); */
#if (configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 0)
	/* try to initialize clock, ignore the failed condition */
	clock_init();

	/* try to initialize ddr, ignore the failed condition */
	ddr_init();

	/* try to initialize clock late source, ignore the failed condition */
	clk_src_late_init();


	/* here DRAM is ready, init its section */
	memcpy(&_dram,(void*)(MEM(PM_ARG_BOOT_ENTRY)+(uint32_t)&_dram_load_offset),(size_t)&_dram_size);
#endif /* configINCLUDE_TRACE_RELATED_CLI_COMMANDS */
	if( (0xFFFFFFFF != uxGetImageAddress(IMAGE_ID_KALIMBA_FW)) &&
	    (0xFFFFFFFF != uxGetImageAddress(IMAGE_ID_AUDIO_FILE)) )
		m3_audio_support = true;

	if (m3_audio_support)
		audio_init();

	/* set it for lcdvip domain gpio test */
	xGpioChipInitialize( eGpioVdifmLcdVip );
	xUartDeviceInitialize( UART0, UART_MODE_INTR, configCLI_BAUD_RATE, configSOC_IO_CLK );

#ifndef __PXP_ENABLE__
	vCanbusInitialize( CAN_PORT1, CAN_SPEED_48M_1M );
#ifdef CONFIG_ENABLE_CANBUS_DEMO
	vCanbusDemoStart(CAN_PORT1);
#endif
#endif
	SirfDrive3_Init();

	vTaskRemoteprocCreate();


	vUARTCommandConsoleStart( mainUART_COMMAND_CONSOLE_STACK_SIZE + 8,
				mainUART_COMMAND_CONSOLE_TASK_PRIORITY );
	vRegisterSampleCLICommands();

#ifdef __PIC_ENABLE__
	DebugMsg("FreeRTOS-PIE main entry:0x%08x, %s boot, %s mode\r\n",
			  MEM(PM_ARG_BOOT_ENTRY), pcWakeSrcString[uxWakeSrc], ( PM_BOOT_INDEPENDENT == uxBootMode ) ? "Independent" : "Dependent");
#else
	DebugMsg("FreeRTOS(%x) main entry:0x%08x, %s boot, %s mode\r\n",PWRC_GET_FREERTOS_VERSION(),
			  MEM(PM_ARG_BOOT_ENTRY), pcWakeSrcString[uxWakeSrc], ( PM_BOOT_INDEPENDENT == uxBootMode ) ? "Independent" : "Dependent");
#endif

#ifdef __ATLAS7_STEP_B__
	DebugMsg(" STEP-B, PON status = %X\r\n",uxIoBridgeRead( A7DA_PWRC_PON_STATUS ));
#else
	DebugMsg(" STEP-A, PON status = %X\r\n",uxIoBridgeRead( A7DA_PWRC_PON_STATUS ));
#endif
	DebugMsg(" BUILD @ %s %s, start cost %dus(%dus)\r\n",__DATE__,__TIME__,uxStartupTime,MEM(PM_BOOT_TIME_MEM));
	DebugMsg(" uxChimePlayTime = %dus\n", uxChimePlayTime);

	vPmPowerModeManager();

	vTaskDelete(NULL); /* end of startup, job is done */
}

void Main( void )
{
	uxStartupTime = uxOsGetCurrentTick();

	REQUEST_HWLOCK( 0 );
	PWRC_CLEAR_FREERTOS_VERSION();
	PWRC_SET_FREERTOS_VERSION(0x10);
	RELEASE_HWLOCK( 0 );

	/* Setup the ports used by the demo and the clock. */
	prvSetupHardware();

	xTaskCreate(prvStartupTask, "startup", ( configMINIMAL_STACK_SIZE * 3UL ),
				(void *)NULL, tskIDLE_PRIORITY + 1, NULL);

	/* Start the scheduler running the tasks and co-routines just created. */
	vTaskStartScheduler();

	/* Should not get here unless we did not have enough memory to start the
	scheduler. */
	while ( 1 );
}
