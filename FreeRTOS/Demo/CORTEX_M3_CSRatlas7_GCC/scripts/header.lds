/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

MEMORY
{
	FLASH (rx) : ORIGIN = $FLASH_ADDR, LENGTH = 512K
	SPRAM (rwx) : ORIGIN = 0x17e00000, LENGTH = 32K
	RETAIN (rwx) : ORIGIN = 0x17e08000, LENGTH = 32K
	 /* check atlas7-evb-common.dtsi cm3_reserved:
	  *  cm3_reserved: cm3@45400000 {
	  *     reg = <0x45400000 0x100000>;
	  *     no-map;
	  *  };
	  * Here use the last 64K as data RAM for M3. */
	DRAM (rw) : ORIGIN = 0x054f0000, LENGTH = 64K
}

REGION_ALIAS("REGION_TEXT", FLASH);
REGION_ALIAS("REGION_RODATA", FLASH);
REGION_ALIAS("REGION_DATA", SPRAM);
REGION_ALIAS("REGION_BSS", SPRAM);

/*
 * REGION_SPRAM can store the data & code after DRAM power off.
 * For example, hiberation operating code.
 */
REGION_ALIAS("REGION_SPRAM", SPRAM);

/*
 * REGION_RETAIN can store the data & code after SPRAM power off.
 * For example, wake up code.
 */
REGION_ALIAS("REGION_RETAIN", RETAIN);

_m3_rom_start_addr = ORIGIN(FLASH);
_m3_rom_size = LENGTH(FLASH);

_m3_spram_start_addr = ORIGIN(SPRAM);
_m3_spram_size = LENGTH(SPRAM);

_m3_retain_start_addr = ORIGIN(RETAIN);
_m3_retain_size = LENGTH(RETAIN);

/* Use spam as default memory */
_m3_memory_start_addr = _m3_spram_start_addr;
_m3_memory_size = _m3_spram_size;

/* Place stack on spram */
_m3_stack_size = $STACK_SZ;

/* Set stack top and bottom */
_stack_bottom_addr = _m3_spram_start_addr;
_stack_top_addr = _stack_bottom_addr + _m3_stack_size;
_stack_top_addr_temp = _stack_top_addr + 0x20;
