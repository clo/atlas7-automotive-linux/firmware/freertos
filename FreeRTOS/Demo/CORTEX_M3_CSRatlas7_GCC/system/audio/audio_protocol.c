/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "ctypes.h"
#include "debug.h"
#include "soc_irq.h"
#include "misclib.h"
#include "core_cm3.h"
#include "system/rpmsg/rpmsg.h"
#include "audio_protocol.h"
#include "kalimba_ipc.h"
#include "kalimba.h"
#include "buffer.h"
#include "semphr.h"
#include "errno.h"
#include "kcm/kasobj.h"
#include "kcm/kcm.h"

#include <soc_cache.h>
#include <soc_mem.h>
#include <soc_clkc.h>
#include <soc_timer.h>
#include <soc_kas.h>
#include "iacc.h"
#include "io.h"
#include "task.h"

extern bool m3_audio_reset;
TaskHandle_t  crash_check_task;

#define _A7DA_DMAC3_BASE					0x10D60000
#define A7DA_DMAC3_DMA_INT_OWNER_REG0		(_A7DA_DMAC3_BASE + 0x164)
#define A7DA_DMAC3_DMA_INT_OWNER_REG1		(_A7DA_DMAC3_BASE + 0x168)
#define A7DA_DMAC3_DMA_CH_INT			(_A7DA_DMAC3_BASE + 0x144)
#define A7DA_DMAC3_DMA_INT_EN_CLR		(_A7DA_DMAC3_BASE + 0x14C)
#define A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR		(_A7DA_DMAC3_BASE + 0x15C)
#define A7DA_DMAC3_DMA_CH_VALID			(_A7DA_DMAC3_BASE + 0x140)

#define MSG_START_STREAM			0x00000001
#define MSG_STOP_STREAM				0x00000002
#define MSG_OPERATOR_CFG			0x00000003
#define MSG_PS_ADDR_SET				0x00000004
#define MSG_PS_UPDATE				0x00000005
#define MSG_DATA_PRODUCED			0x00000006
#define MSG_DATA_CONSUMED			0x00000007
#define MSG_AUDIO_CODEC_SET			0x00000008
#define MSG_GET_AUDIO_CODEC_VOL_RANGE		0x00000009
#define MSG_AUDIO_CODEC_VOL_SET			0x0000000A
#define MSG_DSP_COMMAND				0x0000000B
#define MSG_LICENSE_REQ				0x0000000C
#define MSG_LICENSE_RESP			0x0000000D
#define MSG_DRAM_ALLOCATION_REQ			0x0000000E
#define MSG_DRAM_ALLOCATION_RESP		0x0000000F
#define MSG_DRAM_FREE_REQ			0x00000010
#define MSG_DRAM_FREE_RESP			0x00000011
#define MSG_OP_OBJ_REQ				0x00000012
#define MSG_OP_OBJ_RESP				0x00000013
#define MSG_CTRL_REQ				0x00000014
#define MSG_CTRL_RESP				0x00000015
#define MSG_CREATE_STREAM			0x00000016
#define MSG_DESTROY_STREAM			0x00000017
#define MSG_COREDUMP				0x00000018
#define MSG_CREATE_STREAM_RESP			0x00000019
#define MSG_DESTROY_STREAM_RESP			0x0000001A
#define MSG_START_STREAM_RESP			0x0000001B
#define MSG_STOP_STREAM_RESP			0x0000001C

#define MSG_NEED_ACK				0x1
#define MSG_NEED_RSP				0x2

#define MSG_RESP_SUCCESS			0x00
#define MSG_RESP_ERROR				(~0x00)

struct rpmsg_device *audio_rpdev;
SemaphoreHandle_t crash_mutex;

struct stream_context_t {
	bool is_created;
	bool is_started;
	struct kasobj_fe *fe;
	u32 rate;
	u32 channels;
	u32 buff_addr;
	u32 buff_bytes;
	u32 period_bytes;
};

struct coredump_desc_t {
	u32 base_addr;

	u32 pm_addr;
	u32 pm_len;

	u32 dm1_addr;
	u32 dm1_len;

	u32 dm2_addr;
	u32 dm2_len;

	u32 rm_addr;
	u32 rm_len;

	u32 kregs_addr;
	u32 kregs_len;
};

#define HEADER_SIZE   1024

enum mtype {KAS_PM, KAS_DM1, KAS_DM2, KAS_RM};

static struct kas_mem {
	enum mtype mem_type;
	char *mem_name;
	u32 mem_start;
	u32 mem_size;
} kas_memory[] = {
	{ KAS_PM,  "DC", KAS_PM_SRAM_START_ADDR,  0x10000},
	{ KAS_DM1, "DD", KAS_DM1_SRAM_START_ADDR, 0x8000},
	{ KAS_DM2, "DD", KAS_DM2_SRAM_START_ADDR, 0x8000},
	{ KAS_RM,  "DR", 0x00FFFE00, 0x00200},
};

char *kregs[] = {
	"R PC",
	"R rMAC2",
	"R rMAC1",
	"R rMAC0",
	"R rMAC24",
	"R R0",
	"R R1",
	"R R2",
	"R R3",
	"R R4",
	"R R5",
	"R R6",
	"R R7",
	"R R8",
	"R R9",
	"R R10",
	"R RLINK",
	"R FLAGS",
	"R RMACB24",
	"R I0",
	"R I1",
	"R I2",
	"R I3",
	"R I4",
	"R I5",
	"R I6",
	"R I7",
	"R M0",
	"R M1",
	"R M2",
	"R M3",
	"R L0",
	"R L1",
	"R L3",
	"R L4",
	"R RUNCLKS",
	"R NUMINSTRS",
	"R NUMSTALLS",
	"R rMACB2",
	"R rMACB1",
	"R rMACB0",
	"R B0",
	"R B1",
	"R B4",
	"R B5",
	"R FP",
	"R SP"
};
void do_coredump(void);

/*
 * stop kalimba
 */
static void kerror_stopdsp(void)
{
	/* Stop kalimba */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_DEBUG << 2)
			| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;
}

/*
 * kas dump memory
 */
static void kerror_dumpmem(enum mtype mt,
			u32 start_addr, u32 length, u32 *data)
{
	u32 i, addr;

	/* Set address auto incriment mode on every 4 bytes */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;

	/* Check if the memory type is Program Memory (PM) */
	if (mt == KAS_PM)
		addr = start_addr | (0x3 << 30);
	else
		addr = (start_addr << 2) | (0x2 << 30);

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = addr;

	for (i = 0; i < length; i++)
		*(data + i) = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);

}

static struct stream_context_t __on_dram stream_contexts[MAX_STREAMS];
static void set_dmac_owner(int ch)
{
	if (ch > 15)
		return;
	if (ch < 10)
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG0, 7 << (ch * 3),
				3 << (ch * 3));
	else
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG1, 7 << ((ch  - 10)* 3),
				3 << ((ch - 10) * 3));
}

#ifdef KALIMBA_IPC_CRASH_CHECK
static void clear_dmac_owner(int ch)
{
	if (ch > 15)
		return;
	if (ch < 10)
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG0, 7 << (ch * 3),
				2 << (ch * 3));
	else
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG1, 7 << ((ch  - 10)* 3),
				2 << ((ch - 10) * 3));
}

static void disable_dmac(int ch)
{
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR, BIT(ch) | BIT(ch + 16),
			BIT(ch) | BIT(ch + 16));
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_EN_CLR, BIT(ch), BIT(ch));
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_INT, BIT(ch), BIT(ch));
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_VALID, BIT(ch), BIT(ch));
}
#endif	/* KALIMBA_IPC_CRASH_CHECK */

int destroy_stream(u32 stream, u32 channels)
{
	struct stream_context_t *stream_context;
	struct kasobj_fe *obj_fe;
	int ret;

	if (stream >= MAX_STREAMS) {
		DebugMsg("Max support streams range is from 0 to %d, the require stream is %d\n",
			MAX_STREAMS - 1, stream);
		return -EINVAL;;
	}

	stream_context = &stream_contexts[stream];
	if (!stream_context->is_created)
		return 0;

	obj_fe = stream_context->fe;
	if (obj_fe->running_chain) {
		ret = __kcm_stop_chain_link(obj_fe->running_chain);
		if (ret) {
			DebugMsg("Audio: %s() stream '%s' stop LINK error\r\n",
				__func__, obj_fe->obj.name);
			return -EINVAL;
		}
		ret = __kcm_stop_chain_op(obj_fe->running_chain);
		if (ret) {
			DebugMsg("Audio: %s() stream '%s' stop OP error\r\n",
				__func__, obj_fe->obj.name);
			return -EINVAL;
		}
		ret = kcm_put_chain(obj_fe->running_chain);
		if (ret) {
			DebugMsg("Audio: %s() stream '%s' destroy error\r\n",
				__func__, obj_fe->obj.name);
			return -EINVAL;
		}
		obj_fe->running_chain = NULL;
	}
	stream_context->is_started = false;
	stream_context->is_created = false;

	return 0;
}

static struct kasobj_param __on_dram _stream_kasparam;
int create_stream(u32 stream, u32 rate, u32 channels, u32 buff_addr,
		u32 buff_bytes, u32 period_bytes)
{
	struct kasobj_fe *obj_fe = kcm_find_fe(stream);
	struct stream_context_t *stream_context;
	struct kcm_chain *chain;
	struct kasobj_param *kasparam;
	static int init_flag = 0;
	int ret;

	if (!obj_fe) {
		DebugMsg("Audio(%s): Find front-end failed\r\n", __func__);
		return -EINVAL;
	}

	LOG("Audio: %s(), stream_id = %d, obj_fe = 0x%x, channels = %d, buff_addr = 0x%x, buff_bytes = %d, period_bytes = %d\r\n",
		__func__, stream, obj_fe, channels, buff_addr, buff_bytes, period_bytes);

	if (stream >= MAX_STREAMS) {
		DebugMsg("Max support streams range is from 0 to %d, the require stream is %d\n",
			MAX_STREAMS - 1, stream);
		return -EINVAL;;
	}

	stream_context = &stream_contexts[stream];
	stream_context->fe = obj_fe;
	if (stream_context->is_created) {
		DebugMsg("Audio: Stream is running/busy, create stream(FE = %s) fialed\r\n", obj_fe->obj.name);
		return -EINVAL;;
	}

	chain = kcm_prepare_chain(obj_fe, obj_fe->db->playback, channels);
	if (IS_ERR(chain))
	{
		DebugMsg("Audio: %s() stream '%s' prepare error\r\n", __func__, obj_fe->obj.name);
		return -EINVAL;
	}

	stream_context->rate = rate;
	stream_context->channels = channels;
	stream_context->buff_addr = buff_addr;
	stream_context->buff_bytes = buff_bytes;
	stream_context->period_bytes = period_bytes;

	kasparam = &_stream_kasparam;
	kasparam->rate = rate;
	kasparam->channels = channels;
	kasparam->period_size = period_bytes / 4;
	kasparam->format = KCM_FORMAT_S16_LE;
	kasparam->ep_handle_pa = buff_addr;
	kasparam->buffer_bytes = buff_bytes;

	ret = kcm_get_chain(chain, kasparam);
	if (ret) {
		DebugMsg("Audio: %s() create chain '%s' error \r\n", __func__, chain->name);
		return -EINVAL;
	}
	obj_fe->running_chain = chain;

	if (init_flag == 0) {
		SOC_REG (A7DA_CLKC_AUDIO_DMAC3_LEAF_CLK_EN_SET) = BIT0;
		set_dmac_owner(7);
		set_dmac_owner(8);
		set_dmac_owner(3);
		set_dmac_owner(9);

		set_dmac_owner(0); // iacc rx0

		set_dmac_owner(1); // i2s1-rx
		set_dmac_owner(2); // i2s1-tx

		init_flag++;
	}
	stream_context->is_created = true;

	return 0;
}

int start_stream(u32 stream)
{
	struct kasobj_fe *obj_fe;
	struct stream_context_t *stream_context;
	int ret;

	stream_context = &stream_contexts[stream];
	obj_fe = stream_context->fe;
	ret = __kcm_start_chain_hw(obj_fe->running_chain);
	if (ret) {
		DebugMsg("Audio: %s() stream '%s' start HW error\r\n",
			__func__, obj_fe->obj.name);
		return -EINVAL;
	}
	if (!stream_context->is_started) {
		ret = __kcm_start_chain_link(obj_fe->running_chain);
		if (ret) {
			DebugMsg("Audio: %s() stream '%s' start LINK error\r\n",
				__func__, obj_fe->obj.name);
			return -EINVAL;
		}
		ret = __kcm_start_chain_op(obj_fe->running_chain);
		if (ret) {
			DebugMsg("Audio: %s() stream '%s' start OP error\r\n",
				__func__, obj_fe->obj.name);
			return -EINVAL;
		}
	}
	if (obj_fe->db->playback && !obj_fe->db->internal)
		kalimba_data_produced(obj_fe->ep_id[0]);

	stream_context->is_started = true;

	return 0;
}

int stop_stream(u32 stream)
{
	struct kasobj_fe *obj_fe;
	struct stream_context_t *stream_context;
	int ret;

	stream_context = &stream_contexts[stream];
	obj_fe = stream_context->fe;
	ret = __kcm_stop_chain_hw(obj_fe->running_chain);
	if (ret) {
		DebugMsg("Audio: %s() stream '%s' stop HW error\r\n",
			__func__, obj_fe->obj.name);
		return -EINVAL;
	}

	return 0;
}

void stream_send_resp(u32 msg_id, u32 ret)
{
	u32 msg[2];

	if (!audio_rpdev) {
		DebugMsg("Audio IPC: Stream response, rpdev is NULL !\r\n");
		return;
	}
	msg[0] = msg_id;
	if (ret)
		msg[1] = MSG_RESP_ERROR;
	else
		msg[1] = MSG_RESP_SUCCESS;
	rpmsg_send(audio_rpdev, (void *)msg, 2 * sizeof(u32));
}


void start_chime_stream(u32 stream, u32 rate, u32 channels, u32 buff_addr,
		u32 buff_bytes, u32 period_bytes)
{
	xSemaphoreTake(crash_mutex, portMAX_DELAY);
	create_stream(stream, rate, channels, buff_addr, buff_bytes, period_bytes);
	start_stream(stream);
	xSemaphoreGive(crash_mutex);
}

void stop_chime_stream(u32 stream, u32 channels)
{
	xSemaphoreTake(crash_mutex, portMAX_DELAY);
	stop_stream(stream);
	destroy_stream(stream, channels);
	xSemaphoreGive(crash_mutex);
}

void data_produced(u32 stream, u32 pos)
{

}

void send_op_obj(u32 *op_name, u32 len)
{
	static u32 msg[2];
	static char name[33];

	if (!audio_rpdev) {
		DebugMsg("Audio IPC: send OP OBJ, rpdev is NULL !\r\n");
		return;
	}
	if (!op_name || len < 1) {
		DebugMsg("Audio IPC: send OP OBJ, invalid OP name !\r\n");
		return;
	}
	strncpy(name, (char *)op_name, len);
	name[len] = '\0';
	msg[0] = MSG_OP_OBJ_RESP;
	msg[1] = (u32)kasobj_find_obj(name, kasobj_type_op);
	LOG("Audio IPC: op-m3 %s 0x%x\r\n", name, msg[1]);
	rpmsg_send(audio_rpdev, (void *)msg, 2 * sizeof(u32));
}

void op_ctrl_rsp(int put, void *op_obj, u32 ctrl_id, u32 value_id, u32 value)
{
	static u32 msg[2];

	if (!audio_rpdev) {
		DebugMsg("Audio IPC: OP ctrl response, rpdev is NULL !\r\n");
		return;
	}
	if (!op_obj) {
		DebugMsg("Audio IPC: OP ctrl response, OP OBJ is NULL !\r\n");
		return;
	}
	msg[0] = MSG_CTRL_RESP;
	msg[1] = kasobj_handle_op_ctrl(op_obj, put, ctrl_id, value_id, value);
	rpmsg_send(audio_rpdev, (void *)msg, 2 * sizeof(u32));
}

int process_audio_msg(u32 msg_id, void *data, int msg_len, void *resp)
{
	u32 *msg = (u32 *)data;
	u32 need_ack_rsp;
	u32 dsp_cmd_payload_size;
	u16 *dsp_cmd_payload;
	u32 ret;

	xSemaphoreTake(crash_mutex, portMAX_DELAY);

	switch (msg_id) {
	case MSG_CREATE_STREAM:
		ret = create_stream(msg[0], msg[1], msg[2], msg[3], msg[4], msg[5]);
		stream_send_resp(MSG_CREATE_STREAM_RESP, ret);
		break;
	case MSG_DESTROY_STREAM:
		ret = destroy_stream(msg[0], msg[1]);
		stream_send_resp(MSG_DESTROY_STREAM_RESP, ret);
		break;
	case MSG_START_STREAM:
		ret = start_stream(msg[0]);
		stream_send_resp(MSG_START_STREAM_RESP, ret);
		break;
	case MSG_STOP_STREAM:
		ret = stop_stream(msg[0]);
		stream_send_resp(MSG_STOP_STREAM_RESP, ret);
		break;
	case MSG_OPERATOR_CFG:
		break;
	case MSG_PS_ADDR_SET:
		kalimba_set_ps_region_addr(msg[0]);
		break;
	case MSG_DATA_PRODUCED:
		data_produced(msg[0], msg[1]);
		break;
	case MSG_DATA_CONSUMED:
		break;
	case MSG_AUDIO_CODEC_SET:
		break;
	case MSG_GET_AUDIO_CODEC_VOL_RANGE:
		break;
	case MSG_AUDIO_CODEC_VOL_SET:
		break;
	case MSG_LICENSE_RESP:
		kalimba_license_resp_send((u16 *)&msg[1], msg[0]);
		break;
	case MSG_DRAM_ALLOCATION_RESP:
		kalimba_dram_allocation_rsp_send(msg[0]);
		break;
	case MSG_DRAM_FREE_RESP:
		kalimba_dram_free_rsp_send();
		break;
	case MSG_DSP_COMMAND:
		need_ack_rsp = msg[0];
		dsp_cmd_payload_size = msg[1] / sizeof (u16);
		dsp_cmd_payload = (u16 *)(&msg[2]);
#if 0
		DebugMsg("DSP CMD: ");
		for (i = 0; i < dsp_cmd_payload_size; i++)
			DebugMsg("%04x ", dsp_cmd_payload[i]);

		DebugMsg("\n");MSG_PS_UPDATE
#endif

		ipc_send_msg(dsp_cmd_payload, dsp_cmd_payload_size, need_ack_rsp,
				resp);

		if (need_ack_rsp & MSG_NEED_RSP) {
			xSemaphoreGive(crash_mutex);
			return 1;
		}
		break;
	case MSG_OP_OBJ_REQ:
		send_op_obj(&msg[1], msg[0]);
		break;
	case MSG_CTRL_REQ:
		op_ctrl_rsp(msg[0], (void *)msg[1], msg[2], msg[3], msg[4]);
		break;
	default:
		DebugMsg("%s: invaild audio message id: %d\n", __func__, msg_id);
		break;
	}
	xSemaphoreGive(crash_mutex);
	return 0;
}

#define DSP_REQ_DATA_BYTES	22

u32 to_linux_msg[3 + (DSP_REQ_DATA_BYTES + 3) / sizeof(u32)];

void kalimba_msg_to_m3_handler(u16 *msg)
{
	u32 msg_len = 0;
	u16 dev_id;
	u32 *kas_data_pointer = (u32 *)(TO_MCU_IO_DRAM(KAS_DATA_POINTER));

	switch (msg[0]) {
	case DATA_PRODUCED:
		dev_id = kasobj_find_dev_id_by_ep(msg[2]);
		if (dev_id < MAX_STREAMS)
			kas_data_pointer[dev_id] = (msg[3] << 16 | msg[4]) * 4;
		break;
	case DATA_CONSUMED:
		dev_id = kasobj_find_dev_id_by_ep(msg[2]);
		if (CHIME_DEV_ID != dev_id && dev_id < MAX_STREAMS)
			kas_data_pointer[dev_id] = (msg[3] << 16 | msg[4]) * 4;
		break;
	case PS_FLUSH_REQ:
		to_linux_msg[0] = MSG_PS_UPDATE;
		msg_len = sizeof(u32);
		break;
	case KASCMD_SIGNAL_ID_LICENCE_CHECK_REQ:
		to_linux_msg[0] = MSG_LICENSE_REQ;
		to_linux_msg[1] = DSP_REQ_DATA_BYTES;
		memcpy(&to_linux_msg[2], &msg[2], DSP_REQ_DATA_BYTES);
		msg_len = 2 * sizeof(u32) + DSP_REQ_DATA_BYTES;
		break;
	case DRAM_ALLOCATION_REQ:
		to_linux_msg[0] = MSG_DRAM_ALLOCATION_REQ;
		to_linux_msg[1] = (u32)msg[2] * 4;
		msg_len = 2 * sizeof(u32);
		break;
	case DRAM_FREE_REQ:
		to_linux_msg[0] = MSG_DRAM_FREE_REQ;
		to_linux_msg[1] = (msg[2] & 0xffff) | (msg[3] << 16);
		msg_len = 2 * sizeof(u32);
		break;
	default:
		break;
	}

	if (msg_len > 0 && audio_rpdev)
		rpmsg_send(audio_rpdev, (void *)to_linux_msg, msg_len);
}

struct cmd_result {
	u32 cmd_result_id;
	u32 cmd_result_bytes;
	u16 resp[64];
};

static void __rpmsg_audio_protocol_cb(struct rpmsg_device *rpdev,
			void *data, int len, u32 src)
{
	struct cmd_result cmd_result;
	u32 msg_id = ((u32 *)data)[0];

	if (len == 4 && msg_id== 0xffffffff) {
		/* Get the address of source, the linux should send a sync signal to ensure the rpmsg audio connection has worked. */
		rpdev->remote = src;
		return;
	}
	len -= 4;
	data += 4;

	if (process_audio_msg(msg_id, data, len , cmd_result.resp)) {
		cmd_result.cmd_result_id = msg_id | 0x10000000;
		cmd_result.cmd_result_bytes = (cmd_result.resp[1] + 2) * 2;
		rpmsg_sendto(rpdev,  &cmd_result, cmd_result.cmd_result_bytes + 8, src);
	}
}

void do_coredump()
{
	int msg_len = 0;
	struct coredump_desc_t *coredump_desc;

	/*store head structre at the beginning of coredump pointer*/
	coredump_desc = (struct coredump_desc_t *)(TO_MCU_IO_DRAM(KAS_COREDUMP_HEAD_ADDR));

	coredump_desc->base_addr = KAS_COREDUMP_POINTER;
	/*dp to store kas memory data*/
	coredump_desc->pm_addr = KAS_COUREDUMP_PM_ADDR;
	coredump_desc->pm_len = KAS_COUREDUMP_PM_SIZE;

	coredump_desc->dm1_addr = KAS_COUREDUMP_DM1_ADDR;
	coredump_desc->dm1_len = KAS_COUREDUMP_DM1_SIZE;

	coredump_desc->dm2_addr = KAS_COUREDUMP_DM2_ADDR;
	coredump_desc->dm2_len = KAS_COUREDUMP_DM2_SIZE;

	coredump_desc->rm_addr = KAS_COUREDUMP_RM_ADDR;
	coredump_desc->rm_len = KAS_COUREDUMP_RM_SIZE;

	coredump_desc->kregs_addr = KAS_COUREDUMP_KREGS_ADDR;
	coredump_desc->kregs_len = KAS_COUREDUMP_KREGS_SIZE;

	kerror_stopdsp();

	/*dump PM Area*/
	kerror_dumpmem(kas_memory[0].mem_type,
			kas_memory[0].mem_start,
			kas_memory[0].mem_size,
			(u32 *)(TO_MCU_IO_DRAM(coredump_desc->pm_addr)));

	/*dump DM1 Area*/
	kerror_dumpmem(kas_memory[1].mem_type,
			kas_memory[1].mem_start,
			kas_memory[1].mem_size,
			(u32 *)(TO_MCU_IO_DRAM(coredump_desc->dm1_addr)));

	/*dump DM2 Area*/
	kerror_dumpmem(kas_memory[2].mem_type,
			kas_memory[2].mem_start,
			kas_memory[2].mem_size,
			(u32 *)(TO_MCU_IO_DRAM(coredump_desc->dm2_addr)));

	/*dump RM Area*/
	kerror_dumpmem(kas_memory[3].mem_type,
			kas_memory[3].mem_start,
			kas_memory[3].mem_size,
			(u32 *)(TO_MCU_IO_DRAM(coredump_desc->rm_addr)));

	kerror_dumpmem(KAS_RM, 0xffffc0,
			ARRAY_SIZE(kregs),
			(u32 *)(TO_MCU_IO_DRAM(coredump_desc->kregs_addr)));

	/* generate coredump of mem regions */
	to_linux_msg[0] = MSG_COREDUMP;
	to_linux_msg[1] = KAS_COREDUMP_HEAD_ADDR;
	to_linux_msg[2] = KAS_COREDUMP_HEAD_SIZE;
	msg_len = 3* sizeof(u32);

	if (audio_rpdev)
		rpmsg_send(audio_rpdev, (void *)to_linux_msg, msg_len);
}

void stop_all_streams(void)
{
	int i;

	for (i = 0; i < MAX_STREAMS; i++) {
		if (stream_contexts[i].is_created)
			stop_stream(i);
			destroy_stream(i, stream_contexts[i].channels);
	}
}

#ifdef KALIMBA_IPC_CRASH_CHECK
static void resume_stream(void)
{
	int i;

	kcm_reset();
	for (i = 0; i < MAX_STREAMS; i++) {
		if (stream_contexts[i].is_created) {
			create_stream(i, stream_contexts[i].rate, stream_contexts[i].channels,
					stream_contexts[i].buff_addr, stream_contexts[i].buff_bytes,
					stream_contexts[i].period_bytes);
			start_stream(i);
		}
	}
}

static u16  resp[5];
static u32 version;

static void vkalimba_ipc_crash_check_task(void *pvParameters)
{
	while (1) {
		if (m3_audio_reset) {
			vTaskDelay(100);
			continue;
		}
		if (kalimba_get_version_id(&version, resp) != 0) {
			xSemaphoreTake(crash_mutex, portMAX_DELAY);
			DebugMsg("Kalimba crash, resuming\n");
			iacc_tx_disable();
			clear_dmac_owner(7);
			clear_dmac_owner(8);
			clear_dmac_owner(3);
			clear_dmac_owner(9);
			disable_dmac(7);
			disable_dmac(8);
			disable_dmac(3);
			disable_dmac(9);
			SOC_REG(A7DA_CLKC_NOC_CLK_IDLEREQ_SET) = BIT2;
			while (!(SOC_REG(A7DA_CLKC_NOC_CLK_IDLEACK_STATUS) & BIT2))
				;
			SOC_REG(A7DA_CLKC_RSTC_AUDIO_DMAC3_SW_RST_CLR) = BIT2;
			vUsWait(1);
			SOC_REG (A7DA_CLKC_AUDIO_DMAC3_LEAF_CLK_EN_CLR) = BIT2;
			vUsWait(1);
			SOC_REG(A7DA_CLKC_RSTC_AUDIO_DMAC3_SW_RST_SET) = BIT2;
			vUsWait(1);
			SOC_REG (A7DA_CLKC_AUDIO_DMAC3_LEAF_CLK_EN_SET) = BIT2;
			SOC_REG(A7DA_CLKC_NOC_CLK_IDLEREQ_CLEAR) = BIT2;
			kalimba_load_firmware(NULL);
			vTaskDelay(1);
			resume_stream();
			xSemaphoreGive(crash_mutex);
		}
		vTaskDelay(100);
	}
}
#endif	/* KALIMBA_IPC_CRASH_CHECK */

void audio_protocol_pre_init(void)
{
	crash_mutex = xSemaphoreCreateMutex();
	if (crash_mutex == NULL) {
		DebugMsg("create kas crash semaphore failed!\r\n");
		return;
	}
}

void audio_protocol_init(void)
{
	audio_rpdev = rpmsg_create_device("rpmsg-audio",
			__rpmsg_audio_protocol_cb,
					0x1234, RPMSG_ADDR_ANY);
#ifdef KALIMBA_IPC_CRASH_CHECK
	/* TODO: fix this later */
	if (m3_audio_reset)
		m3_audio_reset = false;
	else
		xTaskCreate(vkalimba_ipc_crash_check_task,
			"kalimba_ipc_crash_check_task", configMINIMAL_STACK_SIZE * 4,
			NULL, tskIDLE_PRIORITY + 3, &crash_check_task);
#endif /* KALIMBA_IPC_CRASH_CHECK */
}

void audio_protocol_deinit(void)
{
	vSemaphoreDelete(crash_mutex);
}

static portBASE_TYPE audio_pipe_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	create_stream(0, 48000, 4, 0x40900000, 230400, 5760);
	start_stream(0);
	return 0;
}

const CLI_Command_Definition_t audio_pipe_testParameter =
{
	"audio_pipe_test",
	"\r\naudio_pipe_test\r\n",
	audio_pipe_test,
	0
};
const CLI_Command_Definition_t *P_KALIMBA_AUDIO_PIPE_TEST_CLI_Parameter =
		&audio_pipe_testParameter;

static portBASE_TYPE ps_update( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	u32 msg = MSG_PS_UPDATE;

	rpmsg_send(audio_rpdev, (void *)&msg, sizeof(u32));
	return 0;
}

const CLI_Command_Definition_t ps_updateParameter =
{
	"ps_update",
	"\r\nps_update\r\n",
	ps_update,
	0
};
const CLI_Command_Definition_t *P_KALIMBA_PS_UPDATE_CLI_Parameter =
		&ps_updateParameter;

static u16 lic_req_data[] = {
		0xb9ec, 0xefa6, 0x44db,
		0xcf11, 0x5a9e, 0x1220, 0xf74b,
		0xca12, 0x4207, 0xf02f, 0x7aac
};

static portBASE_TYPE lic_ctrl_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	to_linux_msg[0] = MSG_LICENSE_REQ;
	to_linux_msg[1] = DSP_REQ_DATA_BYTES;

	memcpy(&to_linux_msg[2], lic_req_data , DSP_REQ_DATA_BYTES);
	rpmsg_send(audio_rpdev, (void *)to_linux_msg,
			2 * sizeof(u32) + DSP_REQ_DATA_BYTES);
	return 0;
}

const CLI_Command_Definition_t lic_ctrl_testParameter =
{
	"lic_ctrl_test",
	"\r\nlic_ctrl_test\r\n",
	lic_ctrl_test,
	0
};
const CLI_Command_Definition_t *P_KALIMBA_LIC_CTRL_TEST_CLI_Parameter =
		&lic_ctrl_testParameter;


static portBASE_TYPE dram_free_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	u32 addr;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );

	if (pcParameter1 == NULL) {
		DebugMsg("Command error: dram_free_test <addr>\n");
		return 0;
	}
	addr = (u32)strtoul( pcParameter1, NULL, 0 );
	to_linux_msg[0] = MSG_DRAM_FREE_REQ;
	to_linux_msg[1] = addr;
	rpmsg_send(audio_rpdev, (void *)to_linux_msg, 8);
	return 0;
}

const CLI_Command_Definition_t dram_free_testParameter =
{
	"dram_free_test",
	"\r\ndram_free_test <addr>:\r\n",
	dram_free_test,
	-1
};
const CLI_Command_Definition_t *P_KALIMBA_DRAM_FREE_TEST_CLI_Parameter =
		&dram_free_testParameter;

static portBASE_TYPE dram_allocation_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	u32 size;

	pcParameter1 = (char *) FreeRTOS_CLIGetParameter(pcCommandString, 1,
			&xParameter1StringLength);

	if (pcParameter1 == NULL) {
		DebugMsg("Command error: dram_free_test <addr>\n");
		return 0;
	}
	size = (u32) strtoul(pcParameter1, NULL, 0);

	to_linux_msg[0] = MSG_DRAM_ALLOCATION_REQ;
	to_linux_msg[1] = size;
	rpmsg_send(audio_rpdev, (void *)to_linux_msg, 8);
	return 0;
}

const CLI_Command_Definition_t dram_allocation_testParameter =
{
	"dram_allocation_test",
	"\r\ndram_allocation_test <size>\r\n",
	dram_allocation_test,
	-1
};
const CLI_Command_Definition_t *P_KALIMBA_DRAM_ALLOCATION_TEST_CLI_Parameter =
		&dram_allocation_testParameter;
