/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUDIO_PROTOCOL_H_
#define AUDIO_PROTOCOL_H_

#include "ctypes.h"

struct ep_handler {
	u32 *buff_addr;
	u32 buff_length;
	u32 *read_pointer;
	u32 *write_pointer;
};

#define CHIME_DEV_ID                    15

#define MAX_STREAMS			32
#define EP_SIZE				128
#define EP_HANDLER_TOTAL_CELL		64

#define KAS_DRAM_BASE			0x4FF00000
#define KAS_DRAM_RESERVED_SIZE		0x200000

#define KAS_DATA_POINTER		KAS_DRAM_BASE
#define KAS_DATA_POINTER_SIZE		256

#define KAS_FW_START_ADDR		(KAS_DATA_POINTER + KAS_DATA_POINTER_SIZE)
#define KAS_FW_MEM_SIZE			(1 * 1024 * 1024)
#define KAS_FW_END_ADDR			(KAS_DRAM_BASE + KAS_FW_MEM_SIZE - 1)

#define HW_EP_BUFF_SIZE			(4096)

#define KAS_IACC_HW_EP_BUFF_START_ADDR	(KAS_FW_END_ADDR + 1)
#define KAS_IACC_HW_EP_BUFF_END_ADDR	(KAS_IACC_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_I2S_HW_EP_BUFF_START_ADDR		(KAS_IACC_HW_EP_BUFF_END_ADDR + 1)
#define KAS_I2S_HW_EP_BUFF_END_ADDR		(KAS_I2S_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_USP0_HW_EP_BUFF_START_ADDR		(KAS_I2S_HW_EP_BUFF_END_ADDR + 1)
#define KAS_USP0_HW_EP_BUFF_END_ADDR		(KAS_USP0_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_USP1_HW_EP_BUFF_START_ADDR		(KAS_USP0_HW_EP_BUFF_END_ADDR + 1)
#define KAS_USP1_HW_EP_BUFF_END_ADDR		(KAS_USP1_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_USP2_HW_EP_BUFF_START_ADDR		(KAS_USP1_HW_EP_BUFF_END_ADDR + 1)
#define KAS_USP2_HW_EP_BUFF_END_ADDR		(KAS_USP2_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_USP3_HW_EP_BUFF_START_ADDR		(KAS_USP2_HW_EP_BUFF_END_ADDR + 1)
#define KAS_USP3_HW_EP_BUFF_END_ADDR		(KAS_USP3_HW_EP_BUFF_START_ADDR + HW_EP_BUFF_SIZE - 1)

#define KAS_EP_HANDLER_BUFF_MEM_START_ADDR		(KAS_USP3_HW_EP_BUFF_END_ADDR + 1)
#define KAS_EP_HANDLER_BUFF_MEM_SIZE				(EP_HANDLER_TOTAL_CELL * EP_SIZE)
#define KAS_EP_HANDLER_BUFF_MEM_END_ADDR		(KAS_EP_HANDLER_BUFF_MEM_START_ADDR +	\
			KAS_EP_HANDLER_BUFF_MEM_SIZE - 1)

#define KALIMBA_BUFF_SIZE				256
#define GENERAL_MSG_BUFF				(KAS_EP_HANDLER_BUFF_MEM_END_ADDR + 1)
#define OP_MSG_BUFF					(GENERAL_MSG_BUFF + KALIMBA_BUFF_SIZE)
#define OP_RSP_BUFF					(OP_MSG_BUFF + KALIMBA_BUFF_SIZE)

#define KAS_EMERGENT_AUDIO_DATA_MEM_START_ADDR		(OP_RSP_BUFF + KALIMBA_BUFF_SIZE)
#define KAS_EMERGENT_AUDIO_DATA_MEM_END_ADDR			(KAS_DRAM_BASE + KAS_DRAM_RESERVED_SIZE - 1)

#define KAS_COREDUMP_POINTER			(KAS_FW_START_ADDR)

#define KAS_COREDUMP_HEAD_ADDR		(KAS_COREDUMP_POINTER)
#define KAS_COREDUMP_HEAD_SIZE		(1024)

#define KAS_COUREDUMP_PM_ADDR			(KAS_COREDUMP_POINTER + KAS_COREDUMP_HEAD_SIZE)
#define KAS_COUREDUMP_PM_SIZE			(0x10000 * 4)

#define KAS_COUREDUMP_DM1_ADDR			(KAS_COUREDUMP_PM_ADDR + KAS_COUREDUMP_PM_SIZE)
#define KAS_COUREDUMP_DM1_SIZE			(0x8000 * 4)

#define KAS_COUREDUMP_DM2_ADDR			(KAS_COUREDUMP_DM1_ADDR + KAS_COUREDUMP_DM1_SIZE)
#define KAS_COUREDUMP_DM2_SIZE			(0x8000 * 4)

#define KAS_COUREDUMP_RM_ADDR			(KAS_COUREDUMP_DM2_ADDR + KAS_COUREDUMP_DM2_SIZE)
#define KAS_COUREDUMP_RM_SIZE			( 0x200 * 4)

#define KAS_COUREDUMP_KREGS_ADDR		(KAS_COUREDUMP_RM_ADDR + KAS_COUREDUMP_RM_SIZE)
#define KAS_COUREDUMP_KREGS_SIZE			(0x200 * 4)
#define KAS_COREDUMP_SIZE				(KAS_FW_MEM_SIZE)
#define KAS_COREDUMP_TMP_BUF			(KAS_FW_START_ADDR + KAS_FW_MEM_SIZE - 0x20000)

void audio_protocol_pre_init(void);
void stop_all_streams(void);
void audio_protocol_init(void);
void kalimba_msg_to_m3_handler(u16 *msg);
int create_stream(u32 stream, u32 rate, u32 channels, u32 buff_addr,
		u32 buff_bytes, u32 period_bytes);
int destroy_stream(u32 stream, u32 channels);
int start_stream(u32 stream);
int stop_stream(u32 stream);
void start_chime_stream(u32 stream, u32 rate, u32 channels, u32 buff_addr,
		u32 buff_bytes, u32 period_bytes);
void stop_chime_stream(u32 stream, u32 channels);
void do_coredump(void);
#endif /* AUDIO_PROTOCOL_H_ */
