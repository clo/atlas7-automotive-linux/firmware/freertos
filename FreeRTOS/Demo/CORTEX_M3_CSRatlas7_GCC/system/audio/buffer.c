/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "io.h"
#include "debug.h"
#include "task.h"
#include "errno.h"
#include "semphr.h"
#include "ctypes.h"
#include "audio_protocol.h"

SemaphoreHandle_t ep_handler_buffer_mutex;
u32 ep_handler_buffer_use_mask[BIT_WORD(EP_HANDLER_TOTAL_CELL)];

void buffer_init(void)
{
	ep_handler_buffer_mutex = xSemaphoreCreateMutex();
	if (ep_handler_buffer_mutex == NULL) {
		configASSERT(0);
		return;
	}
}

struct ep_handler *get_ep_handler(void)
{
	int i;
	struct ep_handler *handler = NULL;

	xSemaphoreTake(ep_handler_buffer_mutex, portMAX_DELAY);
	for (i = 0; i < EP_HANDLER_TOTAL_CELL; i++) {
		if (!__test_bit(i, (volatile unsigned long *)ep_handler_buffer_use_mask)) {
			__set_bit(i, (volatile unsigned long *)ep_handler_buffer_use_mask);
			handler = (struct ep_handler *)(KAS_EP_HANDLER_BUFF_MEM_START_ADDR +
					i * EP_SIZE - SOC_SYSTEM_MAP + SOC_SYSAHB_DDRAM);
			break;
		}
	}
	xSemaphoreGive(ep_handler_buffer_mutex);
	if (handler == NULL)
		LOG("Kas Endpoint handler pool is full.\n");
	return handler;
}

void put_ep_handler(struct ep_handler *handler)
{
	int index;
	u32 offset;

	offset = (u32)handler -SOC_SYSAHB_DDRAM + SOC_SYSTEM_MAP - KAS_EP_HANDLER_BUFF_MEM_START_ADDR;
	if (offset % sizeof(struct ep_handler)) {
		LOG("Error handler address: %x\n", handler);
		return;
	}

	index = offset / EP_SIZE;
	xSemaphoreTake(ep_handler_buffer_mutex, portMAX_DELAY);
	__clear_bit(index, (volatile unsigned long *)ep_handler_buffer_use_mask);
	xSemaphoreGive(ep_handler_buffer_mutex);
}
