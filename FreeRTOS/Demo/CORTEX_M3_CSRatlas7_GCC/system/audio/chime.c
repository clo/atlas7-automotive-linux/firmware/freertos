/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "ctypes.h"
#include "debug.h"
#include "soc_irq.h"
#include "misclib.h"
#include "core_cm3.h"
#include "audio_protocol.h"
#include "image_header.h"
#include "kalimba_ipc.h"
#include "kalimba.h"
#include "buffer.h"
#include "errno.h"

#include <soc_cache.h>
#include <soc_mem.h>
#include <soc_clkc.h>
#include <soc_timer.h>

const int chime_interval[6] = {-1, 48000, 24000, 16000, 9600, 0};

static u16 *audio_addr;

/*
 * The audio data are consist of two sine blocks
 * The first block has 48000 samples, with fade in and fade out
 * The second blcok is standard sine, has 48000 samples
 * without fade in or fade out
 */
void chime_init_audio_data()
{
	u32 audio_size;

	audio_addr = (u16 *) uxGetImageAddressAndSize(IMAGE_ID_AUDIO_FILE, &audio_size);
}

static u16 get_audio_sample(int sample_index, int chime_level)
{
	if (chime_level == 5)
		return audio_addr[4800 + sample_index % 4800];

	if (sample_index - (sample_index / chime_interval[chime_level] * chime_interval[chime_level]) < 4800)
		return audio_addr[sample_index % 4800];
	else
		return 0;
}

void chime_init_buffer(int level_fl, int level_fr, int level_rl, int level_rr)
{
	u16 *buff = (u16 *)(TO_MCU_DRAM(KAS_EMERGENT_AUDIO_DATA_MEM_START_ADDR));
	int i;

	for (i = 0; i < 48000; i++) {
		buff[i * 4] = get_audio_sample(i, level_fl);
		buff[i * 4 + 1] = get_audio_sample(i, level_fr);
		buff[i * 4 + 2] = get_audio_sample(i, level_rl);
		buff[i * 4 + 3] = get_audio_sample(i, level_rr);
	}
	vDcacheFlush();
}

void chime_start_stream()
{
	int buff_size = 48000 * 4 * sizeof(u16);

	start_chime_stream(CHIME_DEV_ID, 48000, 4,
			KAS_EMERGENT_AUDIO_DATA_MEM_START_ADDR, buff_size, buff_size / 2);
}

void chime_stop_stream()
{
	stop_chime_stream(CHIME_DEV_ID, 4);
}

extern uint32_t uxChimePlayTime;

void trigger_chime(int level_fl, int level_fr, int level_rl, int level_rr)
{
	chime_init_buffer(5, 5, 5, 5);
	chime_start_stream();
	uxChimePlayTime = uxOsGetCurrentTick();
	vUsWait(200000);
	chime_stop_stream();
}

const CLI_Command_Definition_t m3_chime_testParameter;

static portBASE_TYPE m3_chime_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	BaseType_t xParameterStringLength[5];
	char *action;
	char *fll;
	char *frl;
	char *rll;
	char *rrl;
	int i;
	int test_level[4];

	action = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameterStringLength[0] );

	if (!strncmp(action, "stop", 4)) {
		chime_stop_stream();
		return 0;
	}

	fll = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameterStringLength[1] );
	frl = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameterStringLength[2] );
	rll = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameterStringLength[3] );
	rrl = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 5, &xParameterStringLength[4] );

	action[xParameterStringLength[0]] = '\0';
	fll[xParameterStringLength[1]] = '\0';
	frl[xParameterStringLength[2]] = '\0';
	rll[xParameterStringLength[3]] = '\0';
	rrl[xParameterStringLength[4]] = '\0';

	test_level[0] = (int)strtoul(fll, NULL, 0);
	test_level[1] = (int)strtoul(frl, NULL, 0);
	test_level[2] = (int)strtoul(rll, NULL, 0);
	test_level[3] = (int)strtoul(rrl, NULL, 0);
	for (i = 0; i < 4; i++) {
		if (test_level[i] < 1 || test_level[i] > 5) {
			DebugMsg("%s: the level must be 1 to 5\r\n", __func__);
			DebugMsg("%s", m3_chime_testParameter.pcHelpString);
			return 0;
		}
	}

	if (!strncmp(action, "start", 5)) {
		chime_init_buffer(test_level[0], test_level[1], test_level[2], test_level[3]);
		chime_start_stream();
	} else if (!strncmp(action, "change", 6))
		chime_init_buffer(test_level[0], test_level[1], test_level[2], test_level[3]);
	else {
		DebugMsg("%s", m3_chime_testParameter.pcHelpString);
		return 0;
	}
	return 0;
}

const CLI_Command_Definition_t m3_chime_testParameter =
{
	"m3_chime_test",
	"\r\nm3_chime_test <action> <fll> <frl> <rll> <rrl> \r\n\
                      <action>: start; stop; change\r\n\
                      <fll>: front left level, from 1 to 5\r\n\
                      <frl>: front right level, from 1 to 5\r\n\
                      <rll>: rear left level, from 1 to 5\r\n\
                      <rrl>: rear right level, from 1 to 5\r\n",
	m3_chime_test,
	-1
};
const CLI_Command_Definition_t *P_M3_CHIME_TEST_CLI_Parameter =
		&m3_chime_testParameter;
