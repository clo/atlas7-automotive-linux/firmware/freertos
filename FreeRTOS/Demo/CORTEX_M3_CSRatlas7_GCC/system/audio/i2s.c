/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* Platform includes. */
#include "ctypes.h"
#include "soc_irq.h"
#include "core_cm3.h"
#include "debug.h"
#include "errno.h"
#include "image_header.h"
#include "io.h"
#include "misclib.h"
#include "../i2c.h"
#include "cs42888.h"
#include "i2s.h"

#include <soc_clkc.h>
#include <soc_pinctrl.h>
#include <soc_cache.h>

/* #define I2S_TEST */
#define ATLAS7_EVB_BOARD

#define _A7DA_I2S_BASE				0x10D02000
#define A7DA_AUDIO_CTRL_MODE_SEL		(_A7DA_I2S_BASE + 0x0000)
#define A7DA_AUDIO_CTRL_I2S_CTRL		(_A7DA_I2S_BASE + 0x0020)
#define A7DA_AUDIO_CTRL_I2S_TX_RX_EN		(_A7DA_I2S_BASE + 0x0024)
#define A7DA_AUDIO_CTRL_I2S_TDM_CTRL		(_A7DA_I2S_BASE + 0x0030)
#define A7DA_AUDIO_CTRL_I2S_TXFIFO_OP		(_A7DA_I2S_BASE + 0x0040)
#define A7DA_AUDIO_CTRL_I2S_TXFIFO_LEV_CHK	(_A7DA_I2S_BASE + 0x0044)
#define A7DA_AUDIO_CTRL_I2S_TXFIFO_STS		(_A7DA_I2S_BASE + 0x0048)
#define A7DA_AUDIO_CTRL_I2S_TXFIFO_INT		(_A7DA_I2S_BASE + 0x004C)
#define A7DA_AUDIO_CTRL_I2S_TXFIFO_INT_MSK	(_A7DA_I2S_BASE + 0x0050)
#define A7DA_AUDIO_CTRL_I2S_RXFIFO_OP		(_A7DA_I2S_BASE + 0x00B8)
#define A7DA_AUDIO_CTRL_I2S_RXFIFO_LEV_CHK	(_A7DA_I2S_BASE + 0x00BC)
#define A7DA_AUDIO_CTRL_I2S_RXFIFO_STS		(_A7DA_I2S_BASE + 0x00C0)
#define A7DA_AUDIO_CTRL_I2S_RXFIFO_INT		(_A7DA_I2S_BASE + 0x00C4)
#define A7DA_AUDIO_CTRL_I2S_RXFIFO_INT_MSK	(_A7DA_I2S_BASE + 0x00C8)

#define I2S_LOOP_BACK				(1<<3)
#define	I2S_MCLK_DIV_SHIFT			15
#define I2S_MCLK_DIV_MASK			(0x1FF<<I2S_MCLK_DIV_SHIFT)
#define I2S_BITCLK_DIV_SHIFT			24
#define I2S_BITCLK_DIV_MASK			(0xFF<<I2S_BITCLK_DIV_SHIFT)

#define I2S_TDM_MASK_TX                 0x00F8338F
#define I2S_TDM_MASK_RX                 0x0007CC7F

#define I2S_TDM_WORD_SIZE_TX(v)         (((v) - 1) << 19)
#define I2S_TDM_WORD_SIZE_RX(v)         (((v) - 1) << 14)

#define I2S_TDM_DATA_ALIGN_TX_LEFT_J    (0 << 9)
#define I2S_TDM_DATA_ALIGN_TX_I2S       (1 << 9)

#define I2S_TDM_WORD_ALIGN_TX_LEFT_J    (0 << 7)
#define I2S_TDM_WORD_ALIGN_TX_I2S0	(1 << 7)
#define I2S_TDM_WORD_ALIGN_TX_I2S1	(2 << 7)

#define I2S_TDM_FRAME_POLARITY_HIGH	(0 << 3)
#define I2S_TDM_FRAME_POLARITY_LOW	(1 << 3)

#define I2S_TDM_FRAME_SYNC_I2S		(0 << 1)
#define I2S_TDM_FRAME_SYNC_DSP0		(1 << 1)
#define I2S_TDM_FRAME_SYNC_DSP1		(2 << 1)

/* Word alignment, record */
#define I2S_TDM_WORD_ALIGN_RX_LEFT_J	(0 << 4)
#define I2S_TDM_WORD_ALIGN_RX_I2S0	(1 << 4)
#define I2S_TDM_WORD_ALIGN_RX_I2S1	(2 << 4)
/* Bit alignment in one word, record */
#define I2S_TDM_DATA_ALIGN_RX_LEFT_J	(0 << 6)
#define I2S_TDM_DATA_ALIGN_RX_I2S	(1 << 6)

#define I2S_TDM_ENA			1

/* Codec I2S Control Register defines */
#define I2S_SLAVE_MODE			(1 << 0)
#define I2S_SIX_CHANNELS		(1 << 1)
#define I2S_L_CHAN_LEN_SHIFT		(4)
#define I2S_L_CHAN_LEN_MASK		(0x1f << I2S_L_CHAN_LEN_SHIFT)
#define I2S_FRAME_LEN_SHIFT		(9)
#define I2S_FRAME_LEN_MASK		(0x3f << I2S_FRAME_LEN_SHIFT)

#define I2S_MCLK_EN				(1<<2)
#define I2S_REF_CLK_SEL_EXT			(1<<3)
#define I2S_DOUT_OE				(1<<4)

#define I2S_TDM_ADC_CH(c)			((((c) / 2) - 1) << 10)
#define I2S_TDM_DAC_CH(c)			((((c) / 2) - 1) << 12)

#define I2S_TDM_RX_RESET			(1 << 26)
#define I2S_TDM_TX_RESET			(1 << 27)

#define AUDIO_FIFO_START			(1 << 0)
#define AUDIO_FIFO_RESET			(1 << 1)

#define I2S_RX_ENABLE				(1 << 0)
#define I2S_TX_ENABLE				(1 << 1)

#define DTO_RESL_DOUBLE				(1ULL << 29)

#ifdef I2S_TEST
#define _A7DA_DMAC3_BASE			0x10D60000
#define A7DA_DMAC3_DMA_CH_ADDR(ch) \
	(_A7DA_DMAC3_BASE + 0x00 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_XLEN(ch) \
	(_A7DA_DMAC3_BASE + 0x04 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_YLEN(ch) \
	(_A7DA_DMAC3_BASE + 0x08 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_CTRL(ch) \
	(_A7DA_DMAC3_BASE + 0x0C + ch * 0x10)

#define A7DA_DMAC3_DMA_WIDTH(ch) \
	(_A7DA_DMAC3_BASE + 0x100 + ch * 4)
#define A7DA_DMAC3_DMA_CH_VALID		(_A7DA_DMAC3_BASE + 0x140)
#define A7DA_DMAC3_DMA_CH_INT           (_A7DA_DMAC3_BASE + 0x144)
#define A7DA_DMAC3_DMA_INT_EN           (_A7DA_DMAC3_BASE + 0x148)
#define A7DA_DMAC3_DMA_INT_EN_CLR       (_A7DA_DMAC3_BASE + 0x14C)
#define A7DA_DMAC3_DMA_CH_LOOP_CTRL	(_A7DA_DMAC3_BASE + 0x158)
#define A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR	(_A7DA_DMAC3_BASE + 0x15C)
#define A7DA_DMAC3_DMA_INT_OWNER_REG0	(_A7DA_DMAC3_BASE + 0x164)
#define A7DA_DMAC3_DMA_INT_OWNER_REG1	(_A7DA_DMAC3_BASE + 0x168)

#define I2S_TX						2
#define I2S_RX						1

#define DMA_DEV_TO_MEM					0
#define DMA_MEM_TO_DEV					1
#endif

static void sirf_i2s_tx_enable(void)
{
	/* Reset TDM playback logic */
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TDM_CTRL,
			I2S_TDM_TX_RESET, I2S_TDM_TX_RESET);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TDM_CTRL,
			I2S_TDM_TX_RESET, 0);

	/* First start the FIFO, then enable the tx/rx */
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TXFIFO_OP,
		AUDIO_FIFO_RESET, AUDIO_FIFO_RESET);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TXFIFO_OP,
		AUDIO_FIFO_RESET, 0);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TXFIFO_OP,
		AUDIO_FIFO_START, AUDIO_FIFO_START);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TX_RX_EN,
		I2S_TX_ENABLE | I2S_DOUT_OE,
		I2S_TX_ENABLE | I2S_DOUT_OE);
}

static void sirf_i2s_tx_disable(void)
{
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TX_RX_EN,
		I2S_TX_ENABLE, 0);
	/* First disable the tx/rx, then stop the FIFO */
	SOC_REG(A7DA_AUDIO_CTRL_I2S_TXFIFO_OP) = 0;
}

static void sirf_i2s_rx_enable(void)
{
	/* Reset TDM record logic */
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TDM_CTRL,
			I2S_TDM_RX_RESET, I2S_TDM_RX_RESET);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TDM_CTRL,
			I2S_TDM_RX_RESET, 0);

	/* First start the FIFO, then enable the tx/rx */
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_RXFIFO_OP,
		AUDIO_FIFO_RESET, AUDIO_FIFO_RESET);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_RXFIFO_OP,
		AUDIO_FIFO_RESET, 0);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_RXFIFO_OP,
		AUDIO_FIFO_START, AUDIO_FIFO_START);
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TX_RX_EN,
		I2S_RX_ENABLE, I2S_RX_ENABLE);
}

static void sirf_i2s_rx_disable(void)
{
	REG_UPDATE_BITS(A7DA_AUDIO_CTRL_I2S_TX_RX_EN,
		I2S_RX_ENABLE, 0);
	/* First disable the tx/rx, then stop the FIFO */
	SOC_REG(A7DA_AUDIO_CTRL_I2S_RXFIFO_OP) = 0;
}

/* TODO: Remove this function after Kalimba takes over the job */
void sirf_i2s_start(int playback)
{
	if (playback)
		sirf_i2s_tx_enable();
	else
		sirf_i2s_rx_enable();
}

/* TODO: Remove this function after Kalimba takes over the job */
void sirf_i2s_stop(int playback)
{
	if (playback)
		sirf_i2s_tx_disable();
	else
		sirf_i2s_rx_disable();
}

static u32 __div64_32(u64 *n, u32 base)
{
	u64 rem = *n;
	u64 b = base;
	u64 res, d = 1;
	u32 high = rem >> 32;

	/* Reduce the thing a bit first */
	res = 0;
	if (high >= base) {
		high /= base;
		res = (u64) high << 32;
		rem -= (u64) (high*base) << 32;
	}

	while ((INT64)b > 0 && b < rem) {
		b = b+b;
		d = d+d;
	}

	do {
		if (rem >= b) {
			rem -= b;
			res += d;
		}
		b >>= 1;
		d >>= 1;
	} while (d);

	*n = res;
	return rem;
}

#define I2S_SOURCE_CLK_RATE			1200000000
static void sirf_i2s_set_clk_path_div(int sysclk)
{
	u64 dividend = (u64)(sysclk * DTO_RESL_DOUBLE);
	__div64_32(&dividend,I2S_SOURCE_CLK_RATE);

	SOC_REG(A7DA_CLKC_AUDIO_DTO_SRC) = 1;
	SOC_REG(A7DA_CLKC_I2S_CLK_SEL) = 2;
	SOC_REG(A7DA_CLKC_AUDIO_DTO_DROFF) = 0;
	SOC_REG(A7DA_CLKC_AUDIO_DTO_INC) = (u32)dividend;
}

int sirf_i2s_params_adv(int channels, int rate, int slave, int playback)
{
	u32 i2s_ctrl = 0;
	u32 i2s_tx_rx_en = 0;
	u32 tdm_ctrl = 0;
	u32 tdm_mask;
	u32 left_len, frame_len;
	u32 bclk_div, bclk_ratio;
	u32 sysclk;

	sysclk = rate * 1024;
	left_len = 16;
	frame_len = left_len * 2;
	bclk_ratio = rate * frame_len;

	sirf_i2s_set_clk_path_div(sysclk * 2);

	switch (channels) {
	case 2:
		i2s_ctrl &= ~I2S_SIX_CHANNELS;
		bclk_ratio = 32;
		break;
	case 4:
		tdm_ctrl = SOC_REG(A7DA_AUDIO_CTRL_I2S_TDM_CTRL);
		if (playback) {
			tdm_mask = I2S_TDM_MASK_TX;
			tdm_ctrl &= ~tdm_mask;
			tdm_ctrl |= (I2S_TDM_WORD_SIZE_TX(32))
				|(I2S_TDM_DAC_CH(channels))
				| I2S_TDM_DATA_ALIGN_TX_LEFT_J
				| I2S_TDM_WORD_ALIGN_TX_LEFT_J
				| I2S_TDM_FRAME_POLARITY_LOW
				| I2S_TDM_FRAME_SYNC_DSP0;
		} else {
			tdm_mask = I2S_TDM_MASK_RX;
			tdm_ctrl &= ~tdm_mask;
			tdm_ctrl |= (I2S_TDM_WORD_SIZE_RX(32))
				|(I2S_TDM_ADC_CH(channels))
				|I2S_TDM_DATA_ALIGN_RX_LEFT_J
				|I2S_TDM_WORD_ALIGN_RX_I2S0
				|I2S_TDM_FRAME_POLARITY_HIGH
				|I2S_TDM_FRAME_SYNC_DSP0;
		}
		tdm_ctrl |= I2S_TDM_ENA;
		bclk_ratio = 128;
		break;
	case 6:
		i2s_ctrl |= I2S_SIX_CHANNELS;
		break;
	case 8:
		tdm_ctrl = SOC_REG(A7DA_AUDIO_CTRL_I2S_TDM_CTRL);
		if (playback) {
			tdm_mask = I2S_TDM_MASK_TX;
			tdm_ctrl &= ~tdm_mask;
			tdm_ctrl |= (I2S_TDM_WORD_SIZE_TX(32))
					|(I2S_TDM_DAC_CH(channels))
					|I2S_TDM_DATA_ALIGN_TX_LEFT_J
					|I2S_TDM_WORD_ALIGN_TX_I2S0
					|I2S_TDM_FRAME_POLARITY_HIGH
					|I2S_TDM_FRAME_SYNC_DSP0;
		} else {
			tdm_mask = I2S_TDM_MASK_RX;
			tdm_ctrl &= ~tdm_mask;
			tdm_ctrl |= (I2S_TDM_WORD_SIZE_RX(32))
					|(I2S_TDM_ADC_CH(channels))
					|I2S_TDM_DATA_ALIGN_RX_LEFT_J
					|I2S_TDM_WORD_ALIGN_RX_I2S0
					|I2S_TDM_FRAME_POLARITY_HIGH
					|I2S_TDM_FRAME_SYNC_DSP0;
		}
		tdm_ctrl |= I2S_TDM_ENA;
		bclk_ratio = 256;
		break;
	default:
		DebugMsg("%d channels unsupported\n", channels);
		return -EINVAL;
	}

	if (!slave) {
		i2s_ctrl &= ~I2S_SLAVE_MODE;
		bclk_div = (sysclk / (rate * bclk_ratio * 2)) - 1;

		if (!bclk_div) {
			DebugMsg("bclk div %d  error\n", bclk_div);
			return -EINVAL;
		}
		i2s_ctrl |= (bclk_div << I2S_BITCLK_DIV_SHIFT);
	} else
		i2s_ctrl |= I2S_SLAVE_MODE;

	i2s_ctrl |= ((frame_len - 1) << I2S_FRAME_LEN_SHIFT)
		| ((left_len - 1) << I2S_L_CHAN_LEN_SHIFT);

	i2s_tx_rx_en = SOC_REG(A7DA_AUDIO_CTRL_I2S_TX_RX_EN);
	i2s_tx_rx_en &= ~I2S_REF_CLK_SEL_EXT;
	i2s_tx_rx_en |= I2S_MCLK_EN;

	SOC_REG(A7DA_AUDIO_CTRL_I2S_CTRL) = i2s_ctrl;
	SOC_REG(A7DA_AUDIO_CTRL_I2S_TX_RX_EN) = i2s_tx_rx_en;
	SOC_REG(A7DA_AUDIO_CTRL_I2S_TDM_CTRL) =  tdm_ctrl;
	return 0;
}

#ifdef I2S_TEST
static void set_dmac_owner(int ch)
{
	if (ch > 15)
		return;
	if (ch < 10)
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG0, 7 << (ch * 3),
				2 << (ch * 3));
	else
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG1, 7 << ((ch  - 10) * 3),
				2 << ((ch - 10) * 3));
}

static void enable_dmac(int ch, u32 addr, int buf_len, int dir)
{
	int i, ylen, width = 0, period_len = buf_len / 2;

	addr += SOC_SYSTEM_MAP;
	for (i = 1024; i >= 16; i /= 2) {
		if (!(period_len % i)) {
			width = i / 4;
			break;
		}
	}
	if (width == 0) {
		DebugMsg("Invalid buffer size\n");
		return;
	}
	ylen = buf_len / (width * 4) - 1;

	dir = !!dir;

	/* Clear pending interrupt */
	SOC_REG(A7DA_DMAC3_DMA_CH_INT) = BIT(ch);

	/* Configure DMA channel */
	SOC_REG(A7DA_DMAC3_DMA_WIDTH(ch)) = width;
	/* Burst mode */
	SOC_REG(A7DA_DMAC3_DMA_CH_CTRL(ch)) = ch | BIT(4) | (dir << 5);
	SOC_REG(A7DA_DMAC3_DMA_CH_XLEN(ch)) = 0;
	SOC_REG(A7DA_DMAC3_DMA_CH_YLEN(ch)) = ylen;

	/* Start DMA */
	SOC_REG(A7DA_DMAC3_DMA_CH_ADDR(ch)) = (addr >> 2);
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_LOOP_CTRL, BIT(ch) | BIT(ch + 16),
			BIT(ch) | BIT(ch + 16));
}

static void disable_dmac(int ch)
{
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR, BIT(ch) | BIT(ch + 16),
				BIT(ch) | BIT(ch + 16));
}

short __on_dram audio_buffer_mono[48] = {
		0, 3422, 6784, 10033, 13105, 15962, 18531, 20804,
		22696, 24224, 25317, 25992, 26214, 25990, 25323, 24215,
		22707, 20792, 18541, 15957, 13107, 10032, 6784, 3423,
		-2, -3418, -6789, -10029, -13108, -15960, -18533, -20801,
		-22699, -24221, -25320, -25991, -26213, -25992, -25319, -24221,
		-22701, -20798, -18536, -15959, -13105, -10035, -6782, -3425
};

short __on_dram audio_buffer_stereo[96];

static void i2s_playback_test(int slave, int playback)
{
	int i;

	for (i = 0; i < 48; i++) {
		audio_buffer_stereo[i * 2] = audio_buffer_mono[i];
		audio_buffer_stereo[i * 2 + 1] = audio_buffer_mono[i];
	}
	vDcacheFlush();

	SOC_REG(A7DA_CLKC_AUDIO_DMAC3_LEAF_CLK_EN_SET) = BIT0;

	set_dmac_owner(I2S_TX);
	enable_dmac(I2S_TX, (u32)audio_buffer_stereo,
				192, DMA_MEM_TO_DEV);
	sirf_i2s_params_adv(2, 48000, slave, playback);
	sirf_i2s_start(playback);
}

static void dump(void)
{
	DebugMsg("\033[32m\033[1maudio buff addr: 0x%08x\r\n\033[0m",
			(u32)audio_buffer_stereo + SOC_SYSTEM_MAP);
	DebugMsg("\033[32m\033[1mDMA ownnership: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_INT_OWNER_REG0));
	DebugMsg("\033[32m\033[1mDMA loop ctrl: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_LOOP_CTRL));
	DebugMsg("\033[31m\033[1mDMA channel: %d\r\n\033[0m", I2S_TX);
	DebugMsg("\033[32m\033[1mWIDTH: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_WIDTH(I2S_TX)));
	DebugMsg("\033[32m\033[1mXLEN: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_CH_XLEN(I2S_TX)));
	DebugMsg("\033[32m\033[1mYLEN: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_CH_YLEN(I2S_TX)));
	DebugMsg("\033[32m\033[1mADDR: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_CH_ADDR(I2S_TX)));
	DebugMsg("\033[32m\033[1mCTRL: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_CH_CTRL(I2S_TX)));
}

#endif

#ifdef ATLAS7_EVB_BOARD
#define SX1506Q_SLAVE_ADDR			0x20

const static struct i2c_msg sx1506q_init_data[12] = {
	{0xad, 0x04},
	{0x05, 0x00},
	{0x04, 0x00},
	{0x07, 0xff},
	{0x06, 0xff},
	{0x21, 0x00},
	{0x20, 0x00},
	{0x01, 0},
	{0x03, 0xFE},
	{0x00, 0x42},
	{0x02, 0xBC},
	{0x01, 1},
};

/*
 * The Atals7 EVB board uses an extand gpio chip(sx1506q)
 * to select the i2s routing.
 *              ---------------
 *             |               |--- i2s LD port
 * IO8 --------|               |--- i2s VP port
 * IO9 --------|               |--- i2s AU port
 *              ---------------
 */
void init_sx1506q(void)
{
	int i;

	i2c_init(A7DA_I2C0);
	for (i = 0; i < ARRAY_SIZE(sx1506q_init_data); i++)
		i2c_xfer_msg(A7DA_I2C0, SX1506Q_SLAVE_ADDR, 0, (u8 *)&sx1506q_init_data[i], 2);
	i2c_deinit(A7DA_I2C0);
}
#else
void init_sx1506q(void)
{
}
#endif

static void i2s_pinctrl_set(void)
{
	uxPinctrlPinFuncSel(I2S_MCLK_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_BCLK_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_WS_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_DOUT0_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_DOUT1_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_DOUT2_PIN, FUNC_I2S);
	uxPinctrlPinFuncSel(I2S_DIN_PIN, FUNC_I2S);
}

void i2s_setup(int channels, int rate, int slave, int playback)
{
	i2s_pinctrl_set();
	clkc_enable_i2s();
#ifdef I2S_TEST
	i2s_playback_test(slave, playback);
	dump();
#endif
}
