/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* Platform includes. */
#include "ctypes.h"
#include "debug.h"
#include "errno.h"
#include "image_header.h"
#include "io.h"
#include "misclib.h"
#include "iacc.h"

#include <soc_clkc.h>
#include <soc_iacc.h>

//#define IACC_TEST

#ifdef IACC_TEST
#define _A7DA_DMAC3_BASE					0x10D60000
#define A7DA_DMAC3_DMA_CH_ADDR(ch) \
	(_A7DA_DMAC3_BASE + 0x00 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_XLEN(ch) \
	(_A7DA_DMAC3_BASE + 0x04 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_YLEN(ch) \
	(_A7DA_DMAC3_BASE + 0x08 + ch * 0x10)
#define A7DA_DMAC3_DMA_CH_CTRL(ch) \
	(_A7DA_DMAC3_BASE + 0x0C + ch * 0x10)

#define A7DA_DMAC3_DMA_WIDTH(ch) \
	(_A7DA_DMAC3_BASE + 0x100 + ch * 4)
#define A7DA_DMAC3_DMA_CH_VALID			(_A7DA_DMAC3_BASE + 0x140)
#define A7DA_DMAC3_DMA_CH_INT           (_A7DA_DMAC3_BASE + 0x144)
#define A7DA_DMAC3_DMA_INT_EN           (_A7DA_DMAC3_BASE + 0x148)
#define A7DA_DMAC3_DMA_INT_EN_CLR       (_A7DA_DMAC3_BASE + 0x14C)
#define A7DA_DMAC3_DMA_CH_LOOP_CTRL		(_A7DA_DMAC3_BASE + 0x158)
#define A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR	(_A7DA_DMAC3_BASE + 0x15C)
#define A7DA_DMAC3_DMA_INT_OWNER_REG0	(_A7DA_DMAC3_BASE + 0x164)
#define A7DA_DMAC3_DMA_INT_OWNER_REG1	(_A7DA_DMAC3_BASE + 0x168)

#define CH_IACC_TX0						7
#define CH_IACC_TX1						8
#define CH_IACC_TX2						3
#define CH_IACC_TX3						9

#define DMA_DEV_TO_MEM					0
#define DMA_MEM_TO_DEV					1
#endif

void iacc_tx_enable(int format, int channels)
{
	int i;

	if (format == FORMAT_16BIT)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_MODE_CTRL, TX_24BIT, 0);
	else if (format == FORMAT_24BIT)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_MODE_CTRL, TX_24BIT, 1);

	if (channels == 4)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_MODE_CTRL,
				TX_SYNC_EN | TX_START_SYNC_EN,
				TX_SYNC_EN | TX_START_SYNC_EN);

	for (i = 0; i < channels; i++) {
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TX_RX_EN, DAC_EN << i,
				DAC_EN << i);
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TXFIFO0_OP + (i * 20),
				FIFO_RESET, FIFO_RESET);
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TXFIFO0_OP + (i * 20),
				FIFO_RESET, ~FIFO_RESET);

		SOC_REG(A7DA_IACC_INTCODECCTL_TXFIFO0_INT_MSK + (i * 20)) =
				0;
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TXFIFO0_OP + (i * 20),
				FIFO_START, FIFO_START);
	}
}

void iacc_tx_disable(void)
{
	int i;

	for (i = 0; i < 4; i++) {
		SOC_REG(A7DA_IACC_INTCODECCTL_TXFIFO0_OP + (i * 20)) = 0;
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TX_RX_EN, DAC_EN << i, 0);
	}
}

void iacc_rx_enable(int format, int channels)
{
	int i;
	u32 rx_dma_ctrl = 0;

	if (format == FORMAT_16BIT)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_MODE_CTRL, RX_24BIT, 0);
	else if (format == FORMAT_24BIT)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_MODE_CTRL, RX_24BIT, RX_24BIT);

	for (i = 0; i < channels; i++) {
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TX_RX_EN, ADC_EN << i,
				ADC_EN << i);
		rx_dma_ctrl |= (1 << i);
	}
	REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_RXFIFO0_OP,
		RX_DMA_CTRL_MASK, rx_dma_ctrl << RX_DMA_CTRL_SHIFT);
}

void atlas7_rx_disable(void)
{
	int i;

	for (i = 0; i < 2; i++)
		REG_UPDATE_BITS(A7DA_IACC_INTCODECCTL_TX_RX_EN, ADC_EN << i, 0);
}

/* TODO: Remove this function after Kalimba takes over the job */
void iacc_start(int playback, int channels)
{
	if (playback)
		iacc_tx_enable(FORMAT_16BIT, channels);
	else
		iacc_rx_enable(FORMAT_16BIT, channels);
}

/* TODO: Remove this function after Kalimba takes over the job */
void iacc_stop(int playback)
{
	if (playback)
		iacc_tx_disable();
	else
		atlas7_rx_disable();
}


#ifdef IACC_TEST
static void set_dmac_owner(int ch)
{
	if (ch > 15)
		return;
	if (ch < 10)
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG0, 7 << (ch * 3),
				2 << (ch * 3));
	else
		REG_UPDATE_BITS(A7DA_DMAC3_DMA_INT_OWNER_REG1, 7 << ((ch  - 10) * 3),
				2 << ((ch - 10) * 3));
}

static void enable_dmac(int ch, u32 addr, int buf_len, int dir)
{
	int i, ylen, width = 0, period_len = buf_len / 2;

	addr += SOC_SYSTEM_MAP;
	for (i = 1024; i >= 16; i /= 2) {
		if (!(period_len % i)) {
			width = i / 4;
			break;
		}
	}
	if (width == 0) {
		DebugMsg("Invalid buffer size\n");
		return;
	}
	ylen = buf_len / (width * 4) - 1;

	dir = !!dir;

	/* Clear pending interrupt */
	SOC_REG(A7DA_DMAC3_DMA_CH_INT) = BIT(ch);

	/* Configure DMA channel */
	SOC_REG(A7DA_DMAC3_DMA_WIDTH(ch)) = width;
	/* Burst mode */
	SOC_REG(A7DA_DMAC3_DMA_CH_CTRL(ch)) = ch | BIT(4) | (dir << 5);
	SOC_REG(A7DA_DMAC3_DMA_CH_XLEN(ch)) = 0;
	SOC_REG(A7DA_DMAC3_DMA_CH_YLEN(ch)) = ylen;

	/* Start DMA */
	SOC_REG(A7DA_DMAC3_DMA_CH_ADDR(ch)) = (addr >> 2);
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_LOOP_CTRL, BIT(ch) | BIT(ch + 16),
			BIT(ch) | BIT(ch + 16));
}

static void disable_dmac(int ch)
{
	REG_UPDATE_BITS(A7DA_DMAC3_DMA_CH_LOOP_CTRL_CLR, BIT(ch) | BIT(ch + 16),
				BIT(ch) | BIT(ch + 16));
}

static const int playback_dma_ch[4] = {
		CH_IACC_TX0,
		CH_IACC_TX1,
		CH_IACC_TX2,
		CH_IACC_TX3
};

static const short audio_buffer[48] = {
		0, 3422, 6784, 10033, 13105, 15962, 18531, 20804,
		22696, 24224, 25317, 25992, 26214, 25990, 25323, 24215,
		22707, 20792, 18541, 15957, 13107, 10032, 6784, 3423,
		-2, -3418, -6789, -10029, -13108, -15960, -18533, -20801,
		-22699, -24221, -25320, -25991, -26213, -25992, -25319, -24221,
		-22701, -20798, -18536, -15959, -13105, -10035, -6782, -3425
};

static void iacc_playback_test(void)
{
	int i;

	SOC_REG(A7DA_CLKC_AUDIO_DMAC3_LEAF_CLK_EN_SET) = BIT0;

	for (i = 0; i < 4; i++)
		set_dmac_owner(playback_dma_ch[i]);
	for (i = 0; i < 4; i++)
		enable_dmac(playback_dma_ch[i], (u32)audio_buffer,
				96, DMA_MEM_TO_DEV);
	iacc_tx_enable(FORMAT_16BIT, 4);
}

static void dump(void)
{
	int i;

	DebugMsg("\033[32m\033[1maudio buff addr: 0x%08x\r\n\033[0m",
			(u32)audio_buffer + SOC_SYSTEM_MAP);
	DebugMsg("\033[32m\033[1mDMA ownnership: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC3_DMA_INT_OWNER_REG0));
	DebugMsg("\033[32m\033[1mDMA loop ctrl: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_LOOP_CTRL));
	for (i = 0; i < 4; i++) {
		DebugMsg("\033[31m\033[1mDMA channel: %d\r\n\033[0m", i);
		DebugMsg("\033[32m\033[1mWIDTH: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_WIDTH(playback_dma_ch[i])));
		DebugMsg("\033[32m\033[1mXLEN: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_XLEN(playback_dma_ch[i])));
		DebugMsg("\033[32m\033[1mYLEN: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_YLEN(playback_dma_ch[i])));
		DebugMsg("\033[32m\033[1mADDR: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_ADDR(playback_dma_ch[i])));
		DebugMsg("\033[32m\033[1mCTRL: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC3_DMA_CH_CTRL(playback_dma_ch[i])));
	}
}

#endif

void iacc_setup(void)
{
	clkc_enable_iacc();
#ifdef IACC_TEST
	iacc_playback_test();
	dump();
#endif
}
