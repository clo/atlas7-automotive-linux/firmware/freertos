/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Platform includes. */
#include "ctypes.h"
#include "debug.h"
#include "errno.h"
#include "image_header.h"
#include "io.h"
#include "misclib.h"
#include "task.h"
#include "kalimba.h"
#include "kalimba_ipc.h"

#include "audio_protocol.h"
#include <soc_clkc.h>
#include <soc_kas.h>
#include <soc_timer.h>

/*#define FW_CHECK*/

struct firmware_code_head {
	int code_size;
	int pm_unpacker_offset;
	int pm_offset;
	int dm1_offset;
	int dm2_offset;
	int const16_offset;
	int const32_offset;
};

struct firmware_pm_unpacker_image {
	/* Start address of parameter block in DM2 */
	u32 params_start_addr;
	/* Start address to load block in PM */
	u32 pm_unpacker_start_addr;
	/* pm unpacker size */
	u32 pm_unpacker_size;
	/* PM program image data */
	u32 pm_unpacker_code;
};

struct firmware_pm_dm_block {
	u32 start_addr;
	u32 size;
	u32 data;
};

struct firmware_code {
	struct firmware_code_head head;
	void *code;
};

#ifdef FW_CHECK
static void fw_check_unpacker(u32 *pm_unpacker, u32 pm_unpacker_start_addr,
		u32 size)
{
	int i;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = pm_unpacker_start_addr
			| (0x3 << 30);
	for (i = 0; i < size; i++) {
		if (SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) != pm_unpacker[i]) {
			DebugMsg(
					"\033[31m\033[1m%s: Check pm unpacker failed: %d\n\033[0m",
					__func__, i);
			return;
		}
	}
	DebugMsg("\033[32m\033[1m%s: Check pm unpacker success\n\033[0m",
			__func__);
}

static void fw_check_pm(u32 *pm, u32 pm_start_addr, u32 size)
{
	int i;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = pm_start_addr
			| (0x3 << 30);

	for (i = 0; i < size; i++) {
		if (SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) != pm[i]) {
			DebugMsg(
					"\033[31m\033[1m%s: Check pm failed: %d\n\033[0m",
					__func__, i);
			return;
		}
	}
	DebugMsg("\033[32m\033[1m%s: Check pm success\n\033[0m", __func__);
}

static void fw_check_dm(u32 *dm, u32 dm_start_addr, u32 size)
{
	int i;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (dm_start_addr << 2)
			| (0x2 << 30);

	for (i = 0; i < size; i++) {
		if (SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) != dm[i]) {
			DebugMsg(
					"\033[31m\033[1m%s: Check dm failed: %d\n\033[0m",
					__func__, i);
			return;
		}
	}
	DebugMsg("\033[32m\033[1m%s: Check dm success\n\033[0m", __func__);
}

static void fw_info_dump(u32 *fw_data, struct firmware_code *code)
{
	u32 const_addr;

	DebugMsg("\033[32m\033[1m fw addr: 0x%08x\n\033[0m", fw_data);
	DebugMsg("\033[32m\033[1m fw size: %d\n\033[0m", code->head.code_size);
	DebugMsg("\033[32m\033[1m unpacker offset: 0x%08x\n\033[0m",
			code->head.pm_unpacker_offset);
	DebugMsg("\033[32m\033[1m pm offset: 0x%08x\n\033[0m",
				code->head.pm_offset);
	DebugMsg("\033[32m\033[1m dm1 offset: 0x%08x\n\033[0m",
				code->head.dm1_offset);
	DebugMsg("\033[32m\033[1m dm2 offset: 0x%08x\n\033[0m",
				code->head.dm2_offset);
	DebugMsg("\033[32m\033[1m const16 offset: 0x%08x\n\033[0m",
				code->head.const16_offset);
	DebugMsg("\033[32m\033[1m const32 offset: 0x%08x\n\033[0m",
					code->head.const32_offset);

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_ADDR_CONST16 << 2)
			| (0x2 << 30);
	const_addr = (SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) << 24);
	const_addr |= SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
	DebugMsg("\033[32m\033[1m const16 addr: 0x%08x\n\033[0m", const_addr);

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_ADDR_CONST32 << 2)
			| (0x2 << 30);
	const_addr = (SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) << 24);
	const_addr |= SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
	DebugMsg("\033[32m\033[1m const32 addr: 0x%08x\n\033[0m", const_addr);
}
#endif

static inline INT32 swapInt32(INT32 value)
{
	 return ((value & 0x000000FF) << 24) |
		 ((value & 0x0000FF00) << 8) |
		((value & 0x00FF0000) >> 8) |
		((value & 0xFF000000) >> 24);
}

static u32 firmware_download_pm_unpacker(struct firmware_code *code,
		u32 *pm_unpacker_start_addr)
{
	u32 i;
	struct firmware_pm_unpacker_image *pm_unpacker_image;
	u32 size;
	u32 *pm_unpacker_code;

	pm_unpacker_image = code->code + code->head.pm_unpacker_offset;
	*pm_unpacker_start_addr = pm_unpacker_image->pm_unpacker_start_addr;
	size = pm_unpacker_image->pm_unpacker_size;
	pm_unpacker_code = &pm_unpacker_image->pm_unpacker_code;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = *pm_unpacker_start_addr
			| (0x3 << 30);
	for (i = 0; i < size; i++)
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) =
				pm_unpacker_code[i];
#ifdef FW_CHECK
	fw_check_unpacker(pm_unpacker_code, *pm_unpacker_start_addr, size);
#endif
	return pm_unpacker_image->params_start_addr;
}

static void firmware_init_dma(u32 transfer_mode)
{
	SOC_REG(A7DA_KAS_DMAC_DMA_VALID) = 1;
	SOC_REG(A7DA_KAS_DMA_MODE) |= KAS_RESET_DMA_CLIENT;
	SOC_REG(A7DA_KAS_DMA_MODE) &= ~KAS_RESET_DMA_CLIENT;

	/* Set KAS DMA as Single transaction */
	SOC_REG(A7DA_KAS_DMA_MODE) &= ~KAS_DMA_CHAIN_MODE;

	SOC_REG(A7DA_KAS_TRANSLATE) = transfer_mode;
	SOC_REG(A7DA_KAS_DMAC_DMA_CTRL) = KAS_DMAC_TRANS_MEM_TO_FIFO;
	SOC_REG(A7DA_KAS_DMAC_DMA_YLEN) = 0;
	SOC_REG(A7DA_KAS_DMAC_DMA_INT_EN) = 0;
	SOC_REG(A7DA_KAS_DMA_INC) = 0;
	SOC_REG(A7DA_KAS_DMAC_DMA_INT) = 0xFFFFFFFF;
}

static void firmware_load_pm_though_keyhole(u32 start_addr, u32 size_bytes,
		void *data)
{
	u32 i;
	u32 *code = (u32 *) data;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = start_addr | (0x3 << 30);
	for (i = 0; i < size_bytes / 4; i++)
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = code[i];
}

static void firmware_load_pm_though_dma(struct firmware_code *code,
		u32 start_addr, u32 size_bytes, void *data)
{
	void *pm_code_dma_addr;

	pm_code_dma_addr = (void *) (SOC_SYSTEM_MAP + data);

	while (!(SOC_REG(A7DA_KAS_DMA_STATUS) & KAS_DMAC_IDLE))
		;

	SOC_REG(A7DA_KAS_DMAC_DMA_XLEN) = size_bytes / 4;
	SOC_REG(A7DA_KAS_DMAC_DMA_WIDTH) = size_bytes / 4;
	SOC_REG(A7DA_KAS_DMA_ADDR) = start_addr / 4;
	SOC_REG(A7DA_KAS_DMAC_DMA_ADDR) = (u32) pm_code_dma_addr;

	while (!(SOC_REG(A7DA_KAS_DMAC_DMA_INT) & KAS_DMAC_FINISH_INT))
		;

	SOC_REG(A7DA_KAS_DMAC_DMA_INT) |= KAS_DMAC_FINISH_INT;
}

static void firmware_run_pm(u32 start_addr)
{
	if (!(start_addr >= KAS_PM_SRAM_START_ADDR
			&& start_addr <= KAS_PM_SRAM_END_ADDR)) {
		DebugMsg("%s: the start address(0x%x) is not correct.\n",
				__func__, start_addr);
		return;
	}
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_DEBUG << 2)
			| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;

	/* Set KAS program counter */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_REGFILE_PC << 2)
			| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = start_addr;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_DEBUG << 2)
			| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_RUN;
}

static void firmware_run_pm_unpacker(u32 dm_block_src, u32 pm_block_dest,
		u32 pm_bytes_len, u32 params_start_addr,
		u32 pm_unpacker_image_start_addr)
{

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_DEBUG << 2)
			| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) =
			(PM_UNPACKER_PARAMS_DM_SRC(params_start_addr) << 2)
					| (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = dm_block_src;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = pm_block_dest / 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = pm_bytes_len / 4;
	/* IPC_TRGT.lo16 */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = 0;
	/* IPC_TRGT.hi16 */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = 0;
	/* UNPACK_STATUS */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = 0;

	/* Set KAS program counter */
	firmware_run_pm(pm_unpacker_image_start_addr);

	/* Wait PM-unpacker complete */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 0;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) =
			(PM_UNPACKER_PARAMS_UNPACK_STATUS(params_start_addr)
					<< 2) | (0x2 << 30);
	while (!(SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) & 1))
		;
}

static void firmware_download_pm(struct firmware_code *code)
{
	u32 i;
	u32 pm_unpacker_start_addr;
	u32 params_start_addr;
	void *pm_image = code->code + code->head.pm_offset;
	u32 block_num = ((u32 *) pm_image)[0];
	struct firmware_pm_dm_block *pm_block_ptr = pm_image + sizeof(u32);

	/* Stop pm running */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_DEBUG << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;

	params_start_addr = firmware_download_pm_unpacker(code,
			&pm_unpacker_start_addr);

	firmware_init_dma(KAS_TRANSLATE_24BIT);

	/* The KAS's DMA must transfer multiples-of-12 bytes */
	for (i = 0; i < block_num; i++) {
		u32 current_start_addr;
		int remaining_bytes;
		void *data = &pm_block_ptr->data;

		current_start_addr = pm_block_ptr->start_addr;
		remaining_bytes = pm_block_ptr->size * 4;
		while (remaining_bytes > 0) {
			if (remaining_bytes < 12) {
				/* If remaining_bytes is less than 12 bytes,
				 * use the keyhole to download
				 */
				firmware_load_pm_though_keyhole(
						current_start_addr,
						remaining_bytes, data);
				remaining_bytes = 0;
			} else if (current_start_addr
					>= DOWN_ALIGN_12BYTES(
						pm_unpacker_start_addr)) {
				/* If the start address overlaps the
				 * PM-unpacker area,use the keyhole to download
				 */
				firmware_load_pm_though_keyhole(
						current_start_addr,
						remaining_bytes, data);
				remaining_bytes = 0;
			} else {
				u32 dma_transfer_bytes;
				/* Round-up to multiples-of-12 bytes */
				dma_transfer_bytes =
					UP_ALIGN_12BYTES(
					remaining_bytes) >
					DOWN_ALIGN_12BYTES(KAS_DM1_SRAM_BYTES)
					?
					DOWN_ALIGN_12BYTES(
						KAS_DM1_SRAM_BYTES) :
					UP_ALIGN_12BYTES(
						remaining_bytes);
				if ((current_start_addr + dma_transfer_bytes)
						>= DOWN_ALIGN_12BYTES(
						pm_unpacker_start_addr)) {
					dma_transfer_bytes =
					DOWN_ALIGN_12BYTES(
							pm_unpacker_start_addr)
							- current_start_addr;
				}
				/* If the current_start_addr is near the
				 * pm-unpacker start address, then use
				 * the keyhole to download remaining bytes
				 */
				if (dma_transfer_bytes < 12) {
					firmware_load_pm_though_keyhole(
							current_start_addr,
							remaining_bytes, data);
					break;
				}

				firmware_load_pm_though_dma(code,
				KAS_DM1_SRAM_START_ADDR, dma_transfer_bytes,
						data);
				firmware_run_pm_unpacker(
				KAS_DM1_SRAM_START_ADDR, current_start_addr,
						dma_transfer_bytes,
						params_start_addr,
						pm_unpacker_start_addr);

				current_start_addr += dma_transfer_bytes;
				remaining_bytes -= dma_transfer_bytes;
				data += dma_transfer_bytes;
			}
		}
#ifdef FW_CHECK
		fw_check_pm((u32 *) (&pm_block_ptr->data),
				pm_block_ptr->start_addr, pm_block_ptr->size);
#endif
		pm_block_ptr = (void *) (&pm_block_ptr->data
				+ pm_block_ptr->size);
	}
}

static void firmware_download_dm(struct firmware_code *code, void *dm_image)
{
	u32 block_num = ((u32 *) dm_image)[0];
	struct firmware_pm_dm_block *dm_block_ptr = dm_image + sizeof(u32);
	u32 i;
	void *dm_code_dma_addr;

	firmware_init_dma(KAS_TRANSLATE_24BIT_RIGHT_ALIGNED);

	for (i = 0; i < block_num; i++) {
		dm_code_dma_addr =
				(void *) &dm_block_ptr->data + SOC_SYSTEM_MAP;
		while (!(SOC_REG(A7DA_KAS_DMA_STATUS) & KAS_DMAC_IDLE))
			;

		SOC_REG(A7DA_KAS_DMAC_DMA_XLEN) = dm_block_ptr->size;
		SOC_REG(A7DA_KAS_DMAC_DMA_WIDTH) = dm_block_ptr->size;
		SOC_REG(A7DA_KAS_DMA_ADDR) = dm_block_ptr->start_addr;
		SOC_REG(A7DA_KAS_DMAC_DMA_ADDR) =
				(u32) dm_code_dma_addr;

		while (!(SOC_REG(A7DA_KAS_DMAC_DMA_INT) & KAS_DMAC_FINISH_INT))
			;

		SOC_REG(A7DA_KAS_DMAC_DMA_INT) |= KAS_DMAC_FINISH_INT;

#ifdef FW_CHECK
		fw_check_dm((u32 *) (&dm_block_ptr->data),
				dm_block_ptr->start_addr, dm_block_ptr->size);
#endif
		dm_block_ptr = (((void *) dm_block_ptr) + 8
				+ dm_block_ptr->size * sizeof(u32));
	}
}

static int firmware_download_const(struct firmware_code *code,
		void *const_image, int kas_addr_ptr)
{
	/* Update kas pointers in DM*/
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (kas_addr_ptr << 2) | (0x2 << 30);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) =
			((u32)const_image + SOC_SYSTEM_MAP) >> 24;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) =
			(u32)const_image + SOC_SYSTEM_MAP;

	return 0;
}

static void firmware_download(u32 *fw_data)
{
	struct firmware_code code;
	int i;
	int *p = (int *) &code.head;
	int head_size = sizeof(struct firmware_code_head) / sizeof(int);

	for (i = 0; i < head_size; i++)
		p[i] = swapInt32(fw_data[i]);
	code.code = &fw_data[head_size];
	firmware_download_pm(&code);
	firmware_download_dm(&code, code.code + code.head.dm1_offset);
	firmware_download_dm(&code, code.code + code.head.dm2_offset);
	firmware_download_const(&code, code.code + code.head.const16_offset,
			KAS_ADDR_CONST16);
	firmware_download_const(&code, code.code + code.head.const32_offset,
			KAS_ADDR_CONST32);
#ifdef FW_CHECK
	fw_info_dump(fw_data, &code);
#endif
}

#define KALIMBA_INDEP_ADDR (0x05400000)
void kalimba_load_firmware(u32 *kalimba_fw_addr)
{
	u32 kalimba_fw_size;
	u32 *kalimba_indep_addr = (u32 *)KALIMBA_INDEP_ADDR;

	if (kalimba_fw_addr == NULL)
		kalimba_fw_addr = (u32 *) uxGetImageAddressAndSize(IMAGE_ID_KALIMBA_FW,
			&kalimba_fw_size);
	memcpy((void *)kalimba_indep_addr,(void *)kalimba_fw_addr,kalimba_fw_size);
	firmware_download(kalimba_indep_addr);
	firmware_run_pm(0);
}

void kalimba_init(void)
{
	/* Enable kalimba clock */
	clkc_enable_kalimba();
	kalimba_load_firmware(NULL);
}

/* Kalimba messages send functions*/
int kalimba_get_version_id(u32 *version_id, u16 *resp)
{
	u16 msg[2] = {GET_VERSION_ID_REQ, 0};
	int ret;

	ret = ipc_send_msg(msg, 2,
		MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (version_id != NULL) {
		if (ret < 0)
			*version_id = -1;
		else
			*version_id = resp[3] | (resp[4] << 16);
	}

	return ret;
}

TaskHandle_t  crash_polling_task;

static void vCrash_polling_task(void *pvParameters)
{
	while (1) {
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (KAS_DEBUG << 2)
			| (0x2 << 30);
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;
		vTaskDelay(5 * 100); /* Test interval is 5 seconds */
	}
}

static portBASE_TYPE kas_crash_test(char *pcWriteBuffer,
		size_t xWriteBufferLen,
		const char *pcCommandString)
{
	xTaskCreate(vCrash_polling_task,
			"crash_polling_task", configMINIMAL_STACK_SIZE,
			NULL, tskIDLE_PRIORITY + 1, &crash_polling_task);
       return 0;
}

const CLI_Command_Definition_t kas_crash_testParameter =
{
	"kas_crash_test",
	"\r\nkas_crash_test\r\n",
	kas_crash_test,
	0
};

const CLI_Command_Definition_t *P_KALIMBA_KAS_CRASH_TEST_CLI_Parameter =
	&kas_crash_testParameter;

static portBASE_TYPE get_fw_id( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	u32 version_id;
	u16 resp[64];

	kalimba_get_version_id(&version_id, resp);
	DebugMsg("%s: 0x%x\n", __func__, version_id);
	return 0;
}

const CLI_Command_Definition_t get_fw_idParameter =
{
	"get_fw_id",
	"\r\nget_fw_id <...>:\r\n",
	get_fw_id,
	0
};
const CLI_Command_Definition_t *P_KALIMBA_IPC_GET_FW_ID_CLI_Parameter =
		&get_fw_idParameter;

static portBASE_TYPE load_fw( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	firmware_download((u32 *)0x00900000);
	return 0;
}

const CLI_Command_Definition_t load_fwParameter =
{
	"load_fw",
	"\r\nload_fw <...>:\r\n",
	load_fw,
	0
};
const CLI_Command_Definition_t *P_KALIMBA_LOAD_FW_CLI_Parameter =
		&load_fwParameter;

static portBASE_TYPE read_dm( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	u32 dm_addr;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );

	if (pcParameter1 == NULL) {
		DebugMsg("Command error: read_dm <addr>\n");
		return 0;
	}
	dm_addr = (u32)strtoul( pcParameter1, NULL, 0 );
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (dm_addr << 2) | (0x2 << 30);
	DebugMsg("DM addr(0x%x): %x\n", dm_addr, SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA));
	return 0;
}

const CLI_Command_Definition_t read_dmParameter =
{
	"read_dm",
	"\r\nread_dm <addr>:\r\n",
	read_dm,
	-1
};
const CLI_Command_Definition_t *P_KALIMBA_READ_DM_CLI_Parameter =
		&read_dmParameter;

int kalimba_create_operator(u16 capability_id, u16 *operator_id, u16 *resp)
{
	u16 msg[3] = {CREATE_OPERATOR_REQ, 1, capability_id};
	int ret;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP, resp);
	if (ret < 0)
		return ret;
	*operator_id = resp[3];

	return 0;
}

int kalimba_get_source(u16 endpoint_type, u16 instance_id, u16 channels,
	u32 handle_addr, u16 *endpoint_id, u16 *resp)
{
	int i, ret;
	u16 msg[7] = {GET_SOURCE_REQ, 5, endpoint_type,	instance_id, channels,
		handle_addr & 0xffff, handle_addr >> 16};

	ret = ipc_send_msg(msg, 7, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (endpoint_id && ret == 0) {
		for (i = 0; i < channels; i++)
			endpoint_id[i] = resp[3 + i];
	}
	return ret;
}

int kalimba_get_sink(u16 endpoint_type, u16 instance_id, u16 channels,
		u32 handle_addr, u16 *endpoint_id, u16 *resp)
{
	int i, ret;
	u16 msg[7] = {GET_SINK_REQ, 5, endpoint_type,
		instance_id, channels, handle_addr & 0xffff, handle_addr >> 16};

	ret = ipc_send_msg(msg, 7, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (endpoint_id && ret == 0) {
		for (i = 0; i < channels; i++)
			endpoint_id[i] = resp[3 + i];
	}
	return ret;
}

int kalimba_config_endpoint(u16 endpoint_id, u16 config_key,
		u32 config_value, u16 *resp)
{
	u16 msg[6] = {ENDPOINT_CONFIGURE_REQ, 4, endpoint_id,
		config_key, config_value & 0xffff, config_value >> 16};

	return ipc_send_msg(msg, 6, MSG_NEED_ACK | MSG_NEED_RSP, resp);
}

int kalimba_connect_endpoints(u16 source_endpoint_id, u16 sink_endpoint_id,
		u16 *connect_id, u16 *resp)
{
	u16 msg[4] = {CONNECT_REQ, 2, source_endpoint_id, sink_endpoint_id};
	int ret;

	ret = ipc_send_msg(msg, 4, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (connect_id && ret == 0)
		*connect_id = resp[3];
	return ret;
}

int kalimba_start_operator(u16 operators_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = START_OPERATOR_REQ;
	msg[1] = 1;
	msg[2] = operators_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (resp[3] != 1) {
		DebugMsg("Operator start failed: 0x%x\n",
				resp[3]);
		DebugMsg("First failure reason: 0x%x\n", resp[4]);
		return ret;
	}

	return ret;
}

int kalimba_start_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp)
{
	int msg_size = 2 + operator_count;
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int i, ret;

	if (operator_count < 1) {
		DebugMsg("%s: The operator numbers must great than zero: %d\r\n",
			__func__, operator_count);
		return -EINVAL;
	}

	msg[0] = START_OPERATOR_REQ;
	msg[1] = operator_count;
	for (i = 0; i < msg[1]; i++)
		msg[2 + i] = operators_id[i];

	ret = ipc_send_msg(msg, msg_size, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (resp[3] != operator_count || ret < 0) {
		DebugMsg("Operator start failed: %d %d\r\n",
				operator_count, resp[3]);
		DebugMsg("First failure reason: %x\r\n", resp[4]);
		return -EINVAL;
	}

	return ret;
}

void kalimba_dram_allocation_rsp_send(u32 addr)
{
	u16 msg[5] = {DRAM_ALLOCATION_RSP, 3, 0, (u16)(addr & 0xffff), (u16)(addr >> 16)};

	ipc_send_msg(msg, 5, MSG_NEED_ACK, NULL);
}

void kalimba_dram_free_rsp_send(void)
{
	u16 msg[3] = {DRAM_FREE_RSP, 1, 0};

	ipc_send_msg(msg, 3, MSG_NEED_ACK, NULL);
}

#define DSP_LICENSE_RESP_DATA_BYTES	24

void kalimba_license_resp_send(u16 *resp, u32 resp_size)
{
	u16 msg[2 + (DSP_LICENSE_RESP_DATA_BYTES >> 1)];

	msg[0] = KASCMD_SIGNAL_ID_LICENCE_CHECK_RSP;
	msg[1] = resp_size;
	memcpy(&msg[2], resp, resp_size * sizeof(u16));

	ipc_send_msg(msg, 2 + (DSP_LICENSE_RESP_DATA_BYTES >> 1),
			MSG_NEED_ACK, NULL);
}

int kalimba_stop_operator(u16 operators_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = STOP_OPERATOR_REQ;
	msg[1] = 1;
	msg[2] = operators_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (resp[3] != 1) {
		DebugMsg("Operator start failed: 0x%x\r\n", resp[3]);
		DebugMsg("First failure reason: 0x%x\r\n", resp[4]);
		return ret;
	}

	return ret;
}

int kalimba_stop_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp)
{
	int msg_size = 2 + operator_count;
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int i, ret;

	if (operator_count < 1) {
		DebugMsg("%s: The operator numbers must great than zero: %d\r\n",
			__func__, operator_count);
		return -EINVAL;
	}

	msg[0] = STOP_OPERATOR_REQ;
	msg[1] = operator_count;
	for (i = 0; i < msg[1]; i++)
		msg[2 + i] = operators_id[i];

	ret = ipc_send_msg(msg, msg_size, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (resp[3] != operator_count || ret < 0) {
		DEBUG_MSG("Operator stop failed: %d %d\r\n",
				operator_count, resp[3]);
		DEBUG_MSG("First failure reason: %x\r\n", resp[4]);
		return -EINVAL;
	}
	return ret;
}

int kalimba_disconnect_endpoint(u16 connect_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = DISCONNECT_REQ;
	msg[1] = 1;
	msg[2] = connect_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP, resp);

	return ret;
}

int kalimba_disconnect_endpoints_kcm(u16 connect_count, u16 *connect_id, u16 *resp)
{
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int ret;

	msg[0] = DISCONNECT_REQ;
	msg[1] = connect_count;
	memcpy(&msg[2], connect_id, connect_count * 2);

	ret = ipc_send_msg(msg, 2 + connect_count,
			MSG_NEED_ACK | MSG_NEED_RSP, resp);

	return ret;
}

int kalimba_close_source(u16 endpoint_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = CLOSE_SOURCE_REQ;
	msg[1] = 1;
	msg[2] = endpoint_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP,
		resp);
	return ret;
}

int kalimba_close_sink(u16 endpoint_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = CLOSE_SINK_REQ;
	msg[1] = 1;
	msg[2] = endpoint_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP,
		resp);
	return ret;
}

int kalimba_close_source_kcm(u16 endpoint_count, u16 *endpoint_id, u16 *resp)
{
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int ret;

	msg[0] = CLOSE_SOURCE_REQ;
	msg[1] = endpoint_count;
	memcpy(&msg[2], endpoint_id, endpoint_count * 2);

	ret = ipc_send_msg(msg, 2 + endpoint_count, MSG_NEED_ACK | MSG_NEED_RSP,
		resp);

	return ret;
}

int kalimba_close_sink_kcm(u16 endpoint_count, u16 *endpoint_id, u16 *resp)
{
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int ret;

	msg[0] = CLOSE_SINK_REQ;
	msg[1] = endpoint_count;
	memcpy(&msg[2], endpoint_id, endpoint_count * 2);

	ret = ipc_send_msg(msg, 2 + endpoint_count,
			MSG_NEED_ACK | MSG_NEED_RSP,
			resp);

	return ret;
}

int kalimba_destroy_operator(u16 operators_id, u16 *resp)
{
	u16 msg[3];
	int ret;

	msg[0] = DESTROY_OPERATOR_REQ;
	msg[1] = 1;
	msg[2] = operators_id;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK | MSG_NEED_RSP, resp);
	return ret;
}

int kalimba_destroy_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp)
{
	int msg_size = 2 + operator_count;
	u16 *msg = (u16 *)(GENERAL_MSG_BUFF - SOC_SYSTEM_MAP);
	int i;
	int ret;

	if (operator_count < 1) {
		DEBUG_MSG("%s: The operator numbers must great than zero: %d\n",
			__func__, operator_count);
		return -EINVAL;
	}

	msg[0] = DESTROY_OPERATOR_REQ;
	msg[1] = operator_count;
	for (i = 0; i < operator_count; i++)
		msg[2 + i] = operators_id[i];

	ret = ipc_send_msg(msg, msg_size,
			MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (resp[3] != operator_count || ret < 0) {
		DEBUG_MSG("Operator destroy failed: %d %d\n", operator_count,
			resp[3]);
		DEBUG_MSG("First failure reason: %x\n", resp[4]);
		return -EINVAL;
	}
	return 0;
}

int kalimba_data_produced(u16 endpoint_id)
{
	u16 msg[3] = {DATA_PRODUCED, 1, endpoint_id};
	int ret;

	ret = ipc_send_msg(msg, 3, MSG_NEED_ACK, NULL);
	if (ret < 0)
		return -EKASIPC;

	return ret;
}

void kalimba_set_ps_region_addr(u32 addr)
{
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (DSP_PS_FILE_BASE_ADDR << 2)
			| (0x2 << 30);

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = (addr >> 24);
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = (addr & 0xffffff);
}

int kalimba_operator_message(u16 operator_id, u16 msg_id, int message_data_len,
	u16 *msg_data, u16 **res_msg_data, u16 *rsp_msg_len, u16 *resp)
{
	int msg_size = 2 + 2 + message_data_len;
	u16 *msg = (u16 *)(OP_MSG_BUFF - SOC_SYSTEM_MAP);
	int i;
	int ret;

	msg[0] = OPERATOR_MESSAGE_REQ;
	msg[1] = 2 + message_data_len;
	msg[2] = operator_id;
	msg[3] = msg_id;

	for (i = 0; i < message_data_len; i++)
		msg[4 + i] = msg_data[i];

	ret = ipc_send_msg(msg, msg_size,
			MSG_NEED_ACK | MSG_NEED_RSP, resp);

	if (res_msg_data != NULL && ret == 0) {
		*rsp_msg_len = resp[1] - 3;
		*res_msg_data = (u16 *)(OP_RSP_BUFF - SOC_SYSTEM_MAP);
		for (i = 0; i < *rsp_msg_len; i++)
			*res_msg_data[i] = resp[5 + i];
	}

	return ret;
}
