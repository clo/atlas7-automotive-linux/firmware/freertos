/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KALIMBA_H_
#define KALIMBA_H_

#include "ctypes.h"

/* KAS DSP definitions */
#define KAS_REGFILE_PC				0xFFFFC0
#define KAS_DEBUG				0xFFFF8C

#define KAS_DEBUG_RUN				(1 << 0)
#define KAS_DEBUG_STOP				(0 << 0)

#define KAS_RESET_DMA_CLIENT			(1 << 0)
#define KAS_DMA_CHAIN_MODE			(1 << 1)

#define KAS_DMAC_IDLE				(1 << 0)

#define KAS_PM_SRAM_BYTES			SZ_256K
#define KAS_DM1_SRAM_BYTES			(SZ_64K + SZ_32K)
#define KAS_DM2_SRAM_BYTES			KAS_DM1_SRAM_BYTES

#define KAS_PM_SRAM_SIZE			(KAS_PM_SRAM_BYTES >> 2)
#define KAS_DM1_SRAM_SIZE			(KAS_DM1_SRAM_BYTES >> 2)
#define KAS_DM2_SRAM_SIZE			KAS_DM1_SRAM_SIZE

#define KAS_DM1_SRAM_START_ADDR			0
#define KAS_DM1_SRAM_END_ADDR			(KAS_DM1_SRAM_START_ADDR + \
						KAS_DM1_SRAM_BYTES - 1)
#define KAS_DM2_SRAM_START_ADDR			0xFF3000
#define KAS_DM2_SRAM_END_ADDR			(KAS_DM2_SRAM_START_ADDR + \
						KAS_DM2_SRAM_BYTES - 1)

#define KAS_PM_SRAM_START_ADDR			0
#define KAS_PM_SRAM_END_ADDR			(KAS_PM_SRAM_START_ADDR + \
						KAS_PM_SRAM_BYTES - 1)

#define KAS_TRANSLATE_24BIT_RIGHT_ALIGNED	(0 << 4)
#define KAS_TRANSLATE_24BIT_LEFT_ALIGNED	(1 << 4)
#define KAS_TRANSLATE_2x16BIT			(2 << 4)
#define KAS_TRANSLATE_24BIT			(3 << 4)
#define KAS_TRANSLATE_LEFT_ALIGNED		(0 << 3)
#define KAS_TRANSLATE_RIGHT_ALIGNED		(1 << 3)

#define KAS_DMAC_FINISH_INT			(1 << 0)

#define PM_UNPACKER_PARAMS_DM_SRC(x)		(x)
#define PM_UNPACKER_PARAMS_PM_DEST(x)		(x + 1)
#define PM_UNPACKER_PARAMS_PM_LEN(x)		(x + 2)
#define PM_UNPACKER_PARAMS_IPC_TRGT_LO16(x)	(x + 3)
#define PM_UNPACKER_PARAMS_IPC_TRGT_HI16(x)	(x + 4)
#define PM_UNPACKER_PARAMS_UNPACK_STATUS(x)	(x + 5)

#define KAS_DMAC_TRANS_FIFO_TO_MEM		(0 << 4)
#define KAS_DMAC_TRANS_MEM_TO_FIFO		(1 << 4)

#define UP_ALIGN_12BYTES(x)			((x + 11) / 12 * 12)
#define DOWN_ALIGN_12BYTES(x)			(x / 12 * 12)

#define ARM_SEND_COUNT_ADDR					0xFFAFAE
#define ARM_ACK_COUNT_ADDR					0xFFAFAF
#define ARM_MESSAGE_SEND_ADDR				0xFFAFB0

#define DSP_SEND_COUNT_ADDR					0x007FAE
#define DSP_ACK_COUNT_ADDR					0x007FAF
#define DSP_MESSAGE_SEND_ADDR				0x007FB0
#define DSP_INTR_RAISED_ADDR					0x007FBA

#define KAS_ADDR_CONST16			0x007F9A
#define KAS_ADDR_CONST32			0x007F9C

#define DSP_PS_FILE_BASE_ADDR			0x007F9E

void kalimba_init(void);

#define ENDPOINT_TYPE_USP			1
#define ENDPOINT_TYPE_I2S			2
#define ENDPOINT_TYPE_IACC			3
#define ENDPOINT_TYPE_SPDIF			5
#define ENDPOINT_TYPE_FILE			10
#define ENDPOINT_TYPE_APPDATA			11

#define ENDPOINT_PHY_DEV_PCM0			0
#define ENDPOINT_PHY_DEV_PCM1			1
#define ENDPOINT_PHY_DEV_PCM2			2
#define ENDPOINT_PHY_DEV_I2S0			3
#define ENDPOINT_PHY_DEV_I2S1			4
#define ENDPOINT_PHY_DEV_AC97			5
#define ENDPOINT_PHY_DEV_A7CA			6
#define ENDPOINT_PHY_DEV_IACC			7
#define ENDPOINT_PHY_DEV_SPDIF			8

#define MESSAGE_SEND_ADDR			0xFFAF9C
#define MESSAGE_SEND_ACK_ADDR			0x007FA0

#define CAPABILITY_ID_BASIC_PASSTHROUGH		0x0001
#define CAPABILITY_ID_RESAMPLER			0x0009
#define CAPABILITY_ID_MIXER			0x000A
#define CAPABILITY_ID_SPLITTER			0x0013
#define CAPABILITY_ID_CVC_RCV_NB		0x001D
#define CAPABILITY_ID_CVC_RCV_WB		0x001F
#define CAPABILITY_ID_CVCHF1MIC_SEND_NB		0x001C
#define CAPABILITY_ID_CVCHF1MIC_SEND_WB		0x001E
#define CAPABILITY_ID_CVCHF2MIC_SEND_NB		0x0020
#define CAPABILITY_ID_CVCHF2MIC_SEND_WB		0x0021
#define CAPABILITY_ID_CVC_RCV_UWB		0x0053
#define CAPABILITY_ID_CVCHF1MIC_SEND_UWB	0x0056
#define CAPABILITY_ID_CVCHF2MIC_SEND_UWB	0x0059
#define CAPABILITY_ID_DBE			0x002F
#define CAPABILITY_ID_DELAY			0x0035
#define CAPABILITY_ID_AEC_REF_1MIC		0x0040
#define CAPABILITY_ID_AEC_REF_2MIC		0x0041
#define CAPABILITY_ID_VOLUME_CONTROL		0x0048
#define CAPABILITY_ID_PEQ			0x0049
#define CAPABILITY_ID_DBE_FULLBAND_IN_OUT	0x0090
#define CAPABILITY_ID_DBE_FULLBAND_IN		0x0091
#define CAPABILITY_ID_CHANNEL_MIXER		0x0097
#define CAPABILITY_ID_SOURCE_SYNC		0x0099

#define CAPABILITY_ID_CVC_RCV_DUMMY CAPABILITY_ID_CVC_RCV_WB
#define CAPABILITY_ID_CVCHF_SEND_DUMMY CAPABILITY_ID_CVCHF1MIC_SEND_WB
#define CAPABILITY_ID_AEC_REF_DUMMY CAPABILITY_ID_AEC_REF_1MIC

#define AEC_REF_SET_SAMPLE_RATES		0x00FE
#define OPERATOR_MSG_SET_UCID			0x2007
#define OPERATOR_MSG_SET_CVC_SAMPLE_RATE	0x1F40
#define MIXER_SUPPORT_STREAMS			3
#define MIXER_SUPPORT_CHANNELS			12

#define RESAMPLER_SET_CONVERSION_RATE		0x0002
#define RESAMPLER_SET_CUSTOM_RATE		0x0003
#define RESAMPLER_FILTER_COEFFICIENTS		0x0004

#define CHANNEL_MIXER_SET_PARAMETERS		0x0001

#define SOURCESYNC_SET_ROUTE			0x0001
#define SOURCESYNC_SET_SINK_GROUPS		0x0003

#define OPERATOR_MSG_VOLUME_CTRL_SET_CONTROL	0x2002

#define CREATE_OPERATOR_REQ			0x0001
#define CREATE_OPERATOR_RSP			0x1001

#define START_OPERATOR_REQ			0x0002
#define START_OPERATOR_RSP			0x1002

#define STOP_OPERATOR_REQ			0x0003
#define STOP_OPERATOR_RSP			0x1003

#define RESET_OPERATOR_REQ			0x0004
#define RESET_OPERATOR_RSP			0x1004

#define DESTROY_OPERATOR_REQ			0x0005
#define DESTROY_OPERATOR_RSP			0x1005

#define OPERATOR_MESSAGE_REQ			0x0006
#define OPERATOR_MESSAGE_RSP			0x1006

#define OPERATOR_MSG_SET_CHANNELS		2
#define OPERATOR_MSG_SET_GAINS			1
#define OPERATOR_MSG_SET_RAMP_NUM_SAMPLES	3
#define OPERATOR_MSG_SET_PRIMARY_STREAM		4
#define OPERATOR_MSG_SET_CHANNEL_GAINS		5

#define OPERATOR_MSG_SET_PASSTHROUGH_GAIN	2

#define GET_SOURCE_REQ				0x0008
#define GET_SOURCE_RSP				0x1008

#define GET_SINK_REQ				0x0009
#define GET_SINK_RSP				0x1009

#define CLOSE_SOURCE_REQ			0x000A
#define CLOSE_SOURCE_RSP			0x100A

#define CLOSE_SINK_REQ				0x000B
#define CLOSE_SINK_RSP				0x100B

#define SYNC_ENDPOINTS_REQ			0x000C
#define SYNC_ENDPOINTS_RSP			0x100C

#define ENDPOINT_CONFIGURE_REQ			0x000D
#define ENDPOINT_CONFIGURE_RSP			0x100D

#define ENDPOINT_GET_INFO_REQ			0x000E
#define ENDPOINT_GET_INFO_RSP			0x100E

#define CONNECT_REQ				0x000F
#define CONNECT_RSP				0x100F

#define DISCONNECT_REQ				0x0010
#define DISCONNECT_RSP				0x1010

#define CAPABILITY_CODE_DRAM_ADDR_SET_REQ	0x0011
#define CAPABILITY_CODE_DRAM_ADDR_SET_RSP	0x1011

#define CAPABILITY_CODE_DRAM_ADDR_CLEAR_REQ	0x0012
#define CAPABILITY_CODE_DRAM_ADDR_CLEAR_RSP	0x1012

#define DATA_PRODUCED				0x001A
#define DATA_CONSUMED			0x001B
#define PS_FLUSH_REQ				0x001C

#define GET_VERSION_ID_REQ			0x0013
#define GET_VERSION_ID_RSP			0x1013

#define GET_CAPID_LIST_REQ			0x0014
#define GET_CAPID_LIST_RSP			0x1014

#define GET_OPID_LIST_REQ			0x0015
#define GET_OPID_LIST_RSP			0x1015

#define GET_CONNECTION_LIST_REQ			0x0016
#define GET_CONNECTION_LIST_RSP			0x1016

#define DRAM_ALLOCATION_REQ			0x0017
#define DRAM_ALLOCATION_RSP			0x1017

#define DRAM_FREE_REQ				0x0018
#define DRAM_FREE_RSP				0x1018

/* license messages from design spec */
#define KASCMD_SIGNAL_ID_LICENCE_CHECK_REQ	0x0022
#define KASCMD_SIGNAL_ID_LICENCE_CHECK_RSP	0x1022

#define CREATE_OPERATOR_EXTENDED_REQ		0x0023
#define CREATE_OPERATOR_EXTENDED_RSP		0x1023

#define ENDPOINT_CONF_AUDIO_SAMPLE_RATE		0x0A00
#define ENDPOINT_CONF_AUDIO_DATA_FORMAT		0x0A01
#define ENDPOINT_CONF_DRAM_PACKING_FORMAT	0x0A02
#define ENDPOINT_CONF_INTERLEAVING_MODE		0x0A03
#define ENDPOINT_CONF_CLOCK_MASTER		0x0A04
#define ENDPOINT_CONF_PERIOD_SIZE		0x0A05

#define OPMSG_COMMON_GET_CAPABILITY_VERSION     0x1000
#define OPMSG_COMMON_ENABLE_FADE_OUT            0x2000
#define OPMSG_COMMON_DISABLE_FADE_OUT           0x2001
#define OPMSG_COMMON_SET_CONTROL                0x2002
#define OPMSG_COMMON_GET_PARAMS                 0x2003
#define OPMSG_COMMON_GET_DEFAULTS               0x2004
#define OPMSG_COMMON_SET_PARAMS                 0x2005
#define OPMSG_COMMON_GET_STATUS                 0x2006
#define OPMSG_COMMON_SET_UCID                   0x2007
#define OPMSG_COMMON_GET_LOGICAL_PS_ID          0x2008
#define OPMSG_COMMON_SET_BUFFER_SIZE            0x200C
#define OPMSG_COMMON_SET_TERMINAL_BUFFER_SIZE   0x200D
#define OPMSG_COMMON_SET_SAMPLE_RATE            0x200E
#define OPMSG_COMMON_SET_DATA_STREAM_BASED      0x200F

#define OPMSG_PEQ_SET_COEFFS                    0x0001

#define PEQ_NUM_MAX			5
#define PEQ_BAND_MIN			1
#define PEQ_BAND_MAX			10
#define PEQ_PARAMS_ARRAY_LEN_16B	66
#define PEQ_MSG_PARAMS_ARRAY_LEN_16B	(PEQ_PARAMS_ARRAY_LEN_16B + 3)
#define PEQ_MSG_CNTL_BLOCK_LEN_16B	3
#define PEQ_PARAMS_ARRAY_LEN_24B	44
#define PEQ_SAMPLE_RATE			0x0780	/* 48K/25 */

/* PEQ reg */
/* base band    cntl   */
/*|----|----|----|----|*/
/*15   11   7    3    0*/
#define PEQ_BASE_MASK			0xf000
#define PEQ_BASE_SHIFT			12
#define PEQ_BAND_MASK			0x0f00
#define PEQ_BAND_SHIFT			8
#define PEQ_CNTL_MASK			0x00ff
#define USER_PEQ_BASE			0x0000
#define SPK1_PEQ_BASE			0x1000
#define SPK2_PEQ_BASE			0x2000
#define SPK3_PEQ_BASE			0x3000
#define SPK4_PEQ_BASE			0x4000
#define PEQ_CNTL_SWITCH			0x00ff
#define PEQ_PARAM_CONFIG		0x0000
#define PEQ_PARAM_CORE_TYPE		0x0001
#define PEQ_PARAM_BANDS_NUM		0x0002
#define PEQ_PARAM_MASTER_GAIN		0x0003
#define PEQ_PARAM_BAND_FILTER		0x0004
#define PEQ_PARAM_BAND_FC		0x0005
#define PEQ_PARAM_BAND_GAIN		0x0006
#define PEQ_PARAM_BAND_Q		0x0007
#define Q24_MASK			0x00ffffff

/* PEQ default parameter-set array: for init */
static const u16 peq_params_array_def[] = {
	/* PEQ_CONF  CORE_TYPE    NUM_BANDS MASTER_GAIN */
	/*|--------||--------|    |--------||--------|*/
	0x0000, 0x0000, 0x0000, 0x0000, 0x0a00, 0x0000,
	/* band: 1 ~ 10 */
	/*  FILTER      FC           GAIN       Q     */
	/*|--------||--------|    |--------||--------|*/
	0x0000, 0x0D00, 0x0200, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x0400, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x07D0, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x0FA0, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x1F40, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x3E80, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0x7D00, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D00, 0xFA00, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D01, 0xF400, 0x0000, 0x0000, 0xB505,
	0x0000, 0x0D03, 0xE800, 0x0000, 0x0000, 0xB505,
};

#define DBE_PARAMS_ARRAY_LEN_16B	12
#define DBE_MSG_PARAMS_ARRAY_LEN_16B	(DBE_PARAMS_ARRAY_LEN_16B + 3)
#define DBE_CNTL_MASK		int kalimba_get_version_id(u32 *version_id, u16 *resp)	0x00ff
#define DBE_CNTL_SWITCH			0x00ff
#define DBE_PARAM_XOVER_FC		0x0000
#define DBE_PARAM_MIX_BALANCE		0x0001
#define DBE_PARAM_EFFECT_STRENGTH	0x0002
#define DBE_PARAM_AMP_LIMIT		0x0003
#define DBE_PARAM_LP_FC			0x0004
#define DBE_PARAM_HP_FC			0x0005
#define DBE_PARAM_HARM_CONTENT		0x0006

/* DBE default parameter-set array: for init */
static const u16 dbe_params_array_def[]  = {
	/* XOVER_FC  MIX_BANL      EFFT_STR AMP_LIMIT */
	/*|--------||--------|    |--------||--------|*/
	0x000C, 0x8000, 0x0032, 0x0000, 0x3200, 0x0000,
	/*   LP_FC   HP_FC         HARM_CONT  PADDING */
	/*|--------||--------|    |--------||--------|*/
	0x0006, 0x4000, 0x0640, 0x0000, 0x3200, 0x0000,
};

#define DELAY_PARAMS_ARRAY_LEN_16B	12
#define DELAY_MSG_PARAMS_ARRAY_LEN_16B	(DELAY_PARAMS_ARRAY_LEN_16B + 3)
#define DELAY_CNTL_MASK			0x00ff
#define DELAY_PARAM_CHAN1_DELAY		0x0000
#define DELAY_PARAM_CHAN2_DELAY		0x0001
#define DELAY_PARAM_CHAN3_DELAY		0x0002
#define DELAY_PARAM_CHAN4_DELAY		0x0003
#define DELAY_PARAM_CHAN5_DELAY		0x0004
#define DELAY_PARAM_CHAN6_DELAY		0x0005
#define DELAY_PARAM_CHAN7_DELAY		0x0006
#define DELAY_PARAM_CHAN8_DELAY		0x0007

void kalimba_load_firmware(u32 *kalimba_fw_addr);
int kalimba_get_version_id(u32 *version_id, u16 *resp);
int kalimba_create_operator(u16 capability_id, u16 *operator_id, u16 *resp);
int kalimba_get_source(u16 endpoint_type, u16 instance_id, u16 channels,
	u32 handle_addr, u16 *endpoint_id, u16 *resp);
int kalimba_get_sink(u16 endpoint_type, u16 instance_id, u16 channels,
		u32 handle_addr, u16 *endpoint_id, u16 *resp);
int kalimba_config_endpoint(u16 endpoint_id, u16 config_key,
		u32 config_value, u16 *resp);
int kalimba_connect_endpoints(u16 source_endpoint_id, u16 sink_endpoint_id,
		u16 *connect_id, u16 *resp);
int kalimba_start_operator(u16 operators_id, u16 *resp);
int kalimba_stop_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp);
int kalimba_stop_operator(u16 operators_id, u16 *resp);
int kalimba_start_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp);
int kalimba_disconnect_endpoint(u16 connect_id, u16 *resp);
int kalimba_disconnect_endpoints_kcm(u16 connect_count, u16 *connect_id, u16 *resp);
int kalimba_close_source(u16 endpoint_id, u16 *resp);
int kalimba_close_sink(u16 endpoint_id, u16 *resp);
int kalimba_close_source_kcm(u16 endpoint_count, u16 *endpoint_id, u16 *resp);
int kalimba_close_sink_kcm(u16 endpoint_count, u16 *endpoint_id, u16 *resp);
int kalimba_destroy_operator(u16 operators_id, u16 *resp);
int kalimba_destroy_operator_kcm(u16 *operators_id, u16 operator_count, u16 *resp);
int kalimba_data_produced(u16 endpoint_id);
void kalimba_set_ps_region_addr(u32 addr);
void kalimba_license_resp_send(u16 *resp, u32 resp_size);
void kalimba_dram_allocation_rsp_send(u32 addr);
void kalimba_dram_free_rsp_send(void);
int kalimba_operator_message(u16 operator_id, u16 msg_id, int message_data_len,
	u16 *msg_data, u16 **res_msg_data, u16 *rsp_msg_len, u16 *resp);
#endif /* KALIMBA_H_ */
