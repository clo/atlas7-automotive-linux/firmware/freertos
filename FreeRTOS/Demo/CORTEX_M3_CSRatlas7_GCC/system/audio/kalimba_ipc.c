/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "event_groups.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "soc_irq.h"
#include "core_cm3.h"
#include "ctypes.h"
#include "debug.h"
#include "errno.h"
#include "io.h"
#include "misclib.h"

#include "soc_irq.h"
#include "soc_ipc.h"
#include <soc_kas.h>
#include "soc_timer.h"
#include "kalimba.h"
#include "kalimba_ipc.h"
#include "audio_protocol.h"

#define IPC_COMM_TIMEOUT				(10000 / portTICK_PERIOD_MS)

#define MSG_IPC_RSP					BIT0

 struct ipc_msg {
 	u16 payload[64];
 };

 static struct kalimba_ipc {
	SemaphoreHandle_t ipc_comm_mutex;
	SemaphoreHandle_t ipc_send_mutex;
	EventGroupHandle_t event_ipc_msg;
	u16 payload[64];
	struct ipc_msg message;
	struct ipc_msg respone;
	u16 *cur_offs;	/*Current the payload fill offset */
	TaskHandle_t kas_ipc_recv_task;
	SemaphoreHandle_t ipc_recv_semaphore;
 } kalimba_ipc;

 static u32 read_sram(u32 address)
 {
	 SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (address << 2) | (0x2 << 30);
 	return SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
 }

 static void write_sram(u32 address, u32 value)
 {
	 SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = (address << 2) | (0x2 << 30);
	 SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = value;
 }

 static void increment_counter(u32 address)
 {
 	u32 counter;

 	counter = read_sram(address);
 	counter++;
 	write_sram(address, counter);
 }

 static bool is_ipc_ack(void)
 {
 	u32 arm_send_count;
 	u32 dsp_ack_count;

 	arm_send_count = read_sram(ARM_SEND_COUNT_ADDR);
 	dsp_ack_count = read_sram(DSP_ACK_COUNT_ADDR);

 	/*
 	 * If the counter of ARM send equal the counter of the DSP ack,
 	 * that means the kalimba sends ACKs signal for the messages by
 	 * the ARM.
 	 */
 	if (arm_send_count == dsp_ack_count)
 		return true;
 	return false;
 }

 static int ipc_send_msg_package(u16 *msg, int size, u16 msg_short_type,
 	int total_len, u32 need_ack_rsp)
 {
		int i;
		int ret = 0;

		SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) =
				(ARM_MESSAGE_SEND_ADDR << 2) | (0x2 << 30);

		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = msg_short_type;

		if (msg_short_type == MESSAGING_SHORT_COMPLETE
				|| msg_short_type == MESSAGING_SHORT_START)
			SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = (u32)total_len;

		for (i = 0; i < size; i++)
			SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = msg[i];
		increment_counter(ARM_SEND_COUNT_ADDR);
		SOC_REG(A7DA_IPC_TRGT3_INIT2_1) = ARM_IPC_INTR_TO_KALIMBA;
		__DMB();

		if (need_ack_rsp & MSG_NEED_ACK) {
			/* Try to check the ACK for 10 times */
			for (i = 0; i < 10; i++) {
				if (is_ipc_ack()) {
					break;
				}
				xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
				vUsWait(50);
				xSemaphoreTake(kalimba_ipc.ipc_comm_mutex, portMAX_DELAY);
			}
			if (i == 10) {
				DebugMsg("Ack from DSP timeout: Maybe Kalimba is down\r\n");
				do_coredump();
				ret = -EKASCRASH;
				goto out;
			}
		}
	out:
		return ret;
 }

 static int check_response(u16 msg_id, u16 resp_id, u16 status)
 {
 	/*
 	 * If the response id is not correct or the status is error code,
 	 * then cause the system oops.
 	 */
 	if ((resp_id != (msg_id | 0x1000)) || status != 0) {
 		DebugMsg("kas response: %d\r\n", status);
 		DebugMsg("msg id: 0x%04x, resp id: 0x%04x, status: 0x%04x\r\n",
 			msg_id, resp_id, status);
 		return -EKASMSGRSP;
 	}

 	return 0;
 }

 int ipc_send_msg(u16 *msg, int size, u32 need_ack_rsp, u16 *resp)
 {
 	u16 msg_id;
 	int ret = 0;

 	xSemaphoreTake(kalimba_ipc.ipc_send_mutex, portMAX_DELAY);
 	xSemaphoreTake(kalimba_ipc.ipc_comm_mutex, portMAX_DELAY);
 	msg_id = msg[0];

 	if (size <= FRAME_MAX_START_COMPLETE_DATA_SIZE) {
 		ret = ipc_send_msg_package(msg, size,
 				MESSAGING_SHORT_COMPLETE,
 				size, need_ack_rsp);
 		if (ret < 0)
 			goto error;
 	} else {
 		ret = ipc_send_msg_package(msg,
 				FRAME_MAX_START_COMPLETE_DATA_SIZE,
 				MESSAGING_SHORT_START,
 				size, need_ack_rsp);
 		if (ret < 0)
 			goto error;
 		msg += FRAME_MAX_START_COMPLETE_DATA_SIZE;
 		size -= FRAME_MAX_START_COMPLETE_DATA_SIZE;
 		while (size > FRAME_MAX_CONTINUE_END_DATA_SIZE) {
 			ret = ipc_send_msg_package(
 					msg, FRAME_MAX_CONTINUE_END_DATA_SIZE,
 					MESSAGING_SHORT_CONTINUE,
 					0, need_ack_rsp);
 			if (ret < 0)
 				goto error;
 			msg += FRAME_MAX_CONTINUE_END_DATA_SIZE;
 			size -= FRAME_MAX_CONTINUE_END_DATA_SIZE;
 		}
 		ret = ipc_send_msg_package(msg, size,
 				MESSAGING_SHORT_END,
 				0, need_ack_rsp);
 		if (ret < 0)
 			goto error;
 	}

 	if (need_ack_rsp & MSG_NEED_RSP) {
 		xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
 		if (xEventGroupWaitBits(kalimba_ipc.event_ipc_msg, MSG_IPC_RSP, pdTRUE,
 				pdTRUE, IPC_COMM_TIMEOUT) == 0) {
 				xEventGroupClearBits(kalimba_ipc.event_ipc_msg, MSG_IPC_RSP);
 				DebugMsg("RSP from DSP timeout: Maybe Kalimba is down\r\n");
				do_coredump();
 				ret = -EKASCRASH;
 				goto out;
 		}

 		check_response(msg_id, kalimba_ipc.respone.payload[0],
 				kalimba_ipc.respone.payload[2]);

 		if (resp)
 			memcpy(resp, kalimba_ipc.respone.payload,
 				64 * sizeof(u16));
 		xSemaphoreTake(kalimba_ipc.ipc_comm_mutex, portMAX_DELAY);
 	}
error:
	xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
out:
	xSemaphoreGive(kalimba_ipc.ipc_send_mutex);
 	return ret;
 }

 /* Clear the flag of DSP interrupt raised, then send a ACK to kalimba */
 void ipc_clear_raised_and_send_ack(void)
 {
 	/*
 	 * Clear the interrupt raised flag of kalimba,
 	 * let the kalimba know next IPC interrupt can be sent.
 	 */
 	write_sram(DSP_INTR_RAISED_ADDR, 0);
 	/* Increnment the ACK counter, then send a ACK single to kalimba */
 	increment_counter(ARM_ACK_COUNT_ADDR);
 	/* Send IPC intr to kalimba */
 	SOC_REG(A7DA_IPC_TRGT3_INIT2_1) = ARM_IPC_INTR_TO_KALIMBA;
 	__DMB();
 }

 static u32 read_msg_payload(u32 *payload_size)
 {
 	u32 msg_type = 0;
 	u32 i;
 	u32 val;

 	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
 	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) =
 			(DSP_MESSAGE_SEND_ADDR << 2) | (0x2 << 30);
 	msg_type = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
 	switch (msg_type) {
 	case MESSAGING_SHORT_COMPLETE:
 	case MESSAGING_SHORT_START:
 		*payload_size = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
 		for (i = 0; i < FRAME_MAX_START_COMPLETE_DATA_SIZE; i++) {
 			val = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
 			kalimba_ipc.payload[i] = (u16)val;
 		}
 		kalimba_ipc.cur_offs = kalimba_ipc.payload +
 				FRAME_MAX_START_COMPLETE_DATA_SIZE;
 		break;
 	case MESSAGING_SHORT_CONTINUE:
 	case MESSAGING_SHORT_END:
 		for (i = 0; i < FRAME_MAX_CONTINUE_END_DATA_SIZE; i++) {
 			val = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
 			kalimba_ipc.cur_offs[i] = (u16)val;
 		}
 		kalimba_ipc.cur_offs += FRAME_MAX_CONTINUE_END_DATA_SIZE;
 		break;
 	default:
 		msg_type = -EKASMSGTYPE;
 		break;
 	}
 	return msg_type;
 }

 static int process_ipc_payload(u32 msg_type)
 {
 	int ret;

 	switch (msg_type) {
 	case MESSAGING_SHORT_COMPLETE:
 	case MESSAGING_SHORT_END:
 		if (kalimba_ipc.payload[0] & 0x1000) {
 			memcpy(kalimba_ipc.respone.payload, kalimba_ipc.payload,
 				64 * sizeof(u16));
 			ipc_clear_raised_and_send_ack();
 			xEventGroupSetBits(kalimba_ipc.event_ipc_msg, MSG_IPC_RSP);
 			ret = IPC_SEND_RSP_TO_ARM;
 		} else {
 			memcpy(kalimba_ipc.message.payload, kalimba_ipc.payload,
 				64 * sizeof(u16));
 			ret = IPC_SEND_MSG_TO_ARM;
 		}
 		break;
 	case MESSAGING_SHORT_CONTINUE:
 	case MESSAGING_SHORT_START:
 		ipc_clear_raised_and_send_ack();
 		ret = IPC_SEND_NO_COMPLETE;
 		break;
 	default:
 		ret = -EKASPLD;
 		break;
 	}
 	return ret;
 }

 static int ipc_recv_msg_payload_handler(u32 *payload_size)
 {
 	u32 msg_type;
 	u32 arm_ack_count;
 	u32 dsp_send_count;
 	int ret;

 	xSemaphoreTake(kalimba_ipc.ipc_comm_mutex, portMAX_DELAY);
 	arm_ack_count = read_sram(ARM_ACK_COUNT_ADDR);
 	dsp_send_count = read_sram(DSP_SEND_COUNT_ADDR);

 	/*
 	 * If the counter of ARM ack unequal to the counter of the DSP send,
 	 * that means the kalimba sends a message or a response to the ARM.
 	 */
 	if (arm_ack_count != dsp_send_count) {
 		msg_type = read_msg_payload(payload_size);
 		if (msg_type < 0) {
 			ret = msg_type;
 			DebugMsg("Error msg_type\n");
 			goto out;
 		}

 		ret = process_ipc_payload(msg_type);
 	}
 out:
	xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
 	return ret;
 }

 void INT_IPC_TRGT2_INIT3_INTR1_Handler(void)
 {
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	NVIC_DisableIRQ(INT_IPC_TRGT2_INIT3_INTR1_IRQn);

	/* Unblock the task by releasing the semaphore. */
	xSemaphoreGiveFromISR(kalimba_ipc.ipc_recv_semaphore, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
 }

 static void prvKASIPCRECVTask( void *pvParameters )
 {
	u32 payload_size;
	u32 reg;

	while (1) {
		if (xSemaphoreTake(kalimba_ipc.ipc_recv_semaphore, portMAX_DELAY) == pdFALSE)
				continue;
		/* Read from IPC interrupt register will clear the interrupt */
		/* read register to clear the interrupt */
		reg = SOC_REG(A7DA_IPC_TRGT2_INIT3_1);
		reg = reg; /* remove complier warning */
		/*
		 * If the DSP send counter unequal to  ARM ack counter,
		 * that means the DSP send a message or respone to the ARM
		 * After a message or respone processed, check again.
		 */
		xSemaphoreTake(kalimba_ipc.ipc_comm_mutex, portMAX_DELAY);
		if (read_sram(DSP_SEND_COUNT_ADDR)
				== read_sram(ARM_ACK_COUNT_ADDR)) {
			xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
		} else {
			xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);

			if (ipc_recv_msg_payload_handler(&payload_size) == IPC_SEND_MSG_TO_ARM) {
				kalimba_msg_to_m3_handler(kalimba_ipc.payload);
				xSemaphoreTake(kalimba_ipc.ipc_comm_mutex,
						portMAX_DELAY);
				ipc_clear_raised_and_send_ack();
				xSemaphoreGive(kalimba_ipc.ipc_comm_mutex);
			}
		}
		NVIC_EnableIRQ(INT_IPC_TRGT2_INIT3_INTR1_IRQn);
	}
 }

 int kalimba_ipc_init(void)
 {
	 int ret = 0;

	 kalimba_ipc.ipc_comm_mutex = xSemaphoreCreateMutex();
	 if (kalimba_ipc.ipc_comm_mutex == NULL) {
	 		configASSERT( 0 );
	 		return -EINVAL;
	 }

	 kalimba_ipc.ipc_send_mutex = xSemaphoreCreateMutex();
	  if ( kalimba_ipc.ipc_send_mutex == NULL ) {
	  		configASSERT( 0 );
	  		ret = -EINVAL;
	  		goto create_send_mutex_failed;
	 }

	 kalimba_ipc.event_ipc_msg = xEventGroupCreate();
	 if (kalimba_ipc.event_ipc_msg == NULL) {
		 configASSERT( 0 );
		 ret = -EINVAL;
		 goto create_event_ipc_msg_failed;
	 }

	 kalimba_ipc.ipc_recv_semaphore = xSemaphoreCreateBinary();
	if (kalimba_ipc.ipc_recv_semaphore == NULL) {
		DebugMsg("create kas ipc receive semaphore failed!\r\n");
		goto create_ipc_recv_semaphore_failed;
	}

	 ret = xTaskCreate(prvKASIPCRECVTask, "kasipc", configMINIMAL_STACK_SIZE * 4,
	 			NULL, tskIDLE_PRIORITY + 2, &kalimba_ipc.kas_ipc_recv_task);
	 if (ret != pdTRUE) {
	 	DebugMsg("create kasipc task failed!ret=%d\r\n", ret);
	 	goto create_kas_ipc_recv_task_failed;
	 }

	 NVIC_SetPriority(INT_IPC_TRGT2_INIT3_INTR1_IRQn, API_SAFE_INTR_PRIO_0);
	 NVIC_EnableIRQ(INT_IPC_TRGT2_INIT3_INTR1_IRQn);

	 return ret;
create_kas_ipc_recv_task_failed:
	vSemaphoreDelete(kalimba_ipc.ipc_recv_semaphore);
create_ipc_recv_semaphore_failed:
	vEventGroupDelete(kalimba_ipc.event_ipc_msg);
create_event_ipc_msg_failed:
	vSemaphoreDelete(kalimba_ipc.ipc_send_mutex);
create_send_mutex_failed:
	vSemaphoreDelete(kalimba_ipc.ipc_comm_mutex);
	return ret;
 }
