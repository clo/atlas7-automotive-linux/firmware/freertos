/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __AUDIO_IPC_H
#define __AUDIO_IPC_H

#define MSG_NEED_ACK							0x1
#define MSG_NEED_RSP							0x2

#define MESSAGING_SHORT_BASE					0x0000
#define MESSAGING_SHORT_COMPLETE			(MESSAGING_SHORT_BASE + 0x3)
#define MESSAGING_SHORT_START				(MESSAGING_SHORT_BASE + 0x2)
#define MESSAGING_SHORT_CONTINUE			(MESSAGING_SHORT_BASE + 0x0)
#define MESSAGING_SHORT_END					(MESSAGING_SHORT_BASE + 0x1)

#define FRAME_MAX_SIZE						10
#define FRAME_MAX_START_COMPLETE_DATA_SIZE	(FRAME_MAX_SIZE - 2)
#define FRAME_MAX_CONTINUE_END_DATA_SIZE	(FRAME_MAX_SIZE - 1)

#define ARM_IPC_INTR_TO_KALIMBA				1

#define IPC_SEND_MSG_TO_ARM					0x1
#define IPC_SEND_RSP_TO_ARM					0x2
#define IPC_SEND_NO_COMPLETE					0x3
#define IPC_SEND_ACK_TO_ARM					0x4

#define EKASMSGTYPE                     					20
#define EKASPLD                         					21
#define EKASMSGRSP							22
#define EKASCRASH								23
#define EKASIPC 					                        24

int kalimba_ipc_init(void);
int ipc_send_msg(u16 *msg, int size, u32 need_ack_rsp, u16 *resp);
#endif
