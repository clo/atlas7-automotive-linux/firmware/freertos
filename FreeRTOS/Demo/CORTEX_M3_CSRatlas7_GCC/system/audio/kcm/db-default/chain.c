/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const struct kasdb_chain chain[] = {
	/* Music */
	{
		.name = __S("chain_music_2"),
		.trg_fe_name = __S("Music"),
		.trg_channels = 2,
		.links = __S("lk_music_src;lk_src_srcsync;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Music I2S output */
	{
		.name = __S("chain_music_i2s_2"),
		.trg_fe_name = __S("Music-i2s"),
		.trg_channels = 2,
		.links = __S("lk_musici2s_src;lk_src_srcsync;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_i2s;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_2;chain_navi;chain_alarm;chain_chime;"
			"chain_cvc_send_1mic;chain_cvc_send_2mic;chain_lin_to_lout_2;"
			"chain_a2dp_2ch;chain_voicecall_playback;"
			"chain_i2s_to_iacc_2;chain_tunex;"
			"chain_usp0_2ch;chain_usp2_2ch"),
	},

	/* Navigation */
	{
		.name = __S("chain_navi"),
		.trg_fe_name = __S("Navigation"),
		.trg_channels = 2,
		.links = __S("lk_navi_srcsync;lk_srcsync_mixer;lk_mixer_mixer2;"
			"lk_mixer2_volctrl;lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Alarm */
	{
		.name = __S("chain_alarm"),
		.trg_fe_name = __S("Alarm"),
		.trg_channels = 1,
		.links = __S("lk_alarm_src;lk_src_srcsync_alarm;"
			"lk_srcsync_split_1x2;lk_alarm_split;"
			"lk_alarm_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Chime */
	{
		.name = __S("chain_chime"),
		.trg_fe_name = __S("Chime"),
		.trg_channels = 4,
		.links = __S("lk_chime_src;"
			"lk_chime_srcsync;lk_chime_mixer;lk_mixer_mixer2;"
			"lk_mixer2_volctrl;lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* CVC Voice call */
	{
		.name = __S("chain_cvc_send_1mic"),
		.trg_fe_name = __S("Voicecall-iacc-to-bt"),
		.trg_channels = 1,
		.cvc_mic = single,
		.links = __S("lk_usp3_cvc_recv;lk_cvc_recv_src;"
			"lk_src_srcsync_cvc;lk_srcsync_split1x2_cvc;"
			"lk_split1x2_mixer2_cvc;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_codec_aecref_1mic;"
			"lk_volctrl_aecref;lk_aecref_cvc_send_1mic;"
			"lk_cvc_send_usp3;lk_aecref_cvc_send_ref"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cap_mono;"
			"chain_cap_stereo;chain_a2dp_2ch;"
			"chain_voicecall_capture_1mic;"
			"chain_voicecall_capture_2mic;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},
	{
		.name = __S("chain_cvc_send_2mic"),
		.trg_fe_name = __S("Voicecall-iacc-to-bt"),
		.trg_channels = 1,
		.cvc_mic = doub,
		.links = __S("lk_usp3_cvc_recv;lk_cvc_recv_src;"
			"lk_src_srcsync_cvc;lk_srcsync_split1x2_cvc;"
			"lk_split1x2_mixer2_cvc;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_codec_aecref_2mic;"
			"lk_volctrl_aecref;lk_aecref_cvc_send_2mic;"
			"lk_cvc_send_usp3;lk_aecref_cvc_send_ref"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cap_mono;"
			"chain_cap_stereo;chain_a2dp_2ch;"
			"chain_voicecall_capture_1mic;"
			"chain_voicecall_capture_2mic;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},
	{
		.name = __S("chain_cvc_recv"),
		.trg_fe_name = __S("Voicecall-bt-to-iacc"),
		.trg_channels = 1,
		.links = __S(NULL),
		.mutexs = __S("chain_a2dp_2ch;chain_voicecall_playback;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Microphone */
	{
		.name = __S("chain_cap_mono"),
		.trg_fe_name = __S("AnalogCapture"),
		.trg_channels = 1,
		.links = __S("lk_codec_src_1ch;lk_src_pass_1ch;"
			"lk_pass_cap_1ch"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cvc_send_1mic;"
			"chain_cvc_send_2mic;chain_voicecall_capture_1mic;"
			"chain_voicecall_capture_2mic"),
	},
	{
		.name = __S("chain_cap_stereo"),
		.trg_fe_name = __S("AnalogCapture"),
		.trg_channels = 2,
		.links = __S("lk_codec_src_2ch;lk_src_pass_2ch;"
			"lk_pass_cap_2ch"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cvc_send_1mic;"
			"chain_cvc_send_1mic;chain_voicecall_capture_1mic;"
			"chain_voicecall_capture_2mic"),
	},

	/* Microphone I2S input */
	{
		.name = __S("chain_cap_i2s_stereo"),
		.trg_fe_name = __S("Analog-i2s-Capture"),
		.trg_channels = 2,
		.links = __S("lk_i2s_src_2ch;lk_src_pass_2ch;"
			"lk_pass_capi2s_2ch"),
		.mutexs = __S("chain_i2s_to_iacc_2;chain_lin_to_lout_i2s_2"),
	},

	/* IACC Line-In to Line-Out */
	{
		.name = __S("chain_lin_to_lout_2"),
		.trg_fe_name = __S("Iacc-loopback-playback"),
		.trg_channels = 2,
		.links = __S("lk_codec_src;lk_src_srcsync_lin;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_cap_mono;chain_cap_stereo;"
			"chain_cvc_send_1mic;chain_cvc_send_2mic;"
			"chain_voicecall_capture_1mic;"
			"chain_voicecall_capture_2mic;"
			"chain_usp0_2ch;chain_usp2_2ch;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* I2S Line-In to Line-Out */
	{
		.name = __S("chain_lin_to_lout_i2s_2"),
		.trg_fe_name = __S("I2s-loopback-playback"),
		.trg_channels = 2,
		.links = __S("lk_i2s_srcline;lk_src_srcsync_lin;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_i2s;lk_volctrl_aecref"),
		.mutexs = __S("chain_cap_i2s_stereo;"
			"chain_music_2;chain_navi;chain_alarm;chain_chime;"
			"chain_cvc_send_1mic;chain_cvc_send_2mic;chain_lin_to_lout_2;"
			"chain_a2dp_2ch;chain_voicecall_playback;"
			"chain_i2s_to_iacc_2;chain_tunex;"
			"chain_usp0_2ch;chain_usp2_2ch"),
	},

	/* A2DP */
	{
		.name = __S("chain_a2dp_2ch"),
		.trg_fe_name = __S("A2DP"),
		.trg_channels = 2,
		.links = __S("lk_usp3_srcsync;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_cvc_send_1mic;chain_cvc_send_2mic;"
			"chain_cvc_recv;chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* USP0 Playback */
	{
		.name = __S("chain_usp0_2ch"),
		.trg_fe_name = __S("USP0"),
		.trg_channels = 2,
		.links = __S("lk_usp0_src;lk_src_srcsync_radio;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_i2s_to_iacc_2;chain_usp2_2ch;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* USP2 Playback */
	{
		.name = __S("chain_usp2_2ch"),
		.trg_fe_name = __S("USP2"),
		.trg_channels = 2,
		.links = __S("lk_usp2_src;lk_src_srcsync_radio;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_i2s_to_iacc_2;chain_usp0_2ch;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Carplay */
	{
		.name = __S("chain_voicecall_capture_1mic"),
		.trg_fe_name = __S("Voicecall-capture"),
		.trg_channels = 1,
		.cvc_mic = single,
		.links = __S("lk_codec_aecref_1mic;lk_aecref_cvc_send_1mic;"
			"lk_aecref_cvc_send_ref;lk_cvc_send_vocall_cap"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cap_mono;"
			"chain_cap_stereo;chain_cvc_send_1mic;"
			"chain_cvc_send_2mic"),
	},
	{
		.name = __S("chain_voicecall_capture_2mic"),
		.trg_fe_name = __S("Voicecall-capture"),
		.trg_channels = 1,
		.cvc_mic = doub,
		.links = __S("lk_codec_aecref_2mic;lk_aecref_cvc_send_2mic;"
			"lk_aecref_cvc_send_ref;lk_cvc_send_vocall_cap"),
		.mutexs = __S("chain_lin_to_lout_2;chain_cap_mono;"
			"chain_cap_stereo;chain_cvc_send_1mic;"
			"chain_cvc_send_2mic"),
	},
	{
		.name = __S("chain_voicecall_playback"),
		.trg_fe_name = __S("Voicecall-playback"),
		.trg_channels = 1,
		.links = __S("lk_vocall_play_cvc_recv;"
			"lk_cvc_recv_src;lk_src_srcsync_cvc;"
			"lk_srcsync_split1x2_cvc;lk_split1x2_mixer2_cvc;"
			"lk_mixer2_volctrl;lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_cvc_recv;chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* I2S to IACC loop */
	{
		.name = __S("chain_i2s_to_iacc_2"),
		.trg_fe_name = __S("I2S-to-iacc-loopback"),
		.trg_channels = 2,
		.links = __S("lk_i2s_src;lk_src_srcsync_radio;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_usp0_2ch;chain_usp2_2ch;"
			"chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},

	/* Tunex */
	{
		.name = __S("chain_tunex"),
		.trg_fe_name = __S("Tunex"),
		.trg_channels = 2,
		.links = __S("lk_tunex_src;lk_tunex_src_srcsync;"
			"lk_srcsync_pass;lk_pass_bass;lk_bass_split;"
			"lk_split_upeq;lk_upeq_delay;lk_delay_s1peq;"
			"lk_delay_s2peq;lk_delay_s3peq;lk_delay_s4peq;"
			"lk_s1peq_mixer;lk_s2peq_mixer;lk_s3peq_mixer;"
			"lk_s4peq_mixer;lk_mixer_mixer2;lk_mixer2_volctrl;"
			"lk_aecref_codec;lk_volctrl_aecref"),
		.mutexs = __S("chain_music_i2s_2;chain_lin_to_lout_i2s_2"),
	},
};
