/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const struct kasdb_fe fe[] = {
	{
		.name = __S("Music"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Music Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Navigation"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Navigation Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_SAMPLE_RATE_48000,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Alarm"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Alaram Playback" */
		.channels_min = 1,
		.channels_max = 1,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("A2DP"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S(NULL),	/* "A2DP Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_SAMPLE_RATE_48000,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Voicecall-bt-to-iacc"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S(NULL),
		.channels_min = 1,
		.channels_max = 1,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		/* Carplay Voicecall-playback */
		.name = __S("Voicecall-playback"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S("Voicecall-playback"),
		.channels_min = 1,
		.channels_max = 1,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Iacc-loopback-playback"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S("Iacc-loopback-playback"),
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_SAMPLE_RATE_48000,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		/* Radio Playback */
		.name = __S("I2S-to-iacc-loopback"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S(NULL),
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("AnalogCapture"),
		.playback = 0,
		.internal = 0,
		.stream_name = __S("Analog Capture"),
		.channels_min = SO_CODEC_CH_MIN,
		.channels_max = SO_CODEC_CH_MAX,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(NULL),
		.source_codec = __S(CODEC_TYPE),
	},
	{
		.name = __S("Voicecall-iacc-to-bt"),
		.playback = 0,
		.internal = 1,
		.stream_name = __S(NULL),
		.channels_min = 1,
		.channels_max = 1,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(NULL),
		.source_codec = __S(CODEC_TYPE),
	},
	{
		/* Carplay Voicecall-capture */
		.name = __S("Voicecall-capture"),
		.playback = 0,
		.internal = 0,
		.stream_name = __S("Voicecall-capture"),
		.channels_min = 1,
		.channels_max = 1,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(NULL),
		.source_codec = __S(CODEC_TYPE),
	},
	{
		.name = __S("Iacc-loopback-capture"),
		.playback = 0,
		.internal = 1,
		.stream_name = __S("Iacc-loopback-capture"),
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_SAMPLE_RATE_48000,
		.formats = KCM_FORMATS,
		.sink_codec = __S(NULL),
		.source_codec = __S(CODEC_TYPE),
	},
	{
		.name = __S("USP0"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S(NULL),	/* "USP0 Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("USP2"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S(NULL),	/* "USP2 Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("I2s-loopback-playback"),
		.playback = 1,
		.internal = 1,
		.stream_name = __S("I2s-loopback-playback"),
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_SAMPLE_RATE_48000,
		.formats = KCM_FORMATS,
		.sink_codec = __S("i2s"),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Chime"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Chime Playback" */
		.channels_min = 4,
		.channels_max = 4,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Music-i2s"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Music Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S("i2s"),
		.source_codec = __S(NULL),
	},
	{
		.name = __S("Analog-i2s-Capture"),
		.playback = 0,
		.internal = 0,
		.stream_name = __S("Analog-i2s-Capture"),
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(NULL),
		.source_codec = __S("i2s"),
	},
	{
		.name = __S("Tunex"),
		.playback = 1,
		.internal = 0,
		.stream_name = __S(NULL),	/* "Tunex Playback" */
		.channels_min = 2,
		.channels_max = 2,
		.rates = KCM_RATES,
		.formats = KCM_FORMATS,
		.sink_codec = __S(CODEC_TYPE),
		.source_codec = __S(NULL),
	},
};
