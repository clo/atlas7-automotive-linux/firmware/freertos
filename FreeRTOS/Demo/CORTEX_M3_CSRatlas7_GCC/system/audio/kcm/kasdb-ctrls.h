/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef OP_CTRLS_H
#define OP_CTRLS_H

#define KCM_PEQ_ALL_BAND(name, param)	\
	name " PEQ Band1 " param ";"	\
	name " PEQ Band2 " param ";"	\
	name " PEQ Band3 " param ";"	\
	name " PEQ Band4 " param ";"	\
	name " PEQ Band5 " param ";"	\
	name " PEQ Band6 " param ";"	\
	name " PEQ Band7 " param ";"	\
	name " PEQ Band8 " param ";"	\
	name " PEQ Band9 " param ";"	\
	name " PEQ Band10 " param ";"

#define KCM_CTRLS_PEQ(name)		\
	KCM_PEQ_ALL_BAND(name, "Gain")	\
	KCM_PEQ_ALL_BAND(name, "FC")	\
	name " PEQ Bands Num;"		\
	name " PEQ Core Type;"		\
	name " PEQ Master Gain;"	\
	name " PEQ Switch Mode;"	\
	name " PEQ UCID"

#define KCM_MIXER_STREAM(name)		\
	name " Stream CH1 Gain;"	\
	name " Stream CH2 Gain;"	\
	name " Stream CH3 Gain;"	\
	name " Stream CH4 Gain;"	\
	name " Stream CH5 Gain;"	\
	name " Stream CH6 Gain;"	\
	name " Stream Vol;"		\
	name " Stream Mute;"		\
	name " Stream Ramp"

/* If a stream has no controls, fill the parameter with "NOCTRL" */
#define KCM_CTRLS_MIXER(stream1, stream2, stream3)\
	KCM_MIXER_STREAM(stream1) ";"	\
	KCM_MIXER_STREAM(stream2) ";"	\
	KCM_MIXER_STREAM(stream3)

#define KCM_CTRLS_BASICPASS(name)	\
	name " Pregain;"		\
	name " Premute"

#define KCM_CTRLS_BASS(name)		\
	name " DBE Effect Strength;"	\
	name " DBE Amp Limit;"		\
	name " DBE LP FC;"		\
	name " DBE HP FC;"		\
	name " DBE Harm Content;"	\
	name " DBE Xover FC;"		\
	name " DBE Mix Balance;"	\
	name " DBE Switch Mode;"	\
	name " DBE UCID"

#define KCM_CTRLS_DELAY(name)	\
	name " Chan1 Delay;"	\
	name " Chan2 Delay;"	\
	name " Chan3 Delay;"	\
	name " Chan4 Delay"

#define KCM_CTRLS_VOLCTRL(name)		\
	name " Vol Front Left;"		\
	name " Vol Front Right;"	\
	name " Vol Rear Left;"		\
	name " Vol Rear Right;"		\
	name " Vol Master Gain;"	\
	name " Vol Master Mute"

#define KCM_CTRLS_AECREF(name)	\
	name " CVC 2Mic Switch;"\
	"Input Path"

#define KCM_CTRLS_CVCSEND(name)	\
	name " CVC Send Mode;"	\
	name " CVC Send UCID"

#define KCM_CTRLS_CVCRECV(name)	\
	name " CVC Recv Mode;"	\
	name " CVC Recv UCID"

#define KCM_CTRLS_SOURCESYNC(name)	\
	name " Srcsync Active Stream;"	\
	name " Srcsync Purge Flag;"	\
	name " Srcsync Trans Samples"

#define KCM_CTRLS_CHMIXER(name)		\
	name " CH Mixer Gain"

#endif /* OP_CTRLS_H */
