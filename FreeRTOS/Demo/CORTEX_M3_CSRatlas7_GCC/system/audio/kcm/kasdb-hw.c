/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

const struct kasdb_hw hw[] = {
	{
		.name = __S("so_i2s"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 8,
		.def_channels = 8,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_i2s"),
		.is_sink = 1,
		.is_slave = 0,
		.max_channels = 8,
		.def_channels = 8,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_i2s_4ch"),
		.is_sink = 1,
		.is_slave = 0,
		.max_channels = 4,
		.def_channels = 4,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_i2s_6ch"),
		.is_sink = 1,
		.is_slave = 0,
		.max_channels = 6,
		.def_channels = 6,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_i2s_2mic"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 8,
		.def_channels = 8,		/* For two mic cvc */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_i2s_linein"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 8,
		.def_channels = 8,		/* For stereo linein */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_i2s_radio"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 2,
		.def_channels = 0,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
		.param = 1,    /*force i2s endpoint to slave endpoint*/
	},
	{
		.name = __S("si_usp3"),
		.is_sink = 1,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_A7CA,
		.max_channels = 4,
		.def_channels = 0,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_usp3"),
		.is_sink = 0,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_A7CA,
		.max_channels = 4,
		.def_channels = 0,	/* Stream dependent */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_usp2"),
		.is_sink = 1,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_A7CA,
		.max_channels = 4,
		.def_channels = 0,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_usp2"),
		.is_sink = 0,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_PCM2,
		.max_channels = 4,
		.def_channels = 0,	/* Stream dependent */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_usp1"),
		.is_sink = 1,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_A7CA,
		.max_channels = 4,
		.def_channels = 0,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_usp1"),
		.is_sink = 0,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_PCM1,
		.max_channels = 4,
		.def_channels = 0,	/* Stream dependent */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_usp0"),
		.is_sink = 1,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_A7CA,
		.max_channels = 4,
		.def_channels = 0,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("so_usp0"),
		.is_sink = 0,
		.is_slave = 0,
		.instance_id = ENDPOINT_PHY_DEV_PCM0,
		.max_channels = 4,
		.def_channels = 0,	/* Stream dependent */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 0,
		.bytes_per_ch = 192,
	},
	{
		.name = __S("si_iacc"),
		.is_sink = 1,
		.is_slave = 0,
		.max_channels = 4,
		.def_channels = 4,
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
		.param = 0,			/* Input path: playback */
	},
	{
		.name = __S("so_iacc"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 1,
		.def_channels = 1,		/* Stream dependent */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
		.param = 1,			/* One Mic capture */
	},
	{
		.name = __S("so_iacc_2mic"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 2,
		.def_channels = 2,		/* For two mic */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
		.param = 2,			/* Two Mic capture */
	},
	{
		.name = __S("so_iacc_linein"),
		.is_sink = 0,
		.is_slave = 0,
		.max_channels = 2,
		.def_channels = 2,		/* For stereo LineIn */
		.audio_format = 0,
		.pack_format = kasdb_pack_16,
		.def_rate = 48000,
		.bytes_per_ch = 192,
		.param = 3,			/* LineIn capture */
	},
};
