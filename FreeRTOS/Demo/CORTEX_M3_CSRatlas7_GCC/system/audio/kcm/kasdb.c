/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <memory.h>
#include "FreeRTOS.h"
#include "kasobj.h"
#include "debug.h"
#include "kasdb-ctrls.h"
#include "../kalimba.h"
#include "../i2s.h"
#include "../usp.h"
#include "../iacc.h"

#if KCM_SAMPLE_RATE_5512 != 1 << 0 || KCM_SAMPLE_RATE_192000 != 1 << 12
#error "Rate definition changed in kernel!"
#endif

#define __S(str)	(str)

#define KCM_RATES (KCM_SAMPLE_RATE_CONTINUOUS | KCM_SAMPLE_RATE_8000_48000)
#define KCM_FORMATS (KCM_FMTBIT_S16_LE)

#ifdef CONFIG_SND_SOC_SIRF_KALIMBA_KCM_CODEC_I2S
#define I2S_CODEC_ENABLE 1
#define IACC_CODEC_ENABLE 0
#define CODEC_TYPE ("i2s")
#define SI_CODEC ("si_i2s")
#define SO_CODEC ("so_i2s")
#define SO_CODEC_2MIC ("so_i2s_2mic")
#define SO_CODEC_LINEIN ("so_i2s_linein")
#define SO_CODEC_CH_MIN 2
#define SO_CODEC_CH_MAX 2
#else
#define I2S_CODEC_ENABLE 0
#define IACC_CODEC_ENABLE 1
#define CODEC_TYPE ("iacc")
#define SI_CODEC ("si_iacc")
#define SO_CODEC ("so_iacc")
#define SO_CODEC_2MIC ("so_iacc_2mic")
#define SO_CODEC_LINEIN ("so_iacc_linein")
#define SO_CODEC_CH_MIN 1
#define SO_CODEC_CH_MAX 2
#endif

#include "kasdb-hw.c"
#include "db-default/fe.c"     //TODO, which db source
#include "db-default/op.c"
#include "db-default/link.c"
#include "db-default/chain.c"

static struct kasobj_hw    __on_dram obj_hw[ARRAY_SIZE(hw)];
static struct kasobj_fe    __on_dram obj_fe[ARRAY_SIZE(fe)];
static struct kasobj_op    __on_dram obj_op[ARRAY_SIZE(op)];
static struct kasobj_link  __on_dram obj_link[ARRAY_SIZE(link)];
static struct kcm_chain    __on_dram obj_chain[ARRAY_SIZE(chain)];

void  kasdb_load_database(void)
{
	int i;

	kasobj_clean_objlist();
	memset(obj_hw, 0, sizeof(struct kasobj_hw) * ARRAY_SIZE(hw));
	memset(obj_fe, 0, sizeof(struct kasobj_fe) * ARRAY_SIZE(fe));
	memset(obj_op, 0, sizeof(struct kasobj_op) * ARRAY_SIZE(op));
	memset(obj_link, 0, sizeof(struct kasobj_link) * ARRAY_SIZE(link));
	memset(obj_chain, 0, sizeof(struct kcm_chain) * ARRAY_SIZE(chain));
	for (i = 0; i < ARRAY_SIZE(hw); i++)
		kasobj_add(&hw[i], &obj_hw[i], kasdb_elm_hw);
	for (i = 0; i < ARRAY_SIZE(fe); i++)
		kasobj_add(&fe[i], &obj_fe[i], kasdb_elm_fe);
	for (i = 0; i < ARRAY_SIZE(op); i++)
		kasobj_add(&op[i], &obj_op[i], kasdb_elm_op);
	for (i = 0; i < ARRAY_SIZE(link); i++)
		kasobj_add(&link[i], &obj_link[i], kasdb_elm_link);
	for (i = 0; i < ARRAY_SIZE(chain); i++)
		kasobj_add(&chain[i], &obj_chain[i], kasdb_elm_chain);
}
