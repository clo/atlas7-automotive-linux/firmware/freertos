/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _KCM_KASDB_H
#define _KCM_KASDB_H

#define KCM_SAMPLE_RATE_5512		(1<<0)		/* 5512Hz */
#define KCM_SAMPLE_RATE_8000		(1<<1)		/* 8000Hz */
#define KCM_SAMPLE_RATE_11025		(1<<2)		/* 11025Hz */
#define KCM_SAMPLE_RATE_16000		(1<<3)		/* 16000Hz */
#define KCM_SAMPLE_RATE_22050		(1<<4)		/* 22050Hz */
#define KCM_SAMPLE_RATE_32000		(1<<5)		/* 32000Hz */
#define KCM_SAMPLE_RATE_44100		(1<<6)		/* 44100Hz */
#define KCM_SAMPLE_RATE_48000		(1<<7)		/* 48000Hz */
#define KCM_SAMPLE_RATE_64000		(1<<8)		/* 64000Hz */
#define KCM_SAMPLE_RATE_88200		(1<<9)		/* 88200Hz */
#define KCM_SAMPLE_RATE_96000		(1<<10)		/* 96000Hz */
#define KCM_SAMPLE_RATE_176400		(1<<11)		/* 176400Hz */
#define KCM_SAMPLE_RATE_192000		(1<<12)		/* 192000Hz */
#define KCM_SAMPLE_RATE_CONTINUOUS	(1<<30)		/* continuous range */
#define KCM_SAMPLE_RATE_KNOT		(1<<31)		/* supports more non-continuos rates */
#define KCM_SAMPLE_RATE_8000_44100	(KCM_SAMPLE_RATE_8000|KCM_SAMPLE_RATE_11025|\
					 KCM_SAMPLE_RATE_16000|KCM_SAMPLE_RATE_22050|\
					 KCM_SAMPLE_RATE_32000|KCM_SAMPLE_RATE_44100)
#define KCM_SAMPLE_RATE_8000_48000	(KCM_SAMPLE_RATE_8000_44100|KCM_SAMPLE_RATE_48000)

// TODO hard coded value, asound.h
#define KCM_FMTBIT_S16_LE		(1<<2)  // TODO

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

struct kasdb_head {
#define KASDB_MAGIC	0xFACE
#define	KASDB_VERSION	0x0101	/* To match user and kernel code */
	short magic;
	short version;
	int elements;
	int reloc_off;		/* Offset of relocation table */
	int cksum;
	char data[0];
};

/* Relocation table to change string offset to pointer */
struct kasdb_reloc {
	int cnt;
	int offset[];
};

/* In user db, all string pointers are replaced with offsets to db header,
 * and recovered to pointer in kernel. This introduce unnecessary difficulty
 * to default config, which is setup in kernel. So str is defined as a union.
 */

/* Each element is appended by one kasdb_elm structure
 * XXX: must start from 0 and be continuous, see kasobj_add() in kasobj.c
 */
enum {
	kasdb_elm_hw,
	kasdb_elm_fe,
	kasdb_elm_op,
	kasdb_elm_link,
	kasdb_elm_chain,
	kasdb_elm_max,
};

struct kasdb_elm {
#define KASDB_ELM_MAGIC	0x5A
	char magic;
	char type;
	unsigned short size;
	char data[0];
};

/* Sample rates are represented in kernel by bits. sound/core/pcm_native.c
 * contains according code. Similar behaviour is copied here for user mode
 * script parser.
 */
#ifndef __KERNEL__
static inline int kasdb_rate_alsa(int rate)
{
	int i, rates[] = { 5512, 8000, 11025, 16000, 22050, 32000, 44100, 48000,
		64000, 88200, 96000, 176400, 192000 };

	for (i = 0; i < sizeof(rates)/sizeof(rates[0]); i++)
		if (rate == rates[i])
			return 1 << i;
	return 0;
}
#endif

struct kasdb_codec {
	char *name;	/* iacc, i2s */
	char *chip_name;/* Description of the codec */
	char *dai_name; /* Soc dai driver name */
	char enable;		/* 0: disable, 1: enable */
	int rate;		/* 48000, 96000 ... */
	char playback;		/* 1: support playback, 0: not support */
	char capture;		/* 1: support capture, 0: not support */
};

/* Value must be consistent with Kalimba definition */
enum {
	kasdb_pack_24r,		/* 32-bit right aligned */
	kasdb_pack_24l,		/* 32-bit left aligned */
	kasdb_pack_16,		/* 16-bit */
	kasdb_pack_24,		/* 24-bit */
};

struct kasdb_hw {
	char *name;	/* iacc, usp, i2s */
	char is_sink;		/* 1 - playback, 0 - capture */
	char is_slave;		/* 1 - slave, 0 - master */
	char instance_id;	/* Only for USP */
	char max_channels;	/* Max channels */
	char def_channels;	/* Default channels */
	char audio_format;	/* XXX: Where's the definition? */
	char pack_format;	/* kasdb_pack_24r, ..., kasdb_pack_16, ... */
	int def_rate;		/* 48000, 96000, ..., 0 - stream dependent */
	int bytes_per_ch;	/* Bytes per channel */
	int param;
};

/* Only supports one stream */
struct kasdb_fe {
	char *name;	/* Card name */
	short playback;		/* 1 - playback, 0 - capture */
	short internal;		/* 1 - Internal loopback */
	char *stream_name;
	short channels_min;
	short channels_max;
	int rates;		/* kasdb_rate_alsa(48000) | ... */
	int formats;		/* KCM_FORMAT_S16_LE | ... */
	char *sink_codec;
	char *source_codec;
};

struct kasdb_op {
#define KASDB_SRCSYNC_CH_MAX	24
	char *name;
	char *ctrl_base;	/* Base control name */
	char *ctrl_names;	/* Control names, separated by ":" */
	short cap_id;
	int rate;	/* Most operators need configure sample rate */
	union {
		int dummy;
		int resampler_custom_output;	/* 1: capture, 0: playback */
		int mixer_streams;	/* 2, 3 */
		int delay_channels;
		int bass_pair_idx;	/* 0: default use, 1~11: user use*/
		int chmixer_io;		/* number of input/output channels */
		struct {
			/* number of channels for each stream */
			char stream_ch[KASDB_SRCSYNC_CH_MAX];
			/* the output pin will be connected to each input pin */
			char input_map[KASDB_SRCSYNC_CH_MAX];
		} srcsync_cfg;
	} param;	/* Operator specific parameter */
};

struct kasdb_link {
#define KASDB_CH_MAX	8
	char *name;
	char *source_name;
	char *sink_name;
	char source_pins[KASDB_CH_MAX];	/* Pin number start from 1 */
	char sink_pins[KASDB_CH_MAX];	/* " */
	int channels;
};

struct kasdb_chain {
	char *name;
	char *trg_fe_name;	/* Trigger by which FE */
	short trg_channels;		/* Trigger by how many channels */
	enum {ignore, single, doub} cvc_mic; /* Distinguish CVC streams */
	char *links;		/* "link1:link2:xxx" */
	char *mutexs;		/* Exclusive chains "music-4:music-6" */
};

void kasdb_load_database(void);

#endif
