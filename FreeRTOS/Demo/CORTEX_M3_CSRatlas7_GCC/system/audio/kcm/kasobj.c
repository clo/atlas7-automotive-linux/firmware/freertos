/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

#include "io.h"
#include "errno.h"
#include "debug.h"
#include "misclib.h"
#include <soc_cache.h>

#include "ctypes.h"
#include "queue.h"
#include "../audio_protocol.h"
#include "../atlas7-codec.h"
#include "../iacc.h"
#include "../i2s.h"
#include "../usp.h"
#include "../kalimba.h"
#include "../buffer.h"

#include "kcm.h"
#include "kasobj.h"
#include "kasdb.h"
#include "kasop.h"

#include "kasobj/fe.c"
#include "kasobj/hw.c"
#include "kasobj/op.c"
#include "kasobj/link.c"

static struct list_head hw_list = CIRCLEQ_HEAD_INITIALIZER(hw_list);
static struct list_head fe_list = CIRCLEQ_HEAD_INITIALIZER(fe_list);
static struct list_head op_list = CIRCLEQ_HEAD_INITIALIZER(op_list);
static struct list_head link_list = CIRCLEQ_HEAD_INITIALIZER(link_list);

int __kasobj_fe_cnt;

void kasobj_clean_objlist(void)
{
	CIRCLEQ_INIT(&hw_list);
	CIRCLEQ_INIT(&fe_list);
	CIRCLEQ_INIT(&op_list);
	CIRCLEQ_INIT(&link_list);
}

static void kasobj_init_obj(struct kasobj *obj, const char *name,
		struct list_head *obj_list, int type, struct kasobj_ops *ops)
{
	obj->name = name;
	obj->life_cnt = obj->start_cnt = 0;
	obj->type = type;
	obj->ops = ops;

	CIRCLEQ_INSERT_TAIL(obj_list, obj, link);
}

static void kasobj_add_hw(const void *db, const void *db_obj)
{
	struct kasobj_hw *hw = (struct kasobj_hw *)db_obj;

	hw->db = db;
	kasobj_init_obj(&hw->obj, hw->db->name, &hw_list, kasobj_type_hw,
			&hw_ops);
}

static void kasobj_add_fe(const void *db, const void *db_obj)
{
	struct kasobj_fe *fe = (struct kasobj_fe *)db_obj;

	fe->db = db;
	kasobj_init_obj(&fe->obj, fe->db->name, &fe_list, kasobj_type_fe,
			&fe_ops);
	__kasobj_fe_cnt++;
}

static void kasobj_add_op(const void *db, const void *db_obj)
{
	struct kasobj_op *op = (struct kasobj_op *)db_obj;

	op->db = db;
	kasobj_init_obj(&op->obj, op->db->name, &op_list, kasobj_type_op,
			&op_ops);
}

static void kasobj_add_link(const void *db, const void *db_obj)
{
	struct kasobj_link *link = (struct kasobj_link *)db_obj;

	link->db = db;
	kasobj_init_obj(&link->obj, link->db->name, &link_list,
			kasobj_type_lk, &link_ops);
}

int kasobj_add(const void *db, const void *db_obj, int type)
{
	static void (*type2func[kasdb_elm_max])(const void *,const void *) = {
		kasobj_add_hw,
		kasobj_add_fe,
		kasobj_add_op,
		kasobj_add_link,
		kcm_add_chain,
	};

	if (type < 0 || type >= kasdb_elm_max) {
		kcm_err("KASOBJ: unknown element!");
		return -EINVAL;
	}

	type2func[type](db, db_obj);
	return 0;
}

static int kasobj_init_all(struct list_head *obj_list)
{
	int ret;
	struct kasobj *obj;

	CIRCLEQ_FOREACH(obj, obj_list, link) {
		if (!obj->ops->init)
			continue;
		ret = obj->ops->init(obj);
		if (ret) {
			kcm_err("KASOBJ: %s init failed!\r\n", obj->name);
			return ret;
		}
	}
	return 0;
}

struct kasobj_fe *kasobj_find_fe_by_dev_id(const int dev_id)
{
	struct kasobj *obj;
	struct kasobj_fe *fe;

	CIRCLEQ_FOREACH(obj, &fe_list, link) {
		fe = kasobj_to_fe(obj);
		/* Normally, dai_name = obj->name + " PIN" */
		if (fe->dev_id == dev_id)
			return fe;
	}
	return NULL;
}

u16 kasobj_find_dev_id_by_ep(u16 ep_id)
{
	struct kasobj *obj;
	struct kasobj_fe *fe;

	CIRCLEQ_FOREACH(obj, &fe_list, link) {
		fe = kasobj_to_fe(obj);
		/* Normally, dai_name = obj->name + " PIN" */
		if (fe->ep_id[0] == ep_id)
			return fe->dev_id;
	}

	kcm_err("KCM: %s(), can't find stream by ep_id = 0x%x\r\n", __func__, ep_id);
	return -1;
}

/* Return the kasobj_op with a squence of op_idx in op_list */
struct kasobj_op *kasobj_find_op_by_capid(const u16 capid, int op_idx)
{
	struct kasobj *obj;
	struct kasobj_op *op;
	int idx = 0;

	CIRCLEQ_FOREACH(obj, &op_list, link) {
		op = kasobj_to_op(obj);
		if (op->cap_id != capid)
			continue;
		if (op_idx == idx++)
			return op;
	}
	return NULL;
}

u32 kasobj_handle_op_ctrl(void *op_obj, bool put, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct kasobj_op *op = (struct kasobj_op *)op_obj;
	struct kasobj *obj = (struct kasobj *)op_obj;
	u32 ret;

	if (!op_obj)
		return 0;

	if (put) {
		if (!op->ctrl_put) {
			kcm_err("KASOBJ: %s invalid ctrl put()!\r\n", obj->name);
			return -1;
		}
		ret = op->ctrl_put(op, ctrl_id, value_id, value);
	} else {
		if (!op->ctrl_put) {
			kcm_err("KASOBJ: %s invalid ctrl gett()!\r\n", obj->name);
			return -1;
		}
		ret = op->ctrl_get(op, ctrl_id, value_id);
	}

	return ret;
}

int kasobj_reset(void)
{
	struct kasobj *obj;
	struct kasobj_op *op;
	struct kasobj_fe *fe;
	struct kasobj_hw *hw;
	struct kasobj_link *link;

	CIRCLEQ_FOREACH(obj, &link_list, link) {
		obj->life_cnt = 0;
		obj->start_cnt = 0;
		link = kasobj_to_link(obj);
		memset(link->conn_id, KCM_INVALID_EP_ID, 8 * sizeof(u16));
	}

	CIRCLEQ_FOREACH(obj, &op_list, link) {
		obj->life_cnt = 0;
		obj->start_cnt = 0;
		op = kasobj_to_op(obj);
		op->op_id = KCM_INVALID_EP_ID;
		op->used_sink_pins = 0;
		op->used_source_pins = 0;
		op->active_sink_pins = 0;
		op->active_source_pins = 0;
	}

	CIRCLEQ_FOREACH(obj, &fe_list, link) {
		obj->life_cnt = 0;
		obj->start_cnt = 0;
		fe = kasobj_to_fe(obj);
		memset(fe->ep_id, KCM_INVALID_EP_ID, 8 * sizeof(u16));
		fe->ep_cnt = 0;
		if (fe->ep_handle) {
			memset(fe->ep_handle, 0, sizeof(struct ep_handler));
			put_ep_handler(fe->ep_handle);
			fe->ep_handle = NULL;
		}
	}

	CIRCLEQ_FOREACH(obj, &hw_list, link) {
		obj->life_cnt = 0;
		obj->start_cnt = 0;
		hw = kasobj_to_hw(obj);
		memset(hw->ep_id, KCM_INVALID_EP_ID, 8 * sizeof(u16));
		if (hw->ep_handle) {
			memset(hw->ep_handle, 0, sizeof(struct ep_handler));
			put_ep_handler(hw->ep_handle);
			hw->ep_handle = NULL;
			hw->ep_handle_pa = 0;
		}
	}
	return 0;
}

/* Find FE, BE, OP, Link */
struct kasobj *kasobj_find_obj(const char *name, int types)
{
	struct kasobj *obj;

	/* Shall I add { } to fix this ugly code format */
	if (types & kasobj_type_hw)
		CIRCLEQ_FOREACH(obj, &hw_list, link)
			if (strcasecmp(name, obj->name) == 0)
				return obj;

	if (types & kasobj_type_fe)
		CIRCLEQ_FOREACH(obj, &fe_list, link)
			if (strcasecmp(name, obj->name) == 0)
				return obj;

	if (types & kasobj_type_op)
		CIRCLEQ_FOREACH(obj, &op_list, link)
			if (strcasecmp(name, obj->name) == 0)
				return obj;

	if (types & kasobj_type_lk)
		CIRCLEQ_FOREACH(obj, &link_list, link)
			if (strcasecmp(name, obj->name) == 0)
				return obj;

	return NULL;
}

int kasobj_init(void)
{
	int ret;

	fe_reset();
	/* Initialize all objects (order matters) */
	ret = kasobj_init_all(&fe_list);
	if (ret)
		return ret;
	ret = kasobj_init_all(&hw_list);
	if (ret)
		return ret;
	ret = kasobj_init_all(&op_list);
	if (ret)
		return ret;
	ret = kasobj_init_all(&link_list);
	if (ret)
		return ret;

	return kcm_init_chain();
}
