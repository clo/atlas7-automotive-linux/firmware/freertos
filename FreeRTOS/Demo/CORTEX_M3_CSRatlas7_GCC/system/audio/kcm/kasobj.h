/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _KCM_OBJ_H
#define _KCM_OBJ_H

#include "ctypes.h"
#include "io.h"
#include "queue.h"
#include "kasdb.h"
#include "../audio_protocol.h"
#include "../kalimba_ipc.h"

struct kasobj;
struct kasobj_param;
struct kcm_chain;

/* Interface */
struct kasobj_ops {
	int (*init)(struct kasobj *obj);
	int (*get)(struct kasobj *obj, const struct kasobj_param *param);
	int (*put)(struct kasobj *obj);
	int (*start)(struct kasobj *obj);
	int (*stop)(struct kasobj *obj);
	u16 (*get_ep)(struct kasobj *obj, unsigned pin, int is_sink);
	int (*put_ep)(struct kasobj *obj, unsigned pin, int is_sink);
	int (*start_ep)(struct kasobj *obj, unsigned pin_mask, int is_sink);
	int (*stop_ep)(struct kasobj *obj, unsigned pin_mask, int is_sink);
};

/* Only for stream dependent objects (FE, resampler, etc) */
struct kasobj_param {
	int rate;
	int channels;
	int period_size;		/* In words */
	int format;
	u32 ep_handle_pa;
	int buffer_bytes;
};

/* Object types */
enum {
	kasobj_type_cd = BIT(0),	/* Codec */
	kasobj_type_hw = BIT(1),	/* Sink/Source */
	kasobj_type_fe = BIT(2),	/* Front End */
	kasobj_type_op = BIT(3),	/* Operator */
	kasobj_type_lk = BIT(4),	/* Link */
};

/* Base class */
struct kasobj {
	const char *name;
	struct kasobj_ops *ops;

	/* Two reference counts are used here:
	 * - "life_cnt is" to track object life cycle. It's increased in get()
	 *   and decreased in put().
	 *   Some operators, such as mixer, are shared by several audio chains.
	 *   We should send IPC to create Kalimba operator only when life_cnt
	 *   changes from 0 to 1. Same rule apply to audio controller and links.
	 * - start_cnt is to track start/stop commands received. It's increased
	 *   in start() and decreased in stop().
	 *   Ex. Audio controller are shared by several audio chains and may
	 *   be started/stopped several times. We should set according hardware
	 *   registers only on the first start and last stop. Same rule apply
	 *   to operators and audio links.
	 */
	int life_cnt;
	int start_cnt;

	int type;
	CIRCLEQ_ENTRY(kasobj) link;	/* Link to objects of same type */
};
CIRCLEQ_HEAD(list_head, kasobj);

struct kasobj_codec {
	struct kasobj obj;
	const struct kasdb_codec *db;

	int rate;
};
#define kasobj_to_codec(pobj)	container_of((pobj), struct kasobj_codec, obj)

struct kasobj_hw {
	struct kasobj obj;
	const struct kasdb_hw *db;

	int channels;
	int rate;
	int param;			/* USP port: 0~3 */
	struct ep_handler *ep_handle;
	u32 ep_handle_pa;
	u32 *buff;
	size_t buff_bytes;
	u16 ep_id[8];			/* Depends on channels */
	int ep_slave_mode;
};
#define kasobj_to_hw(pobj)	container_of((pobj), struct kasobj_hw, obj)

struct kasobj_fe {
	struct kasobj obj;
	const struct kasdb_fe *db;
	struct kcm_chain *running_chain;
	struct ep_handler *ep_handle;
	int ep_cnt;
	u16 ep_id[8];			/* Depends on max channels */
	u16 dev_id;			/* The device number in ALSA */
};
#define kasobj_to_fe(pobj)	container_of((pobj), struct kasobj_fe, obj)

struct kasobj_op {
	struct kasobj obj;
	const struct kasdb_op *db;

	const struct kasop_impl *impl;	/* Operator specific implementation */
	void *context;			/* Operator specific context */
	u16 cap_id;
	u16 op_id;
	u32 used_sink_pins;		/* Occupied pins mask */
	u32 used_source_pins;
	u32 active_sink_pins;		/* Running pins mask */
	u32 active_source_pins;
	int (*ctrl_put)(struct kasobj_op *op, u16 ctrl_id, u16 value_id, u32 value);
	int (*ctrl_get)(struct kasobj_op *op, u16 ctrl_id, u16 value_id);
};
#define kasobj_to_op(pobj)	container_of((pobj), struct kasobj_op, obj)

struct kasobj_link {
	struct kasobj obj;
	const struct kasdb_link *db;

	struct kasobj *source;
	struct kasobj *sink;
	u16 conn_id[8];
};
#define kasobj_to_link(pobj)	container_of((pobj), struct kasobj_link, obj)

struct kcm_chain_obj {
	struct kasobj *obj;
	CIRCLEQ_ENTRY(kcm_chain_obj) link;
};
CIRCLEQ_HEAD(list_chain_obj_head, kcm_chain_obj);

/* Object list in ex_list */
struct kcm_chain_ex {
	struct kcm_chain *chain;
	CIRCLEQ_ENTRY(kcm_chain_ex) link;
};
CIRCLEQ_HEAD(list_chain_ex_head, kcm_chain_ex);

struct kcm_chain {
	const struct kasdb_chain *db;
	const char *name;
	const struct kasobj_fe *trg_fe;
	CIRCLEQ_ENTRY(kcm_chain) link;	/* Used by chain_list */
	/* Exclusive chains, kcm_chain_ex */
	struct list_chain_ex_head ex_list;
	struct list_chain_obj_head lk_list;
	struct list_chain_obj_head fe_list;
	struct list_chain_obj_head hw_list;
	struct list_chain_obj_head op_list;
	int prepared;			/* 0 - idle */
};
CIRCLEQ_HEAD(list_chain_head, kcm_chain);

/* Create kasobj_xxx based on kasdb_xxx */
/* Database interface is not OO, type must be designated explicitly. */
int kasobj_add(const void *db, const void *db_obj,int type);
void kcm_add_chain(const void *db, const void *db_obj);

int kasobj_init(void);
int kasobj_reset(void);
void kasobj_clean_objlist(void);
int kcm_init_chain(void);

struct kasobj *kasobj_find_obj(const char *name, int types);
struct kasobj_fe *kasobj_find_fe_by_dev_id(int dev_id);
struct kasobj_op *kasobj_find_op_by_capid(const u16 capid, int op_idx);
u16 kasobj_find_dev_id_by_ep(u16 ep_id);

u32 kasobj_handle_op_ctrl(void *op_obj, bool put, u16 ctrl_id, u16 value_id, u32 value);

/* Hack: object count */
extern int __kasobj_fe_cnt, __kasobj_codec_cnt;

#define KCM_INVALID_EP_ID	0xFFFF
extern u16 __kcm_resp[64];

#endif
