/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

static int fe_dev_idx = 0;

static void fe_parse_format(int format, int channels,
		int *audio_format, int *pack_format)
{
	*audio_format = 0;	/* XXX: Where's the definition? */

	switch (format) {
	case KCM_FORMAT_S16_LE:
		*pack_format = kasdb_pack_16;
		break;
	case KCM_FORMAT_S24_LE:
		*pack_format = kasdb_pack_24r;	/* XXX: Maybe 24l? */
		break;
	default:
		kcm_err("KASFE: unknown format!\r\n");
		*pack_format = kasdb_pack_16;
		break;
	}
}


/* Create and register CPU DAI, DAPM graph, Card DAI to kcm driver */
static int fe_init(struct kasobj *obj)
{
	int i;
	struct kasobj_fe *fe = kasobj_to_fe(obj);

	for (i = 0; i < fe->db->channels_max; i++)
		fe->ep_id[i] = KCM_INVALID_EP_ID;
	fe->ep_cnt = 0;
	fe->dev_id = fe_dev_idx++;

	return 0;
}

static int fe_get(struct kasobj *obj, const struct kasobj_param *param)
{
	int i, audio_format, pack_format;
	int ret;
	struct kasobj_fe *fe = kasobj_to_fe(obj);

	if (obj->life_cnt++) {
		kcm_debug("FE '%s' refcnt++: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	fe->ep_handle = get_ep_handler();

	if (fe->ep_handle == NULL) {
		kcm_err("KASFE: no memory for ep_handle!\r\n");
		return -ENOMEM;
	}
	fe->ep_handle->buff_length = param->buffer_bytes/4;
	fe->ep_handle->buff_addr =  (u32 *)param->ep_handle_pa;
	fe->ep_handle->read_pointer = NULL;
	fe->ep_handle->write_pointer = NULL;

	fe_parse_format(param->format, param->channels,
			&audio_format, &pack_format);
	fe->ep_cnt = param->channels;
	kcm_debug("FE '%s': ep_handle = %x, buff_addr = %x, channels = %d, rate = %d, buffer_bytes = %d \r\n",
		obj->name, fe->ep_handle, param->ep_handle_pa, param->channels, param->rate, param->buffer_bytes);

	if (fe->db->playback) {
		ret = kalimba_get_source(ENDPOINT_TYPE_FILE, 0, fe->ep_cnt,
				SYSAHB_TO_SYSTEM_MAP((u32)fe->ep_handle), fe->ep_id, __kcm_resp);
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d get source error\r\n", obj->name, ret);
	} else {
		ret = kalimba_get_sink(ENDPOINT_TYPE_FILE, 0, fe->ep_cnt,
				SYSAHB_TO_SYSTEM_MAP((u32)fe->ep_handle), fe->ep_id, __kcm_resp);
		if (ret == -EKASCRASH)
				kcm_err("FE '%s' err = %d get sink error\r\n", obj->name, ret);
	}
	for (i = 0; i < fe->ep_cnt; i++) {
		ret = kalimba_config_endpoint(fe->ep_id[i],
				ENDPOINT_CONF_AUDIO_SAMPLE_RATE,
				param->rate, __kcm_resp);
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d sample rate error\r\n", obj->name, ret);
		ret = kalimba_config_endpoint(fe->ep_id[i],
				ENDPOINT_CONF_AUDIO_DATA_FORMAT,
				audio_format, __kcm_resp);
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d audio data format error\r\n", obj->name, ret);
		ret = kalimba_config_endpoint(fe->ep_id[i],
				ENDPOINT_CONF_DRAM_PACKING_FORMAT,
				pack_format, __kcm_resp);
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d packing format error\r\n", obj->name, ret);
		ret = kalimba_config_endpoint(fe->ep_id[i],
				ENDPOINT_CONF_INTERLEAVING_MODE,
				1, __kcm_resp);		/* Needs configure? */
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d interleaveing mode error\r\n", obj->name, ret);
		ret = kalimba_config_endpoint(fe->ep_id[i],
				ENDPOINT_CONF_PERIOD_SIZE,
				param->period_size, __kcm_resp);
		if (ret == -EKASCRASH)
			kcm_err("FE '%s' err = %d period size error\r\n", obj->name, ret);
		if (!fe->db->playback) {
			ret = kalimba_config_endpoint(fe->ep_id[i],
					ENDPOINT_CONF_CLOCK_MASTER,
					1, __kcm_resp);	/* Needs configure? */
			if (ret == -EKASCRASH)
				kcm_err("FE '%s' err = %d clock error\r\n", obj->name, ret);
		}
	}
	kcm_debug("FE '%s' created\r\n", obj->name);

	return 0;
}

static int fe_put(struct kasobj *obj)
{
	int i, ret = 0;
	struct kasobj_fe *fe = kasobj_to_fe(obj);

	if (!obj->life_cnt) {
	    kcm_err("FE '%s' creating, lift cnt is 0\r\n", obj->name);
	    return -EINVAL;
	}

	if (--obj->life_cnt) {
		kcm_debug("FE '%s' refcnt--: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	if (fe->ep_id[0] != KCM_INVALID_EP_ID) {
		if (fe->db->playback)
			ret = kalimba_close_source_kcm(fe->ep_cnt, fe->ep_id, __kcm_resp);
		else
			ret = kalimba_close_sink_kcm(fe->ep_cnt, fe->ep_id, __kcm_resp);
	}
	for (i = 0; i < fe->db->channels_max; i++)
		fe->ep_id[i] = KCM_INVALID_EP_ID;
	fe->ep_cnt = 0;

	memset(fe->ep_handle, 0, sizeof(struct ep_handler));
	put_ep_handler(fe->ep_handle);
	fe->ep_handle = NULL;

	kcm_debug("FE '%s' destroyed\r\n", obj->name);

	return ret;
}

static u16 fe_get_ep(struct kasobj *obj, unsigned pin, int is_sink)
{
	struct kasobj_fe *fe = kasobj_to_fe(obj);

	if (pin >= fe->ep_cnt) {
	    kcm_err("FE '%s' too many pins \r\n", obj->name);
	    return -EINVAL;
	}

	return fe->ep_id[pin];
}

static void fe_reset(void)
{
	fe_dev_idx = 0;
}

static struct kasobj_ops fe_ops = {
	.init = fe_init,
	.get = fe_get,
	.put = fe_put,
	.get_ep = fe_get_ep,
};
