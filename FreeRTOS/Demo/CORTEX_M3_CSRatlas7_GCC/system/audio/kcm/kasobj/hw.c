/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

struct hw_name_ops {
	const char *name;
	int (*config)(struct kasobj_hw *hw);
	int (*start)(struct kasobj *obj);
	int (*stop)(struct kasobj *obj);
	int ep_type;
	int ep_phy_dev;
	int port;	/* Only for USP: 0~3 */
	u32 buff_start_addr;
};

static struct hw_name_ops *hw_find_ops(const char *name);

/* IACC implementation */
static int hw_iacc_config(struct kasobj_hw *hw)
{
	int channels = hw->channels;
	int rate = hw->rate;
	int playback = hw->db->is_sink;
	int path;
	u32 *addr;

	if (kcm_input_path)
		path = kcm_input_path;
	else
		path = hw->db->param;
	atlas7_codec_start(playback, channels, path, rate);
	iacc_setup();
	if (hw->db->is_sink)
		addr = hw->buff;
	else
		addr = hw->buff + (HW_EP_BUFF_SIZE >> 1);
	hw->ep_handle->buff_length = hw->buff_bytes / sizeof(u32);
	hw->ep_handle->buff_addr = addr;
	hw->ep_handle->read_pointer = 0;
	hw->ep_handle->write_pointer = 0;
	return 0;
}

static int hw_iacc_start(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' starting error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (obj->start_cnt++ == 0) {
		iacc_start(hw->db->is_sink, hw->channels);
		kcm_debug("HW '%s' started\r\n", obj->name);
	}
	return 0;
}

static int hw_iacc_stop(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' stopping error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (obj->start_cnt && --obj->start_cnt == 0) {
		iacc_stop(hw->db->is_sink);
		kcm_debug("HW '%s' stopped\r\n", obj->name);
	}
	return 0;
}

/* I2S implementation */
static int hw_i2s_config(struct kasobj_hw *hw)
{
	int channels = hw->channels;
	int rate = hw->rate;
	int slave = (hw->db->is_slave);
	int playback = hw->db->is_sink;
	void *addr;

	i2s_setup(channels, rate, slave, playback);
	if (hw->db->is_sink)
		addr = hw->buff;
	else
		addr = hw->buff + (HW_EP_BUFF_SIZE >> 1);
	hw->ep_handle->buff_length = hw->buff_bytes / sizeof(u32);
	hw->ep_handle->buff_addr = addr;
	hw->ep_handle->read_pointer = 0;
	hw->ep_handle->write_pointer = 0;

	sirf_i2s_params_adv(channels, rate, slave, playback);
	if (hw->db->param == 1)
		hw->ep_slave_mode = !hw->db->is_slave;
	return 0;
}

static int hw_i2s_start(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' starting error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (obj->start_cnt++ == 0) {
		sirf_i2s_start(hw->db->is_sink);
		kcm_debug("HW '%s' started\r\n", obj->name);
	}
	return 0;
}

static int hw_i2s_stop(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' starting error, lift_cnt is 0 \r\n", obj->name);
		return -EINVAL;
	}

	if (obj->start_cnt && --obj->start_cnt == 0) {
		sirf_i2s_stop(hw->db->is_sink);
		kcm_debug("HW '%s' stopped\r\n", obj->name);
	}
	return 0;
}

/* USP implementation */
static int hw_usp_config(struct kasobj_hw *hw)
{
	struct hw_name_ops *ops = hw_find_ops(hw->db->name);
	int channels = hw->channels;
	int rate = hw->rate;
	int slave = hw->db->is_slave;
	int playback = hw->db->is_sink;
	void *addr;

	kcm_debug("HW usp port: %d, channels: %d, rate: %d, slave: %d, playback: %d\r\n",
		ops->port, channels, rate, slave, playback);
	usp_setup(ops->port, channels, rate, slave, playback);
	if (hw->db->is_sink)
		addr = hw->buff;
	else
		addr = hw->buff + (HW_EP_BUFF_SIZE >> 1);
	usp_params(hw->param, hw->db->is_sink, hw->channels, hw->rate, DSP_A_MODE);
	hw->ep_handle->buff_length = hw->buff_bytes / sizeof(u32);
	hw->ep_handle->buff_addr =  addr;
	hw->ep_handle->read_pointer = 0;
	hw->ep_handle->write_pointer = 0;
	return 0;
}

static int hw_usp_start(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' starting error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}
	if (obj->start_cnt++ == 0) {
		usp_start(hw->param, hw->db->is_sink);
		kcm_debug("HW '%s' started\n", obj->name);
	}
	return 0;
}

static int hw_usp_stop(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (!obj->life_cnt) {
		kcm_err("HW '%s' stopping error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}
	if (obj->start_cnt && --obj->start_cnt == 0) {
		usp_stop(hw->param, hw->db->is_sink);
		kcm_debug("HW '%s' stopped\n", obj->name);
	}
	return 0;
}

static struct hw_name_ops _hw_name_ops[] = {
	{ "iacc", hw_iacc_config, hw_iacc_start, hw_iacc_stop,
		ENDPOINT_TYPE_IACC, ENDPOINT_PHY_DEV_IACC, 0,
		KAS_IACC_HW_EP_BUFF_START_ADDR },
	{ "usp3", hw_usp_config, hw_usp_start, hw_usp_stop,
		ENDPOINT_TYPE_USP, ENDPOINT_PHY_DEV_A7CA, 3,
		KAS_USP3_HW_EP_BUFF_START_ADDR },
	{ "usp2", hw_usp_config, hw_usp_start, hw_usp_stop,
		ENDPOINT_TYPE_USP, ENDPOINT_PHY_DEV_PCM2, 2,
		KAS_USP2_HW_EP_BUFF_START_ADDR },
	{ "usp1", hw_usp_config, hw_usp_start, hw_usp_stop,
		ENDPOINT_TYPE_USP, ENDPOINT_PHY_DEV_PCM1, 1,
		KAS_USP1_HW_EP_BUFF_START_ADDR },
	{ "usp0", hw_usp_config, hw_usp_start, hw_usp_stop,
		ENDPOINT_TYPE_USP, ENDPOINT_PHY_DEV_PCM0, 0,
		KAS_USP0_HW_EP_BUFF_START_ADDR },
	{ "i2s", hw_i2s_config, hw_i2s_start, hw_i2s_stop,
		ENDPOINT_TYPE_I2S, ENDPOINT_PHY_DEV_I2S1, 0,
		KAS_I2S_HW_EP_BUFF_START_ADDR },
};

static struct hw_name_ops *hw_find_ops(const char *name)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(_hw_name_ops); i++) {
		if (kcm_strcasestr(name, _hw_name_ops[i].name))
			return &_hw_name_ops[i];
	}

	return NULL;
}

static int hw_init(struct kasobj *obj)
{
	int i;
	struct kasobj_hw *hw = kasobj_to_hw(obj);
	const struct kasdb_hw *db = hw->db;
	struct hw_name_ops *ops = hw_find_ops(db->name);

	/* Replace start, stop implementation */
	if (!ops) {
		kcm_err("KASHW: unknown hardware '%s'!\r\n", db->name);
		return -EINVAL;
	}

	hw->param = ops->port;
	hw->buff = (u32 *)ops->buff_start_addr;
	for (i = 0; i < db->max_channels; i++)
		hw->ep_id[i] = KCM_INVALID_EP_ID;

	return 0;
}

static int hw_start(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);
	const struct kasdb_hw *db = hw->db;
	struct hw_name_ops *ops = hw_find_ops(db->name);

	if (!obj->life_cnt)
		return 0;

	ops->start(obj);

	return 0;
}

static int hw_stop(struct kasobj *obj)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);
	const struct kasdb_hw *db = hw->db;
	struct hw_name_ops *ops = hw_find_ops(db->name);

	if (!obj->life_cnt)
		return 0;

	ops->stop(obj);

	return 0;
}

static int hw_get(struct kasobj *obj, const struct kasobj_param *param)
{
	int i;
	int ret = 0;
	struct kasobj_hw *hw = kasobj_to_hw(obj);
	const struct kasdb_hw *db = hw->db;
	struct hw_name_ops *ops = hw_find_ops(db->name);

	if (obj->life_cnt++) {
		kcm_debug("HW '%s' refcnt++: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	/* Get rate and channel count */
	hw->channels = db->def_channels;
	hw->rate = db->def_rate;
	if (hw->channels == 0)
		hw->channels = param->channels;
	if (hw->rate == 0)
		hw->rate = param->rate;

	hw->ep_slave_mode = hw->db->is_slave;
	/* Allocate audio buffer */
	hw->buff_bytes = db->bytes_per_ch * hw->channels;

	hw->ep_handle = get_ep_handler();
	if (hw->ep_handle == NULL) {
		obj->life_cnt--;
		kcm_err("KASHW '%s': no memory for ep_handle!\r\n", obj->name);
		return -ENOMEM;
	}

	/* Configure audio controller */
	ret = ops->config(hw);
	if (ret) {
		kcm_err("KASHW '%s': config hw error!\r\n", obj->name);
		return -EINVAL;
	}

	kcm_debug("%s hw buff_addr = %x, buff_length = %d \r\n",
		__func__, hw->ep_handle->buff_addr, hw->ep_handle->buff_length);

	/* Create Endpoints */
	if (db->is_sink)
		ret = kalimba_get_sink(ops->ep_type, ops->ep_phy_dev,
				hw->channels, SYSAHB_TO_SYSTEM_MAP((u32)hw->ep_handle),
				hw->ep_id, __kcm_resp);
	else
		ret = kalimba_get_source(ops->ep_type, ops->ep_phy_dev,
				hw->channels, SYSAHB_TO_SYSTEM_MAP((u32)hw->ep_handle),
				hw->ep_id, __kcm_resp);
	if (ret) {
		kcm_err("KASHW '%s': create endpoint failed\r\n", obj->name);
		return -EINVAL;
	}
	for (i = 0; i < hw->channels; i++) {
		ret = kalimba_config_endpoint(hw->ep_id[i],
				ENDPOINT_CONF_AUDIO_SAMPLE_RATE,
				hw->rate, __kcm_resp);
		if (ret) {
			kcm_err("KASHW '%s': config sample rate failed\r\n", obj->name);
			return -EINVAL;
		}
		ret = kalimba_config_endpoint(hw->ep_id[i],
				ENDPOINT_CONF_AUDIO_DATA_FORMAT,
				db->audio_format, __kcm_resp);
		if (ret) {
			kcm_err("KASHW '%s': config data format failed\r\n", obj->name);
			return -EINVAL;
		}
		ret = kalimba_config_endpoint(hw->ep_id[i],
				ENDPOINT_CONF_DRAM_PACKING_FORMAT,
				db->pack_format, __kcm_resp);
		if (ret) {
			kcm_err("KASHW '%s': config packing format failed\r\n", obj->name);
			return -EINVAL;
		}
		ret = kalimba_config_endpoint(hw->ep_id[i],
				ENDPOINT_CONF_INTERLEAVING_MODE,
				1, __kcm_resp);	/* Needs configure? */
		if (ret) {
			kcm_err("KASHW '%s': config interleaving mode failed\r\n", obj->name);
			return -EINVAL;
		}
		ret = kalimba_config_endpoint(hw->ep_id[i],
				ENDPOINT_CONF_CLOCK_MASTER,
				!hw->ep_slave_mode, __kcm_resp);/* param2: 0 - slave endpoint; 1 - master endpoint */
		if (ret) {
			kcm_err("KASHW '%s': config clock master failed\r\n", obj->name);
			return -EINVAL;
		}
	}

	kcm_debug("HW '%s' created\r\n", obj->name);
	return 0;
}

static int hw_put(struct kasobj *obj)
{
	int i, ret = 0;
	struct kasobj_hw *hw = kasobj_to_hw(obj);
	const struct kasdb_hw *db = hw->db;

	if (!obj->life_cnt)
	{
		kcm_err("HW '%s' destroying error, lift_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (--obj->life_cnt) {
		kcm_debug("HW '%s' refcnt--: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	if (obj->start_cnt) {
		kcm_err("HW '%s' destroyed error, start_cnt is still not 0\r\n", obj->name);
		return -EINVAL;
	}

	if (hw->ep_id[0] != KCM_INVALID_EP_ID) {
		if (hw->db->is_sink)
			ret = kalimba_close_sink_kcm(hw->channels, hw->ep_id,
					__kcm_resp);
		else
			ret = kalimba_close_source_kcm(hw->channels, hw->ep_id,
					__kcm_resp);
	}
	for (i = 0; i < db->max_channels; i++)
		hw->ep_id[i] = KCM_INVALID_EP_ID;

	memset(hw->ep_handle, 0, sizeof(struct ep_handler));
	put_ep_handler(hw->ep_handle);

	hw->ep_handle = NULL;
	hw->ep_handle_pa = 0;

	kcm_debug("HW '%s' destroyed\r\n", obj->name);
	return ret;
}

static u16 hw_get_ep(struct kasobj *obj, unsigned pin, int is_sink)
{
	struct kasobj_hw *hw = kasobj_to_hw(obj);

	if (pin >= hw->db->max_channels) {
		kcm_err("HW '%s' get HW ep error, too many pins\r\n", obj->name);
		return -EINVAL;
	}
	return hw->ep_id[pin];
}

static struct kasobj_ops hw_ops = {
	.init = hw_init,
	.get = hw_get,
	.put = hw_put,
	.get_ep = hw_get_ep,
	.start = hw_start,
	.stop = hw_stop,
};
