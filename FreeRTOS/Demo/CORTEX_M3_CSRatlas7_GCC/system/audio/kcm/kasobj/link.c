/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

static int link_init(struct kasobj *obj)
{
	int i;
	const int types = kasobj_type_hw | kasobj_type_fe | kasobj_type_op;
	struct kasobj_link *link = kasobj_to_link(obj);
	const struct kasdb_link *db = link->db;

	for (i = 0; i < db->channels; i++) {
		link->conn_id[i] = KCM_INVALID_EP_ID;
		if (db->source_pins[i] < 1 || db->source_pins[i] > 32 ||
				db->sink_pins[i] < 1 || db->sink_pins[i] > 32) {
			kcm_err("KASLINK '%s': invalid pin definition!\r\n", obj->name);
			return -EINVAL;
		}
	}

	link->source = kasobj_find_obj(db->source_name, types);
	link->sink = kasobj_find_obj(db->sink_name, types);
	if (!link->source || !link->sink) {
		kcm_err("KASLINK '%s': cannot find link source/sink!\r\n", obj->name);
		return -EINVAL;
	}
	return 0;
}

static int link_get(struct kasobj *obj, const struct kasobj_param *param)
{
	int i, ret;
	struct kasobj_link *link = kasobj_to_link(obj);
	const struct kasdb_link *db = link->db;
	struct kasobj *source = link->source, *sink = link->sink;

	if (obj->life_cnt++) {
		kcm_debug("LK '%s' refcnt++: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	/* Request source/sink pins and connect them */
	for (i = 0; i < db->channels; i++) {
		u16 source_ep = source->ops->get_ep(source,
				db->source_pins[i] - 1, 0);
		u16 sink_ep = sink->ops->get_ep(sink, db->sink_pins[i] - 1, 1);

		if (source_ep == KCM_INVALID_EP_ID ||
				sink_ep == KCM_INVALID_EP_ID) {
			kcm_err("KASLINK '%s': Pin confliction! ", obj->name);
			kcm_err("KASLINK '%s': Forget adding exclusive chains?\r\n", obj->name);
			return -EINVAL;
		}

		ret = kalimba_connect_endpoints(source_ep, sink_ep,
				&link->conn_id[i], __kcm_resp);
		if (ret) {
			kcm_err("KASLINK '%s': connecting failed\r\n", obj->name);
			return -EINVAL;
		}
	}

	kcm_debug("LK '%s' connected: '%s'-->'%s', %d channels\r\n",
			obj->name, source->name, sink->name, db->channels);
	return 0;
}

static int link_put(struct kasobj *obj)
{
	int i, ret;
	struct kasobj_link *link = kasobj_to_link(obj);
	const struct kasdb_link *db = link->db;
	struct kasobj *source = link->source, *sink = link->sink;

	if (!obj->life_cnt) {
		kcm_err("LK '%s' creating error, life_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (--obj->life_cnt) {
		kcm_debug("LK '%s' refcnt--: %d\r\n", obj->name, obj->life_cnt);
		return 0;
	}

	if (obj->start_cnt) {
		kcm_err("LK '%s' created, start_cnt is still not 0\r\n", obj->name);
		return -EINVAL;
	}

	ret = kalimba_disconnect_endpoints_kcm(db->channels, link->conn_id, __kcm_resp);
	if (ret) {
		kcm_err("KASLINK '%s': disconnecting failed\r\n", obj->name);
		return -EINVAL;
	}
	for (i = 0; i < db->channels; i++)
		link->conn_id[i] = KCM_INVALID_EP_ID;

	/* Free source/sink pins */
	for (i = 0; i < db->channels; i++) {
		if (source->ops->put_ep)
			source->ops->put_ep(source, db->source_pins[i] - 1, 0);
		if (sink->ops->put_ep)
			sink->ops->put_ep(sink, db->sink_pins[i] - 1, 1);
	}

	kcm_debug("LK '%s' disconnected\r\n", obj->name);
	return 0;
}

static int link_start(struct kasobj *obj)
{
	int i, ret = 0;
	struct kasobj_link *link = kasobj_to_link(obj);
	const struct kasdb_link *db = link->db;
	struct kasobj *source = link->source, *sink = link->sink;
	unsigned source_pins_mask = 0, sink_pins_mask = 0;

	if (!obj->life_cnt) {
		kcm_err("LK '%s' starting error, life_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	if (obj->start_cnt++)
		return 0;

	for (i = 0; i < db->channels; i++) {
		source_pins_mask |= BIT(db->source_pins[i] - 1);
		sink_pins_mask |= BIT(db->sink_pins[i] - 1);
	}

	if (source->ops->start_ep)
		ret = source->ops->start_ep(source, source_pins_mask, 0);
	if (sink->ops->start_ep)
		ret = sink->ops->start_ep(sink, sink_pins_mask, 1);

	return ret;
}

static int link_stop(struct kasobj *obj)
{
	int i, ret = 0;
	struct kasobj_link *link = kasobj_to_link(obj);
	const struct kasdb_link *db = link->db;
	struct kasobj *source = link->source, *sink = link->sink;
	unsigned source_pins_mask = 0, sink_pins_mask = 0;

	if (!obj->life_cnt) {
		kcm_debug("LK '%s' stopping error, life_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}

	for (i = 0; i < db->channels; i++) {
		source_pins_mask |= BIT(db->source_pins[i] - 1);
		sink_pins_mask |= BIT(db->sink_pins[i] - 1);
	}

	if (obj->start_cnt && --obj->start_cnt == 0) {
		if (source->ops->stop_ep)
			ret = source->ops->stop_ep(source, source_pins_mask, 0);
		if (sink->ops->stop_ep)
			ret = sink->ops->stop_ep(sink, sink_pins_mask, 1);
	}
	return ret;
}

static struct kasobj_ops link_ops = {
	.init = link_init,
	.get = link_get,
	.put = link_put,
	.start = link_start,
	.stop = link_stop,
};
