/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

static const struct kasop_impl *op_find_cap(int cap_id)
{
	const struct kasop_impl *impl = kcm_find_cap(cap_id);

	if (!impl) {
		kcm_err("KASOP: unsupported capability %d!\r\n", cap_id);
		return ERR_PTR(-EINVAL);
	}
	return impl;
}

static int op_init(struct kasobj *obj)
{
	struct kasobj_op *op = kasobj_to_op(obj);
	const struct kasop_impl *impl = op_find_cap(op->db->cap_id);

	if (op->impl) {
		kcm_err("KASOP(%s): double initialization?\r\n", obj->name);
		return -EINVAL;
	}

	op->op_id = KCM_INVALID_EP_ID;
	if (IS_ERR(impl))
		return PTR_ERR(impl);

	op->impl = impl;
	if (impl->init)
		return impl->init(op);
	return 0;
}

static int op_get(struct kasobj *obj, const struct kasobj_param *param)
{
	int ret = 0;
	struct kasobj_op *op = kasobj_to_op(obj);

	if (obj->life_cnt++ == 0) {
		op->cap_id = op->db->cap_id;
		if (op->impl->prepare)
			ret = op->impl->prepare(op, param);
		ret = kalimba_create_operator(op->cap_id, &op->op_id, __kcm_resp);
		if (ret)
			return -EINVAL;
		if (op->impl->create)
			ret = op->impl->create(op, param);
		kcm_debug("OP '%s' created, id = 0x%X\r\n", obj->name, op->op_id);
	} else if (op->impl->reconfig) {
		/* Some OP(resampler) depends on stream property and requires
		 * re-configuration even it's already created.
		 */
		ret = op->impl->reconfig(op, param);
		kcm_debug("OP '%s' refcnt++: %d\r\n", obj->name, obj->life_cnt);
	}
	return ret;
}

static int op_put(struct kasobj *obj)
{
	struct kasobj_op *op = kasobj_to_op(obj);
	int ret;

	if(!obj->life_cnt) {
		kcm_err("OP '%s' destroying error, life_cnt is 0\r\n", obj->name);
		return -EINVAL;
	}
	if (--obj->life_cnt == 0) {
		if(obj->start_cnt) {
			kcm_err("OP '%s' destroyed error, life_cnt is still 0\r\n", obj->name);
			return -EINVAL;
		}
		ret = kalimba_destroy_operator_kcm(&op->op_id, 1, __kcm_resp);
		if (ret)
			return -EINVAL;
		op->op_id = KCM_INVALID_EP_ID;
		op->used_sink_pins = op->used_source_pins = 0;
		op->active_sink_pins = op->active_source_pins = 0;
		kcm_debug("OP '%s' destroyed\r\n", obj->name);
	} else {
		kcm_debug("OP '%s' refcnt--: %d\r\n", obj->name, obj->life_cnt);
	}
	return 0;
}

static u16 op_get_ep(struct kasobj *obj, unsigned pin, int is_sink)
{
	u16 ep_id;
	struct kasobj_op *op = kasobj_to_op(obj);

	if (op->op_id == KCM_INVALID_EP_ID) {
		kcm_err("OP '%s' creating endpoint, op_id is invalid\r\n", obj->name);
		return -EINVAL;
	}

	if (pin >= 32) {
		kcm_err("OP '%s' creating endpoint, pin is invalid\r\n", obj->name);
		return -EINVAL;
	}

	if (is_sink) {
		if (op->used_sink_pins & BIT(pin)) {
			kcm_err("KASOP(%s): sink pin %d occupied!\r\n",
					obj->name, pin);
			return KCM_INVALID_EP_ID;
		}
		op->used_sink_pins |= BIT(pin);
		ep_id = op->op_id + pin + 0xA000;
	} else {
		if (op->used_source_pins & BIT(pin)) {
			kcm_err("KASOP(%s): source pin %d occupied!\r\n",
					obj->name, pin);
			return KCM_INVALID_EP_ID;
		}
		op->used_source_pins |= BIT(pin);
		ep_id = op->op_id + pin + 0x2000;
	}

	return ep_id;
}

static int op_put_ep(struct kasobj *obj, unsigned pin, int is_sink)
{
	struct kasobj_op *op = kasobj_to_op(obj);

	if (op->op_id == KCM_INVALID_EP_ID) {
		kcm_err("OP '%s' destroying endpoint, op_id is invalid\r\n", obj->name);
		return -EINVAL;
	}

	if (pin >= 32) {
		kcm_err("OP '%s', to many pins(%d)\r\n", obj->name, pin);
		return -EINVAL;
	}

	if (is_sink)
		op->used_sink_pins &= ~BIT(pin);
	else
		op->used_source_pins &= ~BIT(pin);

	return 0;
}

static int op_start_ep(struct kasobj *obj, unsigned pin_mask, int is_sink)
{
	struct kasobj_op *op = kasobj_to_op(obj);
	int ret = 0;

	if (op->op_id == KCM_INVALID_EP_ID) {
		kcm_err("OP '%s' starting endpoint, op_id is invalid\r\n", obj->name);
		return -EINVAL;
	}

	if (is_sink) {
		op->active_sink_pins |= pin_mask;
		if ((op->used_sink_pins & pin_mask) != pin_mask) {
			kcm_err("KASOP(%s): sink pins error!\r\n", obj->name);
			return -EINVAL;
		}
	} else {
		op->active_source_pins |= pin_mask;
		if ((op->used_source_pins & pin_mask) != pin_mask) {
			kcm_err("KASOP(%s): source pins error!\r\n", obj->name);
			return -EINVAL;
		}
	}

	if (op->impl->trigger)
		ret = op->impl->trigger(op,
			KASOP_MAKE_EVENT(kasop_event_start_ep, is_sink));

	return ret;
}

static int op_stop_ep(struct kasobj *obj, unsigned pin_mask, int is_sink)
{
	struct kasobj_op *op = kasobj_to_op(obj);
	int ret = 0;

	if (op->op_id == KCM_INVALID_EP_ID) {
		kcm_err("OP '%s' stopping endpint, op_id is invalid\r\n", obj->name);
		return -EINVAL;
	}

	if (is_sink)
		op->active_sink_pins &= ~pin_mask;
	else
		op->active_source_pins &= ~pin_mask;

	if (op->impl->trigger)
		ret = op->impl->trigger(op,
			KASOP_MAKE_EVENT(kasop_event_stop_ep, is_sink));

	return ret;
}

static struct kasobj_ops op_ops = {
	.init = op_init,
	.get = op_get,
	.put = op_put,
	.get_ep = op_get_ep,
	.put_ep = op_put_ep,
	.start_ep = op_start_ep,
	.stop_ep = op_stop_ep,
};
