/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _KCM_KASOP_H
#define _KCM_KASOP_H

#include "errno.h"
#include <soc_cache.h>
#include "kcm.h"
#include "../kalimba.h"
#include "../buffer.h"
#include "../audio_protocol.h"

enum {
	kasop_event_start_ep,
	kasop_event_stop_ep,
	/* Cannot exceed 16 events, see following macro */
};

#define KASOP_MAKE_EVENT(event, param)	((event) | ((param) << 4))
#define KASOP_GET_EVENT(event_param)	((event_param) & 0xF)
#define KASOP_GET_PARAM(event_param)	((event_param) >> 4)
#define KASOP_MAX_SAMPLE_RATE		(48000)

struct kasop_impl {
	int (*init)(struct kasobj_op *op);
	int (*prepare)(struct kasobj_op *op, const struct kasobj_param *param);
	int (*create)(struct kasobj_op *op, const struct kasobj_param *param);
	int (*reconfig)(struct kasobj_op *op, const struct kasobj_param *param);
	int (*trigger)(struct kasobj_op *op, int event);
};

#endif
