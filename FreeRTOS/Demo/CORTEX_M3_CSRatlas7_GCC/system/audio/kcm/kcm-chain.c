/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"
#include "misclib.h"
#include "../kalimba.h"
#include "kasobj.h"
#include "kasop.h"
#include "kcm.h"

#define KCM_CHAIN_LENGTH 512

/* Object list in lk_list, fe_list, hw_list, op_list */
static struct list_chain_head chain_list = CIRCLEQ_HEAD_INITIALIZER(chain_list);

static struct kcm_chain *kcm_find_chain(const char *name)
{
	struct kcm_chain *chain;

	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		if (strcasecmp(chain->name, name) == 0)
			return chain;
	}
	return NULL;
}

#if __KCM_DEBUG > 1
static void kcm_print_list(struct list_head *list, const char *prefix)
{
	struct kcm_chain_obj *chain_obj;

	kcm_debug("%s:\r\n", prefix);
	CIRCLEQ_FOREACH(chain_obj, list, link)
		kcm_debug("  %s\r\n", chain_obj->obj->name);
}

static void kcm_print_list_ex(struct list_head *list)
{
	struct kcm_chain_ex *chain_ex;

	kcm_debug("Exclusive Chains:\r\n");
	CIRCLEQ_FOREACH(chain_ex, list, link)
		kcm_debug("  %s\r\n", chain_ex->chain->name);
}

static void kcm_print_chains(void)
{
	struct kcm_chain *chain;

	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		kcm_debug("-----------------------------------------------\r\n");
		kcm_debug("Chain: %s\r\n", chain->name);
		kcm_debug("Trigger: FE=%s, Channels=%d\r\n",
			chain->trg_fe->obj.name, chain->db->trg_channels);
		kcm_print_list(&chain->lk_list, "Link");
		kcm_print_list(&chain->fe_list, "Front End");
		kcm_print_list(&chain->op_list, "Operator");
		kcm_print_list(&chain->hw_list, "Hardware");
		kcm_print_list_ex(&chain->ex_list);
	}
	kcm_debug("-----------------------------------------------\r\n");
}
#else
static void kcm_print_chains(void)
{
}
#endif

#define CHAIN1_OBJ_MAX 800
static struct kcm_chain_obj __on_dram chain1_obj_buf[CHAIN1_OBJ_MAX];
static int __chain1_obj_cnt;

static struct kcm_chain_obj * get_chain1_obj()
{
	struct kcm_chain_obj *ptr = NULL;

	if (__chain1_obj_cnt < CHAIN1_OBJ_MAX)
	{
		ptr = &chain1_obj_buf[__chain1_obj_cnt];
		__chain1_obj_cnt++;
	}
	else
	{
		kcm_err("KASCHAIN: chain1 obj overflow (cnt = %d)\r\n",
			__chain1_obj_cnt);
	}
	return ptr;
}

#define CHAIN1_EX_MAX 400
static struct kcm_chain_ex __on_dram chain1_ex_buf[CHAIN1_EX_MAX];
static int __chain1_ex_cnt;

static struct kcm_chain_ex * get_chain1_ex()
{
	struct kcm_chain_ex *ptr = NULL;

	if (__chain1_ex_cnt < CHAIN1_EX_MAX)
	{
		ptr = &chain1_ex_buf[__chain1_ex_cnt];
		__chain1_ex_cnt++;
	}
	else
	{
		kcm_err("KASCHAIN: chain1 ex overflow \r\n");
	}
	return ptr;
}

static int kcm_init_chain1_obj(struct kcm_chain *chain,
		struct kasobj *obj)
{
	struct kcm_chain_obj *chain_obj;
	struct list_chain_obj_head *list;

	if (obj->type == kasobj_type_fe)
		list = &chain->fe_list;
	else if (obj->type == kasobj_type_hw)
		list = &chain->hw_list;
	else if (obj->type == kasobj_type_op)
		list = &chain->op_list;
	else
		return -EINVAL;

	/* Ignore if already present, most of the time the collision will be
	 * the last added one if the links are arranged in order.
	 */
	CIRCLEQ_FOREACH(chain_obj, list, link)
		if (chain_obj->obj == obj)
			return 0;

	chain_obj = get_chain1_obj();
	chain_obj->obj = obj;
	CIRCLEQ_INSERT_TAIL(list, chain_obj, link);
	return 0;
}

/* Setup FE/HW/OP/Link list */
char __on_dram lkbuf[KCM_CHAIN_LENGTH]={0};
static int kcm_init_chain1(struct kcm_chain *chain)
{
	char *lkbuf_ptr = lkbuf, *lk_name;
	struct kcm_chain_obj *chain_obj;
	struct kasobj *obj;

	/* Find trigger FE object */
	obj = kasobj_find_obj(chain->db->trg_fe_name, kasobj_type_fe);
	if (!obj) {
		kcm_err("KASCHAIN: cannot find FE '%s'!\r\n",
				chain->db->trg_fe_name);
		return -EINVAL;
	}
	chain->trg_fe = kasobj_to_fe(obj);

	if (!chain->db->links)
		return 0;	/* Dummy chain only to trigger codec */

	memset(lkbuf,0,KCM_CHAIN_LENGTH);
	if (snprintf(lkbuf, KCM_CHAIN_LENGTH, "%s",
		chain->db->links) >= KCM_CHAIN_LENGTH) {
		kcm_err("KASCHAIN(%s): links too long!\r\n", chain->name);
		return -EINVAL;
	}

	/* Setup Link object lists */
	while ((lk_name = strsep(&lkbuf_ptr, ":;"))) {
		obj = kasobj_find_obj(lk_name, kasobj_type_lk);
		if (!obj) {
			kcm_err("KASCHAIN: cannot find link '%s'!\r\n", lk_name);
			return -EINVAL;
		}

		chain_obj = get_chain1_obj();
		if (chain_obj){
			chain_obj->obj = obj;
			CIRCLEQ_INSERT_TAIL(&chain->lk_list, chain_obj, link);
		}
	}

	/* Setup FE/HW/OP object list */
	CIRCLEQ_FOREACH(chain_obj, &chain->lk_list, link) {
		struct kasobj_link *lk = kasobj_to_link(chain_obj->obj);

		if (kcm_init_chain1_obj(chain, lk->source) ||
				kcm_init_chain1_obj(chain, lk->sink))
			return -EINVAL;
	}
	return 0;
}

/* Setup exclusive list */
char __on_dram exbuf[KCM_CHAIN_LENGTH]={0};
static int kcm_init_chain1_ex(struct kcm_chain *chain)
{
	char *exbuf_ptr = exbuf, *ex_name;
	struct kcm_chain_ex *chain_ex;

	if (!chain->db->mutexs)
		return 0;	/* No exclusive list */

	memset(exbuf,0,KCM_CHAIN_LENGTH);
	if (snprintf(exbuf, KCM_CHAIN_LENGTH, "%s",
		chain->db->mutexs) >= KCM_CHAIN_LENGTH) {
		kcm_err("KASCHAIN(%s): mutexs too long!\n", chain->name);
		return -EINVAL;
	}

	while ((ex_name = strsep(&exbuf_ptr, ":;"))) {
		struct kcm_chain *exchain = kcm_find_chain(ex_name);

		if (!exchain) {
			kcm_err("KASCHAIN: cannot find ex-chain '%s'!\n",
					ex_name);
			return -EINVAL;
		}

		chain_ex = get_chain1_ex();
		chain_ex->chain = exchain;
		CIRCLEQ_INSERT_TAIL(&chain->ex_list, chain_ex, link);
	}
	return 0;
}

int kcm_init_chain(void)
{
	int ret;
	struct kcm_chain *chain;

	/* Setup FE/HW/OP/Link list for each chain */
	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		ret = kcm_init_chain1(chain);
		if (ret)
			return ret;
	}

	/* Setup exclusive list for each chain */
	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		ret = kcm_init_chain1_ex(chain);
		if (ret)
			return ret;
	}

	kcm_print_chains();
	return 0;
}

int kcm_reset_chain(void)
{
	struct kcm_chain *chain;

	/* Reset prepared for each chain */
	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		chain->prepared = 0;
	}

	CIRCLEQ_INIT(&chain_list);
	__chain1_obj_cnt = 0;
	__chain1_ex_cnt = 0;
	return 0;
}

void kcm_add_chain(const void *db, const void *db_obj)
{
	struct kcm_chain *chain = (struct kcm_chain *)db_obj;

	chain->db = db;
	chain->name = chain->db->name;
	chain->prepared = 0;
	CIRCLEQ_INIT(&chain->ex_list);
	CIRCLEQ_INIT(&chain->lk_list);
	CIRCLEQ_INIT(&chain->fe_list);
	CIRCLEQ_INIT(&chain->hw_list);
	CIRCLEQ_INIT(&chain->op_list);

	CIRCLEQ_INSERT_TAIL(&chain_list, chain, link);
}

struct kcm_chain *kasobj_find_chain_by_fe(const struct kasobj_fe *fe,
	int channels)
{
	struct kcm_chain *chain;
	int mic_num;

	if (kcm_enable_2mic_cvc)
		mic_num = 2;
	else
		mic_num = 1;

	/* Find chain by FE */
	CIRCLEQ_FOREACH(chain, &chain_list, link)
		if (chain->trg_fe == fe &&
			(chain->db->trg_channels == channels ||
			 chain->db->trg_channels == 0) &&
			(chain->db->cvc_mic == mic_num ||
			 chain->db->cvc_mic == 0))
			return chain;

	return NULL;
}

struct kcm_chain *kcm_prepare_chain(const struct kasobj_fe *fe,
		int playback, int channels)
{
	int okay = 0, mic_num;
	struct kcm_chain *chain;
	struct kcm_chain_ex *chain_ex;

	if (kcm_enable_2mic_cvc)
		mic_num = 2;
	else
		mic_num = 1;

	/* Find chain by FE */
	CIRCLEQ_FOREACH(chain, &chain_list, link) {
		if (chain->trg_fe == fe &&
			(chain->db->trg_channels == channels ||
			 chain->db->trg_channels == 0) &&
			(chain->db->cvc_mic == mic_num ||
			 chain->db->cvc_mic == 0)) {
			okay = 1;
			break;
		}
	}
	if (!okay) {
		kcm_err("KASCHAIN: cannot find chain(FE=%s, %d channels)!\r\n",
				fe->obj.name, channels);
		return ERR_PTR(-EINVAL);
	}
	if (chain->prepared) {
		kcm_err("KASCHAIN: chain '%s' busy!\r\n", chain->name);
		return ERR_PTR(-EINVAL);
	}

	/* Check exclusive chains */
	kcm_lock();
	CIRCLEQ_FOREACH(chain_ex, &chain->ex_list, link) {
		if (chain_ex->chain->prepared) {
			okay = 0;
			kcm_err("KASCHAIN: conflict with '%s'!\r\n",
					chain_ex->chain->name);
			break;
		}
	}
	if (okay)
		chain->prepared = 1;
	kcm_unlock();

	if (okay) {
		kcm_debug("Chain '%s' prepared\r\n", chain->name);
		return chain;
	} else {
		return ERR_PTR(-EBUSY);
	}
}

void kcm_unprepare_chain(struct kcm_chain *chain)
{
	chain->prepared = 0;
}

int kcm_get_chain(struct kcm_chain *chain, const struct kasobj_param *param)
{
	int ret = 0;
	struct kcm_chain_obj *chain_obj;

	if (!chain->prepared) {
		kcm_err("KASCHAIN: chain '%s' unready!\r\n", chain->name);
		return -EINVAL;
	}

	kcm_lock();

	/* Propagate get() to individual objects */
	CIRCLEQ_FOREACH(chain_obj, &chain->fe_list, link) {
		ret = chain_obj->obj->ops->get(chain_obj->obj, param);
		if (ret)
			goto out;
	}
	CIRCLEQ_FOREACH(chain_obj, &chain->hw_list, link) {
		ret = chain_obj->obj->ops->get(chain_obj->obj, param);
		if (ret)
			goto out;
	}
	CIRCLEQ_FOREACH(chain_obj, &chain->op_list, link) {
		ret = chain_obj->obj->ops->get(chain_obj->obj, param);
		if (ret)
			goto out;
	}

	/* Link must be last, it requires FE/HW/OP endpoints ready */
	CIRCLEQ_FOREACH(chain_obj, &chain->lk_list, link) {
		ret = chain_obj->obj->ops->get(chain_obj->obj, param);
		if (ret)
			goto out;
	}

out:
	if (ret) {
		/* TODO: In theory, we can revert all operations before the
		 * error point to recover a consistent internal status.
		 */
		kcm_err("KCMCHAIN: inconsistent internal status!\r\n");
		kcm_err("KCMCHAIN: cannot recover cleanly, reboot required!\r\n");
	} else {
		kcm_debug("Chain '%s' instantiated\r\n", chain->name);
	}
	kcm_unlock();

	return ret;
}

int kcm_put_chain(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret;

	kcm_lock();

	/* Link must be first, to disconnect endpoints */
	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->lk_list, link) {
		ret = chain_obj->obj->ops->put(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->op_list, link) {
		ret = chain_obj->obj->ops->put(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->hw_list, link) {
		ret = chain_obj->obj->ops->put(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->fe_list, link) {
		ret = chain_obj->obj->ops->put(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}

	chain->prepared = 0;
	kcm_debug("Chain '%s' torn down\r\n", chain->name);

	kcm_unlock();
	return 0;
}

/* Start/stop operators in batch mode
 * XXX: Following code breaks encapsulation
 */
#define KCM_CHAIN_MAX_OPS	32	/* Maximum operators in a chain */

int __kcm_start_chain_op(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret = 0, op_cnt = 0;
	u16 op_ids[KCM_CHAIN_MAX_OPS];

	CIRCLEQ_FOREACH(chain_obj, &chain->op_list, link) {
		struct kasobj_op *op = kasobj_to_op(chain_obj->obj);

		if (!op->obj.life_cnt) {
			kcm_err("OP '%s' starting, life_cnt is 0\r\n", op->obj.name);
			return -EINVAL;
		}
		if (op->obj.start_cnt++ == 0) {
			if (op_cnt >= KCM_CHAIN_MAX_OPS ||
					op->op_id == KCM_INVALID_EP_ID) {
				kcm_err("OP '%s' starting, op_cnt or op_id invalid\r\n", op->obj.name);
				return -EINVAL;
			}
			op_ids[op_cnt++] = op->op_id;
			kcm_debug("OP '%s' started\r\n", op->obj.name);
		}
	}
	if (op_cnt)
		ret = kalimba_start_operator_kcm(op_ids, op_cnt, __kcm_resp);

	return ret;
}


int __kcm_stop_chain_op(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret = 0, op_cnt = 0;
	u16 op_ids[KCM_CHAIN_MAX_OPS];

	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->op_list, link) {
		struct kasobj_op *op = kasobj_to_op(chain_obj->obj);

		if (!op->obj.life_cnt) {
			kcm_err("OP '%s' stopping, life_cnt is 0 \r\n", op->obj.name);
			return -EINVAL;
		}
		if (op->obj.start_cnt && --op->obj.start_cnt == 0) {
			if (op_cnt >= KCM_CHAIN_MAX_OPS ||
					op->op_id == KCM_INVALID_EP_ID) {
				kcm_err("OP '%s' stopping, op_cnt or op_id invalid\r\n", op->obj.name);
				return -EINVAL;
			}
			op_ids[op_cnt++] = op->op_id;
			kcm_debug("OP '%s' stopped\r\n", op->obj.name);
		}
	}
	if (op_cnt)
		ret = kalimba_stop_operator_kcm(op_ids, op_cnt, __kcm_resp);

	return ret;
}

int __kcm_start_chain_hw(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret;

	CIRCLEQ_FOREACH(chain_obj, &chain->hw_list, link) {
		ret = chain_obj->obj->ops->start(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	return 0;
}

int __kcm_stop_chain_hw(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret;

	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->hw_list, link) {
		ret = chain_obj->obj->ops->stop(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	return 0;
}

int __kcm_start_chain_link(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret;

	CIRCLEQ_FOREACH(chain_obj, &chain->lk_list, link) {
		ret = chain_obj->obj->ops->start(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	return 0;
}

int __kcm_stop_chain_link(struct kcm_chain *chain)
{
	struct kcm_chain_obj *chain_obj;
	int ret;

	CIRCLEQ_FOREACH_REVERSE(chain_obj, &chain->lk_list, link) {
		ret = chain_obj->obj->ops->stop(chain_obj->obj);
		if (ret)
			return -EINVAL;
	}
	return 0;
}

int kcm_start_chain(struct kcm_chain *chain)
{
	kcm_lock();
	if (__kcm_start_chain_link(chain))
		return -EINVAL;
	if (__kcm_start_chain_hw(chain))
		return -EINVAL;
	if (__kcm_start_chain_op(chain))
		return -EINVAL;
	kcm_unlock();

	return 0;
}

int kcm_stop_chain(struct kcm_chain *chain)
{
	kcm_lock();
	if (__kcm_stop_chain_link(chain))
		return -EINVAL;
	if (__kcm_stop_chain_hw(chain))
		return -EINVAL;
	if (__kcm_stop_chain_op(chain))
		return -EINVAL;
	kcm_unlock();

	return 0;
}
