/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"
#include "misclib.h"
#include "semphr.h"
#include "debug.h"

#include "queue.h"

#include "kasobj.h"
#include "kasop.h"
#include "kcm.h"

extern bool m3_audio_reset;
/* TODO: suspend/resume */

/* A global response buffer is safe as IPC in KCM are exclusive */
u16 __kcm_resp[64];

static int _kcm_init_status;

bool kcm_enable_2mic_cvc = false;
int  kcm_input_path = 0;

int kcm_drv_status(void)
{
	return _kcm_init_status;
}

/* Capability implementation list. Created before driver init(). */
struct _kasop_cap {
	int cap_id;
	const struct kasop_impl *impl;
	CIRCLEQ_ENTRY(_kasop_cap) link;
};

CIRCLEQ_HEAD(list_impl_head, _kasop_cap);

static struct list_impl_head _cap_impl_list = CIRCLEQ_HEAD_INITIALIZER(_cap_impl_list);

/* Called before driver init() */
const struct kasop_impl *kcm_find_cap(int cap_id)
{
	struct _kasop_cap *cap;

	CIRCLEQ_FOREACH(cap, &_cap_impl_list, link) {
		if (cap->cap_id == cap_id)
			return cap->impl;
	}
	return NULL;
}

/* Called before driver init() */
#define CAP_MAX_NUM  (15)
static struct _kasop_cap __on_dram __cap_ary[CAP_MAX_NUM];
static unsigned int __cap_ary_idx = 0;
int kcm_register_cap(int cap_id, const struct kasop_impl *impl)
{
	struct _kasop_cap *cap;

	if (kcm_find_cap(cap_id)) {
		kcm_err("KASOP: capability %d already exists!\r\n", cap_id);
		return -EINVAL;
	}

	if (__cap_ary_idx < CAP_MAX_NUM) {
		cap = &__cap_ary[__cap_ary_idx];
		__cap_ary_idx++;
	} else {
		kcm_err("KASOP: capability array is full \r\n");
		return -ENOMEM;
	}

	cap->cap_id = cap_id;
	cap->impl = impl;
	CIRCLEQ_INSERT_TAIL(&_cap_impl_list, cap, link);

	return 0;
}

#define CAP_CTX_ALL_SIZE (2048)
char __on_dram __cap_ctx_ary[CAP_CTX_ALL_SIZE];
u32 __cap_ctx_off = 0;

u32 kcm_get_cap_ctx_addr(u32 size)
{
	u32 addr = __cap_ctx_off + (u32)__cap_ctx_ary;
	if (__cap_ctx_off + size < CAP_CTX_ALL_SIZE) {
		__cap_ctx_off += size;
	} else {
		kcm_err("%s cap_ctx_ary is too small \r\n", __func__);
		addr = 0;
	}

	return addr;
}

/* Global mutex. Only used by chain and control callbacks. */

SemaphoreHandle_t _mtx;

void kcm_lock(void)
{
	xSemaphoreTake(_mtx, portMAX_DELAY);
}

void kcm_unlock(void)
{
	xSemaphoreGive(_mtx);
}

struct kasobj_fe *kcm_find_fe(int dev_id)
{
	return kasobj_find_fe_by_dev_id(dev_id);
}


/* Find s2 in s1, case insensitive. Put under lib? */
char *kcm_strcasestr(const char *s1, const char *s2)
{
	size_t l1, l2;

	l2 = strlen(s2);
	if (!l2)
		return (char *)s1;
	l1 = strlen(s1);
	while (l1 >= l2) {
		l1--;
		if (!strncasecmp(s1, s2, l2))
			return (char *)s1;
		s1++;
	}
	return NULL;
}

int kcm_reset(void)
{
	kasobj_reset();
	kcm_reset_chain();

	return 0;
}

int kcm_init(void)
{
	if (!m3_audio_reset)
		_mtx = xSemaphoreCreateMutex();

	if (m3_audio_reset) {
		kcm_reset();
		__cap_ary_idx = 0;
		__cap_ctx_off = 0;
		CIRCLEQ_INIT(&_cap_impl_list);
	}

	/*
	 * Load user database or default database,
	 * it can be selected in menuconfig
	 */
	kasdb_load_database();
	kcm_debug("KCM: database loaded\r\n");

	kasop_init_passthr();
	kasop_init_aec_ref();
	kasop_init_bass();
	kasop_init_cvc_recv();
	kasop_init_cvc_send();
	kasop_init_delay();
	kasop_init_mixer();
	kasop_init_peq();
	kasop_init_resampler();
	kasop_init_splitter();
	kasop_init_volctrl();
	kasop_init_source_sync();
	kasop_init_chmixer();

	/* Initialize all objects and chains */
	_kcm_init_status = kasobj_init();
	kcm_debug("KCM: kcm_init_status = %d\r\n", _kcm_init_status);
	return _kcm_init_status;
}
