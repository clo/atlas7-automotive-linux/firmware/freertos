/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _KCM_KCM_H
#define _KCM_KCM_H

#include "ctypes.h"
#include "debug.h"

#define __KCM_DEBUG	0	/* 0 - disable, 1 - enable, 2 - verbose */
#define	KCM_FORMAT_S16_LE	(2)
#define	KCM_FORMAT_S24_LE 	(6)

#if __KCM_DEBUG
#define kcm_debug(...)	DebugMsg(__VA_ARGS__)
#else
#define kcm_debug(...)	do {}  while (0)
#endif

#define kcm_err(...)  DebugMsg(__VA_ARGS__)

extern bool kcm_enable_2mic_cvc;
extern bool kcm_force_iacc_cap;
extern int  kcm_input_path;

struct kcm_chain;
struct kasobj_fe;
struct kasobj_param;
struct kasop_impl;
struct snd_soc_dapm_widget;
struct snd_soc_dapm_route;
struct kcm_card_data {
	int mclk_fs;
	int fmt;
};

int kcm_drv_status(void);
void kcm_set_dev(void *dev);

void kcm_register_ctrl(void *ctrl);
struct snd_kcontrol_new *kcm_ctrl_first(void);
struct snd_kcontrol_new *kcm_ctrl_next(void);

const struct kasop_impl *kcm_find_cap(int cap_id);
int kcm_register_cap(int cap_id, const struct kasop_impl *impl);
u32 kcm_get_cap_ctx_addr(u32 size);
struct kcm_chain *kasobj_find_chain_by_fe(const struct kasobj_fe *fe, int channels);

struct kasobj_fe *kcm_find_fe(int dev_id);
struct kcm_chain *kcm_prepare_chain(const struct kasobj_fe *fe,
		int playback, int channels);
void kcm_unprepare_chain(struct kcm_chain *chain);
int kcm_get_chain(struct kcm_chain *chain, const struct kasobj_param *param);
int kcm_put_chain(struct kcm_chain *chain);
int kcm_start_chain(struct kcm_chain *chain);
int kcm_stop_chain(struct kcm_chain *chain);
int kcm_reset_chain(void);
int __kcm_start_chain_hw(struct kcm_chain *chain);
int __kcm_start_chain_link(struct kcm_chain *chain);
int __kcm_start_chain_op(struct kcm_chain *chain);
int __kcm_stop_chain_hw(struct kcm_chain *chain);
int __kcm_stop_chain_link(struct kcm_chain *chain);
int __kcm_stop_chain_op(struct kcm_chain *chain);

void kcm_lock(void);
void kcm_unlock(void);
char *kcm_strcasestr(const char *s1, const char *s2);
void kcm_set_vol_ctrl_gain(int vol);

int kcm_init(void);
int kcm_reset(void);

int kasop_init_aec_ref(void);
int kasop_init_bass(void);
int kasop_init_passthr(void);
int kasop_init_cvc_recv(void);
int kasop_init_cvc_send(void);
int kasop_init_delay(void);
int kasop_init_mixer(void);
int kasop_init_peq(void);
int kasop_init_resampler(void);
int kasop_init_splitter(void);
int kasop_init_volctrl(void);
int kasop_init_source_sync(void);
int kasop_init_chmixer(void);

#endif
