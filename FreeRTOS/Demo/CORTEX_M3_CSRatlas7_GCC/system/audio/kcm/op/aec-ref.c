/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

/* Any case will not use 48k output sample rate for Mic stream */
#define AEC_REF_DEFAULT_OUT_RATE (48000)
#define AEC_REF_MIC_PIN (2) /* The first Mic in pin */
#define CVC_2MIC_SWITCH (0)
#define CVC_INPUT_PATH	(1)

struct aec_ref_ctx {
	int mic_out_rate; /* current value of output sample rate(Mic stream) */
	int sample_rate; /* sample rate from stream parameter */
};

static void set_sample_rate(struct kasobj_op *op)
{
	struct aec_ref_ctx *ctx = op->context;
	u16 sample_rate[2];
	int ret;

	sample_rate[0] = op->db->rate;
	sample_rate[1] = ctx->mic_out_rate; /* NW/WB/UWB CVC: 8/16/24KHz */
	ret = kalimba_operator_message(op->op_id, AEC_REF_SET_SAMPLE_RATES,
		2, sample_rate, NULL, NULL, __kcm_resp);
	if (ret)
		kcm_err("KASOBJ(%s): set sample rate failed(%d)!\r\n",
			op->obj.name, ret);
}

static int aec_ref_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	int ret;

	if (ctrl_id > CVC_INPUT_PATH) {
		kcm_err("KASOBJ(%s): invalid ctrl_id(%d)!\r\n", op->obj.name, ctrl_id);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case CVC_2MIC_SWITCH:
		ret = kcm_enable_2mic_cvc;
		break;
	case CVC_INPUT_PATH:
		ret = kcm_input_path;
		break;
	default:
		break;
	}

	return ret;
}

static int aec_ref_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	if (ctrl_id > CVC_INPUT_PATH) {
		kcm_err("KASOBJ(%s): invalid ctrl_id(%d)!\r\n", op->obj.name, ctrl_id);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case CVC_2MIC_SWITCH:
		kcm_enable_2mic_cvc = (bool)value;
		break;
	case CVC_INPUT_PATH:
		/*
		 * 0- auto selection
		 * 1- one mic, 2- two mic, 3- line0in, 4- line1in,
		 * 5- line2in, 6- line3in
		 */
		if (value > 6) {
			kcm_err("KASOBJ(%s): invalid inputpath(%d)\r\n",
				op->obj.name, value);
			return -EINVAL;
		}
		kcm_input_path = (int)value;
		break;
	default:
		break;
	}

	return 0;
}

/* Create control interfaces */
static int aec_ref_init(struct kasobj_op *op)
{
	struct aec_ref_ctx *ctx;
	ctx = (struct aec_ref_ctx *)kcm_get_cap_ctx_addr(sizeof(struct aec_ref_ctx));
	if (ctx == NULL) {
		kcm_err("KASOBJ(%s): ctx malloc failed\r\n", op->obj.name);
		return -EINVAL;
	}

	ctx->mic_out_rate = AEC_REF_DEFAULT_OUT_RATE;
	op->context = ctx;

	op->ctrl_put = aec_ref_put;
	op->ctrl_get = aec_ref_get;

	return 0;
}

/* Called before the operator is created */
static int aec_ref_prepare(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	if (kcm_enable_2mic_cvc)
		op->cap_id = CAPABILITY_ID_AEC_REF_2MIC;
	else
		op->cap_id = CAPABILITY_ID_AEC_REF_1MIC;

	return 0;
}

/* Called after the operator is created */
static int aec_ref_create(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	struct aec_ref_ctx *ctx = op->context;
	u16 aec_ref_ucid = 4; /* stable user case ID */
	int ret;

	if (!op->db->rate)
		kcm_err("KASOBJ(%s): sample rate invalid(%d)!\r\n",
			op->obj.name, op->db->rate);

	ctx->sample_rate = param->rate;
	set_sample_rate(op);
	ret = kalimba_operator_message(op->op_id, OPERATOR_MSG_SET_UCID,
		1, &aec_ref_ucid, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set UCID failed(%d)!\r\n", op->obj.name, ret);
		return ret;
	}

	return 0;
}

static int aec_ref_trigger(struct kasobj_op *op, int event)
{
	struct aec_ref_ctx *ctx = op->context;
	const int is_sink = KASOP_GET_PARAM(event);

	if (!is_sink)
		return 0;

	switch (KASOP_GET_EVENT(event)) {
	case kasop_event_start_ep:
		if ((ctx->mic_out_rate == AEC_REF_DEFAULT_OUT_RATE) &&
			(op->active_sink_pins & BIT(AEC_REF_MIC_PIN))) {
			ctx->mic_out_rate = ctx->sample_rate;
			set_sample_rate(op);
		}
		break;
	case kasop_event_stop_ep:
		if (!(op->active_sink_pins & BIT(AEC_REF_MIC_PIN)) &&
			(ctx->mic_out_rate != AEC_REF_DEFAULT_OUT_RATE))
			ctx->mic_out_rate = AEC_REF_DEFAULT_OUT_RATE;
		break;
	default:
		break;
	}

	return 0;
}

static int aec_ref_reconfig(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	struct aec_ref_ctx *ctx = op->context;

	ctx->sample_rate = param->rate;

	return 0;
}

static const struct kasop_impl aec_ref_impl = {
	.init = aec_ref_init,
	.prepare = aec_ref_prepare,
	.create = aec_ref_create,
	.trigger = aec_ref_trigger,
	.reconfig = aec_ref_reconfig,
};

/* registe AEC-Ref operator */
int kasop_init_aec_ref(void)
{
	return kcm_register_cap(CAPABILITY_ID_AEC_REF_DUMMY, &aec_ref_impl);
}
