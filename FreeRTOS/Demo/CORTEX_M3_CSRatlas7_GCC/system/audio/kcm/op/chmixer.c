/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define MIN_DB		(-120)
#define STEP_DB		1
#define MAXV		(-MIN_DB / STEP_DB)
#define CHANNEL_MAX	8

static const int chmixer_gain_max[CHANNEL_MAX] =
		/* -20 * log(input_channels) */
		{ 0, -7, -10, -13, -14, -16, -17, -19 };

struct chmixer_ctx {
	int input_ch;
	int output_ch;
	int gain_max;

	/* gain[in_idx][out_idx]: the weighting of each route */
	int gain[CHANNEL_MAX * CHANNEL_MAX];
};

struct chmixer_msg {
	u16 in_ch;
	u16 out_ch;

	/* "CHANNEL_MAX * CHANNEL_MAX" is the maximum number of gain
	 * sequence of gain: gain00, gain01, gain02, gain03,
	 *		     gain10, gain11, gain12, gain13
	 */
	u16 gain[CHANNEL_MAX * CHANNEL_MAX];
};

static struct chmixer_msg __on_dram _chmixer_msg;

static int set_chmixer_param(struct kasobj_op *op)
{
	struct chmixer_ctx *ctx = op->context;
	struct chmixer_msg *msg = &_chmixer_msg;
	int ret, gain_num, idx, msg_len;

	if (!op->obj.life_cnt)
		return 0;

	msg->in_ch = ctx->input_ch;
	msg->out_ch = ctx->output_ch;
	gain_num = ctx->input_ch * ctx->output_ch;
	msg_len = gain_num + 2;
	for (idx = 0; idx < gain_num; idx++)
		msg->gain[idx] = ctx->gain[idx] * 60;

	ret = kalimba_operator_message(op->op_id, CHANNEL_MIXER_SET_PARAMETERS,
		msg_len, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set channel mixer parameters failed(%d)!\n",
			op->obj.name, ret);
		return ret;
	}

	return 0;
}

static int chmixer_get(struct kasobj_op *op, u16 ctrl_id, u16 value_idx)
{
	int value, ctrl_num;
	struct chmixer_ctx *ctx = op->context;

	ctrl_num = ctx->input_ch * ctx->output_ch;
	if (ctrl_id < 0 || ctrl_id >= ctrl_num) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}
	value =	ctx->gain[ctrl_id] + MAXV - ctx->gain_max;

	return value;
}

static int chmixer_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	int ctrl_num;
	struct chmixer_ctx *ctx = op->context;

	ctrl_num = ctx->input_ch * ctx->output_ch;
	if (ctrl_id < 0 || ctrl_id >= ctrl_num) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}
	if (value > MAXV) {
		kcm_err("KASOP(%s): channel mixer put, invalid gain value !\n",
			op->obj.name);
		return -EINVAL;
	}
	ctx->gain[ctrl_id] = value - MAXV + ctx->gain_max;

	return 0;
}

/* Create control interfaces */
static int chmixer_init(struct kasobj_op *op)
{
	struct chmixer_ctx *ctx;
	int io_num, gain_num, idx;

	ctx = (struct chmixer_ctx *)kcm_get_cap_ctx_addr(sizeof(struct chmixer_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}
	op->context = ctx;
	/* 0x00XY -> X: input channel num, Y: output channel num */
	io_num = op->db->param.chmixer_io;
	ctx->output_ch = io_num & 0x000f;
	ctx->input_ch = (io_num >> 4) & 0x000f;
	if (ctx->input_ch < 1 || ctx->output_ch < 1) {
		kcm_err("KASOP(%s): invalid input(%d)/output(%d) channels !\n",
			op->obj.name, ctx->input_ch, ctx->output_ch);
		return -EINVAL;
	}
	gain_num = ctx->input_ch * ctx->output_ch;
	ctx->gain_max = chmixer_gain_max[ctx->input_ch - 1];
	for (idx = 0; idx < gain_num; idx++)
		ctx->gain[idx] = ctx->gain_max;

	op->ctrl_put = chmixer_put;
	op->ctrl_get = chmixer_get;

	return 0;
}

/* Called after the operator is created */
static int chmixer_create(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	return set_chmixer_param(op);
}

static const struct kasop_impl chmixer_impl = {
	.init = chmixer_init,
	.create = chmixer_create,
};

/* registe channel mixer operator */
int kasop_init_chmixer(void)
{
	return kcm_register_cap(CAPABILITY_ID_CHANNEL_MIXER, &chmixer_impl);
}
