/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define CVC_SEND_DEFAULT_MODE (1)
#define CVC_SEND_DEFAULT_UCID (0x01)
#define CVC_SEND_CUST_UCID (0x02)

#define CVC_SEND_CTRL_NUM (2)
#define CVC_SEND_CTRL_MODE_IDX (0)
#define CVC_SEND_CTRL_UCID_IDX (1)

#define CVC_SEND_MODE_MAX (2)
#define CVC_SEND_UCID_MAX (2)

#define CVC_SEND_CTRL_ID_MODE (0x1)
#define CVC_SEND_CTRL_ID_MUTE (0x2)

struct cvc_send_ctx {
	u16 mode; /* 0: mute, 1: process, 2: passthrough */
	u16 ucid; /* 0x01: default setting, 0x02: tier1 predefined setting */
};

struct cvc_send_mode_msg {
	u16 block;
	u16 ctrl_id;
	u16 value_h;
	u16 value_l;
};

static int set_cvc_send_ucid(struct kasobj_op *op)
{
	struct cvc_send_ctx *ctx = op->context;
	u16 ucid;
	int ret;

	if (!op->obj.life_cnt)
		return 0;

	ucid = ctx->ucid;
	if (ucid != CVC_SEND_DEFAULT_UCID && ucid != CVC_SEND_CUST_UCID) {
		kcm_err("KASOBJ(%s): invalid UCID(0x%x)!\r\n", op->obj.name, ucid);
		return -EINVAL;
	}

	ret = kalimba_operator_message(op->op_id, OPERATOR_MSG_SET_UCID,
		1, &ucid, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set UCID failed(%d)!\r\n", op->obj.name, ret);
		return ret;
	}

	return 0;
}

static inline void send_mode_msg(struct kasobj_op *op,
	struct cvc_send_mode_msg mode_msg)
{
	int ret;

	ret = kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_CONTROL,
		4, (u16 *)&mode_msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): Set CVC Send mode failed(%d)!\r\n",
			op->obj.name, ret);
	}
}

static int set_cvc_send_mode(struct kasobj_op *op)
{
	struct cvc_send_ctx *ctx = op->context;
	struct cvc_send_mode_msg msg = {
		.block = 1,
		.value_h = 0,
	};

	if (!op->obj.life_cnt)
		return 0;

	switch (ctx->mode) {
	case 0:	/* mute */
		msg.ctrl_id = CVC_SEND_CTRL_ID_MODE;
		msg.value_l = 1; /* 1-standby will mute cvc send */
		send_mode_msg(op, msg);
		break;
	case 1: /* process */
		msg.ctrl_id = CVC_SEND_CTRL_ID_MODE;
		msg.value_l = 2;
		send_mode_msg(op, msg);
		break;
	case 2: /* passthrough */
		msg.ctrl_id = CVC_SEND_CTRL_ID_MODE;
		msg.value_l = 4; /* First mic */
		send_mode_msg(op, msg);
		break;
	default:
		kcm_err("KASOBJ(%s): Invalid mode value(%d)!\r\n",
			op->obj.name, ctx->mode);
		return -EINVAL;
	}

	return 0;
}

static int cvc_send_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	u16 value;
	struct cvc_send_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= CVC_SEND_CTRL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case CVC_SEND_CTRL_MODE_IDX:
		value = ctx->mode;
		break;
	case CVC_SEND_CTRL_UCID_IDX:
		value = ctx->ucid;
		break;
	default:
		kcm_err("KASOP(%s): CVC Send get, invalid control number !\r\n",
			op->obj.name);
		return -EINVAL;
	}
	return value;
}

static int cvc_send_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct cvc_send_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= CVC_SEND_CTRL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case CVC_SEND_CTRL_MODE_IDX:
		if (value != ctx->mode) {
			kcm_lock();
			ctx->mode = value;
			set_cvc_send_mode(op);
			kcm_unlock();
		}
		break;
	case CVC_SEND_CTRL_UCID_IDX:
		if (value != ctx->ucid) {
			/* UCID: 1 ~ 2 */
			if (value != CVC_SEND_DEFAULT_UCID &&
				value != CVC_SEND_CUST_UCID)
				return -EINVAL;
			kcm_lock();
			ctx->ucid = value;
			set_cvc_send_ucid(op);
			kcm_unlock();
		}
		break;
	default:
		kcm_err("KASOP(%s): CVC Send put, invalid control number !\r\n",
			op->obj.name);
		return -EINVAL;
	}

	return 0;
}

/* Create control interfaces */
static int cvc_send_init(struct kasobj_op *op)
{
	struct cvc_send_ctx *ctx;
	ctx = (struct cvc_send_ctx *)kcm_get_cap_ctx_addr(sizeof(struct cvc_send_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}

	ctx->mode = CVC_SEND_DEFAULT_MODE;
	ctx->ucid = CVC_SEND_DEFAULT_UCID;
	op->context = ctx;

	op->ctrl_put = cvc_send_put;
	op->ctrl_get = cvc_send_get;

	return 0;
}

/* Called before the operator is created */
static int cvc_send_prepare(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	if (kcm_enable_2mic_cvc) {
		switch (param->rate) {
		case 8000:
			op->cap_id = CAPABILITY_ID_CVCHF2MIC_SEND_NB;
			break;
		case 16000:
			op->cap_id = CAPABILITY_ID_CVCHF2MIC_SEND_WB;
			break;
		case 24000:
			op->cap_id = CAPABILITY_ID_CVCHF2MIC_SEND_UWB;
			break;
		default:
			kcm_err("KASOBJ(%s): Unsupported sample rate(%d) for 2Mic!\r\n",
				op->obj.name, param->rate);
			return -EINVAL;
		}
	} else {
		switch (param->rate) {
		case 8000:
			op->cap_id = CAPABILITY_ID_CVCHF1MIC_SEND_NB;
			break;
		case 16000:
			op->cap_id = CAPABILITY_ID_CVCHF1MIC_SEND_WB;
			break;
		case 24000:
			op->cap_id = CAPABILITY_ID_CVCHF1MIC_SEND_UWB;
			break;
		default:
			kcm_err("KASOBJ(%s): Unsupported sample rate(%d) for 1Mic!\r\n",
				op->obj.name, param->rate);
			return -EINVAL;
		}

	}

	return 0;
}

/* Called after the operator is created */
static int cvc_send_create(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	struct cvc_send_ctx *ctx = op->context;
	int ret;

	/* Reset to default when HF call started */
	ctx->mode = CVC_SEND_DEFAULT_MODE;
	ctx->ucid = CVC_SEND_DEFAULT_UCID;

	ret = set_cvc_send_ucid(op);
	if (ret)
		return ret;

	ret = set_cvc_send_mode(op);

	return ret;
}

static const struct kasop_impl cvc_send_impl = {
	.init = cvc_send_init,
	.prepare = cvc_send_prepare,
	.create = cvc_send_create,
};

/* registe cvc send operator */
int kasop_init_cvc_send(void)
{
	return kcm_register_cap(CAPABILITY_ID_CVCHF_SEND_DUMMY, &cvc_send_impl);
}
