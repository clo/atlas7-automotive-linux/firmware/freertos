/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define MAX_CHANNELS 8
#define PARAM_LEN 12 /* (MAX_CHANNELS * 3) / 2 */
#define MSG_LEN 15 /* (3 + (MAX_CHANNELS * 3) / 2) */
#define MIN_SAMPLES 0
#define MAX_SAMPLES 768

#define DELAY_CTRL_ID_CH_BASE 0
#define DELAY_CTRL_ID_CH_1 (DELAY_CTRL_ID_CH_BASE+0)
#define DELAY_CTRL_ID_CH_2 (DELAY_CTRL_ID_CH_BASE+1)
#define DELAY_CTRL_ID_CH_3 (DELAY_CTRL_ID_CH_BASE+2)
#define DELAY_CTRL_ID_CH_4 (DELAY_CTRL_ID_CH_BASE+3)

struct delay_ctx {
	int samples[MAX_CHANNELS]; /* delay: 0 ~ 768 samples */
	int channels;
};

struct delay_msg {
	u16 block;
	u16 offset;
	u16 channels;
	u16 params[PARAM_LEN];
};

static struct delay_msg __on_dram _delay_msg;

static int set_delay_samples(struct kasobj_op *op)
{
	struct delay_ctx *ctx;
	int ret, idx, m_idx, *sp, tmp;
	struct delay_msg *msg = &_delay_msg;

	/* IPC only if operator is instantiated */
	if (!op->obj.life_cnt)
		return 0;

	ctx = op->context;
	sp = ctx->samples;
	msg->block = 1;
	msg->offset = 0;
	msg->channels = MAX_CHANNELS;

	/* Every time, set the parameters of all channels */
	for (idx = 0, m_idx = 0; (idx < MAX_CHANNELS) && m_idx < PARAM_LEN;) {
		msg->params[m_idx++] = (u16)((sp[idx] >> 8) & 0x0000ffff);
		tmp = (sp[idx++] & 0x000000ff) << 8;
		msg->params[m_idx++] = (u16)(tmp |
			((sp[idx] & 0x00ff0000) >> 16));
		msg->params[m_idx++] = (u16)(sp[idx++] & 0x0000ffff);
	}

	ret = kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_PARAMS,
		 MSG_LEN, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set parametor failed(%d)!\n",
			op->obj.name, ret);
		return ret;
	}

	return ret;
}

static int delay_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	struct delay_ctx *ctx = op->context;
	int ret;

	if (ctrl_id < 0 || ctrl_id >= ctx->channels)
	{
		kcm_err("KASOP(%s): unknown control ID!\r\n", op->obj.name);
		ret = -EINVAL;
		return ret;
	}

	ret = ctx->samples[DELAY_CTRL_ID_CH_BASE+ctrl_id];

	return ret;
}

static int delay_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct delay_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= ctx->channels)
	{
		kcm_err("KASOP(%s): unknown control ID!\r\n", op->obj.name);
		return -EINVAL;
	}

	if (value < MIN_SAMPLES || value > MAX_SAMPLES)
		return -EINVAL;

	kcm_lock();
	if (value != ctx->samples[DELAY_CTRL_ID_CH_BASE+ctrl_id]) {
		ctx->samples[DELAY_CTRL_ID_CH_BASE+ctrl_id] = value;
		set_delay_samples(op);
	}
	kcm_unlock();

	return 0;
}
/* Create control interfaces */
static int delay_init(struct kasobj_op *op)
{
	struct delay_ctx *ctx;
	int idx;

	ctx = (struct delay_ctx *)kcm_get_cap_ctx_addr(sizeof(struct delay_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}

	op->context = ctx;

	/* Current supported channels */
	ctx->channels = op->db->param.delay_channels;
	if (ctx->channels > MAX_CHANNELS) {
		kcm_err("KASOBJ(%s): channel count > %d!\r\n",
			op->obj.name, MAX_CHANNELS);
		ctx->channels = MAX_CHANNELS;
	}
	/* Set the default parameters */
	for (idx = 0; idx < MAX_CHANNELS; idx++)
		ctx->samples[idx] = MIN_SAMPLES;

	op->ctrl_put = delay_put;
	op->ctrl_get = delay_get;

	return 0;
}
/* Called after the operator is created */
static int delay_create(struct kasobj_op *op, const struct kasobj_param *param)
{
	return set_delay_samples(op);
}

static const struct kasop_impl delay_impl = {
	.init = delay_init,
	.create = delay_create,
};

/* registe Delay operator */
int kasop_init_delay(void)
{
	return kcm_register_cap(CAPABILITY_ID_DELAY, &delay_impl);
}
