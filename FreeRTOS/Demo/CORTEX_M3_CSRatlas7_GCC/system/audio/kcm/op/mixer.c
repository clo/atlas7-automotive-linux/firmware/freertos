/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"
#include "io.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define MIXER_CTRL_CH1	0
#define MIXER_CTRL_CH2	1
#define MIXER_CTRL_CH3	2
#define MIXER_CTRL_CH4	3
#define MIXER_CTRL_CH5	4
#define MIXER_CTRL_CH6	5
#define MIXER_CTRL_GAIN 6
#define MIXER_CTRL_MUTE 7
#define MIXER_CTRL_RAMP 8

#define MIXER_CTRLS_PER_STREAM 9
#define MIXER_MIN_RAMP_SAMPLES 240
#define MIXER_MAX_RAMP_SAMPLES 480000

#define MIXER_MAX_STREAMS	3
#define MIXER_MIN_STREAMS	2
#define MIXER_MAX_CHANNELS	6
#define MIXER_MAX_CH_3STREAMS	4
#define MIXER_MAX_CH_2STREAMS	6
#define MIXER_MIN_DB		(-120)
#define MIXER_STEP_DB		1
#define MIXER_MAXV		(-MIXER_MIN_DB / MIXER_STEP_DB)
#define MIXER_DEFV		MIXER_MAXV   /* May big noise if all streams are 0dB */

/* Supported pattern upto: 3streams*4channels or 2streams*6channels */
struct mixer_ctx {
	int gain[MIXER_MAX_STREAMS];
	int muted[MIXER_MAX_STREAMS];
	int ramp[2][MIXER_MAX_STREAMS];	/* 0: for volume, 1: for mute/unmute */
	int ch_gain[MIXER_MAX_STREAMS][MIXER_MAX_CHANNELS];
	u16 streams;
	u16 channels[MIXER_MAX_STREAMS];
	u16 primary_stream;	/* Starts from 1 */
};

static void set_stream_gain(struct kasobj_op *op, int samples)
{
	int i, ret;
	struct mixer_ctx *ctx = op->context;
	u16 db[MIXER_MAX_STREAMS];
	u16 msg_ramp[2];

	if (!op->obj.life_cnt)
		return;

	/* <MS_8bits> <LS_16bits> */
	msg_ramp[0] = samples >> 16;
	msg_ramp[1] = samples & 0xffff;

	for (i = 0; i < ctx->streams; i++) {
		if (ctx->muted[i])
			db[i] = -32768;
		else
			db[i] = (ctx->gain[i] - MIXER_MAXV) * 60;
	}
	ret = kalimba_operator_message(op->op_id,
			OPERATOR_MSG_SET_RAMP_NUM_SAMPLES,
			2, msg_ramp, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOP(%s): set stream ramp failed, errno is %d!\r\n",
			op->obj.name, ret);
		return;
	}
	ret = kalimba_operator_message(op->op_id, OPERATOR_MSG_SET_GAINS,
			ctx->streams, db, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOP(%s): set stream gain failed, errno is %d!\r\n",
			op->obj.name, ret);
		return;
	}
}

static void set_channel_gain(struct kasobj_op *op, int stream,
			int channel, int gain)
{
	struct mixer_ctx *ctx = op->context;
	u16 msg[3] = {1, 0, 0};
	u16 msg_ramp[2];
	int idx, ret;

	/* if muted, do not send ipc msg */
	if (!op->obj.life_cnt || ctx->muted[stream])
		return;

	/* <MS_8bits> <LS_16bits> */
	msg_ramp[0] = ctx->ramp[0][stream] >> 16;
	msg_ramp[1] = ctx->ramp[0][stream] & 0xffff;

	for (idx = 0; idx < stream; idx++)
		msg[1] += ctx->channels[idx];
	msg[1] += channel;
	msg[2] = (gain - MIXER_MAXV) * 60;

	ret = kalimba_operator_message(op->op_id,
		OPERATOR_MSG_SET_RAMP_NUM_SAMPLES,
		2, msg_ramp, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOP(%s): set channel ramp failed, errno is %d !\r\n",
			op->obj.name, ret);
		return;
	}
	ret = kalimba_operator_message(op->op_id,
		OPERATOR_MSG_SET_CHANNEL_GAINS,
		3, msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOP(%s): set channel gain failed, errno is %d !\r\n",
			op->obj.name, ret);
		return;
	}
}

static void set_primary_stream(struct kasobj_op *op)
{
	struct mixer_ctx *ctx = op->context;

	if (op->obj.life_cnt)
		kalimba_operator_message(op->op_id,
				OPERATOR_MSG_SET_PRIMARY_STREAM, 1,
				&ctx->primary_stream, NULL, NULL, __kcm_resp);
}

/* Find primary stream (last active stream) */
static int select_primary_stream(struct kasobj_op *op)
{
	int i, ch;
	struct mixer_ctx *ctx = op->context;

	/*
	 * Check first channel pin of each stream
	 * eg. 3streams, 4channels: 0,4,8
	 */
	for (i = 0, ch = 0; i < ctx->streams; ch += ctx->channels[i++])
		if (op->active_sink_pins & BIT(ch))
			return i + 1;

	return ctx->primary_stream;
}

/* Endpoint activity changed, we may have to pick new primary stream */
static void pin_changed(struct kasobj_op *op, int is_sink)
{
	if (is_sink) {
		/* Pick primary stream based on current input pins activity */
		int primary_stream = select_primary_stream(op);
		struct mixer_ctx *ctx = op->context;

		if (primary_stream != ctx->primary_stream) {
			ctx->primary_stream = primary_stream;
			set_primary_stream(op);
			kcm_debug("KASOP(%s): set primary stream to %d\r\n",
					op->obj.name, primary_stream);
		}
	}
}

static int mixer_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	int stream_idx, ctrl_idx, param_idx, value;
	struct mixer_ctx *ctx = op->context;

	ctrl_idx = ctrl_id;
	if (ctrl_idx < 0 || ctrl_idx >= ctx->streams *
		MIXER_CTRLS_PER_STREAM) {
		kcm_err("%s invalid ctrl_idx \r\n", __func__);
		return -EINVAL;
	}

	stream_idx = ctrl_idx / MIXER_CTRLS_PER_STREAM;
	param_idx = ctrl_idx % MIXER_CTRLS_PER_STREAM;
	if (stream_idx >= ctx->streams) {
		kcm_err("KASOP(%s): mixer get, invalid stream !\r\n",
			op->obj.name);
		value = 0;
		return value;
	}
	switch (param_idx) {
	case MIXER_CTRL_GAIN:
		value = ctx->gain[stream_idx];
		break;
	case MIXER_CTRL_MUTE:
		value = ctx->muted[stream_idx];
		break;
	case MIXER_CTRL_RAMP:
		value = ctx->ramp[value_id][stream_idx];
		break;
	case MIXER_CTRL_CH1:
	case MIXER_CTRL_CH2:
	case MIXER_CTRL_CH3:
	case MIXER_CTRL_CH4:
	case MIXER_CTRL_CH5:
	case MIXER_CTRL_CH6:
		value = ctx->ch_gain[stream_idx][param_idx];
		break;
	default:
		kcm_err("KASOP(%s): mixer get, invalid control number !\r\n",
			op->obj.name);
	}

	return value;
}

static int mixer_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	int stream_idx, param_idx, ctrl_idx, cnt;
	struct mixer_ctx *ctx = op->context;
	int ramp;

	ctrl_idx = ctrl_id;
	if (ctrl_idx < 0 || ctrl_idx >= ctx->streams *
		MIXER_CTRLS_PER_STREAM){
		kcm_err("%s invalid ctrl_idx \r\n", __func__);
		return -EINVAL;
	}

	stream_idx = ctrl_idx / MIXER_CTRLS_PER_STREAM;
	param_idx = ctrl_idx % MIXER_CTRLS_PER_STREAM;
	if (stream_idx >= ctx->streams) {
		kcm_err("KASOP(%s): mixer put, invalid stream !\r\n",
			op->obj.name);
		return 0;
	}
	switch (param_idx) {
	case MIXER_CTRL_GAIN:
		ctx->gain[stream_idx] = value;
		/* overwrite channel gains of the stream */
		for (cnt = 0; cnt < ctx->channels[stream_idx]; cnt++)
			ctx->ch_gain[stream_idx][cnt] = value;
		/* if muted, just save the vlaue */
		if (!ctx->muted[stream_idx]) {
			kcm_lock();
			set_stream_gain(op, ctx->ramp[0][stream_idx]);
			kcm_unlock();
		}
		break;
	case MIXER_CTRL_MUTE:
		if (ctx->muted[stream_idx] != value) {
			ctx->muted[stream_idx] = value;
			/* when unmute, send all the saved channel gain */
			kcm_lock();
			if (!value) {
				cnt = 0;
				while (cnt < ctx->channels[stream_idx]) {
					set_channel_gain(op, stream_idx, cnt,
						ctx->ch_gain[stream_idx][cnt]);
					cnt++;
				}
			} else {
				set_stream_gain(op, ctx->ramp[1][stream_idx]);
			}
			kcm_unlock();
		}
		break;
	case MIXER_CTRL_RAMP:
		ramp = value;
		if (ramp < MIXER_MIN_RAMP_SAMPLES || /* RAMP: 240 ~ 480000 */
			ramp > MIXER_MAX_RAMP_SAMPLES)
			return -EINVAL;
		if (ctx->ramp[value_id][stream_idx] != ramp)
			ctx->ramp[value_id][stream_idx] = ramp;
		break;
	case MIXER_CTRL_CH1:
	case MIXER_CTRL_CH2:
	case MIXER_CTRL_CH3:
	case MIXER_CTRL_CH4:
	case MIXER_CTRL_CH5:
	case MIXER_CTRL_CH6:
		if (ctx->ch_gain[stream_idx][param_idx] != value) {
			ctx->ch_gain[stream_idx][param_idx] = value;
			kcm_lock();
			for (cnt = 0; cnt < ctx->channels[stream_idx]; cnt++)
				set_channel_gain(op, stream_idx, cnt,
					ctx->ch_gain[stream_idx][cnt]);
			kcm_unlock();
		}
		break;
	default:
		kcm_err("KASOP(%s): mixer put, invalid control number !\r\n",
			op->obj.name);
	}

	return 0;
}

static int mixer_init(struct kasobj_op *op)
{
	u16 st, ch, max, st_config;
	struct mixer_ctx *ctx;

	ctx = (struct mixer_ctx *)kcm_get_cap_ctx_addr(sizeof(struct mixer_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}

	op->context = ctx;
	st_config = (u16)op->db->param.mixer_streams;

	/* Analysis the number of stream and channel
	 *   XXX--> channel number of stream3
	 *   ||
	 *   |+---> channel number of stream2
	 *   +----> channel number of stream1
	 * Example: 124 means 1 channel for stream1, 2 channels for stream2,
	 *   4 channels for stream3.
	 */
	if ((st_config & (~0x0fff)) || !(st_config & (~0x000f))) {
		kcm_err("KASOP(%s): Too many or too few streams!\r\n",
			op->obj.name);
		return -EINVAL;
	}
	if (st_config & 0xf00) {
		/* 3 streams */
		ctx->streams = 3;
		max = MIXER_MAX_CH_3STREAMS;
	} else {
		/* 2 streams */
		ctx->streams = 2;
		max = MIXER_MAX_CH_2STREAMS;
	}

	for (st = 0; st < ctx->streams; st++) {
		ch = st_config & 0x00f;
		if (ch > max) {
			kcm_err("KASOP(%s): Invalid channels of stream(%d)!\r\n",
				op->obj.name, st);
			return -EINVAL;
		}
		ctx->channels[ctx->streams - st - 1] = ch;
		st_config >>= 4;
	}

	for (st = 0; st < MIXER_MAX_STREAMS; st++) {
		ctx->gain[st] = MIXER_DEFV;
		ctx->muted[st] = 0;
		ctx->ramp[0][st] = 96000;
		ctx->ramp[1][st] = 240;
		for (ch = 0; ch < MIXER_MAX_CHANNELS; ch++)
			ctx->ch_gain[st][ch] = MIXER_DEFV;
	}
	if (op->db->rate == 0) {
		kcm_err("KASOP(%s): invalid sample rate!\r\n", op->obj.name);
		return -EINVAL;
	}

	op->ctrl_get = mixer_get;
	op->ctrl_put = mixer_put;

	return 0;
}

static int mixer_create(struct kasobj_op *op, const struct kasobj_param *param)
{
	struct mixer_ctx *ctx = op->context;
	u16 *stream_cfg;
	u16 rate = op->db->rate / 25;
	int st, ch;

	stream_cfg = ctx->channels;
	kalimba_operator_message(op->op_id, OPERATOR_MSG_SET_CHANNELS,
			ctx->streams, stream_cfg, NULL, NULL, __kcm_resp);
	if (rate <= 0) {
		kcm_err("KASOBJ(%s): Invalid sample rate (%d)\r\n",
			op->obj.name, rate);
		return -EINVAL;
	}
	kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_SAMPLE_RATE,
			1, &rate, NULL, NULL, __kcm_resp);
	for (st = 0; st < ctx->streams; st++) {
		if (ctx->muted[st]) {
			set_stream_gain(op, ctx->ramp[1][st]);
			continue;
		}
		for (ch = 0; ch < ctx->channels[st]; ch++)
			set_channel_gain(op, st, ch, ctx->ch_gain[st][ch]);
	}

	ctx->primary_stream = 0;

	return 0;
}

static int mixer_trigger(struct kasobj_op *op, int event)
{
	const int param = KASOP_GET_PARAM(event);

	switch (KASOP_GET_EVENT(event)) {
	case kasop_event_start_ep:
	case kasop_event_stop_ep:
		pin_changed(op, param);
		break;
	default:
		break;
	}
	return 0;
}

static struct kasop_impl mixer_impl = {
	.init = mixer_init,
	.create = mixer_create,
	.trigger = mixer_trigger,
};

int kasop_init_mixer(void)
{
	return kcm_register_cap(CAPABILITY_ID_MIXER, &mixer_impl);
}
