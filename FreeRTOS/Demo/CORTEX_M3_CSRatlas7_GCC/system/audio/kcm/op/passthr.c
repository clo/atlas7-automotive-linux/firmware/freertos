/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define PASSTHR_CTRL_ID_GAIN 0
#define PASSTHR_CTRL_ID_MUTE 1

#define MIN_DB	(-120)
#define STEP_DB	1
#define MAXV	(-MIN_DB / STEP_DB)

/* Context for each instance */
struct passthr_ctx {
	int gain;	/* gain = 0 ~ 120 => -120dB ~ 0dB */
	int muted;	/* 1 - muted, 0 - unmuted */
};

static int set_gain(struct kasobj_op *op)
{
	struct passthr_ctx *ctx = op->context;
	u16 db;
	int ret;

	/* IPC only if operator is instantiated */
	if (!op->obj.life_cnt)
		return 0;

	if (ctx->muted)
		db = -32768;	/* Minimal value */
	else
		db = (ctx->gain - MAXV) * 60;
	ret = kalimba_operator_message(op->op_id, OPERATOR_MSG_SET_PASSTHROUGH_GAIN,
			1, &db, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOP(%s): send kalimba IPC failed\r\n", op->obj.name);
		return -EINVAL;
	}

	return 0;
}

static int passthr_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	struct passthr_ctx *ctx = op->context;
	u32 ret;

	switch (ctrl_id) {
	case PASSTHR_CTRL_ID_GAIN:
		ret = ctx->gain;
		break;
	case PASSTHR_CTRL_ID_MUTE:
		ret = ctx->muted;
		break;
	default:
		kcm_err("KASOP(%s): unknown control ID %d!\r\n", op->obj.name, ctrl_id);
		ret = -EINVAL;
		break;
	}

	return ret;
}

static int passthr_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct passthr_ctx *ctx = op->context;
	int ret = 0, gain, mute;

	switch (ctrl_id) {
	case PASSTHR_CTRL_ID_GAIN:
		gain = value;
		if (gain == ctx->gain)
			break;
		kcm_lock();
		ctx->gain = gain;
		if (!ctx->muted)
			ret = set_gain(op);
		kcm_unlock();
		break;
	case PASSTHR_CTRL_ID_MUTE:
		mute = value;
		if (mute == ctx->muted)
			break;
		kcm_lock();
		ctx->muted = mute;
		ret = set_gain(op);
		kcm_unlock();
		break;
	default:
		kcm_err("KASOP(%s): unknown control ID %d!\r\n", op->obj.name, ctrl_id);
		ret = -EINVAL;
		break;
	}

	return ret;
}

static int passthr_init(struct kasobj_op *op)
{
	struct passthr_ctx *ctx;
	ctx = (struct passthr_ctx *)kcm_get_cap_ctx_addr(sizeof(struct passthr_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}
	ctx->gain = MAXV;	/* default 0dB */
	ctx->muted = 0;         /* default not muted */
	op->context = ctx;
	op->ctrl_put = passthr_put;
	op->ctrl_get = passthr_get;

	return 0;
}

/* Called after operator is created */
static int passthr_create(struct kasobj_op *op, const struct kasobj_param *parm)
{
	set_gain(op);
	return 0;
}

static struct kasop_impl passthr_impl = {
	.init = passthr_init,
	.create = passthr_create,
};

int kasop_init_passthr(void)
{
	return kcm_register_cap(CAPABILITY_ID_BASIC_PASSTHROUGH, &passthr_impl);
}
