/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

static int __on_dram rates[] = { 8000, 11025, 12000, 16000, 22050, 24000,
		32000, 44100, 48000 };

static int resampler_rate_index(int rate)
{
	int si = 0, ei = ARRAY_SIZE(rates) - 1;

	/* Yes, I'm sick enough to adopt binary search here. */
	while (si <= ei) {
		int mi = (si + ei) / 2;

		if (rate < rates[mi])
			ei = mi - 1;
		else if (rate > rates[mi])
			si = mi + 1;
		else
			return mi;
	}
	return -1;
}

static u16 resampler_conversion_rate(int input_rate, int output_rate)
{
	int input_rate_index = resampler_rate_index(input_rate);
	int output_rate_index = resampler_rate_index(output_rate);

	if (input_rate_index < 0 || output_rate_index < 0)
		return 0xFFFF;
	return (input_rate_index << 4) | output_rate_index;
}

/* Set conversion rate */
static int resampler_create(struct kasobj_op *op,
		const struct kasobj_param *param)
{
	const struct kasdb_op *db = op->db;
	int input_rate, output_rate;
	u16 conversion_rate;

	if (db->param.resampler_custom_output) {
		input_rate = db->rate;
		output_rate = param->rate;
	} else {
		input_rate = param->rate;
		output_rate = db->rate;
	}

	conversion_rate = resampler_conversion_rate(input_rate, output_rate);
	if (conversion_rate == 0xFFFF) {
		kcm_err("KASOBJ(%s): rate not supported: %d to %d\r\n",
			       op->obj.name, input_rate, output_rate);
		return -EINVAL;
	}

	kalimba_operator_message(op->op_id, RESAMPLER_SET_CONVERSION_RATE, 1,
			&conversion_rate, NULL, NULL, __kcm_resp);

	return 0;
}

static struct kasop_impl resampler_impl = {
	.create = resampler_create,
};

int kasop_init_resampler(void)
{
	return kcm_register_cap(CAPABILITY_ID_RESAMPLER, &resampler_impl);
}
