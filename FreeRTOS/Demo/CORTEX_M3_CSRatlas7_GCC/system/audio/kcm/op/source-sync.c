/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *	notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *	copyright notice, this list of conditions and the following
 *	disclaimer in the documentation and/or other materials provided
 *	with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *	contributors may be used to endorse or promote products derived
 *	from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

#define SOURCE_SYNC_CTRL_NUM (3)
#define SOURCE_SYNC_CTRL_IDX_ACTIVE_STREAM (0)
#define SOURCE_SYNC_CTRL_IDX_PURGE_FLAG (1)
#define SOURCE_SYNC_CTRL_IDX_TRANS_SAMPLES (2)

#define SOURCE_SYNC_GROUPS_MAX (24)
#define SOURCE_SYNC_CHANNELS_MAX (24)
#define SOURCE_SYNC_TRANS_SAMPLES_MAX (65536)
#define SOURCE_SYNC_PURGE_OFF	(0x00)
#define SOURCE_SYNC_PURGE_ON	(~SOURCE_SYNC_PURGE_OFF)

struct source_sync_ctx {
	u16 streams;
	u16 channels[SOURCE_SYNC_GROUPS_MAX];
	u16 input_map[SOURCE_SYNC_CHANNELS_MAX];	/* starts from 1 */
	u16 output_map[SOURCE_SYNC_CHANNELS_MAX];	/* starts from 1 */
	u32 connected_pins;
	u32 purge_flag; /* “1” - purge sink if connected but not associated with a route */
	u16 active_stream;	/* starts from 1 */
	u16 switch_in_num;
	u16 trans_samples;
	u16 sample_rate;
};

struct sink_groups_msg {
	u16 group_num;
	u32 sync_group[SOURCE_SYNC_GROUPS_MAX];
};

struct route_item {
	u16 source_idx;
	u16 sink_idx;
	u16 rate;
	u16 gain;
	u16 trans_samples;
};

struct switch_route_msg {
	u16 route_num;
	struct route_item route[SOURCE_SYNC_CHANNELS_MAX];
};

struct srcsync_param_msg {
	u16 block;
	u16 offset;
	u16 param_num;
	u16 value_h;
	u16 value_l;
	u16 pad;
};

static struct sink_groups_msg __on_dram _sink_groups_msg;
static struct switch_route_msg __on_dram _switch_route_msg;
static struct srcsync_param_msg __on_dram _srcsync_param_msg;

static int set_sink_groups(struct kasobj_op *op)
{
	struct source_sync_ctx *ctx = op->context;
	struct sink_groups_msg *msg = &_sink_groups_msg;
	int idx, shift, ret, msg_len;
	u32 mask;

	if (!op->obj.life_cnt)
		return 0;

	msg->group_num = ctx->streams;
	for (idx = 0, shift = 0; idx < ctx->streams; idx++) {
		mask = ~0;			/* 0xFFFFFFFF */
		mask <<= ctx->channels[idx];	/* 0xFFFFFFF0 (if ch is 4) */
		mask = ~mask;			/* 0x0000000F */
		mask <<= shift;			/* 0x00000F00 (if shift is 8) */
		msg->sync_group[idx] = mask;
		shift += ctx->channels[idx];
	}
	msg_len = 2 * ctx->streams + 1;

	ret = kalimba_operator_message(op->op_id, SOURCESYNC_SET_SINK_GROUPS,
		msg_len, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set sink groups failed(%d)!\r\n",
			op->obj.name, ret);
		return ret;
	}

	return 0;
}

static int set_routes(struct kasobj_op *op)
{
	struct source_sync_ctx *ctx = op->context;
	struct switch_route_msg *msg = &_switch_route_msg;
	int idx, cnt, ret, msg_len, tmp;

	if (!op->obj.life_cnt)
		return 0;

	for (idx = 0, cnt = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx++) {
		tmp = ctx->output_map[idx];
		if (tmp == 0)
			continue;
		msg->route[cnt].source_idx = idx;
		msg->route[cnt].sink_idx = tmp - 1;
		msg->route[cnt].gain = 0;	/* 0dB */
		msg->route[cnt].trans_samples = ctx->trans_samples;
		msg->route[cnt++].rate = ctx->sample_rate / 25;
	}
	msg->route_num = cnt;
	msg_len = 5 * cnt + 1;
	ret = kalimba_operator_message(op->op_id, SOURCESYNC_SET_ROUTE,
		msg_len, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set route failed(%d)!\r\n",
			op->obj.name, ret);
		return ret;
	}

	return 0;
}

static int find_active_stream(struct kasobj_op *op)
{
	int i, ch, left;
	u32 pin0, pin1;
	struct source_sync_ctx *ctx = op->context;

	/*
	 * Check first channel pin of each stream
	 * eg. 3streams, 4channels: 0,4,8
	 */
	left = 0;
	kcm_debug("KASOBJ(%s): active_sink_pins = %d, current_si_pins = 0x%x, history_si_pins = 0x%x\r\n",
		op->obj.name, ctx->switch_in_num, op->active_sink_pins, ctx->connected_pins);
	for (i = 0, ch = 0; ch < ctx->switch_in_num; ch += ctx->channels[i++]) {
		pin0 = op->active_sink_pins & BIT(ch);
		if (pin0) {
			left = i;
			break;
		}
	}

	for (i = 0, ch = 0; ch < ctx->switch_in_num; ch += ctx->channels[i++]) {
		pin0 = op->active_sink_pins & BIT(ch); /* current connection */
		pin1 = ctx->connected_pins & BIT(ch);  /* connection history */
		if (pin0 ^ pin1) {
			ctx->connected_pins = op->active_sink_pins;
			if (pin0)
				return i + 1;
			else
				return left + 1;
		}
	}

	return -EINVAL;
}

static void change_active_stream(struct kasobj_op *op, int active_st)
{
		struct source_sync_ctx *ctx = op->context;
		int ch, shift, idx, out_idx;

		ctx->active_stream = active_st;
		active_st--;
		for (idx = 0, shift = 0; idx < active_st; idx++)
			shift += ctx->channels[idx];
		ch = ctx->channels[active_st];
		for (idx = 0; idx < ch; idx++) {
			out_idx = ctx->input_map[idx + shift];
			ctx->output_map[out_idx - 1] = idx + shift + 1;
		}
}

/* Endpoint activity changed, we may have to pick new primary stream */
static void pin_changed(struct kasobj_op *op, int is_sink)
{
	if (is_sink) {
		/* Pick active stream based on current input pins activity */
		int active_stream;
		struct source_sync_ctx *ctx = op->context;

		active_stream = find_active_stream(op);
		if (active_stream == -EINVAL)
			active_stream = ctx->active_stream;
		change_active_stream(op, active_stream);
		set_routes(op);
		kcm_debug("KASOP(%s): set active stream to %d\r\n",
				op->obj.name, ctx->active_stream);
	}
}

static int set_purge_flag(struct kasobj_op *op)
{
	struct source_sync_ctx *ctx = op->context;
	int ret, value;
	struct srcsync_param_msg *msg = &_srcsync_param_msg;

	/* IPC only if operator is instantiated */
	if (!op->obj.life_cnt)
		return 0;

	msg->block = 1;
	msg->offset = 3;
	msg->param_num = 1;
	msg->pad = 0;

	if (ctx->purge_flag)
		value = SOURCE_SYNC_PURGE_ON;
	else
		value = SOURCE_SYNC_PURGE_OFF;
	msg->value_h = (u16)((value >> 8) & 0x0000ffff);
	msg->value_l = (u16)((value & 0x000000ff) << 8);

	ret = kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_PARAMS,
		6, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set purge flag failed(%d)!\r\n",
			op->obj.name, ret);
		return -EINVAL;
	}

	return 0;
}

static int source_sync_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	int value;
	struct source_sync_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= SOURCE_SYNC_CTRL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case SOURCE_SYNC_CTRL_IDX_ACTIVE_STREAM:
		value = ctx->active_stream;
		break;
	case SOURCE_SYNC_CTRL_IDX_PURGE_FLAG:
		value = ctx->purge_flag;
		break;
	case SOURCE_SYNC_CTRL_IDX_TRANS_SAMPLES:
		value = ctx->trans_samples;
		break;
	default:
		kcm_err("KASOP(%s): source sync get, invalid control number !\r\n",
			op->obj.name);
		return -EINVAL;
	}

	return value;
}

static int source_sync_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct source_sync_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= SOURCE_SYNC_CTRL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}

	switch (ctrl_id) {
	case SOURCE_SYNC_CTRL_IDX_ACTIVE_STREAM:
		if (value < 1)
			return 0;
		kcm_lock();
		change_active_stream(op, value);
		set_routes(op);
		kcm_unlock();
		break;
	case SOURCE_SYNC_CTRL_IDX_PURGE_FLAG:
		if (value != ctx->purge_flag) {
			kcm_lock();
			ctx->purge_flag = value;
			set_purge_flag(op);
			kcm_unlock();
		}
		break;
	case SOURCE_SYNC_CTRL_IDX_TRANS_SAMPLES:
		if (value != ctx->trans_samples)
			ctx->trans_samples = value;
		break;
	default:
		kcm_err("KASOP(%s): source sync put, invalid control number !\r\n",
			op->obj.name);
		return -EINVAL;
	}

	return 0;
}

/* Create control interfaces */
static int source_sync_init(struct kasobj_op *op)
{
	struct source_sync_ctx *ctx;
	int ch, idx, cnt, tmp, input, switch_out_num, flag;

	ctx = (struct source_sync_ctx *)kcm_get_cap_ctx_addr(sizeof(struct source_sync_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}

	op->context = ctx;
	ctx->trans_samples = 0;
	ctx->purge_flag = SOURCE_SYNC_PURGE_ON;
	ctx->sample_rate = op->db->rate;
	if (op->db->rate == 0) {
		kcm_err("KASOP(%s): invalid sample rate!\r\n", op->obj.name);
		return -EINVAL;
	}

	for (idx = 0, ch = 0, cnt = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx++) {
		tmp = op->db->param.srcsync_cfg.stream_ch[idx];
		if (tmp == 0)
			break;
		if (tmp > SOURCE_SYNC_CHANNELS_MAX) {
			kcm_err("KASOP(%s): invalid channel group config!\r\n",
				op->obj.name);
			return -EINVAL;
		}
		ch += tmp;
		if (ch > SOURCE_SYNC_CHANNELS_MAX) {
			kcm_err("KASOP(%s): too many total channels(%d)!\r\n",
				op->obj.name, ch);
			return -EINVAL;
		}
		ctx->channels[idx] = tmp;
		cnt += tmp;
	}
	ctx->streams = idx;
	for (idx = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx++) {
		tmp = op->db->param.srcsync_cfg.input_map[idx];
		if (tmp > SOURCE_SYNC_CHANNELS_MAX) {
			kcm_err("KASOP(%s): invalid input-output map!\r\n",
				op->obj.name);
			return -EINVAL;
		}
		ctx->input_map[idx] = tmp;
	}
	switch_out_num = op->db->param.srcsync_cfg.stream_ch[0];
	for (idx = 0, cnt = 0, flag = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx += switch_out_num) {
		for (ch = 0; ch < switch_out_num; ch++) {
			input = op->db->param.srcsync_cfg.input_map[ch];
			tmp = op->db->param.srcsync_cfg.input_map[idx + ch];
			if (input != tmp) {
				ctx->switch_in_num = idx;
				flag = 1;
				break;
			}
		}
		if (flag)
			break;
	}
	ctx->connected_pins = 0;

	op->ctrl_get = source_sync_get;
	op->ctrl_put = source_sync_put;

	return 0;
}

/* Called after the operator is created */
static int source_sync_create(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	struct source_sync_ctx *ctx = op->context;
	int idx, out_ch, ret;

	for (idx = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx++)
		ctx->output_map[idx] = 0;

	for (idx = 0; idx < SOURCE_SYNC_CHANNELS_MAX; idx++) {
		out_ch = ctx->input_map[idx];
		if (out_ch == 0)
			continue;
		ctx->output_map[out_ch - 1] = idx + 1;
	}
	ctx->connected_pins = 0;

	ret = set_sink_groups(op);
	if (ret)
		return ret;

	return set_purge_flag(op);
}

static int source_sync_trigger(struct kasobj_op *op, int event)
{
	const int param = KASOP_GET_PARAM(event);

	switch (KASOP_GET_EVENT(event)) {
	case kasop_event_start_ep:
	case kasop_event_stop_ep:
		pin_changed(op, param);
		break;
	default:
		break;
	}

	return 0;
}

static const struct kasop_impl source_sync_impl = {
	.init = source_sync_init,
	.create = source_sync_create,
	.trigger = source_sync_trigger,
};

/* register source sync operator */
int kasop_init_source_sync(void)
{
	return kcm_register_cap(CAPABILITY_ID_SOURCE_SYNC, &source_sync_impl);
}
