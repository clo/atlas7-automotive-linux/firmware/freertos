/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "errno.h"

#include "../kasobj.h"
#include "../kasop.h"
#include "../kcm.h"
#include "../../kalimba.h"

/* Volume control ID */
#define VOLCTRL_FRONT_LEFT_ID 0x0010
#define VOLCTRL_FRONT_RIGHT_ID 0x0011
#define VOLCTRL_REAR_LEFT_ID 0x0012
#define VOLCTRL_REAR_RIGHT_ID 0x0013
#define VOLCTRL_MASTER_GAIN_ID 0x0021

/* the control sequence */
#define VOLCTRL_CTRL_FRONT_LEFT 0
#define VOLCTRL_CTRL_FRONT_RIGHT 1
#define VOLCTRL_CTRL_REAR_LEFT 2
#define VOLCTRL_CTRL_REAR_RIGHT 3
#define VOLCTRL_CTRL_MASTER_GAIN 4
#define VOLCTRL_CTRL_MASTER_MUTE 5
#define VOLCTRL_CONTROL_NUM 6

#define VOLCTRL_MIN_DB (-120)
#define VOLCTRL_MAX_DB 9
#define VOLCTRL_STEP_DB 1
#define VOLCTRL_MAX_GAIN (VOLCTRL_MAX_DB - VOLCTRL_MIN_DB)

struct volctrl_ctx {
	int front_left;
	int front_right;
	int rear_left;
	int rear_right;
	int master_gain;
	int master_mute;
};

struct volctrl_msg {
	u16 block;
	u16 ctrl_id;
	u16 value_h;
	u16 value_l;
};

static struct volctrl_msg __on_dram _volctrl_msg;

static int set_volctrl_params(struct kasobj_op *op, int ctl_idx)
{
	struct volctrl_ctx *ctx = op->context;
	int ret;
	u32 volume;
	struct volctrl_msg *msg = &_volctrl_msg;

	/* IPC only if operator is instantiated */
	if (!op->obj.life_cnt)
		return 0;

	msg->block = 1;

	switch (ctl_idx) {
	case VOLCTRL_CTRL_FRONT_LEFT:
		msg->ctrl_id = VOLCTRL_FRONT_LEFT_ID;
		/* 1/60th of one dB resolution,( <dB gain> * 60) */
		volume = ctx->front_left * 60;
		break;
	case VOLCTRL_CTRL_FRONT_RIGHT:
		msg->ctrl_id = VOLCTRL_FRONT_RIGHT_ID;
		volume = ctx->front_right * 60;
		break;
	case VOLCTRL_CTRL_REAR_LEFT:
		msg->ctrl_id = VOLCTRL_REAR_LEFT_ID;
		volume = ctx->rear_left * 60;
		break;
	case VOLCTRL_CTRL_REAR_RIGHT:
		msg->ctrl_id = VOLCTRL_REAR_RIGHT_ID;
		volume = ctx->rear_right * 60;
		break;
	case VOLCTRL_CTRL_MASTER_GAIN:
		msg->ctrl_id = VOLCTRL_MASTER_GAIN_ID;
		volume = ctx->master_gain * 60;
		break;
	case VOLCTRL_CTRL_MASTER_MUTE:
		msg->ctrl_id = VOLCTRL_MASTER_GAIN_ID;
		/* mute equals minamal master gain */
		if (ctx->master_mute)
			volume = VOLCTRL_MIN_DB * 60;
		else
			volume = ctx->master_gain * 60;
		break;
	default:
		kcm_err("KASOP(%s): volume control, set parameter exception !\r\n",
			 op->obj.name);
		return -EINVAL;
	}
	msg->value_h = (u16)(volume >> 16);
	msg->value_l = (u16)(volume & 0xffff);
	ret = kalimba_operator_message(op->op_id,
		OPERATOR_MSG_VOLUME_CTRL_SET_CONTROL,
		4, (u16 *)msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set parametor failed(%d)!\r\n",
			op->obj.name, ret);
		return ret;
	}

	return 0;
}

static void set_volctrl_master_gain(struct kasobj_op *op, int vol)
{
	struct volctrl_ctx *ctx = op->context;

	if (vol < -120 || vol > 9) {
		kcm_err("KASOP(%s): invalid master gain(%d) !\r\n",
			 op->obj.name, vol);
		return;
	}

	kcm_lock();
	((int *)ctx)[VOLCTRL_CTRL_MASTER_GAIN] = vol;
	if (!(ctx->master_mute))
		set_volctrl_params(op, VOLCTRL_CTRL_MASTER_GAIN);
	kcm_unlock();
}

void kcm_set_vol_ctrl_gain(int vol)
{
	struct kasobj_op *op;
	int idx = 0;

	while (1) {
		op = kasobj_find_op_by_capid(
			CAPABILITY_ID_VOLUME_CONTROL, idx++);
		if (op)
			set_volctrl_master_gain(op, vol);
		else
			break;
	}
}

static int volctrl_get(struct kasobj_op *op, u16 ctrl_id, u16 value_id)
{
	int value;
	struct volctrl_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= VOLCTRL_CONTROL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}
	if (ctrl_id == VOLCTRL_CTRL_MASTER_MUTE)
		value = ctx->master_mute;
	else
		value = ((int *)ctx)[ctrl_id] + 120; /* 0~129 => -120~9 dB*/

	return value;
}

static int volctrl_put(struct kasobj_op *op, u16 ctrl_id,
		u16 value_id, u32 value)
{
	struct volctrl_ctx *ctx = op->context;

	if (ctrl_id < 0 || ctrl_id >= VOLCTRL_CONTROL_NUM) {
		kcm_err("%s invalid ctrl_id \r\n", __func__);
		return -EINVAL;
	}

	if (ctrl_id != VOLCTRL_CTRL_MASTER_MUTE)
		value -= 120;

	kcm_lock();
	if (((int *)ctx)[ctrl_id] != value) {
		((int *)ctx)[ctrl_id] = value;
		if (!((ctrl_id == VOLCTRL_CTRL_MASTER_GAIN) &&
			ctx->master_mute))
			set_volctrl_params(op, ctrl_id);
	}
	kcm_unlock();

	return 0;
}

/* Create control interfaces */
static int volctrl_init(struct kasobj_op *op)
{
	int idx;
	struct volctrl_ctx *ctx;
	ctx = (struct volctrl_ctx *)kcm_get_cap_ctx_addr(sizeof(struct volctrl_ctx));
	if (ctx == NULL) {
		kcm_err("%s ctx malloc failed\r\n", __func__);
		return -EINVAL;
	}

	op->context = ctx;
	for (idx = 0; idx < VOLCTRL_CONTROL_NUM - 1; idx++)
		((int *)ctx)[idx] = 0;
	ctx->master_mute = 0;

	op->ctrl_get = volctrl_get;
	op->ctrl_put = volctrl_put;

	return 0;
}

/* Called after the operator is created */
static int volctrl_create(struct kasobj_op *op,
	const struct kasobj_param *param)
{
	u16 sample_rate;  /* sample rate / 25 */
	int ret, idx;
	u16 msg[5] = {1, 68, 1, 0x0015, 0x0};

	/*
	 * The db->rate has two function:
	 * First, it is to decide which rate value will be used (db->rate
	 * or param->rate), which can be implemented by setting it with
	 * zero or non-zero value.
	 * Second, it is used to config sample rate with a non-zero value.
	 * For operator, there are two position within the audio pipeling:
	 * 1. Ahead of resampler, db->rate should be 0. Rate value from
	 *    app should be send to kalimba, and resampler will convert the
	 *    rate to the rate of codec.
	 * 2. Behind resampler, db->rate should not be 0 and should be
	 *    equal to the rate of codec.
	 */
	if (op->db->rate > 0 && op->db->rate <= KASOP_MAX_SAMPLE_RATE)
		sample_rate = op->db->rate / 25;
	else if (op->db->rate == 0)
		sample_rate = param->rate / 25; /* sample rate / 25 */
	else {
		kcm_err("KASOBJ(%s): Invalid sample rate (%d)!\r\n",
			op->obj.name, op->db->rate);
		return -EINVAL;
	}
	ret = kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_SAMPLE_RATE,
		1, &sample_rate, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set sample rate failed(%d)!\r\n",
			op->obj.name, ret);
		return ret;
	}

	/* Set the time of fade-in of volume-ctrl as 16ms */
	ret = kalimba_operator_message(op->op_id, OPMSG_COMMON_SET_PARAMS,
		5, msg, NULL, NULL, __kcm_resp);
	if (ret) {
		kcm_err("KASOBJ(%s): set fade-in failed(%d)!\r\n",
			op->obj.name, ret);
		return ret;
	}

	for (idx = 0; idx < VOLCTRL_CONTROL_NUM; idx++) {
		ret = set_volctrl_params(op, idx);
		if (ret)
			return ret;
	}

	return 0;
}

static const struct kasop_impl volctrl_impl = {
	.init = volctrl_init,
	.create = volctrl_create,
};

/* registe volume control operator */
int kasop_init_volctrl(void)
{
	return kcm_register_cap(CAPABILITY_ID_VOLUME_CONTROL, &volctrl_impl);
}
