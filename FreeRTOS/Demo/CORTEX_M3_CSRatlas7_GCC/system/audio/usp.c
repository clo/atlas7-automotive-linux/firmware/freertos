/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

#include <soc_cache.h>
#include <soc_clkc.h>
#include <soc_pinctrl.h>
#include "io.h"
#include "debug.h"
#include "usp.h"

/* #define USP_TEST */

#define _A7DA_USP0_BASE		0x10D20000
#define _A7DA_USP1_BASE		0x10D30000
#define _A7DA_USP2_BASE		0x10D40000
#define _A7DA_USP3_BASE		0x11001000

/* USP Registers */
#define USP_MODE1		0x00
#define USP_MODE2		0x04
#define USP_TX_FRAME_CTRL	0x08
#define USP_RX_FRAME_CTRL	0x0C
#define USP_TX_RX_ENABLE	0x10
#define USP_INT_ENABLE		0x14
#define USP_INT_STATUS		0x18
#define USP_PIN_IO_DATA		0x1C
#define USP_RISC_DSP_MODE	0x20
#define USP_AYSNC_PARAM_REG	0x24
#define USP_IRDA_X_MODE_DIV	0x28
#define USP_SM_CFG		0x2C
#define USP_TX_DMA_IO_CTRL	0x100
#define USP_TX_DMA_IO_LEN	0x104
#define USP_TX_FIFO_CTRL	0x108
#define USP_TX_FIFO_LEVEL_CHK	0x10C
#define USP_TX_FIFO_OP		0x110
#define USP_TX_FIFO_STATUS	0x114
#define USP_TX_FIFO_DATA	0x118
#define USP_RX_DMA_IO_CTRL	0x120
#define USP_RX_DMA_IO_LEN	0x124
#define USP_RX_FIFO_CTRL	0x128
#define USP_RX_FIFO_LEVEL_CHK	0x12C
#define USP_RX_FIFO_OP		0x130
#define USP_RX_FIFO_STATUS	0x134
#define USP_RX_FIFO_DATA	0x138

/* USP MODE register-1 */
#define USP_SYNC_MODE			0x00000001
#define USP_CLOCK_MODE_SLAVE		0x00000002
#define USP_LOOP_BACK_EN		0x00000004
#define USP_HPSIR_EN			0x00000008
#define USP_ENDIAN_CTRL_LSBF		0x00000010
#define USP_EN				0x00000020
#define USP_RXD_ACT_EDGE_FALLING	0x00000040
#define USP_TXD_ACT_EDGE_FALLING	0x00000080
#define USP_RFS_ACT_LEVEL_LOGIC1	0x00000100
#define USP_TFS_ACT_LEVEL_LOGIC1	0x00000200
#define USP_SCLK_IDLE_MODE_TOGGLE	0x00000400
#define USP_SCLK_IDLE_LEVEL_LOGIC1	0x00000800
#define USP_SCLK_PIN_MODE_IO	0x00001000
#define USP_RFS_PIN_MODE_IO	0x00002000
#define USP_TFS_PIN_MODE_IO	0x00004000
#define USP_RXD_PIN_MODE_IO	0x00008000
#define USP_TXD_PIN_MODE_IO	0x00010000
#define USP_SCLK_IO_MODE_INPUT	0x00020000
#define USP_RFS_IO_MODE_INPUT	0x00040000
#define USP_TFS_IO_MODE_INPUT	0x00080000
#define USP_RXD_IO_MODE_INPUT	0x00100000
#define USP_TXD_IO_MODE_INPUT	0x00200000
#define USP_IRDA_WIDTH_DIV_MASK	0x3FC00000
#define USP_IRDA_WIDTH_DIV_OFFSET	0
#define USP_IRDA_IDLE_LEVEL_HIGH	0x40000000
#define USP_TX_UFLOW_REPEAT_ZERO	0x80000000
#define USP_TX_ENDIAN_MODE		0x00000020
#define USP_RX_ENDIAN_MODE		0x00000020

/* USP Mode Register-2 */
#define USP_RXD_DELAY_LEN_MASK		0x000000FF
#define USP_RXD_DELAY_LEN_OFFSET	0

#define USP_TXD_DELAY_LEN_MASK		0x0000FF00
#define USP_TXD_DELAY_LEN_OFFSET	8

#define USP_ENA_CTRL_MODE		0x00010000
#define USP_FRAME_CTRL_MODE		0x00020000
#define USP_TFS_SOURCE_MODE             0x00040000
#define USP_TFS_MS_MODE                 0x00080000
#define USP_CLK_DIVISOR_MASK		0x7FE00000
#define USP_CLK_DIVISOR_OFFSET		21

#define USP_TFS_CLK_SLAVE_MODE		(1<<20)
#define USP_RFS_CLK_SLAVE_MODE		(1<<19)

#define USP_IRDA_DATA_WIDTH		0x80000000

/* USP Transmit Frame Control Register */

#define USP_TXC_DATA_LEN_MASK		0x000000FF
#define USP_TXC_DATA_LEN_OFFSET		0

#define USP_TXC_SYNC_LEN_MASK		0x0000FF00
#define USP_TXC_SYNC_LEN_OFFSET		8

#define USP_TXC_FRAME_LEN_MASK		0x00FF0000
#define USP_TXC_FRAME_LEN_OFFSET	16

#define USP_TXC_SHIFTER_LEN_MASK	0x1F000000
#define USP_TXC_SHIFTER_LEN_OFFSET	24

#define USP_TXC_SLAVE_CLK_SAMPLE	0x20000000

#define USP_TXC_CLK_DIVISOR_MASK	0xC0000000
#define USP_TXC_CLK_DIVISOR_OFFSET	30

/* USP Receive Frame Control Register */

#define USP_RXC_DATA_LEN_MASK		0x000000FF
#define USP_RXC_DATA_LEN_OFFSET		0

#define USP_RXC_FRAME_LEN_MASK		0x0000FF00
#define USP_RXC_FRAME_LEN_OFFSET	8

#define USP_RXC_SHIFTER_LEN_MASK	0x001F0000
#define USP_RXC_SHIFTER_LEN_OFFSET	16

#define USP_START_EDGE_MODE	0x00800000
#define USP_I2S_SYNC_CHG	0x00200000

#define USP_RXC_CLK_DIVISOR_MASK	0x0F000000
#define USP_RXC_CLK_DIVISOR_OFFSET	24
#define USP_SINGLE_SYNC_MODE		0x00400000

/* Tx - RX Enable Register */

#define USP_RX_ENA		0x00000001
#define USP_TX_ENA		0x00000002

/* USP Interrupt Enable and status Register */
#define USP_RX_DONE_INT			0x00000001
#define USP_TX_DONE_INT			0x00000002
#define USP_RX_OFLOW_INT		0x00000004
#define USP_TX_UFLOW_INT		0x00000008
#define USP_RX_IO_DMA_INT		0x00000010
#define USP_TX_IO_DMA_INT		0x00000020
#define USP_RXFIFO_FULL_INT		0x00000040
#define USP_TXFIFO_EMPTY_INT		0x00000080
#define USP_RXFIFO_THD_INT		0x00000100
#define USP_TXFIFO_THD_INT		0x00000200
#define USP_UART_FRM_ERR_INT		0x00000400
#define USP_RX_TIMEOUT_INT		0x00000800
#define USP_TX_ALLOUT_INT		0x00001000
#define USP_RXD_BREAK_INT		0x00008000

/* All possible TX interruots */
#define USP_TX_INTERRUPT		(USP_TX_DONE_INT|USP_TX_UFLOW_INT|\
					USP_TX_IO_DMA_INT|\
					USP_TXFIFO_EMPTY_INT|\
					USP_TXFIFO_THD_INT)
/* All possible RX interruots */
#define USP_RX_INTERRUPT		(USP_RX_DONE_INT|USP_RX_OFLOW_INT|\
					USP_RX_IO_DMA_INT|\
					USP_RXFIFO_FULL_INT|\
					USP_RXFIFO_THD_INT|\
					USP_RXFIFO_THD_INT|USP_RX_TIMEOUT_INT)

#define USP_INT_ALL        0x1FFF

/* USP Pin I/O Data Register */

#define USP_RFS_PIN_VALUE_MASK	0x00000001
#define USP_TFS_PIN_VALUE_MASK	0x00000002
#define USP_RXD_PIN_VALUE_MASK	0x00000004
#define USP_TXD_PIN_VALUE_MASK	0x00000008
#define USP_SCLK_PIN_VALUE_MASK	0x00000010

/* USP RISC/DSP Mode Register */
#define USP_RISC_DSP_SEL	0x00000001

/* USP ASYNC PARAMETER Register*/

#define USP_ASYNC_TIMEOUT_MASK	0x0000FFFF
#define USP_ASYNC_TIMEOUT_OFFSET	0
#define USP_ASYNC_TIMEOUT(x)	(((x)&USP_ASYNC_TIMEOUT_MASK) \
				<<USP_ASYNC_TIMEOUT_OFFSET)

#define USP_ASYNC_DIV2_MASK		0x003F0000
#define USP_ASYNC_DIV2_OFFSET		16

/* USP TX DMA I/O MODE Register */
#define USP_TX_MODE_IO			0x00000001

/* USP TX DMA I/O Length Register */
#define USP_TX_DATA_LEN_MASK		0xFFFFFFFF
#define USP_TX_DATA_LEN_OFFSET		0

/* USP TX FIFO Control Register */
#define USP_TX_FIFO_WIDTH_MASK		0x00000003
#define USP_TX_FIFO_WIDTH_OFFSET	0

#define USP_TX_FIFO_THD_MASK		0x000001FC
#define USP_TX_FIFO_THD_MASK_ATLAS7	0x000007FC
#define USP_TX_FIFO_THD_OFFSET		2

/* USP TX FIFO Level Check Register */
#define USP_TX_FIFO_SC_OFFSET	0
#define USP_TX_FIFO_LC_OFFSET	10
#define USP_TX_FIFO_HC_OFFSET	20

#define TX_FIFO_SC(x)		((x) << USP_TX_FIFO_SC_OFFSET)
#define TX_FIFO_LC(x)		((x) << USP_TX_FIFO_LC_OFFSET)
#define TX_FIFO_HC(x)		((x) << USP_TX_FIFO_HC_OFFSET)

/* USP TX FIFO Operation Register */
#define USP_TX_FIFO_RESET		0x00000001
#define USP_TX_FIFO_START		0x00000002

/* USP TX FIFO Status Register */
#define USP_TX_FIFO_LEVEL_MASK		0x0000007F
#define USP_TX_FIFO_LEVEL_MASK_ATLAS7	0x000001FF
#define USP_TX_FIFO_LEVEL_OFFSET	0

#define USP_TX_FIFO_FULL		0x00000080
#define USP_TX_FIFO_EMPTY		0x00000100
#define USP_TX_FIFO_FULL_ATLAS7		0x00000200
#define USP_TX_FIFO_EMPTY_ATLAS7	0x00000400

/* USP TX FIFO Data Register */
#define USP_TX_FIFO_DATA_MASK		0xFFFFFFFF
#define USP_TX_FIFO_DATA_OFFSET		0

/* USP RX DMA I/O MODE Register */
#define USP_RX_MODE_IO			0x00000001
#define USP_RX_DMA_FLUSH		0x00000004

/* USP RX DMA I/O Length Register */
#define USP_RX_DATA_LEN_MASK		0xFFFFFFFF
#define USP_RX_DATA_LEN_OFFSET		0

/* USP RX FIFO Control Register */
#define USP_RX_FIFO_WIDTH_MASK		0x00000003
#define USP_RX_FIFO_WIDTH_OFFSET	0

#define USP_RX_FIFO_THD_MASK		0x000001FC
#define USP_RX_FIFO_THD_OFFSET		2

/* USP RX FIFO Level Check Register */

#define USP_RX_FIFO_LEVEL_CHECK_MASK	0x1F
#define USP_RX_FIFO_SC_OFFSET	0
#define USP_RX_FIFO_LC_OFFSET	10
#define USP_RX_FIFO_HC_OFFSET	20

#define RX_FIFO_SC(x)		((x) << USP_RX_FIFO_SC_OFFSET)
#define RX_FIFO_LC(x)		((x) << USP_RX_FIFO_LC_OFFSET)
#define RX_FIFO_HC(x)		((x) << USP_RX_FIFO_HC_OFFSET)

/* USP RX FIFO Operation Register */
#define USP_RX_FIFO_RESET		0x00000001
#define USP_RX_FIFO_START		0x00000002

/* USP RX FIFO Status Register */

#define USP_RX_FIFO_LEVEL_MASK		0x0000007F
#define USP_RX_FIFO_LEVEL_MASK_ATLAS7	0x000001FF
#define USP_RX_FIFO_LEVEL_OFFSET	0

#define USP_RX_FIFO_FULL		0x00000080
#define USP_RX_FIFO_EMPTY		0x00000100
#define USP_RX_FIFO_FULL_ATLAS7		0x00000200
#define USP_RX_FIFO_EMPTY_ATLAS7	0x00000400

/* USP RX FIFO Data Register */

#define USP_RX_FIFO_DATA_MASK		0xFFFFFFFF
#define USP_RX_FIFO_DATA_OFFSET		0

/*
 * When rx thd irq occur, sender just disable tx empty irq,
 * Remaining data in tx fifo wil also be sent out.
 */
#define USP_FIFO_SIZE			128
#define USP_FIFO_SIZE_ATLAS7		512

/* FIFO_WIDTH for the USP_TX_FIFO_CTRL and USP_RX_FIFO_CTRL registers */
#define USP_FIFO_WIDTH_BYTE  0x00
#define USP_FIFO_WIDTH_WORD  0x01
#define USP_FIFO_WIDTH_DWORD 0x02

#define USP_ASYNC_DIV2          16

#define USP_PLUGOUT_RETRY_CNT	2

#define USP_TX_RX_FIFO_WIDTH_DWORD    2

#define SIRF_USP_DIV_MCLK	0

#define SIRF_USP_I2S_TFS_SYNC	0
#define SIRF_USP_I2S_RFS_SYNC	1

#ifdef USP_TEST
#define _A7DA_DMAC2_BASE			0x10D50000
#define _A7DA_DMAC4_BASE			0x11002000
#define A7DA_DMAC_DMA_CH_ADDR(base, ch) \
	(base + 0x00 + ch * 0x10)
#define A7DA_DMAC_DMA_CH_XLEN(base, ch) \
	(base + 0x04 + ch * 0x10)
#define A7DA_DMAC_DMA_CH_YLEN(base, ch) \
	(base + 0x08 + ch * 0x10)
#define A7DA_DMAC_DMA_CH_CTRL(base, ch) \
	(base + 0x0C + ch * 0x10)

#define A7DA_DMAC_DMA_WIDTH(base, ch) \
	(base + 0x100 + ch * 4)
#define A7DA_DMAC_DMA_CH_VALID(base)		(base + 0x140)
#define A7DA_DMAC_DMA_CH_INT(base)		(base + 0x144)
#define A7DA_DMAC_DMA_INT_EN(base)		(base + 0x148)
#define A7DA_DMAC_DMA_INT_EN_CLR(base)		(base + 0x14C)
#define A7DA_DMAC_DMA_CH_LOOP_CTRL(base)	(base + 0x158)
#define A7DA_DMAC_DMA_CH_LOOP_CTRL_CLR(base)	(base + 0x15C)
#define A7DA_DMAC_DMA_INT_OWNER_REG0(base)	(base + 0x164)
#define A7DA_DMAC_DMA_INT_OWNER_REG1(base)	(base + 0x168)

#define USP0_TX				11
#define USP0_RX				10

#define USP1_TX				7
#define USP1_RX				6

#define USP2_TX				13
#define USP2_RX				12

#define USP3_TX				1
#define USP3_RX				2

#define DMA_DEV_TO_MEM					0
#define DMA_MEM_TO_DEV					1
#endif

struct sirf_usp {
	u32 reg_base;
	int fifo_size;
	void (*clkc_enable)(void);
	int pinctrl_func;
	int pins[4];
#ifdef USP_TEST
	u32 dmac_reg_base;
	int dma_tx_ch;
	int dma_rx_ch;
#endif
};

const struct sirf_usp usp[MAX_USP_PORT] = {
	{_A7DA_USP0_BASE,
	512,
	clkc_enable_usp0,
	FUNC_USP0,
	{USP0_CLK_PIN, USP0_TX_PIN, USP0_RX_PIN, USP0_FS_PIN},
#ifdef USP_TEST
	_A7DA_DMAC2_BASE,
	USP0_TX,
	USP0_RX
#endif
	},
	{_A7DA_USP1_BASE,
	512,
	clkc_enable_usp1,
	FUNC_USP1,
	{USP1_CLK_PIN, USP1_TX_PIN, USP1_RX_PIN, USP1_FS_PIN},
#ifdef USP_TEST
	_A7DA_DMAC2_BASE,
	USP1_TX,
	USP1_RX
#endif
	},
	{_A7DA_USP2_BASE,
	128,
	clkc_enable_usp2,
	FUNC_USP2,
	{USP2_CLK_PIN, USP2_TX_PIN, USP2_RX_PIN, USP2_FS_PIN},
#ifdef USP_TEST
	_A7DA_DMAC2_BASE,
	USP2_TX,
	USP2_RX
#endif
	},
	{_A7DA_USP3_BASE,
	512,
	clkc_enable_usp3,
	-1,
	{-1, -1, -1, -1},
#ifdef USP_TEST
	_A7DA_DMAC4_BASE,
	USP3_TX,
	USP3_RX
#endif
	},
};

void usp_tx_enable(int port)
{
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_FIFO_OP,
		USP_TX_FIFO_RESET, USP_TX_FIFO_RESET);
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_FIFO_OP,
		USP_TX_FIFO_RESET, 0);
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_FIFO_OP,
		USP_TX_FIFO_START, USP_TX_FIFO_START);
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_RX_ENABLE,
		USP_TX_ENA, USP_TX_ENA);
}

void usp_tx_disable(int port)
{
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_RX_ENABLE,
		USP_TX_ENA, 0);
	SOC_REG(usp[port].reg_base + USP_TX_FIFO_OP) = 0;
}

void usp_rx_enable(int port)
{
	REG_UPDATE_BITS(usp[port].reg_base + USP_RX_FIFO_OP,
		USP_RX_FIFO_RESET, USP_TX_FIFO_RESET);
	REG_UPDATE_BITS(usp[port].reg_base + USP_RX_FIFO_OP,
		USP_RX_FIFO_RESET, 0);
	REG_UPDATE_BITS(usp[port].reg_base + USP_RX_FIFO_OP,
		USP_RX_FIFO_START, USP_RX_FIFO_START);
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_RX_ENABLE,
		USP_RX_ENA, USP_RX_ENA);
}

void usp_rx_disable(int port)
{
	REG_UPDATE_BITS(usp[port].reg_base + USP_TX_RX_ENABLE,
		USP_RX_ENA, 0);
	SOC_REG(usp[port].reg_base + USP_TX_FIFO_OP) = 0;
}

void usp_start(int port, int playback)
{
	if (playback)
		usp_tx_enable(port);
	else
		usp_rx_enable(port);
}

void usp_stop(int port, int playback)
{
	if (playback)
		usp_tx_disable(port);
	else
		usp_rx_disable(port);
}

void usp_params(int port, int playback, int channels, int rate, int fmt_mode)
{
	u32 data_len = 16;
	u32 frame_len, shifter_len;
	const u32 base = usp[port].reg_base;

	shifter_len = data_len;

	/* SYNC mode I2S or DSP_A*/
	if (fmt_mode == I2S_MODE) {
		REG_UPDATE_BITS(base + USP_RX_FRAME_CTRL,
				USP_I2S_SYNC_CHG, USP_I2S_SYNC_CHG);
		frame_len = data_len;
	} else {
		REG_UPDATE_BITS(base + USP_RX_FRAME_CTRL,
				USP_I2S_SYNC_CHG, 0);
		frame_len = data_len * channels;
	}
	data_len = frame_len;

	if (playback)
		REG_UPDATE_BITS(base +  USP_TX_FRAME_CTRL,
			USP_TXC_DATA_LEN_MASK | USP_TXC_FRAME_LEN_MASK
			| USP_TXC_SHIFTER_LEN_MASK | USP_TXC_SLAVE_CLK_SAMPLE,
			((data_len - 1) << USP_TXC_DATA_LEN_OFFSET)
			| ((frame_len - 1) << USP_TXC_FRAME_LEN_OFFSET)
			| ((shifter_len - 1) << USP_TXC_SHIFTER_LEN_OFFSET)
			| USP_TXC_SLAVE_CLK_SAMPLE);
	else {
		REG_UPDATE_BITS(base + USP_RX_FRAME_CTRL,
			USP_RXC_DATA_LEN_MASK | USP_RXC_FRAME_LEN_MASK
			| USP_RXC_SHIFTER_LEN_MASK | USP_SINGLE_SYNC_MODE,
			((data_len - 1) << USP_RXC_DATA_LEN_OFFSET)
			| ((frame_len - 1) << USP_RXC_FRAME_LEN_OFFSET)
			| ((shifter_len - 1) << USP_RXC_SHIFTER_LEN_OFFSET)
			| USP_SINGLE_SYNC_MODE);
		/*
		 * In single sync mode, TFS is used both as TX and RX, and is
		 * driven by peer. So it should be set to slave mode.
		 */
		REG_UPDATE_BITS(base + USP_TX_FRAME_CTRL,
			USP_TXC_SLAVE_CLK_SAMPLE, USP_TXC_SLAVE_CLK_SAMPLE);
	}
}

static void usp_init(int port)
{
	/* FIFO level check threshold in dwords */
	const int fifo_l = 16 / 4;
	const int fifo_m = (usp[port].fifo_size / 2) / 4;
	const int fifo_h = (usp[port].fifo_size - 16) / 4;
	const u32 base = usp[port].reg_base;

	usp[port].clkc_enable();
	/* Configure RISC mode */
	REG_UPDATE_BITS(base + USP_RISC_DSP_MODE,
		USP_RISC_DSP_SEL, ~USP_RISC_DSP_SEL);

	/*
	 * Configure DMA IO Length register
	 * Set no limit, USP can receive data continuously until it is diabled
	 */
	SOC_REG(base + USP_TX_DMA_IO_LEN) = 0;
	SOC_REG(base + USP_RX_DMA_IO_LEN) = 0;

	/* Configure Mode2 register */
	SOC_REG(base + USP_MODE2) = (1 << USP_RXD_DELAY_LEN_OFFSET) |
		(0 << USP_TXD_DELAY_LEN_OFFSET) |
		USP_TFS_CLK_SLAVE_MODE | USP_RFS_CLK_SLAVE_MODE;

	/* Configure Mode1 register */
	SOC_REG(base + USP_MODE1) =
		USP_SYNC_MODE | USP_EN | USP_TXD_ACT_EDGE_FALLING |
		USP_RFS_ACT_LEVEL_LOGIC1 | USP_TFS_ACT_LEVEL_LOGIC1 |
		USP_TX_UFLOW_REPEAT_ZERO | USP_CLOCK_MODE_SLAVE;

	/* Configure RX DMA IO Control register */
	SOC_REG(base + USP_RX_DMA_IO_CTRL) = 0;

	/* Configure RX FIFO Control register */
	SOC_REG(base + USP_RX_FIFO_CTRL) =
		((usp[port].fifo_size / 2) << USP_RX_FIFO_THD_OFFSET) |
		(USP_TX_RX_FIFO_WIDTH_DWORD << USP_RX_FIFO_WIDTH_OFFSET);

	/* Configure RX FIFO Level Check register */
	SOC_REG(base + USP_RX_FIFO_LEVEL_CHK) =
		RX_FIFO_SC(fifo_l) | RX_FIFO_LC(fifo_m) | RX_FIFO_HC(fifo_h);

	/* Configure TX DMA IO Control register*/
	SOC_REG(base + USP_TX_DMA_IO_CTRL) = 0;

	/* Configure TX FIFO Control register */
	SOC_REG(base + USP_TX_FIFO_CTRL) =
		((usp[port].fifo_size / 2) << USP_TX_FIFO_THD_OFFSET) |
		(USP_TX_RX_FIFO_WIDTH_DWORD << USP_TX_FIFO_WIDTH_OFFSET);

	/* Congiure TX FIFO Level Check register */
	SOC_REG(base + USP_TX_FIFO_LEVEL_CHK) =
		TX_FIFO_SC(fifo_h) | TX_FIFO_LC(fifo_m) | TX_FIFO_HC(fifo_l);

}

#ifdef USP_TEST
static void set_dmac_owner(u32 dmac_reg_base, int ch)
{
	if (ch > 15)
		return;
	if (ch < 10)
		REG_UPDATE_BITS(A7DA_DMAC_DMA_INT_OWNER_REG0(dmac_reg_base), 7 << (ch * 3),
				2 << (ch * 3));
	else
		REG_UPDATE_BITS(A7DA_DMAC_DMA_INT_OWNER_REG1(dmac_reg_base), 7 << ((ch  - 10) * 3),
				2 << ((ch - 10) * 3));
}

static void enable_dmac(u32 dmac_reg_base, int ch, u32 addr, int buf_len, int dir)
{
	int i, ylen, width = 0, period_len = buf_len / 2;

	addr += SOC_SYSTEM_MAP;
	for (i = 1024; i >= 16; i /= 2) {
		if (!(period_len % i)) {
			width = i / 4;
			break;
		}
	}
	if (width == 0) {
		DebugMsg("Invalid buffer size\n");
		return;
	}
	ylen = buf_len / (width * 4) - 1;

	dir = !!dir;

	/* Clear pending interrupt */
	SOC_REG(A7DA_DMAC_DMA_CH_INT(dmac_reg_base)) = BIT(ch);

	/* Configure DMA channel */
	SOC_REG(A7DA_DMAC_DMA_WIDTH(dmac_reg_base, ch)) = width;
	/* Burst mode */
	SOC_REG(A7DA_DMAC_DMA_CH_CTRL(dmac_reg_base, ch)) = ch | BIT(4) | (dir << 5);
	SOC_REG(A7DA_DMAC_DMA_CH_XLEN(dmac_reg_base, ch)) = 0;
	SOC_REG(A7DA_DMAC_DMA_CH_YLEN(dmac_reg_base, ch)) = ylen;

	/* Start DMA */
	SOC_REG(A7DA_DMAC_DMA_CH_ADDR(dmac_reg_base, ch)) = (addr >> 2);
	REG_UPDATE_BITS(A7DA_DMAC_DMA_CH_LOOP_CTRL(dmac_reg_base), BIT(ch) | BIT(ch + 16),
			BIT(ch) | BIT(ch + 16));
}

static void disable_dmac(u32 dmac_reg_base, int ch)
{
	REG_UPDATE_BITS(A7DA_DMAC_DMA_CH_LOOP_CTRL_CLR(dmac_reg_base), BIT(ch) | BIT(ch + 16),
				BIT(ch) | BIT(ch + 16));
}

static short __on_dram audio_buffer_mono[48] = {
		0, 3422, 6784, 10033, 13105, 15962, 18531, 20804,
		22696, 24224, 25317, 25992, 26214, 25990, 25323, 24215,
		22707, 20792, 18541, 15957, 13107, 10032, 6784, 3423,
		-2, -3418, -6789, -10029, -13108, -15960, -18533, -20801,
		-22699, -24221, -25320, -25991, -26213, -25992, -25319, -24221,
		-22701, -20798, -18536, -15959, -13105, -10035, -6782, -3425
};

static short __on_dram audio_buffer_stereo[96];

static void usp_playback_test(int port, int slave, int playback)
{
	int i;
	u32 dmac_reg_base = usp[port].dmac_reg_base;

	for (i = 0; i < 48; i++) {
		audio_buffer_stereo[i * 2] = audio_buffer_mono[i];
		audio_buffer_stereo[i * 2 + 1] = audio_buffer_mono[i];
	}
	vDcacheFlush();

	if (port != 3)
		SOC_REG(A7DA_CLKC_AUDIO_DMAC2_LEAF_CLK_EN_SET) = BIT0;

	set_dmac_owner(dmac_reg_base, usp[port].dma_tx_ch);
	enable_dmac(dmac_reg_base, usp[port].dma_tx_ch, (u32)audio_buffer_stereo,
				192, DMA_MEM_TO_DEV);
	usp_params(port, 1, 2, 48000, DSP_A_MODE);
	usp_start(port, 1);
}

static void dump(int port)
{
	u32 dmac_reg_base = usp[port].dmac_reg_base;
	int dma_tx_ch = usp[port].dma_tx_ch;

	DebugMsg("\033[32m\033[1maudio buff addr: 0x%08x\r\n\033[0m",
			(u32)audio_buffer_stereo + SOC_SYSTEM_MAP);
	DebugMsg("\033[32m\033[1mDMA ownnership: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_INT_OWNER_REG0(dmac_reg_base)));
	DebugMsg("\033[32m\033[1mDMA loop ctrl: 0x%08x\r\n\033[0m",
				SOC_REG(A7DA_DMAC_DMA_CH_LOOP_CTRL(dmac_reg_base)));
	DebugMsg("\033[31m\033[1mDMA channel: %d\r\n\033[0m", dma_tx_ch);
	DebugMsg("\033[32m\033[1mWIDTH: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_WIDTH(dmac_reg_base, dma_tx_ch)));
	DebugMsg("\033[32m\033[1mXLEN: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_CH_XLEN(dmac_reg_base, dma_tx_ch)));
	DebugMsg("\033[32m\033[1mYLEN: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_CH_YLEN(dmac_reg_base, dma_tx_ch)));
	DebugMsg("\033[32m\033[1mADDR: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_CH_ADDR(dmac_reg_base, dma_tx_ch)));
	DebugMsg("\033[32m\033[1mCTRL: 0x%08x\r\n\033[0m",
			SOC_REG(A7DA_DMAC_DMA_CH_CTRL(dmac_reg_base, dma_tx_ch)));
}
#endif

static void usp_pinctrl_set(int port)
{
	int i;

	for (i = 0; i < 4; i++)
		uxPinctrlPinFuncSel(usp[port].pins[i], usp[port].pinctrl_func);
}

void usp_setup(int port, int channels, int rate, int slave, int playback)
{
	/* the USP3 uses the internal pins, so not change the pinctrl */
	if (port != 3)
		usp_pinctrl_set(port);

	usp_init(port);
#ifdef USP_TEST
	usp_playback_test(port, slave, playback);
	dump(port);
#endif
}
