/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "soc_irq.h"
#include "core_cm3.h"
#include "soc_armm3.h"
#include "soc_cache.h"
#include "io.h"

void vIcacheInvalidate( void )
{
	REG( ARMM3_NATIVE_ICM ) |= ICACHE_MGM_INVALIDATE;
	while( REG( ARMM3_NATIVE_ICM ) & ICACHE_MGM_INVALIDATE );
}

void vIcacheDisable( void )
{
	REG( ARMM3_NATIVE_ICC ) &= ( ~ICACHE_CONFIG_ENABLE );
	while( REG( ARMM3_NATIVE_ICC ) & ICACHE_CONFIG_ENABLE );
}

void vIcacheEnable( void )
{
	vIcacheInvalidate();

	REG( ARMM3_NATIVE_ICC ) |= ICACHE_CONFIG_ENABLE;
	while( !( REG( ARMM3_NATIVE_ICC ) & ICACHE_CONFIG_ENABLE ) );
}

#ifdef __ATLAS7_STEP_B__
void vDcacheFlush( void )
{
	REG( ARMM3_NATIVE_DCM ) |= DCACHE_MGM_FLUSH;
	while( REG( ARMM3_NATIVE_DCM ) & DCACHE_MGM_FLUSH );
}

void vDcacheInvalidate( void )
{
	REG( ARMM3_NATIVE_DCM ) |= DCACHE_MGM_INVALIDATE;
	while( REG( ARMM3_NATIVE_DCM ) & DCACHE_MGM_INVALIDATE );
}

void vDcacheEnable( void )
{
	vDcacheInvalidate();

	REG( ARMM3_NATIVE_DCC ) |= DCACHE_CONFIG_ENABLE;
	while( !( REG( ARMM3_NATIVE_DCC ) & DCACHE_CONFIG_ENABLE ) );
}

void vDcacheDisable( void )
{
	vDcacheFlush();

	REG( ARMM3_NATIVE_DCC ) &= ( ~DCACHE_CONFIG_ENABLE );
	while( REG( ARMM3_NATIVE_DCC ) & DCACHE_CONFIG_ENABLE );
}
#endif
