/**************************************************************************/
/*   Title      : CANmod3 low-level driver                                */
/*   Company    : INICORE INC., Newark, CA 94560                          */
/*   File       : CANmod3.c                                               */
/*   Author     : DL                                                      */
/*   Date       : April 5, 2010                                           */
/**************************************************************************/
/*                                                                        */
/*  Copyright (c) 2010-2015 INICORE INC., All Rights Reserved             */
/*                                                                        */
/**************************************************************************/

/* $InicoreDate: 2015/03/03 01:24:39 $ $InicoreAuthor: daniel $ $InicoreRevision: 1.7 $ $InicoreName: CANMOD3_DRIVER-REL-1_1_0 $*/

/** @mainpage Inicore CANmodule3 Driver
*
*  @section intro_sec Introduction
*
*

The CANmod3 driver implements all the low-level handling between the application
software and the CAN controller hardware. Additionally, the driver contains a
Basic CAN API that can be used if the application needs support for Basic CAN
operation. Basic CAN and Full CAN operation can be mixed.

Application specific features such as interrupt handlers are not part of the CAN
driver. The driver code supports multi-port implementations where the target
hardware contains more than just one CAN controller.

The CAN driver can be configured for operation with either the CANmod3 core
(16rx/8tx mailboxes) or the CANmod3x core (32rx/32tx mailboxes).

The CAN driver uses one instance of the canmod3_instance_t structure per port. This
instance is used to identify the target port and a pointer to this instance is
passed as the first argument to all CAN driver functions.

@section operation_sec Operation

\par CAN Controller Configuration

Before using the CAN controller, the driver as well as the CAN controller have to
be initialized by using CANmod3_Init(). Next, the operating mode needs to be selected
with CANmod3_Mode() before the CAN controller can finally be started with CANmod3_Start().
CANmod3_Stop() stops the controller.
CANmod3_SetConfigReg() is used to change the CAN controllers configuration during normal
operation.

\par Operation Status

CANmod3_GetErrorStatus() returns the current CAN error state (error active, error passive,
and bus off). CANmod3_GetRxErrorCount() and CANmod3_GetTxErrorCount() return the
actual error counter values while CANmod3_GetRxGTE96() and CANmod3_GetTxGTE96() show
if the error counters are greater or equal to 96, which indicates a heavy disturbed
bus.

\par Interrupt Support

The interrupt service routines are not part of the CAN driver. But access functions to
the respective interrupt registers are provided.
The individual interrupt enable bits can be set using CANmod3_SetIntEbl() while
CANmod3_GetIntEbl() returns their actual state. CANmod3_SetGlobalIntEbl() enables
global interrupt generation by the core while CANmod3_GetGlobalIntEbl() indicates if
interrupt generation is enabled at all. \n
CANmod3_GetIntStatus() shows the current state of the different interrupt status bits.
Each interrupt status bit can be individually cleared using CANmod3_ClearIntStatus().

\par Helper Functions

The CANmod3 core expects all ID bits to be left aligned. This makes setting the ID a
bit cumbersome. Using CANmod3_SetID(), a given right-aligned ID field is modified
according to the ID size which is indicated by the IDE bit. CANmod3_GetID() provides
the reverse operation: It returns the ID right-aligned. CANmod3_FormatMask() packs
the ID, IDE, and RTR bits together as they are used in the mask registers.

\par Basic CAN Message Handling

A <em>Basic CAN</em> type controller contains one or more message filter and one common
message buffer or FIFO. The CAN driver contains some functions to emulate <em>Basic
CAN</em> operation by linking several buffers together to form a buffer array that
shares one message filter. Since this buffer array is not a real FIFO, message
inversion might happen (eg, a newer message might be pulled from the receive
buffer prior to an older message).

Before using the <em>Basic CAN</em> API, the CAN controller has to be configured
first with a CANmod3_ConfigBuffer() call. This sets up the message array and configures
the message filter. CANmod3_SendMessage() and CANmod3_GetMessage() are used to
send and receive a message from transmit or receive buffers. CANmod3_SendMessageReady()
indicates if a new message can be sent. CANmod3_GetMessageAv() shows if a new message
is available.

\par Full CAN Message Handling

In <em>Full CAN</em> operation, each message mailbox has its own message filter. This reduces
the number of receive interrupts as the host CPU only gets an interrupt when a
message of interest has arrived. Further, software based message filtering overhead
is reduced as well as there are less message to be checked.

Before a buffer can be used for <em>Full CAN</em> operation, it needs to be configured using
CANmod3_ConfigBufferN(). An error is generated if this buffer is already reserved
for <em>Basic CAN</em> operation. CANmod3_GetRxBufferStatus() and CANmod3_GetTxBufferStatus()
indicate the current state of the receive and transmit buffers. With CANmod3_SendMessageN()
a message can be sent using buffer N. A pending message transfer can be aborted with
CANmod3_SendMessageAbortN() and a message can be read with CANmod3_GetMessageN().

If a buffer is set for automatic RTR reply, CANmod3_SetRTRMessageN() sets the CAN
message that is returned upon received of the RTR message. Note that the user has
to set the RTR message filter to match the ... With CANmod3_RTRMessageAbortN() a
pending RTR auto-reply can be aborted.

\par Enhanced Full CAN

This is a special case of <em>Full CAN</em> where several mailboxes are linked together to
create FIFOs that share an identical message filter configuration. The CAN driver
doesn't contain any dedicated support for <em>Enhanced Full CAN</em>, but the necessary
functionality can be built upon the available <em>Full CAN</em> functions.

*/
#include "FreeRTOS.h"
#include "ctypes.h"
#include "debug.h"
#include "io.h"

#include "soc_irq.h"
#include "soc_clkc.h"
#include "soc_ioc.h"
#include "soc_gpio.h"
#include "soc_iobridge.h"
#include "soc_pwrc.h"
#include "soc_pwrc_retain.h"
#include "soc_can.h"

#include "core_cm3.h"
#include "CANmod3.h"

/* --- PUBLIC VARIABLES --- */

canmod3_instance_t g_can[ 2 ];

/* --- PUBLIC FUNCTIONS --- */
static UINT32 prvCanDataAlign(UINT32 value, UINT32 len, UINT32 uxLittleEnd )
{
	if( false == uxLittleEnd )
		return value >> ( 8 * ( 4 - len ) ) << ( 8 * ( 4 - len ) );
	else
		return value & ( 0xffffffff >> 8 * ( 4 - len ) );
}

static void prvCanRxHandle( canmod3_instance_t* this_can, UINT8 ucPort )
{
	UINT32 uxCanRxStatus;
	UINT8 ucNum;
	CANmod3_MSGOBJECT xCanMod3Msg;
	xCanIsrRx *pxRxCallback;

	uxCanRxStatus = CANmod3_GetRxBufferStatus( this_can );
	DebugZone( ZONE_CAN_DEBUG, "CAN rx uxCanRxStatus is 0x%x!\r\n", uxCanRxStatus );

	for( ucNum = 0; ucNum < CANmod3_RX_MAILBOX; ucNum++ )
	{
		if( uxCanRxStatus & ( 0x1 << ucNum ) )
		{
			if( CANmod3_VALID_MSG == CANmod3_GetMessageN( this_can, ucNum, &xCanMod3Msg ) )
			{
				if( xCanMod3Msg.dlc > 8 )
					xCanMod3Msg.dlc = 8;
				xCanMod3Msg.dataHigh = prvCanDataAlign( xCanMod3Msg.dataHigh,
							xCanMod3Msg.dlc >= 4 ? 4 : xCanMod3Msg.dlc,
							this_can->hw_reg->Config.endian );
				if( xCanMod3Msg.dlc > 4 )
					xCanMod3Msg.dataLow = prvCanDataAlign( xCanMod3Msg.dataLow,
							xCanMod3Msg.dlc == 8 ? 4 : xCanMod3Msg.dlc - 4,
							this_can->hw_reg->Config.endian );
				else
					xCanMod3Msg.dataLow = 0;

				pxRxCallback = this_can->pxCanIsrRxCallback;
				if ( pxRxCallback )
				{
					pxRxCallback( ucPort, &xCanMod3Msg, ucNum );
				}
			}
			else
			{
				DebugZone( ZONE_CAN_ERROR, "ERR: CANmod3_GetMessageN failed in prvCanRxHandle!\r\n" );
				break;
			}

			uxCanRxStatus &= ~( 0x1 << ucNum );
		}

		if( 0 == uxCanRxStatus )
			break;
	}
}

static UINT32 prvCanInterruptHandle( canmod3_instance_t* this_can, UINT8 ucPort )
{
	UINT32 uxCanIntStatus;
	xCanIsrError *pxErrCallback;
	xCanIsrTx*  pxTxCallback;
	UINT32 uxBufferStatus;

	uxCanIntStatus = CANmod3_GetIntStatus( this_can );
	if ( 0 == uxCanIntStatus )
		return uxCanIntStatus;

	CANmod3_ClearIntStatus( this_can, uxCanIntStatus );
	DebugZone( ZONE_CAN_ISR, "CAN port %d uxCanIntStatus is 0x%x!\r\n", ucPort, uxCanIntStatus );

	if( uxCanIntStatus & CANMOD3_INT_RX_MSG )
	{
		prvCanRxHandle( this_can, ucPort );
		DebugZone( ZONE_CAN_ISR, "CAN(%d) rx interrupt!\r\n", ucPort );
	}

	if( uxCanIntStatus & CANMOD3_INT_TX_MSG ) 
	{
		DebugZone( ZONE_CAN_ISR, "CAN(%d) tx interrupt!\r\n", ucPort );
		pxTxCallback = this_can->pxCanIsrTxCallback;
		uxBufferStatus = CANmod3_GetTxBufferStatus(this_can);
		if ( pxTxCallback )
		{
			pxTxCallback( ucPort, this_can->tx_request_mask & (this_can->tx_request_mask ^ uxBufferStatus) );
		}
		this_can->tx_request_mask &= uxBufferStatus; /* clear those tx successfully one */
	}

	if( uxCanIntStatus & CANMOD3_INT_RTR_MSG )
		DebugZone( ZONE_CAN_ISR, "CAN(%d) - rtr_msg interrupt!\r\n", ucPort );

	if( uxCanIntStatus & CANMOD3_INT_ERROR_MASK )
	{
		pxErrCallback = this_can->pxCanIsrErrCallback;
		if ( pxErrCallback )
		{
			pxErrCallback( ucPort, (uxCanIntStatus & CANMOD3_INT_ERROR_MASK) );
		}

		if( uxCanIntStatus & CANMOD3_INT_ARB_LOSS )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - arb_loss intrerrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_OVR_LOAD )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - over_load interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_BIT_ERR )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - bit error interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_STUFF_ERR )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - bit stuffing error interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_ACK_ERR )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - ack error interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_FORM_ERR )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - format error interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_CRC_ERR )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - CRC error interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_BUS_OFF )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - bus_off interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_RX_MSG_LOST )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - rx_message_lost interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_STUCK_AT_0 )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - stuck_at_0 interrupt!\r\n", ucPort );

		if( uxCanIntStatus & CANMOD3_INT_SST_FAILURE )
			DebugZone( ZONE_CAN_ERROR, "CAN(%d) - sst_failure interrupt!\r\n", ucPort );
	}

	return uxCanIntStatus;
}

static void prvCanbusISR( UINT8 ucPort )
{
	UINT32 uxStatus;
	canmod3_instance_t* this_can = &g_can[ ucPort ];

	if ( !this_can )
	{
		DebugMsg( "CAN%d hadn't been intialized!\r\n", ucPort );
		return;
	}

	NVIC_DisableIRQ( this_can->ucCanIntrNum );
	NVIC_ClearPendingIRQ( this_can->ucCanIntrNum );

	do {
		uxStatus = prvCanInterruptHandle( this_can, ucPort );
	} while ( uxStatus );

	NVIC_EnableIRQ( this_can->ucCanIntrNum );
}

void INT_CBUS0_Handler( void )
{
	prvCanbusISR( CANmod3_PORT0 );
}

void INT_CBUS1_Handler( void )
{
	prvCanbusISR( CANmod3_PORT1 );
}

INT32 CANmod3_RegisterTxCallback( UINT8 ucPort, xCanIsrTx *pxTxCallback )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrTxCallback = pxTxCallback;

    return CANmod3_OK;
}

INT32 CANmod3_RegisterRxCallback( UINT8 ucPort, xCanIsrRx *pxRxCallback )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrRxCallback = pxRxCallback;

    return CANmod3_OK;
}

INT32 CANmod3_RegisterErrCallback( UINT8 ucPort, xCanIsrError *pxErrCallback )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrErrCallback = pxErrCallback;
    return CANmod3_OK;
}

INT32 CANmod3_UnRegisterTxCallback( UINT8 ucPort )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrTxCallback = NULL;

    return CANmod3_OK;
}

INT32 CANmod3_UnRegisterRxCallback( UINT8 ucPort )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrRxCallback = NULL;

    return CANmod3_OK;
}

INT32 CANmod3_UnRegisterErrCallback( UINT8 ucPort )
{
	canmod3_instance_t* this_can = NULL;
	if (ucPort > CANmod3_PORT1 )
		return CANmod3_ERR;
	this_can = &g_can[ ucPort ];

	this_can->pxCanIsrErrCallback = NULL;

    return CANmod3_OK;
}



/**
*  CANmod3_Init() initializes CAN device structure as well as the CAN controller itself.
*
*  @par Operation
*       - Initializes this_can device structure
*       - Initializes all receive mailboxes and disables them (their settings are memory
*         based and are not affected by a system reset!)
*       - Configures the CAN controller
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*  @param   bitRate
Following default bitrate definitions might be used.\n
For 8MHz system clock:
- CANmod3_SPEED_8M_5K
- CANmod3_SPEED_8M_10K
- CANmod3_SPEED_8M_20K
- CANmod3_SPEED_8M_50K
- CANmod3_SPEED_8M_100K
- CANmod3_SPEED_8M_125K
- CANmod3_SPEED_8M_250K
- CANmod3_SPEED_8M_500K
- CANmod3_SPEED_8M_1M
.
For for 16Mhz system clock:
- CANmod3_SPEED_16M_5K
- CANmod3_SPEED_16M_10K
- CANmod3_SPEED_16M_20K
- CANmod3_SPEED_16M_50K
- CANmod3_SPEED_16M_100K
- CANmod3_SPEED_16M_125K
- CANmod3_SPEED_16M_250K
- CANmod3_SPEED_16M_500K
- CANmod3_SPEED_16M_1M
.
For custom settings, use
- CANmod3_SPEED_MANUAL
.
Additionally, the default configuration can be altered by using
- CANmod3_AUTO_RESTART
- CANmod3_ARB_FIXED_PRIO
- CANmod3_LITTLE_ENDIAN

*  @param   pcan_config      Pointer to configuration structure. This structure
is only used when bitRate == CANmod3_SPEED_MANUAL
*  @param   basic_can_rx_mb  Number of rx mailboxes used in basic CAN mode
*  @param   basic_can_tx_mb  Number of tx mailboxes used in basic CAN mode
*
*  @return  Returns CANmod3_OK as long as parameters meet spec.

@par Example 1: Using a default set for bitrate, tseg1, tseg2, and sjw and additional
configuration parameters.
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
CANmod3_Init(
&g_can0,
CANmod3_SPEED_16M_500K | CANmod3_AUTO_RESTART | CANmod3_LITTLE_ENDIAN,
(PCANmod3_CONFIG_REG)0,
16, // Number of Basic CAN rx mailboxes
7   // Number of Basid CAN tx mailboxes
);

return(0);
}
@endcode

@par Example 2: Using custom settings for bitrate, tseg1, tseg2, and sjw.
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

#define SYSTEM_CLOCK   8000000   // 8 MHz System Clock

int main(void)
{
CAN_CONFIG_REG CANReg;

// Then set up second CAN device with custom parameters.
CANReg.cfg_bitrate = CANmod3_CalcClkDivisor(SYSTEM_CLOCK, 8, 500000);
CANReg.cfg_tseg1 = 4;
CANReg.cfg_tseg2 = 1;
CANReg.auto_restart = 0;
CANReg.cfg_sjw = 0;
CANReg.sampling_mode = 0;
CANReg.edge_mode = 0;
CANReg.endian = 1;

CANmod3_Init(
&g_can0,
CANmod3_SPEED_MANUAL,
&CANReg,
8, // Number of Basic CAN rx mailboxes
4  // Number of Basid CAN tx mailboxes
);

return(0);
}
@endcode

*
*/
UINT8
CANmod3_Init(canmod3_instance_t* this_can,
			UINT8 port_num,
			UINT32 bitRate,
			PCANmod3_CONFIG_REG pcan_config,
			UINT8 basic_can_rx_mb,
			UINT8 basic_can_tx_mb)
{
	int n;
	CANmod3_RXMSGOBJECT CANRxOBJ;

	// Initialize the device structure
	if (CANmod3_PORT0 == port_num) {
		/* set clock */
		vIoBridgeWrite( A7DA_PWRC_CAN0_CLK_EN, BIT1 | BIT0 );

		/* set pinmux */
		ioc_enable_can0_mux();

		/* enable can transceiver through GPIO */
		ioc_enable_can_transceiver_mux();

		/* setup gpio direction and value */
		vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_0, 1 );
		if(FALSE==ENABLE_M3_CAN1_WAKUP())
		{
			vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_2, 1 );
		}

		this_can->ucCanIntrNum = INT_CBUS0_IRQn;

		this_can->hw_reg = CAN0_REGISTER_ADDRESS;
	}
	else if (CANmod3_PORT1 == port_num)
	{
		/* set clock */
		clkc_enable_canbus1_io();
		/* set pinmux */
		ioc_enable_can1_mux();

		this_can->ucCanIntrNum = INT_CBUS1_IRQn;

		this_can->hw_reg = CAN1_REGISTER_ADDRESS;
	}
	else
		return(CANmod3_ERR);

	this_can->pxCanIsrTxCallback = 0;
	this_can->pxCanIsrRxCallback = 0;
	this_can->pxCanIsrErrCallback = 0;

	this_can->basic_can_rx_mb = basic_can_rx_mb;
	this_can->basic_can_tx_mb = basic_can_tx_mb;

	// Initialize the rx mailbox
	CANRxOBJ.id = 0;
	CANRxOBJ.dataHigh = 0;
	CANRxOBJ.dataLow = 0;
	CANRxOBJ.amr.l = 0;
	CANRxOBJ.acr.l = 0;
	CANRxOBJ.amr_d = 0;
	CANRxOBJ.acr_d = 0;
	CANRxOBJ.rxb.l = 0 | CANMOD3_RX_WPNH_EBL | CANMOD3_RX_WPNL_EBL;

	for (n=0; n<CANmod3_RX_MAILBOX; n++)
	{
		CANmod3_ConfigBufferN(this_can, n, &CANRxOBJ);
	}

	// Configure CAN controller
	if (bitRate == CANmod3_SPEED_MANUAL)
	{   // If user wants to specify registers directly
		// Check if parameters meet minimums.
		if (pcan_config->cfg_tseg1 < 2)
			return(CANmod3_TSEG1_TOO_SMALL );

		if ( (pcan_config->cfg_tseg2 == 0) ||
			((pcan_config->sampling_mode == 1) && (pcan_config->cfg_tseg2 == 1)) )
			return(CANmod3_TSEG2_TOO_SMALL );

		if ( (pcan_config->cfg_sjw > pcan_config->cfg_tseg1) ||
			(pcan_config->cfg_sjw > pcan_config->cfg_tseg2) )
			return(CANmod3_SJW_TOO_BIG );

		this_can->hw_reg->Config.l = pcan_config->l;
	}
	else
	{  // User has chosen a default setting.
		this_can->hw_reg->Config.l = bitRate;
	}
	// Make it here and everything is fine.
	this_can->hw_reg->IntEbl.l = 0;      // No interrupts

	NVIC_SetPriority( this_can->ucCanIntrNum, API_SAFE_INTR_PRIO_1 );

	return(CANmod3_OK);
}
/**
*  CANmod3_SetConfigReg() sets the configuration register and starts the CAN controller at the same time.
*
*  @par Description:
*  This function is used when one needs to change the configuration settings while the CAN controller
*  was already initialized using CANmod3_Init() and is running. CANmod3_SetConfigReg() should not be
*  used when the CAN controller wasn't initialized yet.
*
*  @par Tasks performed:
*    - Clears all pending interrupts
*    - Stops CAN controller
*    - Disable interrupts
*    - Sets new configuration
*    - Starts CAN controller
*
*  @param  this_can This is a pointer to an canmod3_instance_t structure.
*  @param  cfg      Configuration settings
*
@par Example: Changing the CAN controller configuration parameters
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
....

CANmod3_SetConfigReg(
&g_can0,
CANmod3_SPEED_16M_500K | CANmod3_AUTO_RESTART | CANmod3_LITTLE_ENDIAN
);

....

return(0);
}
@endcode
*
*/
void CANmod3_SetConfigReg(canmod3_instance_t* this_can, UINT32 cfg)
{
	this_can->hw_reg->IntStatus.l = 0;        // Clear all pending interrupts
	this_can->hw_reg->Command.run_stop = 0;   // Disable CAN Device
	this_can->hw_reg->IntEbl.rx_msg = 0;      // Disable receive interrupts.
	this_can->hw_reg->IntEbl.int_ebl = 0;     // Disable interrupts from CAN device.
	this_can->hw_reg->Config.l = cfg;         // Sets configuration bits
	CANmod3_Start(this_can);
};


/**
*  CANmod3_Mode() sets the CAN controller operating mode.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  mode       Defines the desired operating mode
*                      - CANOP_MODE_NORMAL        : Sets normal operating mode
*                      - CANOP_MODE_LISTEN_ONLY   : In listen-only mode, the CAN controller does
*                                                   not send any messages. Normally used for
*                                                   automatic bitrate detection
*                      - CANOP_MODE_INT_LOOPBACK  : Selects internal loopback mode. This is used
*                                                   for self-test
*                      - CANOP_MODE_EXT_LOOPBACK  : Selects external loopback. The CAN controller
*                                                   will receive a copy of each message sent.
*                      - CANOP_SRAM_TEST_MODE     : Sets SRAM test mode
*                      - CANOP_SW_RESET           : Issues a software reset
*
*  @return CANmod3_OK
*
*  @par Description
*    - Stops CAN device
*    - Sets operating mode.
*    - Does not restart CAN device. Call CANmod3_Start() to do this.
*/
UINT8 CANmod3_Mode(canmod3_instance_t* this_can, UINT32 mode)
{
	this_can->hw_reg->Command.run_stop = 0;
	this_can->hw_reg->Command.l = mode;
	return(CANmod3_OK);
};


/**
*  CANmod3_Start() starts the CAN controller and enables interrupts
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return  CANmod3_OK
*
*  @par Description
*  Clears interrupts and then enables the device. It's expected that the appropriate
*  interrupt enable flags have already been set as well as the operating mode flags
*  using CANmod3_Mode().
*/
UINT8 CANmod3_Start(canmod3_instance_t* this_can)
{
	this_can->hw_reg->IntStatus.l = 0xfffc;        // Clear all pending interrupts
	this_can->hw_reg->Command.run_stop = 1;   // Enable CAN Device
	this_can->hw_reg->IntEbl.rx_msg = 1;      // Enable receive interrupts.
	this_can->hw_reg->IntEbl.int_ebl = 1;     // Enable interrupts from CAN device.

	NVIC_EnableIRQ( this_can->ucCanIntrNum );
	return(CANmod3_OK);
};

/**
*  CANmod3_Stop() stops the CAN controller leaving interrupt flags as they are.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return CANmod3_OK
*/
UINT8 CANmod3_Stop(canmod3_instance_t* this_can)
{
	this_can->hw_reg->Command.run_stop = 0;
	NVIC_DisableIRQ( this_can->ucCanIntrNum );
	return(CANmod3_OK);
};

/**
*  CANmod3_GetID() returns ID bits right justified based on ID size.
*
*  @param pMsg Pointer to message object.
*
*  @return Returns ID
*/
UINT32 CANmod3_GetID(PCANmod3_MSGOBJECT pMsg )
{
	if (pMsg->ide)
		return(pMsg->id);
	else
		return(pMsg->id >> 18);
}


/**
*  CANmod3_SetID() returns ID bits left justified based on ID size.
*
*  @param   pMsg  Pointer to message object.
*
*  @return  Returns ID
*/
UINT32 CANmod3_SetID(PCANmod3_MSGOBJECT pMsg )
{
	if (pMsg->ide)
		return(pMsg->id);
	else
		return(pMsg->id << 18);
}

/**
*  CANmod3_FormatMask() packs the ID, IDE, and RTR bits together as they are used in the
*  message filter mask.
*
*  @param  id    Identifier
*  @param  ide   IDE Flag
*  @param  rtr   RTR Flag
*
*  @returns Packed ID
*/
UINT32 CANmod3_FormatMask(UINT32 id, UINT8 ide, UINT8 rtr)
{
	if (ide)
		id <<= 3;
	else
	{
		id <<= 21;
		id |= 0x3FFFF << 3; // set unused ID bits to 1!
	}
	id |= ((ide << 2) | (rtr << 1));
	return(id);
}


/**
*  CANmod3_SetGlobalIntEbl() sets the global interrupt enable flag.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @par Example
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
....
CANmod3_SetGlobalIntEbl(
&g_can0
);
....

return(0);
}
@endcode
*/
UINT8 CANmod3_SetGlobalIntEbl(canmod3_instance_t* this_can)
{
	this_can->hw_reg->IntEbl.int_ebl = 1;
	return(CANmod3_OK);
}


UINT8 CANmod3_ClrGlobalIntEbl(canmod3_instance_t* this_can)
{
	this_can->hw_reg->IntEbl.int_ebl = 0;
	return(CANmod3_OK);
}


/**
*  CANmod3_SetIntEbl() sets the selected interrupt flags.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  flag  Interrupt enable flag to be set
*
*  @par Example
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
....
CANmod3_SetIntEbl(
&g_can0,
CANMOD3_INT_BUS_OFF | CANMOD3_INT_RX_MSG
);
....

return(0);
}
@endcode
*/
UINT8 CANmod3_SetIntEbl(canmod3_instance_t* this_can, UINT32 flag)
{
	this_can->hw_reg->IntEbl.l = flag;
	return(CANmod3_OK);
}


/**
*  CANmod3_GetGlobalIntEbl() returns the current global interrupt enable flag.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @returns Returns global interrupt enable flag
*
*
*/
UINT32 CANmod3_GetGlobalIntEbl(canmod3_instance_t* this_can)
{
	return(this_can->hw_reg->IntEbl.int_ebl);
}


/**
*  CANmod3_GetIntEbl() returns the current interrupt enable flags.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @returns Returns interrupt enable flag
*
*
*/
UINT32 CANmod3_GetIntEbl(canmod3_instance_t* this_can)
{
	return(this_can->hw_reg->IntEbl.l & 0xFFFFFFFC);
}


/** CANmod3_ClearIntStatus() clears the selected interrupt flags.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  flag       Interrupt flags to be cleared

*  @return  Returns
*  @par Example
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
....
// clear rx msg interrupt flag
CANmod3_ClearIntStatus(
&g_can0,
CANMOD3_INT_RX_MSG
);

....

return(0);
}
@endcode
*
*/
UINT8 CANmod3_ClearIntStatus(canmod3_instance_t* this_can, UINT32 flag)
{
	this_can->hw_reg->IntStatus.l = flag;
	return(CANmod3_OK);
}


/** CANmod3_GetIntStatus() clears the selected interrupt flags.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  flag       Interrupt flags to be cleared

*  @return Returns interrupt status flag
@par Example:
@code
#include "CANmod3.h"

canmod3_instance_t g_can0;

int main(void)
{
....
// check if bus_off flag is set
if (CANmod3_GetIntStatus(&g_can0) & CANMOD3_INT_BUS_OFF){
// bus_off flag is set
....
}
....

return(0);
}
@endcode
*
*/

UINT32 CANmod3_GetIntStatus(canmod3_instance_t* this_can)
{
	return(this_can->hw_reg->IntStatus.l);
}


/**
*  CANmod3_SetRTRMessageN() loads mailbox n with the given CAN message. This message
*  will be sent out after receipt of a RTR message request. The function verifies
*  that the given mailbox is configured for Full CAN and that RTR auto-reply is
*  enabled

*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*  @param   n          Mailbox number
*  @param   pMsg       Pointer to CAN message object
*  @return  CANmod3_OK, CANmod3_BASIC_CAN_MAILBOX, or CANmod3_NO_RTR_MAILBOX
*/

UINT8 CANmod3_SetRTRMessageN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_MSGOBJECT pMsg)
{
	// Is buffer configured for Full CAN?
	if (n >= CANmod3_RX_MAILBOX - this_can->basic_can_rx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX);
	}

	// Is buffer configured for RTR auto-replay?
	if (this_can->hw_reg->RxMsg[n].rxb.rtrReply == 0)
	{
		return(CANmod3_NO_RTR_MAILBOX);
	}
	else
	{
		this_can->hw_reg->RxMsg[n].id = pMsg->id;                // Transfer the ID.
		this_can->hw_reg->RxMsg[n].dataLow = pMsg->dataLow;      // lower data
		this_can->hw_reg->RxMsg[n].dataHigh = pMsg->dataHigh;    // higher data
		return(CANmod3_OK);
	}
}

/**
*  CANmod3_RTRMessageAbortN() aborts a RTR message transmit request on mailbox n and
*  checks that message abort was successful.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  n          Mailbox number
*
*  @return CANmod3_BASIC_CAN_MAILBOX, CANmod3_OK, or CANmod3_ERR
*/
UINT8 CANmod3_RTRMessageAbortN(canmod3_instance_t* this_can, UINT8 n)
{
	// Is buffer configured for Full CAN?
	if (n >= CANmod3_RX_MAILBOX - this_can->basic_can_rx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX); // mailbox is configured for basic CAN
	}

	if (this_can->hw_reg->RxMsg[n].rxb.rtrReply == 0)
		return(CANmod3_NO_RTR_MAILBOX);

	// set abort request
	this_can->hw_reg->RxMsg[n].rxb.rtrAbort = 1;

	// check the abort is granted
	if (this_can->hw_reg->RxMsg[n].rxb.rtrReplyPend == 0)
	{ // If the Rx buffer isn't busy....
		return(CANmod3_OK);        // Abort was successful
	}
	else
	{
		return(CANmod3_ERR);		// Message not aborted.
	}
}

/**
*  CANmod3_ConfigBuffer() configures all mailbox set for Basic CAN operation.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*  @param   pFilter    Pointer to CAN filter object
*  @return  CANmod3_INVALID_MAILBOX, CANmod3_OK, or CANmod3_BASIC_CAN_MAILBOX
*/

UINT8 CANmod3_ConfigBuffer(canmod3_instance_t* this_can, PCANmod3_FILTEROBJECT pFilter, UINT32 RxCtrl)
{
	UINT8 success = CANmod3_NO_MSG;
	UINT8 n;

	// At least one buffer needs to be configured for Basic CAN
	if (this_can->basic_can_rx_mb == 0)
	{
		return(CANmod3_INVALID_MAILBOX);
	}

	// Loop through all Basic CAN buffers
	for (n = CANmod3_RX_MAILBOX - this_can->basic_can_rx_mb; n < CANmod3_RX_MAILBOX; n++)
	{
		// Set filters
		this_can->hw_reg->RxMsg[n].acr.l = pFilter->acr.l;
		this_can->hw_reg->RxMsg[n].amr.l = pFilter->amr.l;
		this_can->hw_reg->RxMsg[n].amr_d = pFilter->amcr_d.mask;
		this_can->hw_reg->RxMsg[n].acr_d = pFilter->amcr_d.code;

		// Configure mailbox

		// is this the last buffer?
		if (n<CANmod3_RX_MAILBOX-1){
			// set link flag
			this_can->hw_reg->RxMsg[n].rxb.l = RxCtrl | CANMOD3_RX_WPNH_EBL |
					CANMOD3_RX_WPNL_EBL | CANMOD3_RX_LINK_EBL;
					// | CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;
		}
		else
		{
			this_can->hw_reg->RxMsg[n].rxb.l = RxCtrl | CANMOD3_RX_WPNH_EBL |
					CANMOD3_RX_WPNL_EBL;// | CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL ;
		}
		success = CANmod3_OK;
	}
	return(success);
}


/**
*  CANmod3_ConfigBufferN() configures rx mailbox n with the given parameters. The
*  function checks that the mailbox is set for Full CAN operation.

*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*  @param   n          Mailbox number
*  @param   pMsg       Pointer to CAN message object
*  @return  CANmod3_OK or CANmod3_BASIC_CAN_MAILBOX
*/

UINT8 CANmod3_ConfigBufferN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_RXMSGOBJECT pMsg)
{
	// Is buffer configured for Full CAN?
	if (n >= CANmod3_RX_MAILBOX - this_can->basic_can_rx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX);
	}

	// Configure mailbox
	this_can->hw_reg->RxMsg[n].id = pMsg->id;                // Transfer the ID.
	this_can->hw_reg->RxMsg[n].dataLow = pMsg->dataLow;      // lower data
	this_can->hw_reg->RxMsg[n].dataHigh = pMsg->dataHigh;    // higher data
	this_can->hw_reg->RxMsg[n].acr.l = pMsg->acr.l;
	this_can->hw_reg->RxMsg[n].amr.l = pMsg->amr.l;
	this_can->hw_reg->RxMsg[n].amr_d = pMsg->amr_d;
	this_can->hw_reg->RxMsg[n].acr_d = pMsg->acr_d;
	this_can->hw_reg->RxMsg[n].rxb.l = pMsg->rxb.l | CANMOD3_RX_WPNH_EBL | CANMOD3_RX_WPNL_EBL; // Set WPNx
	return(CANmod3_OK);
}


/**
*  CANmod3_GetMessageN() pulls a message from rx mailbox n and returns it.
*  The function checks that the mailbox is set for Full CAN operation, is enabled and that
*  a new message is available. Once the message is pulled from the mailbox, the message
*  receipt is acknowledged.
*
*  @param  this_can  This is a pointer to an canmod3_instance_t structure.
*  @param  n         Mailbox number
*  @param  pMsg      Pointer to CAN message object that will hold the CAN message.
*  @return CANmod3_BASIC_CAN_MAILBOX, CANmod3_NO_MSG, or CANmod3_VALID_MSG
*
*/
UINT8 CANmod3_GetMessageN(canmod3_instance_t* this_can, UINT8 n, PCANmod3_MSGOBJECT pMsg)
{
	// Check that a new message is available and get it
	if (this_can->hw_reg->RxMsg[n].rxb.msgAv)
	{

		pMsg->id = this_can->hw_reg->RxMsg[n].id;			// Copy ID
		pMsg->dataLow = this_can->hw_reg->RxMsg[n].dataLow;		// Copy 4 of the data bytes
		pMsg->dataHigh = this_can->hw_reg->RxMsg[n].dataHigh;		// Copy the other 4 data bytes.
		pMsg->l = this_can->hw_reg->RxMsg[n].rxb.l;			// Get DLC, IDE and RTR and time stamp.

		this_can->hw_reg->RxMsg[n].rxb.msgAv = 1;       // Ack that it's been removed from the FIFO
		return(CANmod3_VALID_MSG);   // And let app know there is a message.
	}
	else
	{
		return(CANmod3_NO_MSG);
	}
}


/**
*  CANmod3_GetMessage() pulls a message from the first mailbox set for Basic CAN
*  operation that contains a message. Once the message has been pulled from the
*  mailbox, the message receipt is acknowledged.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  pMsg       Pointer to CAN message object that will hold the received message.
*
*  @return CANmod3_INVALID_MAILBOX, CANmod3_NO_MSG, or CANmod3_VALID_MSG
*
*  @note   Since neither a hardware nor a software FIFO exists, message inversion might happen (eg, a
*          newer message might be pulled from the receive buffer prior to an older message).
*/
UINT8 CANmod3_GetMessage(canmod3_instance_t* this_can, PCANmod3_MSGOBJECT pMsg)
{
	UINT8 success = CANmod3_NO_MSG;
	UINT8 n;

	// Is a buffer configured for Basic CAN?
	if (this_can->basic_can_rx_mb == 0)
	{
		return(CANmod3_INVALID_MAILBOX);
	}

	// Find next BASIC CAN buffer that has a message available
	for (n = CANmod3_RX_MAILBOX - this_can->basic_can_rx_mb; n < CANmod3_RX_MAILBOX; n++)
	{
		// Check that if there is a valid message
		if (this_can->hw_reg->RxMsg[n].rxb.msgAv)
		{
			pMsg->id = this_can->hw_reg->RxMsg[n].id;            // Copy ID
			pMsg->dataLow = this_can->hw_reg->RxMsg[n].dataLow;  // Copy 4 of the data bytes
			pMsg->dataHigh = this_can->hw_reg->RxMsg[n].dataHigh;// Copy the other 4 data bytes.
			pMsg->l = this_can->hw_reg->RxMsg[n].rxb.l;          // Get DLC, IDE and RTR and time stamp.

			this_can->hw_reg->RxMsg[n].rxb.msgAv = 1;      // Ack that it's been removed from the FIFO
			success = CANmod3_OK;
			break;
		}
	}
	return(success);
}


/**
*  CANmod3_GetMessageAv() indicates if the receive buffer contains a new message.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return CANmod3_INVALID_MAILBOX, CANmod3_NO_MSG, or CANmod3_VALID_MSG
*/
UINT8 CANmod3_GetMessageAv(canmod3_instance_t* this_can)
{
	UINT8 success = CANmod3_NO_MSG;
	UINT8 n;

	// Is a buffer configured for Basic CAN?
	if (this_can->basic_can_rx_mb == 0)
	{
		return(CANmod3_INVALID_MAILBOX);
	}

	// Find next BASIC CAN buffer that has a message available
	for(n=CANmod3_RX_MAILBOX-this_can->basic_can_rx_mb; n<CANmod3_RX_MAILBOX; n++)
	{
		// Check that buffer is enabled and contains a message
		if (this_can->hw_reg->RxMsg[n].rxb.msgAv)
		{
			success = CANmod3_OK;
			break;
		}
	}

	return(success);
}


/**
*  CANmod3_SendMessageN() sends a message using mailbox n. The function verifies that
*  this mailbox is configured for Full CAN operation and is empty.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  n          Mailbox number
*  @param  pMsg       Pointer to object that holds the CAN message to transmit.
*
*  @return CANmod3_BASIC_CAN_MAILBOX, CANmod3_NO_MSG, or CANmod3_OK
*/
UINT8 CANmod3_SendMessageN(canmod3_instance_t* this_can, UINT8 n, UINT32 bSst, PCANmod3_MSGOBJECT pMsg)
{
	// Is buffer configured for Full CAN?
	if (n >= CANmod3_TX_MAILBOX - this_can->basic_can_tx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX); // mailbox is configured for basic CAN
	}

	if (this_can->hw_reg->TxMsg[n].txb.txReq == 0)
	{ // If the Tx buffer isn't busy....
		this_can->hw_reg->TxMsg[n].id = pMsg->id;
		this_can->hw_reg->TxMsg[n].dataLow = pMsg->dataLow;
		this_can->hw_reg->TxMsg[n].dataHigh = pMsg->dataHigh;

		if(CAN_SEND_SST == bSst)
			this_can->hw_reg->TxMsg[n].txb.l = pMsg->l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL
									| CANMOD3_TX_REQ | CANMOD3_TX_ABORT_REQ | CANMOD3_TX_INT_EBL;
		else
			this_can->hw_reg->TxMsg[n].txb.l = pMsg->l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ | CANMOD3_TX_INT_EBL;

		this_can->tx_request_mask |= (UINT32)1<<n;
		return(CANmod3_OK);        // All went well.
	}
	else
	{
		return(CANmod3_NO_MSG);    // Message not sent.
	}
}


/**
*  CANmod3_TxMaiboxNReady() indicates if a mailbox n can be used.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return CANmod3_INVALID_MAILBOX, CANmod3_NO_MSG, or CANmod3_OK
*
*/
UINT8 CANmod3_TxMaiboxNReady(canmod3_instance_t* this_can, UINT8 n)
{

	// Is buffer configured for Full CAN?
	if (n >= CANmod3_TX_MAILBOX - this_can->basic_can_tx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX); // mailbox is configured for basic CAN
	}


	if (this_can->hw_reg->TxMsg[n].txb.txReq == 0)
	{ // If the Tx buffer isn't busy....
		return(CANmod3_OK);	   // All went well.
	}
	else
	{
		return(CANmod3_INVALID_MAILBOX);    // Message not sent.
	}

}


/**
*  CANmod3_SendMessageAbortN() aborts a message transmit request on mailbox n and
*  checks that message abort was successful.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  n          Mailbox number
*
*  @return CANmod3_BASIC_CAN_MAILBOX, CANmod3_OK, or CANmod3_ERR
*/
UINT8 CANmod3_SendMessageAbortN(canmod3_instance_t* this_can, UINT8 n)
{
	// Is buffer configured for Full CAN?
	if (n >= CANmod3_TX_MAILBOX - this_can->basic_can_tx_mb)
	{
		return(CANmod3_BASIC_CAN_MAILBOX); // mailbox is configured for basic CAN
	}

	// set abort request
	this_can->hw_reg->TxMsg[n].txb.l = (this_can->hw_reg->TxMsg[n].txb.l & ~CANMOD3_TX_REQ) | CANMOD3_TX_ABORT_REQ;

	// check the abort is granted
	if (this_can->hw_reg->TxMsg[n].txb.txAbort == 0) { // txAbort not pending anymore?
		return(CANmod3_OK);        // Abort was successful
	} else {
		return(CANmod3_ERR);    // Message not aborted.
	}
}


/**
*  CANmod3_SendMessageReady() indicates if a new message can be sent.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return CANmod3_INVALID_MAILBOX, CANmod3_NO_MSG, or CANmod3_OK
*
*/
UINT8 CANmod3_SendMessageReady(canmod3_instance_t* this_can)
{
	UINT8 success = CANmod3_NO_MSG;
	UINT8 n;

	// Is a buffer configured for Basic CAN?
	if (this_can->basic_can_tx_mb == 0)
	{
		return(CANmod3_INVALID_MAILBOX);
	}

	// Find next BASIC CAN buffer that is available
	for (n=CANmod3_TX_MAILBOX-this_can->basic_can_tx_mb; n<CANmod3_TX_MAILBOX; n++)
	{
		if (this_can->hw_reg->TxMsg[n].txb.txReq == 0)
		{ // Tx buffer isn't busy....
			success = CANmod3_OK;
			break;
		}
	}

	return(success);
}


/**
*  CANmod3_SendMessage() sends a message from the first available mailbox set for Basic CAN
*  operation.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  pMsg       Pointer to object that holds the CAN message to transmit.
*
*  @return CANmod3_INVALID_MAILBOX, CANmod3_NO_MSG, or CANmod3_VALID_MSG
*
*  @note   Since neither a hardware nor a software FIFO exists, message inversion might happen (eg, a
*          newer message might be send from the transmit buffer prior to an older message).
*/
UINT8 CANmod3_SendMessage(canmod3_instance_t* this_can, PCANmod3_MSGOBJECT pMsg)
{
	UINT8 success = CANmod3_NO_MSG;
	UINT8 n;

	// Is a buffer configured for Basic CAN?
	if (this_can->basic_can_tx_mb == 0)
	{
		return(CANmod3_INVALID_MAILBOX);
	}

	// Find next BASIC CAN buffer that is available
	for (n=CANmod3_TX_MAILBOX-this_can->basic_can_tx_mb; n<CANmod3_TX_MAILBOX; n++)
	{
		// Check which transmit mailbox is not busy and use it.
		if ((CANmod3_GetTxBufferStatus(this_can) & (1<<n)) == 0)
		{ // If the Tx buffer isn't busy....
			this_can->hw_reg->TxMsg[n].id = pMsg->id;
			this_can->hw_reg->TxMsg[n].dataLow = pMsg->dataLow;
			this_can->hw_reg->TxMsg[n].dataHigh = pMsg->dataHigh;
			this_can->hw_reg->TxMsg[n].txb.l = pMsg->l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_REQ | CANMOD3_TX_INT_EBL;
			this_can->tx_request_mask |= (UINT32)1<<n;
			success = CANmod3_OK;        // All went well.
			break;
		}
	}

	return(success);
}


/**
*  CANmod3_GetMaskN() returns the message filter settings of the selected
*  mailbox. The function checks that the mailbox is set for Full CAN operation.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  n          Mailbox number
*  @param  pamr       Pointer to acceptance mask
*  @param  pacr       Pointer to acceptance code
*  @param  pdta_amr   Pointer to acceptance mask of first two data bytes
*  @param  pdta_acr   Pointer to acceptance code of first two data bytes
*
*  @return  CANmod3_OK or CANmod3_BASIC_CAN_MAILBOX
*/
UINT8
CANmod3_GetMaskN(canmod3_instance_t* this_can, UINT8 n, PUINT32 pamr, PUINT32 pacr, PUINT16 pdta_amr, PUINT16 pdta_acr)
{
	if (n >= CANmod3_RX_MAILBOX)
		return(CANmod3_BASIC_CAN_MAILBOX);

	*pamr = this_can->hw_reg->RxMsg[n].amr.l;
	*pacr = this_can->hw_reg->RxMsg[n].acr.l;
	*pdta_acr = this_can->hw_reg->RxMsg[n].acr_d;
	*pdta_amr = this_can->hw_reg->RxMsg[n].amr_d;

	return(CANmod3_OK);
}


/**
*  CANmod3_SetMaskN() sets the message filter of the selected mailbox. The
*  function checks that the mailbox is set for Full CAN operation.
*
*  @param  this_can   This is a pointer to an canmod3_instance_t structure.
*  @param  n          Mailbox number
*  @param  amr        Acceptance mask
*  @param  acr        Acceptance code
*  @param  dta_amr    Acceptance mask of first two data bytes
*  @param  dta_acr    Acceptance code of first two data bytes
*
*  @return  CANmod3_OK or CANmod3_BASIC_CAN_MAILBOX
*/
UINT8
CANmod3_SetMaskN(canmod3_instance_t* this_can, UINT8 n, UINT32 amr, UINT32 acr, UINT16 dta_amr, UINT16 dta_acr)
{

	if (n >= CANmod3_RX_MAILBOX)
		return(CANmod3_BASIC_CAN_MAILBOX);

	this_can->hw_reg->RxMsg[n].amr.l = amr;
	this_can->hw_reg->RxMsg[n].acr.l = acr;
	this_can->hw_reg->RxMsg[n].amr_d = (UINT32)dta_amr;
	this_can->hw_reg->RxMsg[n].acr_d = (UINT32)dta_acr;

	return(CANmod3_OK);
}


/**
*  CANmod3_GetRxBufferStatus() returns the buffer status of all rx mailboxes
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @returns Rx buffer status
*/
UINT32 CANmod3_GetRxBufferStatus(canmod3_instance_t* this_can)
{
	return(this_can->hw_reg->BufferStatus.rxMsgAv);
}


/**
*  CANmod3_GetTxBufferStatus() returns the buffer status of all tx mailboxes
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @returns Tx buffer status
*/
UINT32 CANmod3_GetTxBufferStatus(canmod3_instance_t* this_can)
{
	return(this_can->hw_reg->BufferStatus.txReq);
}


/**
*  CANmod3_GetErrorStatus() gets the error state of the CAN controller.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*  @param   stat       Returns current error status
*
*  @returns  Error status: 0 error active, 1 error passive, 2 bus-off
*/
UINT8 CANmod3_GetErrorStatus(canmod3_instance_t* this_can, PUINT32 stat)
{
	*stat = this_can->hw_reg->ErrorStatus.l; // Supply error reg info if user wants.
	return(((*stat) >> 16) & 0x03);          // 00 Error Active, 01 Error Passive, 1x Bus Off
}


/**
* CANmod3_GetRxErrorCount() returns the current receive error counter.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return  Receive error counter
*/
UINT32 CANmod3_GetRxErrorCount(canmod3_instance_t* this_can)
{
	return((this_can->hw_reg->ErrorStatus.l >> 8) & 0x0FF);
}


/**
* CANmod3_GetRxGTE96() returns 1 when the receive error counter is greater or
* equal to 96.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return  RxGTE96 flag
*/
UINT32 CANmod3_GetRxGTE96(canmod3_instance_t* this_can)
{
	return((this_can->hw_reg->ErrorStatus.l >> 19) & 0x01);
}


/**
* CANmod3_GetTxErrorCount() returns the current receive error count.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return  Receive error count
*/
UINT32 CANmod3_GetTxErrorCount(canmod3_instance_t* this_can)
{
	return((this_can->hw_reg->ErrorStatus.l >> 8) & 0x0FF);
}


/**
* CANmod3_GetTxGTE96() returns 1 when the receive error counter is greater or
* equal to 96.
*
*  @param   this_can   This is a pointer to an canmod3_instance_t structure.
*
*  @return  TxGTE96 flag
*/
UINT32 CANmod3_GetTxGTE96(canmod3_instance_t* this_can)
{
	return((this_can->hw_reg->ErrorStatus.l >> 18) & 0x01);
}
