/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"
#include "io.h"
#include "pm.h"

#include "soc_irq.h"
#include "soc_pwrc.h"
#include "soc_cache.h"
#include "soc_can.h"
#include "soc_gpio.h"
#include "soc_iobridge.h"
#include "soc_ioc.h"
#include "soc_timer.h"
#include "misclib.h"

#include "core_cm3.h"

#include "CANmod3.h"
#include "canbus.h"

static PCAN_HANDLE s_pxCanHandle[ 2 ];

#ifdef ENABLE_CAN_ERROR_TX_ABORT_TEST
static UINT8 g_last_tx_msg_box[2];
#endif
extern canmod3_instance_t g_can[ 2 ];

static UINT8 cCanZoneQueueFullError = TRUE;
static void prvCanRxCallBack( UINT8 ucPort, PCANmod3_MSGOBJECT pxCanMod3Msg, UINT32 uxBufferNum )
{
	CAN_MSGOBJECT xMsg;
	PCAN_HANDLE pxCanHandle = NULL;

	pxCanHandle = s_pxCanHandle[ ucPort ];

	xMsg.ide = pxCanMod3Msg->ide;
	xMsg.rtr = pxCanMod3Msg->rtr;
	xMsg.dlc = pxCanMod3Msg->dlc;
	xMsg.id  = CANmod3_GetID( pxCanMod3Msg );
	xMsg.dataHigh = pxCanMod3Msg->dataHigh;
	xMsg.dataLow  = pxCanMod3Msg->dataLow;

	BaseType_t xHigherPriorityTaskWoken = FALSE;

	if( pxCanHandle->xRxChannels[ uxBufferNum ] != NULL )
	{
		if( errQUEUE_FULL == xQueueSendFromISR( pxCanHandle->xRxChannels[ uxBufferNum ], &xMsg, &xHigherPriorityTaskWoken ) )
			DebugZone( cCanZoneQueueFullError, "ERR: CAN(%d) queue is full for buffer(%d)!\r\n", ucPort, uxBufferNum );
	}

	if( pxCanHandle->xVirtCanQueue != NULL )
	{
		#ifdef CONFIG_ENABLE_VIRTIO_CAN_TIME_TEST
		if( CONFIG_VIRTIO_CAN_TIME_TEST_ID ==xMsg.id)
		{
			uint32_t xTimeStamp = uxOsGetCurrentTick();
			xMsg.data[0] = (xTimeStamp>>24)&0xFF;
			xMsg.data[1] = (xTimeStamp>>16)&0xFF;
			xMsg.data[2] = (xTimeStamp>>8 )&0xFF;
			xMsg.data[3] = (xTimeStamp    )&0xFF;
		}
		#endif
		if( errQUEUE_FULL == xQueueSendFromISR( pxCanHandle->xVirtCanQueue, &xMsg, &xHigherPriorityTaskWoken ) )
			DebugZone( cCanZoneQueueFullError, "ERR: CAN(%d) queue is full for dump!\r\n", ucPort );
	}

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	return;
}

static void prvCanTxCallBack( UINT8 ucPort, UINT32 xTxMask)
{
	PCAN_HANDLE pxCanHandle = NULL;
	BaseType_t xHigherPriorityTaskWoken, xResult;

	xHigherPriorityTaskWoken = FALSE;

	pxCanHandle = s_pxCanHandle[ ucPort ];
	if( pxCanHandle->xTxEventH != NULL )
	{
		if((xTxMask&0xFFFF0000) != 0)
		{
			xResult = xEventGroupSetBitsFromISR( pxCanHandle->xTxEventH, (EventBits_t)((xTxMask>>16)&0xFFFF), &xHigherPriorityTaskWoken );

			if( xResult == pdPASS )
			{
				portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
			}
		}
	}

	if( pxCanHandle->xTxEventL != NULL )
	{
		if((xTxMask&0x0000FFFF) != 0)
		{
			xResult = xEventGroupSetBitsFromISR( pxCanHandle->xTxEventL, (EventBits_t)(xTxMask&0xFFFF), &xHigherPriorityTaskWoken );

			if( xResult == pdPASS )
			{
				portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
			}
		}
	}
}

static void prvCanErrCallBack( UINT8 ucPort, UINT32 xIntStatus)
{
	PCAN_HANDLE pxCanHandle = NULL;
	BaseType_t xHigherPriorityTaskWoken, xResult;

	pxCanHandle = s_pxCanHandle[ ucPort ];
	#ifdef ENABLE_CAN_ERROR_TX_ABORT_TEST
	if( xIntStatus & CANMOD3_INT_ARB_LOSS )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) arb lost test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}

	if( xIntStatus & CANMOD3_INT_BIT_ERR )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) bit error test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}

	if( xIntStatus & CANMOD3_INT_STUFF_ERR )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) stuff error test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}

	if( xIntStatus & CANMOD3_INT_ACK_ERR )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) ack error test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}

	if( xIntStatus & CANMOD3_INT_FORM_ERR )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) format error test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}

	if( xIntStatus & CANMOD3_INT_CRC_ERR )
	{
		CANmod3_SendMessageAbortN(pxCanHandle->pxCanMod3Instance,g_last_tx_msg_box[ucPort]);
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN(%d) crc error test, abort box %d!\r\n", ucPort ,g_last_tx_msg_box[ucPort]);
	}
	#endif
	if( pxCanHandle->xErrEvent != NULL )
	{
		xResult = xEventGroupSetBitsFromISR( pxCanHandle->xErrEvent, (EventBits_t)xIntStatus, &xHigherPriorityTaskWoken );

		if( xResult == pdPASS )
		{
			portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
		}
	}
}


void vCanSetQueueFullErrorEnabled(BOOL enable)
{
	cCanZoneQueueFullError = enable;
}

/**
*  xCanInit() initializes CAN device structure as well as the CAN controller itself.
*
*  @par Operation
*       - Initializes pxCanHandle->pxCanMod3Instance  device structure
*       - Initializes all receive mailboxes and disables them (their settings are memory
*         based and are not affected by a system reset!)
*       - Configures the CAN controller
*
*  @param   ucPortNum   Which can controller is used.
*  @param   bitRate
Following default bitrate definitions might be used.\n
For 8MHz system clock:

For for 48Mhz system clock:
- CAN_SPEED_48M_5K
- CAN_SPEED_48M_10K
- CAN_SPEED_48M_20K
- CAN_SPEED_48M_50K
- CAN_SPEED_48M_100K
- CAN_SPEED_48M_125K
- CAN_SPEED_48M_250K
- CAN_SPEED_48M_500K
- CAN_SPEED_48M_800K
- CAN_SPEED_48M_1M
.
For custom settings, use
- CANmod3_SPEED_MANUAL
.
Additionally, the default configuration can be altered by using
- CAN_AUTO_RESTART
- CAN_ARB_FIXED_PRIO
- CAN_LITTLE_ENDIAN

*  @param   pcan_config      Pointer to configuration structure. This structure
is only used when bitRate == CANmod3_SPEED_MANUAL
*  @param   basic_can_rx_mb  Number of rx mailboxes used in basic CAN mode
*  @param   basic_can_tx_mb  Number of tx mailboxes used in basic CAN mode
*
*  @return  Returns the pointer to CAN_HANDLE.
*/
INT32 xCanInit( UINT8 ucPortNum, UINT32 uxBitRate, PCAN_CONFIG_REG pxCanConfig,
			UINT8 uxBasicCanRxMb, UINT8 uxBasicCanTxMb )
{
	PCAN_HANDLE pxCanHandle = NULL;

	if ( ucPortNum > CAN_PORT1 )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: Only support CAN0 and CAN1 ports!\r\n" );
		return CAN_ERR;
	}

	if( NULL != s_pxCanHandle[ ucPortNum ] )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN port %d is be using!\r\n", ucPortNum );
		return CAN_ERR;
	}

	pxCanHandle =( PCAN_HANDLE )pvPortMalloc( sizeof( CAN_HANDLE ) );
	if( pxCanHandle == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: allocating memroy for pxCanHandle failed in xCanInit!\r\n" );
		return CAN_ERR;
	}
	memset( pxCanHandle, 0, sizeof( CAN_HANDLE ) );

	pxCanHandle->ucCanPortNum = ucPortNum;
	pxCanHandle->xVirtCanQueue = NULL;

	pxCanHandle->xRxBufferLock = xSemaphoreCreateMutex();
	if( pxCanHandle->xRxBufferLock == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: xRxBufferLock failed in xCanInit!\r\n" );
		goto ERR_FREE_HANDLE;
	}

	pxCanHandle->pxCanMod3Instance = &g_can[ ucPortNum ];

	if( CANmod3_OK != CANmod3_Init( pxCanHandle->pxCanMod3Instance,
					ucPortNum,
					uxBitRate,
					pxCanConfig,
					uxBasicCanRxMb,	/* Number of Basic CAN rx mailboxes */
					uxBasicCanTxMb	/* Number of Basid CAN tx mailboxes */
					) )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: CANmod3_Init failed in xCanInit!\r\n" );
		goto ERR_FREE_BUFFER_LOCK;
	}

	pxCanHandle->xTxEventH = NULL;
	pxCanHandle->xTxEventL = NULL;
	pxCanHandle->xErrEvent = NULL;

	s_pxCanHandle[ ucPortNum ] = pxCanHandle;

	CANmod3_RegisterRxCallback( ucPortNum, prvCanRxCallBack );
	CANmod3_RegisterTxCallback( ucPortNum, prvCanTxCallBack );
	CANmod3_RegisterErrCallback( ucPortNum, prvCanErrCallBack );

	return CAN_OK;

ERR_FREE_BUFFER_LOCK:
	vSemaphoreDelete( pxCanHandle->xRxBufferLock );

ERR_FREE_HANDLE:
	vPortFree( pxCanHandle );

	return CAN_ERR;
}

INT32 xCanDeinit( UINT8 ucPort )
{
	UINT8 uxN;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	if( !pxCanHandle )
	{
		/* Can't do much without this */
		DebugZone( ZONE_CAN_ERROR, "ERR: CAN_Deinit can't find xCanDeinit!\r\n" );
		return CAN_ERR;
	}

	CANmod3_UnRegisterRxCallback( ucPort  );

	CANmod3_UnRegisterErrCallback( ucPort  );

	if( pxCanHandle->xRxBufferLock != NULL )
		vSemaphoreDelete( pxCanHandle->xRxBufferLock );


	/* Delete receive queue */
	for( uxN = 0; uxN < CANmod3_RX_MAILBOX; uxN++ )
	{
		if( NULL != pxCanHandle->xRxChannels[ uxN ] )
		{
			vQueueDelete( pxCanHandle->xRxChannels[ uxN ] );
			pxCanHandle->xRxChannels[ uxN ] = NULL;
		}
	}

	if( pxCanHandle->xTxEventH != NULL )
	{
		vPortFree( pxCanHandle->xTxEventH );
	}

	if( pxCanHandle->xTxEventL != NULL )
	{
		vPortFree( pxCanHandle->xTxEventL );
	}

	if( pxCanHandle->xErrEvent != NULL )
	{
		vPortFree( pxCanHandle->xErrEvent );
	}

	if( pxCanHandle->xVirtCanQueue != NULL )
	{
		vPortFree( pxCanHandle->xVirtCanQueue );
	}

	vPortFree( pxCanHandle );

	s_pxCanHandle[ ucPort ] = NULL;

	return CAN_OK;
}

INT32 xCanStart( UINT8 ucPort, UINT32 uxWorkMode )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	taskENTER_CRITICAL();

	CANmod3_Mode( pxCanHandle->pxCanMod3Instance, uxWorkMode );

	CANmod3_ClearIntStatus( pxCanHandle->pxCanMod3Instance, CANMOD3_INT_MASK );

	CANmod3_SetIntEbl( pxCanHandle->pxCanMod3Instance, CANMOD3_INT_ARB_LOSS | CANMOD3_INT_OVR_LOAD | CANMOD3_INT_BIT_ERR
					| CANMOD3_INT_STUFF_ERR | CANMOD3_INT_ACK_ERR | CANMOD3_INT_FORM_ERR
					| CANMOD3_INT_CRC_ERR | CANMOD3_INT_BUS_OFF | CANMOD3_INT_RX_MSG_LOST
					| CANMOD3_INT_TX_MSG  | CANMOD3_INT_RX_MSG | CANMOD3_INT_RTR_MSG
					| CANMOD3_INT_STUCK_AT_0 | CANMOD3_INT_SST_FAILURE );

	CANmod3_Start( pxCanHandle->pxCanMod3Instance );

	taskEXIT_CRITICAL();

	return CAN_OK;
}


INT32 xCanStop( UINT8 ucPort )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	taskENTER_CRITICAL();

	CANmod3_SetIntEbl( pxCanHandle->pxCanMod3Instance, 0 );
	CANmod3_ClearIntStatus( pxCanHandle->pxCanMod3Instance, CANMOD3_INT_MASK );
	CANmod3_Stop( pxCanHandle->pxCanMod3Instance );

	taskEXIT_CRITICAL();

	return CAN_OK;
}

INT32 xCanReConfigCoreInRunning( UINT8 ucPort, UINT32 uxCfg )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	taskENTER_CRITICAL();

	CANmod3_SetConfigReg( pxCanHandle->pxCanMod3Instance, uxCfg );

	taskEXIT_CRITICAL();

	return CAN_OK;
}

QueueHandle_t xCanReceiveQueueCreate( UINT8 ucPort, PCAN_FILTEROBJECT pxFilter,
			UINT8 ucN,	/* the index of message object buffers*/
			portBASE_TYPE uxQueueLength )
{
	QueueHandle_t xTheQ;
	UINT8 ucMob;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	configASSERT(ucN < CANmod3_RX_MAILBOX);

	xSemaphoreTake( pxCanHandle->xRxBufferLock, portMAX_DELAY);

	if( pxCanHandle->xRxChannels[ ucN ] != NULL )
	{
		xSemaphoreGive( pxCanHandle->xRxBufferLock );
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer have been used!\r\n" );
		return NULL;
	}

	xTheQ = xQueueCreate( uxQueueLength, sizeof( CAN_MSGOBJECT ) );
	if( xTheQ == NULL )
	{
		xSemaphoreGive( pxCanHandle->xRxBufferLock );
		return NULL;
	}

	if( ucN < ( CANmod3_RX_MAILBOX -  pxCanHandle->pxCanMod3Instance->basic_can_rx_mb ) )
	{
		pxCanHandle->xRxChannels[ ucN ] = xTheQ;
	}
	else
	{
		for ( ucMob = CANmod3_RX_MAILBOX -  pxCanHandle->pxCanMod3Instance->basic_can_rx_mb; ucMob < CANmod3_RX_MAILBOX; ucMob++ )
		{
			pxCanHandle->xRxChannels[ ucMob ] = xTheQ;
		}
	}
	xSemaphoreGive( pxCanHandle->xRxBufferLock );

	taskENTER_CRITICAL();
	/* full can */
	if( ucN < CANmod3_RX_MAILBOX -  pxCanHandle->pxCanMod3Instance->basic_can_rx_mb )
	{

		CANmod3_RXMSGOBJECT xMod3RxMsg;
		xMod3RxMsg.id = 0;			/* Transfer the ID.*/
		xMod3RxMsg.dataLow = 0;			/* lower data */
		xMod3RxMsg.dataHigh = 0;			/* higher data */
		xMod3RxMsg.acr.l = pxFilter->acr.l;
		xMod3RxMsg.amr.l = pxFilter->amr.l;
		xMod3RxMsg.amr_d = pxFilter->amcr_d.mask;
		xMod3RxMsg.acr_d = pxFilter->amcr_d.code;
		xMod3RxMsg.rxb.l = CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;

		if( CANmod3_OK != CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg ) )
		{
			DebugZone( ZONE_CAN_ERROR, "ERR: CANmod3_ConfigBufferN() failed in xCanReceiveQueueCreate()!\r\n" );
			vQueueDelete( pxCanHandle->xRxChannels[ ucN ] );
			pxCanHandle->xRxChannels[ ucN ] = NULL;
			xTheQ = NULL;
		}
	}
	else
	{	/* basic can */
		if( CANmod3_OK != CANmod3_ConfigBuffer( pxCanHandle->pxCanMod3Instance, pxFilter, CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL ) )
		{
			DebugZone( ZONE_CAN_ERROR, "ERR: CANmod3_ConfigBuffer() failed in xCanReceiveQueueCreate()!\r\n" );

			for ( ucMob = CANmod3_RX_MAILBOX -  pxCanHandle->pxCanMod3Instance->basic_can_rx_mb; ucMob < CANmod3_RX_MAILBOX; ucMob++ )
			{
				pxCanHandle->xRxChannels[ ucMob ] = NULL;
			}

			xTheQ = NULL;
		}
	}

	taskEXIT_CRITICAL();

	return xTheQ;
}


INT32 xCanReceiveQueueReConfig( UINT8 ucPort, PCAN_FILTEROBJECT pxFilter, UINT8 ucN )
{
	CANmod3_RXMSGOBJECT xMod3RxMsg;
	UINT8 ucRetv = CANmod3_ERR;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	configASSERT( ucN < CANmod3_RX_MAILBOX );

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer have not beed used!\r\n" );
		return ucRetv;
	}

	taskENTER_CRITICAL();

	/* full can */
	if( ucN < CANmod3_RX_MAILBOX -  pxCanHandle->pxCanMod3Instance->basic_can_rx_mb )
	{
		xMod3RxMsg.id = 0;			/* Transfer the ID.*/
		xMod3RxMsg.dataLow = 0;     	 	/* lower data */
		xMod3RxMsg.dataHigh = 0;   		/* higher data */
		xMod3RxMsg.acr.l = pxFilter->acr.l;
		xMod3RxMsg.amr.l = pxFilter->amr.l;
		xMod3RxMsg.amr_d = pxFilter->amcr_d.mask;
		xMod3RxMsg.acr_d = pxFilter->amcr_d.code;
		xMod3RxMsg.rxb.l = CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;

		ucRetv = CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg);
	}
	else
	{
		ucRetv = CANmod3_ConfigBuffer( pxCanHandle->pxCanMod3Instance ,
				pxFilter, CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL );
	}

	taskEXIT_CRITICAL();

	return ucRetv;
}

INT32 xCanStopReceiveQueue( UINT8 ucPort, UINT8 ucN )
{
	CANmod3_RXMSGOBJECT xMod3RxMsg;
	CANmod3_FILTEROBJECT xMod3Filter;
	portCHAR cRetv = CANmod3_ERR;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	configASSERT( ucN < CANmod3_RX_MAILBOX );

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer have not beed used!\r\n" );
		return cRetv;
	}

	taskENTER_CRITICAL();

	/* full can */
	if( ucN < CANmod3_RX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_rx_mb )
	{
		xMod3RxMsg.rxb.l = 0;
		cRetv = CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg );
	}
	else
	{
		cRetv = CANmod3_ConfigBuffer( pxCanHandle->pxCanMod3Instance, &xMod3Filter, 0 );
	}

	taskEXIT_CRITICAL();

	return cRetv;
}

INT32 xCanDeleteReceiveQueue( UINT8 ucPort, UINT8 ucN )
{
	UINT8 ucMob;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	configASSERT( ucN < CANmod3_RX_MAILBOX );

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		return CAN_INVALID_MAILBOX;
	}

	taskENTER_CRITICAL();

	/* full can */
	if( ucN < CANmod3_RX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_rx_mb)
	{
		vQueueDelete( pxCanHandle->xRxChannels[ ucN ] );
		pxCanHandle->xRxChannels[ ucN ] = NULL;
	}
	else
	{
		vQueueDelete( pxCanHandle->xRxChannels[ ucN ] );
		for( ucMob = CANmod3_RX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_rx_mb;
			ucMob < CANmod3_RX_MAILBOX; ucMob++ )
		{
			pxCanHandle->xRxChannels[ ucMob ] = NULL;
		}
	}

	taskEXIT_CRITICAL();
	return CAN_OK;
}

QueueHandle_t xGetVirtCanReceiveQueue( UINT8 ucPort )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	/* dynamic create on get */
	if( NULL == pxCanHandle->xVirtCanQueue)
	{
		pxCanHandle->xVirtCanQueue = xQueueCreate( CANBUS_DUMP_QUEUE_LENGH, sizeof( CAN_MSGOBJECT ));
		if( pxCanHandle->xVirtCanQueue == NULL )
		{
			DebugZone( ZONE_CAN_ERROR, "ERR: xVirtCanQueue create failed in xGetVirtCanReceiveQueue!\r\n" );
		}
	}

	return pxCanHandle->xVirtCanQueue;
}

EventGroupHandle_t xGetCanTransmitEventGroup( UINT8 ucPort, UINT8 ucHigh )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	if(ucHigh)
	{
		if( pxCanHandle->xTxEventH == NULL )
		{
			pxCanHandle->xTxEventH = xEventGroupCreate();
			if( pxCanHandle->xTxEventH == NULL )
			{
				DebugZone( ZONE_CAN_ERROR, "ERR: xTxEventH create failed in xGetCanTransmitEventGroup!\r\n" );
			}
		}

		return pxCanHandle->xTxEventH;
	}

	if( pxCanHandle->xTxEventL == NULL )
	{
		pxCanHandle->xTxEventL = xEventGroupCreate();
		if( pxCanHandle->xTxEventL == NULL )
		{
			DebugZone( ZONE_CAN_ERROR, "ERR: xTxEventL create failed in xGetCanTransmitEventGroup!\r\n" );
		}
	}

	return pxCanHandle->xTxEventL;
}

EventGroupHandle_t xGetCanErrorEventGroup( UINT8 ucPort )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];
	if( pxCanHandle->xErrEvent == NULL )
	{
		pxCanHandle->xErrEvent = xEventGroupCreate();
		if( pxCanHandle->xErrEvent == NULL )
		{
			DebugZone( ZONE_CAN_ERROR, "ERR: xErrEvent create failed in xGetCanErrorEventGroup!\r\n" );
		}
	}
	return pxCanHandle->xErrEvent;
}


INT32 xCanSend( UINT8 ucPort, PCAN_MSGOBJECT pxMsg, UINT8 ucN, UINT32 uxBsst ) /* Single Shot Transmission */
{
	portCHAR cRetv = CAN_OK;
	CANmod3_MSGOBJECT xMod3Msg;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];


	configASSERT( ucN < CANmod3_RX_MAILBOX );

	/* id is left aligned */
	if ( pxMsg->ide )
		xMod3Msg.id = pxMsg->id;
	else
		xMod3Msg.id = pxMsg->id << 18;

	xMod3Msg.dataHigh = pxMsg->dataHigh;
	xMod3Msg.dataLow = pxMsg->dataLow;
	if( pxMsg->dlc > CANBUS_DLC_MAX )
		xMod3Msg.dlc = CANBUS_DLC_MAX;
	else
		xMod3Msg.dlc = pxMsg->dlc;
	xMod3Msg.ide = pxMsg->ide;
	xMod3Msg.rtr = pxMsg->rtr;

	#ifdef ENABLE_CAN_ERROR_TX_ABORT_TEST
	g_last_tx_msg_box[ucPort] = ucN;
	#endif

	/* full can */
	if( ucN < CANmod3_TX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_tx_mb)
	{
		if( CANmod3_OK != ( cRetv = CANmod3_TxMaiboxNReady( pxCanHandle->pxCanMod3Instance, ucN ) ) )
			return cRetv;

		if( CANmod3_OK != ( cRetv = CANmod3_SendMessageN( pxCanHandle->pxCanMod3Instance, ucN, uxBsst, &xMod3Msg ) ) )
			return cRetv;
	}
	else	/* basic can */
	{
		if( CANmod3_OK != ( cRetv = CANmod3_SendMessageReady( pxCanHandle->pxCanMod3Instance ) ) )
			return cRetv;

		if( CANmod3_OK != ( cRetv = CANmod3_SendMessage( pxCanHandle->pxCanMod3Instance, &xMod3Msg ) ) )
			return cRetv;
	}

	#ifdef CONFIG_ENABLE_CANBUS_DEMO
	if( pxCanHandle->xVirtCanQueue != NULL )
	{
		if( errQUEUE_FULL == xQueueSend( pxCanHandle->xVirtCanQueue, pxMsg, 0 ) )
			DebugZone( cCanZoneQueueFullError, "ERR: CAN(%d) queue is full for dump!\r\n", ucPort );
	}
	#endif

	return  cRetv;
}

INT32 xCanAbortTxBufferN( UINT8 ucPort, UINT8 ucN )
{
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	return CANmod3_SendMessageAbortN( pxCanHandle->pxCanMod3Instance, ucN );
}


INT32 xCanRTRMessageNCreate( UINT8 ucPort, PCAN_MSGOBJECT pxMsg,
				PCAN_FILTEROBJECT pxFilter, UINT8 ucN )
{
	portCHAR cRetv = CANmod3_ERR;
	CANmod3_RXMSGOBJECT xMod3RxMsg;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];


	if( ucN >= CANmod3_RX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_rx_mb )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer is not full can buffer!\r\n" );
		return cRetv;
	}

	xSemaphoreTake( pxCanHandle->xRxBufferLock, portMAX_DELAY );
	if( pxCanHandle->xRxChannels[ ucN ] != NULL )
	{
		xSemaphoreGive( pxCanHandle->xRxBufferLock );
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer is being used!\r\n" );
		return cRetv;
	}

	pxCanHandle->xRxChannels[ ucN ] = (void *)0xffff;	/* this ucMob is in use now! */
	xSemaphoreGive( pxCanHandle->xRxBufferLock );

	/* id is left aligned*/
	if (pxMsg->ide)
		xMod3RxMsg.id = pxMsg->id;		/* Transfer the ID.*/
	else
		xMod3RxMsg.id = pxMsg->id << 18;

	xMod3RxMsg.dataLow = pxMsg->dataLow;
	xMod3RxMsg.dataHigh = pxMsg->dataHigh;
	xMod3RxMsg.rxb.l = 0;
	xMod3RxMsg.rxb.ide = pxMsg->ide;
	if( pxMsg->dlc > CANBUS_DLC_MAX )
		xMod3RxMsg.rxb.dlc = CANBUS_DLC_MAX;
	else
		xMod3RxMsg.rxb.dlc = pxMsg->dlc;
	xMod3RxMsg.acr.l = pxFilter->acr.l;
	xMod3RxMsg.amr.l = pxFilter->amr.l;
	xMod3RxMsg.amr_d = pxFilter->amcr_d.mask;
	xMod3RxMsg.acr_d = pxFilter->amcr_d.code;

	xMod3RxMsg.rxb.l |= CANMOD3_RX_RTR_REPLY_EBL | CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;

	taskENTER_CRITICAL();

	if( CANmod3_OK != ( cRetv = CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg ) ) )
	{
		pxCanHandle->xRxChannels[ ucN ] = NULL; /* this ucMob is not use now!*/
	}

	taskEXIT_CRITICAL();

	return cRetv;
}

INT32 xCanSetRTRMessageN( UINT8 ucPort, PCAN_MSGOBJECT pxMsg, UINT8 ucN)
{
	portCHAR cRetv = CANmod3_ERR;
	CANmod3_MSGOBJECT xMod3Msg;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer is not be used!\r\n" );
		return cRetv;
	}

	/* id is left aligned */
	if ( pxMsg->ide )
		xMod3Msg.id = pxMsg->id;
	else
		xMod3Msg.id = pxMsg->id << 18;

	xMod3Msg.dataHigh = pxMsg->dataHigh;
	xMod3Msg.dataLow = pxMsg->dataLow;

	taskENTER_CRITICAL();
	if( CANmod3_OK != ( cRetv = CANmod3_SetRTRMessageN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3Msg ) ) )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: Set RTR Message failed!\r\n" );
	}
	taskEXIT_CRITICAL();

	return cRetv;
}

INT32 xCanRTRMessageAbortN( UINT8 ucPort, UINT8 ucN )
{
	portCHAR cRetv = CANmod3_ERR;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer is not be used!\r\n" );
		return cRetv;
	}

	taskENTER_CRITICAL();

	if(CANmod3_OK != ( cRetv = CANmod3_RTRMessageAbortN( pxCanHandle->pxCanMod3Instance, ucN ) ) )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: RTR Message abort failed!\r\n" );
	}

	taskEXIT_CRITICAL();

	return cRetv;
}

INT32 xCanRTRMessageNDelete( UINT8 ucPort, UINT8 ucN )
{
	portCHAR cRetv = CANmod3_ERR;
	CANmod3_RXMSGOBJECT xMod3RxMsg;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	if( pxCanHandle->xRxChannels[ ucN ] == NULL )
	{
		DebugZone( ZONE_CAN_ERROR, "ERR: This buffer is not be used!\r\n" );
		return cRetv;
	}

	xMod3RxMsg.rxb.l = 0;

	taskENTER_CRITICAL();

	if( CANmod3_OK == ( cRetv = CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg ) ) )
	{
		pxCanHandle->xRxChannels[ ucN ] = NULL; /* this ucMob is not use now!*/
	}

	taskEXIT_CRITICAL();

	return cRetv;
}

/*
* only for arbitration test
*/
INT32 xCanArbTestFunction( PCAN_MSGOBJECT pxMsg0, PCAN_MSGOBJECT pxMsg1,
				UINT32 ucN, UINT32 uxBsst )
{
	portCHAR cRetv = CANmod3_ERR;
	CANmod3_MSGOBJECT xMod3Msg0, xMod3Msg1;
	PCAN_HANDLE pxCanHandle0 = s_pxCanHandle[ CAN_PORT0 ];
	PCAN_HANDLE pxCanHandle1 = s_pxCanHandle[ CAN_PORT1 ];

	if (pxMsg0->ide)
		xMod3Msg0.id = pxMsg0->id;
	else
		xMod3Msg0.id = pxMsg0->id << 18;

	xMod3Msg0.dataHigh = pxMsg0->dataHigh;
	xMod3Msg0.dataLow = pxMsg0->dataLow;
	xMod3Msg0.l = 0;
	xMod3Msg0.dlc = pxMsg0->dlc;
	xMod3Msg0.ide = pxMsg0->ide;
	xMod3Msg0.rtr = pxMsg0->rtr;

	if ( pxMsg1->ide )
		xMod3Msg1.id = pxMsg1->id;
	else
		xMod3Msg1.id = pxMsg1->id << 18;

	xMod3Msg1.dataHigh = pxMsg1->dataHigh;
	xMod3Msg1.dataLow = pxMsg1->dataLow;
	xMod3Msg1.l = 0;
	xMod3Msg1.dlc = pxMsg1->dlc;
	xMod3Msg1.ide = pxMsg1->ide;
	xMod3Msg1.rtr = pxMsg1->rtr;

	#ifdef ENABLE_CAN_ERROR_TX_ABORT_TEST
	g_last_tx_msg_box[CAN_PORT0] = ucN;
	g_last_tx_msg_box[CAN_PORT1] = ucN;
	#endif

	if( CANmod3_OK != ( cRetv = CANmod3_TxMaiboxNReady( pxCanHandle0->pxCanMod3Instance, ucN ) ) )
		return cRetv;
	pxCanHandle0->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].id = xMod3Msg0.id;
	pxCanHandle0->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].dataLow = xMod3Msg0.dataLow;
	pxCanHandle0->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].dataHigh = xMod3Msg0.dataHigh;

	if( CANmod3_OK != ( cRetv = CANmod3_TxMaiboxNReady( pxCanHandle1->pxCanMod3Instance, ucN ) ) )
		return cRetv;
	pxCanHandle1->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].id = xMod3Msg1.id;
	pxCanHandle1->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].dataLow = xMod3Msg1.dataLow;
	pxCanHandle1->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].dataHigh = xMod3Msg1.dataHigh;
	if( CAN_SEND_SST == uxBsst )
	{
		pxCanHandle0->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].txb.l =
			xMod3Msg0.l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ | CANMOD3_TX_ABORT_REQ;
		pxCanHandle1->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].txb.l =
			xMod3Msg1.l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ | CANMOD3_TX_ABORT_REQ;
	}
	else
	{
		pxCanHandle0->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].txb.l =
			xMod3Msg0.l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ;
		pxCanHandle1->pxCanMod3Instance->hw_reg->TxMsg[ ucN ].txb.l =
			xMod3Msg1.l | CANMOD3_TX_WPNH_EBL | CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ;
	}
	return cRetv;
}

INT32 xCanSuspend( UINT8 ucPort, PCAN_FILTEROBJECT pxFilter, UINT8 ucN )
{
	CANmod3_RXMSGOBJECT xMod3RxMsg;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	xMod3RxMsg.id = 0;
	xMod3RxMsg.dataLow = 0;
	xMod3RxMsg.dataHigh = 0;
	xMod3RxMsg.acr.l = pxFilter->acr.l;
	xMod3RxMsg.amr.l = pxFilter->amr.l;
	xMod3RxMsg.amr_d = pxFilter->amcr_d.mask;
	xMod3RxMsg.acr_d = pxFilter->amcr_d.code;
	xMod3RxMsg.rxb.l = CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;

	CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg );
	/*
	vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_0, 0 );
	vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_2, 0 );
	set_pwrc_register(PWRC_OFFSET_TRIGGER_EN_SET, (0x8<<6) | (0x1<<10) );
	set_pwrc_register(PWRC_OFFSET_RTC_CAN_CTRL, 0x1 | (0x1<<22) | (0x1<<26)| (0x1<<27));
	*/
	return CAN_OK;
}

/*
* only for sleep/wakeup test
*/
void __on_retain vCanRetainSend( UINT32 canid, UINT32 dataH, UINT32 dataL )
{
	SOC_REG( A7DA_CANBUS0_TX_BUF0_IDE ) = (canid<<21);
	SOC_REG( A7DA_CANBUS0_TX_BUF0_DH ) = dataH;
	SOC_REG( A7DA_CANBUS0_TX_BUF0_DL ) = dataL;
	SOC_REG( A7DA_CANBUS0_TX_BUF0_CTRL ) = ( 8 << 16 ) | CANMOD3_TX_WPNH_EBL |
				CANMOD3_TX_WPNL_EBL | CANMOD3_TX_REQ;
}

UINT32 __on_retain xCanResume( void )
{
	UINT32 uxRxCtl;
	UINT32 uxRxId;
	UINT8  ucMode;
	UINT32 uxPwrcCanStatus;

	if(FALSE==ENABLE_M3_CAN1_WAKUP())
	{
		SOC_REG(A7DA_IOCRTC_FUNC_SEL_0_REG_CLR) = 0x7 << 8;	/* RTC_GPIO_2*/
	}

	vRetainGpioSetDirectionOutputUnsafe( eGpioRtcm, GPIO_RTC_0, 1 );
	if(FALSE==ENABLE_M3_CAN1_WAKUP())
	{
		vRetainGpioSetDirectionOutputUnsafe( eGpioRtcm, GPIO_RTC_2, 1 );
	}

	uxPwrcCanStatus = uxIoBridgeRead( A7DA_PWRC_RTC_CAN_CTRL );
	if( PWRC_CAN_PRE_WAKEUP_MODE_M3PD & uxPwrcCanStatus )
		return PM_WAKEUP_CANBUS_MODE_WAKEUP_A7;

	/* Need clear the canbus interrupt status*/
	SOC_REG( A7DA_CANBUS0_INT_STATUS ) = CANMOD3_INT_MASK;

	uxRxCtl = SOC_REG( A7DA_CANBUS0_RX_BUF0_CTRL );
	if( uxRxCtl & A7DA_CANBUS_RX_CTRL_IDE )
		uxRxId = SOC_REG( A7DA_CANBUS0_RX_BUF0_IDE ) >> 3;
	else
		uxRxId = SOC_REG( A7DA_CANBUS0_RX_BUF0_IDE ) >> 21;

	SOC_REG( A7DA_CANBUS0_RX_BUF0_CTRL ) = A7DA_CANBUS_RX_CTRL_MSGAV;
	switch (uxRxId)
	{
	case CANBUS_ID_WAKEUP_M3:
		ucMode = PM_WAKEUP_CANBUS_MODE_WAKEUP_M3;
		break;
	case CANBUS_ID_WAKEUP_A7:
		ucMode = PM_WAKEUP_CANBUS_MODE_WAKEUP_A7;
		break;
	case CANBUS_ID_REARVIEW_START:
		ucMode = PM_WAKEUP_CANBUS_MODE_TRIGGER_REARVIEW;
		MEM( PM_ARG_CAN_TRIGGER_REARVIEW ) = CANBUS_TRIGGER_REARVIEW_MAGIC;
		vDcacheFlush();
		break;
	default:
		ucMode = PM_WAKEUP_CANBUS_MODE_GOTO_SLEEP;
	}
	return ucMode;
}


static void prvCanbusConfigBuffers( UINT8 ucPort, UINT8 ucN )
{
	CAN_FILTEROBJECT xFilter;
	PCAN_HANDLE pxCanHandle = NULL;

	configASSERT( ucPort <= CAN_PORT1 );
	pxCanHandle = s_pxCanHandle[ ucPort ];

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	if( ucN < CANmod3_RX_MAILBOX - pxCanHandle->pxCanMod3Instance->basic_can_rx_mb )
	{

		CANmod3_RXMSGOBJECT xMod3RxMsg;
		xMod3RxMsg.id = 0;			/* Transfer the ID.*/
		xMod3RxMsg.dataLow = 0;			/* lower data */
		xMod3RxMsg.dataHigh = 0;		/* higher data */
		xMod3RxMsg.acr.l = xFilter.acr.l;
		xMod3RxMsg.amr.l = xFilter.amr.l;
		xMod3RxMsg.amr_d = xFilter.amcr_d.mask;
		xMod3RxMsg.acr_d = xFilter.amcr_d.code;
		xMod3RxMsg.rxb.l = CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL;

		if( CANmod3_OK != CANmod3_ConfigBufferN( pxCanHandle->pxCanMod3Instance, ucN, &xMod3RxMsg ) )
		{
			DebugMsg( "ERR: CANmod3_ConfigBufferN() failed in xCanReceiveQueueCreate()!\r\n" );
		}
	}
	else
	{	/* basic can */
		if( CANmod3_OK != CANmod3_ConfigBuffer( pxCanHandle->pxCanMod3Instance,
					&xFilter, CANMOD3_RX_BUFFER_EBL | CANMOD3_RX_INT_EBL ) )
		{
			DebugMsg( "ERR: CANmod3_ConfigBuffer() failed in xCanReceiveQueueCreate()!\r\n" );
		}
	}
}

void vCanbusInitialize( UINT8 ucPort, UINT32 uxCanClk )
{
	configASSERT( ucPort <= CAN_PORT1 );

	if( CAN_OK != xCanInit( ucPort, uxCanClk | CAN_AUTO_RESTART | CAN_LITTLE_ENDIAN,
		( PCAN_CONFIG_REG )0, 0, 0 ) )
		return;

	xCanStart( ucPort, CANOP_MODE_NORMAL );

	prvCanbusConfigBuffers( ucPort, 0 );
}
