/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __ATLAS7_STEP_B__
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"
#include "errno.h"

#include "utils.h"
#include "io.h"
#include "soc_clkc.h"
#include "soc_timer.h"
#include "soc_uart.h"
#include "soc_pwrc_retain.h"

#define A7DA_DEV_ID_B0 0xB0
#define A7DA_DEV_ID_B1 0xB1
#define A7DA_DEV_ID_B2 0xB2
#define A7DA_DEV_ID_B3 0xB3

#define A7DA_INT_DEV_ID 0x10220048

#define A7DA_UART1_CLK_DIV 0x18020050

#define request_hwlock()        REQUEST_HWLOCK(0)
#define release_hwlock()        RELEASE_HWLOCK(0)


#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/*
 * Generic virtual read/write.  Note that we don't support half-word
 * read/writes.  We define __arch_*[bl] here, and leave __arch_*w
 * to the architecture specific code.
 */
#define __arch_getl(a)          SOC_REG(((unsigned long)a))
#define __arch_putl(v,a)        SOC_REG(((unsigned long)a)) = v
/*
 * TODO: The kernel offers some more advanced versions of barriers, it might
 * have some advantages to use them instead of the simple one here.
 */
#define dmb()       __asm__ __volatile__ ("" : : : "memory")
#define __iormb()   dmb()
#define __iowmb()   dmb()

#define writel(v,c) ({ u32 __v = v; __iowmb(); __arch_putl(__v,c); __v; })
#define readl(c)    ({ u32 __v = __arch_getl(c); __iormb(); __v; })

#define modify_reg(a, and_value, or_value) { \
	writel((readl(a) & and_value) | or_value, a); \
}

#define __udelay vUsWait

#define poll_reg(a, m, v, l, e) { \
	unsigned int wd = l; \
	while ((readl(a)&m) != (v&m)) { \
		if (--wd == 0) { \
			return ; \
		} \
	} \
}

struct PLLREG {
	u32 r_freq;
	u32 r_ssc;
	u32 r_ctrl0;
	u32 r_ctrl1;
	u32 r_st;

};

struct PLLVAL {
	u32 reg_base;
	u32 divf;
	u32 divr;
	u32 sse;
	u32 ssdiv;
	u32 ssmod;
	u32 ssdepth;
	u32 divq1;
	u32 divq2;
	u32 divq3;
	u32 en;
};

#define PLL_AB_CTRL0_SSE			(1 << 12)
#define PLL_AB_CTRL1_DIVQ1_MASK		0x7
#define PLL_AB_CTRL0_RSB			(1 << 0)
#define PLL_AB_CTRL0_BYPASS			(1 << 4)

#define SYS0_SFT 0
#define SYS1_SFT 8
#define SYS2_SFT 16
#define SYS3_SFT 24

#define S2_20_SFT 0
#define S1_20_SFT 8
#define S1_19_SFT 16
#define S1_18_SFT 24
#define S0_20_SFT 0
#define S1_17_SFT 8

#define SYS0_EN BIT(0)
#define SYS1_EN BIT(4)
#define SYS2_EN BIT(8)
#define SYS3_EN BIT(12)

#define SEL_XIN     0
#define SEL_XINiW   1
#define SEL_SYS0    2
#define SEL_SYS1    3
#define SEL_SYS2    4
#define SEL_SYS3    5

#define SEL_SYS2_D20    2   /* PRE-SEL 2 */
#define SEL_SYS1_D20    3   /* PRE-SEL 3 */
#define SEL_SYS1_D19    4   /* PRE-SEL 4 */
#define SEL_SYS1_D18    5   /* PRE-SEL 5 */
#define SEL_SYS0_D20    6   /* PRE-SEL 6 */
#define SEL_SYS1_D17    7   /* PRE-SEL 7 */


const  struct PLLVAL g_pll_tbl[] = {
	/* base,divf,divr,sse,ssdiv,ssmod,ssdepth,divq1,divq2,divq3,en */
	//{A7DA_CLKC_CPUPLL_AB_FREQ, 60, 0, 0, 0, 0, 0, 2, 0, 0, 0x7},
	{A7DA_CLKC_SYS0_BTPLL_AB_FREQ, 39, 0, 0, 0, 0, 0, 0, 0, 0, 0x3},
	{A7DA_CLKC_SYS1_USBPLL_AB_FREQ, 45, 0, 1, 709, 249, 0, 0, 0, 0, 0x3},
	{A7DA_CLKC_SYS2_ETHPLL_AB_FREQ, 124, 1, 0, 0, 0, 0, 0, 0, 0, 0x0},
	{A7DA_CLKC_SYS3_SSCPLL_AB_FREQ, 116, 3, 1, 176, 105, 3, 0, 0, 0, 0x3},
};

void pll_init(void)
{
	int i;
	struct PLLREG *reg;
	const struct PLLVAL *p_plltbl;


	p_plltbl = g_pll_tbl;

	for (i = 0; i < ARRAY_SIZE(g_pll_tbl); i++) {
		if (p_plltbl[i].reg_base == 0)
			break;
		reg = (struct PLLREG *)p_plltbl[i].reg_base;
		/* set freq reg */
		writel((p_plltbl[i].divf & 0x1ff) |
		       ((p_plltbl[i].divr & 0x7) << 16), &reg->r_freq);
		/* set SS mode */
		if (p_plltbl[i].sse != 0)
			writel(p_plltbl[i].ssdepth << 20 |
		  p_plltbl[i].ssdiv << 8 | p_plltbl[i].ssmod, &reg->r_ssc);
		modify_reg(&reg->r_ctrl0,
			   ~(PLL_AB_CTRL0_SSE), p_plltbl[i].sse << 12);
		/* set DIVQ for pll out 1/2/3 */
		if (p_plltbl[i].divq1 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 0),
				   p_plltbl[i].divq1 << 0);
		if (p_plltbl[i].divq2 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 4),
				   p_plltbl[i].divq2 << 4);
		if (p_plltbl[i].divq3 != 0)
			modify_reg(&reg->r_ctrl1, ~(0x7 << 8),
				   p_plltbl[i].divq3 << 8);
		/* set pll outx en */
		modify_reg(&reg->r_ctrl1, ~(0x7 << 12), p_plltbl[i].en << 12);

		/* clear by-pass bit */
		modify_reg(&reg->r_ctrl0, ~(PLL_AB_CTRL0_BYPASS), 0);

		/* release reset */
		modify_reg(&reg->r_ctrl0, ~(0), PLL_AB_CTRL0_RSB);
	}

	for (i = 0; i < ARRAY_SIZE(g_pll_tbl); i++) {
		/* wait all plls to get locked */
		poll_reg(p_plltbl[i].reg_base + 0x10, 0x1, 0x1, 100000, -1);
	}
}


static void clk_src_init(void)
{
	/* cpu clock sel */
	//writel(4, A7DA_CLKC_CPU_CLK_SEL);

	/* usb phy clock sel */
	writel(49 << SYS1_SFT, A7DA_CLKC_USBPHY_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_USBPHY_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_USBPHY_CLK_SEL);
	writel((1<<1) | (1<<28), A7DA_CLKC_ROOT_CLK_EN1_SET);
	writel((0xf << 9), A7DA_CLKC_MISC1_LEAF_CLK_EN_SET);

	/* bt clock sel */
	writel(12 << SYS0_SFT, A7DA_CLKC_BTSS_CLK_CFG);
	writel(SYS0_EN, A7DA_CLKC_BTSS_CLK_DIVENA);
	writel(SEL_SYS0, A7DA_CLKC_BTSS_CLK_SEL);

	/* rgmii clock sel */
	writel(12 << SYS2_SFT, A7DA_CLKC_RGMII_CLK_CFG);
	writel(SYS2_EN, A7DA_CLKC_RGMII_CLK_DIVENA);
	writel(SEL_SYS2, A7DA_CLKC_RGMII_CLK_SEL);

	/* rgmii clock sel */
	writel(12 << SYS2_SFT, A7DA_CLKC_RGMII_CLK_CFG);
	writel(SYS2_EN, A7DA_CLKC_RGMII_CLK_DIVENA);
	writel(SEL_SYS2, A7DA_CLKC_RGMII_CLK_SEL);

	/* CAN clock sel */
	writel(24 << SYS1_SFT, A7DA_CLKC_CAN_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_CAN_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_CAN_CLK_SEL);

	/* deint clock sel */
	writel(8 << SYS2_SFT, A7DA_CLKC_DEINT_CLK_CFG);
	writel(SYS2_EN, A7DA_CLKC_DEINT_CLK_DIVENA);
	writel(SEL_SYS2, A7DA_CLKC_DEINT_CLK_SEL);

	/* NAND clock sel */
	writel(1 << SYS1_SFT, A7DA_CLKC_NAND_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_NAND_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_NAND_CLK_SEL);

	/* DISP0 clock sel */
	writel(3 << SYS1_SFT, A7DA_CLKC_DISP0_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_DISP0_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_DISP0_CLK_SEL);

	/* DISP1 clock sel */
	writel(3 << SYS1_SFT, A7DA_CLKC_DISP1_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_DISP1_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_DISP1_CLK_SEL);

	/* GPU clock sel */
	writel(3 << SYS1_SFT, A7DA_CLKC_GPU_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_GPU_CLK_DIVENA);
	writel(SEL_SYS1, A7DA_CLKC_GPU_CLK_SEL);

	/* GNSS clock sel: sys0 qa15 */
	writel(8 << SYS1_SFT, A7DA_CLKC_GNSS_CLK_CFG);
	writel(SYS1_EN, A7DA_CLKC_GNSS_CLK_DIVENA);
	poll_reg(A7DA_CLKC_GNSS_CLK_SEL_STATUS, 1, 1, 10000, -1);
	writel(SEL_SYS1, A7DA_CLKC_GNSS_CLK_SEL);
	poll_reg(A7DA_CLKC_GNSS_CLK_SEL_STATUS, 1, 1, 10000, -1);
	/* Pre-sel clock source */
	writel((15 << S2_20_SFT) | (7 << S1_20_SFT) | (5 << S1_19_SFT) |
		(4 << S1_18_SFT), A7DA_CLKC_SHARED_DIVIDERS_CFG0);
	writel((3 << S0_20_SFT) | (3 << S1_17_SFT), A7DA_CLKC_SHARED_DIVIDERS_CFG1);
	writel((1 << 0) | (1 << 4) | (1 << 8) | (1 << 12) |
		(1 << 16) | (1 << 20), A7DA_CLKC_SHARED_DIVIDERS_ENA);

	/* below modules use pre-sel clock */
	writel(4, A7DA_CLKC_VDEC_CLK_SEL);
	if(A7DA_DEV_ID_B3 == (SOC_REG(A7DA_INT_DEV_ID)&0xFF))
	{
		/* KAS 240 Mhz */
		writel(5, A7DA_CLKC_KAS_CLK_SEL);
	}
	else
	{	/* KAS 200 Mhz */
		writel(4, A7DA_CLKC_KAS_CLK_SEL);
	}
	writel(4, A7DA_CLKC_SDR_CLK_SEL);
	writel(4, A7DA_CLKC_NOCR_CLK_SEL);
	writel(4, A7DA_CLKC_NOCD_CLK_SEL);
	writel(5, A7DA_CLKC_TPIU_CLK_SEL);
	writel(6, A7DA_CLKC_SEC_CLK_SEL);
	writel(6, A7DA_CLKC_JPENC_CLK_SEL);
	writel(7, A7DA_CLKC_SYS_CLK_SEL);
	writel(7, A7DA_CLKC_G2D_CLK_SEL);
	writel(6, A7DA_CLKC_GMAC_CLK_SEL);
	writel(7, A7DA_CLKC_USB_CLK_SEL);
	writel(7, A7DA_CLKC_VIP_CLK_SEL);
	writel(7, A7DA_CLKC_HSI2S_CLK_SEL);
}

/* clk_src_late_init() will be called in 2nd stage */
void clk_src_late_init(void)
{
	request_hwlock();

	if(FALSE == PWRC_IS_CLOCK_LATE_INITIALIZED())
	{
		/* ioclk */
		writel(3, A7DA_CLKC_IO_CLK_SEL);

		SOC_REG(A7DA_UART1_CLK_DIV) = 0x000e0056;

		/* sdphy45 clock sel */
		writel(15 << SYS1_SFT, A7DA_CLKC_SDPHY45_CLK_CFG);
		writel(SYS1_EN, A7DA_CLKC_SDPHY45_CLK_DIVENA);
		writel(SEL_SYS1, A7DA_CLKC_SDPHY45_CLK_SEL);

		/* sdphy67 clock sel */
		writel(9 << SYS0_SFT, A7DA_CLKC_SDPHY67_CLK_CFG);
		writel(SYS0_EN, A7DA_CLKC_SDPHY67_CLK_DIVENA);
		writel(SEL_SYS0, A7DA_CLKC_SDPHY67_CLK_SEL);

		PWRC_SET_CLOCK_LATE_INITIALIZED();
	}

	release_hwlock();
}
/* fields for register CLKC_ROOT_CLK_EN0_SET */
#define A7DA_CLKC_ROOT_CLK_EN0_SET_AUDMSCM_KAS	BIT(0)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_GNSS		BIT(1)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_GPU		BIT(2)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_G2D		BIT(3)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_JPENC		BIT(4)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_DISP0		BIT(5)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_DISP1		BIT(6)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_I2S		BIT(8)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_AUDMSCM_IO	BIT(11)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_VDIFM_IO		BIT(12)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_GNSSM_IO		BIT(13)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_MEDIAM_IO		BIT(14)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_BTM_IO		BIT(17)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_SDPHY01		BIT(18)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_SDPHY23		BIT(19)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_SDPHY45		BIT(20)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_SDPHY67		BIT(21)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_AUDMSCM_XIN	BIT(22)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_NAND		BIT(27)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_GNSSM_RTCM_SEC	BIT(28)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_CPUM_CPU		BIT(29)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_GNSSM_XIN		BIT(30)
#define A7DA_CLKC_ROOT_CLK_EN0_SET_VIP		BIT(31)

#define A7DA_CLKC_MISC1_LEAF_SYS2PCI_IO_CLKEN	BIT(3)
#define A7DA_CLKC_MISC1_LEAF_PCIARB_IO_CLKEN		BIT(4)
#define A7DA_CLKC_MISC1_LEAF_SYS2PCI2_IO_CLKEN	BIT(7)

#define A7DA_CLKC_SDIO_LEAF_IO_CLKEN			BIT(0)
#define A7DA_CLKC_SDIO_LEAF_SDPHY_CLKEN			BIT(1)
#define A7DA_CLKC_DMAC0_LEAF_UART0_IO_CLKEN		BIT(1)

void  clock_init(void)
{
	/* wait for sure the A7 ROMcode ends the clock setting */
	while(SOC_REG(A7DA_CLKC_SYS_CLK_SEL) != 7);

	request_hwlock();

	if(FALSE == PWRC_IS_CLOCK_INITIALIZED())
	{
		pll_init();

		SOC_REG(A7DA_UART1_CLK_DIV) = 0x000e0056;

		clk_src_init();

		SOC_REG(A7DA_UART1_CLK_DIV) = 0x000e0056;

		/* init the necesary clocks used in uboot stg1 */
		writel(A7DA_CLKC_ROOT_CLK_EN0_SET_SDPHY01 |
			  A7DA_CLKC_ROOT_CLK_EN0_SET_MEDIAM_IO |
			  A7DA_CLKC_ROOT_CLK_EN0_SET_VDIFM_IO, A7DA_CLKC_ROOT_CLK_EN0_SET);
		writel(A7DA_CLKC_MISC1_LEAF_SYS2PCI_IO_CLKEN |
			   A7DA_CLKC_MISC1_LEAF_PCIARB_IO_CLKEN |
			   A7DA_CLKC_MISC1_LEAF_SYS2PCI2_IO_CLKEN,
			   A7DA_CLKC_MISC1_LEAF_CLK_EN_SET);
		writel(A7DA_CLKC_SDIO_LEAF_IO_CLKEN |
			   A7DA_CLKC_SDIO_LEAF_SDPHY_CLKEN,
			   A7DA_CLKC_SDIO01_LEAF_CLK_EN_SET);
		writel(A7DA_CLKC_SDIO_LEAF_IO_CLKEN |
			   A7DA_CLKC_SDIO_LEAF_SDPHY_CLKEN,
			   A7DA_CLKC_SDIO23_LEAF_CLK_EN_SET);

		PWRC_SET_CLOCK_INITIALIZED();
	}
	release_hwlock();
}
#endif /* __ATLAS7_STEP_B__ */
