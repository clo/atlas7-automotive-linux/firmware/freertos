/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __ATLAS7_STEP_B__
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"
#include "errno.h"

#include "utils.h"
#include "io.h"
#include "soc_clkc.h"
#include "soc_memc.h"
#include "soc_timer.h"
#include "soc_pwrc.h"
#include "soc_iobridge.h"
#include "soc_pwrc_retain.h"

#define sirfsoc_rtc_iobrg_readl 	uxIoBridgeReadUnsafe
#define sirfsoc_rtc_iobrg_writel	vIoBridgeWriteUnsafe

#define request_hwlock()        REQUEST_HWLOCK(0)
#define release_hwlock()        RELEASE_HWLOCK(0)

#define ATLAS7_DDR_800MHZ

#define max(a,b) ((a)>(b)?(a):(b))

#define __udelay vUsWait

#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF 0x10820008
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0 0x10820850


#define                        SCTL_INIT           (0x0)
#define                        SCTL_CFG            (0x1)
#define                        SCTL_GO             (0x2)
#define                        SCTL_SLEEP          (0x3)
#define                        SCTL_WAKEUP         (0x4)
#define                        STAT_INIT_MEM       (0x0)
#define                        STAT_CONFIG         (0x1)
#define                        STAT_ACCESS          (0x3)
#define                        STAT_LOW_POWER      (0x5)
#ifdef ATLAS7_ENTRY
#define DDR_8BITS   1
#endif
#define DDR_2T_EN    1

#define CL          11
#define AL           0

#define trefi     7800
#define tfaw     40000
#define twr      15000
#define trc      48750
#define trp      13750
#define tras     35000
#define trcd     13750
#define trrd      7500
#define trrdn        4
#define twtr      7500
#define twtrn        4
#define trtp      7500
#define trtpn        4
#define tmrdn        4
#define trfc    350000
#define tdllkn     512
#define txp       6000
#define txpn         3
#define txpdll   24000
#define txpdlln     10
#define tzqcs    80000
#define tzqcsn      64
#define tzqinit 640000
#define tzqinitn   512
#define tzqoper 320000
#define tzqopern   256
#define tcksre   10000
#define tcksren      5
#define tcksrx   10000
#define tcksrxn      5
#define tcke      5000
#define tcken        3
#define tmod     15000
#define tmodn       12
#define twlmrdn             40
#define twlo              7500
#define tccdn                4
#define tdllkn             512
#define txs       (trfc+10000)
#define txsn                 5
#define txsdlln         tdllkn
#define tzqcl   (max(tzqinit, tzqoper))
#define tzqcln  (max(tzqinitn, tzqopern))
#define SPARE	0
#define trtwn   (SPARE ? 1 : 0)

#define PWRC_PDN_CTRL_CLR_OFF  A7DA_PWRC_PDN_CTRL_CLR
#define PWRC_PDN_DRAM_HOLD_BIT	0x8
#define PWRC_DDR_LDO_CONFIG    A7DA_PWRC_DDR_LDO_CONFIG

/*
 * Generic virtual read/write.  Note that we don't support half-word
 * read/writes.  We define __arch_*[bl] here, and leave __arch_*w
 * to the architecture specific code.
 */
#define __arch_getl(a)          SOC_REG(((unsigned long)a))
#define __arch_putl(v,a)        SOC_REG(((unsigned long)a)) = v
/*
 * TODO: The kernel offers some more advanced versions of barriers, it might
 * have some advantages to use them instead of the simple one here.
 */
#define dmb()       __asm__ __volatile__ ("" : : : "memory")
#define __iormb()   dmb()
#define __iowmb()   dmb()

#define writel(v,c) ({ u32 __v = v; __iowmb(); __arch_putl(__v,c); __v; })
#define readl(c)    ({ u32 __v = __arch_getl(c); __iormb(); __v; })


#define modify_reg(a, and_value, or_value) { \
	writel((readl(a) & and_value) | or_value, a); \
}

#define poll_reg(a, m, v, l, e) { \
	unsigned int wd = l; \
	while ((readl(a)&m) != (v&m)) { \
		if (--wd == 0) { \
			return e; \
		} \
	} \
}

#define WAIT_PGSR0_IDONE(error_code) { \
	__udelay(1); \
	poll_reg(A7DA_MEMC_PHY_PGSR0, 1, 1, 100000, error_code); \
	poll_reg(A7DA_MEMC_PHY_PGSR0, 0x0ff00000, 0, 1, (0xeeee0000 | \
		((readl(A7DA_MEMC_PHY_PGSR0)>>20)&0xff))); \
}

static unsigned int cycles(unsigned int time_in_ps, unsigned int freq_in_16mhz)
{
	int o;
	unsigned int c;

	o = 0;
	while (time_in_ps > 333000) {
		o++;
		time_in_ps = (unsigned int)((time_in_ps + 1) >> 1);
	}

	c = (unsigned
	     int)(((unsigned long int)time_in_ps *
		   (unsigned long int)freq_in_16mhz + 15999999) / 16000000);

	while (o) {
		c *= 2;
		o--;
	}
	return c;
}


static int ddr_freq_set_26(void)
{
	unsigned int fout;
	int divq;
	unsigned int fvco;
	unsigned int divr;
	int divf;
	unsigned int final_ddr_freq_x16;
	unsigned int xtal_freq;

	xtal_freq = 26;
#ifdef ATLAS7_DDR_800MHZ
	fout = 400;
#else
	fout = 300; /* fout=target_ddr_freq >> 1 */
#endif
	divq = 3; /*  log_2_n_round_up(12);  4*/
	if ((divq < 1) || (divq > 5))
		return 0xee100000;

	fvco = fout << divq; /* 300*16=4800 */
	divr = 0;
#ifdef ATLAS7_DDR_800MHZ
	divf = 60;
#else
	divf = 45; /* (fvco  / (26) / 2 - 1) */
#endif
	if (divf > 511)
		divf = 511;
	if (divf < 0)
		divf = 0;
	writel(divf | (divr << 16), A7DA_CLKC_MEMPLL_AB_FREQ);
	writel((1 << 12) | (divq << 0) | 0x110, A7DA_CLKC_MEMPLL_AB_CTRL1);

	modify_reg(A7DA_CLKC_MEMPLL_AB_CTRL0, ~0x00000010, 0);
	modify_reg(A7DA_CLKC_MEMPLL_AB_CTRL0, 0xffffffff, 1);
	/* xtal_freq * (divf + 1) * 2 / (divr + 1); */
	fvco = xtal_freq * (divf + 1) * 2;
	final_ddr_freq_x16 = fvco << (5 - divq);

	poll_reg(A7DA_CLKC_MEMPLL_AB_STATUS, 1, 1, 100000, -1);

	return final_ddr_freq_x16;
}

/*
 * TODO: figure out the ddr controller setting sequence
 * and split into sub functions.
 */

#define LUT_ENTRY_NUM   30
static int ddr_init_internal(void)
{
	unsigned int ddr_freq_x16;
	unsigned int apb_freq;
	int ddr_retention;
	unsigned int zdata;
	int cas_write_latency;
	int cas_latency;

	int tfaw_cycles;
	int twr_cycles;
	int trp_cycles;
	int tras_cycles;

	int trcd_cycles;
	int trfc_cycles;
	int trrd_cycles;
	int twtr_cycles;
	int trtp_cycles;
	int txp_cycles;
	int txpdll_cycles;
	int tzqcs_cycles;
	int tzqcl_cycles;
	int tcksre_cycles;
	int tcksrx_cycles;
	int tcke_cycles;
	int tmod_cycles;
	int txs_cycles;
	int trc_cycles;
	int twlo_cycles;

	int tfaw_cfg;
	int tfaw_cfg_offset;
	int cas_latency_code;
	int write_recovery_code;
	int mode_register_0;
	int mode_register_1;
	int write_latency;
	int read_latency;
	int cas_write_latency_code;
	int mode_register_2;
	int mode_register_3;
	int ret;
	unsigned int val;
	int i;
	/* unsigned int ddr_ldo_config; */

	apb_freq = 200;
	ddr_freq_x16 = ddr_freq_set_26();
	ddr_retention = 0;
	cas_write_latency = (ddr_freq_x16 <= (400 << 4)) ? 5 :
	    (ddr_freq_x16 <= (533 << 4)) ? 6 :
	    (ddr_freq_x16 <= (666 << 4)) ? 7 :
	    (ddr_freq_x16 <= (800 << 4)) ? 8 :
	    (ddr_freq_x16 <= (934 << 4)) ? 9 :
	    (ddr_freq_x16 <= (1069 << 4)) ? 10 :
	    (ddr_freq_x16 <= (1200 << 4)) ? 11 :
	    (ddr_freq_x16 <= (1333 << 4)) ? 12 : 0;
	cas_latency = (cas_write_latency == 5) ? 6 :
	    (cas_write_latency == 6) ? 8 :
	    (cas_write_latency == 7) ? 10 :
	    (cas_write_latency == 8) ? 11 :
	    (cas_write_latency == 9) ? 13 : (cas_write_latency == 10) ? 14 : 0;

	tfaw_cycles = cycles(tfaw, ddr_freq_x16);
	twr_cycles = cycles(twr, ddr_freq_x16);
	trp_cycles = cycles(trp, ddr_freq_x16);
	tras_cycles = cycles(tras, ddr_freq_x16);

	trcd_cycles = cycles(trcd, ddr_freq_x16);
	trfc_cycles = cycles(trfc, ddr_freq_x16);
	trrd_cycles = max(cycles(trrd, ddr_freq_x16), trrdn);
	twtr_cycles = max(cycles(twtr, ddr_freq_x16), twtrn);
	trtp_cycles = max(cycles(trtp, ddr_freq_x16), trtpn);
	txp_cycles = max(cycles(txp, ddr_freq_x16), txpn);
	txs_cycles = max(cycles(txs, ddr_freq_x16), txsn);
	txpdll_cycles = max(cycles(txpdll, ddr_freq_x16), txpdlln);
	tzqcs_cycles = max(cycles(tzqcs, ddr_freq_x16), tzqcsn);
	tzqcl_cycles = max(cycles(tzqcl, ddr_freq_x16), tzqcln);
	tcksre_cycles = max(cycles(tcksre, ddr_freq_x16), tcksren);
	tcksrx_cycles = max(cycles(tcksrx, ddr_freq_x16), tcksrxn);
	tcke_cycles = max(cycles(tcke, ddr_freq_x16), tcken);
	tmod_cycles = max(cycles(tmod, ddr_freq_x16), tmodn);
	twlo_cycles = cycles(twlo, ddr_freq_x16);

	trc_cycles = trp_cycles + tras_cycles;

	tfaw_cfg = 0;
	tfaw_cfg_offset = 4 * trrd_cycles - tfaw_cycles;

	while (tfaw_cfg_offset < 0) {
		tfaw_cfg++;
		tfaw_cfg_offset += trrd_cycles;
	}
	if (tfaw_cycles != ((4 + tfaw_cfg) * trrd_cycles - tfaw_cfg_offset)) {
		ret = 0xee700000 | (tfaw_cfg << 8) | tfaw_cfg_offset;
		return ret;
	}

	if (twr_cycles < 5)
		twr_cycles = 5;
	if ((twr_cycles > 8) && (twr_cycles & 1))
		twr_cycles++;
	if (twr_cycles > 16)
		return 0xee800000;

	if (trcd_cycles > cas_latency)
		return 0xee810000;
	trcd_cycles = cas_latency;

	cas_latency_code = cas_latency - 4;
	write_recovery_code =
	    (twr_cycles < 8) ? (twr_cycles - 4) : ((twr_cycles >> 1) & 7);
	mode_register_0 =
	    0x100 | (write_recovery_code << 9) | ((cas_latency_code & 8) >> 1) |
	    ((cas_latency_code & 7) << 4);
	#ifdef DDR_8BITS
	mode_register_1 = 0x04;
	#else
	mode_register_1 = 0x06;
	#endif
	write_latency = cas_write_latency + AL;
	read_latency = cas_latency + AL;
	cas_write_latency_code = cas_write_latency - 5;
	mode_register_2 = cas_write_latency_code << 3;
	mode_register_3 = 0x0;

	ddr_retention = sirfsoc_rtc_iobrg_readl(PWRC_PDN_CTRL_CLR_OFF) >> 3;
	ddr_retention &= 1;
	writel(0xb, A7DA_CLKC_RSTC_MEM_SW_RST_CLR);
	__udelay(1);
	writel(0xd, A7DA_CLKC_MEM_LEAF_CLK_EN_CLR);
	__udelay(1);
	writel(0xb, A7DA_CLKC_RSTC_MEM_SW_RST_SET);
	__udelay(1);
	writel(0xd, A7DA_CLKC_MEM_LEAF_CLK_EN_SET);

	/* Take over the DDR LDO control and enable internal DDR LDO*/
	/* TODO: if do this in M3, system hang
	ddr_ldo_config = sirfsoc_rtc_iobrg_readl(PWRC_DDR_LDO_CONFIG);
	sirfsoc_rtc_iobrg_writel(ddr_ldo_config | 0x1,
					PWRC_DDR_LDO_CONFIG);
	*/

#ifdef DDR_8BITS
	writel(0x0, A7DA_MEMC_PHY_DX1GCR);
	writel((1 << 2), DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0);
	writel(2, UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF);
	writel(1, A7DA_MEMC_PPCFG);
	writel(1 << 2, A7DA_MEMC_DFISTCFG0);
#endif

	modify_reg(A7DA_MEMC_PHY_DSGCR, 0xffffffff, (1 << 18));

	writel(((apb_freq + 1) << 21) | ((4 * apb_freq + 1) << 6) |
			 (16 << 0), A7DA_MEMC_PHY_PTR0);
	writel(((100 * apb_freq + 1) << 16) |
		 ((3 * apb_freq + 1) << 0), A7DA_MEMC_PHY_PTR1);

	writel(mode_register_0, A7DA_MEMC_PHY_MR0);
	writel(mode_register_1, A7DA_MEMC_PHY_MR1);
	writel(mode_register_2, A7DA_MEMC_PHY_MR2);
	writel(mode_register_3, A7DA_MEMC_PHY_MR3);
	writel((trc_cycles << 26) | (trrd_cycles << 22) |
		  (tras_cycles << 16) | (trcd_cycles << 12) |
		  (trp_cycles << 8) | (twtr_cycles << 4) |
		 (trtp_cycles << 0), A7DA_MEMC_PHY_DTPR0);
	writel(0x71234000, A7DA_MEMC_PHY_DTAR0);
	writel(0x71234008, A7DA_MEMC_PHY_DTAR1);
	writel(0x71234010, A7DA_MEMC_PHY_DTAR2);
	writel(0x71234018, A7DA_MEMC_PHY_DTAR3);
	modify_reg(A7DA_MEMC_PHY_DTCR, 0xf0ffffff, 0x0100004f);

	if (ddr_retention) {
		poll_reg(A7DA_MEMC_PHY_PGSR0, 1, 1, 100000, -1);
		modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x40001);
		poll_reg(A7DA_MEMC_PHY_PGSR0, 1, 1, 100000, -1);
	} else {
	WAIT_PGSR0_IDONE(0xee100000);
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x40001);
	WAIT_PGSR0_IDONE(0xee110000);
	}
	writel(cycles(1000000 >> 1, ddr_freq_x16), A7DA_MEMC_TOGCNT1U);
	writel(cycles(100000 >> 1, ddr_freq_x16), A7DA_MEMC_TOGCNT100N);
	writel(200, A7DA_MEMC_TINIT);
	writel(500, A7DA_MEMC_TRSTH);

	val =  0x00000021 | (DDR_2T_EN << 3);
	val |= tfaw_cfg << 18;
	writel(val, A7DA_MEMC_MCFG);
	modify_reg(A7DA_MEMC_MCFG1, 0xffffffff, (tfaw_cfg_offset << 8));

	writel(3, A7DA_MEMC_DFITCTRLDELAY);
	writel(1, A7DA_MEMC_DFITPHYWRDATA);
	writel((write_latency & 1) ? ((write_latency - 3) >> 1)
		  : ((write_latency - 4) >> 1), A7DA_MEMC_DFITPHYWRLAT);
	writel((read_latency & 1) ? ((read_latency - 3) >> 1) :
		 ((read_latency - 4) >> 1), A7DA_MEMC_DFITRDDATAEN);
	writel(15, A7DA_MEMC_DFITPHYRDLAT);
	writel(2, A7DA_MEMC_DFITDRAMCLKDIS);
	writel(2, A7DA_MEMC_DFITDRAMCLKEN);
	writel((unsigned int)((ddr_freq_x16 + 4 * apb_freq - 1) /
				 (4 * apb_freq)), A7DA_MEMC_DFITPHYUPDTYPE0);
	writel((unsigned int)((25 * ddr_freq_x16 + apb_freq - 1) /
				 (apb_freq)), A7DA_MEMC_DFITPHYUPDTYPE1);
	writel(1, A7DA_MEMC_DFISTCFG1);
	val = (3 << 28) | (1 << 24) | (7 << 16);
	val |= (3 << 12) | (1 << 8) | (3<<4) | (1 << 0);
	writel(val, A7DA_MEMC_DFILPCFG0);
	writel(0x1, A7DA_MEMC_POWCTL);
	poll_reg(A7DA_MEMC_POWSTAT, 1, 1, 100000, -1);
	writel(0x8, A7DA_MEMC_DFIODTCFG);
	writel((int)(trefi / 100) | (1 << 31), A7DA_MEMC_TREFI);
	writel(tmrdn, A7DA_MEMC_TMRD);
	writel(trfc_cycles, A7DA_MEMC_TRFC);
	writel(trp_cycles, A7DA_MEMC_TRP);
	writel(2, A7DA_MEMC_TRTW);
	writel(AL, A7DA_MEMC_TAL);
	writel(cas_latency, A7DA_MEMC_TCL);
	writel(cas_write_latency, A7DA_MEMC_TCWL);
	writel(tras_cycles, A7DA_MEMC_TRAS);
	writel(trc_cycles, A7DA_MEMC_TRC);
	writel(trcd_cycles, A7DA_MEMC_TRCD);
	writel(trrd_cycles, A7DA_MEMC_TRRD);
	writel(trtp_cycles, A7DA_MEMC_TRTP);
	writel(twr_cycles, A7DA_MEMC_TWR);
	writel(twtr_cycles, A7DA_MEMC_TWTR);
	writel(tdllkn, A7DA_MEMC_TEXSR);
	writel(txp_cycles, A7DA_MEMC_TXP);
	writel(txpdll_cycles, A7DA_MEMC_TXPDLL);
	writel(tzqcl_cycles, A7DA_MEMC_TZQCL);
	writel(tzqcs_cycles, A7DA_MEMC_TZQCS);
	writel(10, A7DA_MEMC_TZQCSI);
	writel(1, A7DA_MEMC_TDQS);
	writel(tcksre_cycles, A7DA_MEMC_TCKSRE);
	writel(tcksrx_cycles, A7DA_MEMC_TCKSRX);
	writel(tcke_cycles, A7DA_MEMC_TCKE);
	writel(tmod_cycles, A7DA_MEMC_TMOD);
	writel(cycles(100000, ddr_freq_x16), A7DA_MEMC_TRSTL);
	writel(0, A7DA_MEMC_TMRR);
	writel(tcke_cycles + 1, A7DA_MEMC_TCKESR);
	writel(0, A7DA_MEMC_TDPD);
	val =  cycles(trefi * 1000, ddr_freq_x16) - 32;
	writel(val, A7DA_MEMC_TREFI_MEM_DDR3);

	val = cycles(8 * trefi * 1000, ddr_freq_x16) - 800;
	modify_reg(A7DA_MEMC_PHY_PGCR2, 0xfffc0000, val);
	modify_reg(A7DA_MEMC_SCFG, 0xffffffff, 0x1);
	writel(((twlo_cycles + 5) << 26) | ((twlmrdn) << 20) |
		  ((trfc_cycles) << 11) | ((tfaw_cycles) << 5) |
		((tmod_cycles - 12) << 2) | ((tmrdn - 4) << 0),
		A7DA_MEMC_PHY_DTPR1);

	writel(((tccdn - 4) << 31) | ((trtwn) << 30) |
		((0) << 29) | ((tdllkn) << 19) | ((tcke_cycles + 1) << 15) |
		((max(txpdll_cycles, txp_cycles)) << 10) |
		((max(txs_cycles, txsdlln)) << 0), A7DA_MEMC_PHY_DTPR2);
	/* Updated FRQSEL range of the PLL */
	modify_reg(A7DA_MEMC_PHY_PLLCR, ~(3 << 18), ((ddr_freq_x16
		<= (250 << 5)) ? (1 << 18) : (0 << 18)));

	if (ddr_retention) {

		zdata = readl(PWRC_SCRATCH_PAD9_F);
		writel((0x50000000 | zdata), A7DA_MEMC_PHY_ZQ0CR0);
		writel(SCTL_CFG, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0x07, STAT_CONFIG,
			 10000, 0xee500000);
		writel(SCTL_GO, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0x07, STAT_ACCESS, 10000, -1);
		writel(SCTL_SLEEP, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0x07, STAT_LOW_POWER,
			 10000, 0xee520000);
		poll_reg(A7DA_MEMC_STAT, 0x70, 0x40, 1, -1);

		sirfsoc_rtc_iobrg_writel(PWRC_PDN_DRAM_HOLD_BIT,
						PWRC_PDN_CTRL_CLR_OFF);

		modify_reg(A7DA_MEMC_PHY_PIR, ~(1 << 27), (1 << 27));
		writel(0x40001, A7DA_MEMC_PHY_PIR);
		WAIT_PGSR0_IDONE(0xee130000);

		writel(SCTL_WAKEUP, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0x07, STAT_ACCESS, 10000, -1);
	}

	if (!(ddr_retention)) {
		writel((1 << 31) | (1 << 20) | (2 << 17) |
		  (mode_register_2 << 4) | 3, A7DA_MEMC_MCMD);
		poll_reg(A7DA_MEMC_MCMD, 0x80000000, 0, 100, -1);

		writel((1 << 31) | (1 << 20) | (3 << 17) |
		  (mode_register_3 << 4) | 3, A7DA_MEMC_MCMD);
		poll_reg(A7DA_MEMC_MCMD, 0x80000000, 0, 100, -1);

		writel((1 << 31) | (1 << 20) | (1 << 17) |
		  (mode_register_1 << 4) | 3, A7DA_MEMC_MCMD);
		poll_reg(A7DA_MEMC_MCMD, 0x80000000, 0, 100, -1);

		writel((1 << 31) | (1 << 20) | (0 << 17) |
		  (mode_register_0 << 4) | 3, A7DA_MEMC_MCMD);
		poll_reg(A7DA_MEMC_MCMD, 0x80000000, 0, 100, -1);

		writel((1 << 31) | (1 << 20) | 5, A7DA_MEMC_MCMD);
		poll_reg(A7DA_MEMC_MCMD, 0x80000000, 0, 100, -1);

		writel(SCTL_CFG, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0xf, STAT_CONFIG, 100, -1);

		writel(SCTL_GO, A7DA_MEMC_SCTL);
		poll_reg(A7DA_MEMC_STAT, 0xf, STAT_ACCESS, 100, -1);
	}
	if (!ddr_retention) {
		#ifdef DDR_8BITS
		val = 0x57;
		modify_reg(A7DA_MEMC_PHY_ZQ0CR1, 0xffffff00, val);
		modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x3);
		#else
		/* update zprog and do zq calibration */
		val = (ddr_freq_x16 > 710 * 16) ? 0x5d : 0x1b;
		modify_reg(A7DA_MEMC_PHY_ZQ0CR1, 0xffffff00, val);
		/* update the A/C and CLK pull up/down impedence LUT value */
		for (i = 0; i < LUT_ENTRY_NUM; i++)
			writel(0xd, A7DA_MEMC_PHY_LUT_AC_PD0 + (i << 2));
		for (i = 0; i < LUT_ENTRY_NUM; i++)
			writel(0xd, A7DA_MEMC_PHY_LUT_AC_PU0 + (i << 2));
		for (i = 0; i < LUT_ENTRY_NUM; i++)
			writel(0xc, A7DA_MEMC_PHY_LUT_CLK_PD0 + (i << 2));
		for (i = 0; i < LUT_ENTRY_NUM; i++)
			writel(0xd, A7DA_MEMC_PHY_LUT_CLK_PU0 + (i << 2));
		modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x0003);
		/* Impedance calibration */
		#endif
		WAIT_PGSR0_IDONE(0xee1e0000);
	}

	#ifdef DDR_8BITS
	writel((0x1C) | (0x1C << 6) | (0x1C << 12) |
		(0x1C << 18) | (0x1C << 24), A7DA_MEMC_PHY_DX0BDLR0);
	writel((0x1C) | (0x1C << 6) | (0x1C << 12) |
		(0x1C << 18) | (0x1C << 24), A7DA_MEMC_PHY_DX0BDLR1);
	writel((0x1C) | (0x1C << 6) | (0x00 << 12), A7DA_MEMC_PHY_DX0BDLR2);
	#else
	writel((0x18) | (0x18 << 6) | (0x18 << 12) |
		(0x18 << 18) | (0x18 << 24), A7DA_MEMC_PHY_DX0BDLR0);
	writel((0x18) | (0x18 << 6) | (0x18 << 12) |
		(0x18 << 18) | (0x18 << 24), A7DA_MEMC_PHY_DX0BDLR1);
	writel((0x18) | (0x18 << 6) | (0x0 << 12), A7DA_MEMC_PHY_DX0BDLR2);
	writel((0x18) | (0x18 << 6) | (0x18 << 12) |
		(0x18 << 18) | (0x18 << 24), A7DA_MEMC_PHY_DX1BDLR0);
	writel((0x18) | (0x18 << 6) | (0x18 << 12) |
		(0x18 << 18) | (0x18 << 24), A7DA_MEMC_PHY_DX1BDLR1);
	writel((0x18) | (0x18 << 6) | (0x0 << 12), A7DA_MEMC_PHY_DX1BDLR2);
	#endif
	modify_reg(A7DA_MEMC_PHY_DX0GCR, 0xC7ffffff, 0);
	modify_reg(A7DA_MEMC_PHY_DX1GCR, 0xC7ffffff, 0);
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x0201);
	WAIT_PGSR0_IDONE(0xee140000);
	/* read_dqs_gate_training */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x0401);
	WAIT_PGSR0_IDONE(0xee150000);
	/* write_leveling2 */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x0801);
	WAIT_PGSR0_IDONE(0xee160000);
	/* read_data_bit_deskew */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x1001);
	WAIT_PGSR0_IDONE(0xee170000);
	/* write_data_bit_deskew */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x2001);
	WAIT_PGSR0_IDONE(0xee180000);
	/* read_data_eye_training */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x4001);
	WAIT_PGSR0_IDONE(0xee190000);
	/* write_data_eye_training */
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x8001);
	WAIT_PGSR0_IDONE(0xee1a0000);
	/* DTMPR=0 (Read Data Training not to use MPR) */
	modify_reg(A7DA_MEMC_PHY_DTCR, ~0x40, 0x0);
	modify_reg(A7DA_MEMC_PHY_PIR, 0xffffffff, 0x0401);
	/* read_dqs_gate_training */
	WAIT_PGSR0_IDONE(0xee1b0000);

	/* keep zdata in always-on domain memory */
	zdata = readl(A7DA_MEMC_PHY_ZQ0SR0) & 0x0fffffff;
	writel(zdata, PWRC_SCRATCH_PAD9_F);
	writel(ddr_retention, PWRC_SCRATCH_PAD10_F);

	return 0;
}

int ddr_init(void)
{
	int ercd = 0;
	request_hwlock();

	if(FALSE == PWRC_IS_DDR_INITIALIZED())
	{
		ercd = ddr_init_internal();

		if(0 == ercd)
		{	/*  initialization pass  */
			PWRC_SET_DDR_INITIALIZED();
		}
	}

	release_hwlock();

	return ercd;
}
#endif /* __ATLAS7_STEP_B__ */
