/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"
#include "errno.h"

#include "soc_irq.h"
#include "core_cm3.h"
#include "soc_gpio.h"

typedef struct GPIO_CHIP_INFO
{
	uint8_t ucIrq;
	uint8_t ucGpioNum;
	uint32_t uxRegBase;
} xGpioChipInfo;

typedef struct GPIO_CHIP
{
	SemaphoreHandle_t xLock;
	void *pxPinIsrDataList[ GPIO_MAX_PIN ];
	xGpioPinISR *pxPinIsrList[ GPIO_MAX_PIN ];
	volatile xGpioChipInfo *pxHwInfo;
} xGpioChip;

/* The GPIO Control Register */
#define GPIO_CTRL_REG( chip, idx )	( ( chip )->pxHwInfo->uxRegBase + 0x04 * ( idx ) )

/* The GPIO interrupt status */
#define GPIO_INTR_STATUS( chip )	( ( chip )->pxHwInfo->uxRegBase + 0x8C )

/* The IRQ number of GPIO */
#define GPIO_IRQ_NUMBER( chip )		( ( chip )->pxHwInfo->ucIrq )

/* The number of GPIO PINs */
#define GPIO_PINS_NUMBER( chip )	( ( chip )->pxHwInfo->ucGpioNum )

/* Definition of GPIO API types */
#define GPIO_API_SAFE	1
#define GPIO_API_UNSAFE	0

static xGpioChip *pxGpioChipList[ eGpioChipMax ];
static xGpioChipInfo xGpioChipHwInfoList[ eGpioChipMax ] =
{
	{ 47, GPIO_RTCM_PINS, GPIO_RTCM_BASE },
	{ 13, GPIO_MEDIAM_LVDS_PINS, GPIO_MEDIAM_LVDS_BASE },
	{ 14, GPIO_MEDIAM_UART_NAND_PINS, GPIO_MEDIAM_UART_NAND_BASE },
	{ 43, GPIO_VDIFM_GNSS_PINS, GPIO_VDIFM_GNSS_BASE },
	{ 44, GPIO_VDIFM_LCD_VIP_PINS, GPIO_VDIFM_LCD_VIP_BASE },
	{ 45, GPIO_VDIFM_SDIO_I2S_PINS, GPIO_VDIFM_SDIO_I2S_BASE },
	{ 46, GPIO_VDIFM_SP_RGMII_PINS, GPIO_VDIFM_SP_RGMII_BASE },
};

static void prvGpioIrqAck( xGpioChip *pxGpioChip, uint8_t ucGpioIdx )
{
	uint32_t uxVal;

	uxVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	/* clear interrupt status */
	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxVal;
}

static void prvGpioIrqMask( xGpioChip *pxGpioChip, uint8_t ucGpioIdx )
{
	uint32_t uxVal;

	uxVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	uxVal &= ~( GPIO_BIT_INTR_EN_MASK | GPIO_BIT_INTR_STATUS_MASK );
	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxVal;
}

static void prvGpioIrqUnmask( xGpioChip *pxGpioChip, uint8_t ucGpioIdx )
{
	uint32_t uxVal;

	uxVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	uxVal &= ~GPIO_BIT_INTR_STATUS_MASK;
	uxVal |= GPIO_BIT_INTR_EN_MASK;
	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxVal;
}

static int32_t prvGpioSetIrqType( xGpioChip *pxGpioChip, uint8_t ucGpioIdx, eGpioIrqType eType )
{
	int32_t xRet = 0;
	uint32_t uxVal;

	uxVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	uxVal &= ~( GPIO_BIT_INTR_STATUS_MASK | GPIO_BIT_INTR_EN_MASK );

	switch ( eType )
	{
	case eGpioIrqTypeEdgeRising:
		uxVal |= GPIO_BIT_INTR_HIGH_MASK | GPIO_BIT_INTR_TYPE_MASK;
		uxVal &= ~GPIO_BIT_INTR_LOW_MASK;
		break;

	case eGpioIrqTypeEdgeFalling:
		uxVal &= ~GPIO_BIT_INTR_HIGH_MASK;
		uxVal |= GPIO_BIT_INTR_LOW_MASK | GPIO_BIT_INTR_TYPE_MASK;
		break;

	case eGpioIrqTypeEdgeBoth:
		uxVal |= GPIO_BIT_INTR_HIGH_MASK | GPIO_BIT_INTR_LOW_MASK | GPIO_BIT_INTR_TYPE_MASK;
		break;

	case eGpioIrqTypeLevelLow:
		uxVal &= ~( GPIO_BIT_INTR_HIGH_MASK | GPIO_BIT_INTR_TYPE_MASK );
		uxVal |= GPIO_BIT_INTR_LOW_MASK;
		break;

	case eGpioIrqTypeLevelHigh:
		uxVal |= GPIO_BIT_INTR_HIGH_MASK;
		uxVal &= ~( GPIO_BIT_INTR_LOW_MASK | GPIO_BIT_INTR_TYPE_MASK );
		break;

	case eGpioIrqTypeNone:
	default:
		xRet = -EINVAL;
		break;
	}

	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxVal;

	return xRet;
}

void vGpioChipISR( eChipId )
{
	uint8_t ucGpioIdx;
	uint32_t uxIntrStatus, uxCtrlReg;
	xGpioPinISR *pxIsrCallback;
	void *pxIsrData;
	xGpioChip *pxGpioChip = pxGpioChipList[ eChipId ];

	NVIC_DisableIRQ( GPIO_IRQ_NUMBER( pxGpioChip ) );
	NVIC_ClearPendingIRQ( GPIO_IRQ_NUMBER( pxGpioChip ) );

	uxIntrStatus = SOC_REG( GPIO_INTR_STATUS( pxGpioChip ) );
	if ( !uxIntrStatus )
	{
		DebugMsg("Bad GPIO Interrupt!\r\n" );
		goto xExit;
	}

	ucGpioIdx = 0;
	while ( uxIntrStatus )
	{
		uxCtrlReg = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );

		/*
		 * Here we must check whether the corresponding GPIO's
		 * interrupt has been enabled, otherwise just skip it
		 */
		if ( ( uxIntrStatus & 0x1 ) && ( uxCtrlReg & GPIO_BIT_INTR_EN_MASK ) )
		{
			LOG( "GpioChip#%d gpio:%d interrupt happens!\r\n", eChipId, ucGpioIdx );
			pxIsrCallback = pxGpioChip->pxPinIsrList[ ucGpioIdx ];
			if ( pxIsrCallback != NULL )
			{
				pxIsrData = pxGpioChip->pxPinIsrDataList[ ucGpioIdx ];
				pxIsrCallback( eChipId, ucGpioIdx, pxIsrData );
			}
		}

		if ( ++ucGpioIdx >= GPIO_PINS_NUMBER( pxGpioChip ) )
			break;

		uxIntrStatus = uxIntrStatus >> 1;
	}

xExit:
	NVIC_EnableIRQ( GPIO_IRQ_NUMBER( pxGpioChip ) );
}

void INT_GPIO_RTCM_Handler( void )
{
	vGpioChipISR( eGpioRtcm );
}

void INT_GPIO_MEDIAM_0_Handler( void )
{
	vGpioChipISR( eGpioMediamLvds );
}

void INT_GPIO_MEDIAM_1_Handler( void )
{
	vGpioChipISR( eGpioMediamUartNand );
}

void INT_GPIO_VDIFM_0_Handler( void )
{
	vGpioChipISR( eGpioVdifmGnss );
}

void INT_GPIO_VDIFM_1_Handler( void )
{
	vGpioChipISR( eGpioVdifmLcdVip );
}

void INT_GPIO_VDIFM_2_Handler( void )
{
	vGpioChipISR( eGpioVdifmSdioI2s );
}

void INT_GPIO_VDIFM_3_Handler( void )
{
	vGpioChipISR( eGpioVdifmSpRgmii );
}

static void prvGpioSetDirectionInput( xGpioChip *pxGpioChip, uint8_t ucGpioIdx, uint8_t ucApiType )
{
	volatile uint32_t uxVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	uxVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	uxVal &= ~GPIO_BIT_OUT_EN_MASK;

	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreGive( pxGpioChip->xLock );
}

static void prvGpioSetDirectionOutput( xGpioChip *pxGpioChip, uint8_t ucGpioIdx, uint8_t ucValue, uint8_t ucApiType )
{
	volatile uint32_t uxRegVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	uxRegVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	if ( ucValue )
		uxRegVal |= GPIO_BIT_DATAOUT_MASK;
	else
		uxRegVal &= ~GPIO_BIT_DATAOUT_MASK;

	uxRegVal &= ~GPIO_BIT_INTR_EN_MASK;
	uxRegVal |= GPIO_BIT_OUT_EN_MASK;

	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxRegVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreGive( pxGpioChip->xLock );
}

static void prvGpioSetValue( xGpioChip *pxGpioChip, uint8_t ucGpioIdx, uint8_t ucValue, uint8_t ucApiType )
{
	volatile uint32_t uxRegVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	uxRegVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	if ( ucValue )
		uxRegVal |= GPIO_BIT_DATAOUT_MASK;
	else
		uxRegVal &= ~GPIO_BIT_DATAOUT_MASK;

	SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) ) = uxRegVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreGive( pxGpioChip->xLock );
}

static uint32_t __on_retain prvGpioGetValue( xGpioChip *pxGpioChip, uint8_t ucGpioIdx, uint8_t ucApiType )
{
	volatile uint32_t uxRegVal;

	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	uxRegVal = SOC_REG( GPIO_CTRL_REG( pxGpioChip, ucGpioIdx ) );
	DebugMsg("0x%x\r\n", uxRegVal);
	if ( ucApiType == GPIO_API_SAFE )
		xSemaphoreGive( pxGpioChip->xLock );

	return !!( uxRegVal & GPIO_BIT_DATAOUT_MASK );
}

/* Functions can be used in normal scenes */
void vGpioEnableInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	prvGpioIrqUnmask( pxGpioChipList[ eChipId ], ucGpioIdx );
}

void vGpioDisableInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	prvGpioIrqMask( pxGpioChipList[ eChipId ], ucGpioIdx );
}

void vGpioClearInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	prvGpioIrqAck( pxGpioChipList[ eChipId ], ucGpioIdx );
}

void vGpioSetDirectionInput( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	prvGpioSetDirectionInput( pxGpioChipList[ eChipId ], ucGpioIdx, GPIO_API_SAFE);
}

void vGpioSetDirectionOutput( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue )
{
	 prvGpioSetDirectionOutput( pxGpioChipList[ eChipId ], ucGpioIdx, ucValue, GPIO_API_SAFE);
}

void vGpioSetValue( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue )
{
	prvGpioSetValue( pxGpioChipList[ eChipId ], ucGpioIdx, ucValue, GPIO_API_SAFE);
}

uint32_t uxGpioGetValue( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	return prvGpioGetValue( pxGpioChipList[ eChipId ], ucGpioIdx, GPIO_API_SAFE);
}

int32_t xGpioChipInitialize( eGpioChipId eChipId )
{
	int xRet;
	xGpioChip *pxGpioChip;

	if ( eChipId >= eGpioChipMax )
	{
		configASSERT( 0 );
		return -ENODEV;
	}

	pxGpioChip = ( xGpioChip * )pvPortMalloc( sizeof( xGpioChip ) );
	if ( pxGpioChip == NULL )
	{
		configASSERT( 0 );
		xRet = -ENOMEM;
		goto failed;
	}

	pxGpioChip->xLock = xSemaphoreCreateMutex();
	if ( pxGpioChip->xLock == NULL )
	{
		configASSERT( 0 );
		xRet = -EINVAL;
		goto freedev;
	}

	pxGpioChip->pxHwInfo = &xGpioChipHwInfoList[ eChipId ];
	pxGpioChipList[ eChipId ] = pxGpioChip;

	NVIC_EnableIRQ( GPIO_IRQ_NUMBER( pxGpioChip ) );
	NVIC_SetPriority( GPIO_IRQ_NUMBER( pxGpioChip ), API_SAFE_INTR_PRIO_0 );

	return 0;

freedev:
	vPortFree( pxGpioChip );

failed:

	return xRet;
}

void vGpioUnRegisterInterrupt( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	xGpioChip *pxGpioChip = pxGpioChipList[ eChipId ];

	configASSERT( pxGpioChip );
	configASSERT( ucGpioIdx < GPIO_PINS_NUMBER( pxGpioChip ) );

	xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	prvGpioIrqMask( pxGpioChip, ucGpioIdx );
	prvGpioIrqAck( pxGpioChip, ucGpioIdx );

	pxGpioChip->pxPinIsrList[ ucGpioIdx ] = NULL;
	pxGpioChip->pxPinIsrDataList[ ucGpioIdx ] = NULL;

	xSemaphoreGive( pxGpioChip->xLock );
}

int32_t xGpioRegisterInterrupt( xGpioPinISR *pxGpioIsr, eGpioChipId eChipId, uint8_t ucGpioIdx, eGpioIrqType eIntrType, void *pxData )
{
	int32_t xRet;
	xGpioChip *pxGpioChip = pxGpioChipList[ eChipId ];

	configASSERT( pxGpioChip );
	configASSERT( ucGpioIdx < GPIO_PINS_NUMBER( pxGpioChip ) );

	xSemaphoreTake( pxGpioChip->xLock, portMAX_DELAY );

	prvGpioIrqMask( pxGpioChip, ucGpioIdx );
	prvGpioSetDirectionInput( pxGpioChip, ucGpioIdx, GPIO_API_UNSAFE );
	xRet = prvGpioSetIrqType( pxGpioChip, ucGpioIdx, eIntrType );
	if ( xRet != 0 )
	{
		configASSERT( 0 );
		goto xFailed;
	}

	pxGpioChip->pxPinIsrList[ ucGpioIdx ] = pxGpioIsr;
	pxGpioChip->pxPinIsrDataList[ ucGpioIdx ] = pxData;

	prvGpioIrqUnmask( pxGpioChip, ucGpioIdx );

xFailed:
	xSemaphoreGive( pxGpioChip->xLock );

	return xRet;
}

void __on_retain vRetainGpioSetDirectionInputUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	if(eGpioRtcm == (eChipId))
	{
		volatile uint32_t uxVal;
		uxVal = SOC_REG(  GPIO_RTCM_BASE + 4*(ucGpioIdx) );
		uxVal &= ~GPIO_BIT_OUT_EN_MASK;
		SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) ) = uxVal;
	}
}
void __on_retain vRetainGpioSetDirectionOutputUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue )
{

	if(eGpioRtcm == (eChipId))
	{
		volatile uint32_t uxRegVal;

		uxRegVal = SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) );
		if ( ucValue )
			uxRegVal |= GPIO_BIT_DATAOUT_MASK;
		else
			uxRegVal &= ~GPIO_BIT_DATAOUT_MASK;

		uxRegVal &= ~GPIO_BIT_INTR_EN_MASK;
		uxRegVal |= GPIO_BIT_OUT_EN_MASK;

		SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) ) = uxRegVal;
	}
}

void __on_retain vRetainGpioSetValueUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx, uint8_t ucValue )
{
	if(eGpioRtcm == (eChipId))
	{
		volatile uint32_t uxRegVal;

		uxRegVal = SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) );
		if ( ucValue )
			uxRegVal |= GPIO_BIT_DATAOUT_MASK;
		else
			uxRegVal &= ~GPIO_BIT_DATAOUT_MASK;

		SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) ) = uxRegVal;
	}
}

uint32_t __on_retain uxRetainGpioGetValueUnsafe( eGpioChipId eChipId, uint8_t ucGpioIdx )
{
	if(eGpioRtcm == (eChipId))
	{
		return (!!(SOC_REG( GPIO_RTCM_BASE + 4*(ucGpioIdx) ) & GPIO_BIT_DATAIN_MASK));
	}

	return 0;
}
