/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"

#include "soc_irq.h"
#include "core_cm3.h"

#include "soc_timer.h"
#include "gpt.h"
/* ================================================ [ MACRO ] ====================================================== */
/* register file */
#define GPT_REG_CTRL(pxGptChip,timer)        		SOC_REG( (pxGptChip->xTimerReg.uxRegCtrl+(timer)*4) )
#define GPT_REG_MATCH(pxGptChip,timer)				SOC_REG( (pxGptChip->xTimerReg.uxRegMatch+(timer)*4) )
#define GPT_REG_MATCH_NUM(pxGptChip,timer)			SOC_REG( (pxGptChip->xTimerReg.uxRegMatchNum+(timer)*4) )
#define GPT_REG_COUNTER(pxGptChip,timer)			SOC_REG( (pxGptChip->xTimerReg.uxRegCounter+(timer)*4) )
#define GPT_REG_INT_STATUS(pxGptChip)				SOC_REG( (pxGptChip->xTimerReg.uxRegIntrStatus) )
#define GPT_REG_WATCHDOG_2ND_MATCH(pxGptChip)		SOC_REG( (pxGptChip->xTimerReg.uxRegWdgMatch) )
#define GPT_REG_WATCHDOG_2ND_COUNTER(pxGptChip)		SOC_REG( (pxGptChip->xTimerReg.uxRegWdgCounter) )
#define GPT_REG_WATCHDOG_STATUS(pxGptChip)			SOC_REG( (pxGptChip->xTimerReg.uxRegWdgStatus) )
#define GPT_REG_WATCHDOG_EN(pxGptChip)				SOC_REG( (pxGptChip->xTimerReg.uxRegWdgEn) )
#define GPT_REG_WATCHDOG_CTRL(pxGptChip)			SOC_REG( (pxGptChip->xTimerReg.uxRegWdgCtrl) )

#define GPT_REG_64COUNTER_CTRL(pxGptChip)         	SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterCtrl) )
#define GPT_REG_64COUNTER_LO(pxGptChip)         	SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterLo) )
#define GPT_REG_64COUNTER_HI(pxGptChip)         	SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterHi) )
#define GPT_REG_64COUNTER_LOAD_LO(pxGptChip)        SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterLoadLo) )
#define GPT_REG_64COUNTER_LOAD_HI(pxGptChip)        SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterLoadHi) )
#define GPT_REG_64COUNTER_RLATCHED_LO(pxGptChip)    SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterRelatchedLo) )
#define GPT_REG_64COUNTER_RLATCHED_HI(pxGptChip)    SOC_REG( (pxGptChip->x64CounterReg[0].uxReg64CounterRelatchedHi) )

#define GPT_TIMER_NUM(pxGptChip)					(pxGptChip->xTimerReg.ucNum)




#define IS_GPT_TIMER_STARTED(pxGptChip,timer)  (0 != (GPT_REG_CTRL(pxGptChip,timer)&(1<<BOF_COUNT_EN)))

#define GPT_IRQ_NUM(chip,timer)					(pxGptChip->xTimerReg.ucIrq[timer])

#define GPT_ISR(id,chip,timer,irqn)             \
    void INT_TIMER_##id##_Handler(void)         \
    {                                           \
		NVIC_DisableIRQ(irqn);					\
		NVIC_ClearPendingIRQ(irqn);				\
		GPT_DEBUG_MSG("  # %s()\r\n",__func__);	\
        prvGptIsrHandler(chip,timer);           \
        NVIC_EnableIRQ(irqn);					\
    }

//#define GPT_ENABLE_LOCK
//#define GPT_DEBUG_MSG(f,...) DebugMsg(f, ##__VA_ARGS__)

#ifndef GPT_DEBUG_MSG
#define GPT_DEBUG_MSG(f,...)
#endif

/* bit offset in CTRL register */
#define BOF_TIMER_DIV  16
#define BOF_LOOP_EN     2
#define BOF_INTR_EN     1
#define BOF_COUNT_EN    0

#define BOF_64_LATCH	0x1
#define BOF_64_LOAD	0x2
/* ================================================ [ TYPE  ] ====================================================== */
typedef struct
{
	uint32_t uxRegCtrl;
	uint32_t uxRegMatch;
	uint32_t uxRegMatchNum;
	uint32_t uxRegCounter;
	uint32_t uxRegIntrStatus;
	uint32_t uxRegWdgEn;
	uint32_t uxRegWdgCounter;
	uint32_t uxRegWdgCtrl;
	uint32_t uxRegWdgMatch;
	uint32_t uxRegWdgStatus;
    uint8_t  ucIrq[GPT_MAX_TIMER];
    uint8_t  ucNum;
    uint8_t  reserved; /* 4 bytes align */
} xGptTimerRegInfo;

typedef struct
{
	uint32_t uxReg64CounterCtrl;
	uint32_t uxReg64CounterLo;
	uint32_t uxReg64CounterHi;
	uint32_t uxReg64CounterLoadHi;
	uint32_t uxReg64CounterLoadLo;
	uint32_t uxReg64CounterRelatchedHi;
	uint32_t uxReg64CounterRelatchedLo;
} xGpt64CounterInfo;

typedef struct GPT_CHIP
{
    #ifdef GPT_ENABLE_LOCK
    SemaphoreHandle_t xLock;
    #endif
    void *pxGptIsrDataList[ GPT_MAX_TIMER ];
    xGptTimerISR *pxGptIsrList[ GPT_MAX_TIMER ];
    xGptTimerRegInfo xTimerReg;
    xGpt64CounterInfo x64CounterReg[0];
} __packed xGptChip ;

/* ================================================ [ DECL  ] ====================================================== */
static void prvGptIsrHandler(eGptChipId eChipID,uint8_t ucTimerIdx);
static void prvGptSetupRegInfo(xGptChip *pxGptChip,eGptChipId  eChipId);
/* ================================================ [ DATA  ] ====================================================== */
static xGptChip *pxGptChipList[ eGptChipMax ];
/* ================================================ [ FUNC  ] ====================================================== */
/*
 * This common ISR handler is shared by OS Timer timer interrupt and watchdog 1st stage timeout.
 */
static void prvGptIsrHandler(eGptChipId eChipId,uint8_t ucTimerIdx)
{
    xGptTimerISR *pxIsrCallback;
    void *pxIsrData;
    xGptChip *pxGptChip;
    pxGptChip = pxGptChipList[ eChipId ];

    GPT_DEBUG_MSG("  # prvGptIsrHandler(%d,%d)\r\n",eChipId,ucTimerIdx);

    /* check whether it is watchdog 1 stage timeout */
    if( (ucTimerIdx==GPT_WDG_TIMER(eChipId)) &&
        (GPT_REG_WATCHDOG_EN(pxGptChip)))
    {	/* yes, it is */
        GPT_DEBUG_MSG("  # Watchdog(%d,wdg_status=0x%x,intr_status=0x%x)\r\n",eChipId,GPT_REG_WATCHDOG_STATUS(pxGptChip),GPT_REG_INT_STATUS(pxGptChip));
        GPT_REG_WATCHDOG_STATUS(pxGptChip) = 1;
    }
    else
    {
    	/* do nothing */
    }

    GPT_REG_INT_STATUS(pxGptChip) = ((uint32_t)1<<ucTimerIdx);

    if( 0 == (GPT_REG_CTRL(pxGptChip,ucTimerIdx)&(1<<BOF_LOOP_EN)) )
    {   /* this is one shot mode, clear the CTRL register, automatically stop*/
        GPT_REG_CTRL(pxGptChip,ucTimerIdx) = 0;
    }

    pxIsrCallback = pxGptChip->pxGptIsrList[ucTimerIdx];
    if ( NULL != pxIsrCallback )
    {
        pxIsrData = pxGptChip->pxGptIsrDataList[ ucTimerIdx ];
        pxIsrCallback( eChipId, ucTimerIdx, pxIsrData );
    }

}
static void prvGptSetupRegInfo(xGptChip *pxGptChip,eGptChipId  eChipId)
{
	if(eGptOsTimerM3 == eChipId)
	{
		pxGptChip->xTimerReg.uxRegCtrl		=A7DA_M3_TIMER_32COUNTER_0_CTRL;
		pxGptChip->xTimerReg.uxRegMatch		=A7DA_M3_TIMER_MATCH_0;
		pxGptChip->xTimerReg.uxRegMatchNum	=A7DA_M3_TIMER_32COUNTER_0_MATCH_NUM;
		pxGptChip->xTimerReg.uxRegCounter	=A7DA_M3_TIMER_COUNTER_0;
		pxGptChip->xTimerReg.uxRegIntrStatus=A7DA_M3_TIMER_INTR_STATUS;
		pxGptChip->xTimerReg.uxRegWdgEn		=A7DA_M3_TIMER_WATCHDOG_EN;
		pxGptChip->xTimerReg.uxRegWdgCounter=A7DA_M3_TIMER_COUNTER_WD;
		pxGptChip->xTimerReg.uxRegWdgCtrl	=A7DA_M3_TIMER_32COUNTER_WD_CTRL;
		pxGptChip->xTimerReg.uxRegWdgMatch	=A7DA_M3_TIMER_MATCH_WD;
		pxGptChip->xTimerReg.uxRegWdgStatus	=A7DA_M3_TIMER_WATCHDOG_STATUS;
		pxGptChip->xTimerReg.ucNum 			= GPT_MAX_TIMER_M3;
		pxGptChip->xTimerReg.ucIrq[0]       = INT_M3_TIMER_0_IRQn;
		pxGptChip->xTimerReg.ucIrq[1]       = INT_M3_TIMER_1_IRQn;
		pxGptChip->xTimerReg.ucIrq[2]       = INT_M3_TIMER_2_IRQn;
	}
	else
	{
		pxGptChip->xTimerReg.uxRegCtrl		=A7DA_TIMER_32COUNTER_0_CTRL+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegMatch		=A7DA_TIMER_MATCH_0+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegMatchNum	=A7DA_TIMER_32COUNTER_0_MATCH_NUM+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegCounter	=A7DA_TIMER_COUNTER_0+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegIntrStatus=A7DA_TIMER_INTR_STATUS+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegWdgEn		=A7DA_TIMER_WATCHDOG_EN+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegWdgCounter=A7DA_TIMER_COUNTER_WD+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegWdgCtrl	=A7DA_TIMER_32COUNTER_WD_CTRL+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegWdgMatch	=A7DA_TIMER_MATCH_WD+(0x8000*eChipId);
		pxGptChip->xTimerReg.uxRegWdgStatus	=A7DA_TIMER_WATCHDOG_STATUS+(0x8000*eChipId);
		pxGptChip->xTimerReg.ucNum 			= GPT_MAX_TIMER;
		pxGptChip->x64CounterReg[0].uxReg64CounterCtrl 	= A7DA_TIMER_64COUNTER_CTRL+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterLo	= A7DA_TIMER_64COUNTER_LO+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterHi	= A7DA_TIMER_64COUNTER_HI+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterLoadHi= A7DA_TIMER_64COUNTER_LOAD_HI+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterLoadLo= A7DA_TIMER_64COUNTER_LOAD_LO+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterRelatchedHi= A7DA_TIMER_64COUNTER_RLATCHED_HI+(0x8000*eChipId);
		pxGptChip->x64CounterReg[0].uxReg64CounterRelatchedLo= A7DA_TIMER_64COUNTER_RLATCHED_LO+(0x8000*eChipId);
		if(eGptOsTimerA==eChipId)
		{
			pxGptChip->xTimerReg.ucIrq[0]       = INT_TIMER_0_IRQn;
			pxGptChip->xTimerReg.ucIrq[1]       = INT_TIMER_1_IRQn;
			pxGptChip->xTimerReg.ucIrq[2]       = INT_TIMER_2_IRQn;
			pxGptChip->xTimerReg.ucIrq[3]       = INT_TIMER_3_IRQn;
			pxGptChip->xTimerReg.ucIrq[4]       = INT_TIMER_4_IRQn;
			pxGptChip->xTimerReg.ucIrq[5]       = INT_TIMER_5_IRQn;
		}
		else
		{
			pxGptChip->xTimerReg.ucIrq[0]       = INT_TIMER_6_IRQn;
			pxGptChip->xTimerReg.ucIrq[1]       = INT_TIMER_7_IRQn;
			pxGptChip->xTimerReg.ucIrq[2]       = INT_TIMER_8_IRQn;
			pxGptChip->xTimerReg.ucIrq[3]       = INT_TIMER_9_IRQn;
			pxGptChip->xTimerReg.ucIrq[4]       = INT_TIMER_10_IRQn;
			pxGptChip->xTimerReg.ucIrq[5]       = INT_TIMER_11_IRQn;
		}
	}
}

int32_t xGptInitialize  (eGptChipId  eChipId)
{
    int32_t xRet;
    uint32_t uxIndex;
    xGptChip * pxGptChip;

    if(eChipId < eGptChipMax)
    {
        pxGptChip = pxGptChipList[eChipId];
        if ( NULL == pxGptChip )
        {
        	if(eGptOsTimerM3 == eChipId)
        	{
        		pxGptChip = ( xGptChip * )pvPortMalloc( sizeof( xGptChip ) );
        	}
        	else
        	{
        		pxGptChip = ( xGptChip * )pvPortMalloc( sizeof( xGptChip ) + sizeof( xGpt64CounterInfo ) );
        	}

            if ( NULL == pxGptChip )
            {
                xRet = -ENOMEM;
            }
            else
            {
                #ifdef GPT_ENABLE_LOCK
                pxGptChip->xLock = xSemaphoreCreateMutex();
                if ( NULL == pxGptChip->xLock )
                {
                    configASSERT( 0 );
                    xRet = -EINVAL;
                    vPortFree( pxGptChip );
                }
                else
                #endif
                {
                	prvGptSetupRegInfo(pxGptChip,eChipId);
                    for ( uxIndex=0; uxIndex<GPT_TIMER_NUM(pxGptChip); uxIndex++ )
                    {
                        pxGptChip->pxGptIsrDataList[uxIndex] = NULL;
                        pxGptChip->pxGptIsrList[uxIndex]     = NULL;
                        NVIC_SetPriority( GPT_IRQ_NUM(pxGptChip,uxIndex), API_SAFE_INTR_PRIO_0 );
                    }

                    if(eGptOsTimerA == eChipId)
                    {	/* WDG ISR from A7 to M3 */
                    	 NVIC_SetPriority( INT_TIMER_WATCHDOG_1ST_IRQn, API_SAFE_INTR_PRIO_0 );
                    }
                    pxGptChipList[eChipId] = pxGptChip;
                    xRet = 0;   /* all initialized OK */
                }
            }
        }
        else
        {
            xRet = -EEXIST; /* device has already been initialized */
        }
    }
    else
    {
        configASSERT( 0 );
        xRet = -ENODEV;
    }

    GPT_DEBUG_MSG("  # xGptInitialize(%d)=%d\r\n",eChipId,xRet);

    return xRet;
}

void    vGptDeinitialize(eGptChipId  eChipId)
{
    xGptChip * pxGptChip;

    if(eChipId < eGptChipMax)
    {
        pxGptChip = pxGptChipList[eChipId];
        if( NULL == pxGptChip )
        {
            /* do nothing as already not initialized */
        }
        else
        {
            #ifdef GPT_ENABLE_LOCK
            vSemaphoreDelete(pxGptChip->xLock);
            #endif
            vPortFree( pxGptChip );
            pxGptChipList[eChipId] = NULL;
        }
    }
    else
    {
        configASSERT( 0 );
    }

    GPT_DEBUG_MSG("  # vGptDeinitialize(%d)\r\n");
}

void    vGptUnRegisterInterrupt( eGptChipId eChipId, uint8_t ucGptIdx )
{
    xGptChip *pxGptChip = pxGptChipList[ eChipId ];

    configASSERT( eChipId < eGptChipMax );
    configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );
    #ifdef GPT_ENABLE_LOCK
    xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
    #endif
    NVIC_DisableIRQ( GPT_IRQ_NUM(pxGptChip,ucGptIdx) );
	if(eGptOsTimerA == eChipId)
	{	/* A7 Timer A to M3 */
		NVIC_DisableIRQ( INT_TIMER_WATCHDOG_1ST_IRQn );
	}
    pxGptChip->pxGptIsrList[ ucGptIdx ] = NULL;
    pxGptChip->pxGptIsrDataList[ ucGptIdx ] = NULL;
    #ifdef GPT_ENABLE_LOCK
    xSemaphoreGive( pxGptChip->xLock );
    #endif
    GPT_DEBUG_MSG("vGptUnRegisterInterrupt(%d,%d)\r\n",eChipId,ucGptIdx);
}

int32_t xGptRegisterInterrupt( xGptTimerISR *pxGptIsr, eGptChipId eChipId, uint8_t ucGptIdx, void *pxData )
{
    int32_t xRet;
    xGptChip *pxGptChip = pxGptChipList[eChipId];

    configASSERT( eChipId < eGptChipMax );
    configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );

    if ( false == IS_GPT_TIMER_STARTED( pxGptChip, ucGptIdx ) )
    {
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
        #endif
        pxGptChip->pxGptIsrList[ucGptIdx] = pxGptIsr;
        pxGptChip->pxGptIsrDataList[ucGptIdx] = pxData;
        NVIC_EnableIRQ( GPT_IRQ_NUM(eChipId,ucGptIdx) );
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreGive( pxGptChip->xLock );
        #endif
        xRet = 0; /* OK */
    }
    else
    {
        xRet = -EACCES;
    }

    GPT_DEBUG_MSG("  # xGptRegisterInterrupt(%d,%d)=%d\r\n",eChipId,ucGptIdx,xRet);

    return xRet;
}

int32_t xGptRegisterWdgInterrupt(  xGptWatchdogISR *pxGptIsr, eGptChipId eChipId,void *pxData )
{
    int32_t xRet;
    xGptChip *pxGptChip = pxGptChipList[eChipId];

    configASSERT( eChipId < eGptChipMax );
    configASSERT( pxGptChip );

    if ( NULL == pxGptChip->pxGptIsrList[GPT_WDG_TIMER(eChipId)] )
    {
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
        #endif
        pxGptChip->pxGptIsrList[GPT_WDG_TIMER(eChipId)] = pxGptIsr;
        pxGptChip->pxGptIsrDataList[GPT_WDG_TIMER(eChipId)] = pxData;

        if(eGptOsTimerA == eChipId)
        {	/* A7 Timer A to M3 */
        	NVIC_EnableIRQ( INT_TIMER_WATCHDOG_1ST_IRQn );
        	/* As INTR_[5] for A ISR can still happened, 2 ISR callout, so disable the INTR_[5] */
        	/* TODO: test point */
        	NVIC_DisableIRQ( GPT_IRQ_NUM(eChipId,GPT_WDG_TIMER(eChipId)) );
        }
        else
        {
        	/* for the others, on M3 side, no interrupt connectivity, so when wdg 1st stage timeout, no ISR to M3 side.
        	 * but still has INTR_[5] for A/B or INTR_[3] ISR can be triggered. */
        	NVIC_EnableIRQ( GPT_IRQ_NUM(eChipId,GPT_WDG_TIMER(eChipId)) );
        }

        #ifdef GPT_ENABLE_LOCK
        xSemaphoreGive( pxGptChip->xLock );
        #endif
        xRet = 0; /* OK */
    }
    else
    {
        xRet = -EACCES;
    }

    GPT_DEBUG_MSG("  # xGptRegisterWdgInterrupt(%d)=%d\r\n",eChipId,xRet);

    return xRet;
}

int32_t xGptStartTimer( eGptChipId eChipId, uint8_t ucGptIdx, const xGptParameter* pxParam )
{
    int32_t xRet;
    xGptChip *pxGptChip = pxGptChipList[eChipId];
    configASSERT( eChipId < eGptChipMax );
    configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );
    configASSERT( pxParam );

    if ( false == IS_GPT_TIMER_STARTED( pxGptChip, ucGptIdx ) )
    {
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
        #endif
        NVIC_ClearPendingIRQ( GPT_IRQ_NUM(pxGptChip,ucGptIdx) );
        GPT_REG_CTRL(pxGptChip,ucGptIdx)    = 0;
        GPT_REG_COUNTER(pxGptChip,ucGptIdx) = 0;
        GPT_REG_MATCH_NUM(pxGptChip,ucGptIdx) = 0;
        GPT_REG_MATCH(pxGptChip,ucGptIdx) = pxParam->uxPeriod;
        GPT_REG_CTRL(pxGptChip,ucGptIdx) = ((uint32_t) pxParam->TIMER_DIV << BOF_TIMER_DIV) + \
                (pxParam->LOOP_EN << BOF_LOOP_EN) + (pxParam->INTR_EN << BOF_INTR_EN) + (1<<BOF_COUNT_EN);
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreGive( pxGptChip->xLock );
        #endif
        xRet = 0; /* OK */
    }
    else
    {
        xRet = -EACCES;
    }

    GPT_DEBUG_MSG("  # xGptStartTimer(%d,%d)=%d\r\n",eChipId,ucGptIdx,xRet);

    return xRet;
}

void vGptStopTimer( eGptChipId eChipId, uint8_t ucGptIdx )
{
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );
    #ifdef GPT_ENABLE_LOCK
    xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
    #endif
    GPT_REG_CTRL(pxGptChip,ucGptIdx) = 0;
    GPT_REG_COUNTER(pxGptChip,ucGptIdx) = 0;
    GPT_REG_MATCH(pxGptChip,ucGptIdx) = 0;
    #ifdef GPT_ENABLE_LOCK
    xSemaphoreGive( pxGptChip->xLock );
    #endif
    GPT_DEBUG_MSG("  # vGptStopTimer(%d,%d)\r\n",eChipId,ucGptIdx);
}
int32_t xGptStart64Timer( eGptChipId eChipId, uint16_t xTIMER_DIV )
{
	int32_t xRet;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
	if (eChipId <= eGptOsTimerB)
	{
		GPT_REG_64COUNTER_CTRL(pxGptChip) = (xTIMER_DIV << BOF_TIMER_DIV);
		GPT_REG_64COUNTER_LOAD_LO(pxGptChip) = 0;
		GPT_REG_64COUNTER_LOAD_HI(pxGptChip) = 0;
		GPT_REG_64COUNTER_CTRL(pxGptChip) |= BOF_64_LOAD;
		xRet = 0;
	}
	else
	{
		xRet = -EACCES;
	}
	return xRet;
}
uint64_t xGptRead64Timer( eGptChipId eChipId )
{
	uint64_t xValue;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
	if (eChipId <= eGptOsTimerB)
	{
		configASSERT(8==sizeof(uint64_t));
		GPT_REG_64COUNTER_CTRL(pxGptChip) |= BOF_64_LATCH;
		xValue = GPT_REG_64COUNTER_RLATCHED_HI(pxGptChip);
		xValue = xValue << 32;
		xValue += GPT_REG_64COUNTER_RLATCHED_LO(pxGptChip);
	}
	else
	{
		configASSERT(0);
		xValue = 0;
	}
	return xValue;
}
uint32_t xGptGetTimerMatchNum( eGptChipId eChipId, uint8_t ucGptIdx )
{
    uint32_t uxValue;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );

    uxValue = GPT_REG_MATCH_NUM(pxGptChip,ucGptIdx);

    return uxValue;
}
uint32_t xGptGetTimerElapsed( eGptChipId eChipId, uint8_t ucGptIdx )
{
    uint32_t uxValue;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );

    if ( false == IS_GPT_TIMER_STARTED( pxGptChip, ucGptIdx ) )
    {
        uxValue = 0;
    }
    else
    {
        uxValue = GPT_REG_COUNTER(pxGptChip,ucGptIdx);
    }

    return uxValue;
}
uint32_t xGptGetTimerRemaining( eGptChipId eChipId, uint8_t ucGptIdx )
{
    uint32_t uxValue;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );
    configASSERT( ucGptIdx < GPT_TIMER_NUM(pxGptChip) );

    if ( false == IS_GPT_TIMER_STARTED( pxGptChip, ucGptIdx ) )
    {
        uxValue = 0;
    }
    else
    {
        uxValue = GPT_REG_MATCH(pxGptChip,ucGptIdx) - GPT_REG_COUNTER(pxGptChip,ucGptIdx);
    }

    return uxValue;
}
int32_t xGptStartWdg( eGptChipId eChipId,const xWdgParameter* pxParam)
{
    int32_t xRet;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );

    if ( false == IS_GPT_TIMER_STARTED( pxGptChip, GPT_WDG_TIMER(eChipId) ) )
    {
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
        #endif
        NVIC_ClearPendingIRQ( GPT_IRQ_NUM(pxGptChip,GPT_WDG_TIMER(eChipId)) );
        if(eGptOsTimerA == eChipId)
		{	/* A7 Timer A to M3 */
			NVIC_ClearPendingIRQ( INT_TIMER_WATCHDOG_1ST_IRQn );
		}

        GPT_REG_WATCHDOG_EN(pxGptChip) = 0;
        GPT_REG_CTRL(pxGptChip,GPT_WDG_TIMER(eChipId))    = 0;
        GPT_REG_COUNTER(pxGptChip,GPT_WDG_TIMER(eChipId)) = 0;
        GPT_REG_MATCH(pxGptChip,GPT_WDG_TIMER(eChipId)) = pxParam->ux1stPeriod;

        GPT_REG_WATCHDOG_2ND_COUNTER(pxGptChip) = 0;
        GPT_REG_WATCHDOG_2ND_MATCH(pxGptChip) = pxParam->ux2ndPeriod;

        GPT_REG_WATCHDOG_CTRL(pxGptChip)      = ((uint32_t) pxParam->TIMER_DIV2 << BOF_TIMER_DIV);

        GPT_REG_CTRL(pxGptChip,GPT_WDG_TIMER(eChipId)) = ((uint32_t) pxParam->TIMER_DIV1 << BOF_TIMER_DIV) + (1 << BOF_INTR_EN) + (1<<BOF_COUNT_EN); /* INTR_EN=1 */

        GPT_REG_WATCHDOG_EN(pxGptChip) = 1;
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreGive( pxGptChip->xLock );
        #endif
        xRet = 0; /* OK */
    }
    else
    {
        xRet = -EACCES;
    }

    GPT_DEBUG_MSG("  # xGptStartWdg(%d)=%d\r\n",eChipId,xRet);

    return xRet;
}
int32_t xGptFeedWdg( eGptChipId eChipId)
{
    int32_t xRet;
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );

    if ( true == IS_GPT_TIMER_STARTED( pxGptChip, GPT_WDG_TIMER(eChipId) ) )
    {
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
        #endif
        GPT_REG_COUNTER(pxGptChip,GPT_WDG_TIMER(eChipId)) = 0;
        #ifdef GPT_ENABLE_LOCK
        xSemaphoreGive( pxGptChip->xLock );
        #endif
        xRet = 0; /* OK */
    }
    else
    {
        xRet = -EACCES;
    }

    GPT_DEBUG_MSG("  # xGptFeedWdg(%d)=%d\r\n",eChipId,xRet);

    return xRet;
}
void    vGptStopWdg( eGptChipId eChipId)
{
	xGptChip *pxGptChip = pxGptChipList[eChipId];
	configASSERT( eChipId < eGptChipMax );
	configASSERT( pxGptChip );

    #ifdef GPT_ENABLE_LOCK
    xSemaphoreTake( pxGptChip->xLock, portMAX_DELAY );
    #endif
    GPT_REG_WATCHDOG_EN(pxGptChip) = 0;
    GPT_REG_CTRL(pxGptChip,GPT_WDG_TIMER(eChipId)) = 0;
    GPT_REG_COUNTER(pxGptChip,GPT_WDG_TIMER(eChipId)) = 0;
    GPT_REG_MATCH(pxGptChip,GPT_WDG_TIMER(eChipId)) = 0;
    if(eGptOsTimerA == eChipId)
	{	/* A7 Timer A to M3 */
		NVIC_DisableIRQ( INT_TIMER_WATCHDOG_1ST_IRQn );
	}
	else
	{
	}
    #ifdef GPT_ENABLE_LOCK
    xSemaphoreGive( pxGptChip->xLock );
    #endif

    GPT_DEBUG_MSG("  # vGptStopWdg(%d,%d)\r\n",eChipId);

}
/* ------------------------ ISR ------------------------------------------------------- */
GPT_ISR(0,eGptOsTimerA,0,INT_TIMER_0_IRQn)
GPT_ISR(1,eGptOsTimerA,1,INT_TIMER_1_IRQn)
GPT_ISR(2,eGptOsTimerA,2,INT_TIMER_2_IRQn)
GPT_ISR(3,eGptOsTimerA,3,INT_TIMER_3_IRQn)
GPT_ISR(4,eGptOsTimerA,4,INT_TIMER_4_IRQn)
GPT_ISR(5,eGptOsTimerA,5,INT_TIMER_5_IRQn)

GPT_ISR(6,eGptOsTimerB,0,INT_TIMER_6_IRQn)
GPT_ISR(7,eGptOsTimerB,1,INT_TIMER_7_IRQn)
GPT_ISR(8,eGptOsTimerB,2,INT_TIMER_8_IRQn)
GPT_ISR(9,eGptOsTimerB,3,INT_TIMER_9_IRQn)
GPT_ISR(10,eGptOsTimerB,4,INT_TIMER_10_IRQn)
GPT_ISR(11,eGptOsTimerB,5,INT_TIMER_11_IRQn)

GPT_ISR(M3_0,eGptOsTimerM3,0,INT_M3_TIMER_0_IRQn)
GPT_ISR(M3_1,eGptOsTimerM3,1,INT_M3_TIMER_1_IRQn)
GPT_ISR(M3_2,eGptOsTimerM3,2,INT_M3_TIMER_2_IRQn)


/* from A7 to M3: IRQ NUMBER 58 */
GPT_ISR(WATCHDOG_1ST,eGptOsTimerA,5,INT_TIMER_WATCHDOG_1ST_IRQn)

/* from M3 to A7: IRQ NUMBER 58 */
/*	This interrupt is not visible by M3
 * void INT_TIMER_M3_WATCHDOG_1ST(void)
 * {
 *
 * }
 */
