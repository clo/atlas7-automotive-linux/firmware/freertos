/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ctypes.h"
#include "debug.h"

#include "io.h"

void __get_registers_from_stack(u32 *p_fault_stack_addr)
{
	/*
	 * These are volatile to try and prevent the compiler/linker
	 * optimising them away as the variables never actually get used.
	 * If the debugger won't show the values of the variables, make
	 * them global my moving their declaration outside of this function.
	 */
	volatile u32 r0;
	volatile u32 r1;
	volatile u32 r2;
	volatile u32 r3;
	volatile u32 r12;
	volatile u32 lr; /* Link register. */
	volatile u32 pc; /* Program counter. */
	volatile u32 psr;/* Program status register. */

	r0 = p_fault_stack_addr[0];
	r1 = p_fault_stack_addr[1];
	r2 = p_fault_stack_addr[2];
	r3 = p_fault_stack_addr[3];

	r12 = p_fault_stack_addr[4];
	lr = p_fault_stack_addr[5];
	pc = p_fault_stack_addr[6];
	psr = p_fault_stack_addr[7];

	DebugMsg("Hard fault handler, last pc is 0x%X\r\n"
		"r0 :0x%X r1:0x%X r2:0x%X r3 :0x%X\r\n"
		"r12:0x%X lr:0x%X pc:0x%X psr:0x%X\r\n",
		 pc, r0, r1, r2, r3, r12, lr, pc, psr);
	return;
}

void HardFault_Handler( void ) __attribute__( ( naked ) );
void HardFault_Handler(void)
{
    __asm volatile
    (
	" tst lr, #4                                                \n"
	" ite eq                                                    \n"
	" mrseq r0, msp                                             \n"
	" mrsne r0, psp                                             \n"
	" ldr r1, [r0, #24]                                         \n"
	" adr r2, __get_registers_from_stack                        \n"
	" orr r2, r2, #1                                            \n"
	" bx r2                                                     \n"
    );
}

/* Debug purpose */
void NMI_Handler(void)
{
	DebugMsg("NMI Handler");
}

void MemManage_Handler(void)
{
	DebugMsg("MemManage_Handler");
}

void UsageFault_Handler (void)
{
	DebugMsg("UsageFault_Handler");
}

void DebugMon_Handler(void)
{
	DebugMsg("DebugMon_Handler");
}

void BusFault_Handler(void)
{
	DebugMsg("BusFault_Handler");
}
