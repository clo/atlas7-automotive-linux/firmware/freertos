/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "event_groups.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "soc_irq.h"
#include "core_cm3.h"
#include "ctypes.h"
#include "debug.h"
#include "errno.h"
#include "io.h"
#include "misclib.h"
#include <soc_clkc.h>
#include "i2c.h"
#include <soc_pinctrl.h>

#define _A7DA_I2C0_BASE					0x17020000
#define _A7DA_I2C1_BASE					0x17030000

#define A7DA_I2C_CLK_CTRL					0x0
#define A7DA_I2C_STATUS					0xC
#define A7DA_I2C_CTRL						0x10
#define A7DA_I2C_IO_CTRL					0x14
#define A7DA_I2C_SDA_DELAY				0x18
#define A7DA_I2C_CMD_START				0x1C
#define A7DA_I2C_CMD_BUF				0x30
#define A7DA_I2C_DATA_BUF				0x80

#define A7DA_I2C_CMD_BUF_MAX				16
#define A7DA_I2C_DATA_BUF_MAX			16

#define A7DA_I2C_CMD(x)				(A7DA_I2C_CMD_BUF + (x)*0x04)
#define A7DA_I2C_DATA_MASK(x)			(0xFF<<(((x)&3)*8))
#define A7DA_I2C_DATA_SHIFT(x)			(((x)&3)*8)

#define A7DA_I2C_DIV_MASK				(0xFFFF)

/* I2C status flags */
#define A7DA_I2C_STAT_BUSY				BIT0
#define A7DA_I2C_STAT_TIP				BIT1
#define A7DA_I2C_STAT_NACK				BIT2
#define A7DA_I2C_STAT_TR_INT				BIT4
#define A7DA_I2C_STAT_STOP				BIT6
#define A7DA_I2C_STAT_CMD_DONE				BIT8
#define A7DA_I2C_STAT_ERR				BIT9
#define A7DA_I2C_CMD_INDEX				(0x1F<<16)

/* I2C control flags */
#define A7DA_I2C_RESET				BIT0
#define A7DA_I2C_CORE_EN				BIT1
#define A7DA_I2C_MASTER_MODE				BIT2
#define A7DA_I2C_CMD_DONE_EN				BIT11
#define A7DA_I2C_ERR_INT_EN				BIT12

#define A7DA_I2C_SDA_DELAY_MASK			(0xFF)
#define A7DA_I2C_SCLF_FILTER				(3 << 8)

#define A7DA_I2C_START_CMD				BIT0

#define A7DA_I2C_CMD_RP(x)				((x)&0x7)
#define A7DA_I2C_NACK				BIT3
#define A7DA_I2C_WRITE				BIT4
#define A7DA_I2C_READ				BIT5
#define A7DA_I2C_STOP				BIT6
#define A7DA_I2C_START				BIT7

#define A7DA_I2C_DEFAULT_SPEED			400000
#define A7DA_I2C_ERR_NOACK				1
#define A7DA_I2C_ERR_TIMEOUT				2

#define A7DA_I2C_XFER_DONE				BIT0
#define IO_CLK_RATE					150000000

struct sirfsoc_i2c {
	u32 base;
	int irq;
	u8 *buff;
	u32 cmd_ptr;
	u32 msg_len;
	u16 flags;
	u8 addr;
	TaskHandle_t task;
	u32 finished_len;
	u32 read_cmd_len;
	int err_status;
	EventGroupHandle_t done;
	SemaphoreHandle_t irq_task_semaphore;
	void (*clk_enable)(void);
	void (*clk_disable)(void);
};

struct sirfsoc_i2c __on_dram siic[2] = {
	{_A7DA_I2C0_BASE, INT_I2C0_IRQn, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clkc_enable_i2c0, clkc_disable_i2c0},
	{_A7DA_I2C1_BASE, INT_I2C1_IRQn, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clkc_enable_i2c1, clkc_disable_i2c1}
};

static void i2c_queue_cmd(struct sirfsoc_i2c *siic)
{
	u32 regval;
	int i = 0;
	if (siic->flags & I2C_M_RD) {
		while (((siic->finished_len + i) < siic->msg_len)
			&& (siic->cmd_ptr < A7DA_I2C_CMD_BUF_MAX)) {
			regval = A7DA_I2C_READ | A7DA_I2C_CMD_RP(0);
			if ((siic->finished_len + i) ==
				(siic->msg_len - 1))
				regval |= A7DA_I2C_STOP | A7DA_I2C_NACK;
			SOC_REG(siic->base + A7DA_I2C_CMD(siic->cmd_ptr++)) =
				regval;
			i++;
		}
		siic->read_cmd_len = i;
	} else {
		while ((siic->cmd_ptr < A7DA_I2C_CMD_BUF_MAX - 1)
				&& (siic->finished_len < siic->msg_len)) {
			regval = A7DA_I2C_WRITE | A7DA_I2C_CMD_RP(0);
			if (siic->finished_len == (siic->msg_len - 1))
				regval |= A7DA_I2C_STOP;
			SOC_REG(siic->base + A7DA_I2C_CMD(siic->cmd_ptr++)) = 
				regval;
			SOC_REG(siic->base + A7DA_I2C_CMD(siic->cmd_ptr++)) = 
				siic->buff[siic->finished_len++];
		}
	}
	siic->cmd_ptr = 0;

	__DMB();
	/* Trigger the transfer */
	SOC_REG(siic->base + A7DA_I2C_CMD_START) = A7DA_I2C_START_CMD;
}

static void i2c_read_data(struct sirfsoc_i2c *siic)
{
	u32 data = 0;
	int i;

	for (i = 0; i < siic->read_cmd_len; i++) {
		if (!(i & 0x3))
			data = SOC_REG(siic->base + A7DA_I2C_DATA_BUF + i * 4);
		siic->buff[siic->finished_len++] =
			(u8)((data & A7DA_I2C_DATA_MASK(i)) >>
				A7DA_I2C_DATA_SHIFT(i));
	}
}

void INT_I2C0_Handler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	NVIC_DisableIRQ(siic[A7DA_I2C0].irq);
	xSemaphoreGiveFromISR(siic[A7DA_I2C0].irq_task_semaphore, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void INT_I2C1_Handler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	NVIC_DisableIRQ(siic[A7DA_I2C1].irq);
	xSemaphoreGiveFromISR(siic[A7DA_I2C1].irq_task_semaphore, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

static void prvI2CTask(void *pvParameters)
{
	struct sirfsoc_i2c *siic = (struct sirfsoc_i2c *)pvParameters;
	u32 i2c_stat;

	while (1) {
		if (xSemaphoreTake(siic->irq_task_semaphore, portMAX_DELAY) == pdFALSE)
			continue;
		i2c_stat = SOC_REG(siic->base + A7DA_I2C_STATUS);
		if (i2c_stat & A7DA_I2C_STAT_ERR) {
			siic->err_status = A7DA_I2C_ERR_NOACK;
			SOC_REG(siic->base + A7DA_I2C_STATUS) = A7DA_I2C_STAT_ERR;

			if (i2c_stat & A7DA_I2C_STAT_NACK)
				DebugMsg("ACK not received\n");
			else
				DebugMsg("I2C error\n");
			SOC_REG(siic->base + A7DA_I2C_CTRL) |= A7DA_I2C_RESET;
			while(SOC_REG(siic->base + A7DA_I2C_CTRL) & A7DA_I2C_RESET)
				__DMB();
			xEventGroupSetBits(siic->done, A7DA_I2C_XFER_DONE);
		} else if (i2c_stat & A7DA_I2C_STAT_CMD_DONE) {
			if (siic->flags & I2C_M_RD)
				i2c_read_data(siic);
			if (siic->finished_len == siic->msg_len)
				xEventGroupSetBits(siic->done, A7DA_I2C_XFER_DONE);
			else
				i2c_queue_cmd(siic);

			SOC_REG(siic->base + A7DA_I2C_STATUS) = A7DA_I2C_STAT_CMD_DONE;
		}
		NVIC_EnableIRQ(siic->irq);
	}
}

static void i2c_set_address(struct sirfsoc_i2c *siic)
{
	u8 addr = siic->addr;
	u32 regval = A7DA_I2C_START | A7DA_I2C_CMD_RP(0) | A7DA_I2C_WRITE;

	if (siic->msg_len == 0)
		regval |= A7DA_I2C_STOP;

	SOC_REG(siic->base + A7DA_I2C_CMD(siic->cmd_ptr++)) = regval;

	addr <<= 1;
	if (siic->flags & I2C_M_RD)
		addr |= 1;

	if (siic->flags & I2C_M_REV_DIR_ADDR)
		addr ^= 1;
	SOC_REG(siic->base + A7DA_I2C_CMD(siic->cmd_ptr++)) = addr;
}

int i2c_xfer_msg(int i2c_cell, u8 addr, u16 flags, u8 *buff, u16 len)
{
	u32 regval = SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL);
	int timeout = (len + 1) * 50 / portTICK_PERIOD_MS;

	siic[i2c_cell].buff = buff;
	siic[i2c_cell].msg_len = len;
	siic[i2c_cell].cmd_ptr = 0;
	siic[i2c_cell].flags = flags;
	siic[i2c_cell].addr = addr;
	siic[i2c_cell].err_status = 0;
	siic[i2c_cell].finished_len = 0;

	i2c_set_address(&siic[i2c_cell]);
	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) = (regval | A7DA_I2C_CMD_DONE_EN
		| A7DA_I2C_ERR_INT_EN);
	i2c_queue_cmd(&siic[i2c_cell]);

	if (xEventGroupWaitBits(siic[i2c_cell].done, A7DA_I2C_XFER_DONE, pdTRUE, pdTRUE,
		timeout) == 0) {
		siic->err_status = A7DA_I2C_ERR_TIMEOUT;
		DebugMsg("Transfer timeout\n");
	}

	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) =
		regval & ~(A7DA_I2C_CMD_DONE_EN | A7DA_I2C_ERR_INT_EN);
	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CMD_START) = 0;

	/* i2c control doesn't response, reset it */
	if (siic->err_status == A7DA_I2C_ERR_TIMEOUT) {
		SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) |= A7DA_I2C_RESET;
		while (SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) & A7DA_I2C_RESET)
			__DMB();
	}
	return siic[i2c_cell].err_status ? -EAGAIN : 0;
}

void i2c_deinit(int i2c_cell)
{
	siic[i2c_cell].clk_disable();
	NVIC_DisableIRQ(siic[i2c_cell].irq);
	if (siic[i2c_cell].task != NULL)
		vTaskDelete(siic[i2c_cell].task);
	if (siic[i2c_cell].done != NULL)
		vEventGroupDelete(siic[i2c_cell].done);
	if (siic[i2c_cell].irq_task_semaphore != NULL)
		vSemaphoreDelete(siic[i2c_cell].irq_task_semaphore);
}

void i2c_init(int i2c_cell)
{
	int ret;
	char task_name[8];
	u32 regval;

	siic[i2c_cell].clk_enable();

	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) = A7DA_I2C_RESET;
	while (SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) & A7DA_I2C_RESET)
		__DMB();
	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CTRL) =
		A7DA_I2C_CORE_EN | A7DA_I2C_MASTER_MODE;
	regval = IO_CLK_RATE / (A7DA_I2C_DEFAULT_SPEED * 6);
	SOC_REG(siic[i2c_cell].base + A7DA_I2C_CLK_CTRL) = regval;
	SOC_REG(siic[i2c_cell].base + A7DA_I2C_SDA_DELAY) = regval;

	uxPinctrlPinFuncSel(I2C0_SDA_PIN, FUNC_I2C);
	uxPinctrlPinFuncSel(I2C0_SCL_PIN, FUNC_I2C);

	snprintf(task_name, 8, "i2c%d", i2c_cell);
	NVIC_SetPriority(siic[i2c_cell].irq, API_SAFE_INTR_PRIO_0);
	
	siic[i2c_cell].done = xEventGroupCreate();
	if (siic[i2c_cell].done == NULL) {
		DebugMsg("Create %s done event failed\n", task_name);
		return;
	}

	siic[i2c_cell].irq_task_semaphore = xSemaphoreCreateBinary();
	if (siic[i2c_cell].irq_task_semaphore == NULL) {
		DebugMsg("Create %s irq task semaphore event failed\n", task_name);
		return;
	}

	NVIC_EnableIRQ(siic[i2c_cell].irq);
	ret = xTaskCreate(prvI2CTask, task_name, configMINIMAL_STACK_SIZE * 2,
		(void *)(&siic[i2c_cell]), tskIDLE_PRIORITY + 1, &siic[i2c_cell].task);
	
	if (ret != pdTRUE) {
		DebugMsg("create %s task failed!ret=%d\r\n", task_name, ret);
		return;
	}
}

static portBASE_TYPE i2c_test( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	u8 val[16];
	BaseType_t xParameterStringLength[16];
	char *dir;
	char *slave_addr;
	u8 u_slave_addr;
	char *reg_addr;
	u8 u_reg_addr;
	char *num_of_bytes;
	u16 u_num_of_bytes;
	u8 data[13];
	char *s_data[12];
	int data_number = 0;
	int i;

	dir = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameterStringLength[0] );
	slave_addr = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameterStringLength[1] );
	reg_addr = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameterStringLength[2] );
	num_of_bytes = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameterStringLength[3] );
	if (dir == NULL || slave_addr == NULL || reg_addr == NULL || num_of_bytes == NULL) {
		DebugMsg( "failed: invalid input paramter\r\n" );
		return 0;
	}
	do {
		s_data[data_number] = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 5 + data_number, &xParameterStringLength[4 + data_number]);
		if (s_data[data_number] == NULL)
			break;
		data_number++;
	} while (1);
	dir[xParameterStringLength[0]] = '\0';
	slave_addr[xParameterStringLength[1]] = '\0';
	reg_addr[xParameterStringLength[2]] = '\0';
	num_of_bytes[xParameterStringLength[3]] = '\0';
	u_slave_addr = (u8)strtoul( slave_addr, NULL, 0 );
	u_reg_addr = (u8)strtoul( reg_addr, NULL, 0 );
	u_num_of_bytes = (u16)strtoul( num_of_bytes, NULL, 0 );

	for (i = 0; i < data_number; i++) {
		s_data[i][xParameterStringLength[4 + i]] = '\0';
		data[i + 1] = (u8)strtoul( s_data[i], NULL, 0 );
	}

	DebugMsg("i2c_test %s 0x%02x 0x%02x %d ", dir, u_slave_addr, u_reg_addr, u_num_of_bytes);
	for (i = 0; i < data_number; i++)
		DebugMsg("0x%02x ", data[i + 1]);
	i2c_init(A7DA_I2C0);
	if (dir[0] == 'w') {
		data[0] = u_reg_addr;
		i = i2c_xfer_msg(A7DA_I2C0, u_slave_addr, 0, data, data_number + 1);
		DebugMsg("ret = %d ", i);
	} else if (dir[0] == 'r') {
		i = i2c_xfer_msg(A7DA_I2C0, u_slave_addr, 0, &u_reg_addr, 1);
		i = i2c_xfer_msg(A7DA_I2C0, u_slave_addr, I2C_M_RD, val, u_num_of_bytes);
		DebugMsg("ret = %d ", i);
		for (i = 0; i < u_num_of_bytes; i++)
			DebugMsg("[0x%02x] ", val[i]);
	} else
		DebugMsg( "failed: invalid input paramter\r\n" );
	DebugMsg("\n");
	i2c_deinit(A7DA_I2C0);
	return 0;
}

const CLI_Command_Definition_t i2c_testParameter =
{
	"i2c_test",
	"\r\ni2c_test w <slave address> <register address> <number of bytes> [byte 1] [byte 2] .... [byte N]\r\n \
	i2c_test r <slave address> <register address> <number of bytes>\r\n",
	i2c_test,
	-1
};
const CLI_Command_Definition_t *P_KALIMBA_I2C_TEST_CLI_Parameter =
		&i2c_testParameter;
