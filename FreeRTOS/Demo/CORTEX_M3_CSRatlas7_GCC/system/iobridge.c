/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ctypes.h"
#include "io.h"
#include "soc_iobridge.h"
#include "soc_pwrc_retain.h"

extern uint32_t Irq_Save(void);
extern void Irq_Restore(uint32_t imask);
uint32_t __on_retain uxIoBridgeLock( void )
{
	uint32_t imask;

	imask = Irq_Save();
	/* Request Hardware Spinlock */
	REQUEST_HWLOCK( 0 );

	return imask;
}

void __on_retain vIoBridgeUnlock( uint32_t imask )
{
	/* Release Hardware Spinlock */
	RELEASE_HWLOCK( 0 );
	Irq_Restore(imask);
}

uint32_t __on_retain uxIoBridgeReadUnsafe( uint32_t uxOffset )
{
	volatile uint32_t data;
	SOC_REG( A7DA_CPURTCIOBG_ADDR ) = uxOffset;
	SOC_REG( A7DA_CPURTCIOBG_WRBE ) = 0;
	SOC_REG( A7DA_CPURTCIOBG_CTRL ) = 1;

	while( SOC_REG(A7DA_CPURTCIOBG_CTRL ) );

	/* Store Data */
	data = SOC_REG( A7DA_CPURTCIOBG_DATA );

	return data;
}

void __on_retain vIoBridgeWriteUnsafe( uint32_t uxAdrs, uint32_t uxVal )
{
	SOC_REG( A7DA_CPURTCIOBG_WRBE ) = 0xF1;
	SOC_REG( A7DA_CPURTCIOBG_ADDR ) = uxAdrs;
	SOC_REG( A7DA_CPURTCIOBG_DATA ) = uxVal;
	SOC_REG( A7DA_CPURTCIOBG_CTRL ) = 1;

	while( SOC_REG( A7DA_CPURTCIOBG_CTRL ) );
}

uint32_t __on_retain uxIoBridgeRead( uint32_t uxOffset )
{
	volatile uint32_t data;
	uint32_t imask;

	imask = uxIoBridgeLock();

	/* Release Hardware Spinlock */
	data = uxIoBridgeReadUnsafe(uxOffset);

	vIoBridgeUnlock(imask);

	return data;
}

void __on_retain vIoBridgeWrite( uint32_t uxAdrs, uint32_t uxVal )
{
	uint32_t imask;

	imask = uxIoBridgeLock();

	vIoBridgeWriteUnsafe(uxAdrs,uxVal);

	vIoBridgeUnlock(imask);
}
