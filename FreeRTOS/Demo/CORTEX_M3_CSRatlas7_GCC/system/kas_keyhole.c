/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"

/* Library include files. */
#include "soc_irq.h"
#include "core_cm3.h"

/* platform include files */
#include "ctypes.h"
#include "io.h"
#include "sizes.h"
#include "utils.h"
#include "debug.h"
#include "pm.h"
#include "soc_clkc.h"
#include "soc_kas.h"

/* KAS DSP definitions */
#define KAS_REGFILE_PC				0xFFFFC0
#define KAS_DEBUG				0xFFFF8C


#define KAS_DEBUG_RUN				(1 << 0)
#define KAS_DEBUG_STOP				(0 << 0)

#define KAS_RESET_DMA_CLIENT			(1 << 0)
#define KAS_DMA_CHAIN_MODE			(1 << 1)

#define KAS_DMAC_IDLE				(1 << 0)

#define KAS_PM_SRAM_BYTES			SZ_128K
#define KAS_DM1_SRAM_BYTES			(SZ_64K + SZ_32K)
#define KAS_DM2_SRAM_BYTES			KAS_DM1_SRAM_BYTES

#define KAS_PM_SRAM_SIZE			(KAS_PM_SRAM_BYTES >> 2)
#define KAS_DM1_SRAM_SIZE			(KAS_DM1_SRAM_BYTES >> 2)
#define KAS_DM2_SRAM_SIZE			KAS_DM1_SRAM_SIZE

#define KAS_DM1_SRAM_START_ADDR			0
#define KAS_DM2_SRAM_START_ADDR			0xFF3000

#define KAS_PM_SRAM_START_ADDR			0

#define KAS_TRANSLATE_24BIT_RIGHT_ALIGNED	(0 << 4)
#define KAS_TRANSLATE_24BIT_LEFT_ALIGNED	(1 << 4)
#define KAS_TRANSLATE_2x16BIT			(2 << 4)
#define KAS_TRANSLATE_24BIT			(3 << 4)
#define KAS_TRANSLATE_LEFT_ALIGNED		(0 << 3)
#define KAS_TRANSLATE_RIGHT_ALIGNED		(1 << 3)

#define KAS_DMAC_FINISH_INT			(1 << 0)

#define PM_UNPACKER_PARAMS_DM_SRC(x)		(x)
#define PM_UNPACKER_PARAMS_PM_DEST(x)		(x + 1)
#define PM_UNPACKER_PARAMS_PM_LEN(x)		(x + 2)
#define PM_UNPACKER_PARAMS_IPC_TRGT_LO16(x)	(x + 3)
#define PM_UNPACKER_PARAMS_IPC_TRGT_HI16(x)	(x + 4)
#define PM_UNPACKER_PARAMS_UNPACK_STATUS(x)	(x + 5)

#define KAS_DMAC_TRANS_FIFO_TO_MEM		(0 << 4)
#define KAS_DMAC_TRANS_MEM_TO_FIFO		(1 << 4)

#define UP_ALIGN_12BYTES(x)			((x + 11) / 12 * 12)
#define DOWN_ALIGN_12BYTES(x)			(x / 12 * 12)

extern unsigned long _kas_firmware_offset;

struct kas_image_header
{
	int magic;
	int pm_img_ofs;
	int pm_img_len;
	int dm1_img_ofs;
	int dm1_img_len;
	int dm2_img_ofs;
	int dm2_img_len;
	int reserve[9];
};

struct firmware_code_head {
	int code_size;
	int pm_offset;
	int dm1_offset;
	int dm2_offset;
};

struct firmware_pm_dm_block {
	u32 start_addr;
	u32 size;
	u32 data;
};

struct firmware_code {
	struct firmware_code_head head;
	void *code;
};

static void firmware_run_pm(u32 start_addr)
{
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_DEBUG << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;

	/* Set KAS program counter */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_REGFILE_PC << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = start_addr;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_DEBUG << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_RUN;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_REGFILE_PC << 2 | 0x2 << 30;

	start_addr = SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA);
	DebugMsg("Kalimba application is running at:%X\r\n", start_addr);
}

void firmware_stop_pm(void)
{
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_DEBUG << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_REGFILE_PC << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = 0;
}

static void firmware_load_pm_though_keyhole(u32 start_addr,
					u32 size_bytes, void *data)
{
	u32 i;
	u32 *code = (u32 *)data;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = start_addr | 0x3 << 30;
	for (i = 0; i < size_bytes >> 2; i++)
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = code[i];
}

static void firmware_load_dm_though_keyhole(u32 start_addr,
					u32 size_bytes, void *data)
{
	u32 i;
	u32 *code = (u32 *)data;

	SOC_REG(A7DA_KAS_CPU_KEYHOLE_MODE) = 4;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = start_addr << 2 | 0x2 << 30;

	for (i = 0; i < size_bytes >> 2; i++)
		SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = code[i];
}

static void firmware_download_pm(struct firmware_code *code)
{
	u32 i;
	void *pm_image = code->code + code->head.pm_offset;
	u32 block_num = ((u32 *)pm_image)[0];
	struct firmware_pm_dm_block *pm_block_ptr = pm_image + sizeof(u32);

	/* Stop pm running */
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_ADDR) = KAS_DEBUG << 2 | 0x2 << 30;
	SOC_REG(A7DA_KAS_CPU_KEYHOLE_DATA) = KAS_DEBUG_STOP;

	/* The KAS's DMA must transfer multiples-of-12 bytes */
	DebugMsg("PM BLOCK:START DOWNLOADING...\r\n");
	for (i = 0; i < block_num; i++) {
		LOG("PM Block start_addr:%X (%X) size:%X\r\n",
				pm_block_ptr->start_addr,
				pm_block_ptr->start_addr | 0x3 << 30,
				pm_block_ptr->size * 4);
		firmware_load_pm_though_keyhole(pm_block_ptr->start_addr,
				pm_block_ptr->size * 4, &pm_block_ptr->data);
		pm_block_ptr = (void *)(&pm_block_ptr->data + pm_block_ptr->size);
	}
}

static void firmware_download_dm(struct firmware_code *code,
					void *dm_image)
{
	u32 i;
	u32 block_num = ((u32 *)dm_image)[0];
	struct firmware_pm_dm_block *dm_block_ptr = dm_image + sizeof(u32);

	DebugMsg("DM BLOCK:START DOWNLOADING...\r\n");
	for (i = 0; i < block_num; i++) {
		LOG("DM Block start_addr:%X (%X) size:%X\r\n",
				dm_block_ptr->start_addr,
				dm_block_ptr->start_addr << 2 | 0x2 << 30,
				dm_block_ptr->size * 4);
		firmware_load_dm_though_keyhole(dm_block_ptr->start_addr,
				dm_block_ptr->size * 4, &dm_block_ptr->data);
		dm_block_ptr = (void *)(&dm_block_ptr->data + dm_block_ptr->size);
	}
}

static void firmware_download_code(struct firmware_code *code)
{
	firmware_download_pm(code);
	firmware_download_dm(code, code->code + code->head.dm1_offset);
	firmware_download_dm(code, code->code + code->head.dm2_offset);
}

void prvSetupKas( void )
{
	uint32_t uxIdx;
	struct firmware_code kas_code, *puxCode;
	struct kas_image_header *pxHeader;
	uint8_t *pucData = (uint8_t *)&_kas_firmware_offset;

	pucData += MEM(PM_ARG_BOOT_ENTRY);

	DebugMsg("KAS FW address %08x\n", pucData);
	for( uxIdx = 0; uxIdx < 16; uxIdx++ )
	{
		if( pucData[ uxIdx ] == 0x22 && pucData[ uxIdx + 1 ] == 0x18 && pucData[ uxIdx + 2 ] == 0xDA && pucData[ uxIdx + 3 ] == 0xCB )
			break;
	}

	if( uxIdx >= 16 )
	{
		DebugMsg("Could not find KAS image\r\n");
		return;
	}

	pxHeader = (void *)( pucData + uxIdx );

	DebugMsg("pxHeader = %X\r\n", pxHeader);
	DebugMsg("magic = %X\r\n", pxHeader->magic);
	DebugMsg("pm_img_ofs = %X\r\n", pxHeader->pm_img_ofs);
	DebugMsg("pm_img_len = %d\r\n", pxHeader->pm_img_len);
	DebugMsg("dm1_img_ofs = %X\r\n", pxHeader->dm1_img_ofs);
	DebugMsg("dm1_img_len = %d\r\n", pxHeader->dm1_img_len);
	DebugMsg("dm2_img_ofs = %X\r\n", pxHeader->dm2_img_ofs);
	DebugMsg("dm2_img_len = %d\r\n", pxHeader->dm2_img_len);

	puxCode = &kas_code;
	puxCode->code = pxHeader;
	puxCode->head.code_size = pxHeader->pm_img_len + pxHeader->dm1_img_len + pxHeader->dm2_img_len;
	puxCode->head.pm_offset = pxHeader->pm_img_ofs;
	puxCode->head.dm1_offset = pxHeader->dm1_img_ofs;
	puxCode->head.dm2_offset = pxHeader->dm2_img_ofs;

	clkc_enable_kalimba();

	firmware_download_code( puxCode );
	firmware_stop_pm();
	firmware_run_pm(0);
}
