/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __ATLAS7_STEP_B__
/* Scheduler include files. */
#include "FreeRTOS.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

#include "ctypes.h"
#include "pm.h"
#include "debug.h"
#include "misclib.h"
#include "soc_timer.h"
#include "soc_pwrc.h"
#include "soc_pwrc_retain.h"
#include "soc_iobridge.h"
#include "soc_cache.h"
#include "image_header.h"

#define LOADER_U_BOOT_FROM_ADDR 0x18080000
#define LOADER_U_BOOT_TO_ADDR   0x00100000
#define LOADER_U_BOOT_SIZE 		(512*1024)
extern int ddr_init(void);

#ifdef CONFIG_ENABLE_LOADER_TEST
static uint32_t mark1,mark2,mark3,mark4,mark5,mark6;
#endif

/*
 *   loader will do load the u-boot.csr which was stored in the address of 0x80000(512K) of QSPI to
 * DDRAM address 0x100000(1M), and then notify A7 ROM code to start the DDRAM. That is the ROM code
 * of A7 will then on copy the u-boot image from 0x40100000 to address 0x5b800000, and then jump
 * to the u-boot.
 *
 * Command to get a image which combine rtos and u-boot
 *	  cp -v gcc/RTOSDemo.bin $poky/sources/linux-x86/devtools/sign/
 *    cd $poky/sources/linux-x86/devtools/sign/
 *    ./sign -r RTOSDemo.bin M3_key.bin output.bin 0 0
 *    cp -v $poky/build/burn-image-here/u-boot.csr .
 *    cp output.bin output_temp.bin
 *    truncate -s 512k output_temp.bin
 *    cat output_temp.bin u-boot.csr > rtos_with_boot.bin
 * Then on flash it in dependent mode on A7 with command qspi_loop.sh
 *
 *	MAKE:  make all M3_TEST_ENABLE=yes M3_MODE=independent BOARD_VER=atlas7b M3_LOADER_ENABLE=yes
 *
 * Enable DRAM boot by keep SW2 PIN1 and PIN2 OFF; SW2 PIN3 and PIN4 ON.
 * Enable SD   boot by keep SW2 PIN1 and PIN3 OFF; SW2 PIN2 ON.
 * once the uboot image has been flashed to QSPI, the key status doesn't matter
 * only make sure it is independent mode
 */
void vLoaderStartup(void)
{
	#ifdef __M3_TINY_LOADER_USED__
	uint32_t u32Address,u32Size=0;
	u32Address = uxGetImageAddressAndSize(IMAGE_ID_UBOOT,&u32Size);
	#endif
	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark1 = uxOsGetCurrentTick();
	#endif

	/* NoC Disconnect */
	vIoBridgeWrite(A7DA_PWRC_RTC_NOC_PWRCTL_SET, 0x49);
	while ((uxIoBridgeRead(A7DA_PWRC_RTC_NOC_PWRCTL_SET) & 0x1ff) != 0x1ff);

	/* Core reset */
	vIoBridgeWrite(A7DA_PWRC_RTC_SW_RSTC_CLR, 0x1);

	/* Hold A7 */
	SOC_REG(A7DA_PWRC_M3_CPU_RESET) = 0;

	/* End core reset */
	vIoBridgeWrite(A7DA_PWRC_RTC_SW_RSTC_SET, 0x1);

	/* Wait for core power on */
	vUsWait(5);

	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark2 = uxOsGetCurrentTick();
	#endif

	PWRC_CLEAR_DDR_INITIALIZED_FLAG();
	ddr_init();
	PWRC_CLEAR_CLOCK_INITIALIZED_FLAG();
	PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG();
	PWRC_CLEAR_DDR_INITIALIZED_FLAG();
	__asm__ __volatile__ ("dsb" : : : "memory");

	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark3 = uxOsGetCurrentTick();
	#endif

	#ifdef __M3_TINY_LOADER_USED__
	memcpy((void*)LOADER_U_BOOT_TO_ADDR,(void*)u32Address,u32Size);
	#else
	memcpy((void*)LOADER_U_BOOT_TO_ADDR,(void*)LOADER_U_BOOT_FROM_ADDR,LOADER_U_BOOT_SIZE);
	#endif
	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark4 = uxOsGetCurrentTick();
	#endif

	vDcacheFlush();
	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark5 = uxOsGetCurrentTick();
	#endif

	/* Set DRAM boot tag and address */
	SOC_REG(PWRC_SCRATCH_PAD1_F) = 0x5AC60909;
	SOC_REG(PWRC_SCRATCH_PAD5_F) = LOADER_U_BOOT_TO_ADDR+0x40000000;
	__asm__ __volatile__ ("dsb" : : : "memory");

	/* Release A7 */
	SOC_REG(A7DA_PWRC_M3_CPU_RESET) = 1;

	/* NoC Reconnect */
	vIoBridgeWrite(A7DA_PWRC_RTC_NOC_PWRCTL_CLR, 0x49);
	while ((uxIoBridgeRead(A7DA_PWRC_RTC_NOC_PWRCTL_SET) & 0x124));
	#ifdef CONFIG_ENABLE_LOADER_TEST
	mark6 = uxOsGetCurrentTick();
	#endif

}
#ifdef CONFIG_ENABLE_LOADER_TEST
void vLoaderPerformance(void)
{
	DebugMsg("  >> Init hold reset of A7 cost %d=(%d-%d) us\r\n",mark2-mark1,mark2,mark1);
	DebugMsg("  >> Init DDRAM cost %d=(%d-%d) us\r\n",mark3-mark2,mark3,mark2);
	DebugMsg("  >> Copy uboot to DDRAM cost %d=(%d-%d) us\r\n",mark4-mark3,mark4,mark3);
	DebugMsg("  >> Flush cache to DDRAM cost %d=(%d-%d) us\r\n",mark5-mark4,mark5,mark4);
	DebugMsg("  >> End release reset of A7 cost %d=(%d-%d) us\r\n",mark6-mark5,mark6,mark5);
	DebugMsg("  >> All cost %d=(%d-%d) us\r\n",mark6-mark1,mark6,mark1);
}
#endif

#endif /* __ATLAS7_STEP_B__ */
