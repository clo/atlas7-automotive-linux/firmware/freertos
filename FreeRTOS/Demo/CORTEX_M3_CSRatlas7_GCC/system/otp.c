/*
 * OTP read/write from discretix interface
 *
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "io.h"
#include "soc_otp.h"

/* macros */
#define DX_HOST_AIB_ADDR_REG_REG_OFFSET		0xAA4UL
#define DX_HOST_AIB_WDATA_REG_REG_OFFSET	0xAA8UL
#define DX_HOST_AIB_RDATA_REG_REG_OFFSET	0xAACUL
#define DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET	0xAB0UL
#define DX_AIB_FUSE_ACK_REG_OFFSET		0xAB4UL

#define OTP_MAX_OFFSET				0xff
#define SEC_DXH_MODULE_BASE_ADDR		0x18240000

#define SYS_WriteRegister(addr, val) SOC_REG(addr) = val
#define SYS_ReadRegister(addr, val) val = SOC_REG(addr)

#define DX_MNG_READ_WORD_VIA_AIB(nvm_addr, nvm_data)	\
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			SEC_DXH_MODULE_BASE_ADDR + \
			DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvm_addr); \
		do { \
			SYS_ReadRegister( \
				SEC_DXH_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		SYS_ReadRegister( \
			SEC_DXH_MODULE_BASE_ADDR + \
			DX_HOST_AIB_RDATA_REG_REG_OFFSET, nvm_data);\
	} while (0)

/* Write a word via NVM */
#define DX_MNG_WRITE_WORD_VIA_AIB(nvm_addr, nvm_data) \
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			SEC_DXH_MODULE_BASE_ADDR + \
			DX_HOST_AIB_WDATA_REG_REG_OFFSET, nvm_data); \
		SYS_WriteRegister( \
			SEC_DXH_MODULE_BASE_ADDR +  \
			DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvm_addr); \
		do { \
			SYS_ReadRegister( \
				SEC_DXH_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		do { \
			SYS_ReadRegister( \
				SEC_DXH_MODULE_BASE_ADDR + \
				DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET, \
				tval); \
		} while (!(tval & 0x1)); \
	} while (0)

int read_OTP_word(unsigned long offset, unsigned long *val)
{
	unsigned int old_val;

	if (offset > OTP_MAX_OFFSET)
		return -1;
	/* OTP read in specification describes in WORD values at WORD offset;
	* while the ADDR register needs to be set in 4 bytes aligned fomrat.
	* Expand the WORD offset into byte offset here before reading */
	offset *= sizeof(unsigned long);
	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);

	*val = old_val;

	return 0;
}

int write_OTP_word(unsigned long offset, unsigned long val)
{
	unsigned int old_val;

	if (offset > OTP_MAX_OFFSET)
		return -1;

	/* OTP write in specification describes in WORD values at WORD offset;
	* while the ADDR register needs to be set in 4 bytes aligned fomrat.
	* Expand the WORD offset into byte offset here before writing */
	offset *= sizeof(unsigned long);

	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);
	val |= old_val;
	DX_MNG_WRITE_WORD_VIA_AIB((0x1 << 0x11) | offset, val);
	DX_MNG_READ_WORD_VIA_AIB((0x1 << 0x10) | offset, old_val);

	if (old_val != val)
		return -1;

	return 0;
}
