/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "debug.h"
#include "errno.h"

#include "soc_irq.h"
#include "core_cm3.h"
#include "soc_ioc.h"
#include "soc_pinctrl.h"
#include "io.h"

/*
 * This table will not be use in code, it's only a text description of PIN list
 *	const struct xPinctrlPinDesc xPinctrlIocPads[] = {
 *		RTC PADs
 *		PINCTRL_PIN(0, "rtc_gpio_0"),
 *		PINCTRL_PIN(1, "rtc_gpio_1"),
 *		PINCTRL_PIN(2, "rtc_gpio_2"),
 *		PINCTRL_PIN(3, "rtc_gpio_3"),
 *		PINCTRL_PIN(4, "low_bat_ind_b"),
 *		PINCTRL_PIN(5, "on_key_b"),
 *		PINCTRL_PIN(6, "ext_on"),
 *		PINCTRL_PIN(7, "mem_on"),
 *		PINCTRL_PIN(8, "core_on"),
 *		PINCTRL_PIN(9, "io_on"),
 *		PINCTRL_PIN(10, "can0_tx"),
 *		PINCTRL_PIN(11, "can0_rx"),
 *		PINCTRL_PIN(12, "spi0_clk"),
 *		PINCTRL_PIN(13, "spi0_cs_b"),
 *		PINCTRL_PIN(14, "spi0_io_0"),
 *		PINCTRL_PIN(15, "spi0_io_1"),
 *		PINCTRL_PIN(16, "spi0_io_2"),
 *		PINCTRL_PIN(17, "spi0_io_3"),
 *
 *		TOP PADs
 *		PINCTRL_PIN(18, "spi1_en"),
 *		PINCTRL_PIN(19, "spi1_clk"),
 *		PINCTRL_PIN(20, "spi1_din"),
 *		PINCTRL_PIN(21, "spi1_dout"),
 *		PINCTRL_PIN(22, "trg_spi_clk"),
 *		PINCTRL_PIN(23, "trg_spi_di"),
 *		PINCTRL_PIN(24, "trg_spi_do"),
 *		PINCTRL_PIN(25, "trg_spi_cs_b"),
 *		PINCTRL_PIN(26, "trg_acq_d1"),
 *		PINCTRL_PIN(27, "trg_irq_b"),
 *		PINCTRL_PIN(28, "trg_acq_d0"),
 *		PINCTRL_PIN(29, "trg_acq_clk"),
 *		PINCTRL_PIN(30, "trg_shutdown_b_out"),
 *		PINCTRL_PIN(31, "sdio2_clk"),
 *		PINCTRL_PIN(32, "sdio2_cmd"),
 *		PINCTRL_PIN(33, "sdio2_dat_0"),
 *		PINCTRL_PIN(34, "sdio2_dat_1"),
 *		PINCTRL_PIN(35, "sdio2_dat_2"),
 *		PINCTRL_PIN(36, "sdio2_dat_3"),
 *		PINCTRL_PIN(37, "df_ad_7"),
 *		PINCTRL_PIN(38, "df_ad_6"),
 *		PINCTRL_PIN(39, "df_ad_5"),
 *		PINCTRL_PIN(40, "df_ad_4"),
 *		PINCTRL_PIN(41, "df_ad_3"),
 *		PINCTRL_PIN(42, "df_ad_2"),
 *		PINCTRL_PIN(43, "df_ad_1"),
 *		PINCTRL_PIN(44, "df_ad_0"),
 *		PINCTRL_PIN(45, "df_dqs"),
 *		PINCTRL_PIN(46, "df_cle"),
 *		PINCTRL_PIN(47, "df_ale"),
 *		PINCTRL_PIN(48, "df_we_b"),
 *		PINCTRL_PIN(49, "df_re_b"),
 *		PINCTRL_PIN(50, "df_ry_by"),
 *		PINCTRL_PIN(51, "df_cs_b_1"),
 *		PINCTRL_PIN(52, "df_cs_b_0"),
 *		PINCTRL_PIN(53, "l_pclk"),
 *		PINCTRL_PIN(54, "l_lck"),
 *		PINCTRL_PIN(55, "l_fck"),
 *		PINCTRL_PIN(56, "l_de"),
 *		PINCTRL_PIN(57, "ldd_0"),
 *		PINCTRL_PIN(58, "ldd_1"),
 *		PINCTRL_PIN(59, "ldd_2"),
 *		PINCTRL_PIN(60, "ldd_3"),
 *		PINCTRL_PIN(61, "ldd_4"),
 *		PINCTRL_PIN(62, "ldd_5"),
 *		PINCTRL_PIN(63, "ldd_6"),
 *		PINCTRL_PIN(64, "ldd_7"),
 *		PINCTRL_PIN(65, "ldd_8"),
 *		PINCTRL_PIN(66, "ldd_9"),
 *		PINCTRL_PIN(67, "ldd_10"),
 *		PINCTRL_PIN(68, "ldd_11"),
 *		PINCTRL_PIN(69, "ldd_12"),
 *		PINCTRL_PIN(70, "ldd_13"),
 *		PINCTRL_PIN(71, "ldd_14"),
 *		PINCTRL_PIN(72, "ldd_15"),
 *		PINCTRL_PIN(73, "lcd_gpio_20"),
 *		PINCTRL_PIN(74, "vip_0"),
 *		PINCTRL_PIN(75, "vip_1"),
 *		PINCTRL_PIN(76, "vip_2"),
 *		PINCTRL_PIN(77, "vip_3"),
 *		PINCTRL_PIN(78, "vip_4"),
 *		PINCTRL_PIN(79, "vip_5"),
 *		PINCTRL_PIN(80, "vip_6"),
 *		PINCTRL_PIN(81, "vip_7"),
 *		PINCTRL_PIN(82, "vip_pxclk"),
 *		PINCTRL_PIN(83, "vip_hsync"),
 *		PINCTRL_PIN(84, "vip_vsync"),
 *		PINCTRL_PIN(85, "sdio3_clk"),
 *		PINCTRL_PIN(86, "sdio3_cmd"),
 *		PINCTRL_PIN(87, "sdio3_dat_0"),
 *		PINCTRL_PIN(88, "sdio3_dat_1"),
 *		PINCTRL_PIN(89, "sdio3_dat_2"),
 *		PINCTRL_PIN(90, "sdio3_dat_3"),
 *		PINCTRL_PIN(91, "sdio5_clk"),
 *		PINCTRL_PIN(92, "sdio5_cmd"),
 *		PINCTRL_PIN(93, "sdio5_dat_0"),
 *		PINCTRL_PIN(94, "sdio5_dat_1"),
 *		PINCTRL_PIN(95, "sdio5_dat_2"),
 *		PINCTRL_PIN(96, "sdio5_dat_3"),
 *		PINCTRL_PIN(97, "rgmii_txd_0"),
 *		PINCTRL_PIN(98, "rgmii_txd_1"),
 *		PINCTRL_PIN(99, "rgmii_txd_2"),
 *		PINCTRL_PIN(100, "rgmii_txd_3"),
 *		PINCTRL_PIN(101, "rgmii_txclk"),
 *		PINCTRL_PIN(102, "rgmii_tx_ctl"),
 *		PINCTRL_PIN(103, "rgmii_rxd_0"),
 *		PINCTRL_PIN(104, "rgmii_rxd_1"),
 *		PINCTRL_PIN(105, "rgmii_rxd_2"),
 *		PINCTRL_PIN(106, "rgmii_rxd_3"),
 *		PINCTRL_PIN(107, "rgmii_rx_clk"),
 *		PINCTRL_PIN(108, "rgmii_rxc_ctl"),
 *		PINCTRL_PIN(109, "rgmii_mdio"),
 *		PINCTRL_PIN(110, "rgmii_mdc"),
 *		PINCTRL_PIN(111, "rgmii_intr_n"),
 *		PINCTRL_PIN(112, "i2s_mclk"),
 *		PINCTRL_PIN(113, "i2s_bclk"),
 *		PINCTRL_PIN(114, "i2s_ws"),
 *		PINCTRL_PIN(115, "i2s_dout0"),
 *		PINCTRL_PIN(116, "i2s_dout1"),
 *		PINCTRL_PIN(117, "i2s_dout2"),
 *		PINCTRL_PIN(118, "i2s_din"),
 *		PINCTRL_PIN(119, "gpio_0"),
 *		PINCTRL_PIN(120, "gpio_1"),
 *		PINCTRL_PIN(121, "gpio_2"),
 *		PINCTRL_PIN(122, "gpio_3"),
 *		PINCTRL_PIN(123, "gpio_4"),
 *		PINCTRL_PIN(124, "gpio_5"),
 *		PINCTRL_PIN(125, "gpio_6"),
 *		PINCTRL_PIN(126, "gpio_7"),
 *		PINCTRL_PIN(127, "sda_0"),
 *		PINCTRL_PIN(128, "scl_0"),
 *		PINCTRL_PIN(129, "coex_pio_0"),
 *		PINCTRL_PIN(130, "coex_pio_1"),
 *		PINCTRL_PIN(131, "coex_pio_2"),
 *		PINCTRL_PIN(132, "coex_pio_3"),
 *		PINCTRL_PIN(133, "uart0_tx"),
 *		PINCTRL_PIN(134, "uart0_rx"),
 *		PINCTRL_PIN(135, "uart1_tx"),
 *		PINCTRL_PIN(136, "uart1_rx"),
 *		PINCTRL_PIN(137, "uart3_tx"),
 *		PINCTRL_PIN(138, "uart3_rx"),
 *		PINCTRL_PIN(139, "uart4_tx"),
 *		PINCTRL_PIN(140, "uart4_rx"),
 *		PINCTRL_PIN(141, "usp0_clk"),
 *		PINCTRL_PIN(142, "usp0_tx"),
 *		PINCTRL_PIN(143, "usp0_rx"),
 *		PINCTRL_PIN(144, "usp0_fs"),
 *		PINCTRL_PIN(145, "usp1_clk"),
 *		PINCTRL_PIN(146, "usp1_tx"),
 *		PINCTRL_PIN(147, "usp1_rx"),
 *		PINCTRL_PIN(148, "usp1_fs"),
 *		PINCTRL_PIN(149, "lvds_tx0d4p"),
 *		PINCTRL_PIN(150, "lvds_tx0d4n"),
 *		PINCTRL_PIN(151, "lvds_tx0d3p"),
 *		PINCTRL_PIN(152, "lvds_tx0d3n"),
 *		PINCTRL_PIN(153, "lvds_tx0d2p"),
 *		PINCTRL_PIN(154, "lvds_tx0d2n"),
 *		PINCTRL_PIN(155, "lvds_tx0d1p"),
 *		PINCTRL_PIN(156, "lvds_tx0d1n"),
 *		PINCTRL_PIN(157, "lvds_tx0d0p"),
 *		PINCTRL_PIN(158, "lvds_tx0d0n"),
 *		PINCTRL_PIN(159, "jtag_tdo"),
 *		PINCTRL_PIN(160, "jtag_tms"),
 *		PINCTRL_PIN(161, "jtag_tck"),
 *		PINCTRL_PIN(162, "jtag_tdi"),
 *		PINCTRL_PIN(163, "jtag_trstn"),
 *	};
 */

const struct xPinctrlPadConfig xPinctrlIocPadConfs[] = {
	/* The Configuration of IOC_RTC Pads */
	PADCONF(0, 3, 0x0, 0x100, 0x200, -1, 0, 0, 0, 0),
	PADCONF(1, 3, 0x0, 0x100, 0x200, -1, 4, 2, 2, 0),
	PADCONF(2, 3, 0x0, 0x100, 0x200, -1, 8, 4, 4, 0),
	PADCONF(3, 5, 0x0, 0x100, 0x200, -1, 12, 6, 6, 0),
	PADCONF(4, 4, 0x0, 0x100, 0x200, -1, 16, 8, 8, 0),
	PADCONF(5, 4, 0x0, 0x100, 0x200, -1, 20, 10, 10, 0),
	PADCONF(6, 3, 0x0, 0x100, 0x200, -1, 24, 12, 12, 0),
	PADCONF(7, 3, 0x0, 0x100, 0x200, -1, 28, 14, 14, 0),
	PADCONF(8, 3, 0x8, 0x100, 0x200, -1, 0, 16, 16, 0),
	PADCONF(9, 3, 0x8, 0x100, 0x200, -1, 4, 18, 18, 0),
	PADCONF(10, 4, 0x8, 0x100, 0x200, -1, 8, 20, 20, 0),
	PADCONF(11, 4, 0x8, 0x100, 0x200, -1, 12, 22, 22, 0),
	PADCONF(12, 5, 0x8, 0x100, 0x200, -1, 16, 24, 24, 0),
	PADCONF(13, 6, 0x8, 0x100, 0x200, -1, 20, 26, 26, 0),
	PADCONF(14, 5, 0x8, 0x100, 0x200, -1, 24, 28, 28, 0),
	PADCONF(15, 5, 0x8, 0x100, 0x200, -1, 28, 30, 30, 0),
	PADCONF(16, 5, 0x10, 0x108, 0x208, -1, 0, 0, 0, 0),
	PADCONF(17, 5, 0x10, 0x108, 0x208, -1, 4, 2, 2, 0),
	/* The Configuration of IOC_TOP Pads */
	PADCONF(18, 5, 0x80, 0x180, 0x300, -1, 0, 0, 0, 0),
	PADCONF(19, 5, 0x80, 0x180, 0x300, -1, 4, 2, 2, 0),
	PADCONF(20, 5, 0x80, 0x180, 0x300, -1, 8, 4, 4, 0),
	PADCONF(21, 5, 0x80, 0x180, 0x300, -1, 12, 6, 6, 0),
	PADCONF(22, 5, 0x88, 0x188, 0x308, -1, 0, 0, 0, 0),
	PADCONF(23, 5, 0x88, 0x188, 0x308, -1, 4, 2, 2, 0),
	PADCONF(24, 5, 0x88, 0x188, 0x308, -1, 8, 4, 4, 0),
	PADCONF(25, 6, 0x88, 0x188, 0x308, -1, 12, 6, 6, 0),
	PADCONF(26, 5, 0x88, 0x188, 0x308, -1, 16, 8, 8, 0),
	PADCONF(27, 6, 0x88, 0x188, 0x308, -1, 20, 10, 10, 0),
	PADCONF(28, 5, 0x88, 0x188, 0x308, -1, 24, 12, 12, 0),
	PADCONF(29, 5, 0x88, 0x188, 0x308, -1, 28, 14, 14, 0),
	PADCONF(30, 5, 0x90, 0x188, 0x308, -1, 0, 16, 16, 0),
	PADCONF(31, 2, 0x98, 0x190, 0x310, -1, 0, 0, 0, 0),
	PADCONF(32, 1, 0x98, 0x190, 0x310, -1, 4, 2, 4, 0),
	PADCONF(33, 1, 0x98, 0x190, 0x310, -1, 8, 4, 6, 0),
	PADCONF(34, 1, 0x98, 0x190, 0x310, -1, 12, 6, 8, 0),
	PADCONF(35, 1, 0x98, 0x190, 0x310, -1, 16, 8, 10, 0),
	PADCONF(36, 1, 0x98, 0x190, 0x310, -1, 20, 10, 12, 0),
	PADCONF(37, 1, 0xa0, 0x198, 0x318, -1, 0, 0, 0, 0),
	PADCONF(38, 1, 0xa0, 0x198, 0x318, -1, 4, 2, 2, 0),
	PADCONF(39, 1, 0xa0, 0x198, 0x318, -1, 8, 4, 4, 0),
	PADCONF(40, 1, 0xa0, 0x198, 0x318, -1, 12, 6, 6, 0),
	PADCONF(41, 1, 0xa0, 0x198, 0x318, -1, 16, 8, 8, 0),
	PADCONF(42, 1, 0xa0, 0x198, 0x318, -1, 20, 10, 10, 0),
	PADCONF(43, 1, 0xa0, 0x198, 0x318, -1, 24, 12, 12, 0),
	PADCONF(44, 1, 0xa0, 0x198, 0x318, -1, 28, 14, 14, 0),
	PADCONF(45, 0, 0xa8, 0x198, 0x318, -1, 0, 16, 16, 0),
	PADCONF(46, 0, 0xa8, 0x198, 0x318, -1, 4, 18, 18, 0),
	PADCONF(47, 1, 0xa8, 0x198, 0x318, -1, 8, 20, 20, 0),
	PADCONF(48, 1, 0xa8, 0x198, 0x318, -1, 12, 22, 22, 0),
	PADCONF(49, 1, 0xa8, 0x198, 0x318, -1, 16, 24, 24, 0),
	PADCONF(50, 1, 0xa8, 0x198, 0x318, -1, 20, 26, 26, 0),
	PADCONF(51, 1, 0xa8, 0x198, 0x318, -1, 24, 28, 28, 0),
	PADCONF(52, 1, 0xa8, 0x198, 0x318, -1, 28, 30, 30, 0),
	PADCONF(53, 0, 0xb0, 0x1a0, 0x320, -1, 0, 0, 0, 0),
	PADCONF(54, 0, 0xb0, 0x1a0, 0x320, -1, 4, 2, 2, 0),
	PADCONF(55, 0, 0xb0, 0x1a0, 0x320, -1, 8, 4, 4, 0),
	PADCONF(56, 0, 0xb0, 0x1a0, 0x320, -1, 12, 6, 6, 0),
	PADCONF(57, 0, 0xb0, 0x1a0, 0x320, -1, 16, 8, 8, 0),
	PADCONF(58, 0, 0xb0, 0x1a0, 0x320, -1, 20, 10, 10, 0),
	PADCONF(59, 0, 0xb0, 0x1a0, 0x320, -1, 24, 12, 12, 0),
	PADCONF(60, 0, 0xb0, 0x1a0, 0x320, -1, 28, 14, 14, 0),
	PADCONF(61, 0, 0xb8, 0x1a0, 0x320, -1, 0, 16, 16, 0),
	PADCONF(62, 0, 0xb8, 0x1a0, 0x320, -1, 4, 18, 18, 0),
	PADCONF(63, 0, 0xb8, 0x1a0, 0x320, -1, 8, 20, 20, 0),
	PADCONF(64, 0, 0xb8, 0x1a0, 0x320, -1, 12, 22, 22, 0),
	PADCONF(65, 0, 0xb8, 0x1a0, 0x320, -1, 16, 24, 24, 0),
	PADCONF(66, 0, 0xb8, 0x1a0, 0x320, -1, 20, 26, 26, 0),
	PADCONF(67, 0, 0xb8, 0x1a0, 0x320, -1, 24, 28, 28, 0),
	PADCONF(68, 0, 0xb8, 0x1a0, 0x320, -1, 28, 30, 30, 0),
	PADCONF(69, 0, 0xc0, 0x1a8, 0x328, -1, 0, 0, 0, 0),
	PADCONF(70, 0, 0xc0, 0x1a8, 0x328, -1, 4, 2, 2, 0),
	PADCONF(71, 0, 0xc0, 0x1a8, 0x328, -1, 8, 4, 4, 0),
	PADCONF(72, 0, 0xc0, 0x1a8, 0x328, -1, 12, 6, 6, 0),
	PADCONF(73, 0, 0xc0, 0x1a8, 0x328, -1, 16, 8, 8, 0),
	PADCONF(74, 0, 0xc8, 0x1b0, 0x330, -1, 0, 0, 0, 0),
	PADCONF(75, 0, 0xc8, 0x1b0, 0x330, -1, 4, 2, 2, 0),
	PADCONF(76, 0, 0xc8, 0x1b0, 0x330, -1, 8, 4, 4, 0),
	PADCONF(77, 0, 0xc8, 0x1b0, 0x330, -1, 12, 6, 6, 0),
	PADCONF(78, 0, 0xc8, 0x1b0, 0x330, -1, 16, 8, 8, 0),
	PADCONF(79, 0, 0xc8, 0x1b0, 0x330, -1, 20, 10, 10, 0),
	PADCONF(80, 0, 0xc8, 0x1b0, 0x330, -1, 24, 12, 12, 0),
	PADCONF(81, 0, 0xc8, 0x1b0, 0x330, -1, 28, 14, 14, 0),
	PADCONF(82, 0, 0xd0, 0x1b0, 0x330, -1, 0, 16, 16, 0),
	PADCONF(83, 0, 0xd0, 0x1b0, 0x330, -1, 4, 18, 18, 0),
	PADCONF(84, 0, 0xd0, 0x1b0, 0x330, -1, 8, 20, 20, 0),
	PADCONF(85, 2, 0xd8, 0x1b8, 0x338, -1, 0, 0, 0, 0),
	PADCONF(86, 1, 0xd8, 0x1b8, 0x338, -1, 4, 4, 4, 0),
	PADCONF(87, 1, 0xd8, 0x1b8, 0x338, -1, 8, 6, 6, 0),
	PADCONF(88, 1, 0xd8, 0x1b8, 0x338, -1, 12, 8, 8, 0),
	PADCONF(89, 1, 0xd8, 0x1b8, 0x338, -1, 16, 10, 10, 0),
	PADCONF(90, 1, 0xd8, 0x1b8, 0x338, -1, 20, 12, 12, 0),
	PADCONF(91, 2, 0xe0, 0x1c0, 0x340, -1, 0, 0, 0, 0),
	PADCONF(92, 1, 0xe0, 0x1c0, 0x340, -1, 4, 4, 4, 0),
	PADCONF(93, 1, 0xe0, 0x1c0, 0x340, -1, 8, 6, 6, 0),
	PADCONF(94, 1, 0xe0, 0x1c0, 0x340, -1, 12, 8, 8, 0),
	PADCONF(95, 1, 0xe0, 0x1c0, 0x340, -1, 16, 10, 10, 0),
	PADCONF(96, 1, 0xe0, 0x1c0, 0x340, -1, 20, 12, 12, 0),
	PADCONF(97, 0, 0xe8, 0x1c8, 0x348, -1, 0, 0, 0, 0),
	PADCONF(98, 0, 0xe8, 0x1c8, 0x348, -1, 4, 2, 2, 0),
	PADCONF(99, 0, 0xe8, 0x1c8, 0x348, -1, 8, 4, 4, 0),
	PADCONF(100, 0, 0xe8, 0x1c8, 0x348, -1, 12, 6, 6, 0),
	PADCONF(101, 2, 0xe8, 0x1c8, 0x348, -1, 16, 8, 8, 0),
	PADCONF(102, 0, 0xe8, 0x1c8, 0x348, -1, 20, 12, 12, 0),
	PADCONF(103, 0, 0xe8, 0x1c8, 0x348, -1, 24, 14, 14, 0),
	PADCONF(104, 0, 0xe8, 0x1c8, 0x348, -1, 28, 16, 16, 0),
	PADCONF(105, 0, 0xf0, 0x1c8, 0x348, -1, 0, 18, 18, 0),
	PADCONF(106, 0, 0xf0, 0x1c8, 0x348, -1, 4, 20, 20, 0),
	PADCONF(107, 0, 0xf0, 0x1c8, 0x348, -1, 8, 22, 22, 0),
	PADCONF(108, 0, 0xf0, 0x1c8, 0x348, -1, 12, 24, 24, 0),
	PADCONF(109, 1, 0xf0, 0x1c8, 0x348, -1, 16, 26, 26, 0),
	PADCONF(110, 0, 0xf0, 0x1c8, 0x348, -1, 20, 28, 28, 0),
	PADCONF(111, 1, 0xf0, 0x1c8, 0x348, -1, 24, 30, 30, 0),
	PADCONF(112, 5, 0xf8, 0x200, 0x350, -1, 0, 0, 0, 0),
	PADCONF(113, 5, 0xf8, 0x200, 0x350, -1, 4, 2, 2, 0),
	PADCONF(114, 5, 0xf8, 0x200, 0x350, -1, 8, 4, 4, 0),
	PADCONF(115, 5, 0xf8, 0x200, 0x350, -1, 12, 6, 6, 0),
	PADCONF(116, 5, 0xf8, 0x200, 0x350, -1, 16, 8, 8, 0),
	PADCONF(117, 5, 0xf8, 0x200, 0x350, -1, 20, 10, 10, 0),
	PADCONF(118, 5, 0xf8, 0x200, 0x350, -1, 24, 12, 12, 0),
	PADCONF(119, 5, 0x100, 0x250, 0x358, -1, 0, 0, 0, 0),
	PADCONF(120, 5, 0x100, 0x250, 0x358, -1, 4, 2, 2, 0),
	PADCONF(121, 5, 0x100, 0x250, 0x358, -1, 8, 4, 4, 0),
	PADCONF(122, 5, 0x100, 0x250, 0x358, -1, 12, 6, 6, 0),
	PADCONF(123, 6, 0x100, 0x250, 0x358, -1, 16, 8, 8, 0),
	PADCONF(124, 6, 0x100, 0x250, 0x358, -1, 20, 10, 10, 0),
	PADCONF(125, 6, 0x100, 0x250, 0x358, -1, 24, 12, 12, 0),
	PADCONF(126, 6, 0x100, 0x250, 0x358, -1, 28, 14, 14, 0),
	PADCONF(127, 6, 0x108, 0x250, 0x358, -1, 16, 24, 24, 0),
	PADCONF(128, 6, 0x108, 0x250, 0x358, -1, 20, 26, 26, 0),
	PADCONF(129, 0, 0x110, 0x258, 0x360, -1, 0, 0, 0, 0),
	PADCONF(130, 0, 0x110, 0x258, 0x360, -1, 4, 2, 2, 0),
	PADCONF(131, 0, 0x110, 0x258, 0x360, -1, 8, 4, 4, 0),
	PADCONF(132, 0, 0x110, 0x258, 0x360, -1, 12, 6, 6, 0),
	PADCONF(133, 6, 0x118, 0x260, 0x368, -1, 0, 0, 0, 0),
	PADCONF(134, 6, 0x118, 0x260, 0x368, -1, 4, 2, 2, 0),
	PADCONF(135, 6, 0x118, 0x260, 0x368, -1, 16, 8, 8, 0),
	PADCONF(136, 6, 0x118, 0x260, 0x368, -1, 20, 10, 10, 0),
	PADCONF(137, 6, 0x118, 0x260, 0x368, -1, 24, 12, 12, 0),
	PADCONF(138, 6, 0x118, 0x260, 0x368, -1, 28, 14, 14, 0),
	PADCONF(139, 6, 0x120, 0x260, 0x368, -1, 0, 16, 16, 0),
	PADCONF(140, 6, 0x120, 0x260, 0x368, -1, 4, 18, 18, 0),
	PADCONF(141, 5, 0x128, 0x268, 0x378, -1, 0, 0, 0, 0),
	PADCONF(142, 5, 0x128, 0x268, 0x378, -1, 4, 2, 2, 0),
	PADCONF(143, 5, 0x128, 0x268, 0x378, -1, 8, 4, 4, 0),
	PADCONF(144, 5, 0x128, 0x268, 0x378, -1, 12, 6, 6, 0),
	PADCONF(145, 5, 0x128, 0x268, 0x378, -1, 16, 8, 8, 0),
	PADCONF(146, 5, 0x128, 0x268, 0x378, -1, 20, 10, 10, 0),
	PADCONF(147, 5, 0x128, 0x268, 0x378, -1, 24, 12, 12, 0),
	PADCONF(148, 5, 0x128, 0x268, 0x378, -1, 28, 14, 14, 0),
	PADCONF(149, 7, 0x130, 0x270, -1, 0x480, 0, 0, 0, 0),
	PADCONF(150, 7, 0x130, 0x270, -1, 0x480, 4, 2, 0, 1),
	PADCONF(151, 7, 0x130, 0x270, -1, 0x480, 8, 4, 0, 2),
	PADCONF(152, 7, 0x130, 0x270, -1, 0x480, 12, 6, 0, 3),
	PADCONF(153, 7, 0x130, 0x270, -1, 0x480, 16, 8, 0, 4),
	PADCONF(154, 7, 0x130, 0x270, -1, 0x480, 20, 10, 0, 5),
	PADCONF(155, 7, 0x130, 0x270, -1, 0x480, 24, 12, 0, 6),
	PADCONF(156, 7, 0x130, 0x270, -1, 0x480, 28, 14, 0, 7),
	PADCONF(157, 7, 0x138, 0x278, -1, 0x480, 0, 0, 0, 8),
	PADCONF(158, 7, 0x138, 0x278, -1, 0x480, 4, 2, 0, 9),
	PADCONF(159, 5, 0x140, 0x280, 0x380, -1, 0, 0, 0, 0),
	PADCONF(160, 6, 0x140, 0x280, 0x380, -1, 4, 2, 2, 0),
	PADCONF(161, 5, 0x140, 0x280, 0x380, -1, 8, 4, 4, 0),
	PADCONF(162, 6, 0x140, 0x280, 0x380, -1, 12, 6, 6, 0),
	PADCONF(163, 6, 0x140, 0x280, 0x380, -1, 16, 8, 8, 0),
};

const struct xPinctrlDsMaInfo xPinctrlMa2dsMap[] = {
	{2, DS_16ST_0, DS_4WE_0, DS_M31_0, DS_NULL},
	{4, DS_16ST_1, DS_NULL, DS_M31_1, DS_NULL},
	{6, DS_16ST_2, DS_NULL, DS_NULL, DS_M31_0},
	{8, DS_16ST_3, DS_4WE_1, DS_NULL, DS_NULL},
	{10, DS_16ST_4, DS_NULL, DS_NULL, DS_M31_1},
	{12, DS_16ST_5, DS_NULL, DS_NULL, DS_NULL},
	{14, DS_16ST_6, DS_NULL, DS_NULL, DS_NULL},
	{16, DS_16ST_7, DS_4WE_2, DS_NULL, DS_NULL},
	{18, DS_16ST_8, DS_NULL, DS_NULL, DS_NULL},
	{20, DS_16ST_9, DS_NULL, DS_NULL, DS_NULL},
	{22, DS_16ST_10, DS_NULL, DS_NULL, DS_NULL},
	{24, DS_16ST_11, DS_NULL, DS_NULL, DS_NULL},
	{26, DS_16ST_12, DS_NULL, DS_NULL, DS_NULL},
	{28, DS_16ST_13, DS_4WE_3, DS_NULL, DS_NULL},
	{30, DS_16ST_14, DS_NULL, DS_NULL, DS_NULL},
	{32, DS_16ST_15, DS_NULL, DS_NULL, DS_NULL},
};

const struct xPinctrlDsInfo xPinctrlDsMap[] = {
	{ePAD_T_4WE_PD, DS_2BIT_MASK, DS_2BIT_IM_VAL},
	{ePAD_T_4WE_PU, DS_2BIT_MASK, DS_2BIT_IM_VAL},
	{ePAD_T_16ST, DS_4BIT_MASK, DS_4BIT_IM_VAL},
	{ePAD_T_M31_0204_PD, DS_1BIT_MASK, DS_1BIT_IM_VAL},
	{ePAD_T_M31_0204_PU, DS_1BIT_MASK, DS_1BIT_IM_VAL},
	{ePAD_T_M31_0610_PD, DS_1BIT_MASK, DS_1BIT_IM_VAL},
	{ePAD_T_M31_0610_PU, DS_1BIT_MASK, DS_1BIT_IM_VAL},
	{ePAD_T_AD, DS_NULL, DS_NULL},
};

/* Pull Register value map to status */
const struct xPinctrlMapData xPinctrlP4wePullV2s[] = {
	{P4WE_PULL_UP, PULL_UP},
	{P4WE_HIGH_HYSTERESIS, HIGH_HYSTERESIS},
	{P4WE_HIGH_Z, HIGH_Z},
	{P4WE_PULL_DOWN, PULL_DOWN},
};

const struct xPinctrlMapData xPinctrlP16stPullV2s[] = {
	{P16ST_PULL_UP, PULL_UP},
	{PD, PULL_UNKNOWN},
	{P16ST_HIGH_Z, HIGH_Z},
	{P16ST_PULL_DOWN, PULL_DOWN},
};

const struct xPinctrlMapData xPinctrlPm31PullV2s[] = {
	{PM31_PULL_DISABLED, PULL_DOWN},
	{PM31_PULL_ENABLED, PULL_UP},
};

const struct xPinctrlMapData xPinctrlPangdPullV2s[] = {
	{PANGD_PULL_UP, PULL_UP},
	{PD, PULL_UNKNOWN},
	{PANGD_HIGH_Z, HIGH_Z},
	{PANGD_PULL_DOWN, PULL_DOWN},
};

/* Pull status map to register value */
const struct xPinctrlMapData xPinctrlP4wePullS2v[] = {
	{PULL_UP, P4WE_PULL_UP},
	{HIGH_HYSTERESIS, P4WE_HIGH_HYSTERESIS},
	{HIGH_Z, P4WE_HIGH_Z},
	{PULL_DOWN, P4WE_PULL_DOWN},
	{PULL_DISABLE, -1},
	{PULL_ENABLE, -1},
};

const struct xPinctrlMapData xPinctrlP16stPullS2v[] = {
	{PULL_UP, P16ST_PULL_UP},
	{HIGH_HYSTERESIS, -1},
	{HIGH_Z, P16ST_HIGH_Z},
	{PULL_DOWN, P16ST_PULL_DOWN},
	{PULL_DISABLE, -1},
	{PULL_ENABLE, -1},
};

const struct xPinctrlMapData xPinctrlPm31PullS2v[] = {
	{PULL_UP, PM31_PULL_ENABLED},
	{HIGH_HYSTERESIS, -1},
	{HIGH_Z, -1},
	{PULL_DOWN, PM31_PULL_DISABLED},
	{PULL_DISABLE, -1},
	{PULL_ENABLE, -1},
};

const struct xPinctrlMapData xPinctrlPangdPullS2v[] = {
	{PULL_UP, PANGD_PULL_UP},
	{HIGH_HYSTERESIS, -1},
	{HIGH_Z, PANGD_HIGH_Z},
	{PULL_DOWN, PANGD_PULL_DOWN},
	{PULL_DISABLE, -1},
	{PULL_ENABLE, -1},
};

const struct xPinctrlPullInfo xPinctrlPullMap[] = {
	{ePAD_T_4WE_PD, P4WE_PULL_MASK, xPinctrlP4wePullV2s,
	 xPinctrlP4wePullS2v},
	{ePAD_T_4WE_PU, P4WE_PULL_MASK, xPinctrlP4wePullV2s,
	 xPinctrlP4wePullS2v},
	{ePAD_T_16ST, P16ST_PULL_MASK, xPinctrlP16stPullV2s,
	 xPinctrlP16stPullS2v},
	{ePAD_T_M31_0204_PD, PM31_PULL_MASK, xPinctrlPm31PullV2s,
	 xPinctrlPm31PullS2v},
	{ePAD_T_M31_0204_PU, PM31_PULL_MASK, xPinctrlPm31PullV2s,
	 xPinctrlPm31PullS2v},
	{ePAD_T_M31_0610_PD, PM31_PULL_MASK, xPinctrlPm31PullV2s,
	 xPinctrlPm31PullS2v},
	{ePAD_T_M31_0610_PU, PM31_PULL_MASK, xPinctrlPm31PullV2s,
	 xPinctrlPm31PullS2v},
	{ePAD_T_AD, PANGD_PULL_MASK, xPinctrlPangdPullV2s,
	 xPinctrlPangdPullS2v},
};

static uint32_t prvPinctrlConvertCurrentToDS( uint32_t uxType, uint32_t uxMa )
{
	uint32_t uxIdx;

	for( uxIdx = 0; uxIdx < ARRAY_SIZE( xPinctrlMa2dsMap ); uxIdx++ )
	{
		if( xPinctrlMa2dsMap[uxIdx].uxMa != uxMa )
			continue;

		if( uxType == ePAD_T_4WE_PD || uxType == ePAD_T_4WE_PU )
			return xPinctrlMa2dsMap[uxIdx].uxDs4we;
		else if( uxType == ePAD_T_16ST )
			return xPinctrlMa2dsMap[uxIdx].uxDs16st;
		else if( uxType == ePAD_T_M31_0204_PD || uxType == ePAD_T_M31_0204_PU )
			return xPinctrlMa2dsMap[uxIdx].uxDs0204m31;
		else if( uxType == ePAD_T_M31_0610_PD || uxType == ePAD_T_M31_0610_PU )
			return xPinctrlMa2dsMap[uxIdx].uxDs0610m31;
	}

	return DS_NULL;
}

static void prvPinctrlSetRegister( uint32_t uxPin, uint32_t uxRegAddr, uint8_t ucVal, uint8_t ucMask, uint8_t ucShift )
{
	if( uxPin >= ATLAS7_PINCTRL_BANK_0_PINS )
		uxRegAddr += _IOC_TOP_REG_BASE;
	else
		uxRegAddr += _IOC_RTC_REG_BASE;

	/* Clear register bits */
	SOC_REG( CLR_REG( uxRegAddr ) ) = ucMask << ucShift;

	/* Set new value to register */
	SOC_REG( uxRegAddr ) = ( ucVal & ucMask ) << ucShift;
}

static uint32_t prvPinctrlPmxPinAnalogEnable( uint32_t uxPin )
{
	const struct xPinctrlPadConfig *pxPad = &xPinctrlIocPadConfs[ uxPin ];

	/* Only ePAD_T_AD pins can change between Analogue&Digital */
	if( pxPad->eType != ePAD_T_AD )
		return -EINVAL;

	prvPinctrlSetRegister( uxPin, pxPad->uxAdCtrlReg, 0, ANA_CLEAR_MASK, pxPad->ucAdCtrlBit );

	return 0;
}

static int prvPinctrlPmxPinDigitalEnable( uint32_t uxPin )
{
	const struct xPinctrlPadConfig *pxPad = &xPinctrlIocPadConfs[ uxPin ];

	/* Other type pads are always digital */
	if( pxPad->eType != ePAD_T_AD )
		return 0;

	prvPinctrlSetRegister( uxPin, pxPad->uxAdCtrlReg, 1, ANA_CLEAR_MASK, pxPad->ucAdCtrlBit );

	return 0;
}

/*
 * uxPinctrlPinFuncSel:	set pin's work function
 * @uxPin:	the index of pin
 * @uxFunc:	the index of setted function
 *
 * set the pin@vPinIndex to function@vFuncIndex, the default function
 * is gpio function(FUNC_GPIO).
 */
uint32_t uxPinctrlPinFuncSel( uint32_t uxPin, uint32_t uxFunc )
{
	const struct xPinctrlPadConfig *pxPad = &xPinctrlIocPadConfs[ uxPin ];

	/* analog? */
	if( uxFunc == FUNC_ANALOGUE )
	{
		/* set it to analog function */
		return prvPinctrlPmxPinAnalogEnable( uxPin );
	}

	/* Set to digtal function */
	prvPinctrlPmxPinDigitalEnable( uxPin );

	/* Set target pad mux function */
	prvPinctrlSetRegister( uxPin, pxPad->uxMuxReg, uxFunc, FUNC_CLEAR_MASK, pxPad->ucMuxBit );

	return 0;
}

/*
 * uxPinctrlPinDsSel:	set pin's drive strength
 * @uxPin:	the index of pin
 * @uxMa:	the setted current(ma)
 *
 * first convert the setted current into specified bit value, write them
 * into related drive strength setting register. more info please refer to
 * include/soc_pinctrl.h "Drive-Strength" parts.
 */
uint32_t uxPinctrlPinDsSel( uint32_t uxPin, uint32_t uxMa )
{
	const struct xPinctrlPadConfig *pxPad = &xPinctrlIocPadConfs[ uxPin ];
	const struct xPinctrlDsInfo *pxDsInfo;
	uint32_t uxDsIndex;

	pxDsInfo = &xPinctrlDsMap[ pxPad->eType ];
	/* convert current to driver strength */
	uxDsIndex = prvPinctrlConvertCurrentToDS( pxPad->eType, uxMa );
	if( uxDsIndex & ( ~( pxDsInfo->ucMask ) ) )
		goto unsupport;

	prvPinctrlSetRegister( uxPin, pxPad->uxDrvstrReg, uxDsIndex, pxDsInfo->ucMask, pxPad->ucDrvstrBit );

	return 0;
unsupport:
	DebugMsg("Pad#%d type[%d] doesn't support ds code[%d]!\n", uxPin, pxPad->eType, uxDsIndex);
	return -EINVAL;
}

/*
 * uxPinctrlPinPullSel:	set pin's pull select
 * @uxPin:		the index of pin
 * @uxPullIndex:	the pull index of pin
 *
 * different type of pad have different pull sel function, more info
 * please refer to include/soc_pinctrl.h for "Definition of Pull Types" parts.
 */
uint32_t uxPinctrlPinPullSel( uint32_t uxPin, uint32_t uxPullIndex )
{
	const struct xPinctrlPadConfig *pxPad;
	const struct xPinctrlPullInfo *pxPullInfo;

	pxPad = &xPinctrlIocPadConfs[ uxPin ];
	pxPullInfo = &xPinctrlPullMap[ pxPad->eType ];

	prvPinctrlSetRegister( uxPin, pxPad->uxPupdReg, pxPullInfo->pxS2v[uxPullIndex].ucData, pxPullInfo->ucMask, pxPad->ucPupdBit );

	return 0;
}
