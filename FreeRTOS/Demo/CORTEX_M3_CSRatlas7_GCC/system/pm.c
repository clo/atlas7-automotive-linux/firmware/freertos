/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "croutine.h"

/* Library include files. */
#include "soc_irq.h"
#include "core_cm3.h"

/* platform include files */
#include "ctypes.h"
#include "io.h"
#include "utils.h"
#include "debug.h"

#include "soc_ipc.h"
#include "soc_uart.h"
#include "soc_pwrc.h"
#include "soc_pwrc_retain.h"
#include "soc_clkc.h"
#include "soc_memc.h"
#include "soc_timer.h"
#include "soc_sysrtc.h"
#include "soc_iobridge.h"
#include "soc_ioc.h"
#include "soc_uart.h"
#include "soc_can.h"
#include "soc_cache.h"
#include "pm.h"
#include "soc_gpio.h"
#include "qspi.h"
#include "canbus.h"
#include "loader.h"
#include "sirfdrive3.h"
#include "gpt.h"
#include "audio/audio_protocol.h"
#include "audio/audio.h"

#define configSYSTICK_CLOCK_HZ			configCPU_CLOCK_HZ
#define portNVIC_SYSTICK_CTRL_REG		( * ( ( volatile uint32_t * ) 0xe000e010 ) )
#define portNVIC_SYSTICK_LOAD_REG		( * ( ( volatile uint32_t * ) 0xe000e014 ) )
#define portNVIC_SYSTICK_CURRENT_VALUE_REG	( * ( ( volatile uint32_t * ) 0xe000e018 ) )
#define portNVIC_SYSTICK_ENABLE_BIT		( 1UL << 0UL )
#define portNVIC_SYSTICK_COUNT_FLAG_BIT		( 1UL << 16UL )
#define portNVIC_SYSTICK_CLK_BIT		( 1UL << 2UL )

static uint32_t g_uxPmLevel = PM_NORMAL;
static SemaphoreHandle_t xPmManagerSemaphore;
extern bool m3_audio_support;

extern void clock_init(void);
extern void clk_src_late_init(void);
extern int ddr_init(void);

void __on_retain vNoOsDelayTicks( uint32_t uxTicks )
{
	uint32_t uxIdx;

	for( uxIdx = 0; uxIdx < uxTicks; uxIdx++ )
	{
		portNVIC_SYSTICK_LOAD_REG = ( configSYSTICK_CLOCK_HZ / configTICK_RATE_HZ ) - 1UL;
		portNVIC_SYSTICK_CTRL_REG = ( portNVIC_SYSTICK_CLK_BIT | portNVIC_SYSTICK_ENABLE_BIT );
		while ( !( portNVIC_SYSTICK_CTRL_REG & portNVIC_SYSTICK_COUNT_FLAG_BIT ) ) {} ;
		portNVIC_SYSTICK_CTRL_REG = 0;
	}
}

void __on_retain vChangeCpuClock( uint32_t uxClk )
{
	volatile uint32_t uxRtcPllCtl, uxRtcPllCtlOld;

#ifndef __ATLAS7_STEP_B__
	if( uxClk != configCPU_CLOCK_HZ )
		return;

	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	uxRtcPllCtl = uxRtcPllCtlOld;
	uxRtcPllCtl &= ( ~( 0x3FFF << 1 ) );
	uxRtcPllCtl |= ( 0x1404 << 1 ); /* 168MHZ */
	vIoBridgeWrite( A7DA_PWRC_RTC_PLL_CTRL, uxRtcPllCtl );
	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	if( ( ( uxRtcPllCtlOld >> 1 ) & 0x3FFF ) != 0x1404 )
	{
		return;
	}

	uxRtcPllCtl = uxRtcPllCtlOld;
	uxRtcPllCtl &= ( ~( 0x3FFF << 1 ) );
	uxRtcPllCtl |= ( 0x14FB << 1 ); /* 176MHZ */
	vIoBridgeWrite( A7DA_PWRC_RTC_PLL_CTRL, uxRtcPllCtl );
	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	if( ( ( uxRtcPllCtlOld >> 1 ) & 0x3FFF ) != 0x14FB )
	{
		return;
	}

	uxRtcPllCtl = uxRtcPllCtlOld;
	uxRtcPllCtl &= ( ~( 0x3FFF << 1 ) );
	uxRtcPllCtl |= ( 0x15EC << 1 ); /* 184MHZ */
	vIoBridgeWrite( A7DA_PWRC_RTC_PLL_CTRL, uxRtcPllCtl );
	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	if( ( ( uxRtcPllCtlOld >> 1 ) & 0x3FFF ) != 0x15EC )
	{
		return;
	}

	uxRtcPllCtl = uxRtcPllCtlOld;
	uxRtcPllCtl &= ( ~( 0x3FFF << 1 ) );
	uxRtcPllCtl |= ( 0x16E0 << 1 ); /* 192MHZ */
	vIoBridgeWrite( A7DA_PWRC_RTC_PLL_CTRL, uxRtcPllCtl );
	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	if( ( ( uxRtcPllCtlOld >> 1 ) & 0x3FFF ) != 0x16E0 )
	{
		return;
	}
#else
	uint32_t uxDiv;
	uxDiv = uxClk / 1000000 * 1000000 / 32768;

	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	uxRtcPllCtl = uxRtcPllCtlOld;
	uxRtcPllCtl &= ( ~( 0x3FFF << 1 ) );
	uxRtcPllCtl |= ( uxDiv << 1 );
	vIoBridgeWrite( A7DA_PWRC_RTC_PLL_CTRL, uxRtcPllCtl );
	uxRtcPllCtlOld = uxIoBridgeRead( A7DA_PWRC_RTC_PLL_CTRL );
	if( ( ( uxRtcPllCtlOld >> 1 ) & 0x3FFF ) != uxDiv )
	{
		return;
	}
	/* re-configure os tick as Timer clock change */
	vOsTimerInit(uxClk,uxOsGetCurrentTick());
#endif
}

#define RESET_LATCH_FLAG	0x3C1A
#define RESET_LATCH_STANDALONE	0x00BA
uint32_t __on_retain uxPmCheckBootMode( void )
{
#ifdef __M3_INDEPENDENT__
	/* The Reset Latch is invalid, treat as dependent mode */
	if( SOC_REG( PWRC_RESET_LATCH_REG ) & BIT4 )
		return PM_BOOT_INDEPENDENT;
#endif
	return PM_BOOT_DEPENDENT;
}

uint32_t __on_retain uxPmCheckWakeupSource( void )
{
	uint32_t uxVal;

	uxVal = uxIoBridgeRead( A7DA_PWRC_PON_STATUS );
	if( uxVal & PWRC_PON_CAN_BOOT )
		return PM_WAKE_CAN;

	if( uxVal & PWRC_PON_A7_WARM_RST_BOOT )
	{
		if( uxVal & PWRC_PON_WATCH_DOG_BOOT_BITS )
			return PM_WAKE_WDOG;

		return PM_WAKE_WARM_BOOT;
	}

	return PM_WAKE_COLD_BOOT;
}

void __on_retain vPmResetModule( uint32_t uxVal )
{
	uint32_t imask = uxIoBridgeLock();
	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_SW_RSTC_CLR, uxVal );
	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_SW_RSTC_SET, uxVal );
	vIoBridgeUnlock(imask);
}

void __on_retain vPmClearWakeupSource( uint32_t uxVal )
{
	uint32_t imask = uxIoBridgeLock();
	uint32_t uxReg = uxIoBridgeReadUnsafe( A7DA_PWRC_PON_STATUS );
	uxReg &= ~uxVal;
	vIoBridgeWriteUnsafe( A7DA_PWRC_PON_STATUS, uxReg );
	vIoBridgeUnlock(imask);
}

void __on_retain vNocClkReconnect( void )
{
	uint32_t imask = uxIoBridgeLock();
	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_REGI_IDLE_REQ );

	while ( ( uxIoBridgeReadUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR ) & PWRC_NOC_REGI_IDLE ) != 0 );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_REGT_IDLE_REQ );

	while ( ( uxIoBridgeReadUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR) & PWRC_NOC_REGT_IDLE ) != 0 );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_DRAMT_IDLE_REQ );

	while ( ( uxIoBridgeReadUnsafe(A7DA_PWRC_RTC_NOC_PWRCTL_CLR) & PWRC_NOC_DRAMT_IDLE ) != 0 );
	vIoBridgeUnlock(imask);
}

/* reconnect RTCM's NOC, don't wait for the idle signals to clear. */
void __on_retain vNocClkReconnectAndNoWait( void )
{
	uint32_t imask = uxIoBridgeLock();
	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_REGI_IDLE_REQ );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_REGT_IDLE_REQ );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_CLR, PWRC_NOC_DRAMT_IDLE_REQ );
	vIoBridgeUnlock(imask);
}

/* disconnect RTCM's NOC */
void __on_retain vNocClkDisconnect( void )
{
	uint32_t imask = uxIoBridgeLock();
	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET, PWRC_NOC_REGI_IDLE_REQ );

	while ( !( ( uxIoBridgeReadUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET ) ) & PWRC_NOC_REGI_IDLE ) );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET, PWRC_NOC_REGT_IDLE_REQ );

	while ( !( uxIoBridgeReadUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET) & PWRC_NOC_REGT_IDLE ) );

	vIoBridgeWriteUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET, PWRC_NOC_DRAMT_IDLE_REQ );

	while ( !( uxIoBridgeReadUnsafe( A7DA_PWRC_RTC_NOC_PWRCTL_SET) & PWRC_NOC_DRAMT_IDLE ) );
	vIoBridgeUnlock(imask);
}

void vPmEnableCanWakeup( uint32_t uxType )
{
	switch( uxType )
	{
	case PM_CAN_PARTIAL:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_LISTENER0:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_LIST0_EN | PWRC_CAN_PRE_WAKEUP_CAN0 | PWRC_CAN_PRE_WAKEUP_MODE_M3PD | PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_LISTENER1:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_LIST1_EN | PWRC_CAN_PRE_WAKEUP_CAN1 | PWRC_CAN_CAN0_SEL_TRANSCEIVER_1 | PWRC_CAN_PRE_WAKEUP_MODE_M3PD | PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_CONTROLLER0:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_LIST0_EN | PWRC_CAN_PRE_WAKEUP_CAN0 | PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_CONTROLLER1:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_LIST1_EN | PWRC_CAN_PRE_WAKEUP_CAN1 | PWRC_CAN_CAN0_SEL_TRANSCEIVER_1 | PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_CONTROLLER0_OR_1:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_LIST0_EN | PWRC_CAN_LIST1_EN | PWRC_CAN_PRE_WAKEUP_CAN0 | PWRC_CAN_PRE_WAKEUP_CAN1 | PWRC_CAN_CAN0_SEL_TRANSCEIVER_AUTO | PWRC_CAN_WAKEUP_EN );
		break;
	case PM_CAN_WAKEUP_TRANSCEIVER0:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_PRE_WAKEUP_TRANSCEIVER | PWRC_CAN_WAKEUP_EN | PWRC_CAN_PRE_WAKEUP_MODE_M3PD );
		vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_0, 0 );
		if(FALSE==ENABLE_M3_CAN1_WAKUP())
		{
			vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_2, 1 );
		}
		break;
	case PM_CAN_WAKEUP_TRANSCEIVER1:
		vIoBridgeWrite( A7DA_PWRC_RTC_CAN_CTRL, PWRC_CAN_PRE_WAKEUP_TRANSCEIVER | PWRC_CAN_EXT_IRQ_CONNECT_CAN1 | PWRC_CAN_WAKEUP_EN | PWRC_CAN_CAN0_SEL_TRANSCEIVER_1 | PWRC_CAN_PRE_WAKEUP_MODE_M3PD );
		vGpioSetDirectionOutput( eGpioRtcm, GPIO_RTC_0, 0 );		
		break;
	default:
		DebugMsg( "can not support such type.\r\n" );
	}
}

void __on_retain vM3WakeupA7( void )
{
	uint32_t uxReg;

	uint32_t imask = uxIoBridgeLock();
	uxReg = uxIoBridgeReadUnsafe( A7DA_PWRC_FSM_M3_CTRL );

	uxReg &= ~BIT30;	/* clear M3 power off bit */
	uxReg |= BIT31;		/* set wakeup bit */

	vIoBridgeWriteUnsafe( A7DA_PWRC_FSM_M3_CTRL, uxReg );
	vIoBridgeUnlock(imask);
}

/* SW_M3_POWER_OFF */
void __on_retain vM3SwPowerOff( void )
{
	vIoBridgeWrite( A7DA_PWRC_FSM_M3_CTRL, uxIoBridgeRead( A7DA_PWRC_FSM_M3_CTRL ) | BIT30 );
	while(1);
}

void __on_retain vWakeUpEntry( void )
{
#ifdef __PIC_ENABLE__
	asm volatile(
		"ldr sp, =_stack_top_addr \n\t"
		"ldr r9, =_m3_got_table_addr \n\t"
	);
#else
	asm volatile(
		"ldr sp, =_stack_top_addr \n\t"
	);
#endif

	uint32_t uxBootMode;
	uint32_t uxWakeupSrc;

	/* pull configurations for low_bat_ind */
	SOC_REG(A7DA_IOCRTC_PULL_EN_0_REG_SET) = BIT8;
	/* set RTC_LDO to Power Down Mode (LPM) disabled */
	vIoBridgeWrite( A7DA_PWRC_PDN_CTRL_CLR, BIT20 );

	uxBootMode = uxPmCheckBootMode();
	uxWakeupSrc = uxPmCheckWakeupSource();
	if( uxWakeupSrc == PM_WAKE_CAN )
	{
		portCHAR cRet;

		/* Call CAN wakeup handler */
		cRet = xCanResume();
		/* Noting todo, back to sleep */
		if( cRet == PM_WAKEUP_CANBUS_MODE_GOTO_SLEEP )
		{
			/* Reset CAN listener*/
			vPmResetModule( PWRC_RTC_SW_RESTC_CAN_LIST0_RESET );
			/* Reset Power ON Status Register */
			vPmClearWakeupSource( 0xFFFFFFFF );
			vM3SwPowerOff();
		}
		/* Need A7 to support */
		else if( cRet == PM_WAKEUP_CANBUS_MODE_WAKEUP_A7 || cRet == PM_WAKEUP_CANBUS_MODE_TRIGGER_REARVIEW ||
			( cRet == PM_WAKEUP_CANBUS_MODE_WAKEUP_M3 && uxBootMode == PM_BOOT_DEPENDENT ) )
		{
			vM3WakeupA7();
		}
	}

	if( PM_BOOT_DEPENDENT == uxBootMode )
	{
		while ( M3_WAKEUP_FLAG_VALID != clkc_get_m3_wakeup_flag() )
			__WFI();
	}
	else
	{
		vChangeCpuClock( configCPU_CLOCK_HZ );
		vQspiEnableXipFromWakeup();
	}

	asm volatile (
		"mov r3, %0\n\t"
		"orr r3, r3, #1\n\t"
		"bx  r3\n\t"
		:
		:"r"(MEM(PM_ARG_BOOT_ENTRY))
		: "r3"
	);
}

/* power-off without retention */
static void __on_retain prvStartPsavingAndHibernation(void)
{
	vIoBridgeWrite( A7DA_PWRC_PDN_CTRL_SET, BIT0 );
}

/* power-off with retention */
static void __on_retain prvStartPsavingAndDeepSleep(void)
{
	uint32_t imask = uxIoBridgeLock();
	vIoBridgeWriteUnsafe( A7DA_PWRC_PDN_CTRL_SET, BIT1 );
	vIoBridgeWriteUnsafe( A7DA_PWRC_PDN_CTRL_SET, BIT0 );
	vIoBridgeUnlock(imask);
}

uint32_t __on_retain uxGetPmLevel(void)
{
	return g_uxPmLevel;
}

void __on_retain vGotoPowerSavingMode( uint32_t uxPmLevel )
{
	uint32_t uxPwrcCanStatus;
#ifdef CONFIG_ENABLE_CANBUS_DEMO
	CAN_FILTEROBJECT Filter;

	if( (uxPmLevel == PM_DEEP_SLEEP) || (uxPmLevel == PM_HIBERNATION) )
	{
		#if 0	/* any message */
		Filter.amr.l = 0xffffffff;
		Filter.acr.l = 0;
		#else
		/* only wakeup by standard message from range 0 to 0xFF */
		Filter.amr.l = ~((0x700<<21)|BIT2|BIT1);
		Filter.acr.l = 0;
		#endif
		Filter.amcr_d.mask = 0xffff;
		Filter.amcr_d.code = 0;

		xCanSuspend( CAN_PORT0, &Filter, 0 );
	}
#endif

	g_uxPmLevel = uxPmLevel;

	if( (uxPmLevel == PM_HIBERNATION_M3) || (uxPmLevel == PM_DEEP_SLEEP_M3) )
	{
		/* before going into power saving mode, need to set uart0_rx pin-mux as
		* uart0 rx function 0x1, that is because A7 kernel usb0 has used it as gpio
		* function which make uart0 occur rx receive interrupt continously,  which
		* will lead M3 uart0 read function to access uart0 register, but A7 will
		* power down bellow, if M3 still access uart0 register, which will make
		* M3 get stuck*/
		SOC_REG(A7DA_IOCTOP_FUNC_SEL_19_REG_CLR) = 0x77;
		SOC_REG(A7DA_IOCTOP_FUNC_SEL_19_REG_SET) = 0x11;
	}

	PWRC_CLEAR_CLOCK_INITIALIZED_FLAG();
	PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG();
	PWRC_CLEAR_DDR_INITIALIZED_FLAG();
	vDcacheFlush();

	SirfDrive3_DeInit();

	if( (uxPmLevel == PM_DEEP_SLEEP) || (uxPmLevel == PM_HIBERNATION) )
	{
		uxPwrcCanStatus = uxIoBridgeRead( A7DA_PWRC_RTC_CAN_CTRL);

		if(0 != (uxPwrcCanStatus&(PWRC_CAN_CAN0_SEL_TRANSCEIVER_1|PWRC_CAN_CAN0_SEL_TRANSCEIVER_AUTO)))
		{
			SOC_REG(A7DA_IOCRTC_FUNC_SEL_0_REG_CLR) = SHIFT_L(FUNC_MASK, 8) | SHIFT_L(FUNC_MASK, 12);
			SOC_REG(A7DA_IOCRTC_FUNC_SEL_0_REG_SET) = SHIFT_L(0x5, 8) | SHIFT_L(0x5, 12);
			SOC_REG(A7DA_IOCRTC_IN_DISABLE_2_REG_CLR) = BIT9;
			SOC_REG(A7DA_IOCRTC_IN_DISABLE_0_REG_SET) = BIT4;
		}

		SOC_REG( A7DA_CANBUS0_INT_EN ) = CANMOD3_INT_GLOBAL|CANMOD3_INT_RX_MSG;
		SOC_REG( A7DA_CANBUS0_INT_STATUS ) = CANMOD3_INT_MASK;
	}

	if( (uxPmLevel == PM_DEEP_SLEEP) || (uxPmLevel == PM_DEEP_SLEEP_M3) )
	{
		/* make ddr enter self-reflesh */
		clkc_set_ddrm_idle();
		memc_set_sleep_state();
	}
	if( (uxPmLevel == PM_DEEP_SLEEP) || (uxPmLevel == PM_HIBERNATION) )
	{
		/* pull configurations */
		SOC_REG(A7DA_IOCRTC_PULL_EN_0_REG_CLR) = BIT8;
		/* set RTC_LDO to Power Down Mode (LPM) */
		vIoBridgeWrite( A7DA_PWRC_PDN_CTRL_SET, BIT20 );
		/* put M3 memory BANK1&BANK4 to deep sleep state when in PSAVING_MODE
		 * put M3 memory BANK2&BANK3 to shutdown state when in PSAVING_MODE
		 * FIXME: if BANK2&BANK3 are used, please set to deep sleep state
		 */
		vIoBridgeWrite(A7DA_PWRC_M3_MEMORIES,0x8cc8);
		vIoBridgeWrite(A7DA_PWRC_CAN0_MEMORY,0x8);
		vIoBridgeWrite(A7DA_PWRC_RTC_GNSS_MEMORY,0x8);

		/* QSPI clock turn off */
		vIoBridgeWrite(A7DA_PWRC_SPI0_CLK_EN, 0x0 );
	}

#ifndef __M3_TINY_LOADER_USED__
	SOC_REG(PWRC_M3_WAKEUP_ADDR) = (uint32_t)vWakeUpEntry;
#endif

	vNocClkDisconnect();

	if( (uxPmLevel == PM_HIBERNATION) || (uxPmLevel == PM_HIBERNATION_M3) )
		prvStartPsavingAndHibernation();
	else
		prvStartPsavingAndDeepSleep();

	while ( uxIoBridgeRead(A7DA_PWRC_FSM_STATE) != PWRC_FSM_PSAVING_MODE_M3 );

#ifndef __ATLAS7_STEP_B__
	vNocClkReconnectAndNoWait();
#endif

	if( (uxPmLevel == PM_HIBERNATION) || (uxPmLevel == PM_DEEP_SLEEP) )
	{
		/* Reset can listener before sleep */
		vPmResetModule(PWRC_RTC_SW_RESTC_CAN_LIST0_RESET);

		/* Reset Power ON Status Register */
		vPmClearWakeupSource(0xFFFFFFFF);

		vM3SwPowerOff();
	}
}

bool m3_audio_reset = false;

#if defined(__ATLAS7_STEP_B__) && defined(__M3_LOADER_ENABLE__)
void __on_retain vGotoPowerResetA7( void )
{
	vLoaderStartup();

	clkc_check_a7_ready_flags();
	/*
	 * Because A7 has been reset, the IO on core domain has been reset too,
	 * in order to continue print debug message on M3, we have to re-initialize
	 * Debug UART here.
	 */
	vUartDeviceReHwInitialize( UART0 );
}
#else
#define A7_HAND_SHAKE_TIMEOUT 500 /* 500 ms */
static void m3_audio_termination(void)
{
	stop_all_streams();
}

void __on_retain vGotoPowerResetA7( void )
{
	int i;
	uint32_t imask;
	uint32_t timeout;

	if (m3_audio_support)
		m3_audio_termination();

#ifndef __ATLAS7_STEP_B__

	SOC_REG(A7DA_CLKC_RSTC_SREFRESN_EN) = 0;

	/* set IO clock to 26MHz  */
	SOC_REG(A7DA_CLKC_I2S_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_USBPHY_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_BTSS_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_RGMII_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_CPU_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SDPHY01_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SDPHY23_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SDPHY45_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SDPHY67_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_CAN_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_DEINT_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_NAND_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_DISP0_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_DISP1_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_GPU_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_GNSS_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_IO_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_G2D_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_JPENC_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_VDEC_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_GMAC_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_USB_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_KAS_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SEC_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_SDR_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_VIP_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_NOCD_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_NOCR_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_TPIU_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_TEST_CLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_TRG_REFCLK_SEL) = 0;
	SOC_REG(A7DA_CLKC_MEMPLL_AB_CTRL0) = 0x111;
	SOC_REG(A7DA_CLKC_SYS_CLK_SEL) = 0;
#endif

	SirfDrive3_DeInit();

	vTaskRemoteprocDelete();

	do {
		PWRC_CLEAR_CLOCK_INITIALIZED_FLAG();
		PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG();
		PWRC_CLEAR_DDR_INITIALIZED_FLAG();
		vDcacheFlush();

		/* enter iobridge protect section */
		imask = uxIoBridgeLock();
		vIoBridgeWriteUnsafe(0x3038, 0x49);

		/* wait for ack */
		while (( uxIoBridgeReadUnsafe(0x3038) & 0x1ff) != 0x1ff);

		/* Start reset */
		vIoBridgeWriteUnsafe(0x3070, 0x2);

		for(i = 0; i < 1000; i++)
		;
		vIoBridgeWriteUnsafe(0x303c, 0x49);

		/* End reset */
		vIoBridgeWriteUnsafe(0x306c, 0x2);
		vIoBridgeUnlock(imask);

		vUsWait(1000); /* Wait for 1ms for A7 is ready*/
		/* clkc_check_a7_ready_flags(); */
		timeout = 0;
		while( (SOC_REG(A7DA_CLKC_CLKC_SW_REG5) != BOOT_TEST_A7_READY) &&
				(timeout <= A7_HAND_SHAKE_TIMEOUT) )
		{
			timeout ++;
			vUsWait(1000);
		}

	} while(timeout > A7_HAND_SHAKE_TIMEOUT);
	/*
	 * Because A7 has been reset, the IO on core domain has been reset too,
	 * in order to continue print debug message on M3, we have to re-initialize
	 * Debug UART here.
	 */
	vUartDeviceReHwInitialize( UART0 );

	if (m3_audio_support) {
		m3_audio_reset = true;
		audio_init();
	}

	SirfDrive3_Init();

	vTaskRemoteprocCreate();
}
#endif /* #if defined(__ATLAS7_STEP_B__) && defined(__M3_LOADER_ENABLE__) */

/* Reset QSPI Flash device */
static void __on_retain prvResetNorFlash( void )
{
	uint32_t gpio4_save = SOC_REG(A7DA_GPIO_RTCM_CFG_4);
	uint32_t gpio8_save = SOC_REG(A7DA_GPIO_RTCM_CFG_8);

	/* Make sure all QSPI pins are configured as GPIO */
	SOC_REG(A7DA_IOCRTC_FUNC_SEL_0_REG_CLR) = 0x00070000;
	SOC_REG(A7DA_IOCRTC_FUNC_SEL_1_REG_CLR) = 0x77770000;
	SOC_REG(A7DA_IOCRTC_FUNC_SEL_2_REG_CLR) = 0x00000077;

	/* Drive LDO_EN and nCS low */
	SOC_REG(A7DA_GPIO_RTCM_CFG_4) = 1 << 5;
	SOC_REG(A7DA_GPIO_RTCM_CFG_8) = 1 << 5;

	vUsWait(2000);

	/* Drive LDO_EN and nCS high */
	SOC_REG(A7DA_GPIO_RTCM_CFG_8) = 3 << 5;
	SOC_REG(A7DA_GPIO_RTCM_CFG_4) = 3 << 5;

	/* Wait for NOR chip ready */
	vUsWait(2000);

	SOC_REG(A7DA_GPIO_RTCM_CFG_4) = gpio4_save;
	SOC_REG(A7DA_GPIO_RTCM_CFG_8) = gpio8_save;
}

void __on_retain vPmResetNorFlash(void)
{
    prvResetNorFlash();
}

static void __on_retain prvPowerManagementHandler( void )
{
	volatile uint32_t uxReg;
	uint32_t uxEvent;

	/*
	 * Disable System interrupts,
	 * to avoid intr handlers on DRAM to be run
	 */
	portDISABLE_INTERRUPTS();

	/* read power management operation code */
	uxReg = SOC_REG( PWRC_PM_IPC_OPCODE );
	uxEvent = uxReg & PM_IPC_CODE_MASK;

	switch( uxEvent )
	{
	case PM_HIBERNATION:
	case PM_HIBERNATION_M3:
	case PM_DEEP_SLEEP:
	case PM_DEEP_SLEEP_M3:
		DebugMsg("Going to power saving..%d\r\n", uxEvent);
		vGotoPowerSavingMode( uxEvent );
		break;

	case PM_RTC_WAKEUP:
		atlas7_m3_rtc_config();
		break;

	case PM_RESET:
		DebugMsg("Going to reset...\r\n");
		vGotoPowerResetA7();
		break;

	case PM_FORCE_SHUTDOWN:
		DebugMsg("Going to shutdown...\r\n");
		/* Using hibernation to simulate shutdown */
		vGotoPowerSavingMode( PM_HIBERNATION );
		break;
	case PM_M3_NOC_DISCONNECT:
		DebugMsg("m3 noc clock disconnect and reconnect.\r\n");
		vNocClkDisconnect();
		vNocClkReconnectAndNoWait();
		break;
	case PM_RESET_M3:
		DebugMsg("Going to reset m3...\r\n");
		vNocClkDisconnect();
		vNocClkReconnectAndNoWait();
		prvResetNorFlash();
		vIoBridgeWrite(A7DA_PWRC_RTC_SW_RSTC_CLR, PWRC_RESET_M3 );
		while(1);
		break;
	case PM_M3_RCV_HOLD:
		DebugMsg("Going to recover, m3 hold...\r\n");
		vQspiSwitchHost();
		SOC_REG( PWRC_PM_IPC_OPCODE ) = 0x11223344;
		while( 0x11223344 == SOC_REG( PWRC_PM_IPC_OPCODE ))
		{
			uartWriteByte('*');
			vUsWait(1000000);
		}
		uartWriteByte('\r');
		uartWriteByte('\n');
		uartWriteByte('R');
		uartWriteByte('E');
		uartWriteByte('S');
		uartWriteByte('E');
		uartWriteByte('T');
		uartWriteByte('\r');
		uartWriteByte('\n');
		prvResetNorFlash();
		/* reset A7 and M3 at the same time by BIT2 MIX_RESET_B */
		vIoBridgeWrite(A7DA_PWRC_RTC_SW_RSTC_CLR, BIT2 );
		while(1);
		break;

	default:
		DebugMsg("Unknown Power Management opcode:%d\r\n", uxReg);
		break;
	}

	portENABLE_INTERRUPTS();
}
void INT_IPC_TRGT2_INIT1_INTR2_Handler( void )
{
	volatile uint32_t uxReg;
	uint32_t uxEvent;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	NVIC_DisableIRQ(INT_IPC_TRGT2_INIT1_INTR2_IRQn);

	/* read register to clear the interrupt */
	uxReg = SOC_REG( A7DA_IPC_TRGT2_INIT1_2 );

	/* read power management operation code */
	uxReg = SOC_REG( PWRC_PM_IPC_OPCODE );
	uxEvent = uxReg & PM_IPC_CODE_MASK;

	if(PM_RESET == uxEvent)
	{	/* reset of A7 should be handled in Task level */
		xSemaphoreGiveFromISR(xPmManagerSemaphore,&xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else
	{
		prvPowerManagementHandler();
		NVIC_EnableIRQ(INT_IPC_TRGT2_INIT1_INTR2_IRQn);
	}
}

/* IPC_TRGT2_INIT0_INTR2 from U-Boot "reset" command */
void INT_IPC_TRGT2_INIT0_INTR2_Handler( void )
{
	volatile uint32_t uxReg;
	uint32_t uxEvent;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	NVIC_DisableIRQ(INT_IPC_TRGT2_INIT0_INTR2_IRQn);

	/* read register to clear the interrupt */
	uxReg = SOC_REG( A7DA_IPC_TRGT2_INIT0_2 );

	/* read power management operation code */
	uxReg = SOC_REG( PWRC_PM_IPC_OPCODE );
	uxEvent = uxReg & PM_IPC_CODE_MASK;

	if(PM_RESET == uxEvent)
	{	/* reset of A7 should be handled in Task level */
		xSemaphoreGiveFromISR(xPmManagerSemaphore,&xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
	else
	{
		prvPowerManagementHandler();
		NVIC_EnableIRQ(INT_IPC_TRGT2_INIT0_INTR2_IRQn);
	}
}

void vPmWdgHandler( eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	SOC_REG( PWRC_PM_IPC_OPCODE ) = PM_RESET;

	xSemaphoreGiveFromISR(xPmManagerSemaphore,&xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

void vPmPowerModeManager(void)
{	/* This should be called by startup task only for the manage of power mode
 	 * in task level. */
	xPmManagerSemaphore = xSemaphoreCreateBinary();

	/* Enable IPC interrupt for PM */
	NVIC_SetPriority( INT_IPC_TRGT2_INIT1_INTR2_IRQn, API_SAFE_INTR_PRIO_0 );
	NVIC_EnableIRQ( INT_IPC_TRGT2_INIT1_INTR2_IRQn );
	NVIC_SetPriority( INT_IPC_TRGT2_INIT0_INTR2_IRQn, API_SAFE_INTR_PRIO_0 );
	NVIC_EnableIRQ( INT_IPC_TRGT2_INIT0_INTR2_IRQn );
	for ( ; ; )
	{
		/* wait isr to release semaphore */
		if (xSemaphoreTake(xPmManagerSemaphore, portMAX_DELAY) == pdTRUE)
		{
			prvPowerManagementHandler();

			NVIC_EnableIRQ( INT_IPC_TRGT2_INIT1_INTR2_IRQn );
			NVIC_EnableIRQ( INT_IPC_TRGT2_INIT0_INTR2_IRQn );
		}
	}
}
