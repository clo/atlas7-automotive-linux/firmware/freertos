/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* Library include files. */
#include "soc_irq.h"
#include "core_cm3.h"

/* platform include files */
#include "ctypes.h"
#include "io.h"
#include "utils.h"
#include "debug.h"

#include "pulsec.h"

#include "soc_clkc.h"
#include "soc_ioc.h"
#include "soc_pulsec.h"
#include "soc_timer.h"

#define PULSEC_ENABLE			BIT(0)
#define PULSEC_LPRE_EN			BIT(1)
#define PULSEC_RPRE_EN			BIT(2)
#define PULSEC_LEFT_INV         BIT(3)
#define PULSEC_RIGHT_INV        BIT(4)
#define PULSEC_DE_GLITCH_EN     BIT(5)
#define PULSEC_FORWARD_LOW		BIT(6)

#define PULSEC_LW_OVERFLOW_EN   BIT(0)
#define PULSEC_LW_UNDERFLOW_EN	BIT(1)
#define PULSEC_RW_OVERFLOW_EN   BIT(2)
#define PULSEC_RW_UNDERFLOW_EN	BIT(3)

#define PULSEC_LW_OVERFLOW	   	BIT(0)
#define PULSEC_LW_UNDERFLOW		BIT(1)
#define PULSEC_RW_OVERFLOW   	BIT(2)
#define PULSEC_RW_UNDERFLOW		BIT(3)

void vPulseCounterInit(void)

{
	#ifdef __ATLAS7_STEP_B__
	SOC_REG(A7DA_CLKC_GNSS_LEAF_CLK_EN_SET) = BIT(3);		/* enable clock */
	#else
	SOC_REG(A7DA_CLKC_LEAF_CLK_EN1_SET) = BIT(2);		/* enable clock */
	#endif

	SOC_REG(A7DA_IOCTOP_FUNC_SEL_16_REG_CLR) = SHIFT_L(FUNC_MASK, 0)|SHIFT_L(FUNC_MASK, 4);
	SOC_REG(A7DA_IOCTOP_FUNC_SEL_16_REG_SET) = SHIFT_L(BIT(1), 0)|SHIFT_L(BIT(1), 4); /* set GPIO_0 pin as ps.odo_0, GPIO_1 as ps.dr_dir */

	SOC_REG(A7DA_PULSEC_INT_EN) = PULSEC_LW_OVERFLOW_EN | PULSEC_LW_UNDERFLOW_EN | PULSEC_RW_OVERFLOW_EN | PULSEC_RW_UNDERFLOW_EN;

	NVIC_SetPriority( INT_PULSEC_IRQn, API_SAFE_INTR_PRIO_0 );

}

void vPulseCounterStart(u32 direction)
{

	u32 reg;

	SOC_REG(A7DA_PULSEC_LWH_PRE_CNT) = 0x00;

	SOC_REG(A7DA_PULSEC_RWH_PRE_CNT) = 0x00;

	reg = SOC_REG(A7DA_PULSEC_CTRL);

	reg |= PULSEC_LPRE_EN|PULSEC_RPRE_EN;

	SOC_REG(A7DA_PULSEC_CTRL) = reg;

	//vUsWait(1000);

	reg = SOC_REG(A7DA_PULSEC_CTRL);

	/*Must disable R_PRE_EN and L_PRE_EN when start sample, otherwise can't sample*/

	reg &= ~(PULSEC_LPRE_EN|PULSEC_RPRE_EN);

	reg |= PULSEC_ENABLE;

	if (direction == SIRFSOC_PULSEC_BACKWARD)
		reg = reg & (~PULSEC_FORWARD_LOW);
	else
		reg = reg | PULSEC_FORWARD_LOW;

	SOC_REG(A7DA_PULSEC_CTRL) = reg;

	NVIC_EnableIRQ( INT_PULSEC_IRQn );

}

void INT_PULSEC_Handler(void)
{
	u32 uxIntStatus;

	NVIC_DisableIRQ(INT_PULSEC_IRQn);
	NVIC_ClearPendingIRQ(INT_PULSEC_IRQn);

	uxIntStatus = SOC_REG(A7DA_PULSEC_INT_STATUS);
	if(uxIntStatus&PULSEC_LW_OVERFLOW)
	{
		DebugMsg("  PulseC: LW_OVERFLOW\r\n");
	}
	else if(uxIntStatus&PULSEC_RW_OVERFLOW)
	{
		DebugMsg("  PulseC: RW_OVERFLOW\r\n");
	}
	else if(uxIntStatus&PULSEC_LW_UNDERFLOW)
	{
		DebugMsg("  PulseC: LW_UNDERFLOW\r\n");
	}
	else if(uxIntStatus&PULSEC_RW_UNDERFLOW)
	{
		DebugMsg("  PulseC: LW_UNDERFLOW\r\n");
	}
	else
	{
		DebugMsg("  Invalid PulseC status: %d\r\n",uxIntStatus);
	}

	SOC_REG(A7DA_PULSEC_INT_STATUS) = uxIntStatus; /* clear INT status */

	NVIC_EnableIRQ( INT_PULSEC_IRQn );
}
