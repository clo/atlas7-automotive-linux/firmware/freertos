/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

#include "ctypes.h"
#include "debug.h"
#include "io.h"
#include "pm.h"
#include "misclib.h"

#include "soc_qspi.h"
#include "soc_cache.h"
#include "soc_pwrc.h"
#include "soc_timer.h"
#include "soc_ioc.h"
#include "soc_iobridge.h"
#include "soc_pwrc_retain.h"
#include "qspi.h"

/* SPI Flash Device Table */
struct nor_flash_info {
	char            *name;
	/*
	 * JEDEC id zero means "no ID" (most older chips); otherwise it has
	 * a high byte of zero plus three uxData bytes: the manufacturer id,
	 * then a two byte device id.
	 */
	UINT32          jedec_id;
	u16             ext_id;
	/*
	 * The max read opcode for nor flash, it should be follow the
	 * qspi SPEC:
	 * 0 - FAST_READ;
	 * 1 - READ2O
	 * 2 - READ2IO
	 * 3 - READ4O
	 * 4 - READ4IO
	 */
	u8		ucMaxRdOp;
	/*
	 * dummy_2b is for read2io dummy cycles
	 * dummy_4b is for read4io dummy cycles
	 */
	UINT8		dummy_2b;
	UINT8		dummy_4b;
	UINT32		( *quad_enable )();
};

static void prvQspiNorMacronixQuadEnable( void );
static void prvQspiNorSpansionQuadEnable( void );
static void prvQspiNorWinbondQuadEnable( void );

#define DEF_NOR_FLASH_TYPE 0
struct nor_flash_info flash_types[] = {
	/* default */
	{ "default", 0, 0, 8, 8, 0, 0 },
	/* Micron */
	{ "MT25QL256ABA8ESF", 0x20BA19, 0, 4, 8, 10, 0},
	/*Macronix */
	{ "mx25l25635f", 0xc22019, 0, 4, 4, 6,
		( void * )prvQspiNorMacronixQuadEnable },
	{ "mx25l12835f", 0xc22018, 0, 4, 4, 6,
		( void * )prvQspiNorMacronixQuadEnable },
	{ "MX25L6445E", 0xc22017, 0, 4, 4, 6,
		( void * )prvQspiNorMacronixQuadEnable },
	/* Spansion */
	{ "s25fl164K", 0x014017, 0, 4, 0, 4,
		( void * )prvQspiNorSpansionQuadEnable },
	{ "S25FL116K",0x014015, 0, 3 , 0, 4,
		( void * )prvQspiNorSpansionQuadEnable},
	/* Winbond w25xx */
	{ "w25q80dv", 0xef4014, 0, 4, 0, 4,
		( void * )prvQspiNorWinbondQuadEnable },
	{ "W25Q32FV",0xEF4016, 0, 4, 0, 4,
		( void * )prvQspiNorWinbondQuadEnable },
	/* Sentinel */
	{},
};

struct nor_flash_info * gp_xNorInfo = NULL;

/* functions */
char* cQspiGetNorName( void )
{
	return gp_xNorInfo->name;
}

static inline __on_retain void prvQspiReadyWait( void )
{
	while ( ( SOC_REG( A7DA_QSPI_STAT ) & QSPI_REQUEST_RDY ) == 0 );
}

static void __on_retain prvQspiCustomOut( UINT32 uxCommand, UINT8 * pcDataBuf, UINT32 uxSize )
{
	UINT8 * pcBuf = pcDataBuf;
	UINT32 uxData = 0;
	UINT32 uxLen = uxSize;
	UINT32 uxIdx = 0;

	prvQspiReadyWait();

	if( uxLen > 0 )
	{
		uxIdx = 0;
		uxData = 0;
		while( ( uxLen > 0 ) && ( uxIdx < 4 ) )
		{
			uxData |= pcBuf[ uxIdx ] << ( uxIdx * 8 );
			uxIdx++;
			uxLen--;
		}
		SOC_REG( A7DA_QSPI_CIDR0 ) = uxData;
	}
	if( uxLen > 0 )
	{
		uxIdx = 0;
		uxData = 0;
		pcBuf += sizeof( UINT32 );
		while( ( uxLen > 0 ) && ( uxIdx < 4 ) )
		{
			uxData |= pcBuf[ uxIdx ] << ( uxIdx * 8 );
			uxIdx++;
			uxLen--;
		}
		SOC_REG( A7DA_QSPI_CIDR1 ) = uxData;
	}
	SOC_REG( A7DA_QSPI_CICFG ) = QSPI_CI_OPCODE( uxCommand ) |
			QSPI_CI_LENGTH( uxSize ) | QSPI_CI_SPI_HOLDN |
			QSPI_CI_CHECK_WIP;

	prvQspiReadyWait();
}

static void __on_retain prvQspiCustomIn( UINT32 uxCommand, UINT8 * pcDataBuf, UINT32 uxSize )
{
	UINT8 * pcBuf = pcDataBuf;
	UINT32 uxData = 0;
	UINT32 uxLen = uxSize;
	UINT32 uxIdx = 0;
	UINT8 *pcByte = ( UINT8 *)&uxData;

	prvQspiReadyWait();

	SOC_REG( A7DA_QSPI_CICFG ) = QSPI_CI_OPCODE( uxCommand ) |
			QSPI_CI_LENGTH( uxSize ) | QSPI_CI_SPI_HOLDN |
			QSPI_CI_CHECK_WIP;

	prvQspiReadyWait();

	if( uxLen > 0 )
	{
		uxData = SOC_REG( A7DA_QSPI_CIDR0 );
		uxIdx = 0;
		while( ( uxLen > 0 ) && ( uxIdx < 4 ) )
		{
			pcBuf[ uxIdx ] = pcByte[ uxIdx ];
			uxIdx++;
			uxLen--;
		}
	}
	if( uxLen > 0 )
	{
		pcBuf += sizeof( UINT32 );
		uxData = SOC_REG( A7DA_QSPI_CIDR1 );
		uxIdx = 0;
		while( ( uxLen > 0 ) && ( uxIdx < 4 ) )
		{
			pcBuf[ uxIdx ] = pcByte[ uxIdx ];
			uxIdx++;
			uxLen--;
		}
	}
}


#define SPINOR_OP_WREN		0x06	/* Write enable */
#define SPINOR_OP_RDSR		0x05	/* Read status register */
#define SPINOR_OP_RDSR2		0x35	/* Read status register 2 */
#define SPINOR_OP_WRSR		0x01	/* Write status register 1 byte */
#define QSPI_MACRONIX_QUAD_EN_BIT	( 0x1<<6)
static void __on_retain prvQspiNorMacronixQuadEnable( void )
{
	UINT8 ucVal = 0;

	prvQspiCustomIn( SPINOR_OP_RDSR, &ucVal, 1 );

	prvQspiCustomOut( SPINOR_OP_WREN, 0, 0 );

	ucVal|= QSPI_MACRONIX_QUAD_EN_BIT;
	prvQspiCustomOut( SPINOR_OP_WRSR, &ucVal, 1 );
}

#define ATLAS7_QSPI_SPANSION_QUAD_EN_BIT        (0x1<<1)
static void __on_retain prvQspiNorSpansionQuadEnable( void )
{
	UINT8 ucVal[2];

	prvQspiCustomIn( SPINOR_OP_RDSR, &ucVal[0], 1 );

	prvQspiCustomIn( SPINOR_OP_RDSR2, &ucVal[1], 1 );

	prvQspiCustomOut( SPINOR_OP_WREN, 0, 0 );

	ucVal[1] |= ATLAS7_QSPI_SPANSION_QUAD_EN_BIT;
	prvQspiCustomOut( SPINOR_OP_WRSR, ucVal, 2 );
}

#define ATLAS7_QSPI_WINBOND_QUAD_EN_BIT        (0x1<<1)
static void __on_retain prvQspiNorWinbondQuadEnable( void )
{
	UINT8 ucVal[2];

	prvQspiCustomIn( SPINOR_OP_RDSR, &ucVal[0], 1 );

	prvQspiCustomIn( SPINOR_OP_RDSR2, &ucVal[1], 1 );

	prvQspiCustomOut( SPINOR_OP_WREN, 0, 0 );

	ucVal[1] |= ATLAS7_QSPI_WINBOND_QUAD_EN_BIT;
	prvQspiCustomOut( SPINOR_OP_WRSR, ucVal, 2 );
}

#define SPINOR_OP_RDID		0x9f	/* Read JEDEC ID */
static void __on_retain prvQspiNorReadJedec( UINT8 * pcJedec, UINT32 uxSize )
{
	UINT8 ucCmd = SPINOR_OP_RDID;

	prvQspiCustomOut( ucCmd, 0, 0 );
	prvQspiCustomIn( ucCmd, pcJedec, uxSize );
}

struct nor_flash_info * __on_retain xQspiNorJedecProbe( void )
{
	struct nor_flash_info	*pxInfo;
	UINT16			usExtJedec;
	UINT32			uxJedec;
	UINT8			ucId[ 5 ];

	prvQspiNorReadJedec( ucId, 5 );

	uxJedec = ucId[ 0 ] << 16 | ucId[ 1 ] << 8 | ucId[ 2 ];
	/*
	 * JEDEC also defines an optional "extended device information"
	 * string for after vendor-specific uxData, after the three bytes
	 * we use here. Supporting some chips might require using it.
	 */
	usExtJedec = ucId[ 3 ] << 8  | ucId[ 4 ];
	for( pxInfo = flash_types; pxInfo->name; pxInfo++ )
	{
		if( pxInfo->jedec_id == uxJedec )
		{
			if (pxInfo->ext_id && pxInfo->ext_id != usExtJedec )
				continue;
			return pxInfo;
		}
	}

	return NULL;
}

void __on_retain vQspiSetClock( UINT32 uxClk )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_CTRL );
	UINT32 uxSpeedHz, uxClkDelay;

	uxReg &= 0x0fffff00;
	uxReg |= uxClk << 28;

	uxSpeedHz = configCPU_CLOCK_HZ / ( 2 * ( uxClk + 1 ) );
 	uxClkDelay = 9 * ( 1000000000 / uxSpeedHz );
	if( uxClkDelay < 100 )
		uxClkDelay = 100;
	uxClkDelay = uxClkDelay / ( 1000000000 / configCPU_CLOCK_HZ );
	if( uxClkDelay > 255 )
		uxClkDelay = 255;

	uxReg |= uxClkDelay;
	SOC_REG( A7DA_QSPI_CTRL ) = uxReg;
}

void __on_retain vQspiSetMode( UINT32 uxMode )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_CTRL );

	uxReg &= 0xf5ffffff;
	uxReg |= ( uxMode & 0x1 ) << 25;
	SOC_REG( A7DA_QSPI_CTRL ) = uxReg;
}

void __on_retain vQspiSetClkDelay( UINT32 uxClkDelay )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_CTRL );

	uxReg &= 0xffffff00;
	uxReg |= ( uxClkDelay & 0xff );
	SOC_REG( A7DA_QSPI_CTRL ) = uxReg;
}

void __on_retain vQspiSetWorkMode( UINT32 uxRdOp, UINT32 uxWrtOp, UINT32 uxExtAddr )
{
	SOC_REG( A7DA_QSPI_DEFMEM ) =
		( uxRdOp & 0x7) | ( uxWrtOp & 0x7) << 3 | uxExtAddr << 6;
}

void __on_retain vQspiSetDummy( UINT32 uxR2IO, UINT32 uxR4IO )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_RDC );

	uxReg &= 0x700;
	uxReg |= ( uxR2IO & 0xf ) | ( uxR4IO & 0xf ) << 4;
	SOC_REG( A7DA_QSPI_RDC ) = uxReg;
}

void __on_retain vQspiSetRxDelay( UINT32 uxRxDelay )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_RDC );

	uxReg &= 0xff;
	uxReg |= ( uxRxDelay & 0x7 ) << 8;
	SOC_REG( A7DA_QSPI_RDC ) = uxReg;
}

void __on_retain vQspiEnterDpm( void )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_CTRL );

	SOC_REG( A7DA_QSPI_DEFMEM ) = 1 << 7;

	uxReg |= QSPI_ENTER_DPM;
	prvQspiReadyWait();
	DebugMsg( "qspi ctl register is 0x%x\r\n", (UINT32 )SOC_REG( A7DA_QSPI_CTRL ) );
	DebugMsg( "qspi ddpm register is 0x%x\r\n", (UINT32 )SOC_REG( A7DA_QSPI_DDPM ) );
	SOC_REG( A7DA_QSPI_CTRL ) = uxReg;
	DebugMsg( "qspi ctl register is 0x%x\r\n", (UINT32 )SOC_REG( A7DA_QSPI_CTRL ) );
}

void __on_retain vQspiLeaveDpm( void )
{
	UINT32 uxReg = SOC_REG( A7DA_QSPI_CTRL );

	uxReg &= ~QSPI_ENTER_DPM;
	SOC_REG( A7DA_QSPI_CTRL ) = uxReg;
	DebugMsg( "qspi ctl register is 0x%x\r\n", (UINT32 )SOC_REG( A7DA_QSPI_CTRL ) );
}

void __on_retain vQspiEnter32addr( void )
{
	prvQspiReadyWait();
	SOC_REG( A7DA_QSPI_CICFG ) = 0xe1b7;
}

void __on_retain vQspiLeave32addr( void )
{
	prvQspiReadyWait();
	SOC_REG( A7DA_QSPI_CICFG ) = 0xe1e9;
}

void __on_retain vQspiSetXipBase( UINT32 uxBase )
{
	SOC_REG( A7DA_QSPI_XOTF_BASE ) = uxBase;
}

void __on_retain vQspiSwitchHost( void )
{
	SOC_REG( A7DA_QSPI_XOTF_EN ) = 0;
	vUsWait( 10 );
	prvQspiReadyWait();
}

void __on_retain vQspiSwitchXotf( void )
{
	prvQspiReadyWait();
	SOC_REG( A7DA_QSPI_XOTF_EN ) = 1;
	__asm volatile("dsb");
}

void __on_retain vQspiWaitTillWIPIdle( void )
{
	UINT8 ucVal = 0;

	do {
		prvQspiCustomIn( SPINOR_OP_RDSR, &ucVal, 1 );
	}while((ucVal&0x01) != 0);

}
void __on_retain vQspiInit( UINT32 uxDelayLine )
{
	struct nor_flash_info *pxInfo;
	/*enable clock*/
	DebugMsg( "enable QSPI clock...\r\n" );
	vIoBridgeWrite( A7DA_PWRC_SPI0_CLK_EN, 0x1 );

	DebugMsg( "Set QSPI IOC\r\n" );
	ioc_enable_qspi_mux();

	/*config QSPI control register
	set Default Memory Register
	*/
	SOC_REG( A7DA_QSPI_DEFMEM ) = 0;

	/*After write to DMR, need wait at least 4 clock cycles*/
	vUsWait( 1 );

	/*Disable XOTF for safety
	Wait QSPI controller ready for next command
	*/
	vQspiSwitchHost();

	/*set Control Register, clock_div =7, 80/8=10MHz*/
	SOC_REG( A7DA_QSPI_CTRL ) = QSPI_RX_FIFO_THD( 0x10 ) | QSPI_TX_FIFO_THD( 0x10 );

	vQspiSetClock(7);

	/*set uxRxDelay*/
	SOC_REG( A7DA_QSPI_RDC ) = 0x7 << 8;

	SOC_REG( A7DA_QSPI_DELAY_LINE ) = uxDelayLine;

	pxInfo = xQspiNorJedecProbe();
	if( !pxInfo )
	{
		/* Use the default nor flash configuration */
		pxInfo = &flash_types[DEF_NOR_FLASH_TYPE];
	}
	if( 0 != pxInfo->quad_enable )
		pxInfo->quad_enable();

	gp_xNorInfo = pxInfo;
	/*set uxRxDelay*/
	SOC_REG( A7DA_QSPI_RDC ) = QSPI_RX_DELAY( 0x7 ) |
				QSPI_RDC_READ4IO( pxInfo->dummy_4b ) |
				QSPI_RDC_READ2IO( pxInfo->dummy_2b );
}

void __on_retain vQspiEraseSector( UINT32 uxFlashAddr )
{
	prvQspiReadyWait();

	SOC_REG( A7DA_QSPI_ACCRR0 ) = uxFlashAddr;
	SOC_REG( A7DA_QSPI_ACCRR1 ) = 0;
	SOC_REG( A7DA_QSPI_ACCRR2 ) = 0x2;
	vUsWait( 10 );

	prvQspiReadyWait();
}

void __on_retain vQspiEraseFlash( UINT32 uxFlashAddr, UINT32 uxSize )
{
	UINT32 uxN = 0;
	UINT32 uxAddr = uxFlashAddr;

	for ( uxN = 0; uxN < uxSize; )
	{
		vQspiEraseSector( uxAddr );
		uxN += 0x1000;
		uxAddr += 0x1000;
	}
}


void __on_retain vQspiIoWrite( UINT32 uxFlashAddr, const UINT32 * pxSource, UINT32 uxSize )
{
	UINT32 uxN;

	prvQspiReadyWait();

	SOC_REG( A7DA_QSPI_ACCRR0 ) = uxFlashAddr;
	SOC_REG( A7DA_QSPI_ACCRR1 ) = uxSize;
	SOC_REG( A7DA_QSPI_ACCRR2 ) = 0x1;

	for( uxN = 0; uxN < uxSize / sizeof( UINT32 ); uxN++ )
	{
		while ( ( SOC_REG( A7DA_QSPI_STAT ) & 0x2 ) == 0 )
			;
		SOC_REG( A7DA_QSPI_RWDATA ) = pxSource[ uxN ];
	}
}

void __on_retain vQspiIoRead( UINT32 uxFlashAddr, UINT32 *pxDest, UINT32 uxSize )
{
	UINT32 uxN;

	prvQspiReadyWait();

	SOC_REG( A7DA_QSPI_ACCRR0 ) = uxFlashAddr;
	SOC_REG( A7DA_QSPI_ACCRR1 ) = uxSize;
	SOC_REG( A7DA_QSPI_ACCRR2 ) = 0x0;

	for ( uxN = 0; uxN < uxSize / sizeof( UINT32 ); uxN++ )
	{
		while ( ( SOC_REG( A7DA_QSPI_STAT ) & 0x1 ) == 0 )
			;
		pxDest[ uxN ] = SOC_REG( A7DA_QSPI_RWDATA );
	}
}

void __on_retain vQspiEnableXipFromWakeup( void )
{
	vIoBridgeWrite(A7DA_PWRC_SPI0_CLK_EN, 0x1 );
	ioc_enable_qspi_mux();

	/* Disable QSPI XOTF mode */
	SOC_REG( A7DA_QSPI_XOTF_EN ) &= ~QSPI_XOTF_ACTIVATED;
	SOC_REG( A7DA_QSPI_XOTF_EN ) |= QSPI_XOTF_DEACTIVATED;

	/* Restore QSPI configuration registers */
	SOC_REG( A7DA_QSPI_CTRL ) = MEM( PM_ARG_QSPI_REG_CTRL );
	SOC_REG( A7DA_QSPI_RDC ) = MEM( PM_ARG_QSPI_REG_RDC );
	SOC_REG( A7DA_QSPI_DEFMEM ) = MEM( PM_ARG_QSPI_REG_DEFMEM );
	SOC_REG( A7DA_QSPI_DELAY_LINE ) = MEM( PM_ARG_QSPI_REG_DELAY_LINE );

	/* Check for ready */
	prvQspiReadyWait();

	/* Enable XOTF */
	SOC_REG( A7DA_QSPI_XOTF_EN ) |= QSPI_XOTF_ACTIVATED;
}

void __on_retain vQspiBoost( void )
{
	UINT32 uxToUs;
	struct nor_flash_info	*pxInfo;
	UINT32 uxQspiParam;
	UINT32 uxQspi32Bits;

	ioc_enable_qspi_mux();

	vIoBridgeWrite(A7DA_PWRC_XINW_FMODE_CTRL,0x1);
	/* Disable QSPI XOTF mode */
	SOC_REG( A7DA_QSPI_XOTF_EN ) &= ~QSPI_XOTF_ACTIVATED;
	SOC_REG( A7DA_QSPI_XOTF_EN ) |= QSPI_XOTF_DEACTIVATED;

	/* Wait 10 us for QSPI controller ready */
	uxToUs = uxOsGetCurrentTick() + 11;
	while( uxOsGetCurrentTick() < uxToUs );

	pxInfo = xQspiNorJedecProbe();
	if( !pxInfo )
	{
		/* Use the default nor flash configuration */
		pxInfo = &flash_types[DEF_NOR_FLASH_TYPE];
	}

	/* Check for ready */
	prvQspiReadyWait();

	/* Wait 1 us for QSPI controller ready */
	uxToUs = uxOsGetCurrentTick() + 2;
	while( uxOsGetCurrentTick() < uxToUs );

	/* Configure QSPI Registers, save for Power Management Wake up */
	/* set Control Register, clock_div =0, 192/2=96MHz*/
	MEM( PM_ARG_QSPI_REG_CTRL ) = QSPI_CLK_DIV( 0 ) | QSPI_TX_FIFO_THD( 0x10 ) |
				QSPI_RX_FIFO_THD( 0x10 ) | QSPI_CLK_DELAY( 0x14 );
	SOC_REG( A7DA_QSPI_CTRL ) = MEM( PM_ARG_QSPI_REG_CTRL );

	/* set uxRxDelay*/
	MEM( PM_ARG_QSPI_REG_RDC ) = QSPI_RX_DELAY( 0x2 ) |
				QSPI_RDC_READ4IO( pxInfo->dummy_4b ) |
				QSPI_RDC_READ2IO( pxInfo->dummy_2b );
	SOC_REG( A7DA_QSPI_RDC ) = MEM( PM_ARG_QSPI_REG_RDC );

	/* enable 4 bit mode */
	uxQspiParam = SOC_REG(A7DA_PWRC_QSPI_PARAMS);
	uxQspiParam >>= 16;
	uxQspi32Bits = 0;
	if( ( uxQspiParam & 0xFF ) == 0x5A )
		uxQspi32Bits = ( uxQspiParam >> 10 ) & 0x1;	/* Bit 26 */
	MEM( PM_ARG_QSPI_REG_DEFMEM ) = QSPI_DEF_MEM_READ_OPCODE( pxInfo->ucMaxRdOp );
	if( uxQspi32Bits )
		MEM( PM_ARG_QSPI_REG_DEFMEM ) |= QSPI_DEF_MEM_ATTR_32;
	SOC_REG( A7DA_QSPI_DEFMEM ) = MEM( PM_ARG_QSPI_REG_DEFMEM );

	MEM( PM_ARG_QSPI_REG_DELAY_LINE ) = 0;
	SOC_REG( A7DA_QSPI_DELAY_LINE ) = MEM( PM_ARG_QSPI_REG_DELAY_LINE );

	if( 0 != pxInfo->quad_enable )
		pxInfo->quad_enable();

	/* Check for ready */
	prvQspiReadyWait();

        /* Enable XIP */
        SOC_REG( A7DA_QSPI_XOTF_EN ) |= QSPI_XOTF_ACTIVATED;

        /* Flush QSPI Parameters into Memory */
        vDcacheFlush();
}
