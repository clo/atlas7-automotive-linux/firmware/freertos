/*
 * Virtio-based remote processor messaging bus
 *
 * Copyright (C) 2011 Texas Instruments, Inc.
 * Copyright (C) 2011 Google, Inc.
 *
 * Ohad Ben-Cohen <ohad@wizery.com>
 * Brian Swetland <swetland@google.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "ctypes.h"
#include "errno.h"
#include "debug.h"
#include "io.h"
#include "misclib.h"

/* Internal includes */
#include "virtio.h"
#include "vring.h"
#include "rproc.h"
#include "rpmsg.h"
#include "image_header.h"

#include "system/audio/audio_protocol.h"

extern bool m3_audio_support;
 /* virtio remote processor messaging */
#define VIRTIO_ID_RPMSG	7

/* Indices of rpmsg virtio features we support */
/* RP supports name service notifications */
#define VIRTIO_RPMSG_F_NS 0

/* RPMSG customized MMIO */
/* This MMIO space store the rpmsg memory pool's physical address */
#define RPMSG_MMIO_MEMPOOL	MMIO_CONFIG_BASE

#define RPMSG_MAX_ENDPOINTS	2

struct virtio_rpmsg_bus {
	int status;
	struct virtio_device *vdev;
	struct virtqueue *rvq, *svq;
	void *rbufs, *sbufs;
	int last_sbuf;
	dma_addr_t bufs_dma;
	SemaphoreHandle_t sendq;
	SemaphoreHandle_t tx_lock;
	SemaphoreHandle_t endpoints_lock;
	u32 endpoint_ids;

	struct rpmsg_device ns_dev;
	struct rpmsg_device client_devs[RPMSG_MAX_ENDPOINTS];
};

static struct virtio_rpmsg_bus rpmsg_bus;

#define RPMSG_NUM_BUFS		(512)
#define RPMSG_BUF_SIZE		(512)
#define RPMSG_TOTAL_BUF_SPACE	(RPMSG_NUM_BUFS * RPMSG_BUF_SIZE)

/*
 * Local addresses are dynamically allocated on-demand.
 * We do not dynamically assign addresses from the low 1024 range,
 * in order to reserve that address range for predefined services.
 */
#define RPMSG_RESERVED_ADDRESSES	(1024)

/* Address 53 is reserved for advertising remote services */
#define RPMSG_NS_ADDR			(53)

/* for more info, see below documentation of rpmsg_create_ept() */
static struct
rpmsg_device *__rpmsg_create_device(struct virtio_rpmsg_bus *vrbus,
		const char *name, rpmsg_rx_cb_t cb,
		u32 local, u32 remote)
{
	struct rpmsg_device *ept;

	xSemaphoreTake(vrbus->endpoints_lock, portMAX_DELAY);

	if (local == RPMSG_NS_ADDR) {
		ept = &vrbus->ns_dev;
		goto setup_dev;
	}

	if (vrbus->endpoint_ids >= RPMSG_MAX_ENDPOINTS) {
		LOG("failed to request a new ept\r\n");
		ept = NULL;
		goto failed;
	}

	ept = &vrbus->client_devs[vrbus->endpoint_ids];
	vrbus->endpoint_ids++;

setup_dev:
	ept->cb = cb;
	ept->name = name;
	ept->local = local;
	ept->remote = remote;
	ept->vrbus = vrbus;

failed:
	xSemaphoreGive(vrbus->endpoints_lock);
	return ept;
}

struct rpmsg_device *rpmsg_create_device(const char *name, rpmsg_rx_cb_t cb,
			u32 local, u32 remote)
{
	int err;
	struct rpmsg_device *ept;
	struct rpmsg_ns_msg nsm;

	ept = __rpmsg_create_device(&rpmsg_bus, name, cb,
					local, remote);
	if (!ept)
		return NULL;

	strncpy(nsm.name, name, RPMSG_NAME_SIZE);
	nsm.addr = ept->local;
	nsm.flags = RPMSG_NS_CREATE;

	err = rpmsg_sendto(ept, &nsm, sizeof(nsm), RPMSG_NS_ADDR);
	if (err) {
		LOG("failed to announce service %d\r\n", err);
		return NULL;
	}

	return ept;
}

/* super simple buffer "allocator" that is just enough for now */
static void *get_a_tx_buf(struct virtio_rpmsg_bus *vrbus,
				u32 *idx, u32 *len)
{
	void *ret;

	/* support multiple concurrent senders */
	xSemaphoreTake(vrbus->tx_lock, portMAX_DELAY);

	ret = virtqueue_get_avail_buf(vrbus->svq, idx, len);

	xSemaphoreGive(vrbus->tx_lock);

	return ret;
}

int rpmsg_send_offchannel_raw(struct rpmsg_device *rpdev, u32 src, u32 dst,
					void *data, int len, bool wait)
{
	struct virtio_rpmsg_bus *vrbus = rpdev->vrbus;
	struct rpmsg_hdr *msg;
	unsigned int buf_idx, buf_len;

	/* bcasting isn't allowed */
	if (src == RPMSG_ADDR_ANY || dst == RPMSG_ADDR_ANY) {
		LOG("invalid addr (src 0x%x, dst 0x%x)\r\n", src, dst);
		return -EINVAL;
	}

	/*
	 * We currently use fixed-sized buffers, and therefore the payload
	 * length is limited.
	 *
	 * One of the possible improvements here is either to support
	 * user-provided buffers (and then we can also support zero-copy
	 * messaging), or to improve the buffer allocator, to support
	 * variable-length buffer sizes.
	 */
	if (len > RPMSG_BUF_SIZE - sizeof(struct rpmsg_hdr)) {
		LOG("message is too big (%d)\r\n", len);
		return -EFBIG;
	}

	/* grab a buffer */
	msg = get_a_tx_buf(vrbus, &buf_idx, &buf_len);
	if (!msg && !wait)
		return -ENOMEM;

	while (!msg) {
		xSemaphoreTake(vrbus->sendq, portMAX_DELAY);
		msg = get_a_tx_buf(vrbus, &buf_idx, &buf_len);
	}

	msg->len = len;
	msg->flags = 0;
	msg->src = src;
	msg->dst = dst;
	msg->reserved = 0;
	memcpy(msg->data, data, len);

	LOG("TX From 0x%x, To 0x%x, Len %d, Flags %d, Reserved %d\r\n",
				msg->src, msg->dst, msg->len,
				msg->flags, msg->reserved);

	xSemaphoreTake(vrbus->tx_lock, portMAX_DELAY);

	virtqueue_set_used_buf(vrbus->svq, buf_idx, buf_len);

	/* tell the remote processor it has a pending message to read */
	virtqueue_kick(vrbus->svq);
	LOG("ADDR:%X <<%s\r\n", msg, data);

	xSemaphoreGive(vrbus->tx_lock);
	return 0;
}

static struct
rpmsg_device *__rpmsg_get_device_by_addr(struct virtio_rpmsg_bus *vrbus,
						u32 addr)
{
	struct rpmsg_device *ept;
	int idx;

	for (idx = 0; idx < vrbus->endpoint_ids; idx++) {
		ept = &vrbus->client_devs[idx];
		if (ept->local == addr)
			return ept;
	}

	return NULL;
}

static int rpmsg_recv_single(struct virtio_rpmsg_bus *vrbus,
			     struct rpmsg_hdr *msg, unsigned int len)
{
	struct rpmsg_device *ept;

	LOG("From: 0x%x, To: 0x%x, Len: %d, Flags: %d, Reserved: %d\r\n",
					msg->src, msg->dst, msg->len,
					msg->flags, msg->reserved);
	/*
	 * We currently use fixed-sized buffers, so trivially sanitize
	 * the reported payload length.
	 */
	if (len > RPMSG_BUF_SIZE ||
		msg->len > (len - sizeof(struct rpmsg_hdr))) {
		LOG("inbound msg too big: (%d, %d)\r\n", len, msg->len);
		return -EINVAL;
	}

	if (msg->dst == RPMSG_NS_ADDR)
		ept = &vrbus->ns_dev;
	else
		ept = __rpmsg_get_device_by_addr(vrbus, msg->dst);

	if (ept && ept->cb)
		ept->cb(ept, msg->data, msg->len, msg->src);
	else
		LOG("msg received with no recipient\r\n");

	return 0;
}

/* called when an rx buffer is used, and it's time to digest a message */
static void rpmsg_recv_done(struct virtqueue *rvq)
{
	struct virtio_rpmsg_bus *vrbus = rvq->vdev->priv;
	struct rpmsg_hdr *msg;
	u32 idx, len, msgs_received = 0;
	int err;

	msg = virtqueue_get_avail_buf(rvq, &idx, &len);
	if (!msg) {
		LOG("uhm, incoming signal, but no used buffer ?\r\n");
		return;
	}

	while (msg) {
		err = rpmsg_recv_single(vrbus, msg, len);
		if (err)
			break;

		virtqueue_set_used_buf(rvq, idx, len);

		msgs_received++;

		msg = virtqueue_get_avail_buf(rvq, &idx, &len);
	};

	/* tell the remote processor we added another available rx buffer */
	if (msgs_received)
		virtqueue_kick(vrbus->rvq);
}

/*
 * This is invoked whenever the remote processor completed processing
 * a TX msg we just sent it, and the buffer is put back to the used ring.
 *
 * Normally, though, we suppress this "tx complete" interrupt in order to
 * avoid the incurred overhead.
 */
static void rpmsg_xmit_done(struct virtqueue *svq)
{
	struct virtio_rpmsg_bus *vrbus = svq->vdev->priv;

	/* wake up potential senders that are waiting for a tx buffer */
	xSemaphoreGive(vrbus->sendq);
}

/* invoked when a name service announcement arrives */
static void rpmsg_ns_cb(struct rpmsg_device *rpdev,
			void *data, int len, u32 src)
{
	LOG("###RPMSG Name Service Device Callback###\r\n");
}

static int __rpmsg_turn_online(struct virtio_device *vdev,
				struct virtio_rpmsg_bus *vrbus)
{
	vq_callback_t *vq_cbs[2];
	const char *names[2];
	struct virtqueue *vqs[2];
	void *bufs_va;
	int err;
	struct rpmsg_device *ns_ept;

	vq_cbs[0] = rpmsg_xmit_done;
	vq_cbs[1] = rpmsg_recv_done;
	names[0] = "output";
	names[1] = "input";

	err = rproc_virtio_find_vqs(vdev, 2, vqs, vq_cbs, names);
	if (err)
		return err;

	vrbus->svq = vqs[0];
	vrbus->rvq = vqs[1];

	/* Get frontend prepared RPMSG Memory Pool address */
	vrbus->bufs_dma = virtqueue_get_data_address(vrbus->svq, 0);
	bufs_va = (void *)TO_MCU_IO_DRAM(vrbus->bufs_dma);
	DebugMsg("buffers: va 0x%08x, dma 0x%x\r\n", (UINT32)bufs_va, (UINT32)vrbus->bufs_dma);

	/* half of the buffers is dedicated for TX */
	vrbus->sbufs = bufs_va;
	/* and half is dedicated for RX */
	vrbus->rbufs = bufs_va + (RPMSG_TOTAL_BUF_SPACE / 2);

	/* if supported by the remote processor, enable the name service */
	/* a dedicated endpoint handles the name service msgs */
	ns_ept = __rpmsg_create_device(vrbus, NULL, rpmsg_ns_cb,
					RPMSG_NS_ADDR, RPMSG_NS_ADDR);
	if (!ns_ept) {
		LOG("failed to create the ns device\r\n");
		return -ENOSPC;
	}

	DebugMsg("rpmsg host is online\r\n");

	if (m3_audio_support){
		audio_protocol_init();
		DebugMsg("audio protocal is online\r\n");
	}

	return 0;
}

static void __rpmsg_client_sample_cb(struct rpmsg_device *rpdev,
			void *data, int len, u32 src)
{
	int ret;
	char *str = data;

	LOG("###__rpmsg_client_sample_cb src:%X data:%s\r\n",
			src, data);
	str[0] = str[1] = 'B';
	ret = rpmsg_sendto(rpdev, data, len, src);
	if (ret)
		DebugMsg("Send back to front sample failed!err=%d\r\n", ret);
}

static int rpmsg_create_predefined_channels(struct virtio_rpmsg_bus *vrbus)
{
	rpmsg_create_device("rpmsg-client-sample",
				__rpmsg_client_sample_cb,
				0x1234, RPMSG_ADDR_ANY);
	return 0;
}

static int rpmsg_online(struct virtio_device *vdev, u32 offset)
{
	int ret;
	struct virtio_rpmsg_bus *vrbus = vdev->priv;

	/* Turn rpmsg online */
	ret = __rpmsg_turn_online(vdev, vrbus);
	if (ret) {
		LOG("turn rpmsg bus failed!err:%d\r\n", ret);
		return ret;
	}

	/* create predefined channels */
	ret = rpmsg_create_predefined_channels(vrbus);

	return ret;
}

int virtio_rpmsg_bus_init(struct rproc *rproc)
{
	struct virtio_device *vdev;

	vdev = rproc_add_virtio_device(rproc, VIRTIO_ID_RPMSG, 2, 256,
				1 << VIRTIO_RPMSG_F_NS);
	if (!vdev) {
		LOG("vdev#rpmsg initialized failed!\r\n");
		return -EINVAL;
	}

	rpmsg_bus.vdev = vdev;
	DebugMsg("start probe virtio rpmsg backend bus\r\n");

	rpmsg_bus.sendq = xSemaphoreCreateBinary();
	if (rpmsg_bus.sendq == NULL) {
		LOG("create rpmsg bus sendq failed!\r\n");
		return -EINVAL;
	}

	rpmsg_bus.tx_lock = xSemaphoreCreateMutex();
	if (rpmsg_bus.tx_lock == NULL) {
		LOG("create rpmsg bus tx_lock failed!\r\n");
		return -EINVAL;
	}

	rpmsg_bus.endpoints_lock = xSemaphoreCreateMutex();
	if (rpmsg_bus.endpoints_lock == NULL) {
		LOG("create rpmsg bus endpoints_lock failed!\r\n");
		return -EINVAL;
	}

	rpmsg_bus.endpoint_ids = 0;

	vdev->priv = &rpmsg_bus;

	rproc_set_online_handler(vdev, (rproc_dev_online *)rpmsg_online);

	return 0;
}
extern void* virtqueue_get_vvq(struct virtqueue *_vq);
extern void audio_protocol_deinit(void);
void virtio_rpmsg_bus_deinit(struct rproc *rproc)
{
	struct virtio_device *vdev;
	struct rproc_vdev *rvdev;

	vdev = rpmsg_bus.vdev;
	rvdev = vdev_to_rvdev(vdev);

	if(rpmsg_bus.sendq != NULL)
	{
		vSemaphoreDelete(rpmsg_bus.sendq);
		rpmsg_bus.sendq = NULL;
	}

	if(rpmsg_bus.tx_lock)
	{
		vSemaphoreDelete(rpmsg_bus.tx_lock);
		rpmsg_bus.tx_lock = NULL;
	}

	if(rpmsg_bus.endpoints_lock)
	{
		vSemaphoreDelete(rpmsg_bus.endpoints_lock);
		rpmsg_bus.endpoints_lock = NULL;
	}

	if( RPROC_DEV_STATE_RUNNING == rvdev->status )
	{	/* free the 2 vrings */
		vPortFree(virtqueue_get_vvq(rvdev->vring[0].vq));
		vPortFree(virtqueue_get_vvq(rvdev->vring[1].vq));
	}
}


