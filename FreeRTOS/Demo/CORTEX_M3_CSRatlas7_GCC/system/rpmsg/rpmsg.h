#ifndef __RPMSG_H__
#define __RPMSG_H__

#define RPMSG_NAME_SIZE		32

#define VIRTIO_RPMSG_F_NS	0

struct rpmsg_hdr {
	u32 src;
	u32 dst;
	u32 reserved;
	u16 len;
	u16 flags;
	u8 data[0];
} __packed;

struct rpmsg_ns_msg {
	char name[RPMSG_NAME_SIZE];
	u32 addr;
	u32 flags;
} __packed;

enum rpmsg_ns_flags {
	RPMSG_NS_CREATE = 0,
	RPMSG_NS_DESTROY,
};

#define RPMSG_ADDR_ANY		0xFFFFFFFF

struct virtio_rpmsg_bus;

struct rpmsg_device;
typedef void (*rpmsg_rx_cb_t)(struct rpmsg_device *, void *, int, u32);

struct rpmsg_device {
	const char *name;
	rpmsg_rx_cb_t cb;
	u32 local;
	u32 remote;
	struct virtio_rpmsg_bus *vrbus;
};

int rpmsg_send_offchannel_raw(struct rpmsg_device *rpdev, u32 local, u32 remote,
					void *data, int len, bool wait);

static inline int rpmsg_send(struct rpmsg_device *rpdev, void *data, int len)
{
	return rpmsg_send_offchannel_raw(rpdev,
			rpdev->local, rpdev->remote, data, len, true);
}

static inline
int rpmsg_sendto(struct rpmsg_device *rpdev, void *data, int len, u32 remote)
{
	return rpmsg_send_offchannel_raw(rpdev,
			rpdev->local, remote, data, len, true);
}

static inline
int rpmsg_send_offchannel(struct rpmsg_device *rpdev, u32 local, u32 remote,
						void *data, int len)
{
	return rpmsg_send_offchannel_raw(rpdev,
			local, remote, data, len, true);
}

static inline
int rpmsg_trysend(struct rpmsg_device *rpdev, void *data, int len)
{
	return rpmsg_send_offchannel_raw(rpdev,
			rpdev->local, rpdev->remote, data, len, false);
}

static inline
int rpmsg_trysendto(struct rpmsg_device *rpdev, void *data, int len, u32 remote)
{
	return rpmsg_send_offchannel_raw(rpdev,
			rpdev->local, remote, data, len, false);
}

static inline
int rpmsg_trysend_offchannel(struct rpmsg_device *rpdev, u32 local, u32 remote,
							void *data, int len)
{
	return rpmsg_send_offchannel_raw(rpdev,
			local, remote, data, len, false);
}

struct rpmsg_device * rpmsg_create_device(const char *name, rpmsg_rx_cb_t cb,
			u32 local, u32 remote);
#endif /* __RPMSG_H__ */
