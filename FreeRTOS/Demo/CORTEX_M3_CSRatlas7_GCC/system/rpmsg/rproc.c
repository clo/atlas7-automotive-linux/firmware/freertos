/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "ctypes.h"
#include "debug.h"
#include "errno.h"
#include "io.h"
#include "misclib.h"

#include "soc_irq.h"
#include "soc_ipc.h"
#include "core_cm3.h"

/* Internal includes */
#include "virtio.h"
#include "vring.h"
#include "rproc.h"

/* The stack size of rproc task progress */
#define M3_RPROC_STACK_SIZE	(configMINIMAL_STACK_SIZE * 2)

/* save rproc instance */
static struct rproc *ipc_rprocs[RPROC_TRGT2_MAX];

#if 0
void dump_hex(void *data, int len)
{
	int idx;
	u8 val, *ptr;

	for (idx = 0; idx < len; idx++) {
		ptr = data + idx;
		val = (*ptr) & 0xff;

		if (idx & 0xf)
			DebugMsg(" %B", val);
		else
			DebugMsg("\n%H: %B", ptr, val);
	}

	DebugMsg("\r\n");
}
#endif

static int hwspin_lock_timeout_irq(u32 lock, u32 to)
{
	u32 time = 0;

	do {
		portDISABLE_INTERRUPTS();
		if (HW_SPINLOCK_FREE == SOC_REG(A7DA_IPC_TESTANDSET(lock)))
			return 0;
		else {
			portENABLE_INTERRUPTS();
			if (time >= to)
				break;
		}
		vTaskDelay(HW_SPINLOCK_DELAY);
		time += HW_SPINLOCK_DELAY;
	} while (1);

	return -EBUSY;
}

static void hwspin_unlock_irq(u32 lock)
{
	SOC_REG(A7DA_IPC_TESTANDSET(lock)) = HW_SPINLOCK_LOCKED;
	portENABLE_INTERRUPTS();
}

static int fifo_write(struct fifo_buffer *fifo,
		const void *data, u32 len)
{
	u32 overflow, count;
	int err;

	err = hwspin_lock_timeout_irq(fifo->lock, 100);
	if (err) {
		DebugMsg("Get hwspinlock failed!\r\n");
		return err;
	}

	if (len > fifo->size) {
		err = -EFBIG;
		goto err_exit;
	}

	count = *fifo->count;
	overflow = len > (fifo->size - count);
	if (overflow) {
		/* previous data hasn't been read, FIFO busy */
		err = -EBUSY;
		goto err_exit;
	}

	/* copy data to fifo buffer */
	memcpy(fifo->buffer + fifo->w_pos, data, len);
	/* update fifo position */
	fifo->w_pos = (fifo->w_pos + len) % fifo->size;
	*fifo->count = count + len;

	/* memory barrier */
	/* Fix it: TODO */

	err = 0;

err_exit:
	hwspin_unlock_irq(fifo->lock);

	return err;
}

static int fifo_read(struct fifo_buffer *fifo,
		void *data, u32 len)
{
	int err;
	u32 count;

	err = hwspin_lock_timeout_irq(fifo->lock, 100);
	if (err) {
		DebugMsg("Get hwspinlock failed!\r\n");
		return err;
	}

	count = *fifo->count;
	if (!count) {
		err = -ENOSPC;
		goto err_exit;
	}

	if (len > count)
		len = count;

	/* copy data from fifo buffer */
	memcpy(data, fifo->buffer + fifo->r_pos, len);
	/* update fifo position */
	fifo->r_pos = (fifo->r_pos + len) % fifo->size;
	*fifo->count = count - len;

	err = 0;

err_exit:
	hwspin_unlock_irq(fifo->lock);

	return err;
}

static void fifo_init(struct fifo_buffer *fifo, void *buffer,
			int size, int hwlock_id)
{
	fifo->lock = hwlock_id;
	fifo->count = (u32 *)buffer;
	fifo->buffer = (u8 *)(buffer + sizeof(u32));
	fifo->w_pos = 0;
	fifo->r_pos = 0;
	fifo->size = size - 4;
	*fifo->count = 0;
}

/* kick the remote processor, and let it know which virtqueue to poke at */
static bool rproc_virtio_kick(struct rproc *rproc, int notify_id)
{
	int ret;
	bool b_err;

	xSemaphoreTake(rproc->xMutex, portMAX_DELAY);

	ret = fifo_write(&rproc->w_fifo, &notify_id, sizeof(notify_id));
	if (ret) {
		LOG("fifo_write could not completed, err=%d\r\n", ret);
		b_err =  false;
		goto err_exit;
	}
	b_err = true;

err_exit:
	xSemaphoreGive(rproc->xMutex);

	/* Trigger interrupt to remote side */
	SOC_REG(A7DA_IPC_TRGT1_INIT2_1) = 0x01;
	return b_err;
}

/* kick the remote processor, and let it know which virtqueue to poke at */
static bool rproc_virtio_notify(struct virtqueue *vq)
{
	struct rproc_vring *rvring = vq->priv;
	struct rproc *rproc = rvring->rvdev->rproc;

	return rproc_virtio_kick(rproc, rvring->notifyid);
}

static struct virtqueue *rp_find_vq(struct virtio_device *vdev,
				    unsigned id,
				    void (*callback)(struct virtqueue *vq),
				    const char *name)
{
	struct rproc *rproc;
	struct rproc_vdev *rvdev;
	struct rproc_vring *rvring;
	struct rsc_vdev *rsc;
	struct virtqueue *vq;

	rvdev = vdev_to_rvdev(vdev);
	if (id >= rvdev->num_of_vring) {
		LOG("rp_find_vq index[%d] out of range!\r\n", id);
		return NULL;
	}

	rproc = rvdev->rproc;
	/* resource entry of rvdev */
	rsc = (void *)rproc->table + rvdev->rsc_offset;

	/* Get A7 allocated vring dma address */
	rvring = &rvdev->vring[id];
	rvring->dma = rsc->vring[id].da;
	rvring->pa = (void *)TO_MCU_IO_DRAM(rsc->vring[id].da);

	LOG("vring%d: pa %x dma %x size %x idr %d\r\n", id,
			rvring->pa, rvring->dma,
			PAGE_ALIGN(vring_size(rvring->len, rvring->align)),
			rvring->notifyid);
	/*
	 * Create the new vq, and tell virtio we're not interested in
	 * the 'weak' smp barriers, since we're talking with a real device.
	 */
	vq = vring_new_virtqueue(id, rvring->len, rvring->align, vdev, false,
			rvring->pa, rproc_virtio_notify, callback, name);
	if (!vq) {
		LOG("vring_new_virtqueue %s failed\r\n", name);
		return NULL;
	}

	rvring->vq = vq;
	vq->priv = rvring;

	return vq;
}

int rproc_virtio_find_vqs(struct virtio_device *vdev, unsigned nvqs,
		       struct virtqueue *vqs[],
		       vq_callback_t *callbacks[],
		       const char *names[])
{
	int i;

	for (i = 0; i < nvqs; i++) {
		vqs[i] = rp_find_vq(vdev, i, callbacks[i], names[i]);
		if (NULL == vqs[i])
			return -EINVAL;
	}

	return 0;
}

static u8 rproc_virtio_get_status(struct virtio_device *vdev)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);

	return rvdev->status;
}

static void rproc_virtio_set_status(struct virtio_device *vdev, u8 status)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);

	rvdev->status = status;
}

/* provide the vdev features as retrieved from the firmware */
u32 rproc_virtio_get_features(struct virtio_device *vdev)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);
	struct rproc *rproc = rvdev->rproc;
	struct rsc_vdev *rsc;

	rsc = (void *)rproc->table + rvdev->rsc_offset;

	return (rsc->dfeatures & ~(1 << VIRTIO_RPROC_F_FRONT))
			| (1 << VIRTIO_RPROC_F_BACK);

}

void rproc_virtio_get(struct virtio_device *vdev, unsigned offset,
							void *buf, unsigned len)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);
	struct rproc *rproc = rvdev->rproc;
	struct rsc_vdev *rsc;
	void *cfg;

	rsc = (void *)rproc->table + rvdev->rsc_offset;
	cfg = &rsc->vring[rsc->num_of_vrings];

	if (offset + len > rsc->config_len || offset + len < len) {
		LOG("rproc_virtio_get: access out of bounds\r\n");
		return;
	}

	memcpy(buf, cfg + offset, len);
}

void rproc_virtio_set(struct virtio_device *vdev, unsigned offset,
		      const void *buf, unsigned len)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);
	struct rproc *rproc = rvdev->rproc;
	struct rsc_vdev *rsc;
	void *cfg;

	rsc = (void *)rproc->table + rvdev->rsc_offset;
	cfg = &rsc->vring[rsc->num_of_vrings];

	if (offset + len > rsc->config_len || offset + len < len) {
		LOG("rproc_virtio_set: access out of bounds\r\n");
		return;
	}

	memcpy(cfg + offset, buf, len);
}

static int rproc_vq_interrupt(struct rproc *rproc, int notifyid)
{
	u32 index;
	struct rproc_vdev *rvdev;
	struct rproc_vring *rvring;

	index = notifyid - RPROC_VRING_MIN_ID;
	LOG("vq index[%d] %H is interrupted\r\n", notifyid, index);

	if (index >= rproc->vq_entries)
		return -EINVAL;

	rvring = rproc->vrings[index];
	configASSERT(rvring);
	rvdev = rvring->rvdev;
	configASSERT(rvdev);

	if (rvdev->status == RPROC_DEV_STATE_RUNNING)
		vring_interrupt(notifyid, rvring->vq);
	else {
		LOG("vq hasn't been created, call device to init vq\r\n",
				notifyid);
		if (rvdev && rvdev->online) {
			int ret;
			ret = rvdev->online(&rvdev->vdev, 0);
			if (ret)
				rvdev->status = RPROC_DEV_STATE_CRASHED;
			else
				rvdev->status = RPROC_DEV_STATE_RUNNING;
		}
	}

	return 0;
}

/* A7_NS to M3 Interrupt#1 Handler */
void INT_IPC_TRGT2_INIT1_INTR1_Handler(void)
{
	volatile u32 uxReg;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	struct rproc *rproc = ipc_rprocs[RPROC_A7NS_M3_1];

	NVIC_DisableIRQ(INT_IPC_TRGT2_INIT1_INTR1_IRQn);

	LOG("INT_IPC_TRGT2_INIT1_INTR1_Handler!!!!\r\n");

	/* read register to clear the interrupt */
	uxReg = SOC_REG( A7DA_IPC_TRGT2_INIT1_1 );

	if(NULL==rproc)
	{	/* Safety consideration what if rproc is not ready but A7 gives a probe
	 	 * request */
		NVIC_EnableIRQ(INT_IPC_TRGT2_INIT1_INTR1_IRQn);
		return;
	}

	if(uxReg)
	{
		/* Unblock the task by releasing the semaphore. */
		xSemaphoreGiveFromISR(rproc->xSemaphore,&xHigherPriorityTaskWoken);
	}
}

static int rproc_init_rsc(struct rproc *rproc)
{
	u32 index = 0, offset = 0;
	void *tx_buffer, *rx_buffer;
	struct rsc_hdr *rsc_hdr;

	/* initialize the vdev resource entry */
	/* first vdev resource entry offset */
	offset = (u32)&(rproc->table->offset[rproc->vdev_entries]);
	for (index = 0; index < rproc->vdev_entries; index++) {
		rsc_hdr = (struct rsc_hdr *)(offset +
				rproc->vdev_entry_size * index);
		/* Set each entry to UNUSED. */
		rsc_hdr->type = RSC_LAST;
		rproc->table->offset[index] = (u32)rsc_hdr - (u32)rproc->table;
	}

	/* Set the last entry to RSC_LAST to indicate end. */
	rsc_hdr->type = RSC_LAST;

	rx_buffer = rproc->fifo_base + rproc->fifo_sz * FIFO_LOGIC_CHN_0;
	tx_buffer = rproc->fifo_base + rproc->fifo_sz * FIFO_LOGIC_CHN_1;

	fifo_init(&rproc->r_fifo, rx_buffer,
			rproc->fifo_sz, rproc->fifo_rlock);
	fifo_init(&rproc->w_fifo, tx_buffer,
			rproc->fifo_sz, rproc->fifo_wlock);
	LOG("rx_fifo:0x%x tx_fifo:0x%x\r\n", rx_buffer, tx_buffer);
	return 0;
}

static struct rproc_vdev *rproc_setup_rproc_vdev(struct rproc *rproc,
				u32 virtio_id, u32 vq_number,
				u32 vq_length, u32 dfeatures)
{
	struct rsc_vdev *rsc;
	struct rsc_hdr *rsc_hdr;
	struct rproc_vdev *rvdev;
	void *cfg;
	u32 idx, offset, index;

	index = rproc->table->num;
	if (index >= rproc->vdev_entries) {
		LOG("rsc entry#%d out of range!\r\n");
		return NULL;
	}

	offset = rproc->table->offset[index];
	rsc_hdr = (struct rsc_hdr *)((void *)rproc->table + offset);
	if (rsc_hdr->type == RSC_VDEV) {
		LOG("rsc entry#%d has been used aleady!\r\n");
		return NULL;
	}

	index = rproc->vdev_notifyid_index - RPROC_VDEV_MIN_ID;
	rsc = (struct rsc_vdev *)rsc_hdr->data;
	rsc->id = virtio_id;
	rsc->notifyid = rproc->vdev_notifyid_index++;
	rsc->dfeatures = dfeatures;
	rsc->gfeatures = 0;
	rsc->num_of_vrings = vq_number;
	rsc->reserved[0] = 0;
	rsc->reserved[1] = 0;
	rsc->config_len = RPROC_VDEV_MMIO_SIZE;

	rvdev = &rproc->rvdevs[index];
	rvdev->rproc = rproc;
	rvdev->rsc_offset = offset + sizeof(struct rsc_hdr);
	rvdev->notifyid = rsc->notifyid;
	rvdev->num_of_vring = RVDEV_NUM_VRINGS;
	rvdev->features = rsc->dfeatures;
	rvdev->status = RPROC_DEV_STATE_OFFLINE;
	rvdev->online = NULL;

	LOG("VDEV#%u RSCINFO:\r\n"
		"\tid        : %u\r\n"
		"\tnotifier  : %u\r\n"
		"\tdfeatures : 0x%x\r\n"
		"\tvring_num : %u\r\n"
		"\thdr_offset: 0x%x\r\n"
		"\tdat_offset: 0x%x\r\n"
		"\tconfig_len: %u\r\n", index,
		rsc->id, rsc->notifyid, rsc->dfeatures, rsc->num_of_vrings,
		offset, rvdev->rsc_offset, rsc->config_len);

	/* Setup vring resource */
	for (idx = 0; idx < vq_number; idx++) {
		rsc->vring[idx].notifyid = rproc->vq_notifyid_index++;
		rsc->vring[idx].align = RPROC_VRING_ALIGN_PAGE;
		rsc->vring[idx].num = vq_length;
		rsc->vring[idx].reserved = 0;
		rsc->vring[idx].da = 0;

		rvdev->vring[idx].rvdev = rvdev;
		rvdev->vring[idx].notifyid = rsc->vring[idx].notifyid;
		rvdev->vring[idx].align = RPROC_VRING_ALIGN_PAGE;
		rvdev->vring[idx].len = vq_length;
		rvdev->vring[idx].pa = 0;

		offset = rsc->vring[idx].notifyid - RPROC_VRING_MIN_ID;
		rproc->vrings[offset] = &rvdev->vring[idx];
		LOG("\n\tVRING#%u INFO:\r\n"
			"\t\tnotifier : %u\r\n"
			"\t\talign    : 0x%x\r\n"
			"\t\tlength   : %u\r\n"
			"\t\taddr     : 0x%x\r\n", idx,
			rsc->vring[idx].notifyid, rsc->vring[idx].align,
			rsc->vring[idx].num, rsc->vring[idx].da);
	}

	rproc->table->num++;
	rsc_hdr->type = RSC_VDEV;

	/* Setup device private data */
	cfg = &rsc->vring[rsc->num_of_vrings];
	memset(cfg, 0, RPROC_VDEV_MMIO_SIZE);

	return rvdev;
}

static struct virtio_config_ops s_vdev_cfg_ops = {
	.get = rproc_virtio_get,
	.set = rproc_virtio_set,
	.get_status = rproc_virtio_get_status,
	.set_status = rproc_virtio_set_status,
	.get_features = rproc_virtio_get_features,
};

void rproc_set_online_handler(struct virtio_device *vdev,
				rproc_dev_online *online)
{
	struct rproc_vdev *rvdev = vdev_to_rvdev(vdev);

	rvdev->online = online;
}

struct virtio_device *rproc_add_virtio_device(struct rproc *rproc,
				u32 virtio_id, u32 vq_number,
				u32 vq_length, u32 features)
{
	struct virtio_device *vdev;
	struct rproc_vdev *rvdev;

	rvdev = rproc_setup_rproc_vdev(rproc,
			virtio_id, vq_number, vq_length,
			features);
	if (!rvdev)
		return NULL;

	vdev = &rvdev->vdev;
	vdev->index = rvdev->notifyid;
	vdev->virtio_id = virtio_id;
	vdev->vq_number = vq_number;
	vdev->vq_length = vq_length;
	vdev->features = features;
	vdev->priv = NULL;
	vdev->config = &s_vdev_cfg_ops;

	return vdev;
}

static void *rproc_alloc(u32 addr, u32 rsc_sz, u32 fifo_sz,
			 u32 rlock, u32 wlock)
{
	u32 entry_size = 0, table_size, vdev_entries;
	struct rproc *rproc;
	void *tbl_addr = (void *)DRAM_IO(addr);

	table_size = rsc_sz - fifo_sz * 2;
	entry_size = sizeof(struct rsc_hdr) + sizeof(struct rsc_vdev)
			+ sizeof(struct rsc_vdev_vring) * RVDEV_NUM_VRINGS
			+ RPROC_VDEV_MMIO_SIZE;

	/*
	 * Each Entry has one u32 offset, one rsc header, and
	 * one rsc vdev.
	 */
	vdev_entries = (table_size - sizeof(struct rsc_table))
				/ (sizeof(u32) + entry_size);
	if (vdev_entries > RPROC_MAX_VDEV_NUM)
		vdev_entries = RPROC_MAX_VDEV_NUM;

	rproc = pvPortMalloc(sizeof(*rproc) +
			sizeof(struct rproc_vdev) * vdev_entries +
			sizeof(void *) * vdev_entries * RVDEV_NUM_VRINGS);
	if (!rproc) {
		DebugMsg("allocate rproc m3 failed!\r\n");
		return NULL;
	}

	memset(tbl_addr, 0, rsc_sz);
	rproc->table = tbl_addr;
	rproc->table_size = table_size;
	rproc->table->ver = 0x1;
	rproc->table->num = 0;
	rproc->vdev_entries = vdev_entries;
	rproc->vdev_entry_size = entry_size;
	rproc->vdev_notifyid_index = RPROC_VDEV_MIN_ID;
	rproc->vq_notifyid_index = RPROC_VRING_MIN_ID;
	rproc->fifo_base = tbl_addr + table_size;
	rproc->fifo_sz = fifo_sz;
	rproc->fifo_rlock = rlock;
	rproc->fifo_wlock = wlock;
	rproc->vq_entries = vdev_entries * RVDEV_NUM_VRINGS;
	rproc->vrings = (struct rproc_vring **)&rproc->rvdevs[vdev_entries];

	LOG("RPROC info:\r\n"
		"rsc table       : 0x%x size :%uBytes\r\n"
		"fifo base       : 0x%x channel size :%uBytes\r\n"
		"fifo read lock  : %u write lock :%u\r\n"
		"vdev entries    : %u per entry size :%uBytes\r\n"
		"vdev notify id  : %u\r\n"
		"vring notify id : %u\r\n",
		rproc->table, rproc->table_size,
		rproc->fifo_base, rproc->fifo_sz,
		rproc->fifo_rlock, rproc->fifo_wlock,
		rproc->vdev_entries, rproc->vdev_entry_size,
		rproc->vdev_notifyid_index, rproc->vq_notifyid_index);
	return rproc;
}

extern int virtio_rpmsg_bus_init(struct rproc *rproc);
extern int virtio_can_init(struct rproc *rproc);
static void __rproc_init_virtio_devices(struct rproc *rproc)
{
	virtio_rpmsg_bus_init(rproc);
	virtio_can_init(rproc);
};

static void prvRemoteprocTask( void *pvParameters )
{
	struct rproc *rproc = pvParameters;
	struct fifo_buffer *fifo;
	u32 notifyid;
	int err;

	__rproc_init_virtio_devices(rproc);

	fifo = &rproc->r_fifo;
	for( ;; ) {
		/* wait isr to release semaphore */
		if (xSemaphoreTake(rproc->xSemaphore, portMAX_DELAY) == pdFALSE)
			continue;

		do {
			err = fifo_read(fifo, &notifyid, sizeof(notifyid));
			if (err)
				break;

			/* We will handle vq, vdev, vbus in different route */
			rproc_vq_interrupt(rproc, notifyid);
		} while (1);

		NVIC_EnableIRQ(INT_IPC_TRGT2_INIT1_INTR1_IRQn);
	}
}

extern void virtio_rpmsg_bus_deinit(struct rproc *rproc);
extern void virtio_can_deinit(struct rproc *rproc);
void vTaskRemoteprocDelete(void)
{
	struct rproc *rproc = ipc_rprocs[RPROC_A7NS_M3_1];

	NVIC_DisableIRQ(INT_IPC_TRGT2_INIT1_INTR1_IRQn);

	vTaskDelete(rproc->xTask);

	vSemaphoreDelete(rproc->xSemaphore);

	vSemaphoreDelete(rproc->xMutex);

	virtio_rpmsg_bus_deinit(rproc);
	virtio_can_deinit(rproc);

	vPortFree(rproc);
}

int vTaskRemoteprocCreate(void)
{
	int ret;
	struct rproc *rproc;

	rproc = rproc_alloc(RPROC_M3_RSC_ADDR,
				RPROC_M3_RSC_SIZE, RPROC_M3_FIFO_SIZE,
				M3_TO_A7NS0_RL, M3_TO_A7NS0_WL);
	if (!rproc)
		return -ENOMEM;

	ret = rproc_init_rsc(rproc);
	if (ret) {
		DebugMsg("rsc table initialized failed!\r\n");
		goto failed;
	}

	rproc->xSemaphore = xSemaphoreCreateBinary();
	if (rproc->xSemaphore == NULL) {
		DebugMsg("create remoteproc semaphore failed!\r\n");
		goto failed;
	}

	rproc->xMutex = xSemaphoreCreateMutex();
	if (rproc->xMutex == NULL) {
		DebugMsg("create remoteproc mutex failed!\r\n");
		goto failed;
	}

	NVIC_SetPriority(INT_IPC_TRGT2_INIT1_INTR1_IRQn, API_SAFE_INTR_PRIO_0);
	NVIC_EnableIRQ(INT_IPC_TRGT2_INIT1_INTR1_IRQn);

	ret = xTaskCreate(prvRemoteprocTask, "rproc", M3_RPROC_STACK_SIZE * 2,
			(void *)rproc, tskIDLE_PRIORITY + 1, &rproc->xTask);
	if (ret != pdTRUE) {
		DebugMsg("create remoteproc task failed!ret=%d\r\n", ret);
		goto failed;
	}

	ipc_rprocs[RPROC_A7NS_M3_1] = rproc;

failed:
	return ret;
}
