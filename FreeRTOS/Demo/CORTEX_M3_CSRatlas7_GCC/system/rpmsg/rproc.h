/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define RPROC_M3_RSC_ADDR	0x05600000
#define RPROC_M3_RSC_SIZE	0x10000
#define RPROC_M3_FIFO_SIZE	0x1000

/* indicate this virtio device is attached on backend rproc bus */
#define VIRTIO_RPROC_F_BACK	19
/* indicate this virtio device is attached on frontend rproc bus */
#define VIRTIO_RPROC_F_FRONT	20

/*
*                         NOTIFYID 32 bits
* x xxx xxxx
* |  |   |--------- bit(0~15)  vq id in VQ TYPE, VDEV ID in VDEV TYPE,
* |  |------------- bit(16~27) MMIO offset 0 ~ 0xFFF
* |---------------- bit(28~31) NOTIFYID TYPE (0:VQ, 2:VBUS, 4:VDEV)
*/
#define RPROC_VRING_ALIGN_PAGE		PAGE_SIZE
#define RPROC_VDEV_MMIO_SIZE		32
#define RPROC_MAX_VDEV_NUM		16

/* the version of this mmio space */
#define MMIO_VERSION		0x00
/* the features of vdev */
#define MMIO_FEATURES		0x04
/* virtqueue had been configed in backend, included shared-memory for vring.
 * notify backend to config its virtqueue. */
#define MMIO_FRONT_VQ_READY	0x08
/* frontend had been configed, notify backend that it's online */
#define MMIO_FRONT_ONLINE	0x0C
/* frontend is going to be offline, notify backend to cleanup
 * resource. */
#define MMIO_FRONT_OFFLINE	0x10
/* backend had been configed, notify frontend that it's online */
#define MMIO_BACK_ONLINE	0x14
/* backend is offline */
#define MMIO_BACK_OFFLINE	0x18
/* device suspend */
#define MMIO_SUSPEND		0x1C
/* device resume */
#define MMIO_RESUME		0x20
/* device freeze */
#define MMIO_FREEZE		0x24
/* device restore */
#define MMIO_RESTORE		0x28
/* the private data address of rproc vdev */
#define MMIO_PRIV_DATA		0x2C
/* the size of private data */
#define MMIO_PRIV_SIZE		0x30
/* the last RPROC common MMIO offset,
 * the vdev customized MMIO could use this offset as start base
 */
#define MMIO_CONFIG_BASE	0x40

/* return vq or vdev notify id */
#define GET_NOTIFY_ID(x)    ((x) & 0x0000FFFF)

/* return MMIO offset */
#define GET_MMIO_OFFSET(x)  (((x) >> 16) & 0x00000FFF)

/* is a vq notify */
#define IS_VQ_NOTIFY(x)     (!((x) & 0xF0000000))
/* is a bus notify */
#define IS_BUS_NOTIFY(x)    ((x) & 0x20000000)
/* is a vdev notify */
#define IS_DEV_NOTIFY(x)    ((x) & 0x40000000)

/* combine a bus notify id */
#define MK_BUS_NOTIFYID(x)    (((x) & 0xFFFF) | 0x20000000)
/* combine a vdev notify id */
#define MK_DEV_NOTIFYID(x, y) \
(((x) & 0xFFFF) | (((y) & 0xFFF) << 16) | 0x40000000)

#define RVDEV_NUM_VRINGS	2

/* The Max vdev numbers this rproc supported */
#define RPROC_MAX_VDEVS		32
/* The basic vdev notify id */
#define RPROC_VDEV_MIN_ID	0x20
/* The basic vring notify id */
#define RPROC_VRING_MIN_ID	0x80

/* Definition of Device State MSG ID (0 ~ 0x1F)
 * MSG ID >= 0x20 is new device arrive msg, and
 * the id is the device's index.
 */
#define RPROC_DEV_STATE_OFFLINE		0
#define RPROC_DEV_STATE_SUSPENDED	1
#define RPROC_DEV_STATE_RUNNING		2
#define RPROC_DEV_STATE_CRASHED		3
#define RPROC_DEV_STATE_LAST		0x1F

/**
 * struct rsc_table - rproc rsc table header
 * @ver: version number
 * @num: number of rsc entries
 * @reserved: reserved (must be zero)
 * @offset: array of offsets pointing at the various rsc entries
 */
struct rsc_table {
	u32 ver;
	u32 num;
	u32 reserved[2];
	u32 offset[0];
} __packed;

/**
 * struct rsc_hdr - rproc rsc entry header
 * @type: rsc type
 * @data: rsc data
 */
struct rsc_hdr {
	u32 type;
	u8 data[0];
} __packed;

/**
 * enum rsc_type - types of rsc entries
 *
 * @RSC_CARVEOUT:   request for allocation of a physically contiguous
 *		    memory region.
 * @RSC_DEVMEM:     request to iommu_map a memory-based peripheral.
 * @RSC_TRACE:	    announces the availability of a trace buffer into which
 *		    the remote processor will be writing logs.
 * @RSC_VDEV:       declare support for a virtio device, and serve as its
 *		    virtio header.
 * @RSC_LAST:       just keep this one at the end
 */
enum rsc_type {
	RSC_CARVEOUT	= 0,
	RSC_DEVMEM	= 1,
	RSC_TRACE	= 2,
	RSC_VDEV	= 3,
	RSC_LAST	= 4,
};

#define RSC_ADDR_ANY	0xFFFFFFFF

/**
 * struct rsc_vdev_vring - vring descriptor entry
 * @da: da is the physical memory address of vring descriptor start address,
 * 	this memory is allocated by frontend OS.
 * @align: the alignment between the consumer and producer parts of the vring
 * @num: num of buffers supported by this vring (must be power of two)
 * @notifyid is a unique rproc-wide notify index for this vring. This notify
 *	index is used when kicking a remote processor, to let it know that
 *	this vring is triggered.
 * @reserved: reserved (must be zero)
 */
struct rsc_vdev_vring {
	u32 da;
	u32 align;
	u32 num;
	u32 notifyid;
	u32 reserved;
} __packed;

/**
 * struct rsc_vdev - virtio device header
 * @id: virtio device id (as in virtio_ids.h)
 * @notifyid: is a unique rproc-wide notify index for this vdev.
 *	This notify index is used when kicking a remote processor,
 *	to let it know that the status/features of this vdev have
 *	changes.
 * @dfeatures: specifies the virtio device features supported by
 *	the firmware
 * @gfeatures: is a place holder used by the host to write back
 *	the negotiated features that are supported by both sides.
 * @config_len: is the size of the virtio config space of this vdev.
 *	The config space lies in the resource table immediate after
 *	this vdev header.
 * @status: is a place holder where the host will indicate its
 *	virtio progress.
 * @num_of_vrings indicates how many vrings are described in
 *	this vdev header
 * @reserved: reserved (must be zero)
 * @vring is an array of @num_of_vrings entries of
 *	'struct fw_rsc_vdev_vring'.
 */
struct rsc_vdev {
	u32 id;
	u32 notifyid;
	u32 dfeatures;
	u32 gfeatures;
	u32 config_len;
	u8 status;
	u8 num_of_vrings;
	u8 reserved[2];
	struct rsc_vdev_vring vring[0];
} __packed;

struct rproc_vring {
	void *pa;
	dma_addr_t dma;
	u32 len;
	u32 align;
	u32 notifyid;
	struct rproc_vdev *rvdev;
	struct virtqueue *vq;
};

typedef int rproc_dev_online(void *dev, u32 offset);

struct rproc_vdev {
	struct rproc *rproc;
	struct virtio_device vdev;
	struct rproc_vring vring[RVDEV_NUM_VRINGS];
	u32 rsc_offset;

	u32 notifyid;
	u32 num_of_vring;
	u32 features;
	u32 status;
	rproc_dev_online *online;
};

struct fifo_buffer {
	u32 lock;
	u8 *buffer;
	u32 w_pos;
	u32 r_pos;
	u32 size;
	u32 *count; /* pointer to shared memory */
};

enum rproc_state {
	RPROC_OFFLINE	= 0,
	RPROC_SUSPENDED	= 1,
	RPROC_RUNNING	= 2,
	RPROC_CRASHED	= 3,
	RPROC_ALWAYS_ON = 4,
	RPROC_LAST	= 5,
};

struct rproc {
	TaskHandle_t xTask;
	SemaphoreHandle_t xSemaphore;
	SemaphoreHandle_t xMutex;
	u32 state;
	struct rsc_table *table;
	u32 table_size;
	u32 vdev_entries;
	u32 vdev_entry_size;
	u32 vdev_notifyid_index;
	u32 vq_notifyid_index;
	u32 vq_entries;
	void *fifo_base;
	u32 fifo_sz;
	u32 fifo_rlock;
	u32 fifo_wlock;
	struct fifo_buffer r_fifo;
	struct fifo_buffer w_fifo;
	struct rproc_vring **vrings;
	struct rproc_vdev rvdevs[0];
};

/* The hardware spinlocks used by rproc */
#define	M3_TO_A7NS0_RL	0x0
#define	M3_TO_A7NS0_WL	0x1
#define	M3_TO_A7NS1_RL	0x2
#define	M3_TO_A7NS1_WL	0x3

/* FIFO shared memory has been divided into 2 logical channels */
#define FIFO_LOGIC_CHN_0	0
#define FIFO_LOGIC_CHN_1	1

/* Possible values of HW_SPINLOCK_REG */
#define HW_SPINLOCK_LOCKED	0
#define HW_SPINLOCK_FREE	1
#define HW_SPINLOCK_DELAY	50

enum rproc_index {
    RPROC_A7S_M3_1,
    RPROC_A7S_M3_2,
    RPROC_A7NS_M3_1,
    RPROC_A7NS_M3_2,
    RPROC_KAS_M3_1,
    RPROC_KAS_M3_2,
    RPROC_TRGT2_MAX,
};

static inline struct rproc_vdev *vdev_to_rvdev(struct virtio_device *vdev)
{
	return container_of(vdev, struct rproc_vdev, vdev);
}

void rproc_set_online_handler(struct virtio_device *vdev,
				rproc_dev_online *online);

struct virtio_device *rproc_add_virtio_device(struct rproc *rproc,
				u32 virtio_id, u32 vq_number,
				u32 vq_length, u32 features);

int rproc_virtio_find_vqs(struct virtio_device *vdev, unsigned nvqs,
		       struct virtqueue *vqs[],
		       vq_callback_t *callbacks[],
		       const char *names[]);
