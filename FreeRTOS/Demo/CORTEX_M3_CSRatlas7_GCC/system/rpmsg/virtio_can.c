/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "ctypes.h"
#include "errno.h"
#include "debug.h"
#include "io.h"
#include "misclib.h"

/* Internal includes */
#include "virtio.h"
#include "vring.h"
#include "rproc.h"

#include "canbus.h"

/* CSR Private VIRTIO DEVICE IDs */
#define VIRTIO_ID_CSR_BASE	0x1000
#define VIRTIO_ID_CAN		(VIRTIO_ID_CSR_BASE | 0x09)

/* VIRTIO CAN FRAME type */
#define FRAME_TYPE_NORMAL	0x1000
#define FRAME_TYPE_FD		0x1001

/* CAN CTRL MODES */
#define CAN_CTRLMODE_LOOPBACK		0x01	/* Loopback mode */
#define CAN_CTRLMODE_LISTENONLY		0x02	/* Listen-only mode */
#define CAN_CTRLMODE_3_SAMPLES		0x04	/* Triple sampling mode */
#define CAN_CTRLMODE_ONE_SHOT		0x08	/* One-Shot mode */
#define CAN_CTRLMODE_BERR_REPORTING	0x10	/* Bus-error reporting */
#define CAN_CTRLMODE_FD			0x20	/* CAN FD mode */
#define CAN_CTRLMODE_PRESUME_ACK	0x40	/* Ignore missing CAN ACKs */

/* VIRTIO CAN MMIO */
#define CAN_MMIO_CLOCK_RATE	0
#define CAN_MMIO_CTRL_MODE	4
#define CAN_MMIO_ECHO_SKB_MAX	8
#define CAN_MMIO_MAX_FRAME_SIZE	12
#define CAN_MMIO_BIT_RATE	16

/* controller area network (CAN) definitions */
/* special address description flags for the CAN_ID */
#define CAN_EFF_FLAG 0x80000000U /* EFF/SFF is set in the MSB */
#define CAN_RTR_FLAG 0x40000000U /* remote transmission request */
#define CAN_ERR_FLAG 0x20000000U /* error message frame */

/* valid bits in CAN ID for frame formats */
#define CAN_SFF_MASK 0x000007FFU /* standard frame format (SFF) */
#define CAN_EFF_MASK 0x1FFFFFFFU /* extended frame format (EFF) */
#define CAN_ERR_MASK 0x1FFFFFFFU /* omit EFF, RTR, ERR flags */

/*
 * Controller Area Network Identifier structure
 *
 * bit 0-28	: CAN identifier (11/29 bit)
 * bit 29	: error message frame flag (0 = data frame, 1 = error message)
 * bit 30	: remote transmission request flag (1 = rtr frame)
 * bit 31	: frame format flag (0 = standard 11 bit, 1 = extended 29 bit)
 */
#define CAN_SFF_ID_BITS		11
#define CAN_EFF_ID_BITS		29

/* Flags' bit in ID */
#define CAN_ERR_FLAG_BIT	29
#define CAN_RTR_FLAG_BIT	30
#define CAN_IDE_FLAG_BIT	31

/* CAN payload length and DLC definitions according to ISO 11898-1 */
#define CAN_MAX_DLC 8
#define CAN_MAX_DLEN 8

/* CAN FD payload length and DLC definitions according to ISO 11898-7 */
#define CANFD_MAX_DLC 15
#define CANFD_MAX_DLEN 64

/**
 * struct can_frame - basic CAN frame structure
 * @can_id:  CAN ID of the frame and CAN_*_FLAG flags, see canid_t definition
 * @can_dlc: frame payload length in byte (0 .. 8) aka data length code
 *           N.B. the DLC field from ISO 11898-1 Chapter 8.4.2.3 has a 1:1
 *           mapping of the 'data length code' to the real payload length
 * @data:    CAN frame payload (up to 8 byte)
 */
struct can_frame {
	u32 can_id;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
	u8 can_dlc; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */
	u8 data[CAN_MAX_DLEN] __attribute__((aligned(8)));
};

struct canfd_frame {
	u32 can_id;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
	u8 len;     /* frame payload length in byte */
	u8 flags;   /* additional flags for CAN FD */
	u8 __res0;  /* reserved / padding */
	u8 __res1;  /* reserved / padding */
	u8 data[CANFD_MAX_DLEN] __attribute__((aligned(8)));
};

struct vcan_frame_header
{
	u32 type;
	u32 len;
	u32 channel;
	u32 res;
};

struct vcan_frame {
	struct vcan_frame_header header;
	union {
		u8 raw[8];
		struct can_frame cf;
		struct canfd_frame cfd;
	};
};

/*
 * virtio canbus device instance
 */
struct virtio_can {
	int status;
	struct virtio_device *vdev;
	struct virtqueue *rvq, *svq;
	bool connected;
	/* To protect the vq operations for the control channel */
	SemaphoreHandle_t rvq_lock;
	SemaphoreHandle_t tvq_lock;
	SemaphoreHandle_t sendq;
	u32 max_frame;
	u32 rx_packets, tx_packets;
};

static struct virtio_can s_vcan;

void virtio_can_show_statistic(void)
{
	DebugMsg("VIRTIO CAN RX:<%d> TX:<%d>\r\n",
			s_vcan.rx_packets, s_vcan.tx_packets);
}

static int __virtio_can_recv_single(struct virtio_can *vcan,
			     struct can_frame *cf_msg, u32 len)
{
	CAN_MSGOBJECT Msg;
	DebugMsg("RECV CAN_FRAME: id<%x> dlc<%d>\r\n"
		"data <%02x> <%02x> <%02x> <%02x> <%02x> <%02x> <%02x> <%02x>\r\n",
		cf_msg->can_id, cf_msg->can_dlc, cf_msg->data[0],
		cf_msg->data[1], cf_msg->data[2], cf_msg->data[3],
		cf_msg->data[4], cf_msg->data[5], cf_msg->data[6],
		cf_msg->data[7]);

	if (cf_msg->can_id & BIT(CAN_IDE_FLAG_BIT)) {
		Msg.ide = true;
		Msg.id = cf_msg->can_id & 0x1FFFFFFF;
	} else {
		Msg.ide = false;
		Msg.id = cf_msg->can_id & 0x7FF;
	}

	if (cf_msg->can_id & BIT(CAN_RTR_FLAG_BIT))
		Msg.rtr = true;
	else
		Msg.rtr = false;

	memcpy((void *)Msg.data, (void *)cf_msg->data, CAN_MAX_DLEN);
	/* TODO: Call CANBUS TX API */
	return 0;
}

/* called when an rx buffer is used, and it's time to digest a message */
static void virtio_can_recv_done(struct virtqueue *rvq)
{
	struct virtio_can *vcan = rvq->vdev->priv;
	struct can_frame *cf_msg;
	u32 idx, len, msgs_received = 0;
	int err;

	cf_msg = virtqueue_get_avail_buf(rvq, &idx, &len);
	if (!cf_msg) {
		LOG("uhm, incoming signal, but no used buffer ?\r\n");
		return;
	}

	while (cf_msg) {
		err = __virtio_can_recv_single(vcan, cf_msg, len);
		if (err)
			break;

		virtqueue_set_used_buf(rvq, idx, len);
		vcan->rx_packets++;
		msgs_received++;

		cf_msg = virtqueue_get_avail_buf(rvq, &idx, &len);
	};

	/* tell the remote processor we added another available rx buffer */
	if (msgs_received)
		virtqueue_kick(vcan->rvq);
}

static void virtio_can_xmit_done(struct virtqueue *svq)
{
	struct virtio_can *vcan = svq->vdev->priv;

	/* wake up potential senders that are waiting for a tx buffer */
	xSemaphoreGive(vcan->sendq);
}

static void *__virtio_can_get_xmit_buffer(struct virtio_can *vcan,
				u32 *idx, u32 *len)
{
	void *ret;

	/* support multiple concurrent senders */
	xSemaphoreTake(vcan->tvq_lock, portMAX_DELAY);

	ret = virtqueue_get_avail_buf(vcan->svq, idx, len);

	xSemaphoreGive(vcan->tvq_lock);

	return ret;
}

int virtio_can_send_frame(u32 chn, void *data, bool wait)
{
	struct virtio_can *vcan = &s_vcan;
	CAN_MSGOBJECT *cmsg = data;
	struct vcan_frame *vframe;
	unsigned int buf_idx, buf_len;

	if (vcan->connected == false)
		return -ENODEV;

	/* grab a buffer */
	vframe = __virtio_can_get_xmit_buffer(vcan, &buf_idx, &buf_len);
	if (!vframe && !wait)
		return -ENOMEM;

	while (!vframe) {
		xSemaphoreTake(vcan->sendq, portMAX_DELAY);
		vframe = __virtio_can_get_xmit_buffer(vcan, &buf_idx, &buf_len);
	}

	memset(vframe, 0, vcan->max_frame);
	vframe->header.len = sizeof(struct can_frame);
	vframe->header.type = FRAME_TYPE_NORMAL;
	vframe->header.channel = chn;
	memcpy(vframe->cf.data, (void *)cmsg->data, CAN_MAX_DLEN);
	vframe->cf.can_dlc = cmsg->dlc;
	vframe->cf.can_id = cmsg->id;

	if (cmsg->ide)
		vframe->cf.can_id |=  BIT(CAN_IDE_FLAG_BIT);
	if (cmsg->rtr)
		vframe->cf.can_id |=  BIT(CAN_RTR_FLAG_BIT);

	xSemaphoreTake(vcan->tvq_lock, portMAX_DELAY);

	virtqueue_set_used_buf(vcan->svq, buf_idx, buf_len);

	/* tell the remote processor it has a pending message to read */
	virtqueue_kick(vcan->svq);
	vcan->tx_packets++;

	xSemaphoreGive(vcan->tvq_lock);

	return 0;
}


static int __virtio_can_online(struct virtio_device *vdev,
				struct virtio_can *vcan)
{
	vq_callback_t *vq_cbs[2];
	const char *names[2];
	struct virtqueue *vqs[2];
	int err;

	vq_cbs[0] = virtio_can_xmit_done;
	vq_cbs[1] = virtio_can_recv_done;
	names[0] = "txq";
	names[1] = "rxq";

	err = rproc_virtio_find_vqs(vdev, 2, vqs, vq_cbs, names);
	if (err)
		return err;

	vcan->svq = vqs[0];
	vcan->rvq = vqs[1];
	vcan->connected = true;

	DebugMsg("virtio can is online\r\n");

	return 0;
}

static int virtio_can_online(struct virtio_device *vdev, u32 offset)
{
	int ret;
	struct virtio_can *vcan = vdev->priv;

	/* Turn rpmsg online */
	ret = __virtio_can_online(vdev, vcan);
	if (ret) {
		LOG("turn virtio can failed!err:%d\r\n", ret);
		return ret;
	}

	return ret;
}

int virtio_can_init(struct rproc *rproc)
{
	struct virtio_device *vdev;

	vdev = rproc_add_virtio_device(rproc, VIRTIO_ID_CAN,
				2, 256, 0);
	if (!vdev) {
		LOG("vdev#can initialized failed!\r\n");
		return -EINVAL;
	}

	DebugMsg("start probe virtio canbus backend\r\n");
	s_vcan.connected = false;
	s_vcan.vdev = vdev;
	s_vcan.max_frame = ALIGN(sizeof(struct vcan_frame), 8);

	virtio_cwrite32(vdev, CAN_MMIO_CLOCK_RATE, 0);
	virtio_cwrite32(vdev, CAN_MMIO_BIT_RATE, 1024 * 1024);
	virtio_cwrite32(vdev, CAN_MMIO_CTRL_MODE, 0);
	virtio_cwrite32(vdev, CAN_MMIO_ECHO_SKB_MAX, 64);
	virtio_cwrite32(vdev, CAN_MMIO_MAX_FRAME_SIZE, s_vcan.max_frame);

	LOG("OUT QUEUE slot:256 persz:%d(%d)\r\n",
			s_vcan.max_frame, sizeof(struct vcan_frame));

	s_vcan.sendq = xSemaphoreCreateBinary();
	if (s_vcan.sendq == NULL) {
		LOG("create rpmsg bus sendq failed!\r\n");
		return -EINVAL;
	}

	s_vcan.rvq_lock = xSemaphoreCreateMutex();
	if (s_vcan.rvq_lock == NULL) {
		LOG("create rpmsg bus tx_lock failed!\r\n");
		return -EINVAL;
	}

	s_vcan.tvq_lock = xSemaphoreCreateMutex();
	if (s_vcan.tvq_lock == NULL) {
		LOG("create rpmsg bus endpoints_lock failed!\r\n");
		return -EINVAL;
	}

	s_vcan.rx_packets = 0;
	s_vcan.tx_packets = 0;

	vdev->priv = &s_vcan;

	rproc_set_online_handler(vdev, (rproc_dev_online *)virtio_can_online);

	return 0;
}
extern void* virtqueue_get_vvq(struct virtqueue *_vq);
void virtio_can_deinit(struct rproc *rproc)
{
	struct virtio_device *vdev;
	struct rproc_vdev *rvdev;

	vdev = s_vcan.vdev;
	rvdev = vdev_to_rvdev(vdev);

	if(s_vcan.sendq != NULL)
	{
		vSemaphoreDelete(s_vcan.sendq);
		s_vcan.sendq = NULL;
	}

	if(s_vcan.rvq_lock != NULL)
	{
		vSemaphoreDelete(s_vcan.rvq_lock);
		s_vcan.rvq_lock = NULL;
	}

	if(s_vcan.tvq_lock != NULL)
	{
		vSemaphoreDelete(s_vcan.tvq_lock);
		s_vcan.tvq_lock = NULL;
	}

	if( RPROC_DEV_STATE_RUNNING == rvdev->status )
	{	/* free the 2 vrings */
		vPortFree(virtqueue_get_vvq(rvdev->vring[0].vq));
		vPortFree(virtqueue_get_vvq(rvdev->vring[1].vq));
	}
}
