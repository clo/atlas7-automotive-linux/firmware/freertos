/* Virtio ring implementation.
 *
 *  Copyright 2007 Rusty Russell IBM Corporation
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Platform includes. */
#include "ctypes.h"
#include "errno.h"
#include "io.h"
#include "debug.h"

/* Internal includes */
#include "virtio.h"
#include "vring.h"
#include "rproc.h"
#include "rpmsg.h"


struct vring_virtqueue
{
	struct virtqueue vq;

	/* Actual memory layout for this queue */
	struct vring vring;

	/* Can we use weak barriers? */
	bool weak_barriers;

	/* Other side has made a mess, don't try any more. */
	bool broken;

	/* Host supports indirect buffers */
	bool indirect;

	/* Host publishes avail event idx */
	bool event;

	/* Head of free buffer list. */
	unsigned int free_head;
	/* Number we've added since last sync. */
	unsigned int num_added;

	/* Last available index we've seen. */
	u16 last_avail_idx;

	/* Last used index we've seen. */
	u16 last_used_idx;

	/* How to notify other side. FIXME: commonalize hcalls! */
	bool (*notify)(struct virtqueue *vq);

	/* Tokens for callbacks. */
	void *data[];
};

#define to_vvq(_vq) container_of(_vq, struct vring_virtqueue, vq)

void* virtqueue_get_vvq(struct virtqueue *_vq)
{
	return (void*)to_vvq(_vq);
}

bool virtqueue_kick(struct virtqueue *_vq)
{
	struct vring_virtqueue *vq = to_vvq(_vq);

	if (vq->broken)
		return false;

	/* Check If remote side disable callback for this vq */
	if (vq->vring.avail->flags & VRING_AVAIL_F_NO_INTERRUPT)
		return false;

	/* Check If local side disable event for this vq */
	if (vq->vring.used->flags & VRING_USED_F_NO_NOTIFY)
		return false;


	if (!vq->notify(_vq)) {
		vq->broken = true;
		return false;
	}

	return true;
}

void* virtqueue_get_avail_buf(struct virtqueue *_vq,
				u32 *idx, u32 *len)
{
	void *buf;
	u16 head;
	struct vring_virtqueue *vq = to_vvq(_vq);

	LOG("getAvailBuf vq: %X %d %d %d %X %X\r\n",
		vq, vq->last_avail_idx, vq->vring.avail->idx, vq->vring.num,
		&vq->vring.avail, vq->vring.avail);

	/* There's nothing available? */
	if (vq->last_avail_idx == vq->vring.avail->idx) {
		/* We need to know about added buffers */
		vq->vring.used->flags &= ~VRING_USED_F_NO_NOTIFY;
		return NULL;
	}
	/*
	 * Grab the next descriptor number they're advertising, and increment
	 * the index we've seen.
	 */
	head = vq->vring.avail->ring[vq->last_avail_idx++ % vq->vring.num];

	buf = (void *)TO_MCU_IO_DRAM((u32)vq->vring.desc[head].addr);
	*len = vq->vring.desc[head].len;
	*idx = head;

	LOG("VQ#%d Get Avail:%d %d %X\r\n", _vq->index, *idx, *len, buf);
	return buf;
}

bool virtqueue_set_used_buf(struct virtqueue *_vq,
			unsigned int idx, unsigned int len)
{
	struct vring_virtqueue *vq = to_vvq(_vq);
	struct vring_used_elem *used;

	if ((idx > vq->vring.num) /* || (idx < 0) */)
		return false;

	/*
	* The virtqueue contains a ring of used buffers.  Get a pointer to the
	* next entry in that used ring.
	*/
	used = &vq->vring.used->ring[vq->vring.used->idx % vq->vring.num];
	used->id = idx;
	used->len = len;

	vq->last_used_idx++;
	vq->vring.used->idx = vq->last_used_idx;

	return true;
}

u64 virtqueue_get_data_address(struct virtqueue *_vq, u32 idx)
{
	struct vring_virtqueue *vq = to_vvq(_vq);

	if (idx >= vq->vring.num)
		return 0;

	return vq->vring.desc[idx].addr;
}


int vring_interrupt(int irq, void *_vq)
{
	struct vring_virtqueue *vq = to_vvq(_vq);

	if (vq->broken)
		return 0;

	if (vq->vq.callback)
		vq->vq.callback(&vq->vq);

	return 0;
}

struct virtqueue *vring_new_virtqueue(unsigned int index,
				      unsigned int num,
				      unsigned int vring_align,
				      struct virtio_device *vdev,
				      bool weak_barriers,
				      void *phyaddr,
				      bool (*notify)(struct virtqueue *),
				      vq_callback_t *callback,
				      const char *name)
{
	struct vring_virtqueue *vq;
	unsigned int i;

	/* We assume num is a power of 2. */
	if (num & (num - 1)) {
		LOG("Bad virtqueue length %u\r\n", num);
		return NULL;
	}

	vq = pvPortMalloc(sizeof(*vq) + sizeof(void *) * num);
	if (!vq)
		return NULL;

	vring_init(&vq->vring, num, phyaddr, vring_align);
	vq->vq.callback = callback;
	vq->vq.vdev = vdev;
	vq->vq.name = name;
	vq->vq.num_free = num;
	vq->vq.index = index;
	vq->notify = notify;
	vq->weak_barriers = weak_barriers;
	vq->broken = false;
	vq->last_avail_idx = 0;
	vq->last_used_idx = 0;
	vq->num_added = 0;

	vq->indirect = virtio_has_feature(vdev, VIRTIO_RING_F_INDIRECT_DESC);
	vq->event = virtio_has_feature(vdev, VIRTIO_RING_F_EVENT_IDX);

	/* No callback?  Tell other side not to bother us. */
	if (!callback)
		vq->vring.avail->flags |= VRING_AVAIL_F_NO_INTERRUPT;

	/* Put everything in free lists. */
	vq->free_head = 0;
	for (i = 0; i < num - 1; i++) {
		vq->vring.desc[i].next = i + 1;
		vq->data[i] = NULL;
	}

	vq->data[i] = NULL;

	return &vq->vq;
}

void vring_del_virtqueue(struct virtqueue *vq)
{
	vPortFree(to_vvq(vq));
}

/* Manipulates transport-specific feature bits. */
void vring_transport_features(struct virtio_device *vdev)
{
	unsigned int i;

	for (i = VIRTIO_TRANSPORT_F_START; i < VIRTIO_TRANSPORT_F_END; i++) {
		switch (i) {
		case VIRTIO_RING_F_INDIRECT_DESC:
			break;
		case VIRTIO_RING_F_EVENT_IDX:
			break;
		default:
			/* We don't understand this bit. */
			clear_bit(i, vdev->features);
		}
	}
}
