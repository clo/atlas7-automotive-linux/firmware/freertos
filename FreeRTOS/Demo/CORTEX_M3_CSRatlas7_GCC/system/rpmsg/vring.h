#ifndef __VRING_H__
#define __VRING_H__

#define VRING_DESC_F_NEXT	1
#define VRING_DESC_F_WRITE	2
#define VRING_DESC_F_INDIRECT	4

#define VRING_USED_F_NO_NOTIFY	1
#define VRING_AVAIL_F_NO_INTERRUPT	1

#define VIRTIO_RING_F_INDIRECT_DESC	28
#define VIRTIO_RING_F_EVENT_IDX		29

struct virtqueue;
typedef void vq_callback_t(struct virtqueue *);

/* Virtio ring descriptors: 16 bytes.  These can chain together via "next". */
struct vring_desc {
	u64 addr;
	u32 len;
	u16 flags;
	u16 next;
};

struct vring_avail {
	u16 flags;
	u16 idx;
	u16 ring[];
};

struct vring_used_elem {
	u32 id;
	u32 len;
};

struct vring_used {
	u16 flags;
	u16 idx;
	struct vring_used_elem ring[];
};

struct vring {
	unsigned int num;
	struct vring_desc *desc;
	struct vring_avail *avail;
	struct vring_used *used;
};

/* We publish the used event index at the end of the available ring, and vice
 * versa. They are at the end for backwards compatibility. */
#define vring_used_event(vr) ((vr)->avail->ring[(vr)->num])
#define vring_avail_event(vr) (*(u16 *)&(vr)->used->ring[(vr)->num])

static inline void vring_init(struct vring *vr, unsigned int num, void *p,
			      unsigned long align)
{
	vr->num = num;
	vr->desc = p;
	vr->avail = p + num * sizeof(struct vring_desc);
	vr->used = (void *)(((unsigned long)&vr->avail->ring[num] + sizeof(u16)
		+ align-1) & ~(align - 1));
}

static inline unsigned vring_size(unsigned int num, unsigned long align)
{
	return ((sizeof(struct vring_desc) * num + sizeof(u16) * (3 + num)
		 + align - 1) & ~(align - 1))
		+ sizeof(u16) * 3 + sizeof(struct vring_used_elem) * num;
}

static inline int vring_need_event(u16 event_idx, u16 new_idx, u16 old)
{
	return (u16)(new_idx - event_idx - 1) < (u16)(new_idx - old);
}

struct virtqueue {
	vq_callback_t *callback;
	const char *name;
	struct virtio_device *vdev;
	unsigned int index;
	unsigned int num_free;
	void *priv;
};

bool virtqueue_kick(struct virtqueue *vq);
int vring_interrupt(int irq, void *_vq);

void* virtqueue_get_avail_buf(struct virtqueue *_vq, u32 *idx, u32 *len);

bool virtqueue_set_used_buf(struct virtqueue *_vq,
			unsigned int idx, unsigned int len);

struct virtqueue *vring_new_virtqueue(unsigned int index,
				      unsigned int num,
				      unsigned int vring_align,
				      struct virtio_device *vdev,
				      bool weak_barriers,
				      void *phyaddr,
				      bool (*notify)(struct virtqueue *),
				      vq_callback_t *callback,
				      const char *name);

u64 virtqueue_get_data_address(struct virtqueue *_vq, u32 idx);


#endif /* __VRING_H__ */
