/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "soc_pulsec.h"
#include "soc_timer.h"
#include "soc_ioc.h"
#include "soc_cache.h"
#include "gpt.h"
#include "pulsec.h"
#include "debug.h"
#include "misclib.h"
#include "FreeRTOS.h"

#define DR_SENSOR_DATA_BUFFER_ADDRESS  0x054e0000

/* 50 Hz sample rate */
#define SIRFDRIVER_SAMPLE_FREQUENCE 50

/* to hold data sets in 3 seconds */
#define BUFFER_LEN (3*SIRFDRIVER_SAMPLE_FREQUENCE)

typedef struct DR_SENSOR_DATA_SET_tag {
	UINT32 data_set_time_tag;		/*  time in [ms] */
	UINT16 valid_data_indication;	/*  bitmap of valid sensor data:   bit[0], rev  bit[1],odo  others,TBD */
	INT16 gyro[3];					/*gyro[3], reserved */
	INT16 acc[3];					/*acc[3], reserved */
	INT16 odo;						/* odometer  signal */
	INT16 rev; 						/* reverse signal */
	INT16 reserved;
} DR_SENSOR_DATA_SET;

typedef struct DR_SENSOR_DATA_BUFFER_tag {
	UINT32 buffer_head;
	UINT32 reserved;
	DR_SENSOR_DATA_SET data_set[ BUFFER_LEN ];		/* buffer length according to sample rate*/
} DR_SENSOR_DATA_BUFFER;

static DR_SENSOR_DATA_BUFFER * const pDrSensorBuffer = (DR_SENSOR_DATA_BUFFER *)DR_SENSOR_DATA_BUFFER_ADDRESS;
static const xGptParameter xGptParam_M3 =
{
	.TIMER_DIV=0,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/SIRFDRIVER_SAMPLE_FREQUENCE
};
static  void  xGptM3Timer1ISR ( eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
	UINT32 head = pDrSensorBuffer->buffer_head;
	DR_SENSOR_DATA_SET* pDataSet = &(pDrSensorBuffer->data_set[head]);

	pDataSet->odo = SOC_REG(A7DA_PULSEC_LWH_CNT);
	pDataSet->rev = (SOC_REG(0x13300004)>>7)&0x01;	/* GPIO_VDIFM_CFG_GNSS_1 : GPIO_1 PC_DIR */

	pDataSet->data_set_time_tag = SOC_REG(A7DA_TIMER_COUNTER_2);
	pDataSet->valid_data_indication = 0x3;

	head ++;
	if(head >= BUFFER_LEN)
	{
		head = 0;
	}

	/* done of the sample job */
	pDrSensorBuffer->buffer_head = head;

	vDcacheFlush();
}

void SirfDrive3_Init(void)
{
	int32_t ret;
	memset(pDrSensorBuffer,0,sizeof(DR_SENSOR_DATA_BUFFER));

	/* set PIN GPIO_VDIFM_CFG_GNSS_1 : GPIO_1 PC_DIR as GPIO mode */
	SOC_REG(A7DA_IOCTOP_FUNC_SEL_16_REG_CLR) = SHIFT_L(FUNC_MASK, 4);
	SOC_REG(0x13300004) = 0;	/* set as Input */

	vPulseCounterInit();
	vPulseCounterStart(SIRFSOC_PULSEC_FORWARD);

	ret = xGptRegisterInterrupt(xGptM3Timer1ISR,eGptOsTimerM3,1,NULL);

	if(0==ret)
	{
		ret = xGptStartTimer(eGptOsTimerM3,1,&xGptParam_M3);

		if(0==ret)
		{
			DebugMsg("SirfDriver probe okay! DATA_SIZE=%d\r\n",sizeof(DR_SENSOR_DATA_BUFFER));
		}
	}

	if(0!=ret)
	{
		DebugMsg("SirfDriver probe failed!\r\n");
	}
}

void SirfDrive3_DeInit(void)
{
	vGptStopTimer(eGptOsTimerM3,1);

	vGptUnRegisterInterrupt(eGptOsTimerM3,1);
}
