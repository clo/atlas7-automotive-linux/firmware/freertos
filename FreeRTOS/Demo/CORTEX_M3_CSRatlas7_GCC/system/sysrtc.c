/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* Library include files. */
#include "soc_irq.h"
#include "core_cm3.h"

/* platform include files */
#include "ctypes.h"
#include "io.h"
#include "utils.h"
#include "debug.h"

#include "soc_sysrtc.h"
#include "soc_iobridge.h"
#include "soc_pwrc.h"

void INT_SYSRTC_ALARM0_Handler(void)
{
	u32 value;

	value = uxIoBridgeRead(A7DA_SYSRTC_STATUS);
	value |= BIT0;
	vIoBridgeWrite(A7DA_SYSRTC_STATUS, value);

	DebugMsg("rtc_alarm0 Intr!\r\n");
}


void INT_SYSRTC_ALARM1_Handler(void)
{
	u32 value;

	value = uxIoBridgeRead(A7DA_SYSRTC_STATUS);
	value |= BIT4;
	vIoBridgeWrite(A7DA_SYSRTC_STATUS, value);

	DebugMsg("rtc_alarm1 Intr!\r\n");
}

void atlas7_m3_rtc_config(void)
{
	DebugMsg("start sysrtc test\r\n");
	u32 value;

	vIoBridgeWrite( A7DA_PWRC_TRIGGER_EN_SET, BIT5 );

	/* configure the clock switch registers to 0x3 */
	vIoBridgeWrite(A7DA_SYSRTC_CLK_SWITCH, 0x3);

	/* To generate a 16-Hz divided real-time clock */
	vIoBridgeWrite(A7DA_SYSRTC_DIV, 0x3FF);

	vIoBridgeWrite(A7DA_SYSRTC_COUNTER, 0);
	vIoBridgeWrite(A7DA_SYSRTC_ALARM0, 0x30);

	value = uxIoBridgeRead(A7DA_SYSRTC_STATUS);
	value |= BIT0;
	vIoBridgeWrite(A7DA_SYSRTC_STATUS, value);

	/* enable interrupt */
	NVIC_EnableIRQ(INT_SYSRTC_ALARM0_IRQn);
	NVIC_SetPriority(INT_SYSRTC_ALARM0_IRQn, API_SAFE_INTR_PRIO_0);

	/* enable the RTC alarm0 interrupt */
	vIoBridgeWrite(A7DA_SYSRTC_STATUS, BIT2);

	DebugMsg("end sysrtc test\r\n");
}
