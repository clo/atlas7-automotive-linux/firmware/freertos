/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ctypes.h"
#include "debug.h"
#include "io.h"
#include "soc_timer.h"

static uint32_t uxTimerOutValue;
static uint32_t uxTimerStartPoint;

void __on_retain vUsWait( uint32_t uxUs )
{
	uint32_t uxTick;
	uint32_t uxTarget;

	if( uxUs > 0x10000000 )
		uxUs = 0x10000000;

	uxTick = uxOsGetCurrentTick();
	uxTarget = uxTick + uxUs + 1;

#ifdef __ATLAS7_STEP_B__
	/* if target is too close to limit or timer need overflow, re-init timer */
	if((uxTarget > TIMER_SAFE_LIMIT) || (uxTarget < uxTick)) {
		SOC_REG(A7DA_M3_TIMER_32COUNTER_0_CTRL) &= ~(TIMER_32_EN);
		SOC_REG(A7DA_M3_TIMER_COUNTER_0) = 0;
		SOC_REG(A7DA_M3_TIMER_32COUNTER_0_CTRL) |= TIMER_32_EN;
		uxTarget = uxUs + 1;
	}
#endif
	while( uxOsGetCurrentTick() < uxTarget ) ;
}

/* microsecond  */
void vSetTimeout( uint32_t uxUs )
{
	uxTimerStartPoint = uxOsGetCurrentTick();
	uxTimerOutValue = uxTimerStartPoint + uxUs + 1;
}

/* microsecond  */
uint32_t uxGetSpendTime(void)
{
	return uxOsGetCurrentTick() - uxTimerStartPoint;
}


uint32_t uxIsTimeout(void)
{
	if( uxOsGetCurrentTick() < uxTimerOutValue )
		return 0;
	return 1;
}

uint32_t ulGetRunTimeCounterValue( void )
{
	return uxOsGetCurrentTick();
}
