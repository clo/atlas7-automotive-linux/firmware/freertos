/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

#include "ctypes.h"
#include "io.h"
#include "semphr.h"
#include "soc_irq.h"
#include "core_cm3.h"

#include "soc_tsadc.h"
#include "soc_timer.h"
#include "soc_clkc.h"
#include "soc_ioc.h"
#include "soc_otp.h"

#include "debug.h"

#define ANA_PMUCTL1 0x10E30020
#define ANA_TEMPSENSOR_CTRL 0x10E3002C
#define ANA_TSADCCTL5_TSADC_OFFSET	0x10E30014

#define AUDIO_PLL_CTRL_1 0x10E30230
#define AUDIO_REF_CTRL2 0x10E3003C
#define AUDIO_ANA_REF_CTRL0 0x10E30064

#define OFFSET_ERR_GAIN0_REG           (0x640 >> 5)
#define OFFSET_ERR_GAIN2_REG           (0x660 >> 5)
#define OFFSET_ERR_BITS_MASK           0x3fff
#define GAIN_ERR_GAIN0_REG                     (0x5c0 >> 5)
#define GAIN_ERR_GAIN2_REG                     (0x560 >> 5)
#define GAIN_ERR_BITS_MASK                     0xfff

static SemaphoreHandle_t adc_semaphore;

static struct TscChannel xChannels[] = {
	{ 0x04, A7DA_TSADC_AUX1 },
	{ 0x05, A7DA_TSADC_AUX2 },
	{ 0x06, A7DA_TSADC_AUX3 },
	{ 0x07, A7DA_TSADC_AUX4 },
	{ 0x08, A7DA_TSADC_AUX5 },
	{ 0x09, A7DA_TSADC_AUX6 },
	{ 0x0A, A7DA_TSADC_AUX7 },
	{ 0x0B, A7DA_TSADC_AUX8 },
	{ 0x12, A7DA_TSADC_TEMP1 },
	{ 0x13, A7DA_TSADC_TEMP2 },
	{ }
};

static u32 atlas7_adc_sgain_sel[] = {
	2, /* aux1 */
	2, /* aux2 */
	0, /* aux3 */
	0, /* aux4 */
	0, /* aux5 */
	0, /* aux6 */
	0, /* aux7 */
	0, /* aux8 */
	0, /* temp1 */
	0  /* temp2 */
};
void INT_TSCADC_Handler(void)
{
	u32 val;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	val = SOC_REG(A7DA_TSADC_INTR_STATUS);
	SOC_REG(A7DA_TSADC_INTR_STATUS) =
		A7DA_TSADC_PEN_INTR | A7DA_TSADC_DATA_INTR;

	/* mask compiler warning */
	val = val;

	xSemaphoreGiveFromISR(adc_semaphore, &xHigherPriorityTaskWoken);
}

int vTsadcInit( void )
{
	/* enable adc clock */
	clkc_enable_tsadc();

	/* enable temperature sensor */
	SOC_REG(ANA_TEMPSENSOR_CTRL) = 0x33;

	/* enable tsadc ldo */
	SOC_REG( ANA_PMUCTL1 ) |= BIT1 | BIT4;

	/* enable pll */
	SOC_REG( AUDIO_PLL_CTRL_1 ) = 0x545;
	SOC_REG( AUDIO_PLL_CTRL_1 ) = 0x1545;

	vTaskDelay( 1 );
	SOC_REG( AUDIO_ANA_REF_CTRL0 ) |= BIT6 | BIT4 | BIT0;
	/* Temporary untill vams fix */
	SOC_REG( AUDIO_REF_CTRL2 ) |= BIT7 | BIT2;

	/* tsc init */
	SOC_REG( A7DA_TSADC_INTR_SET ) |= BIT1 | BIT0;
	SOC_REG( A7DA_TSADC_CTRL1 ) |= BIT23;
	SOC_REG( A7DA_TSADC_CTRL2 ) = 0xd025;

	/* enable tscada data interrupt */
	NVIC_SetPriority( INT_TSCADC_IRQn, API_SAFE_INTR_PRIO_1 );
	NVIC_EnableIRQ( INT_TSCADC_IRQn );

	adc_semaphore = xSemaphoreCreateBinary();
	if (adc_semaphore == NULL) {
		DebugMsg("create tscadc semaphore failed!\r\n");
		return -1;
	}

	return 0;
}

void vTsadcEnable()
{
	SOC_REG( ANA_PMUCTL1 ) |= BIT1 | BIT4;
}

void vTsadcDisable()
{
	SOC_REG( ANA_PMUCTL1 ) &= ~( BIT1 | BIT4 );
}

static u32 prvTsadcRawToVolt( ChannelIds eChannel, u32 ulRawValue )
{
	u32 volt_ms;
	unsigned long cali_again;

#define ATLAS7_ADC_1V2_IDEAL0		1064
#define ATLAS7_ADC_1V2_IDEAL2		2757
	/* gain error calibration */
	if (atlas7_adc_sgain_sel[eChannel] == 2) {
		read_OTP_word(GAIN_ERR_GAIN2_REG, &cali_again);
		cali_again = cali_again & GAIN_ERR_BITS_MASK;
		cali_again = cali_again ? cali_again : ATLAS7_ADC_1V2_IDEAL2;
		ulRawValue = ulRawValue * 141 * ATLAS7_ADC_1V2_IDEAL2;
		ulRawValue = ulRawValue / (125 * cali_again);
		volt_ms = ulRawValue * 100 / 259;
	} else {
		read_OTP_word(GAIN_ERR_GAIN0_REG, &cali_again);
		cali_again = cali_again & GAIN_ERR_BITS_MASK;
		cali_again = cali_again ? cali_again : ATLAS7_ADC_1V2_IDEAL0;
		ulRawValue = ulRawValue * 141 * ATLAS7_ADC_1V2_IDEAL0;
		volt_ms = ulRawValue/ (125 * cali_again);
	}
	return volt_ms;
}

u32 ulTsadcGetVoltage( ChannelIds eChannel )
{
	volatile u32 ulCtrl1Tmp = 0;
	volatile u32 ulRawValue = 0;
	volatile u16 usTimeount = 0;

	unsigned long cali_offset;

	/*
	 * Atlas7 and M3 may share the ADC concurrently,
	 * and now use the mode bits in control1 register
	 * to insulate operations from the two core. If the
	 * mode bits are nonzero, the adc is occupied by
	 * the other core, and the driver can't operate
	 * the adc now. If zero, the adc could be use now,
	 * and driver can operate the adc to response the
	 * request. After using, the mode bit must be set
	 * to zero for using by the other core.
	 */
	ulCtrl1Tmp = SOC_REG( A7DA_TSADC_CTRL1 );
	if ( ulCtrl1Tmp & A7DA_TSADC_CHANNEL_MASK )
	{
		DebugMsg("ulTsadcGetVoltage busy\r\n");
		return 0;
	}
	/* disable data interrupt */
	SOC_REG( A7DA_TSADC_INTR_CLR ) &= ~A7DA_TSADC_DATA_INTR;

	/* write offset error calibration to ANA_TSADCCTL5_TSADC_OFFSET,
	the hw will caliborate offset error auto */
	if (atlas7_adc_sgain_sel[eChannel] == 2) {
		read_OTP_word(OFFSET_ERR_GAIN2_REG, &cali_offset);
	} else {
		read_OTP_word(OFFSET_ERR_GAIN0_REG, &cali_offset);
	}
	SOC_REG(ANA_TSADCCTL5_TSADC_OFFSET) = cali_offset & OFFSET_ERR_BITS_MASK;

	/* enable data interrupt */
	SOC_REG( A7DA_TSADC_INTR_SET ) |= A7DA_TSADC_DATA_INTR;

	SOC_REG( A7DA_TSADC_CTRL1 ) = BIT24 | BIT22 | BIT23 | BIT15
				| ( xChannels[ eChannel ].ulMode << 10 );

	usTimeount = 50;
	do {
		if (xSemaphoreTake(adc_semaphore, portMAX_DELAY) == pdFALSE)
			vTaskDelay( 1 );
		ulRawValue = SOC_REG( xChannels[ eChannel ].ulValReg );
		if ( ulRawValue & A7DA_TSADC_DATA_VALID )
		{
			break;
		}
		else if ( usTimeount <= 0 )
		{
			SOC_REG( A7DA_TSADC_CTRL1 ) = ulCtrl1Tmp;
			SOC_REG( A7DA_TSADC_INTR_SET ) |= A7DA_TSADC_DATA_INTR;
			return 0;
		}
		usTimeount--;
	} while( 1 );

	ulRawValue &= A7DA_TSADC_DATA_MASK;
	SOC_REG( A7DA_TSADC_CTRL1 ) = ulCtrl1Tmp;

	return prvTsadcRawToVolt( eChannel, ulRawValue );
}
