/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "errno.h"

#include "soc_irq.h"
#include "core_cm3.h"
#include "soc_uart.h"
#include "soc_timer.h"
#include "soc_clkc.h"
#include "soc_ioc.h"
#include "soc_pwrc.h"
#include "soc_iobridge.h"

#include "pm.h"

#define UART_SAMPLE_DIV_LIMIT   0x41
#define UART_BASE_OFFSET( x )	( ( 0x10000 ) * ( x ) )

typedef struct UART_REGISTERS
{
	uint32_t uxLineCtrl;
	uint32_t uxTxRxEnaReg;
	uint32_t uxDivisor;
	uint32_t uxIntEnableSet;
	uint32_t uxIntStatus;
	uint32_t uxRiscDspMode;
	uint32_t uxIntEnableClr;
	uint32_t uxTxDmaIoCtrl;
	uint32_t uxTxDmaIoLen;
	uint32_t uxTxFifoCtrl;
	uint32_t uxTxFifoLevelChk;
	uint32_t uxTxFifoOp;
	uint32_t uxTxFifoStatus;
	uint32_t uxTxFifoData;
	uint32_t uxRxDmaIoCtrl;
	uint32_t uxRxDmaIoLen;
	uint32_t uxRxFifoCtrl;
	uint32_t uxRxFifoLevelChk;
	uint32_t uxRxFifoOp;
	uint32_t uxRxFifoStatus;
	uint32_t uxRxFifoData;
} xUartRegisters;

typedef struct UART_ADD_REGISTERS
{
	uint32_t uxAfcCtrl;
	uint32_t uxRxDmaCntr;
	uint32_t uxSwhDmaIo;
} xUartAddRegisters;

/* BITs of UART#0,2,3,4,5,6 FIFO Control register */
#define UART_FIFO_THD_MAX_BIT	BIT6

/* BITs of UART#1 FIFO Control register */
#define UART1_FIFO_THD_MAX_BIT	BIT4

/* UART#0,2,3,4,5,6 TX FIFO Status Register */
#define UART_TXFIFO_MAX_BIT	BIT6
#define UART_TXFIFO_FULL	BIT7
#define UART_TXFIFO_EMPTY	BIT8

/* UART#1 TX FIFO Status Register */
#define UART1_TXFIFO_MAX_BIT	BIT4
#define UART1_TXFIFO_FULL	BIT5
#define UART1_TXFIFO_EMPTY	BIT6

typedef struct UART_DEVICE
{
	uint8_t ucPort;
	uint8_t ucUseAddReg;
	uint8_t ucIrq;
	uint8_t ucMode;
	uint32_t uxBaudRate;
	uint32_t uxIoClk;
	SemaphoreHandle_t xRxLock;
	SemaphoreHandle_t xTxLock;
	SemaphoreHandle_t xRxReady;
	SemaphoreHandle_t xTxReady;
	uint32_t uxTxFifoFull;
	uint32_t uxRxFifoEmpty;
	volatile xUartRegisters xRegisters;
	volatile xUartAddRegisters xAddRegisters;
} xUartDevice;

static xUartDevice *pxUartDeviceList[SOC_UART_NUMBER];
static uint8_t xUartDeviceIrqList[SOC_UART_NUMBER] = { 17, 18, 19, 66, 69, 71, 100 };
static uint8_t xUartDeviceAddRegList[SOC_UART_NUMBER] = { 1, 0, 1, 1, 1, 1, 1 };

static u32 CalBaudRate(u32 BaudRate, u32 IOClk)
{
	int ioclkDiv = 0, sampleDiv = 0;
	int tmp = 0, i = 0, div = 0, delta = IOClk;

	if (0 == BaudRate)
		return 0;

	for (i = 16; i < UART_SAMPLE_DIV_LIMIT; i++) {
		 /* round */
		div = (IOClk + (BaudRate * i) / 2) / (BaudRate * i);
		tmp = IOClk - div * BaudRate * i;
		tmp = (tmp > 0 ? tmp : (-tmp));

		if (delta > tmp) {
			delta = tmp;
			/* io clk div in doc */
			ioclkDiv = div - 1;
			/* sample div in doc */
			sampleDiv = i - 1;
		}

		if (!tmp)
			break;
	}

	return ioclkDiv | (sampleDiv << 16);
}

void vUartDeviceISR( uint8_t ucPort )
{
	volatile uint32_t uxIntStatus;
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	xUartDevice *pxUartDevice = pxUartDeviceList[ucPort];

	if ( !pxUartDevice )
	{
		DebugMsg( "UART%d hadn't been intialized!\r\n", ucPort );
		return;
	}

	/* Disable Interrupt */
	NVIC_DisableIRQ( pxUartDevice->ucIrq );
	NVIC_ClearPendingIRQ( pxUartDevice->ucIrq );
	/* Clear Interrupts */
	uxIntStatus = SOC_REG( pxUartDevice->xRegisters.uxIntStatus );
	SOC_REG( pxUartDevice->xRegisters.uxIntStatus ) = uxIntStatus;

	xSemaphoreGiveFromISR( pxUartDevice->xRxReady, &xHigherPriorityTaskWoken );

	NVIC_EnableIRQ( pxUartDevice->ucIrq );
}

void INT_UART0_Handler( void )
{
	vUartDeviceISR( UART0 );
}

void INT_UART1_Handler( void )
{
	vUartDeviceISR( UART1 );
}

void INT_UART2_Handler( void )
{
	vUartDeviceISR( UART2 );
}

void INT_UART3_Handler( void )
{
	vUartDeviceISR( UART3 );
}

void INT_UART4_Handler( void )
{
	vUartDeviceISR( UART4 );
}

void INT_UART5_Handler( void )
{
	vUartDeviceISR( UART5 );
}

void INT_UART6_Handler( void )
{
	vUartDeviceISR( UART6 );
}

uint32_t uxUartDeviceHwInitialize( xUartDevice *pxUartDevice )
{
	if ( pxUartDevice->ucPort == UART0 )
	{
		/* enable uart clock */
		clkc_enable_uart_io();

		/* enable uart pin mux */
		ioc_enable_uart_mux();
	}

	if ( pxUartDevice->ucPort == UART1 )
	{
		pxUartDevice->uxTxFifoFull = UART1_TXFIFO_FULL;
		pxUartDevice->uxRxFifoEmpty = UART1_RXFIFO_EMPTY;
	}
	else
	{
		pxUartDevice->uxTxFifoFull = UART_TXFIFO_FULL;
		pxUartDevice->uxRxFifoEmpty = UART_RXFIFO_EMPTY;
	}

	SOC_REG( pxUartDevice->xRegisters.uxDivisor ) = CalBaudRate( pxUartDevice->uxBaudRate, pxUartDevice->uxIoClk );

	vUsWait(10);

	/* 8 data bits, 1 stop bit, no parity bit */
	SOC_REG( pxUartDevice->xRegisters.uxLineCtrl ) = 0x00000003;

	SOC_REG( pxUartDevice->xRegisters.uxTxDmaIoCtrl ) = UART_TX_IO_MODE;
	SOC_REG( pxUartDevice->xRegisters.uxRxDmaIoCtrl ) = UART_RX_IO_MODE;
	SOC_REG( pxUartDevice->xRegisters.uxTxDmaIoLen ) = 0;
	SOC_REG( pxUartDevice->xRegisters.uxRxDmaIoLen ) = 0;

	SOC_REG( pxUartDevice->xRegisters.uxTxFifoCtrl ) = 0x08 ;
	SOC_REG( pxUartDevice->xRegisters.uxRxFifoCtrl ) = 0x10 ;

	SOC_REG( pxUartDevice->xRegisters.uxTxFifoLevelChk  ) =
			0x06 | (0x04 << 10) | (0x02 << 20);
	SOC_REG( pxUartDevice->xRegisters.uxRxFifoLevelChk  ) =
			0x02 | (0x04 << 10) | (0x06 << 20);

	/* FIFO Reset */
	SOC_REG( pxUartDevice->xRegisters.uxTxFifoOp ) = UART_TXFIFO_RESET;
	SOC_REG( pxUartDevice->xRegisters.uxRxFifoOp ) = UART_RXFIFO_RESET;

	/* FIFO Start */
	SOC_REG( pxUartDevice->xRegisters.uxTxFifoOp ) = UART_TXFIFO_START;
	SOC_REG( pxUartDevice->xRegisters.uxRxFifoOp ) = UART_RXFIFO_START;

	vUsWait(10);

	SOC_REG( pxUartDevice->xRegisters.uxTxRxEnaReg ) = UART_RX_EN | UART_TX_EN;

	/* Disable all interrupts */
	SOC_REG( pxUartDevice->xRegisters.uxIntEnableClr ) = UART_INT_MASK_ALL;

	/* Clear all pending uart interrupts */
	SOC_REG( pxUartDevice->xRegisters.uxIntStatus ) = UART_INT_MASK_ALL;

	if ( pxUartDevice->ucMode == UART_MODE_INTR )
	{
		/* Enable uart interrupts */
		SOC_REG( pxUartDevice->xRegisters.uxIntEnableSet ) = UART_RX_DONE_INT_EN_SET;
	}

	return 0;
}

int xUartDeviceInitialize( uint8_t ucPort, uint8_t ucMode, uint32_t uxBaudRate, uint32_t uxIoClk )
{
	int xRet;
	xUartDevice *pxUartDevice;

	if ( ucPort >= SOC_UART_NUMBER )
	{
		configASSERT( 0 );
		return -ENODEV;
	}

	pxUartDevice = (xUartDevice *)pvPortMalloc( sizeof( xUartDevice ) );
	if ( pxUartDevice == NULL )
	{
		configASSERT( 0 );
		xRet = -ENOMEM;
		goto failed;
	}

	pxUartDevice->ucPort = ucPort;
	pxUartDevice->ucUseAddReg = xUartDeviceAddRegList[ucPort];
	pxUartDevice->ucIrq = xUartDeviceIrqList[ucPort];
	pxUartDevice->ucMode = ucMode;
	pxUartDevice->uxBaudRate = uxBaudRate;
	pxUartDevice->uxIoClk = uxIoClk;

	/* Setup Uart Device Registers */
	pxUartDevice->xRegisters.uxLineCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_LINE_CTRL;
	pxUartDevice->xRegisters.uxTxRxEnaReg = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXRX_ENA_REG;
	pxUartDevice->xRegisters.uxDivisor = UART_BASE_OFFSET( ucPort ) + A7DA_UART_DIVISOR;
	pxUartDevice->xRegisters.uxIntEnableSet = UART_BASE_OFFSET( ucPort ) + A7DA_UART_INT_ENABLE_SET;
	pxUartDevice->xRegisters.uxIntStatus = UART_BASE_OFFSET( ucPort ) + A7DA_UART_INT_STATUS;
	pxUartDevice->xRegisters.uxRiscDspMode = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RISC_DSP_MODE;
	pxUartDevice->xRegisters.uxIntEnableClr = UART_BASE_OFFSET( ucPort ) + A7DA_UART_INT_ENABLE_CLR;
	pxUartDevice->xRegisters.uxTxDmaIoCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TX_DMA_IO_CTRL;
	pxUartDevice->xRegisters.uxTxDmaIoLen = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TX_DMA_IO_LEN;
	pxUartDevice->xRegisters.uxTxFifoCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXFIFO_CTRL;
	pxUartDevice->xRegisters.uxTxFifoLevelChk = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXFIFO_LEVEL_CHK;
	pxUartDevice->xRegisters.uxTxFifoOp = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXFIFO_OP;
	pxUartDevice->xRegisters.uxTxFifoStatus = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXFIFO_STATUS;
	pxUartDevice->xRegisters.uxTxFifoData = UART_BASE_OFFSET( ucPort ) + A7DA_UART_TXFIFO_DATA;
	pxUartDevice->xRegisters.uxRxDmaIoCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RX_DMA_IO_CTRL;
	pxUartDevice->xRegisters.uxRxDmaIoLen = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RX_DMA_IO_LEN;
	pxUartDevice->xRegisters.uxRxFifoCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RXFIFO_CTRL;
	pxUartDevice->xRegisters.uxRxFifoLevelChk = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RXFIFO_LEVEL_CHK;
	pxUartDevice->xRegisters.uxRxFifoOp = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RXFIFO_OP;
	pxUartDevice->xRegisters.uxRxFifoStatus = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RXFIFO_STATUS;
	pxUartDevice->xRegisters.uxRxFifoData = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RXFIFO_DATA;

	/* Setup Uart Device Add Registers */
	if ( pxUartDevice->ucUseAddReg )
	{
		pxUartDevice->xAddRegisters.uxAfcCtrl = UART_BASE_OFFSET( ucPort ) + A7DA_UART_AFC_CTRL;
		pxUartDevice->xAddRegisters.uxRxDmaCntr = UART_BASE_OFFSET( ucPort ) + A7DA_UART_RX_DMA_CNTR;
		pxUartDevice->xAddRegisters.uxSwhDmaIo = UART_BASE_OFFSET( ucPort ) + A7DA_UART_SWH_DMA_IO;
	}
	else
	{
		pxUartDevice->xAddRegisters.uxAfcCtrl = 0;
		pxUartDevice->xAddRegisters.uxRxDmaCntr = 0;
		pxUartDevice->xAddRegisters.uxSwhDmaIo = 0;
	}

	/* Setup Semaphores */
	pxUartDevice->xRxLock = xSemaphoreCreateMutex();
	if ( pxUartDevice->xRxLock == NULL )
	{
		configASSERT( 0 );
		xRet = -EINVAL;
		goto freedev;
	}

	pxUartDevice->xTxLock = xSemaphoreCreateMutex();
	if ( pxUartDevice->xTxLock == NULL  )
	{
		configASSERT( 0 );
		xRet = -EINVAL;
		goto free_rxlock;
	}

	pxUartDevice->xRxReady = xSemaphoreCreateBinary();
	if ( pxUartDevice->xRxReady == NULL  )
	{
		configASSERT( 0 );
		xRet = -EINVAL;
		goto free_txlock;
	}

	pxUartDevice->xTxReady = xSemaphoreCreateBinary();
	if ( pxUartDevice->xTxReady == NULL  )
	{
		configASSERT( 0 );
		xRet = -EINVAL;
		goto free_rxready;
	}

	xRet = uxUartDeviceHwInitialize( pxUartDevice );
	if (xRet)
	{
		configASSERT( 0 );
		goto free_txready;
	}

	if ( pxUartDevice->ucMode == UART_MODE_INTR )
	{
		NVIC_EnableIRQ( pxUartDevice->ucIrq );
		NVIC_SetPriority( pxUartDevice->ucIrq, API_SAFE_INTR_PRIO_0 );
	}

	pxUartDeviceList[ucPort] = pxUartDevice;

	return 0;

free_txready:
	vSemaphoreDelete( pxUartDevice->xTxReady );

free_rxready:
	vSemaphoreDelete( pxUartDevice->xRxReady );

free_txlock:
	vSemaphoreDelete( pxUartDevice->xTxLock );

free_rxlock:
	vSemaphoreDelete( pxUartDevice->xRxLock );

freedev:
	vPortFree( pxUartDevice );

failed:

	return xRet;
}

void vUartDeviceReHwInitialize( uint8_t ucPort )
{
	configASSERT( ucPort < SOC_UART_NUMBER );
	uxUartDeviceHwInitialize( pxUartDeviceList[ ucPort ] );
}

static void __on_retain vUartWriteByte( xUartDevice *pxUartDevice, uint8_t ucData )
{
	/* if A7 has been power down, can't access uart registers anymore */
	if ( uxIoBridgeRead( A7DA_PWRC_FSM_STATE ) == PWRC_FSM_PSAVING_MODE_M3 )
		return;

	while ( SOC_REG( pxUartDevice->xRegisters.uxTxFifoStatus ) & pxUartDevice->uxTxFifoFull )
		;
	SOC_REG( pxUartDevice->xRegisters.uxTxFifoData ) = (ulong)ucData;
}

static uint8_t ucUartReadByte( xUartDevice *pxUartDevice )
{
	uint8_t ucData = 0;

	while(PM_NORMAL != uxGetPmLevel())
	{
		vTaskDelay(1);
	}

	while ( SOC_REG( pxUartDevice->xRegisters.uxRxFifoStatus ) & pxUartDevice->uxRxFifoEmpty )
	{
		if ( pxUartDevice->ucMode == UART_MODE_INTR )
		{
			if ( xSemaphoreTake( pxUartDevice->xRxReady, portMAX_DELAY ) == pdFALSE )
			{
				configASSERT( 0 );
			}
		}
	}
	ucData = SOC_REG( pxUartDevice->xRegisters.uxRxFifoData );
	return ucData;
}

void __on_retain uartWriteByte( uint8_t ucData )
{
	if( (NULL != pxUartDeviceList[configUART_USED_FOR_DEBUG])  && (PM_NORMAL == uxGetPmLevel())  )
	{
		vUartWriteByte( pxUartDeviceList[configUART_USED_FOR_DEBUG], ucData );
	}
}

char uartReadByte( void )
{
	return ( char )ucUartReadByte( pxUartDeviceList[configUART_USED_FOR_DEBUG] );
}
