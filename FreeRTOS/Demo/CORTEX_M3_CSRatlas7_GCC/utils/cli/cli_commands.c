/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

#ifdef CONFIG_ENABLE_TEST
extern CLI_Command_Definition_t *P_PWRC_CLI_Parameter;
extern CLI_Command_Definition_t *P_PWRC_PowerMode_CLI_Parameter;
extern CLI_Command_Definition_t *P_PWRC_CanWakeup_CLI_Parameter;
extern CLI_Command_Definition_t *P_IPC_CLI_Parameter;
extern CLI_Command_Definition_t *P_IPCKAS_CLI_Parameter;
extern CLI_Command_Definition_t *P_REGOPS_CLI_Parameter;
extern CLI_Command_Definition_t *P_RTC_CLI_Parameter;
extern CLI_Command_Definition_t *P_WATCHDOG_CLI_Parameter;

extern CLI_Command_Definition_t *P_CANBUS_TranMes_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_EnablePort_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_DisablePort_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_EnableCan1Wakup_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_QueleFullErrorEnable_CLI_Parameter;

extern CLI_Command_Definition_t *P_CANBUS_DemoInit_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_HFTestInit_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_RobusTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_RetanTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_ArbTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_BasicTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_LinkTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_SSTTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_ErrorDetectTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_Debugmode_Init_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_Debugmode_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_CORE_DebugTran_CLI_Parameter;
extern CLI_Command_Definition_t *P_CANBUS_KPI_CLI_Parameter;
extern CLI_Command_Definition_t *P_CLOCK_CLI_Parameter;
extern CLI_Command_Definition_t *P_QSPI_INIT_CLI_Parameter;
extern CLI_Command_Definition_t *P_QSPI_DpmTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_QSPI_DpmPrintStatus_CLI_Parameter;
extern CLI_Command_Definition_t *P_QSPI_XIPInterface_CLI_Parameter;
extern CLI_Command_Definition_t *P_QSPI_XIPTest_CLI_Parameter;
extern CLI_Command_Definition_t *P_VCAN_CLI_Parameter;
extern CLI_Command_Definition_t *P_GPIO_INTR_CLI_Parameter;
extern CLI_Command_Definition_t *P_TSADC_INIT_CLI_Parameter;
extern CLI_Command_Definition_t *P_TSADC_ENABLE_CLI_Parameter;
extern CLI_Command_Definition_t *P_TSADC_DISABLE_CLI_Parameter;
extern CLI_Command_Definition_t *P_TSADC_GetVolt_CLI_Parameter;
extern CLI_Command_Definition_t *P_GPT_CLI_Parameter;
extern CLI_Command_Definition_t *P_WDG_CLI_Parameter;
extern CLI_Command_Definition_t *P_GPT2PWM_CLI_Parameter;
extern CLI_Command_Definition_t *P_CACHE_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_RECOVER_CLI_Parameter;

extern CLI_Command_Definition_t *P_NTFW_CLI_Parameter;
extern const CLI_Command_Definition_t *P_LOADER_CLI_Parameter;

extern CLI_Command_Definition_t *P_RunTimeStats_CLI_Parameter;
extern CLI_Command_Definition_t *P_TaskStats_CLI_Parameter;
extern CLI_Command_Definition_t *P_StartStopTrace_CLI_Parameter;
extern CLI_Command_Definition_t *P_AES_CLI_Parameter;
extern CLI_Command_Definition_t *P_PULSEC_CLI_Parameter;
extern CLI_Command_Definition_t *P_PULSEC_PWM_CLI_Parameter;
#endif
extern CLI_Command_Definition_t *P_KALIMBA_IPC_GET_FW_ID_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_READ_DM_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_LOAD_FW_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_AUDIO_PIPE_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_PS_UPDATE_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_LIC_CTRL_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_DRAM_FREE_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_DRAM_ALLOCATION_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_KAS_CRASH_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_KALIMBA_I2C_TEST_CLI_Parameter;
extern CLI_Command_Definition_t *P_M3_CHIME_TEST_CLI_Parameter;
/*
 * Registers the CLI commands defined within this file with the FreeRTOS+CLI
 * command line interface.
 */
void vRegisterSampleCLICommands( void )
{
#ifdef CONFIG_ENABLE_TEST
	/* Register all the command line commands defined immediately above. */
#ifdef CONFIG_ENABLE_PWRC_TEST
	FreeRTOS_CLIRegisterCommand( P_PWRC_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_PWRC_CanWakeup_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_PWRC_PowerMode_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_IPC_TEST
	FreeRTOS_CLIRegisterCommand( P_IPC_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_IPCKAS_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_REGOPS_TEST
	FreeRTOS_CLIRegisterCommand( P_REGOPS_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_SYSRTC_TEST
	FreeRTOS_CLIRegisterCommand( P_RTC_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_WATCHDOG_TEST
	FreeRTOS_CLIRegisterCommand( P_WATCHDOG_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CANBUS_BASIC_TEST
	FreeRTOS_CLIRegisterCommand( P_CANBUS_TranMes_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_EnablePort_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_DisablePort_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_EnableCan1Wakup_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_QueleFullErrorEnable_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CANBUS_DEMO
	FreeRTOS_CLIRegisterCommand( P_CANBUS_DemoInit_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CANBUS_INTERFACE_TEST
	FreeRTOS_CLIRegisterCommand( P_CANBUS_HFTestInit_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_RobusTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_RetanTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_ArbTest_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CANBUS_CORE_TEST
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_BasicTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_LinkTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_SSTTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_ErrorDetectTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_Debugmode_Init_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_Debugmode_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_CANBUS_CORE_DebugTran_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CANBUS_KPI_TEST
	FreeRTOS_CLIRegisterCommand(P_CANBUS_KPI_CLI_Parameter);
#endif

#ifdef CONFIG_ENABLE_CLOCK_TEST
	FreeRTOS_CLIRegisterCommand( P_CLOCK_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_QSPI_TEST
	FreeRTOS_CLIRegisterCommand( P_QSPI_INIT_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_QSPI_DpmTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_QSPI_DpmPrintStatus_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_QSPI_XIPTest_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_QSPI_XIPInterface_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_VCAN_TEST
	FreeRTOS_CLIRegisterCommand( P_VCAN_CLI_Parameter );
#endif

#if configUSE_TRACE_FACILITY == 1
	/* Register all the command line commands defined immediately above. */
	FreeRTOS_CLIRegisterCommand( P_RunTimeStats_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_TaskStats_CLI_Parameter );
#endif

#if configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1
	FreeRTOS_CLIRegisterCommand( P_StartStopTrace_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_GPIO_TEST
	FreeRTOS_CLIRegisterCommand( P_GPIO_INTR_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_TSADC_TEST
	FreeRTOS_CLIRegisterCommand( P_TSADC_INIT_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_TSADC_ENABLE_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_TSADC_DISABLE_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_TSADC_GetVolt_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_GPT_TEST
    FreeRTOS_CLIRegisterCommand( P_GPT_CLI_Parameter );
	FreeRTOS_CLIRegisterCommand( P_WDG_CLI_Parameter );
    FreeRTOS_CLIRegisterCommand( P_GPT2PWM_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_CACHE_TEST
    FreeRTOS_CLIRegisterCommand( P_CACHE_TEST_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_AES_TEST
    FreeRTOS_CLIRegisterCommand( P_AES_CLI_Parameter );
#endif

#ifdef CONFIG_ENABLE_NOC_NTFW_TEST
	FreeRTOS_CLIRegisterCommand(P_NTFW_CLI_Parameter);
#endif

#ifdef CONFIG_ENABLE_LOADER_TEST
	#ifdef __ATLAS7_STEP_B__
	FreeRTOS_CLIRegisterCommand(P_LOADER_CLI_Parameter);
    #endif
#endif

#ifdef CONFIG_ENABLE_PULSEC_TEST
	FreeRTOS_CLIRegisterCommand(P_PULSEC_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_PULSEC_PWM_CLI_Parameter);
#endif

#ifdef CONFIG_ENABLE_RECOVER_TEST
	FreeRTOS_CLIRegisterCommand(P_RECOVER_CLI_Parameter);
#endif

#endif
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_IPC_GET_FW_ID_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_LOAD_FW_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_READ_DM_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_AUDIO_PIPE_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_PS_UPDATE_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_LIC_CTRL_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_DRAM_FREE_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_DRAM_ALLOCATION_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_KAS_CRASH_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_KALIMBA_I2C_TEST_CLI_Parameter);
	FreeRTOS_CLIRegisterCommand(P_M3_CHIME_TEST_CLI_Parameter);
}
