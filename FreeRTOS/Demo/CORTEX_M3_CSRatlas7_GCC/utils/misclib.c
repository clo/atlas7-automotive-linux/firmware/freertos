/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* Library include files. */
#include "ctypes.h"

#define UNALIGNED(X)		((long)X & ((sizeof (long)) - 1))
#define BIGBLOCKSIZE		(sizeof (long) << 2)
#define LITTLEBLOCKSIZE		(sizeof (long))
#define TOO_SMALL_B(LEN)	((LEN) < BIGBLOCKSIZE)
#define TOO_SMALL_L(LEN)	((LEN) < LITTLEBLOCKSIZE)

void *memcpy(void *dst, const void *src, size_t n)
{
	char *dst0 = (char *)dst;
	char *src0 = (char *)src;
	long *aligned_dst;
	long *aligned_src;

	/* If the size is small, or either SRC or DST is unaligned,
	then punt into the byte copy loop.  This should be rare.  */
	if (!TOO_SMALL_B(n) &&
		!(UNALIGNED(src0) | UNALIGNED(dst0))) {
		aligned_dst = (long*)dst0;
		aligned_src = (long*)src0;

		/* Copy 4X long words at a time if possible.  */
		while (n >= BIGBLOCKSIZE) {
			*aligned_dst++ = *aligned_src++;
			*aligned_dst++ = *aligned_src++;
			*aligned_dst++ = *aligned_src++;
			*aligned_dst++ = *aligned_src++;
			n -= BIGBLOCKSIZE;
		}

		/* Copy one long word at a time if possible.  */
		while (n >= LITTLEBLOCKSIZE) {
			*aligned_dst++ = *aligned_src++;
			n -= LITTLEBLOCKSIZE;
		}

		/* Pick up any residual with a byte copier.  */
		dst0 = (char*)aligned_dst;
		src0 = (char*)aligned_src;
	}

	while (n--)
		*dst0++ = *src0++;

	return dst;
}

int memcmp (const void *s1, const void *s2, size_t n)
{
    if (n) {
        unsigned const char *p1 = s1, *p2 = s2;

        do {
            if (*p1++ != *p2++) {
                return (*--p1 - *--p2);
            }
        } while (--n);
    }
    return 0;
}

void __attribute__((optimize("O2"))) *memset(void *m, int c, size_t n)
{
	int i;
	ulong buffer;
	ulong *aligned_addr;
	char *s = m;
	int d = c & 0xff;


	while (UNALIGNED(s)) {
		if (n--)
			*s++ = (char) c;
		else
			return m;
	}

	if (!TOO_SMALL_L(n)) {
		/* If we get this far, we know that n is large and s is word-aligned. */
		aligned_addr = (ulong *)s;

		/* Store D into each char sized location in BUFFER so that
		we can set large blocks quickly.  */
		buffer = (d << 8) | d;
		buffer |= (buffer << 16);
		for (i = 32; i < LITTLEBLOCKSIZE * 8; i <<= 1)
			buffer = (buffer << i) | buffer;

		/* Unroll the loop.  */
		while (n >= LITTLEBLOCKSIZE*4) {
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			*aligned_addr++ = buffer;
			n -= 4*LITTLEBLOCKSIZE;
		}

		while (n >= LITTLEBLOCKSIZE) {
			*aligned_addr++ = buffer;
			n -= LITTLEBLOCKSIZE;
		}
		/* Pick up the remainder with a bytewise loop.  */
		s = (char*)aligned_addr;
	}

	while (n--)
		*s++ = (char) c;

	return m;
}

u32 strtoul (char *str, char **ptr, int base)
{
	unsigned long rvalue = 0;
	int neg = 0;
	int c;

	/* Validate parameters */
	if ((str != NULL) && (base >= 0) && (base <= 36))
	{
		/* Skip leading white spaces */
		while (isspace(*str))
			++str;

		/* Check for notations */
		switch (str[0])
		{
		case '0':
			if (base == 0)
			{
				if ((str[1] == 'x') || (str[1] == 'X'))
				{
					base = 16;
					str += 2;
				}
				else
				{
					base = 8;
					str++;
				}
			}
			break;

		case '-':
			neg = 1;
			str++;
			break;

		case '+':
			str++;
			break;

		default:
			break;
		}

		if (base == 0)
			base = 10;

		/* Valid "digits" are 0..9, A..Z, a..z */
		while (isalnum(c = *str))
		{
			/* Convert char to num in 0..36 */
			if ((c -= ('a' - 10)) < 10)	 /* 'a'..'z' */
			{
				if ((c += ('a' - 'A')) < 10)     /* 'A'..'Z' */
					c += ('A' - '0' - 10);	/* '0'..'9' */
			}

			/* check c against base */
			if (c >= base)
				break;

			if (neg)
				rvalue = (rvalue * base) - c;
			else
				rvalue = (rvalue * base) + c;

			++str;
		}
	}

	/* Upon exit, 'str' points to the character at which valid info */
	/* STOPS.  No chars including and beyond 'str' are used.	*/

	if (ptr != NULL)
		*ptr = str;

	return rvalue;
}

size_t strlen(const char *s)
{
	const char *sc;

	for (sc = s; *sc != '\0'; ++sc)
		/* nothing */;
	return sc - s;
}

char *strcpy(char *dest, const char *src)
{
	char *tmp = dest;

	while ((*dest++ = *src++) != '\0')
		/* nothing */;
	return tmp;
}

char *strncpy(char *dest, const char *src, size_t count)
{
	char *tmp = dest;

	while (count) {
		if ((*tmp = *src) != 0)
			src++;
		tmp++;
		count--;
	}
	return dest;
}

int strcmp(const char *cs, const char *ct)
{
	unsigned char c1, c2;

	while (1) {
		c1 = *cs++;
		c2 = *ct++;
		if (c1 != c2)
			return c1 < c2 ? -1 : 1;
		if (!c1)
			break;
	}
	return 0;
}

int strncmp(const char *cs, const char *ct, size_t count)
{
	unsigned char c1, c2;

	while (count) {
		c1 = *cs++;
		c2 = *ct++;
		if (c1 != c2)
			return c1 < c2 ? -1 : 1;
		if (!c1)
			break;
		count--;
	}
	return 0;
}

char *strchr(const char *s, int c)
{
	for (; *s != (char)c; ++s)
		if (*s == '\0')
			return NULL;
	return (char *)s;
}

char *strcat(char *dest, const char *src)
{
	char *tmp = dest;

	while (*dest)
		dest++;
	while ((*dest++ = *src++) != '\0')
		;
	return tmp;
}

char *strncat(char *dst, const char *src, size_t n)
{
	if (n != 0)
	{
		register char *d = dst;
		register const char *s = src;

		while (*d != 0)
			d++;
		do
		{
			if ((*d = *s++) == 0)
				break;
			d++;
		} while (--n != 0);
		*d = 0;
	}

	return dst;
}

char *stpcpy(char *dest, const char *src)
{
	char *d = dest;
	const char *s = src;

	do {
		*d++ = *s;
	} while (*s++ != '\0');

	return d - 1;
}

static long my_do_rand(unsigned long *value)
{
	long quotient, remainder, t;
	quotient = *value / 127773L;
	remainder = *value % 127773L;
	t = 16807L * remainder - 2836L * quotient;
	if (t <= 0)
		t += 0x7FFFFFFFL;

	return ((*value = t) % ((unsigned long)32767 + 1));
}

static unsigned long next = 1;

int my_rand(void)
{
	return my_do_rand(&next);
}

int strcasecmp(const char *s1, const char *s2)
{
    char    c1, c2;
    int     result = 0;

    while (result == 0)
    {
        c1 = *s1++;
        c2 = *s2++;
        if ((c1 >= 'a') && (c1 <= 'z'))
            c1 = (char)(c1 - ' ');
        if ((c2 >= 'a') && (c2 <= 'z'))
            c2 = (char)(c2 - ' ');
        if ((result = (c1 - c2)) != 0)
            break;
        if ((c1 == 0) || (c2 == 0))
            break;
    }
    return result;
}

int strncasecmp (const char *s1, const char *s2, size_t n)
{
    char    c1, c2;
    int     k = 0;
    int     result = 0;

    while ( k++ < n )
    {
        c1 = *s1++;
        c2 = *s2++;
        if ((c1 >= 'a') && (c1 <= 'z'))
            c1 = (char)(c1 - ' ');
        if ((c2 >= 'a') && (c2 <= 'z'))
            c2 = (char)(c2 - ' ');
        if ((result = (c1 - c2)) != 0)
            break;
        if ((c1 == 0) || (c2 == 0))
            break;
    }
    return result;
}

char *strpbrk(const char *cs, const char *ct)
{
	const char *sc1, *sc2;

	for (sc1 = cs; *sc1 != '\0'; ++sc1) {
		for (sc2 = ct; *sc2 != '\0'; ++sc2) {
			if (*sc1 == *sc2)
				return (char *)sc1;
		}
	}
	return 0;
}

char *strsep(char **s, const char *ct)
{
	char *sbegin = *s;
	char *end;

	if (sbegin == (void *)0)
		return (void *)0;

	end = strpbrk(sbegin, ct);
	if (end)
		*end++ = '\0';
	*s = end;
	return sbegin;
}
