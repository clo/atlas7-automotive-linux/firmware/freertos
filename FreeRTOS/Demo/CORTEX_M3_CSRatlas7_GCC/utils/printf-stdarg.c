/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdarg.h>
#include "misclib.h"

extern void uartWriteByte(char ch);
#define putchar(x) uartWriteByte(x)

#if 0
typedef char *  va_list;
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )
#define va_start(ap,v)  ( ap = (va_list)&v + _INTSIZEOF(v) )
#define va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_end(ap)      ( ap = (va_list)0 )
#endif
#define do_div(n,base) ({ \
	int __res; \
	__res = ((unsigned long) n) % (unsigned) base; \
	n = ((unsigned long) n) / (unsigned) base; \
	__res; \
})

static void printchar(char **str, int c)
{
	if (str) {
		**str = (char)c;
		++(*str);
	}
	else
	{
		(void)putchar(c);
	}
}

static int number(char** out, long num, int base, int sign)
{
	int len = 0;
	char tmp[66];
	const char *digits="0123456789abcdefghijklmnopqrstuvwxyz";
	int i;

	if (sign && num < 0) {
		num = -num;
		printchar(out,'-');
	}

	i = 0;
	if (num == 0)
		tmp[i++] = '0';
	else while (num != 0)
		tmp[i++] = digits[do_div(num, base)];

	len = i;
	while (i-- > 0)
		printchar(out,tmp[i]);

	return len;

}

static int printf_wrapped(char** out,const char *fmt, va_list args)
{
	int len;
	int rlen = 0;
	unsigned long num;
	int i, base;
	const char *s;

	int sign = 1;

	for (; *fmt; ++fmt) {
		if (*fmt != '%') {
			printchar(out,*fmt);
			rlen ++;
			continue;
		}

		/* this also skips first '%' */
		++fmt;

		/* skip any of the options for % */
		while((*fmt == '-') || ((*fmt >= '0')&&(*fmt <= '9')))
		{
			++fmt;
		}

		/* default base */
		base = 10;

		switch (*fmt) {
		case 's':
			s = va_arg(args, char *);
			s = s ? s : "<NULL>";
			len = strlen(s);
			rlen += len;
			for (i = 0; i < len; ++i)
				printchar(out,*s++);

			continue;
		case '%':
			printchar(out,'%');
			continue;
		case 'c':
			printchar(out,va_arg(args, int));
			continue;
		case 'x':
		case 'X':
			sign = 0;
			base = 16;
			break;
		case 'd':
			sign = 1;
			break;
		case 'u':
			sign = 0;
			break;
		default:
			break;
		}

		num = sign ? va_arg(args, int) : va_arg(args, unsigned int);

		rlen+=number(out,num, base, sign);
	}
	return rlen;
}

int xPrintf(const char *format, ...)
{
	va_list args;

	va_start( args, format );
	return printf_wrapped( 0, format, args );
}

int sprintf(char *out, const char *format, ...)
{
	va_list args;

	va_start( args, format );
	return printf_wrapped( &out, format, args );
}

int snprintf( char *buf, unsigned int count, const char *format, ... )
{
	va_list args;

	( void ) count;

	va_start( args, format );
	return printf_wrapped( &buf, format, args );
}
