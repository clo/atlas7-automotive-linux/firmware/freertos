/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _AES_H_
#define _AES_H_

#define AES_FRAME_BYTE_NUM      32          /* 128 bit */
#define AES_MAX_FIFO_DEPTH      32          /* 32 DWORDs */


typedef volatile  unsigned int    VUINT32,  *PVUINT32;


typedef unsigned long DWORD;
typedef DWORD *PDWORD;
typedef unsigned char   BYTE;


typedef BYTE *LPBYTE;
typedef BYTE *PBYTE;

typedef struct AES_REG
{
    VUINT32 modeConfig;
    VUINT32 dataCnt;
    VUINT32 ctrCountInit[4];
    VUINT32 ctrKey[4];
    VUINT32 startCp;
    VUINT32 status;
    VUINT32 intEn;
    VUINT32 ctrCountMask[4];
    VUINT32 res[47];

    VUINT32 txDmaAddr;
    VUINT32 txDmaLen;
    VUINT32 txDmaCtrl;
    VUINT32 txFifoLevelChk;
    VUINT32 txFifoOp;
    VUINT32 txFifoStatus;
    VUINT32 txFifoData;
    VUINT32 res2[57];

    VUINT32 rxDmaAddr;
    VUINT32 rxDmaLen;
    VUINT32 rxDmaCtrl;
    VUINT32 rxFifoLevelChk;
    VUINT32 rxFifoOp;
    VUINT32 rxFifoStatus;
    VUINT32 rxFifoData;
}AES_REG;

/* AES_MODE_CONFIG */
#define AES_ENABLE          0x01
#define AES_RESET_SETUP     0x02
#define AES_RESET_CLEAR     0x00
#define AES_LITTLE_ENDIAN   0x04
#define AES_CTR_MODE        0x08

/* AES_TX _DMA_CTRL and AES_RX _DMA_CTRL */
#define AES_IO_MODE         0x01
#define AES_DMA_MODE        0x00
#define AES_READ_FIFO       0x02
#define AES_WRITE_FIFO      0x00
#define AES_RW_BIG_ENDIAN   0x08
#define AES_FLUSH_RX_FIFO   0x04

/* AES_START_CP */
#define AES_START           0x01
#define AES_TX_DMA_START    0x02
#define AES_RX_DMA_START    0x04

/* AES_STATUS */
#define AES_COMPUTE_COMPLETED     0x01
#define AES_TX_DMA_COMPLETED      0x02
#define AES_RX_DMA_COMPLETED      0x04

/* AES_TX _FIFO_STATUS and AES_RX _FIFO_STATUS */
#define	AES_FIFO_EMPTY	          0x2
#define	AES_FIFO_FULL	          0x1


#endif /* _AES_H_ */
