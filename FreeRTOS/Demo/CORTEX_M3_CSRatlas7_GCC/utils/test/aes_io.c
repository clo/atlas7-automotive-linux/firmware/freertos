/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "debug.h"
#include "io.h"

#include "p2.h"
#include "aes.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

unsigned char xResultData[AES_FRAME_BYTE_NUM] = { 0x83, 0x02, 0xa3, 0xc7, 0x57, 0x2c, 0xc6, 0x69,
			0xf7, 0x0a, 0xe7, 0x3f, 0x84, 0xbf, 0x05, 0x65, 0x8f, 0xeb, 0x20, 0x67,
			0x68, 0xb0, 0x36, 0x8b, 0xc9, 0x15, 0x85, 0x62, 0x8f, 0x5c, 0x34, 0x5b };

void vMemPrint( char *u_pComment, void *u_pAdrs, unsigned long u_byteNum )
{
	unsigned long *pAdrs = ( unsigned long * )u_pAdrs;
	unsigned long dwNum = ( u_byteNum + sizeof(unsigned long) - 1) / sizeof( unsigned long );
	unsigned long i;

	DebugMsg( "%s, address: 0x%x \r\n", u_pComment, (unsigned long )pAdrs );

	for(i=0; i<dwNum; i++)
	{
		DebugMsg( "0x%x ", pAdrs[i] );

		if(7 == (i%8))
		{
			DebugMsg("\r\n");
		}
	}

	DebugMsg("\r\n");
}


BOOL xErrPrint( char *u_pCalled, char *u_pCalling )
{
	DebugMsg( "Error! Failed to call %s() in %s(). \r\n", u_pCalled, u_pCalling );

	return FALSE;
}

BOOL xResultCmp( void *u_pAdrs )
{
	int i;
	unsigned char *pAdrs = ( unsigned char * )u_pAdrs;

	for( i=0; i<AES_FRAME_BYTE_NUM; i++ )
	{
		if( pAdrs[i] != xResultData[i] )
		{
			return xErrPrint( "resultCmp", "aesEngine_io" );
		}
	}
	DebugMsg( "aes test pass\r\n" );
	return TRUE;
}
/**************************************************************************************************
    Notice: u_desAdrs and u_srcAdrs must be unsigned long aligned.
*/
void vDwCopy( unsigned long u_desAdrs, unsigned long u_srcAdrs, unsigned long u_byteNum )
{
	unsigned long *pDes = ( unsigned long * )u_desAdrs;
	unsigned long *pSrc = ( unsigned long * )u_srcAdrs;
	unsigned long length = u_byteNum / sizeof( unsigned long );
	unsigned long i;

	for( i=0; i<length; i++ )
	{
		pDes[i] = pSrc[i];
	}
}

BOOL xAesEngineIO( unsigned long *u_pKey, unsigned long *u_pInitCount, unsigned long *w_pBuf, unsigned long u_bufLen )
{
	AES_REG *pReg = ( AES_REG * )_AES_MODULE_BASE;
	unsigned long frameNum = ( u_bufLen + AES_FRAME_BYTE_NUM - 1 ) / AES_FRAME_BYTE_NUM;
	unsigned long dwTotalNum = frameNum * AES_FRAME_BYTE_NUM / sizeof( unsigned long );
	unsigned long txNum, rxNum;

	pReg->modeConfig = AES_ENABLE | AES_RESET_SETUP;
	pReg->modeConfig = AES_ENABLE | AES_RESET_CLEAR | AES_LITTLE_ENDIAN | AES_CTR_MODE | 0x20;

	pReg->dataCnt    = frameNum * AES_FRAME_BYTE_NUM;

	pReg->txDmaLen  = frameNum * AES_FRAME_BYTE_NUM;
	pReg->txDmaAddr  = 0;
	pReg->txDmaCtrl  = AES_IO_MODE | AES_WRITE_FIFO | AES_RW_BIG_ENDIAN;
	pReg->txFifoOp   = 2;
	pReg->txFifoOp   = 0;
	pReg->txFifoOp   = 1;

	pReg->rxDmaLen  = frameNum * AES_FRAME_BYTE_NUM;
	pReg->rxDmaAddr  = 0;
	pReg->rxDmaCtrl  = AES_IO_MODE | AES_READ_FIFO | AES_RW_BIG_ENDIAN | AES_FLUSH_RX_FIFO;
	pReg->rxFifoOp   = 2;
	pReg->rxFifoOp   = 0;
	pReg->rxFifoOp   = 1;

	vDwCopy( ( unsigned long )pReg->ctrKey, ( unsigned long )u_pKey, 4 * sizeof( unsigned long ));
	vDwCopy( ( unsigned long )pReg->ctrCountInit, ( unsigned long )u_pInitCount, 4 * sizeof( unsigned long ) );

	pReg->startCp = AES_START;

	txNum = rxNum = 0;
	while( rxNum < dwTotalNum )
	{

		while( AES_FIFO_FULL != ( pReg->txFifoStatus & AES_FIFO_FULL ) && txNum < dwTotalNum )
		{
			pReg->txFifoData = w_pBuf[txNum];
			txNum++;
		}

		while( AES_FIFO_EMPTY != ( pReg->rxFifoStatus & AES_FIFO_EMPTY ) && rxNum < dwTotalNum )
		{
			w_pBuf[rxNum] = pReg->rxFifoData;
			rxNum++;
		}
	}

	return TRUE;
}

static portBASE_TYPE prvAesCmd( char *pcWriteBuffer,
				size_t xWriteBufferLen,
				const char *pcCommandString )
{
	unsigned char key[AES_FRAME_BYTE_NUM] = { 0xca, 0x9f, 0x83, 0x95, 0x70, 0xd0, 0xd0, 0xf9, 0xcf, 0xe4, 0xeb, 0x54, 0x7e, 0x09, 0xfa, 0x3b };
	unsigned char initCount[AES_FRAME_BYTE_NUM] = { 0xf9, 0xf1, 0x30, 0xa8, 0x2d, 0x5b, 0xe5, 0xc3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };    
	unsigned char data[AES_FRAME_BYTE_NUM] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	unsigned char backupData[AES_FRAME_BYTE_NUM] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	if( FALSE == xAesEngineIO( ( unsigned long * )key, ( unsigned long * )initCount, ( unsigned long * )backupData, sizeof( data ) ) )
	{
		return xErrPrint( "aesEngine_io", "runCompare" );
	}

	vMemPrint( "io data", backupData, AES_FRAME_BYTE_NUM );
	if( xResultCmp( backupData ) == TRUE )
		return pdFALSE;
	else
		return 0;
}

static const CLI_Command_Definition_t xAesParameter =
{
	"aes",
	"\r\naes\r\n \
	aes test by io mode\r\n",
	prvAesCmd,
	0
};
const CLI_Command_Definition_t *P_AES_CLI_Parameter = &xAesParameter;
