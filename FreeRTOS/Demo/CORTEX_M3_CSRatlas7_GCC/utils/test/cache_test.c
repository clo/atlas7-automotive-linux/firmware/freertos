/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "misclib.h"
#include "soc_cache.h"

static portBASE_TYPE prvCacheTestCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxType = 0, uxOperation = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL == pcParameter1 || NULL == pcParameter2 )
	{
		return 0;
	}
	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxType = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	pcParameter2[ xParameter2StringLength ] = 0x00;
	uxOperation = ( uint32_t )strtoul( pcParameter2, NULL, 0 );

	if ( uxType >= 3 || uxOperation >= 4 )
	{
		return 0;
	}

	DebugMsg( "CACHE Test:" );

	if ( uxType == 0 )
	{
		DebugMsg("DCACHE ");
		if ( uxOperation == 0 )
		{
			DebugMsg("Enable ...");
			vDcacheEnable();
		}
		else if ( uxOperation == 1 )
		{
			DebugMsg("Disable ...");
			vDcacheDisable();
		}
		else if ( uxOperation == 2 )
		{
			DebugMsg("Flush ...");
			vDcacheFlush();
		}
		else if ( uxOperation == 3 )
		{
			DebugMsg("Invalidate ...");
			vDcacheInvalidate();
		}
		DebugMsg("OK!\r\n");
	}
	else if ( uxType == 1 )
	{
		DebugMsg("ICACHE ");
		if ( uxOperation == 0 )
		{
			DebugMsg("Enable ...");
			vIcacheEnable();
		}
		else if ( uxOperation == 1 )
		{
			DebugMsg("Disable ...");
			vIcacheDisable();
		}
		else if ( uxOperation == 2 )
		{
			DebugMsg("Flush No support!\r\n");
		}
		else if ( uxOperation == 3 )
		{
			DebugMsg("Invalidate ...");
			vIcacheInvalidate();
		}
		DebugMsg("OK!\r\n");
	}
	else if ( uxType == 2 )
	{
		DebugMsg("I/DCACHE ");
		if ( uxOperation == 0 )
		{
			DebugMsg("Enable ...");
			vCacheEnable();
		}
		else if ( uxOperation == 1 )
		{
			DebugMsg("Disable ...");
			vCacheDisable();
		}
		else if ( uxOperation == 2 )
		{
			DebugMsg("Flush ...");
			vDcacheFlush();
		}
		else if ( uxOperation == 3 )
		{
			DebugMsg("Invalidate ...");
			vIcacheInvalidate();
			vDcacheInvalidate();
		}
		DebugMsg("OK!\r\n");
	}
	return pdFALSE;
}

const CLI_Command_Definition_t xCacheTestCmdParameter =
{
	"cache",
	"\r\ncache: <TYPE> <OPERATION> \r\n \
	TYPE:		0 - DCACHE \r\n \
			1 - ICACHE \r\n \
			2 - I/D CACHE \r\n \
	OPERATION:	0 - Enable \r\n \
			1 - Disable \r\n \
			2 - Flush \r\n \
			3 - Invalidate\r\n",
	prvCacheTestCmd,
	-1
};

const CLI_Command_Definition_t *P_CACHE_TEST_CLI_Parameter = &xCacheTestCmdParameter;
