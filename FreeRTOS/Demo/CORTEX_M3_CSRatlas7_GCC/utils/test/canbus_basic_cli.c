/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "soc_pwrc.h"
#include "soc_iobridge.h"

#include "CANmod3.h"
#include "canbus.h"

#include "misclib.h"
#include "io.h"
#include "pm.h"
#include "soc_pwrc.h"
#include "soc_iobridge.h"
#include "soc_cache.h"
#include "soc_ioc.h"

static portBASE_TYPE prvCanBusTranMesCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3, *pcParameter4, *pcParameter5, *pcParameter6,
		*pcParameter7, *pcParameter8, *pcParameter9, *pcParameter10, *pcParameter11, *pcParameter12;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength,
			xParameter4StringLength, xParameter5StringLength, xParameter6StringLength,
			xParameter7StringLength, xParameter8StringLength, xParameter9StringLength,
			xParameter10StringLength, xParameter11StringLength, xParameter12StringLength;
	uint32_t uxPort, uxMesgType, uxID, uxDataLen;
	UINT8 uxData[8] = {};
	CAN_MSGOBJECT xPacket;
	uint32_t uxRet, i;
	uint32_t uxBox = 31;	/* by default, use the last message box */

	#ifndef ENABLE_CAN_ERROR_TX_ABORT_TEST
	EventGroupHandle_t xEventHandle;
	EventBits_t uxBits;
	#endif

	uxPort = uxMesgType = uxID = uxDataLen = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	pcParameter4 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameter4StringLength );
	pcParameter5 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 5, &xParameter5StringLength );
	pcParameter6 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 6, &xParameter6StringLength );
	pcParameter7 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 7, &xParameter7StringLength );
	pcParameter8 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 8, &xParameter8StringLength );
	pcParameter9 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 9, &xParameter9StringLength );
	pcParameter10 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 10, &xParameter10StringLength );
	pcParameter11 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 11, &xParameter11StringLength );
	pcParameter12 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 12, &xParameter12StringLength );

	if( NULL == pcParameter1 || NULL == pcParameter2 )
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxPort = ( uint32_t )strtoul( pcParameter1, NULL, 0 );

	pcParameter2[ xParameter2StringLength ] = 0x00;
	uxMesgType = ( uint32_t )strtoul( pcParameter2, NULL, 0 );

	if( NULL != pcParameter3 )
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		uxID = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	}

	if( NULL != pcParameter4 )
	{
		pcParameter4[ xParameter4StringLength ] = 0x00;
		uxDataLen = ( uint32_t )strtoul( pcParameter4, NULL, 0 );
	}

	if( NULL != pcParameter5 )
	{
		pcParameter5[ xParameter5StringLength ] = 0x00;
		uxData[ 0 ] = ( uint32_t )strtoul( pcParameter5, NULL, 0 );
	}

	if( NULL != pcParameter6 )
	{
		pcParameter6[ xParameter6StringLength ] = 0x00;
		uxData[ 1 ] = ( uint32_t )strtoul( pcParameter6, NULL, 0 );
	}

	if( NULL != pcParameter7 )
	{
		pcParameter7[ xParameter7StringLength ] = 0x00;
		uxData[ 2 ] = ( uint32_t )strtoul( pcParameter7, NULL, 0 );
	}

	if( NULL != pcParameter8 )
	{
		pcParameter8[ xParameter8StringLength ] = 0x00;
		uxData[ 3 ] = ( uint32_t )strtoul( pcParameter8, NULL, 0 );
	}

	if( NULL != pcParameter9 )
	{
		pcParameter9[ xParameter9StringLength ] = 0x00;
		uxData[ 4 ] = ( uint32_t )strtoul( pcParameter9, NULL, 0 );
	}

	if( NULL != pcParameter10 )
	{
		pcParameter10[ xParameter10StringLength ] = 0x00;
		uxData[ 5 ] = ( uint32_t )strtoul( pcParameter10, NULL, 0 );
	}
	if( NULL != pcParameter11 )
	{
		pcParameter11[ xParameter11StringLength ] = 0x00;
		uxData[ 6 ] = ( uint32_t )strtoul( pcParameter11, NULL, 0 );
	}

	if( NULL != pcParameter12 )
	{
		pcParameter12[ xParameter12StringLength ] = 0x00;
		uxData[ 7 ] = ( uint32_t )strtoul( pcParameter12, NULL, 0 );
	}

	if( 0 != (uxMesgType&0x80000000) )
	{
		uxBox = (uxMesgType&0x1F);
		uxMesgType = 0;
	}

	DebugMsg( "message port is %d, type is %d, id is %x, len is %d, through box %d.\r\n",
			uxPort, uxMesgType, uxID, uxDataLen,uxBox);

	if( uxMesgType == 0 )
		xPacket.rtr = 0;
	else
		xPacket.rtr = 1;

	if( uxID & 0x1ffff800)
		xPacket.ide = 1;
	else
		xPacket.ide = 0;
	xPacket.id = uxID;
	xPacket.dlc = uxDataLen;

	for(i = 0; i < 8; i++)
		xPacket.data[ i ] = uxData[ i ];

	if( uxPort > CAN_PORT1 )
		uxPort = CAN_PORT1;


	#ifndef ENABLE_CAN_ERROR_TX_ABORT_TEST
	xEventHandle = xGetCanErrorEventGroup(uxPort);

	if(xEventHandle != NULL)
	{
		xEventGroupClearBits(xEventHandle,0xFFFF);
	}
	#endif

	uxRet = xCanSend( uxPort, &xPacket, uxBox, CAN_SEND_NORMAL );

	if ( CAN_OK != uxRet)
	{
		DebugMsg( "send failed, ret is 0x%x \r\n", uxRet );
	}
	else
	{
		#ifndef ENABLE_CAN_ERROR_TX_ABORT_TEST
		if(xEventHandle != NULL)
		{
			uxBits = xEventGroupWaitBits(xEventHandle,0xFFFF,pdTRUE,pdFALSE,1);
			if(uxBits != 0)
			{
				xCanAbortTxBufferN(uxPort,uxBox);
				DebugMsg(" abort tx as error 0x%X\r\n",uxBits);
			}
			else
			{
				xEventHandle = xGetCanTransmitEventGroup(uxPort,TRUE);
				if(xEventHandle != NULL)
				{
					uxBits = xEventGroupWaitBits(xEventHandle,0xFFFF,pdTRUE,pdFALSE,1);

					if(uxBits != 0)
					{
						DebugMsg(" send through message box 0x%X OK\r\n",(uxBits<<16));
					}
					else
					{
						xEventHandle = xGetCanTransmitEventGroup(uxPort,FALSE);
						if(xEventHandle != NULL)
						{
							uxBits = xEventGroupWaitBits(xEventHandle,0xFFFF,pdTRUE,pdFALSE,1);
							if(uxBits != 0)
							{
								DebugMsg(" send through message box 0x%X OK\r\n",uxBits);
							}
							else
							{
								DebugMsg(" failed as no tx confirmation\r\n");
							}
						}
					}
				}
			}
		}
		#endif
	}

	return 0;
}

static portBASE_TYPE prvCanBusEnablePortCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxPort = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxPort = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "can enable port is %d\r\n", uxPort );

	xCanStart( uxPort, CANOP_MODE_NORMAL );

	return 0;
}

static portBASE_TYPE prvCanBusQueueFullErrorEnableCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxEnable = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEnable = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "can queue full error is %s\r\n", uxEnable?"enabled":"disabled" );

	vCanSetQueueFullErrorEnabled( uxEnable );

	return 0;
}

static portBASE_TYPE prvCanBusDisablePortCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxPort = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxPort = ( uint32_t )strtoul(pcParameter1, NULL, 0);
	}

	DebugMsg( "can disable port is %d\r\n", uxPort );

	xCanStop( uxPort );

	return 0;
}

static portBASE_TYPE prvCanBusEnableCan1WakupCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxEnable = 0;


	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEnable = ( uint32_t )strtoul(pcParameter1, NULL, 0);
	}

	if(uxEnable)
	{
		MEM(PM_M3_CAN1_WAKUP) = M3_CAN1_WAKUP_FLAG_VALID;
		SOC_REG(A7DA_IOCTOP_FUNC_SEL_19_REG_CLR) =
						SHIFT_L(FUNC_MASK, 24) | SHIFT_L(FUNC_MASK, 28);
	}
	else
	{
		MEM(PM_M3_CAN1_WAKUP) = 0;
		SOC_REG(A7DA_IOCRTC_FUNC_SEL_0_REG_CLR) =
						SHIFT_L(FUNC_MASK, 8) | SHIFT_L(FUNC_MASK, 12);
	}

	vDcacheFlush();

	DebugMsg( "can1 wake up is %s\r\n", uxEnable?"enable":"disable" );


	return 0;
}


static const CLI_Command_Definition_t xCanbusTranMesParameter =
{
	"can_tran_mes",
	"\r\ncan_tran_mes <port> <type/box> <ID> <length> <byte 0 - 7>\r\n \
	port: 0 - can0; 1 - can1 \r\n \
	type/box: 0 - data; 1 - remote; if bit 0x80000000 set, indicate use which box to send\r\n",
	prvCanBusTranMesCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_TranMes_CLI_Parameter = &xCanbusTranMesParameter;

const CLI_Command_Definition_t xCanbusEnablePortParameter =
{
	"can_enable",
	"\r\ncan_enable <port_num>\r\n \
	port_num: 0 - CAN0, 1 - CAN1\r\n",
	prvCanBusEnablePortCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_EnablePort_CLI_Parameter = &xCanbusEnablePortParameter;


const CLI_Command_Definition_t xCanbusDisablePortParameter =
{
	"can_disable",
	"\r\ncan_disable <port_num>\r\n \
	port_num: 0 - CAN0, 1 - CAN1\r\n",
	prvCanBusDisablePortCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_DisablePort_CLI_Parameter = &xCanbusDisablePortParameter;


const CLI_Command_Definition_t xCanbusEnableCan1WakupParameter =
{
	"can1_wakup",
	"\r\ncan1_wakup <option>\r\n \
	option: 0 - disable, 1 - enable\r\n",
	prvCanBusEnableCan1WakupCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_EnableCan1Wakup_CLI_Parameter = &xCanbusEnableCan1WakupParameter;

const CLI_Command_Definition_t xCanbusQueueFullErrorEnableParameter =
{
	"can_queue_full_error",
	"\r\ncan_queue_full_error <enable>\r\n \
	enable: 0 - disable, 1 - enable\r\n",
	prvCanBusQueueFullErrorEnableCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_QueleFullErrorEnable_CLI_Parameter = &xCanbusQueueFullErrorEnableParameter;
