/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "misclib.h"
#include "canbus.h"
#include "canbus_test_lib.h"
#include "canbus_core_test_lib.h"
#include "canbus_core_test_basic.h"
#include "canbus_core_test_link.h"
#include "canbus_core_test_sst.h"
#include "canbus_core_test_error_detect.h"
#include "canbus_core_test_debug_mode.h"

static portBASE_TYPE prvCanBusCoreBasicTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxEn = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "%s canbus basic test\r\n", ( 0 == uxEn ? "stop" : "start" ) );
	if( eCanTestStop == uxEn )
		vCanbusCoreTestBasicStop();
	else
	{
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusCoreTestBasicStart();
	}

	return 0;
}

static portBASE_TYPE prvCanBusCoreLinkTestCommand( char *pcWriteBuffer,
	size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxNum;
	uint32_t uxEn = 0;

	uxNum = 32;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxNum = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}

	DebugMsg( "%s basic link test, basic number is %d.\r\n",
			( 0 == uxEn ? "stop" : "start" ), uxNum );
	if( eCanTestStop == uxEn )
		vCanbusCoreTestLinkStop();
	else
		vCanbusCoreTestLinkStart( uxNum );
	return 0;
}

static portBASE_TYPE prvCanBusCoreSSTTestCommand( char *pcWriteBuffer,
	size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxEn = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "%s sst test\r\n", ( 0 == uxEn ? "stop" : "start" ) );
	if( eCanTestStop == uxEn )
		vCanbusCoreTestSstStop();
	else
	{
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusCoreTestSstStart();
	}
	return 0;
}

enum {
	eCanCoreErrDetectStop = 0,
	eCanCoreErrDetectArb,
	eCanCoreErrDetectSst,
	eCanCoreErrDetectRxMsgLost
};
static portBASE_TYPE prvCanBusCoreErrorDetectTestCommand( char *pcWriteBuffer,
	size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxType;

	uxType = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxType = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	/* get the buffer number to be tested */
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		g_can_error_test_buf_num = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}
	else
	{
		g_can_error_test_buf_num = 0;
	}

	DebugMsg( "%s canbus error detect test, type is %d, buf num is %d.\r\n",
			(eCanCoreErrDetectStop == uxType ? "stop" : "start" ), uxType , g_can_error_test_buf_num);
	switch( uxType )
	{
	case eCanCoreErrDetectStop:
		vCanbusCoreTestErrorDetectStop();
		break;
	case eCanCoreErrDetectArb:
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusCoreTestErrorDetectStart();
		vCanbusCoreTestErrorDetectArbStart();
		break;
	case eCanCoreErrDetectSst:
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusCoreTestErrorDetectStart();
		vCanbusCoreTestErrordetectSstStart();
		break;
	case eCanCoreErrDetectRxMsgLost:
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusCoreTestErrorDetectStart();
		vCanbusCoreTestOverloadDetectStart();
		break;
	default:
		DebugMsg( "cannot support such type.\r\n" );
		return 0;
	}
	return 0;
}

static portBASE_TYPE prvCanBusCoreDebugModeTestInitCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxEn = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "canbus debug mode test %s\r\n", ( 0 == uxEn ? "stop" : "start" ) );

	if( eCanTestStop == uxEn )
		vCanbusDebugModeTestStop();
	else
	{
		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
		else
			vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
		vCanbusDebugModeTestStart();
	}

	return 0;
}

enum {
	eCanDebugNormal = 0,
	eCanDebugListenOnly,
	eCanDebugInternalLoopback,
	eCanDebugExternalLoopback,
};
static portBASE_TYPE prvCanBusCoreDebugmodeCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxPort = 0;
	uint32_t uxMode = 0;
	uint32_t uxModeCmd = CANOP_MODE_NORMAL;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxPort = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxMode = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}
	DebugMsg( "can port is %d, mode is %d\r\n", uxPort, uxMode );

	switch( uxMode )
	{
	case eCanDebugNormal:
		uxModeCmd = CANOP_MODE_NORMAL;
		break;
	case eCanDebugListenOnly:
		uxModeCmd = CANOP_MODE_LISTEN_ONLY;
		break;
	case eCanDebugInternalLoopback:
		uxModeCmd = CANOP_MODE_INT_LOOPBACK;
		break;
	case eCanDebugExternalLoopback:
		uxModeCmd = CANOP_MODE_EXT_LOOPBACK;
		break;
	default:
		DebugMsg( "can not support such type.\r\n" );
	}
	xCanStop( uxPort );
	xCanStart( uxPort, uxModeCmd );

	return 0;
}

static portBASE_TYPE prvCanBusCoreDebugTranCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3, *pcParameter4, *pcParameter5, *pcParameter6;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength, xParameter4StringLength, xParameter5StringLength, xParameter6StringLength;
	uint32_t uxPort, uxMsgType, uxID, uxLen;
	int32_t uxDataHigh, uxDataLow;
	CAN_MSGOBJECT uxPacket;
	int uxRet = CAN_OK;

	uxPort = uxMsgType = uxID = uxLen = uxDataHigh = uxDataLow = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	pcParameter4 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameter4StringLength );
	pcParameter5 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 5, &xParameter5StringLength );
	pcParameter6 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 6, &xParameter6StringLength );

	if( NULL == pcParameter1 || NULL == pcParameter2 )
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxPort = ( uint32_t )strtoul( pcParameter1, NULL, 0 );

	pcParameter2[ xParameter2StringLength ] = 0x00;
	uxMsgType = ( uint32_t )strtoul( pcParameter2, NULL, 0 );

	if( NULL != pcParameter3 )
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		uxID = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	}

	if( NULL != pcParameter4 )
	{
		pcParameter4[ xParameter4StringLength ] = 0x00;
		uxLen = ( uint32_t )strtoul( pcParameter4, NULL, 0 );
	}

	if( NULL != pcParameter5 )
	{
		pcParameter5[ xParameter5StringLength ] = 0x00;
		uxDataHigh = ( uint32_t )strtoul( pcParameter5, NULL, 0 );
	}

	if( NULL != pcParameter6 )
	{
		pcParameter6[ xParameter6StringLength ] = 0x00;
		uxDataLow = ( uint32_t )strtoul( pcParameter6, NULL, 0 );
	}

	DebugMsg( "message port is %d, type is %d, uxID is %x, uxLen is %d, data is 0x%x, 0x%x\r\n",
			uxPort, uxMsgType, uxID, uxLen, uxDataHigh, uxDataLow );

	if( uxMsgType == 0 )
		uxPacket.rtr = 0;
	else
		uxPacket.rtr = 1;

	if( uxID & 0x1ffff800)
		uxPacket.ide = 1;
	else
		uxPacket.ide = 0;
	uxPacket.id = uxID;
	uxPacket.dlc = uxLen;
	uxPacket.dataHigh = uxDataHigh;
	uxPacket.dataLow = uxDataLow;

	if( uxPort > CAN_PORT1 )
		uxPort = CAN_PORT1;

	uxRet = xCanSend( uxPort, &uxPacket, 2, CAN_SEND_NORMAL );

	if ( CAN_OK != uxRet )
		DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );

	return 0;
}

const CLI_Command_Definition_t xCanbusCoreBasicTestParameter =
{
	"can_basic",
	"\r\ncan_basic <en>\r\n \
	en:	0 - stop \r\n \
		1 - start \r\n",
	prvCanBusCoreBasicTestCommand,
	1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_BasicTest_CLI_Parameter = &xCanbusCoreBasicTestParameter;

const CLI_Command_Definition_t xCanbusCoreLinkTestParameter =
{
	"can_link",
	"\r\ncan_link <en> <basic_can_num>: can basic link test\r\n \
	en:	0 - stop \r\n \
		1 - start \r\rn \
	basic_can_num: 1 - 32\r\n",
	prvCanBusCoreLinkTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_LinkTest_CLI_Parameter = &xCanbusCoreLinkTestParameter;

const CLI_Command_Definition_t xCanbusCoreSSTTestParameter =
{
	"can_sst",
	"\r\ncan_sst: <en>\r\n \
	en:	0 - stop \r\n \
		1 - start \r\n",
	prvCanBusCoreSSTTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_SSTTest_CLI_Parameter = &xCanbusCoreSSTTestParameter;

const CLI_Command_Definition_t xCanbusCoreErrorDetectTestParameter =
{
	"can_error",
	"\r\ncan_error <type> [buf num]: canbus error detect\r\n \
	type: \r\n \
	0 - stop test\r\n \
	1 - canbus arb error detect\r\n \
	2 - canbus sst error detect\r\n \
	3 - canbus rx message loss detect\r\n \
	buf num: message box number 0~31, default 0\r\n",
	prvCanBusCoreErrorDetectTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_ErrorDetectTest_CLI_Parameter =
			&xCanbusCoreErrorDetectTestParameter;

const CLI_Command_Definition_t xCanbusCoreDebugModeTestInitParameter =
{
	"can_debug",
	"\r\ncan_debug: <en>\r\n \
	en:	0 - stop \r\n \
		1 - start \r\n",
	prvCanBusCoreDebugModeTestInitCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_Debugmode_Init_CLI_Parameter =
			&xCanbusCoreDebugModeTestInitParameter;

const CLI_Command_Definition_t xCanbusCoreDebugmodeParameter =
{
	"can_mode",
	"\r\ncan_mode <port_num> <mode>\r\n \
	port_num: 0 - CAN0, 1 - CAN1\r\n \
	mode: 	0 - normal, \r\n \
		1 - listen only, \r\n \
		2 - internel loop back, \r\n \
		3 - external loop back.\r\n",
	prvCanBusCoreDebugmodeCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_Debugmode_CLI_Parameter = &xCanbusCoreDebugmodeParameter;

static const CLI_Command_Definition_t xCanbusCoreDebugTranParameter =
{
	"can_debug_tran",
	"\r\ncan_debug_tran <port> <type> <ID> <length> <data1> <data2>\r\n \
	port: 0 - can0; 1 - can1 \r\n \
	type: 0 - data; 1 - remote\r\n",
	prvCanBusCoreDebugTranCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_CORE_DebugTran_CLI_Parameter = &xCanbusCoreDebugTranParameter;
