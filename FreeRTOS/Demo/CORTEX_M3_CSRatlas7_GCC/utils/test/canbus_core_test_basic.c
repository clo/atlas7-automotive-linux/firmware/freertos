/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "canbus.h"
#include "canbus_core_test_lib.h"
#include "canbus_core_test_basic.h"

static xTaskHandle s_xCanSWBasicTestTask = NULL;
/*-----------------------------------------------------------*/
static INT32
prvCanbusBasicCfgTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum )
{
	UINT32 xN, xM, xCfg;

	UINT32 xCan0Clk[] = { CAN_SPEED_40M_5K, CAN_SPEED_40M_10K, CAN_SPEED_40M_20K,
			CAN_SPEED_40M_50K, CAN_SPEED_40M_100K, CAN_SPEED_40M_125K,
			CAN_SPEED_40M_250K, CAN_SPEED_40M_500K, CAN_SPEED_40M_800K,
			CAN_SPEED_40M_1M };

	UINT32 xCan1Clk[] = { CAN_SPEED_48M_5K, CAN_SPEED_48M_10K, CAN_SPEED_48M_20K,
			CAN_SPEED_48M_50K, CAN_SPEED_48M_100K, CAN_SPEED_48M_125K,
			CAN_SPEED_48M_250K, CAN_SPEED_48M_500K, CAN_SPEED_48M_800K,
			CAN_SPEED_48M_1M };

	char * pcBaud[] = { "5K", "10K", "20K", "50K", "100K", "150K", "250K", "500K", "800K", "1M" };

	for( xN = 1; xN < sizeof( xCan0Clk ) / sizeof( UINT32 ); xN++ )
	{
		for( xM = 0; xM < 2; xM++ )
		{
			if( 0 == xM )
				xCfg = CAN_BIG_ENDIAN;
			else
				xCfg = CAN_LITTLE_ENDIAN;

			DebugMsg( "buffer(%d) re-configure core configure:\r\n", ucBufNum );
			DebugMsg( "baudrate is %s\r\n", pcBaud[xN] );
			DebugMsg( "work mode is %s\r\n", ( ( 0 == xM ) ? "Big" : "Little" ) );

			if( configCPU_CLOCK_HZ == 192000000 )
				xCanReConfigCoreInRunning( CAN_PORT0, xCfg | xCan1Clk[ xN ] | CAN_AUTO_RESTART );
			else
				xCanReConfigCoreInRunning( CAN_PORT0, xCfg | xCan0Clk[ xN ] | CAN_AUTO_RESTART );
			xCanReConfigCoreInRunning( CAN_PORT1, xCfg | xCan1Clk[ xN ] | CAN_AUTO_RESTART );

			if( false == xCanbusBasicDataTest( ucRxPort, xQueue, ucTxPort, ucBufNum, xM ) )
			{
				DebugMsg( "data test failed\r\n" );
				return false;
			}
			if( false == xCanbusBasicRTRTest( ucRxPort, xQueue, ucTxPort, ucBufNum ) )
			{
				DebugMsg( "rtr test failed\r\n" );
				return false;
			}
			if( false == xCanbusBasicFilterTest( ucRxPort, xQueue, ucTxPort, ucBufNum, xM ) )
			{
				DebugMsg( "filter test failed\r\n" );
				return false;
			}
		}
	}
	return true;
}

static INT32 prvCanbusBasicBufferTest( UINT8 ucRxPort, UINT8 ucTxPort )
{
	UINT32 xBufNum;
	CAN_FILTEROBJECT xFilter;
	xQueueHandle xQueue = NULL;

	for( xBufNum = 0; xBufNum < 32; xBufNum++ )
	{
		xFilter.amr.l = 0xffffffff;
		xFilter.acr.l = 0;
		xFilter.amcr_d.mask = 0xffff;
		xFilter.amcr_d.code = 0;
		DebugMsg( "buffer number is %d\r\n", xBufNum );
		xQueue = xCanReceiveQueueCreate( ucRxPort, &xFilter, xBufNum, 4 );
		if( NULL == xQueue )
		{
			DebugMsg( "create xQueue failed.\r\n" );
			return false;
		}
		if( false == prvCanbusBasicCfgTest( ucRxPort, xQueue, ucTxPort, xBufNum ) )
			return false;
		xCanStopReceiveQueue( ucRxPort, xBufNum );
		xCanDeleteReceiveQueue( ucRxPort, xBufNum );
	}
	return true;
}

static void prvCanbusBasicTest(void * pvParameters)
{
	for(;;)
	{
		DebugMsg( "can bus basic test start, can0 is rx, can1 is tx\r\n" );
		if( false == prvCanbusBasicBufferTest( CAN_PORT0, CAN_PORT1 ) )
			break;
		DebugMsg( "can bus basic test start, can1 is rx, can0 is tx\r\n" );
		if( false == prvCanbusBasicBufferTest( CAN_PORT1, CAN_PORT0 ) )
			break;
		DebugMsg( "can bus basic test success.\r\n" );
		vTaskDelete(NULL);
	}
	DebugMsg( "can bus basic test failed\r\n" );
	vTaskDelete(NULL);
}


void vCanbusCoreTestBasicStart( void )
{
	if( !s_xCanSWBasicTestTask )
		xTaskCreate( prvCanbusBasicTest, "CBT", 512,
			"can_basic", tskIDLE_PRIORITY + 1, &s_xCanSWBasicTestTask );
}

void vCanbusCoreTestBasicStop( void )
{

	if( s_xCanSWBasicTestTask )
	{
		vTaskDelete( s_xCanSWBasicTestTask );
		s_xCanSWBasicTestTask = NULL;
	}

	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );

}
