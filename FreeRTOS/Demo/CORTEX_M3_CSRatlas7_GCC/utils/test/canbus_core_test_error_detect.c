/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"

#include "canbus.h"
#include "canbus_test_lib.h"
#include "canbus_core_test_lib.h"
#include "canbus_core_test_error_detect.h"

static xTaskHandle s_xCan0ReceiveTask = NULL;
static xTaskHandle s_xCan1ReceiveTask = NULL;
static xTaskHandle s_xCanCoreErrorArbTask = NULL;
static xTaskHandle s_xCanCoreErrorSSTTask = NULL;
static xTaskHandle s_xCan0CoreOverloadRxTask = NULL;
static xTaskHandle s_xCan0CoreOverloadTxTask = NULL;
static xTaskHandle s_xCan1CoreOverloadRxTask = NULL;
static xTaskHandle s_xCan1CoreOverloadTxTask = NULL;
/*-----------------------------------------------------------*/
UINT8 g_can_error_test_buf_num=0;
static void prvCanbus0ErrorDetectTestArb( UINT8 ucIde )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xPacket0, xPacket1;
	UINT32 uxRet, uxN;

	for( uxN = 0; uxN < CAN_ERROR_DETECT_TEST_NUM; uxN++ )
	{
		xPacket0.ide = 0;
		xPacket0.rtr = 0;
		if( true == ucIde )
			xPacket0.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
		else
			xPacket0.id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX;

		if(xPacket0.id > CAN_BUS_TEST_NORMAL_ID_MAX)
			xPacket0.ide = 1;
		xPacket0.dlc = rand_ul() % 9;
		xPacket0.dataHigh = rand_ul();
		xPacket0.dataLow = rand_ul();

		xPacket1.id = xPacket0.id + 1;
		xPacket1.rtr = 0;
		xPacket1.ide = xPacket0.ide;
		xPacket1.dlc = rand_ul() % 9;
		xPacket1.dataHigh = rand_ul();
		xPacket1.dataLow = rand_ul();
		DebugMsg( "can 0 id(0x%x) \r\n", xPacket0.id );
		DebugMsg( "can 0 dlc(%d) data(0x%x) (0x%x) \r\n",
			xPacket0.dlc, xPacket0.dataHigh, xPacket0.dataLow );
		DebugMsg( "can 1 id(0x%x) \r\n", xPacket1.id);
		DebugMsg( "can 1 dlc(%d) data(0x%x) (0x%x) \r\n",
			xPacket1.dlc, xPacket1.dataHigh, xPacket1.dataLow );

		uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ERROR_TEST_ARB_BUF_NUM, CAN_SEND_NORMAL );
		if ( CAN_OK != uxRet)
		{
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet);
			if(CAN_OK != xCanAbortTxBufferN( CAN_PORT0, CAN_ERROR_TEST_ARB_BUF_NUM ) )
				DebugMsg( "abort tx message failed \r\n" );
			if(CAN_OK != xCanAbortTxBufferN( CAN_PORT1, CAN_ERROR_TEST_ARB_BUF_NUM ) )
				DebugMsg( "abort tx message failed \r\n" );
		}
		DebugMsg( "test time(%d) \r\n", uxN );
		vTaskDelayUntil( &xLastWakeTime, CAN_ERROR_TEST_TIMEOUT );
	}
}

static void prvCanbus1ErrorDetectTestArb( UINT8 ucIde )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xPacket0, xPacket1;
	UINT32 uxRet, uxN;

	for( uxN = 0; uxN < CAN_ERROR_DETECT_TEST_NUM; uxN++ )
	{
		xPacket0.ide = 0;
		xPacket0.rtr = 0;
		if( true == ucIde )
			xPacket0.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
		else
			xPacket0.id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX;
		if( xPacket0.id > CAN_BUS_TEST_NORMAL_ID_MAX )
			xPacket0.ide = 1;
		xPacket0.dlc = rand_ul() % 9;
		xPacket0.dataHigh = rand_ul();
		xPacket0.dataLow = rand_ul();

		xPacket1.id = xPacket0.id - 1;
		xPacket1.ide = xPacket0.ide;
		xPacket1.rtr = 0;
		xPacket1.dlc = rand_ul() % 9;
		xPacket1.dataHigh = rand_ul();
		xPacket1.dataLow = rand_ul();
		DebugMsg( "can 0 id(0x%x) \r\n", xPacket0.id );
		DebugMsg( "can 0 dlc(%d) data(0x%x) (0x%x) \r\n",xPacket0.dlc, xPacket0.dataHigh, xPacket0.dataLow );
		DebugMsg( "can 1 id(0x%x) \r\n", xPacket1.id);
		DebugMsg( "can 1 dlc(%d) data(0x%x) (0x%x) \r\n",xPacket1.dlc, xPacket1.dataHigh, xPacket1.dataLow );

		uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ERROR_TEST_ARB_BUF_NUM, CAN_SEND_NORMAL );
		if ( CAN_OK != uxRet )
		{
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );
			if( CAN_OK != xCanAbortTxBufferN( CAN_PORT0, CAN_ERROR_TEST_ARB_BUF_NUM ) )
				DebugMsg( "abort tx message failed \r\n" );
			if( CAN_OK != xCanAbortTxBufferN( CAN_PORT1, CAN_ERROR_TEST_ARB_BUF_NUM ) )
				DebugMsg( "abort tx message failed \r\n" );
		}
		DebugMsg( "test time(%d) \r\n", uxN );
		vTaskDelayUntil( &xLastWakeTime, CAN_ERROR_TEST_TIMEOUT );
	}
}

static void prvCanbusCoreTestErrorDetectArb( void *pvParameters )
{
	for( ; ; )
	{
		DebugMsg( "can 0 is high priority...\r\n" );
		DebugMsg( "normal id:\r\n" );
		prvCanbus0ErrorDetectTestArb( 0 );
		DebugMsg( "extend id:\r\n" );
		prvCanbus0ErrorDetectTestArb( 1 );
		DebugMsg( "can 1 is high priority...\r\n" );
		DebugMsg( "normal id:\r\n" );
		prvCanbus1ErrorDetectTestArb( 0 );
		DebugMsg( "extend id:\r\n" );
		prvCanbus1ErrorDetectTestArb( 1 );
		DebugMsg( "can arb error detect finish.\r\n" );
		vTaskDelete(NULL);
	}
}

void vCanbusCoreTestErrorDetectArbStart( void )
{
	DebugMsg( "can arb error detect start.\r\n" );
	if( !s_xCanCoreErrorArbTask )
		xTaskCreate( prvCanbusCoreTestErrorDetectArb, "CCTEDA", configMINIMAL_STACK_SIZE + 64,
			NULL, tskIDLE_PRIORITY + 1 , &s_xCanCoreErrorArbTask );
}


static void prvCanbus0ErrorDetectTestSst( UINT8 ucIde )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xPacket0, xPacket1;
	UINT32 uxRet, uxN;

	for( uxN = 0; uxN < CAN_ERROR_DETECT_TEST_NUM; uxN++ )
	{
		xPacket0.ide = 0;
		xPacket0.rtr = 0;
		if( true == ucIde )
			xPacket0.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
		else
			xPacket0.id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX;
		DebugMsg( "can 0 id(0x%x) \r\n", xPacket0.id );
		if( xPacket0.id > CAN_BUS_TEST_NORMAL_ID_MAX )
			xPacket0.ide = 1;
		xPacket0.dlc = rand_ul() % 9;
		xPacket0.dataHigh = rand_ul();
		xPacket0.dataLow = rand_ul();

		xPacket1.id = xPacket0.id + 1;
		DebugMsg( "can 1 id(0x%x) \r\n", xPacket1.id );
		xPacket1.rtr = 0;
		xPacket1.ide = xPacket0.ide;
		xPacket1.dlc = rand_ul() % 9;
		xPacket1.dataHigh = rand_ul();
		xPacket1.dataLow = rand_ul();

		uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ERROR_TEST_SST_BUF_NUM, CAN_SEND_SST );
		if ( CAN_OK != uxRet )
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );
		DebugMsg( "test time(%d) \r\n", uxN );
		vTaskDelayUntil( &xLastWakeTime, CAN_ERROR_TEST_TIMEOUT );
	}
}

static void prvCanbus1ErrorDetectTestSst( UINT8 ucIde )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xPacket0, xPacket1;
	UINT32 uxRet, uxN;

	for( uxN = 0; uxN < CAN_ERROR_DETECT_TEST_NUM; uxN++ )
	{
		xPacket0.ide = 0;
		xPacket0.rtr = 0;
		if(1 == ucIde)
			xPacket0.id = rand_ul()%CAN_BUS_TEST_EXT_ID_MAX;
		else
			xPacket0.id = rand_ul()%CAN_BUS_TEST_NORMAL_ID_MAX;
		if(xPacket0.id > CAN_BUS_TEST_NORMAL_ID_MAX)
			xPacket0.ide = 1;
		xPacket0.dlc = rand_ul()%9;
		xPacket0.dataHigh = rand_ul();
		xPacket0.dataLow = rand_ul();

		xPacket1.id = xPacket0.id - 1;
		xPacket1.ide = xPacket0.ide;
		xPacket1.rtr = 0;
		xPacket1.dlc = rand_ul()%9;
		xPacket1.dataHigh = rand_ul();
		xPacket1.dataLow = rand_ul();

		uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ERROR_TEST_SST_BUF_NUM, CAN_SEND_SST );
		if ( CAN_OK != uxRet)
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet);
		DebugMsg( "test time(%d) \r\n", uxN);
		vTaskDelayUntil( &xLastWakeTime, CAN_ERROR_TEST_TIMEOUT );
	}
}

static void prvCanbusCoreTestErrorDetectSst( void *pvParameters )
{
	for( ; ; )
	{
		DebugMsg( "can 0 is high priority...\r\n" );
		DebugMsg( "normal id:\r\n" );
		prvCanbus0ErrorDetectTestSst( 0 );
		DebugMsg( "extend id:\r\n" );
		prvCanbus0ErrorDetectTestSst( 1 );
		DebugMsg( "can 1 is high priority...\r\n" );
		DebugMsg( "normal id:\r\n" );
		prvCanbus1ErrorDetectTestSst( 0 );
		DebugMsg( "extend id:\r\n" );
		prvCanbus1ErrorDetectTestSst( 1 );
		DebugMsg( "can sst error detect finish.\r\n" );
		vTaskDelete(NULL);
	}
}

void vCanbusCoreTestErrordetectSstStart( void )
{
	DebugMsg( "can sst error detect start.\r\n" );
	if( !s_xCanCoreErrorSSTTask )
		xTaskCreate( prvCanbusCoreTestErrorDetectSst, "CCTEDS", configMINIMAL_STACK_SIZE + 64,
			NULL, tskIDLE_PRIORITY + 1 ,&s_xCanCoreErrorSSTTask );
}

static void prvCanbusCoreTestOverloadDetectRx( void* pvPort )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xRxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxPort = ( UINT32 ) pvPort;

	xFilter.amr.l = 0xffffffff;
	xFilter.amr.normal_id = CAN_ID_FOR_OVER_LOAD;
	xFilter.acr.l = 0;
	xFilter.acr.normal_id = CAN_ID_FOR_OVER_LOAD;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xQueueHandle xQueue = xCanReceiveQueueCreate( uxPort, &xFilter, CAN_ERROR_TEST_OVER_LOAD_BUF_NUM, 1 );
	if( xQueue == NULL )
	{
		DebugMsg( "receive function create queue failed.\r\n" );
		vTaskDelete(NULL);
	}
	for( ; ; )
	{
		xQueueReceive( xQueue, &xRxPacket, portMAX_DELAY );
		DebugMsg( "can(%d) receive id is 0x%x, data is 0x%x, 0x%x\r\n", uxPort,
			xRxPacket.id, xRxPacket.dataHigh, xRxPacket.dataLow );
		vTaskDelayUntil( &xLastWakeTime, 200 );
	}
}

static void prvCanbusCoreTestOverloadDetectTx( void* pvPort )
{
	CAN_MSGOBJECT xPacket;
	UINT32 uxRet;
	UINT32 uxPort = ( UINT32 ) pvPort;

	xPacket.ide = 0;
	xPacket.id = CAN_ID_FOR_OVER_LOAD;
	xPacket.rtr = 0;

	for( ; ; )
	{
		xPacket.dlc = rand_ul()%9;
		xPacket.dataHigh = rand_ul();
		xPacket.dataLow = rand_ul();
		uxRet = xCanSend( uxPort, &xPacket, CAN_ERROR_TEST_OVER_LOAD_BUF_NUM, CAN_SEND_NORMAL );
		if ( CAN_OK != uxRet )
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet);
		vTaskDelay( CAN_TEST_INTERVAL );
	}
}

void vCanbusCoreTestOverloadDetectStart( void )
{
	DebugMsg( "can over load message detect start.\r\n" );
	if( !s_xCan0CoreOverloadRxTask )
		xTaskCreate( prvCanbusCoreTestOverloadDetectRx, "CCTODR0", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1 , &s_xCan0CoreOverloadRxTask );
	if( !s_xCan0CoreOverloadTxTask )
		xTaskCreate( prvCanbusCoreTestOverloadDetectTx, "CCTODT0", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1 , &s_xCan0CoreOverloadTxTask );
	if( !s_xCan1CoreOverloadRxTask )
		xTaskCreate( prvCanbusCoreTestOverloadDetectRx, "CCTODR1", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1 , &s_xCan1CoreOverloadRxTask );
	if( !s_xCan1CoreOverloadTxTask )
		xTaskCreate( prvCanbusCoreTestOverloadDetectTx, "CCTODT1", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1 , &s_xCan1CoreOverloadTxTask );
}

void vCanbusCoreTestErrorDetectStart( void )
{
	if( !s_xCan0ReceiveTask )
		xTaskCreate( vCanbusReceiveFunction, "CRF0", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1 , &s_xCan0ReceiveTask );
	if( !s_xCan1ReceiveTask )
		xTaskCreate( vCanbusReceiveFunction, "CRF1", configMINIMAL_STACK_SIZE + 64,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1 , &s_xCan1ReceiveTask );
}

void vCanbusCoreTestErrorDetectStop( void )
{
	if( s_xCan0ReceiveTask )
	{
		vTaskDelete( s_xCan0ReceiveTask );
		s_xCan0ReceiveTask = NULL;
	}

	if( s_xCan1ReceiveTask )
	{
		vTaskDelete( s_xCan1ReceiveTask );
		s_xCan1ReceiveTask = NULL;
	}

	if( s_xCanCoreErrorArbTask )
	{
		vTaskDelete( s_xCanCoreErrorArbTask );
		s_xCanCoreErrorArbTask = NULL;
	}

	if( s_xCanCoreErrorSSTTask )
	{
		vTaskDelete( s_xCanCoreErrorSSTTask );
		s_xCanCoreErrorSSTTask = NULL;
	}

	if( s_xCan0CoreOverloadRxTask )
	{
		vTaskDelete( s_xCan0CoreOverloadRxTask );
		s_xCan0CoreOverloadRxTask = NULL;
	}

	if( s_xCan0CoreOverloadTxTask )
	{
		vTaskDelete( s_xCan0CoreOverloadTxTask );
		s_xCan0CoreOverloadTxTask = NULL;
	}

	if( s_xCan1CoreOverloadRxTask )
	{
		vTaskDelete( s_xCan1CoreOverloadRxTask );
		s_xCan1CoreOverloadRxTask = NULL;
	}

	if( s_xCan1CoreOverloadTxTask )
	{
		vTaskDelete( s_xCan1CoreOverloadTxTask );
		s_xCan1CoreOverloadTxTask = NULL;
	}

	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );

}
