/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"
#include "soc_irq.h"
#include "core_cm3.h"

#include "canbus.h"
#include "canbus_core_test_lib.h"


#define CAN_BUS_TEST_NORMAL_ID_MAX	0x800
#define CAN_BUS_TEST_EXT_ID_MAX		0x20000000

INT32 g_RxBasicCanMb = 0;

/*-----------------------------------------------------------*/

static INT32
prvCanDataCheck( PCAN_MSGOBJECT pxRxMsg, PCAN_MSGOBJECT pxTxMsg, UINT8 ucRTR, UINT8 ucLittleEnd )
{
	INT32 xN = 0;

	if( ( pxRxMsg->id != pxTxMsg->id ) || ( pxRxMsg->dlc != pxTxMsg->dlc ) || ( pxRxMsg->rtr != pxTxMsg->rtr ) )
		goto err;
	if( 0 != ucRTR )
		return true;
	if( 1 == ucLittleEnd )
	{
		for( xN = 0; xN < pxTxMsg->dlc; xN++ )
		{
			if( pxRxMsg->data[ xN ] != pxTxMsg->data[ xN ] )
				goto err;
		}
	}
	else
	{
		if( pxTxMsg->dlc < 5 )
			for( xN = 0; xN < pxTxMsg->dlc; xN++ )
			{
				if( pxRxMsg->data[ 3 - xN ] != pxTxMsg->data[ 3 - xN ] )
					goto err;
			}
		else
		{
			if( pxRxMsg->dataHigh != pxTxMsg->dataHigh )
				goto err;
			for( xN = 0; xN < pxTxMsg->dlc - 4; xN++ )
			{
				if( pxRxMsg->data[ 7 - xN ] != pxTxMsg->data[ 7 - xN ] )
					goto err;
			}
		}
	}
	return true;
err:
	DebugMsg( "tx msg: rtr( 0x%x), id( 0x%x), data_len( 0x%x), datahigh( 0x%x), datalow( 0x%x)\r\n",
		pxTxMsg->rtr, pxTxMsg->id, pxTxMsg->dlc, pxTxMsg->dataHigh, pxTxMsg->dataLow );
	DebugMsg( "rx msg: rtr( 0x%x), id( 0x%x), data_len( 0x%x), datahigh( 0x%x), datalow( 0x%x)\r\n",
		pxRxMsg->rtr, pxRxMsg->id, pxRxMsg->dlc, pxRxMsg->dataHigh, pxRxMsg->dataLow );
	return false;
}

INT32
xCanbusBasicRTRTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum )
{
	UINT32 uxN;
	CAN_MSGOBJECT xRxPacket, xTxPacket;
	CAN_FILTEROBJECT xFilter;

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

	DebugMsg( "start rtr test>>>>>>>>\r\n" );
	for( uxN = 0; uxN < CAN_BASIC_TEST_NUM; uxN++ )
	{
		for( xTxPacket.ide = 0; xTxPacket.ide < 2; xTxPacket.ide++ )
		{
			if( 0 == xTxPacket.ide)
				xTxPacket.id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX ;
			else
				xTxPacket.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;

			xTxPacket.rtr = 1;
			xTxPacket.dlc = rand_ul() % 9;
			xTxPacket.dataHigh = xTxPacket.dataLow = 0;
			xCanSend( ucTxPort, &xTxPacket, ucBufNum, CAN_SEND_NORMAL );

			xRxPacket.dlc = 0;
			xRxPacket.rtr = 0;
			xRxPacket.id = 0;
			xRxPacket.dataHigh = xRxPacket.dataLow = 0;
			xQueueReceive( xQueue, &xRxPacket, CAN_BASIC_RECEIVE_TIMEOUT );
			if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 1, 0 ) )
			{
				DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxN );
				return false;
			}
			vTaskDelay( CAN_TEST_INTERVAL );
		}
		DebugMsg( "test(%d) success.\r\n", uxN );
	}
	return true;
}

static INT32
prvCanbusBasicFilterIdeTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd )
{
	CAN_MSGOBJECT xRxPacket, xTxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxRet = 0;
	UINT32 uxN;
	UINT8 ucRxIde, ucTxIde;

	DebugMsg( "start filter ide test>>>>>>>>.\r\n" );
	xFilter.amr.l = 0xfffffffe;
	xFilter.amr.ide = 0;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	for( ucRxIde = 0; ucRxIde < 2; ucRxIde++ )
	{
		xFilter.acr.ide = ucRxIde;
		xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

		for( ucTxIde = 0; ucTxIde < 2; ucTxIde++ )
		{
			xTxPacket.ide = ucTxIde;
			DebugMsg( "filter ide is (%d), tx ide is (%d)\r\n", xFilter.acr.ide, xTxPacket.ide );
			for( uxN = 0; uxN < CAN_BASIC_FILTER_TEST_NUM; uxN++ )
			{
				if( 0 == xTxPacket.ide)
					xTxPacket.id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX ;
				else
					xTxPacket.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;

				xTxPacket.dlc = rand_ul() % 9;
				xTxPacket.rtr = 0;
				xTxPacket.dataHigh = rand_ul();
				xTxPacket.dataLow = rand_ul();
				xCanSend( ucTxPort, &xTxPacket, ucBufNum, CAN_SEND_SST );

				xRxPacket.dlc = 0;
				xRxPacket.rtr = 0;
				xRxPacket.id = 0;
				xRxPacket.dataHigh = xRxPacket.dataLow = 0;
				uxRet = xQueueReceive( xQueue, &xRxPacket, CAN_BASIC_RECEIVE_TIMEOUT );

				if( ( ( 0 == xFilter.acr.ide ) && ( 1 == xTxPacket.ide ) ) ||
					( ( 1 == xFilter.acr.ide ) && ( 0 == xTxPacket.ide ) ) )
				{
					if( errQUEUE_EMPTY != uxRet )
					{
						DebugMsg( "buffer(%d) filter ide test failed\r\n", ucBufNum );
						return false;
					}
				}
				else
				{
					if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 0, ucLittleEnd ) )
					{
						DebugMsg( "buffer(%d), test(%d) data check error:\r\n", ucBufNum, uxN );
						return false;
					}
				}
				DebugMsg( "test(%d) success.\r\n", uxN );
				vTaskDelay( CAN_TEST_INTERVAL );
			}
		}
	}
	return true;
}

static INT32
prvCanbusBasicFilterRTRTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd  )
{
	CAN_MSGOBJECT xRxPacket, xTxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxRet = 0;
	UINT32 uxN;
	UINT8 ucRxRtr, ucTxRtr;

	DebugMsg( "start filter rtr test>>>>>>>>\r\n" );
	xFilter.amr.l = 0xfffffffe;
	xFilter.amr.rtr = 0;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	for( ucRxRtr = 0; ucRxRtr < 2; ucRxRtr++ )
	{
		xFilter.acr.rtr = ucRxRtr;
		xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

		for( ucTxRtr = 0; ucTxRtr < 2; ucTxRtr++ )
		{
			xTxPacket.rtr = ucTxRtr;
			DebugMsg( "filter rtr is (%d), tx rtr is (%d)\r\n", xFilter.acr.rtr, xTxPacket.rtr );
			for( uxN = 0; uxN < CAN_BASIC_FILTER_TEST_NUM; uxN++ )
			{
				xTxPacket.ide = 0;
				xTxPacket.dlc = rand_ul() % 9;
				xTxPacket.dataHigh = rand_ul();
				xTxPacket.dataLow = rand_ul();
				xTxPacket.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
				if( xTxPacket.id > CAN_BUS_TEST_NORMAL_ID_MAX )
					xTxPacket.ide = 1;

				xCanSend( ucTxPort, &xTxPacket, ucBufNum, CAN_SEND_SST );

				xRxPacket.dlc = 0;
				xRxPacket.rtr = 0;
				xRxPacket.id = 0;
				xRxPacket.dataHigh = xRxPacket.dataLow = 0;
				uxRet = xQueueReceive( xQueue, &xRxPacket, CAN_BASIC_RECEIVE_TIMEOUT );
				if( ( ( 0 == xFilter.acr.rtr) && ( 1 == xTxPacket.rtr) ) ||
					( ( 1 == xFilter.acr.rtr) && ( 0 == xTxPacket.rtr) ) )
				{
					if( errQUEUE_EMPTY != uxRet )
					{
						DebugMsg( "buffer(%d) filter rtr test failed\r\n", ucBufNum );
						return false;
					}
				}
				else if( ( 0 == xFilter.acr.rtr) && ( 0 == xTxPacket.rtr ) )
				{
					if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 0, ucLittleEnd ) )
					{
						DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxN );
						return false;
					}
				}
				else
				{
					if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 1, 0 ) )
					{
						DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxN );
						return false;
					}
				}
				DebugMsg( "test(%d) success.\r\n", uxN );
				vTaskDelay( CAN_TEST_INTERVAL );
			}
		}
	}
	return true;
}

static INT32
prvCanbusBasicFilterIdTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd )
{
	CAN_MSGOBJECT xRxPacket, xTxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxRet = 0;
	UINT32 uxId, uxN, uxM;

	DebugMsg( "start filter id test>>>>>>>>\r\n" );

	xFilter.amr.l = 0;
	xFilter.amr.ide = 1;
	xFilter.amr.rtr = 1;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	for( uxN = 0; uxN < CAN_BASIC_FILTER_TEST_NUM; uxN++ )
	{
		uxId= rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
		if( uxId > 0x7ff )
		{
			xFilter.acr.normal_id = uxId >> 18;
			xFilter.acr.ext_id = uxId & 0x3ffff;
		}
		else
			xFilter.acr.normal_id = uxId;

		xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

		for( uxM = 0; uxM < 10; uxM++ )
		{
			xTxPacket.dlc = rand_ul() % 9;
			xTxPacket.rtr = 0;
			xTxPacket.ide = 0;
			xTxPacket.dataHigh = rand_ul();
			xTxPacket.dataLow = rand_ul();
			xTxPacket.id = xFilter.acr.l >> 3;
			if( 0 != uxM % 2 )
				xTxPacket.id++;

			if( xTxPacket.id > 0x7ff )
				xTxPacket.ide = 1;
			xCanSend( ucTxPort, &xTxPacket, ucBufNum, CAN_SEND_SST );

			xRxPacket.dlc = 0;
			xRxPacket.rtr = 0;
			xRxPacket.id = 0;
			xRxPacket.dataHigh = xRxPacket.dataLow = 0;
			uxRet = xQueueReceive( xQueue, &xRxPacket, CAN_BASIC_RECEIVE_TIMEOUT );
			if( uxM % 2 != 0 )
			{
				if( errQUEUE_EMPTY != uxRet )
				{
					DebugMsg( "buffer(%d) filter id test failed\r\n", ucBufNum );
					return false;
				}
			}
			else
			{
				if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 0, ucLittleEnd ) )
				{
					DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxM );
					return false;
				}
			}
			vTaskDelay( CAN_TEST_INTERVAL );
		}
		DebugMsg( "test(%d) success.\r\n", uxN );
	}
	return true;
}

static INT32
prvCanbusBasicFilterDataTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd )
{
	CAN_MSGOBJECT xRxPacket, xTxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxRet = 0;
	UINT32 uxN, uxM;

	DebugMsg( "start filter data test >>>>>>>>\r\n" );
	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0;

	for( uxN = 0; uxN < CAN_BASIC_FILTER_TEST_NUM; uxN++ )
	{
		xFilter.amcr_d.code = rand_ul() % 0x10000;

		xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

		for( uxM = 0; uxM < 10; uxM++ )
		{
			xTxPacket.dlc = rand_ul() % 9;
			if( xTxPacket.dlc < 2 )
				xTxPacket.dlc = 2;
			xTxPacket.rtr = 0;
			xTxPacket.ide = 0;
			xTxPacket.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
			if( xTxPacket.id > CAN_BUS_TEST_NORMAL_ID_MAX )
				xTxPacket.ide = 1;
			xTxPacket.dataLow = rand_ul();
			if( 1 == ucLittleEnd )
				xTxPacket.dataHigh = ( ( xFilter.amcr_d.code >> 8 ) & 0xff ) |
						( ( xFilter.amcr_d.code<<8 ) & 0xff00 ) | ( rand_ul() << 16 );
			else
				xTxPacket.dataHigh = ( xFilter.amcr_d.code<<16 ) | ( rand_ul() & 0x0000ffff );
			if( 0 != uxM % 2 )
				xTxPacket.dataHigh = ~xTxPacket.dataHigh;
			xCanSend( ucTxPort, &xTxPacket, ucBufNum, CAN_SEND_SST );

			xRxPacket.dlc = 0;
			xRxPacket.rtr = 0;
			xRxPacket.id = 0;
			xRxPacket.dataHigh = xRxPacket.dataLow = 0;
			uxRet = xQueueReceive( xQueue, &xRxPacket, CAN_BASIC_RECEIVE_TIMEOUT );
			if( uxM%2 != 0 )
			{
				if( errQUEUE_EMPTY != uxRet )
				{
					DebugMsg( "buffer(%d) test (%d) filter data test failed\r\n", ucBufNum, uxM );
					return false;
				}
			}
			else
			{
				if( false == prvCanDataCheck( &xRxPacket, &xTxPacket, 0, ucLittleEnd ) )
				{
					DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxM );
					return false;
				}
			}
			vTaskDelay( CAN_TEST_INTERVAL );
		}
		DebugMsg( "test(%d) success.\r\n", uxN );
	}
	return true;
}

INT32
xCanbusBasicFilterTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd )
{
	DebugMsg( "start filter test\r\n");
	if( false == prvCanbusBasicFilterIdeTest( ucRxPort, xQueue, ucTxPort, ucBufNum, ucLittleEnd ) )
	{
		DebugMsg( "filter ide failed\r\n");
		return false;
	}
	if( false == prvCanbusBasicFilterRTRTest( ucRxPort, xQueue, ucTxPort, ucBufNum, ucLittleEnd ) )
	{
		DebugMsg( "filter rtr failed\r\n");
		return false;
	}
	if( false == prvCanbusBasicFilterIdTest( ucRxPort, xQueue, ucTxPort, ucBufNum, ucLittleEnd ) )
	{
		DebugMsg( "filter id test failed\r\n");
		return false;
	}
	if( false == prvCanbusBasicFilterDataTest( ucRxPort, xQueue, ucTxPort, ucBufNum, ucLittleEnd ) )
	{
		DebugMsg( "filter data test failed\r\n");
		return false;
	}
	return true;
}

INT32
xCanbusBasicDataTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd )
{
	UINT32 uxN, uxM;
	CAN_MSGOBJECT xRxPacket[ 32 ], xTxPacket[ 32 ];
	CAN_FILTEROBJECT xFilter;
	UINT8 ucIde;
	UINT32 uxDlc;
	int ret;

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xCanReceiveQueueReConfig( ucRxPort, &xFilter, ucBufNum );

	DebugMsg( "start data test >>>>>>>\r\n" );
	for( uxN = 0; uxN < CAN_BASIC_TEST_NUM; uxN++ )
	{
		for( ucIde = 0; ucIde < 2; ucIde++ )
		{
			for( uxDlc = 0; uxDlc < 9; uxDlc++ )
			{
				DebugMsg( "ide is  %d\r\n", ucIde );
				DebugMsg( "data length is  %d, msg box is %d\r\n", uxDlc,ucBufNum);
				if( ucBufNum < 31 - g_RxBasicCanMb )
				{
					xTxPacket[ ucBufNum ].ide = ucIde;
					xTxPacket[ ucBufNum ].dlc = uxDlc;
					if( 0 == xTxPacket[ ucBufNum ].ide )
						xTxPacket[ ucBufNum ].id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX;
					else
						xTxPacket[ ucBufNum ].id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
					xTxPacket[ ucBufNum ].dataHigh = rand_ul();
					xTxPacket[ ucBufNum ].dataLow = rand_ul();
					xTxPacket[ ucBufNum ].rtr = 0;
					ret = xCanSend( ucTxPort, &xTxPacket[ ucBufNum ], ucBufNum, CAN_SEND_NORMAL );
					if( ret != CAN_OK )
						DebugMsg("send fail, return status is %d\r\n", ret );

					xRxPacket[ ucBufNum ].dlc = 0;
					xRxPacket[ ucBufNum ].rtr = 0;
					xRxPacket[ ucBufNum ].id = 0;
					xRxPacket[ ucBufNum ].dataHigh = xRxPacket[ ucBufNum ].dataLow = 0;
					xQueueReceive( xQueue, &xRxPacket[ ucBufNum ], CAN_BASIC_RECEIVE_TIMEOUT );
					if( false == prvCanDataCheck( &xRxPacket[ ucBufNum ], &xTxPacket[ ucBufNum ], 0, ucLittleEnd ) )
					{
						DebugMsg( "buffer (%d) , test (%d) data check error:\r\n", ucBufNum, uxN );
						return false;
					}
					vTaskDelay( CAN_TEST_INTERVAL );
				}
				else
				{
					xCanStop( ucTxPort );
					for( uxM = 32 - g_RxBasicCanMb; uxM < 32; uxM++ )
					{
						if( 0 == ucIde )
							xTxPacket[ uxM ].id = rand_ul() % CAN_BUS_TEST_NORMAL_ID_MAX;
						else
							xTxPacket[ uxM ].id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
						xTxPacket[ uxM ].ide = ucIde;
						xTxPacket[ uxM ].dlc = uxDlc;
						xTxPacket[ uxM ].dataHigh = rand_ul();
						xTxPacket[ uxM ].dataLow = rand_ul();
						xTxPacket[ uxM ].rtr = 0;
						xCanSend( ucTxPort, &xTxPacket[ uxM ], ucBufNum, CAN_SEND_NORMAL );
					}
					xCanStart( ucTxPort, CANOP_MODE_NORMAL );
					if( CAN_PORT0 == ucRxPort )
					{
						NVIC_DisableIRQ( INT_CBUS0_IRQn );
						vTaskDelay( 5 );
						NVIC_EnableIRQ( INT_CBUS0_IRQn );
					}
					else
					{
						NVIC_DisableIRQ( INT_CBUS1_IRQn );
						vTaskDelay( 5 );
						NVIC_EnableIRQ( INT_CBUS1_IRQn );
					}

					for( uxM = 32 - g_RxBasicCanMb; uxM < 32; uxM++ )
					{
						xRxPacket[ uxM ].dlc = 0;
						xRxPacket[ uxM ].rtr = 0;
						xRxPacket[ uxM ].id = 0;
						xRxPacket[ uxM ].dataHigh = xRxPacket[ uxM ].dataLow = 0;
						xQueueReceive( xQueue, &xRxPacket[ uxM ], CAN_BASIC_RECEIVE_TIMEOUT );
					}
					for( uxM = 32 - g_RxBasicCanMb; uxM < 32; uxM++ )
					{
						if( false == prvCanDataCheck( &xRxPacket[ uxM ], &xTxPacket[ uxM ], 0, ucLittleEnd ) )
						{
							DebugMsg( "buffer (%d) , test (%d) data check error.\r\n", ucBufNum, uxM );
							return false;
						}
					}
					vTaskDelay( CAN_TEST_INTERVAL );
				}
			}
		}
		DebugMsg( "test(%d) success.\r\n", uxN );
	}
	return true;
}
