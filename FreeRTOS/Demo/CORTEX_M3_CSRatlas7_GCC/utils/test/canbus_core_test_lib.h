/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CAN_BUS_CORE_LIB_TEST_H
#define CAN_BUS_CORE_LIB_TEST_H
#include "canbus.h"

#define CAN_TEST_INTERVAL		1

/* can basic test */
#define CAN_BASIC_TEST_NUM		5
#define CAN_BASIC_RECEIVE_TIMEOUT	10

#define CAN_BASIC_FILTER_TEST_NUM	10

/* can sst test */
#define CAN_SST_TEST_NUM		1000

/* can error detect test */
#define CAN_ERROR_DETECT_TEST_NUM	10
#define CAN_ERROR_TEST_TIMEOUT		10


#define CAN_ERROR_TEST_ARB_BUF_NUM	g_can_error_test_buf_num
#define CAN_ERROR_TEST_SST_BUF_NUM	g_can_error_test_buf_num
#define CAN_ERROR_TEST_OVER_LOAD_BUF_NUM	g_can_error_test_buf_num

#define CAN_ERROR_TEST_REC_BUF_NUM	10


#define CAN_ID_FOR_OVER_LOAD		0x5


#define CAN_BUS_TEST_NORMAL_ID_MAX      0x800
#define CAN_BUS_TEST_EXT_ID_MAX         0x20000000


INT32 xCanbusBasicRTRTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum );
INT32 xCanbusBasicFilterTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd );
INT32 xCanbusBasicDataTest( UINT8 ucRxPort,
			xQueueHandle xQueue,
			UINT8 ucTxPort,
			UINT8 ucBufNum, UINT8 ucLittleEnd );

extern UINT8 g_can_error_test_buf_num;
#endif
