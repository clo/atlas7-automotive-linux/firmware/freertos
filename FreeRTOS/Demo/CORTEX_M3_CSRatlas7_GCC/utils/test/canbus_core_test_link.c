/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "canbus.h"
#include "canbus_core_test_lib.h"
#include "canbus_core_test_link.h"

static xTaskHandle xCanSWLinkTask = NULL;
extern INT32 g_RxBasicCanMb;
/*-----------------------------------------------------------*/

static INT32
prvcanbusLinkCfgTest( UINT8 ucRxPort,
			xQueueHandle queue,
			UINT8 ucTxPort,
			UINT8 ucBufNum )
{
	UINT32 uxN, uxM, uxCfg;

	UINT32 uxCan0Clk[] = { CAN_SPEED_40M_125K, CAN_SPEED_40M_1M };

	UINT32 uxCan1Clk[] = { CAN_SPEED_48M_125K, CAN_SPEED_48M_1M };
	char * pcBaud[] = { "125K", "1M" };


	for( uxN = 0; uxN < sizeof( uxCan0Clk )/ sizeof( INT32 ); uxN++ )
	{
		for( uxM = 0; uxM < 2; uxM++ )
		{
			if( 0 == uxM )
				uxCfg = CAN_BIG_ENDIAN;
			else
				uxCfg = CAN_LITTLE_ENDIAN;

			DebugMsg( "buffer(%d) re-configure core configure:\r\n", ucBufNum );
			DebugMsg( "baudrate is %s\r\n", pcBaud[ uxN ] );
			DebugMsg( "work mode is %s\r\n", ( ( 0 == uxM ) ? "Big":"Little" ) );
			if( configCPU_CLOCK_HZ == 192000000 )
				xCanReConfigCoreInRunning( CAN_PORT0, uxCfg | uxCan1Clk[ uxN ] | CAN_AUTO_RESTART );
			else
				xCanReConfigCoreInRunning( CAN_PORT0, uxCfg | uxCan0Clk[ uxN ] | CAN_AUTO_RESTART );
			xCanReConfigCoreInRunning( CAN_PORT1, uxCfg | uxCan1Clk[ uxN ] | CAN_AUTO_RESTART );

			if( false == xCanbusBasicDataTest( ucRxPort, queue, ucTxPort, ucBufNum, uxM ) )
			{
				DebugMsg( "data test failed\r\n" );
				return false;
			}
			if( false == xCanbusBasicRTRTest( ucRxPort, queue, ucTxPort, ucBufNum ) )
			{
				DebugMsg( "rtr test failed\r\n" );
				return false;
			}
			if( false == xCanbusBasicFilterTest( ucRxPort, queue, ucTxPort, ucBufNum, uxM ) )
			{
				DebugMsg( "filter test failed\r\n" );
				return false;
			}
		}
	}
	return true;
}

static INT32 prvCanbusLinkBufferTest( UINT8 ucRxPort, UINT8 ucTxPort )
{
	CAN_FILTEROBJECT xFilter;
	xQueueHandle xQueue = NULL;

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;
	xQueue = xCanReceiveQueueCreate( ucRxPort, &xFilter, 31, 32 );

	if( NULL == xQueue )
	{
		DebugMsg( "create queue failed.\r\n" );
		return false;
	}
	if( false == prvcanbusLinkCfgTest( ucRxPort, xQueue, ucTxPort, 31 ) )
		return false;
	return true;
}


static void prvCanbusLinkTest( void *pvParameters )
{
	for( ; ; )
	{
		DebugMsg( "can bus basic link test start, can0 is rx, can1 is tx\r\n" );
		if( false == prvCanbusLinkBufferTest( CAN_PORT0, CAN_PORT1 ) )
			break;
		DebugMsg( "can bus basic link test start, can1 is rx, can0 is tx\r\n" );
		if( false == prvCanbusLinkBufferTest( CAN_PORT1, CAN_PORT0 ) )
			break;
		DebugMsg( "can bus basic link test success.\r\n" );
		vTaskDelete(NULL);
	}
	DebugMsg( "can bus basic link test failed\r\n" );
	vTaskDelete(NULL);
}

void vCanbusCoreTestLinkStart( UINT32 uxBasicCanNum )
{
	g_RxBasicCanMb = uxBasicCanNum;

	xCanInit( CAN_PORT0, CAN_SPEED_48M_125K | CAN_AUTO_RESTART | CAN_BIG_ENDIAN,
				(PCAN_CONFIG_REG)0, uxBasicCanNum, uxBasicCanNum );

	xCanInit( CAN_PORT1, CAN_SPEED_48M_125K | CAN_AUTO_RESTART | CAN_BIG_ENDIAN,
				(PCAN_CONFIG_REG)0, uxBasicCanNum, uxBasicCanNum );

	xCanStart( CAN_PORT0, CANOP_MODE_NORMAL );

	xCanStart( CAN_PORT1, CANOP_MODE_NORMAL );

	if( !xCanSWLinkTask )
		xTaskCreate( prvCanbusLinkTest, "CBLT", configMINIMAL_STACK_SIZE + 512,
			NULL, tskIDLE_PRIORITY + 1, &xCanSWLinkTask );
}


void vCanbusCoreTestLinkStop( void )
{
	if( xCanSWLinkTask )
	{
		vTaskDelete( xCanSWLinkTask );
		xCanSWLinkTask = NULL;
	}

	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );
}
