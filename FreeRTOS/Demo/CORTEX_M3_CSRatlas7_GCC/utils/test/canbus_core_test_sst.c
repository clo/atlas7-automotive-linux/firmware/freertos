/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"

#include "canbus.h"
#include "canbus_core_test_lib.h"
#include "canbus_core_test_sst.h"

static xTaskHandle s_xCanSSTTask = NULL;

static INT32 prvCanbusSstBufferTest( UINT8 ucPort )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT  xTxPacket;
	UINT8 ucBufNum;
	UINT32 uxN, uxRet;

	for( ucBufNum=0; ucBufNum < 32; ucBufNum++ )
	{
		DebugMsg( "buffer(%d) test start.\r\n", ucBufNum );
		for( uxN = 0; uxN < CAN_SST_TEST_NUM; uxN++ )
		{
			xTxPacket.dlc = rand_ul() % 9;
			xTxPacket.rtr = 0;
			xTxPacket.ide = 0;
			xTxPacket.dataHigh = rand_ul();
			xTxPacket.dataLow = rand_ul();
			xTxPacket.id = rand_ul() % CAN_BUS_TEST_EXT_ID_MAX;
			if( xTxPacket.id > CAN_BUS_TEST_NORMAL_ID_MAX )
				xTxPacket.id = 1;

			uxRet = xCanSend( ucPort, &xTxPacket, ucBufNum, CAN_SEND_SST );
			if( CANmod3_OK != uxRet )
			{
				DebugMsg("send failed, uxRet is 0x%x \r\n", uxRet );
				return false;
			}
			DebugMsg( "test(%d) success.\r\n", uxN );
			vTaskDelayUntil( &xLastWakeTime, 1 );
		}
	}
	return true;
}

static void prvCanbusSstTest( void *pvParameters )
{
	for( ; ; )
	{
		DebugMsg( "can bus 0 single shot test start.\r\n" );
		xCanStop( CAN_PORT1 );
		xCanStart( CAN_PORT0, CANOP_MODE_NORMAL );
		if( false == prvCanbusSstBufferTest( CAN_PORT0 ) )
			break;
		DebugMsg( "can bus 1 single shot test start.\r\n" );
		xCanStop( CAN_PORT0 );
		xCanStart( CAN_PORT1, CANOP_MODE_NORMAL );
		if( false == prvCanbusSstBufferTest( CAN_PORT1 ) )
			break;
		DebugMsg( "can bus single shot test success.\r\n" );
		vTaskDelete(NULL);
	}
	DebugMsg( "can bus single shot test failed\r\n" );
	vTaskDelete(NULL);
}

void vCanbusCoreTestSstStart( void )
{
	if( !s_xCanSSTTask )
		xTaskCreate( prvCanbusSstTest, "CSSTT", configMINIMAL_STACK_SIZE + 128,
			0, tskIDLE_PRIORITY + 1, &s_xCanSSTTask );

}

void vCanbusCoreTestSstStop( void )
{
	if( s_xCanSSTTask )
	{
		vTaskDelete( s_xCanSSTTask );
		s_xCanSSTTask = NULL;
	}

	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );

}
