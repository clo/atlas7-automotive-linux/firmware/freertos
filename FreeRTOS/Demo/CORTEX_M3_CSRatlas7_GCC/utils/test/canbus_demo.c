/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "soc_pwrc.h"
#include "soc_iobridge.h"

#include "CANmod3.h"
#include "canbus.h"

#include "misclib.h"

#include "canbus_test_lib.h"
#include "canbus_demo_lib.h"

static portBASE_TYPE prvCanBusDemoInitCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength;
	uint32_t uxEn = 0;
	uint32_t can0_clk = 0;
	uint32_t can1_clk = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		can0_clk = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}

	if( NULL != pcParameter3 )
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		can1_clk = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	}

	if( eCanTestStart == uxEn )
	{
		DebugMsg( "can init, can0 clock is %s, can1 clock is %s\r\n",
				( CAN_CLK_125k == can0_clk? "125K" : "1M" ),
				( CAN_CLK_125k == can1_clk? "125K" : "1M" ) );

		if( configCPU_CLOCK_HZ == 192000000 )
			vCanbusTestInit( ( CAN_CLK_125k == can0_clk ? CAN_SPEED_48M_125K : CAN_SPEED_48M_1M ),
					( CAN_CLK_125k == can1_clk ? CAN_SPEED_48M_125K : CAN_SPEED_48M_1M ) );
		else
			vCanbusTestInit( ( CAN_CLK_125k == can0_clk ? CAN_SPEED_40M_125K : CAN_SPEED_40M_1M ),
					( CAN_CLK_125k == can1_clk ? CAN_SPEED_48M_125K : CAN_SPEED_48M_1M ) );
		vCanbusDemoStart(CAN_PORT0);
		vCanbusDemoStart(CAN_PORT1);
	}
	else
	{
		DebugMsg( "can deinit.\r\n" );
		vCanbusDemoStop();
	}
	return 0;
}

const CLI_Command_Definition_t xCanbusDemoInitParameter =
{
	"can_demo",
	"\r\ncan_demo <EN> <can0 clk> <can1 clk>\r\n \
	EN: \r\n \
		0 - stop \r\n \
		1 - start \r\n \
	can0 clk: \r\n \
		0 - 125K\r\n \
		1 - 1M \r\n \
	can1 clk: \r\n \
		0 - 125K\r\n \
		1 - 1M \r\n",
	prvCanBusDemoInitCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_DemoInit_CLI_Parameter = &xCanbusDemoInitParameter;
