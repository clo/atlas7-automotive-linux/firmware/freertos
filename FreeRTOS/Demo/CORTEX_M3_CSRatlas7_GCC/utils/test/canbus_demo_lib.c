/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"
#include "soc_pwrc_retain.h"
#include "soc_ipc.h"
#include "io.h"
#include "pm.h"

#include "canbus.h"
#include "canbus_test_lib.h"
#include "canbus_demo_lib.h"


static xTaskHandle s_xCan0ReceiveTask = NULL;
static xTaskHandle s_xCan1ReceiveTask = NULL;
#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
static xTaskHandle s_xVirtCan0ReceiveTask = NULL;
static xTaskHandle s_xVirtCan1ReceiveTask = NULL;
#endif

/*-----------------------------------------------------------*/
void vCanbusDemoStart( uint8_t ucPort )
{
	if( CAN_PORT0 == ucPort)
	{
		if( !s_xCan0ReceiveTask )
			xTaskCreate( vCanbusReceiveFunction, "CAN0RX", configMINIMAL_STACK_SIZE + 128,
				( void* )CAN_PORT0, tskIDLE_PRIORITY + 4, &s_xCan0ReceiveTask );
		else
			DebugMsg( "CAN 0 receiver task handle is not NULL\r\n" );

#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
		if( !s_xVirtCan0ReceiveTask )
			xTaskCreate( vVirtCanbusReceiveFunction, "VIRTCAN0RX", configMINIMAL_STACK_SIZE + 128,
				( void* )CAN_PORT0, tskIDLE_PRIORITY + 1, &s_xVirtCan0ReceiveTask );
		else
			DebugMsg( "CAN 0 receiver task handle is not NULL\r\n" );
#endif
	}
	else
	{	/* when this routine is called from startup task, A7 is ready */
		if( CANBUS_TRIGGER_REARVIEW_MAGIC == MEM( PM_ARG_CAN_TRIGGER_REARVIEW ) )
		{
			DebugMsg( "CANbus triger rearview\r\n" );
			vCanbusTriggerRearview( 1 );
		}

		if( !s_xCan1ReceiveTask )
			xTaskCreate( vCanbusReceiveFunction, "CAN1RX", configMINIMAL_STACK_SIZE + 128,
				( void* )CAN_PORT1, tskIDLE_PRIORITY + 1, &s_xCan1ReceiveTask );
		else
			DebugMsg( "CAN 1 receiver task handle is not NULL\r\n" );
#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
		if( !s_xVirtCan1ReceiveTask )
			xTaskCreate( vVirtCanbusReceiveFunction, "VIRTCAN1RX", configMINIMAL_STACK_SIZE + 128,
				( void* )CAN_PORT1, tskIDLE_PRIORITY + 1, &s_xVirtCan1ReceiveTask );
		else
			DebugMsg( "CAN 1 receiver task handle is not NULL\r\n" );
#endif
	}
}

void vCanbusDemoStop( void )
{
	if( s_xCan0ReceiveTask )
	{
		vTaskDelete( s_xCan0ReceiveTask );
		s_xCan0ReceiveTask = NULL;
	}
#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
	if( s_xVirtCan0ReceiveTask )
	{
		vTaskDelete( s_xVirtCan0ReceiveTask );
		s_xVirtCan0ReceiveTask = NULL;
	}
#endif

	if( s_xCan1ReceiveTask )
	{
		vTaskDelete( s_xCan1ReceiveTask );
		s_xCan1ReceiveTask = NULL;
	}
#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
	if( s_xVirtCan1ReceiveTask )
	{
		vTaskDelete( s_xVirtCan1ReceiveTask );
		s_xVirtCan1ReceiveTask = NULL;
	}
#endif
	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );
}
