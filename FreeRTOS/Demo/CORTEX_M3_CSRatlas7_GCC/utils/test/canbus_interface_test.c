/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "soc_iobridge.h"

#include "CANmod3.h"
#include "canbus.h"

#include "misclib.h"
#include "canbus_test_lib.h"
#include "canbus_interface_test_lib.h"

static portBASE_TYPE prvCanBusHFTestInitCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxEn = 0;
	uint32_t uxClk = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxClk = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}

	DebugMsg( "can hf test %s, clock is %s\r\n",
			( 0 == uxEn ? "stop" : "start" ),
			( 0 == uxClk ? "125K" : "1M" ) );

	if( eCanTestStart == uxEn )
	{
		switch( uxClk )
		{
		case CAN_CLK_125k:
			if( configCPU_CLOCK_HZ == 192000000 )
				vCanbusTestInit( CAN_SPEED_48M_125K, CAN_SPEED_48M_125K );
			else
				vCanbusTestInit( CAN_SPEED_40M_125K, CAN_SPEED_48M_125K );
			break;
		case CAN_CLK_1M:
			if( configCPU_CLOCK_HZ == 192000000 )
				vCanbusTestInit( CAN_SPEED_48M_1M, CAN_SPEED_48M_1M );
			else
				vCanbusTestInit( CAN_SPEED_40M_1M, CAN_SPEED_48M_1M );
			break;
		default:
			break;
		}

		vCanbusInterfaceTestStart();
	}
	else
		vCanbusInterfaceTestStop();
	return 0;
}

static portBASE_TYPE prvCanBusRobusTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3, *pcParameter4;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength, xParameter4StringLength;
	uint32_t uxEn, uxPort, uxCycle, uxRxBaiscBum;

	uxEn = uxPort = uxCycle = uxRxBaiscBum = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	pcParameter4 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameter4StringLength );
	if( NULL == pcParameter1 )
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxPort = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}

	if( NULL != pcParameter3)
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		uxCycle = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	}

	if(NULL != pcParameter4)
	{
		pcParameter4[ xParameter4StringLength ] = 0x00;
		uxRxBaiscBum = (uint32_t)strtoul(pcParameter4, NULL, 0);
	}

	if( uxPort > CAN_PORT1)
		uxPort = CAN_PORT1;

	DebugMsg( "En is %d, port is %d, uxCycle is %s, rx basic can number is %d\r\n",
			uxEn, uxPort, ( CAN_CLK_125k == uxCycle ? "125K" : "1M" ),uxRxBaiscBum );

	if( 1 == uxEn )
		vCanbusInitialize( uxPort, ( CAN_CLK_125k == uxCycle ? CAN_SPEED_48M_125K : CAN_SPEED_48M_1M) );
	else
	{
		xCanStop( uxPort );
		xCanDeinit( uxPort );
	}

	return 0;
}

static portBASE_TYPE prvCanBusRetranTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxEn, uxId;

	uxEn = uxId = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	if( NULL == pcParameter1 )
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxId = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}
	DebugMsg( "uxEn is %d, uxId is %d\r\n", uxEn, uxId );

	if( 0 == uxEn )
	{
		if( 0 == uxId )
			vCanbusInterfaceTestEnd( CAN_TASK_INDEX_ROTRANS, CAN_PORT0 );
		else
			vCanbusInterfaceTestEnd( CAN_TASK_INDEX_ROTRANS, CAN_PORT1 );
	}
	else
	{
		if( 0 == uxId )
			vCanbusInterfaceRetransStart( CAN_PORT0 );
		else
			vCanbusInterfaceRetransStart( CAN_PORT1 );
	}
	return 0;
}

static portBASE_TYPE prvCanBusArbTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength;
	uint32_t uxEn, uxCycle, uxExtId;

	uxEn = uxCycle = uxExtId = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	if( NULL == pcParameter1 )
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxEn = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		uxCycle = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}
	if( NULL != pcParameter3 )
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		uxExtId = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	}
	DebugMsg( "uxEn is %d, uxCycle is %d, %s ID mode\r\n", uxEn, uxCycle, 1 == uxExtId ? "extand" : "normal" );
	DebugMsg( "uxId is %d\r\n", uxExtId );

	if( 0 == uxCycle )
		uxCycle = 0xfffffff;

	if( 0 == uxEn )
	{
		vCanbusInterfaceTestEnd( CAN_TASK_INDEX_ARB, 0 );
	}
	else
	{
		vCanbusInterfaceArbStart( uxCycle, uxExtId );
	}
	return 0;
}


const CLI_Command_Definition_t xCanbusHFTestInitParameter =
{
	"can_hf_init",
	"\r\ncan_hf_init <uxEn> <uxClk>\r\n \
	uxEn:	0 - stop \r\n \
		1 - start \r\n \
	uxClk:	0 - 125K;\r\n \
		1 - 1M \r\n",
	prvCanBusHFTestInitCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_HFTestInit_CLI_Parameter = &xCanbusHFTestInitParameter;

static const CLI_Command_Definition_t xCanbusRobusTestParameter =
{
	"can_robus",
	"\r\ncan_robus <EN> <ID> <Cycles>\r\n \
	EN:1 - start; 0 - end\r\n \
	ID:0 - can0 is tx; 1 - can1 is tx\r\n \
	uxCycle: 0 - infinite\r\n",
	prvCanBusRobusTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_RobusTest_CLI_Parameter = &xCanbusRobusTestParameter;

static const CLI_Command_Definition_t xCanbusRetranTestParameter =
{
	"can_retran",
	"\r\ncan_retran <EN> <ID>\r\n \
	EN:1 - start; 0 - end\r\n \
	ID:0 - can0 is tx; 1 - can1 is tx\r\n",
	prvCanBusRetranTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_RetanTest_CLI_Parameter = &xCanbusRetranTestParameter;

static const CLI_Command_Definition_t xCanbusArbTestParameter =
{
	"can_arb",
	"\r\ncan_arb <EN> <Cycles> <ect_id>\r\n \
	EN:1 - start; 0 - end\r\n \
	uxCycle: 0 - infinite\r\n \
	uxExtId: 1 - extend uxId mode\r\n",
	prvCanBusArbTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_ArbTest_CLI_Parameter = &xCanbusArbTestParameter;
