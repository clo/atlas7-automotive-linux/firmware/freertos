/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"

#include "canbus.h"
#include "canbus_interface_test_lib.h"
#include "canbus_test_lib.h"

static xQueueHandle g_QueueArb;

static xTaskHandle s_xCan0ReceiveTask = NULL;
static xTaskHandle s_xCan1ReceiveTask = NULL;
static xTaskHandle s_xCan0RetransSendTask = NULL;
static xTaskHandle s_xCan0RetransReceiveTask = NULL;
static xTaskHandle s_xCan1RetransSendTask = NULL;
static xTaskHandle s_xCan1RetransReceiveTask = NULL;
static xTaskHandle s_xCanArbTask = NULL;

#define CAN_NORMAL_LOW_ARB		0x3ff
#define CAN_EXT_LOW_ARB			0x1fffffff
/*-----------------------------------------------------------*/
static void prvCanbusInterfaceArbTestFunction( void *pvParameters )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xPacket0, xPacket1;
	UINT32 uxRet;
	UINT32 uxRecData = 0;
	UINT32 uxCan0Id, uxCan1Id;
	UINT32 uxExtId = ( UINT32 )pvParameters;

	if( 0 != uxExtId )
		DebugMsg( "it is extended id uxMode. \r\n" );

	for( ;; )
	{
		if(0 == uxExtId )
		{
			xPacket0.ide = 0;
			xPacket0.rtr = 0;
			xPacket1.ide = 0;
			xPacket1.rtr = 0;
			uxCan0Id = CAN_NORMAL_LOW_ARB - 1;
			uxCan1Id = CAN_NORMAL_LOW_ARB;
		}
		else
		{
			xPacket0.ide = 1;
			xPacket0.rtr = 0;
			xPacket1.ide = 1;
			xPacket1.rtr = 0;
			uxCan0Id = CAN_EXT_LOW_ARB - 1;
			uxCan1Id = CAN_EXT_LOW_ARB;
		}
		xQueueReceive( g_QueueArb, &uxRecData, portMAX_DELAY );

		do
		{
#if 0
			if(0 == uxExtId )
				uxCan1Id = CAN_NORMAL_LOW_ARB;
			else
				uxCan1Id = CAN_EXT_LOW_ARB;
#endif
			xPacket0.id = uxCan0Id;
			while( uxCan1Id > uxCan0Id )
			{
				xPacket1.id = uxCan1Id;
				xPacket0.dlc = rand_ul() % 9;
				xPacket0.dataHigh = rand_ul();
				xPacket0.dataLow = rand_ul();
				xPacket1.dlc = rand_ul() % 9;
				xPacket1.dataHigh = rand_ul();
				xPacket1.dataLow = rand_ul();
				uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ARB_TEST_BUF_NUM,
					CAN_SEND_NORMAL );
				if ( CANmod3_OK != uxRet )
					DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );
				vTaskDelayUntil( &xLastWakeTime, 3 );
				uxCan1Id--;
			}
			uxCan0Id--;
		} while( uxCan0Id != 0 );
		if(0 == uxExtId )
		{
			xPacket0.ide = 0;
			xPacket1.ide = 0;
			uxCan0Id = CAN_NORMAL_LOW_ARB - 1;
			uxCan1Id = CAN_NORMAL_LOW_ARB;
		}
		else
		{
			xPacket0.ide = 1;
			xPacket1.ide = 1;
			uxCan0Id = CAN_EXT_LOW_ARB - 1;
			uxCan1Id = CAN_EXT_LOW_ARB;
		}
		do
		{
			xPacket1.id = uxCan1Id;

#if 0
			if(0 == uxExtId )
				uxCan0Id = CAN_NORMAL_LOW_ARB;
			else
				uxCan0Id = CAN_EXT_LOW_ARB;
#endif
			while( uxCan0Id > uxCan1Id )
			{
				xPacket0.id = uxCan0Id;
				xPacket0.dlc = rand_ul() % 9;
				xPacket0.dataHigh = rand_ul();
				xPacket0.dataLow = rand_ul();
				xPacket1.dlc = rand_ul() % 9;
				xPacket1.dataHigh = rand_ul();
				xPacket1.dataLow = rand_ul();

				uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ARB_TEST_BUF_NUM, CAN_SEND_NORMAL );
				if ( CANmod3_OK != uxRet )
				uxRet = xCanArbTestFunction( &xPacket0, &xPacket1, CAN_ARB_TEST_BUF_NUM, CAN_SEND_NORMAL );
				if ( CANmod3_OK != uxRet )
					DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );
				vTaskDelayUntil( &xLastWakeTime, 3 );
				uxCan0Id--;
			}
			uxCan1Id--;
		}while( uxCan1Id != 0 );
	}
}

void vCanbusInterfaceArbStart( UINT32 uxCycle, UINT32 uxExtId )
{
	UINT32 uxN;
	UINT32 uxSendData = 1;

	DebugMsg( "vCanbusInterfaceArbStart uxCycles is %d\r\n", uxCycle );
	DebugMsg( "uxExtId is %d.\r\n", uxExtId );

	if( NULL == s_xCanArbTask )
		xTaskCreate( prvCanbusInterfaceArbTestFunction, "CIATF", configMINIMAL_STACK_SIZE + 256,
			(void *)uxExtId, tskIDLE_PRIORITY + 1, &s_xCanArbTask );

	for( uxN = 0; uxN < uxCycle; uxN++ )
		xQueueSend( g_QueueArb, &uxSendData, portMAX_DELAY );
}

/*
* uxRetrans test
*/
static void prvCanbusInterfaceRetransReceiverFunction( void* pvPort )
{
	CAN_MSGOBJECT xRxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxPort = ( UINT32 )pvPort;

	xFilter.amr.l = 0xffffffff;
	xFilter.amr.normal_id = 0;
	xFilter.acr.normal_id = CAN_ID_FOR_ROTRANS_TEST;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xRxPacket.dataHigh = xRxPacket.dataLow = 0;
	xRxPacket.ide = 0;
	xRxPacket.rtr = 0;
	xRxPacket.id = 0;
	xRxPacket.dlc = 0;
	xQueueHandle queue = xCanReceiveQueueCreate( uxPort, &xFilter, CAN_RETRANS_TEST_RX_BUF_NUM, 32 );
	if( queue == NULL )
	{
		DebugMsg( "uxRetrans test create queue failed.\r\n" );
		vTaskDelete(NULL);
	}
	xCanStopReceiveQueue( uxPort, CAN_RETRANS_TEST_RX_BUF_NUM );
	for( ;; )
	{
		xQueueReceive(queue, &xRxPacket, portMAX_DELAY );
		DebugMsg( "uxRetrans test can(%d ) receive id(0x%x), dlc(%d ), data is 0x%x, 0x%x\r\n",
			uxPort, xRxPacket.id, xRxPacket.dlc,
			xRxPacket.dataHigh, xRxPacket.dataLow );
	}
}

static void prvCanbusInterfaceRetransSendTaskFunction( void* pvPort )
{
	portTickType xLastWakeTime = xTaskGetTickCount();
	CAN_MSGOBJECT xTxPacket;
	UINT32 uxRet;
	UINT32 uxPort = ( UINT32 )pvPort;

	xTxPacket.ide = 0;
	xTxPacket.id = CAN_ID_FOR_ROTRANS_TEST;
	xTxPacket.rtr = 1;
	xTxPacket.dlc = 8;
	xTxPacket.dataHigh = 0x00000000;
	xTxPacket.dataLow = 0x00000000;
	for( ;; )
	{
		vTaskDelayUntil( &xLastWakeTime, 10 );
		uxRet = xCanSend( uxPort, &xTxPacket, CAN_RETRANS_TEST_TX_BUF_NUM, CAN_SEND_NORMAL );
		if ( CANmod3_OK != uxRet )
			DebugMsg( "send failed, uxRet is 0x%x \r\n", uxRet );
	}
}

void vCanbusInterfaceRetransStart( UINT32 uxPort )
{
	CAN_MSGOBJECT xPacket;
	CAN_FILTEROBJECT xFilter;

	xFilter.amr.l = 0xffffffff;
	xFilter.amr.normal_id = 0;
	xFilter.acr.normal_id = CAN_ID_FOR_ROTRANS_TEST;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xPacket.ide = 0;
	xPacket.id = CAN_ID_FOR_ROTRANS_TEST;
	xPacket.dlc = 8;
	xPacket.dataHigh = 0x01020304;
	xPacket.dataLow = 0x05060708;

	DebugMsg( "vCanbusInterfaceRetransStart send uxPort is %d\r\n", uxPort );

	if( uxPort == CAN_PORT0 )
	{
		xCanReceiveQueueReConfig( CAN_PORT0, &xFilter, CAN_RETRANS_TEST_RX_BUF_NUM );

		xCanRTRMessageNCreate( CAN_PORT1, &xPacket, &xFilter, CAN_RETRANS_TEST_TX_BUF_NUM );

		xTaskCreate( prvCanbusInterfaceRetransSendTaskFunction, "CIRSTF0", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1, &s_xCan0RetransSendTask );
	}
	else
	{
		xCanReceiveQueueReConfig( CAN_PORT1, &xFilter, CAN_RETRANS_TEST_RX_BUF_NUM );

		xCanRTRMessageNCreate( CAN_PORT0, &xPacket, &xFilter, CAN_RETRANS_TEST_TX_BUF_NUM );

		xTaskCreate( prvCanbusInterfaceRetransSendTaskFunction, "CIRSTF1", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1, &s_xCan1RetransSendTask );
	}
}

void vCanbusInterfaceTestEnd( UINT32 uxIndex, UINT32 uxPort )
{
	switch( uxIndex )
	{
		case CAN_TASK_INDEX_ROTRANS:
			if( ( CAN_PORT0 == uxPort ) && ( NULL != s_xCan0RetransSendTask ) )
			{
				vTaskDelete( s_xCan0RetransSendTask );
				s_xCan0RetransSendTask = NULL;
				xCanStopReceiveQueue( CAN_PORT0, CAN_RETRANS_TEST_RX_BUF_NUM );
				xCanRTRMessageNDelete( CAN_PORT1,CAN_RETRANS_TEST_TX_BUF_NUM );
			}
			else if(  NULL != s_xCan1RetransSendTask )
			{
				vTaskDelete( s_xCan1RetransSendTask );
				s_xCan1RetransSendTask = NULL;
				xCanStopReceiveQueue( CAN_PORT1, CAN_RETRANS_TEST_RX_BUF_NUM );
				xCanRTRMessageNDelete( CAN_PORT0, CAN_RETRANS_TEST_TX_BUF_NUM );
			}
			break;
		case CAN_TASK_INDEX_ARB:
			if( NULL != s_xCanArbTask )
			{
				vTaskDelete( s_xCanArbTask );
				s_xCanArbTask = NULL;
			}
			break;
		default:
			break;
	}
}

void vCanbusInterfaceTestStart( void )
{
	if( !g_QueueArb )
	{
		g_QueueArb = xQueueCreate( 4, sizeof( UINT32 ) );
		if ( !g_QueueArb )
		{
			DebugMsg( "ERR: xQueueCreate failed in canbus_hf_test_init!\r\n" );
			return;
		}
	}

	if( !s_xCan0ReceiveTask )
		xTaskCreate( vCanbusReceiveFunction, "CRF0", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1 , &s_xCan0ReceiveTask );

	if( !s_xCan0RetransReceiveTask )
		xTaskCreate( prvCanbusInterfaceRetransReceiverFunction, "CIRRF0", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT0, tskIDLE_PRIORITY + 1 , &s_xCan0RetransReceiveTask );

	if( !s_xCan1ReceiveTask )
		xTaskCreate( vCanbusReceiveFunction, 	"CRF1", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1 , &s_xCan1ReceiveTask );

	if( !s_xCan1RetransReceiveTask )
		xTaskCreate( prvCanbusInterfaceRetransReceiverFunction, "CIRRF1", configMINIMAL_STACK_SIZE + 128,
			( void* )CAN_PORT1, tskIDLE_PRIORITY + 1 , &s_xCan1RetransReceiveTask );
}

void vCanbusInterfaceTestStop( void )
{
	if( !g_QueueArb )
	{
		vQueueDelete( g_QueueArb );
		g_QueueArb = NULL;
	}
	if ( s_xCan0ReceiveTask )
	{
		vTaskDelete( s_xCan0ReceiveTask );
		s_xCan0ReceiveTask = NULL;
	}

	if ( s_xCan1ReceiveTask )
	{
		vTaskDelete( s_xCan1ReceiveTask );
		s_xCan1ReceiveTask = NULL;
	}

	if( s_xCan0RetransSendTask )
	{
		vTaskDelete( s_xCan0RetransSendTask );
		s_xCan0RetransSendTask = NULL;
	}

	if( s_xCan0RetransReceiveTask )
	{
		vTaskDelete( s_xCan0RetransReceiveTask );
		s_xCan0RetransReceiveTask = NULL;
	}

	if( s_xCan1RetransSendTask )
	{
		vTaskDelete( s_xCan1RetransSendTask );
		s_xCan1RetransSendTask = NULL;
	}

	if( s_xCan1RetransReceiveTask )
	{
		vTaskDelete( s_xCan1RetransReceiveTask );
		s_xCan1RetransReceiveTask = NULL;
	}

	if( s_xCanArbTask )
	{
		vTaskDelete( s_xCanArbTask );
		s_xCanArbTask = NULL;
	}

	xCanStop( CAN_PORT0 );
	xCanStop( CAN_PORT1 );

	xCanDeinit( CAN_PORT0 );
	xCanDeinit( CAN_PORT1 );
}
