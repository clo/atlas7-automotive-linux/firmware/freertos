/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CAN_BUS_INTERFACE_TEST_LIB_H
#define CAN_BUS_INTERFACE_TEST_LIB_H

#define CAN_TASK_INDEX_ROBUS		1
#define CAN_TASK_INDEX_ROTRANS    	2
#define CAN_TASK_INDEX_ARB    		3


#define CAN_SEND_TEST_BUF_NUM		2
#define CAN_ARB_TEST_BUF_NUM		3
#define CAN_RETRANS_TEST_TX_BUF_NUM	4
#define CAN_RETRANS_TEST_RX_BUF_NUM	5

#define CAN_DEMO_BUF_NUM		10

#define CAN_ID_FOR_ROBUS_TEST		0x1
#define CAN_ID_FOR_ROTRANS_TEST		0x2

void vCanbusInterfaceArbStart( UINT32 uxCycle, UINT32 uxExtId );
void vCanbusInterfaceRetransStart( UINT32 uxPort );
void vCanbusInterfaceTestEnd( UINT32 uxIndex, UINT32 uxPort );
void vCanbusInterfaceTestStart( void );
void vCanbusInterfaceTestStop( void );

#endif
