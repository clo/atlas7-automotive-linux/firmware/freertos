/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"

#include "soc_iobridge.h"

#include "CANmod3.h"
#include "canbus.h"

#include "misclib.h"
#include "soc_timer.h"

static void prvCanKpiTestRx( uint32_t uxPort )
{
	CAN_MSGOBJECT xRxPacket;
	CAN_FILTEROBJECT xFilter;
	uint32_t uxTimeout;
	uint32_t uxPreTime,uxCurTime;
	uint32_t uxCounter;

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xQueueHandle xQueue = xCanReceiveQueueCreate( uxPort, &xFilter, 31, 160 );
	if( xQueue == NULL )
	{
		DebugMsg( "receive function create xQueue failed.\r\n" );
		return;
	}

	DebugMsg( "wait first message to start.\r\n" );
	xQueueReceive( xQueue, &xRxPacket, portMAX_DELAY );

	uxPreTime = uxOsGetCurrentTick();
	uxCounter = 0;
	for( uxTimeout=0;uxTimeout<10; )
	{
		xQueueReceive( xQueue, &xRxPacket, portMAX_DELAY );
		uxCounter ++;
		uxCurTime = uxOsGetCurrentTick();
		if((uxCurTime-uxPreTime) >= 1000000)
		{
			DebugMsg("CAN(%d) RX KPI %d CAN messages per %d us ( %d - %d )\r\n",uxPort,uxCounter,(uxCurTime-uxPreTime),uxCurTime,uxPreTime);
			uxTimeout ++;
			uxPreTime = uxCurTime;
			uxCounter = 0;
		}
	}

	xRxPacket.id = 0x740;
	xRxPacket.ide = 0;
	xRxPacket.rtr = 0;
	xRxPacket.data[0]=0x55; xRxPacket.data[1]=0x55; xRxPacket.data[2]=0x55; xRxPacket.data[3]=0x55;
	xRxPacket.data[4]=0x55; xRxPacket.data[5]=0x55; xRxPacket.data[6]=0x55; xRxPacket.data[7]=0x55;
	xRxPacket.dlc = 8;
	xCanSend( uxPort, &xRxPacket, 31, CAN_SEND_NORMAL );

}
static void prvCanKpiTestTx(uint32_t uxPort)
{
	CAN_MSGOBJECT xTxPacket;
	uint32_t uxPreTime,uxCurTime;
	uint32_t uxCounter;
	uint32_t uxCounterMiss;
	uint32_t uxTimeout;
	INT32 xRet;

	/* TX ability test */

	uxPreTime = uxOsGetCurrentTick();
	uxCounter = 0;
	uxCounterMiss=0;
	for( uxTimeout=0;uxTimeout<10; )
	{
		xTxPacket.id = rand_ul()&0x7FF;
		xTxPacket.ide = 0;
		xTxPacket.rtr = 0;
		xTxPacket.data[0]=0x11; xTxPacket.data[1]=0x22; xTxPacket.data[2]=0x33; xTxPacket.data[3]=0x44;
		xTxPacket.data[4]=0x55; xTxPacket.data[5]=0x66; xTxPacket.data[6]=0x77; xTxPacket.data[7]=0x88;
		xTxPacket.dlc = 8;
		xRet = xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		if(CAN_OK == xRet)
		{
			uxCounter ++;
		}
		else
		{
			uxCounterMiss ++;
		}
		uxCurTime = uxOsGetCurrentTick();
		if((uxCurTime-uxPreTime) >= 1000000)
		{
			DebugMsg("CAN(%d) TX KPI %d CAN messages(miss %d) per %d us ( %d - %d )\r\n",uxPort,uxCounter,uxCounterMiss,(uxCurTime-uxPreTime),uxCurTime,uxPreTime);
			uxTimeout ++;
			uxPreTime = uxCurTime;
			uxCounter = 0;
			uxCounterMiss = 0;
		}
	}
}
static portBASE_TYPE prvCanBusLoadTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t uxType, uxPort;

	uxPort = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );

	if( (NULL == pcParameter1) || (NULL == pcParameter2) )
	{
		DebugMsg("can_kpi invalid input of parameter\r\n");
		goto error_exit;
	}

	uxPort = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	uxType = ( uint32_t )strtoul( pcParameter2, NULL, 0 );

	if(uxPort > CAN_PORT1)
	{
		DebugMsg("can_kpi invalid input of parameter for Port\r\n");
		goto error_exit;
	}

	if(uxType > 1)
	{
		DebugMsg("can_kpi invalid input of parameter for Type\r\n");
		goto error_exit;
	}

	/* make sure CAN demo is stopped by CLI "can_demo 0" */
	/* set up environment */
	if( CAN_OK != xCanInit( uxPort, CAN_SPEED_48M_1M | CAN_AUTO_RESTART | CAN_LITTLE_ENDIAN,
		( PCAN_CONFIG_REG )0, 32, 32 ) )
	{
		DebugMsg( "xCanInit failed.\r\n" );
		goto error_exit;
	}
	xCanStart( uxPort, CANOP_MODE_NORMAL );

	if(0 == uxType)
	{
		prvCanKpiTestRx(uxPort);
	}
	else
	{
		prvCanKpiTestTx(uxPort);
	}

error_exit:
	return 0;
}

static const CLI_Command_Definition_t xCanbusLoadTestParameter =
{
	"can_kpi",
	"\r\ncan_kpi <Port> <Type>\r\n \
	Port:0 - CAN0; 1 - CAN1\r\n \
	Type:0 - CAN RX KPI Test ; 1 - CAN TX KPI Test\r\n",
	prvCanBusLoadTestCommand,
	-1
};
const CLI_Command_Definition_t *P_CANBUS_KPI_CLI_Parameter = &xCanbusLoadTestParameter;
