/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"
#include "soc_pwrc_retain.h"
#include "soc_ipc.h"
#include "io.h"
#include "pm.h"
#include "soc_timer.h"

#include "canbus.h"

#ifdef CONFIG_ENABLE_RECOVER_TEST
extern void vEnterRecoverMode(void);
#endif

extern void chime_init_buffer(int level_fl, int level_fr, int level_rl, int level_rr);
extern void chime_start_stream();
extern void chime_stop_stream();
extern bool m3_audio_support ;

static bool audio_in_playing = FALSE;
/*-----------------------------------------------------------*/
void vCanbusTriggerRearview( UINT8 ucEn  )
{
	UINT32 uxReg;

	/* Trigger interrupt to remote side */
	uxReg = SOC_REG( PWRC_PM_IPC_OPCODE );
	uxReg &= 0x7fffffff;
	SOC_REG( PWRC_PM_IPC_OPCODE ) = uxReg | ( ucEn << 31 );
	DebugMsg( "pm ipc is 0x%x.\r\n", ( UINT32 )SOC_REG( PWRC_PM_IPC_OPCODE ) );
	/* Trigger interrupt to remote side */
	SOC_REG( A7DA_IPC_TRGT1_INIT2_2 ) = 0x01;
}

static uint32_t uxCan0StartupTime;
void vCanbusReceiveFunction( void* pvPort )
{
	CAN_MSGOBJECT xRxPacket;
	CAN_FILTEROBJECT xFilter;
	UINT32 uxPort = ( UINT32 )pvPort;

	xFilter.amr.l = 0xffffffff;
	xFilter.acr.l = 0;
	xFilter.amcr_d.mask = 0xffff;
	xFilter.amcr_d.code = 0;

	xQueueHandle xQueue = xCanReceiveQueueCreate( uxPort, &xFilter, 0, 32 );
	if( xQueue == NULL )
	{
		DebugMsg( "receive function create xQueue failed.\r\n" );
		vTaskDelete(NULL);
	}
	if(CAN_PORT0 == uxPort)
	{
		uxCan0StartupTime = uxOsGetCurrentTick();
	}
	else
	{
		DebugMsg( "CAN(0) receive function on-line @ %d us.\r\n", uxCan0StartupTime);
		DebugMsg( "CAN(%d) receive function on-line @ %d us.\r\n", uxPort, uxOsGetCurrentTick());
	}
	for( ;; )
	{
		xQueueReceive( xQueue, &xRxPacket, portMAX_DELAY );
		if( CANBUS_ID_REARVIEW_START == xRxPacket.id )
		{
			vCanbusTriggerRearview( 1 );
		}
		else if ( CANBUS_ID_REARVIEW_STOP == xRxPacket.id )
		{
			vCanbusTriggerRearview( 0 );
		}
		#ifdef CONFIG_ENABLE_RECOVER_TEST
		else if((0x73f==xRxPacket.id) && (0x2==xRxPacket.data[0])
				&& (0x10==xRxPacket.data[1]) && (0x2==xRxPacket.data[2]))
		{
			vEnterRecoverMode();
		}
		#endif
		else if((0x73f==xRxPacket.id) && (0x2==xRxPacket.data[0])
				&& (0x10==xRxPacket.data[1]) && (0x1==xRxPacket.data[2]))
		{
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS session control response.
			 * use to test can response time within 25ms since wakeup */
			xTxPacket.id = 0x740;
			xTxPacket.ide = 0;
			xTxPacket.rtr = 0;
			xTxPacket.data[0]=0x06; xTxPacket.data[1]=0x50; xTxPacket.data[2]=0x01; xTxPacket.data[3]=0x00;
			xTxPacket.data[4]=0x64; xTxPacket.data[5]=0x13; xTxPacket.data[6]=0x88; xTxPacket.data[7]=0x55;
			xTxPacket.dlc = 8;
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		}
		else if((0x73f==xRxPacket.id) && (0x5==xRxPacket.data[0])
				&& (0x31==xRxPacket.data[1]) && (0x1==xRxPacket.data[2])
				&& (0xFF==xRxPacket.data[3]) && (0xF0==xRxPacket.data[4])
				&& (xRxPacket.data[5]<=PM_RESET_M3))
		{
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS routine control start with ID 0xFFF0.
			 * use to request service goto power saving mode by xRxPacket.data[5]*/
			DebugMsg("power mode is: %x\r\n", xRxPacket.data[5]);
			xTxPacket.id = 0x740;
			xTxPacket.ide = 0;
			xTxPacket.rtr = 0;
			xTxPacket.data[0]=0x04; xTxPacket.data[1]=0x71; xTxPacket.data[2]=0x01; xTxPacket.data[3]=0xFF;
			xTxPacket.data[4]=0xF0; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
			xTxPacket.dlc = 8;
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
			portDISABLE_INTERRUPTS();
			vGotoPowerSavingMode(xRxPacket.data[5]);
			portENABLE_INTERRUPTS();
		}
		else if((0x73f==xRxPacket.id) && (0x4==xRxPacket.data[0])
				&& (0x31==xRxPacket.data[1]) && (0x1==xRxPacket.data[2])
				&& (0xFF==xRxPacket.data[3]) && (0xF1==xRxPacket.data[4]))
		{
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS routine control start with ID 0xFFF1.
			 * use to request service wakeup A7 if only M3 wakeup */
			portDISABLE_INTERRUPTS();
			vM3WakeupA7();
			portENABLE_INTERRUPTS();
			xTxPacket.id = 0x740;
			xTxPacket.ide = 0;
			xTxPacket.rtr = 0;
			xTxPacket.data[0]=0x04; xTxPacket.data[1]=0x71; xTxPacket.data[2]=0x01; xTxPacket.data[3]=0xFF;
			xTxPacket.data[4]=0xF1; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
			xTxPacket.dlc = 8;
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		}
		else if((0x73f==xRxPacket.id) && (0x5==xRxPacket.data[0])
				&& (0x31==xRxPacket.data[1]) && (0x1==xRxPacket.data[2])
				&& (0xFF==xRxPacket.data[3]) && (0xF2==xRxPacket.data[4]))
		{	/* example cansend can0 73f#053101fff201 */
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS routine control start with ID 0xFFF2.
			 * use to request service wakeup A7 if only M3 wakeup */
			vPmEnableCanWakeup(xRxPacket.data[5]);
			xTxPacket.id = 0x740;
			xTxPacket.ide = 0;
			xTxPacket.rtr = 0;
			xTxPacket.data[0]=0x04; xTxPacket.data[1]=0x71; xTxPacket.data[2]=0x01; xTxPacket.data[3]=0xFF;
			xTxPacket.data[4]=0xF2; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
			xTxPacket.dlc = 8;
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		}
		else if((0x73f==xRxPacket.id) && (0x7==xRxPacket.data[0])
				&& (0x31==xRxPacket.data[1]) && (0x1==xRxPacket.data[2])
				&& (0xFF==xRxPacket.data[3]) && (0xF3==xRxPacket.data[4]))
		{	/* example cansend can0 73f#073101fff3aabbcc aabbcc can be 3 bytes as parameter */
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS routine control start with ID 0xFFF3.
			 * use to request start early audio with paramter  xRxPacket.data[5] xRxPacket.data[6] xRxPacket.data[7]*/
			if((true == m3_audio_support) && (false == audio_in_playing))
			{
				chime_init_buffer(5, 5, 5, 5);
				chime_start_stream();

				DebugMsg("\r\n\r\n\r\n >> start playing audio\r\n\r\n\r\n");
				audio_in_playing = true;
				xTxPacket.id = 0x740;
				xTxPacket.ide = 0;
				xTxPacket.rtr = 0;
				xTxPacket.data[0]=0x04; xTxPacket.data[1]=0x71; xTxPacket.data[2]=0x01; xTxPacket.data[3]=0xFF;
				xTxPacket.data[4]=0xF3; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
				xTxPacket.dlc = 8;
			}
			else
			{
				xTxPacket.id = 0x740;
				xTxPacket.ide = 0;
				xTxPacket.rtr = 0;
				xTxPacket.data[0]=0x7F; xTxPacket.data[1]=0x31; xTxPacket.data[2]=0x12; xTxPacket.data[3]=0x55;
				xTxPacket.data[4]=0x55; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
				xTxPacket.dlc = 8;
			}
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		}
		else if((0x73f==xRxPacket.id) && (0x7==xRxPacket.data[0])
				&& (0x31==xRxPacket.data[1]) && (0x2==xRxPacket.data[2])
				&& (0xFF==xRxPacket.data[3]) && (0xF3==xRxPacket.data[4]))
		{	/* example cansend can0 73f#073101fff3aabbcc aabbcc can be 3 bytes as parameter */
			CAN_MSGOBJECT xTxPacket;
			/* yes, this is UDS routine control start with ID 0xFFF3.
			 * use to request stop early audio with paramter  xRxPacket.data[5] xRxPacket.data[6] xRxPacket.data[7]*/
			if((true == m3_audio_support) && (true == audio_in_playing))
			{
				chime_stop_stream();

				DebugMsg("\r\n\r\n\r\n >> stop playing audio\r\n\r\n\r\n");
				audio_in_playing = false;

				xTxPacket.id = 0x740;
				xTxPacket.ide = 0;
				xTxPacket.rtr = 0;
				xTxPacket.data[0]=0x04; xTxPacket.data[1]=0x71; xTxPacket.data[2]=0x02; xTxPacket.data[3]=0xFF;
				xTxPacket.data[4]=0xF3; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
				xTxPacket.dlc = 8;
			}
			else
			{
				xTxPacket.id = 0x740;
				xTxPacket.ide = 0;
				xTxPacket.rtr = 0;
				xTxPacket.data[0]=0x7F; xTxPacket.data[1]=0x31; xTxPacket.data[2]=0x12; xTxPacket.data[3]=0x55;
				xTxPacket.data[4]=0x55; xTxPacket.data[5]=0x55; xTxPacket.data[6]=0x55; xTxPacket.data[7]=0x55;
				xTxPacket.dlc = 8;
			}
			xCanSend( uxPort, &xTxPacket, 31, CAN_SEND_NORMAL );
		}
		else
		{
			/* do nothing */
		}
#ifdef CONFIG_ENABLE_CAN_LOG
		DebugMsg( "can(%d) receive id is 0x%x, dlc is %d, data is 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\r\n",
			uxPort, xRxPacket.id, xRxPacket.dlc,
			xRxPacket.data[0], xRxPacket.data[1], xRxPacket.data[2], xRxPacket.data[3],
			xRxPacket.data[4], xRxPacket.data[5], xRxPacket.data[6], xRxPacket.data[7]);
#endif
	}
}
#ifdef CONFIG_ENABLE_VIRTIO_CAN_TASK
extern int virtio_can_send_frame( u32 chn, void *data, bool wait );
void vVirtCanbusReceiveFunction( void* pvPort )
{
	CAN_MSGOBJECT xRxPacket;
	UINT32 uxPort = ( UINT32 )pvPort;

	xQueueHandle xQueue = xGetVirtCanReceiveQueue( uxPort );

	for( ;; )
	{
		xQueueReceive( xQueue, &xRxPacket, portMAX_DELAY );
		if((PM_NORMAL == uxGetPmLevel()))
		{
			if(0 != virtio_can_send_frame( uxPort, &xRxPacket, false ))
			{
				DebugMsg( "ERR: A7 VIRTCAN ring buffer is full!");
			}
		}
	}
}
#endif
void vCanbusTestInit( UINT32 uxCan0Clk, UINT32 uxCan1Clk )
{
	xCanInit( CAN_PORT0, uxCan0Clk | CAN_AUTO_RESTART | CAN_BIG_ENDIAN,
				( PCAN_CONFIG_REG )0, 0, 0 );

	xCanInit( CAN_PORT1, uxCan1Clk | CAN_AUTO_RESTART | CAN_BIG_ENDIAN,
				( PCAN_CONFIG_REG )0, 0, 0 );

	xCanStart( CAN_PORT0, CANOP_MODE_NORMAL );

	xCanStart( CAN_PORT1, CANOP_MODE_NORMAL );
}
