/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "misclib.h"
#include "soc_gpio.h"


eGpioIrqType xConvertIrqType(uint32_t xIntrType)
{
	switch (xIntrType) {
	case 0:
		return eGpioIrqTypeEdgeRising;
	case 1:
		return eGpioIrqTypeEdgeFalling;
	case 2:
		return eGpioIrqTypeLevelLow;
	case 3:
		return eGpioIrqTypeLevelHigh;
	}
	return 0;
}

void vGpioTestPinISR( eGpioChipId eChipId, uint8_t ucGpioIdx, void *pxData )
{
	uint32_t *uxIntrType = (uint32_t *)pxData;

	vGpioDisableInterrupt( eChipId, ucGpioIdx );
	if ( xConvertIrqType(*uxIntrType) == eGpioIrqTypeLevelLow )
		vRetainGpioSetValueUnsafe(eGpioVdifmLcdVip, GPIO_LCD_6, 1);
	if ( xConvertIrqType(*uxIntrType) == eGpioIrqTypeLevelHigh )
		vRetainGpioSetValueUnsafe(eGpioVdifmLcdVip, GPIO_LCD_6, 0);
	vGpioClearInterrupt( eChipId, ucGpioIdx );
	DebugMsg( "GPIO Interrupt, ChipId=%d PinIdx=%d IntrType=%d\r\n", eChipId, ucGpioIdx, *uxIntrType );
	vGpioEnableInterrupt( eChipId, ucGpioIdx );
}

static portBASE_TYPE prvGpioIntrTestCmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2, *pcParameter3;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xParameter3StringLength;
	uint32_t uxStartOrOval = 0, uxDirection = 0, uxIntrType = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
	if( NULL == pcParameter1 ||
		NULL == pcParameter2 ||
		NULL == pcParameter3 )
	{
		return 0;
	}
	pcParameter1[ xParameter1StringLength ] = 0x00;
	uxDirection = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	pcParameter2[ xParameter2StringLength ] = 0x00;
	uxStartOrOval = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	pcParameter3[ xParameter3StringLength ] = 0x00;
	uxIntrType = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
	/* direction: 1 - output; 0 - input */
	DebugMsg( " gpio %s test %s\r\n", uxDirection ? "output" : "input",
			(uxDirection) ? (uxStartOrOval ? "set ot val 1" : "set out val 0") :
			(uxStartOrOval ? "intr start" : "intr stop"));
	if ( !uxDirection ) {
		if ( uxStartOrOval )
			xGpioRegisterInterrupt( vGpioTestPinISR, eGpioVdifmLcdVip, GPIO_LCD_4, xConvertIrqType(uxIntrType), &uxIntrType );
		else
			vGpioUnRegisterInterrupt( eGpioVdifmLcdVip, GPIO_LCD_4 );
	} else {
		vGpioSetDirectionOutput ( eGpioVdifmLcdVip, GPIO_LCD_6, uxStartOrOval);
	}
	return pdFALSE;
}

const CLI_Command_Definition_t xGpioIntrTestCmdParameter =
{
	"gpio_test",
	"\r\ngpio_test: <direction> <en/out_val> <int_type>\r\n \
	direction:	0 - input direction \r\n \
			1 - output direction \r\n \
	en/out_val:	0 - stop or output value 0 \r\n \
			1 - start or output value 1 \r\n \
	int_type:	0 - rising edge \r\n \
			1 - falling edge \r\n \
			2 - low level \r\n \
			3 - high level",
	prvGpioIntrTestCmd,
	-1
};

const CLI_Command_Definition_t *P_GPIO_INTR_CLI_Parameter = &xGpioIntrTestCmdParameter;
