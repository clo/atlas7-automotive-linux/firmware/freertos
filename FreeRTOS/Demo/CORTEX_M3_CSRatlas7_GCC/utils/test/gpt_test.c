/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "gpt.h"
#include "debug.h"
#include "misclib.h"
#include "soc_timer.h"
/* ================================================ [ MACRO ] ====================================================== */
#define GPT_TIMER_NUM(chip)					((eGptOsTimerM3==(chip)) ? GPT_MAX_TIMER_M3 : GPT_MAX_TIMER)

#define WDG_TEST_CMD_START 0
#define WDG_TEST_CMD_STOP  1
#define WDG_TEST_CMD_FEED  2

/* ================================================ [ TYPE  ] ====================================================== */
/* ================================================ [ DECL  ] ====================================================== */
static portBASE_TYPE prvGptCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static portBASE_TYPE prvGpt2PwmCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static portBASE_TYPE prvWdgCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
void __on_retain vGotoPowerResetA7( void );
/* ================================================ [ DATA  ] ====================================================== */
const CLI_Command_Definition_t gptParameter =
{
    "gpt",
    "\rgpt < OSTimer >:\r\n    OSTimer: 0 - OS TImer A; 1 - OS TImer B; 2 - OS TImer M3\r\n",
    prvGptCmd,
    -1
};
const CLI_Command_Definition_t *P_GPT_CLI_Parameter = &gptParameter;

const CLI_Command_Definition_t wdgCmdParameter =
{
    "gptwdg",
    "\rgptwdg < OSTimer > <cmd> [1st] [2nd] :\r\n"
	"    OSTimer: 0 - OS TImer A; 1 - OS TImer B; 2 - OS TImer M3\r\n"
	"    cmd: 0-start; 1-stop; 2-feed;\r\n"
	"    1st: 1st stage timeout in us for cmd start only\r\n"
	"    2st: 2nd stage timeout in us for cmd start only\r\n",
    prvWdgCmd,
    -1
};
const CLI_Command_Definition_t *P_WDG_CLI_Parameter = &wdgCmdParameter;

const CLI_Command_Definition_t gpt2pwmParameter =
{
    "gpt2pwm",
    "\rgpt2pwm < OSTimer > < Channel> <Hz>:\r\n"
    "    OSTimer: 0 - OS TImer A; 1 - OS TImer B; 2 - OS TImer M3\r\n"
    "    Channel: id of the timer\r\n"
    "    Hz: frequency of PWM range (1~1000000)Hz, last for 30s and then auto-stop, output on gpio0 r81\r\n",
    prvGpt2PwmCmd,
    -1
};
const CLI_Command_Definition_t *P_GPT2PWM_CLI_Parameter = &gpt2pwmParameter;
static uint32_t xCounters[eGptChipMax][GPT_MAX_TIMER];
static const xGptParameter xLoopParam[3] =
{
    {.TIMER_DIV=0,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/1000    }, /* 1ms parameter,  loop */
    {.TIMER_DIV=1,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/(2*100) }, /* 10ms parameter, loop */
    {.TIMER_DIV=3,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/(4*10)  }  /* 100ms parameter,loop */
};
static const xGptParameter x1ShotParam[3] =
{
    {.TIMER_DIV=0,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/1000    }, /* 1ms parameter   */
    {.TIMER_DIV=1,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/(2*100) }, /* 10ms parameter  */
    {.TIMER_DIV=3,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configSOC_IO_CLK/(4*10)  }  /* 100ms parameter */
};

static const xGptParameter xLoopParam_M3[3] =
{
    {.TIMER_DIV=0,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/1000    }, /* 1ms parameter,  loop */
    {.TIMER_DIV=1,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/(2*100) }, /* 10ms parameter, loop */
    {.TIMER_DIV=3,.LOOP_EN=1,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/(4*10)  }  /* 100ms parameter,loop */
};
static const xGptParameter x1ShotParam_M3[3] =
{
    {.TIMER_DIV=0,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/1000    }, /* 1ms parameter   */
    {.TIMER_DIV=1,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/(2*100) }, /* 10ms parameter  */
    {.TIMER_DIV=3,.LOOP_EN=0,.INTR_EN=1,.uxPeriod=configCPU_CLOCK_HZ/(4*10)  }  /* 100ms parameter */
};
static const uint32_t xLoopTimes[3] =
{
    1000,
    100,
    10
};
static const xWdgParameter xWdgParam =
{
    .TIMER_DIV1=0,.TIMER_DIV2=0,.ux1stPeriod = configSOC_IO_CLK,.ux2ndPeriod = configSOC_IO_CLK*5 /*1s first stage 5s second stage parameter */
};
static const xWdgParameter xWdgParam_M3 =
{
    .TIMER_DIV1=0,.TIMER_DIV2=0,.ux1stPeriod = configCPU_CLOCK_HZ,.ux2ndPeriod = configCPU_CLOCK_HZ*5 /*1s first stage 5s second stage parameter */
};
static const xGptParameter xTimeParam =
{
   .TIMER_DIV=(configSOC_IO_CLK/1000000 -1),.LOOP_EN=0,.INTR_EN=0,.uxPeriod = 10000000, /* 1 MHz, one shot,10s */
};
static const xGptParameter xTimeParam_M3 =
{
   .TIMER_DIV=(configCPU_CLOCK_HZ/1000000 -1),.LOOP_EN=0,.INTR_EN=0,.uxPeriod = 10000000, /* 1 MHz, one shot,10s */
};
/* ================================================ [ FUNC  ] ====================================================== */
static void prvGptHandler(eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
    xCounters[eChipId][ucGptIdx]++;
}
static void prvWdgHandler( eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
    DebugMsg("    # Wdg Interrupt from OS Timer %s\r\n",(eGptOsTimerA==eChipId)?"A":((eGptOsTimerB==eChipId)?"B":"M3"));

    if( (eGptOsTimerA==eChipId) || (eGptOsTimerB==eChipId) )
    {
    	DebugMsg("    # Reset A7 by M3, so that M3 will not died\r\n");
    	vGotoPowerResetA7();
    }
    else
    {
    	/* This is M3 watchdog timeout, A7 should save important context */
    }
}

static void prvWdgHandler2( eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
    DebugMsg("    # Wdg Interrupt from OS Timer %s\r\n",(eGptOsTimerA==eChipId)?"A":((eGptOsTimerB==eChipId)?"B":"M3"));

    vUsWait(1000000);
}
static portBASE_TYPE prvGptCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    char *pcParameter1;
    uint32_t request;
    int xRet,i;
    BaseType_t xParameter1StringLength;


    pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
    if(pcParameter1 == NULL) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }
    request = (uint32_t)strtoul(pcParameter1, NULL, 0);
    if((request != 0) && (request != 1) && (request != 2)) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }

    /* Initialization */
    xRet = xGptInitialize(request);
    if(xRet != 0)
    {
        DebugMsg("  >> Gpt Initialize failed with code %d\r\n",xRet);
        if ( -EEXIST != xRet )
        {
            goto error_exit;
        }
    }

    if(eGptOsTimerM3 == request)
    {	/* patch as someone start M3 timer 0 before */
    	vGptStopTimer(request,0);
    }
/* -------- 1 loop mode test -------- */
    DebugMsg("  >> 1. Gpt Loop test with loop enabled, check call times within 1 second.\r\n",xRet);
    memset(xCounters,0,sizeof(xCounters));
    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
    	xRet = xGptRegisterInterrupt(prvGptHandler,request,i,NULL);
    	if(xRet !=0) { DebugMsg("    # xGptRegisterInterrupt(%d,%d) failed with code %d\r\n",request,i,xRet); }
    }

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
    	if(eGptOsTimerM3 == request)
    	{
    		xRet = xGptStartTimer( request,i, &xLoopParam_M3[i%3]);
    	}
    	else
    	{
    		xRet = xGptStartTimer( request,i, &xLoopParam[i%3]);
    	}
        if(xRet !=0) { DebugMsg("    # xGptStartTimer(%d,%d) failed with code %d\r\n",request,i,xRet); }
    }

    vTaskDelay(configTICK_RATE_HZ);

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
        vGptStopTimer(request,i);
    }

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
        DebugMsg("    # Gpt(%d,%d) real=%-4d expected=%-4d match_num=%-4d\r\n",(int)request,i,(int)xCounters[request][i],(int)xLoopTimes[i%3],xGptGetTimerMatchNum(request,i));
    }
/* -------- 2 one shot mode test -------- */
    DebugMsg("  >> 2. Gpt Loop test with loop disabled, check call times within 1 second.\r\n",xRet);
    memset(xCounters,0,sizeof(xCounters));

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
    	if(eGptOsTimerM3 == request)
    	{
    		xRet = xGptStartTimer( request,i, &x1ShotParam_M3[i%3]);
    	}
    	else
    	{
    		xRet = xGptStartTimer( request,i, &x1ShotParam[i%3]);
    	}
        if(xRet !=0) { DebugMsg("    # xGptStartTimer(%d,%d) failed with code %d\r\n",request,i,xRet); }
    }

    vTaskDelay(configTICK_RATE_HZ);

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
        vGptStopTimer(request,i);
    }

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
    	DebugMsg("    # Gpt(%d,%d) real=%-4d expected=1    match_num=%-4d\r\n",(int)request,i,(int)xCounters[request][i],xGptGetTimerMatchNum(request,i));
    }
/* -------- 3 get time test  -------- */
    DebugMsg("  >> 3. Gpt test elapsed and remaining value\r\n");
    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
    	if(eGptOsTimerM3 == request)
    	{
    		xRet=xGptStartTimer( request,i, &xTimeParam_M3);
    	}
    	else
    	{
    		xRet=xGptStartTimer( request,i, &xTimeParam);
    	}
        if(xRet !=0) { DebugMsg("    # xGptStartTimer(%d,%d) failed with code %d\r\n",request,i,xRet); }
    }

    vTaskDelay(configTICK_RATE_HZ);
    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
        DebugMsg("    # Gpt(%d,%d) real: elapsed(%d us), remaining(%d us); expected: elapsed(%d us), remaining(%d us) \r\n",
                    request,i,
                    xGptGetTimerElapsed(request,i),xGptGetTimerRemaining(request,i),
                    (i+1)*1000000,(10-i-1)*1000000);
        vTaskDelay(configTICK_RATE_HZ);
    }

    for(i=0;i<GPT_TIMER_NUM(request);i++)
    {
        vGptStopTimer(request,i);
    }
/* -------- 4 get 64 bit timer test  -------- */
    DebugMsg("  >> 4. Gpt test 64 bit timer\r\n");
    if(request<=eGptOsTimerB)
    {
    	uint64_t xValue;
    	if(eGptOsTimerM3 == request)
    	{
    		xGptStart64Timer(request,(configCPU_CLOCK_HZ/1000000)-1);	/* 1 MHz*/
    	}
    	else
    	{
    		xGptStart64Timer(request,(configSOC_IO_CLK/1000000)-1);	/* 1 MHz*/
    	}

    	vTaskDelay(configTICK_RATE_HZ);
    	xValue=xGptRead64Timer(request);
    	DebugMsg("    # Gpt(%d) real: (%d)%08x%08x us; expected: 1000000 us\r\n", request,(uint32_t)xValue,(uint32_t)(xValue>>32),(uint32_t)(xValue&0xFFFFFFFF));
    	vTaskDelay(configTICK_RATE_HZ);
    	xValue=xGptRead64Timer(request);
    	DebugMsg("    # Gpt(%d) real: (%d)%08x%08x us; expected: 2000000 us\r\n", request,(uint32_t)xValue,(uint32_t)(xValue>>32),(uint32_t)(xValue&0xFFFFFFFF));
    }
    else
    {
    	DebugMsg("    # M3 Timer has no 64 bit timer\r\n");
    }
/* -------- 5 watch dog test -------- */
    DebugMsg("  >> 5. Gpt Start watchdog(%d) (1s --> 5s)\r\n",request);
    xRet=xGptRegisterWdgInterrupt(prvWdgHandler,request,NULL);
    if(xRet !=0) { DebugMsg("    # xGptRegisterWdgInterrupt(%d) failed with code %d\r\n",request,xRet); }

    if(eGptOsTimerM3 == request)
    {
    	xRet=xGptStartWdg(request,&xWdgParam_M3);
    }
    else
    {
    	xRet=xGptStartWdg(request,&xWdgParam);
    }
    if(xRet !=0) { DebugMsg("    # xGptStartWdg(%d) failed with code %d\r\n",request,xRet); }
    for(i=0;i<6;i++)
    {
        vTaskDelay(configTICK_RATE_HZ/2); /* 0.5s */
        xGptFeedWdg(request);
        DebugMsg("    # feed dog in 0.5s\r\n");
    }
    DebugMsg("    # let watchdog timeout, reset will happen\r\n");
    vTaskDelay(configTICK_RATE_HZ*12/10);    /* 1.2s */
    vTaskDelay(configTICK_RATE_HZ*5);         /* 5s */
    DebugMsg("    # if you see this message, then watchdog test failed,but if it is OS Timer A or B, it is OK.\r\n");

error_exit:
    return 0;
}
static char now_state;
static void prvDioInit(void)
{
	now_state = 0;
	SOC_REG(0x13300000)= 0x20;
}
static void prvDioToggleState(void)
{
	/* Use Gpio_0, detect R81. */
	if(now_state)
	{
		now_state = 0;
		SOC_REG(0x13300000)= 0x20;
	}
	else
	{
		now_state = 1;
		SOC_REG(0x13300000)= 0x60;
	}
}
static void prvGpt2PwmHandler(eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
	prvDioToggleState();
}
static portBASE_TYPE prvGpt2PwmCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    char *pcParameter1;
    char *pcParameter2;
    char *pcParameter3;
    uint32_t request,timerid,freq;
    uint32_t clck;
    int xRet;
    BaseType_t xParameter1StringLength;
    BaseType_t xParameter2StringLength;
    BaseType_t xParameter3StringLength;
    xGptParameter xParam;


    pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
    pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
    pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
    if( (pcParameter1 == NULL) || (pcParameter2 == NULL) || (pcParameter3 == NULL) ) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }
    request = (uint32_t)strtoul(pcParameter1, NULL, 0);
    if((request != 0) && (request != 1) && (request != 2)) {
        DebugMsg( "failed: invalid input paramter OSTimer \r\n" );
        goto error_exit;
    }

    timerid = (uint32_t)strtoul(pcParameter2, NULL, 0);
	if(timerid >= GPT_TIMER_NUM(request)) {
		DebugMsg( "failed: invalid input paramter Channel\r\n" );
		goto error_exit;
	}

	freq = (uint32_t)strtoul(pcParameter3, NULL, 0);
	if( (freq >= 1000000) || (freq < 1) ){
		DebugMsg( "failed: invalid input paramter Hz\r\n" );
		goto error_exit;
	}

    /* Initialization */
    xRet = xGptInitialize(request);
    if(xRet != 0)
    {
        DebugMsg("  >> Gpt Initialize failed with code %d\r\n",xRet);
        if ( -EEXIST != xRet )
        {
            goto error_exit;
        }
    }

    if(eGptOsTimerM3 == request)
    {	/* patch as someone start M3 timer 0 before */
    	vGptStopTimer(request,0);
    	clck = configCPU_CLOCK_HZ;
    }
    else
    {
    	clck = configSOC_IO_CLK;
    }

    DebugMsg("  >> Test Gpt Period accuracy: on <%d,%d> %d Hz 50%% PWM, tap GPIO0 R81 to measure\r\n",request,timerid,freq);

    xRet = xGptRegisterInterrupt(prvGpt2PwmHandler,request,timerid,NULL);
    if(xRet !=0) { DebugMsg("    # xGptRegisterInterrupt(%d,%d) failed with code %d\r\n",request,timerid,xRet); }

    /* set up parameter */
    xParam.TIMER_DIV = (1<<timerid)-1;
    xParam.uxPeriod=clck/((1<<timerid)*freq*2);
    xParam.INTR_EN = 1;
    xParam.LOOP_EN = 1;

    prvDioInit();
    xRet=xGptStartTimer( request,timerid, &xParam);
    if(xRet !=0) { DebugMsg("    # xGptStartTimer(%d,%d) failed with code %d\r\n",request,timerid,xRet); }

    vTaskDelay(configTICK_RATE_HZ*30); /* 30s */

    vGptStopTimer(request,timerid);

error_exit:
	return 0;
}

static portBASE_TYPE prvWdgCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    char *pcParameter1;
    char *pcParameter2;
    char *pcParameter3;
    char *pcParameter4;
    uint32_t request,cmd,_1st,_2nd;
    uint32_t clck;
    int xRet;
    BaseType_t xParameter1StringLength;
    BaseType_t xParameter2StringLength;
    BaseType_t xParameter3StringLength;
    BaseType_t xParameter4StringLength;
    xWdgParameter xParam;


    pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
    pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
    pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 3, &xParameter3StringLength );
    pcParameter4 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 4, &xParameter4StringLength );
    if( (pcParameter1 == NULL) || (pcParameter2 == NULL) ) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }
    request = (uint32_t)strtoul(pcParameter1, NULL, 0);
    if((request != 0) && (request != 1) && (request != 2)) {
        DebugMsg( "failed: invalid input paramter OSTimer \r\n" );
        goto error_exit;
    }

	cmd = (uint32_t)strtoul(pcParameter2, NULL, 0);
	if( (WDG_TEST_CMD_START != cmd) && (WDG_TEST_CMD_STOP != cmd) && (WDG_TEST_CMD_FEED != cmd) ){
		DebugMsg( "failed: invalid input paramter cmd\r\n" );
		goto error_exit;
	}

	if(WDG_TEST_CMD_START == cmd)
	{
		if( (pcParameter3 == NULL) || (pcParameter4 == NULL) ) {
			DebugMsg( "failed: invalid input paramter for cmd start\r\n" );
			goto error_exit;
		}
	}

    /* Initialization */
    xRet = xGptInitialize(request);
    if(xRet != 0)
    {
        DebugMsg("  >> Gpt Initialize failed with code %d\r\n",xRet);
        if ( -EEXIST != xRet )
        {
            goto error_exit;
        }
    }

    if(eGptOsTimerM3 == request)
    {	/* patch as someone start M3 timer 0 before */
    	clck = configCPU_CLOCK_HZ;
    }
    else
    {
    	clck = configSOC_IO_CLK;
    }

    switch(cmd)
    {
		case WDG_TEST_CMD_START:

			_1st = (uint32_t)strtoul(pcParameter3, NULL, 0);
			_2nd = (uint32_t)strtoul(pcParameter4, NULL, 0);

			DebugMsg("  >> Test Wdg start %d, 1st=%d us 2nd=%d us\r\n",request,_1st,_2nd);
			xRet = xGptRegisterWdgInterrupt(prvWdgHandler2,request,NULL);
			if(xRet !=0) { DebugMsg("    # xGptRegisterWdgInterrupt(%d) failed with code %d\r\n",request,xRet); }

			/* set up parameter */
			xParam.TIMER_DIV1 = clck/1000000-1;
			xParam.TIMER_DIV2 = clck/1000000-1;
			xParam.ux1stPeriod=_1st;
			xParam.ux2ndPeriod=_2nd;

			xRet=xGptStartWdg(request,&xParam);
			if(xRet !=0) { DebugMsg("    # xGptStartWdg(%d) failed with code %d\r\n",request,xRet); }
			break;
		case WDG_TEST_CMD_STOP:
			DebugMsg("  >> Test Wdg stop\r\n");
			vGptStopWdg(request);
			break;
		case WDG_TEST_CMD_FEED:
			DebugMsg("  >> Test Wdg feed\r\n");
			xRet=xGptFeedWdg(request);
			if(xRet !=0) { DebugMsg("    # xGptFeedWdg(%d) failed with code %d\r\n",request,xRet); }
			break;
		default:
			break;
    }

error_exit:
	return 0;
}
