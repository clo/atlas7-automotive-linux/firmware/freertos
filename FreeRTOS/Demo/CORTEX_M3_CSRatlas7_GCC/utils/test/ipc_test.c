/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "soc_clkc.h"
#include "soc_irq.h"
#include "soc_ipc.h"
#include "core_cm3.h"
#include "soc_kas.h"


static int kas_ipc_test_state;

void INT_IPC_TRGT2_INIT0_INTR1_Handler(void)
{
	u32 reg;
	DebugMsg("IPC Intr 1, flag 0x%x, addr 0x%x\r\n",
		clkc_get_m3_wakeup_flag(), clkc_get_m3_wakeup_addr());

	/* read register to clear the interrupt */
	reg = SOC_REG(A7DA_IPC_TRGT2_INIT0_1);
	/* mask compiler warning */
	reg = reg;
}

void INT_IPC_TRGT2_INIT3_INTR1_Handler(void)
{
	u32 reg;

	DebugMsg("INT_IPC_TRGT2_INIT3_INTR1_Handler\r\n");

	/* read register to clear the interrupt */
	reg = SOC_REG(A7DA_IPC_TRGT2_INIT3_1);
	reg = reg; /* remove complier warning */
	if (!kas_ipc_test_state) {
		DebugMsg("START KAS IPC TESTING...\r\n");
		SOC_REG(A7DA_IPC_TRGT3_INIT2_1) = 0x1;
		SOC_REG(A7DA_IPC_TRGT3_INIT2_2) = 0x1;
		kas_ipc_test_state++;
	} else {
		DebugMsg("KAS IPC TEST PASSED 1!\r\n");
	}
}

void INT_IPC_TRGT2_INIT3_INTR2_Handler(void)
{
	volatile u32 reg;

	DebugMsg("INT_IPC_TRGT2_INIT3_INTR2_Handler\r\n");

	/* read register to clear the interrupt */
	reg = SOC_REG(A7DA_IPC_TRGT2_INIT3_2);
	reg = reg; /* remove complier warning */
	if (kas_ipc_test_state) {
		DebugMsg("KAS IPC TEST PASSED 2!\r\n");
	}
}

static portBASE_TYPE ipccmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	uint32_t uxIpcTestIrq0, uxIpcTestIrq1;

	uxIpcTestIrq0 = INT_IPC_TRGT2_INIT0_INTR1_IRQn;
#ifdef __ATLAS7_STEP_B__
	uxIpcTestIrq1 = INT_IPC_TRGT2_INIT0_INTR2_IRQn;
#else
	uxIpcTestIrq1 = INT_IPC_TRGT2_INIT1_INTR2_IRQn;
#endif
	DebugMsg( "start m3-a7 ipc test\r\n" );

	/* Setup IPC interrupts' priority */
	NVIC_SetPriority(uxIpcTestIrq0, API_SAFE_INTR_PRIO_0);
	NVIC_SetPriority(uxIpcTestIrq1, API_SAFE_INTR_PRIO_0);

	/* enable ipc irq from m3 side */
	NVIC_EnableIRQ(uxIpcTestIrq0);
	NVIC_EnableIRQ(uxIpcTestIrq1);


	SOC_REG(A7DA_IPC_TRGT0_INIT2_1) = 0x1;
	vTaskDelay( 1 );
	SOC_REG(A7DA_IPC_TRGT0_INIT2_2) = 0x1;
	vTaskDelay( 1 );
	SOC_REG(A7DA_IPC_TRGT1_INIT2_1) = 0x1;
	vTaskDelay( 1 );
	SOC_REG(A7DA_IPC_TRGT1_INIT2_2) = 0x1;
	vTaskDelay( 1 );

	DebugMsg( "end m3-a7 ipc test\r\n" );

	return 0;
}

static portBASE_TYPE ipckascmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	DebugMsg( "start m3-kas ipc test\r\n" );

	kas_ipc_test_state = 0;

	/* Setup IPC interrupts' priority */
	NVIC_SetPriority(INT_IPC_TRGT2_INIT3_INTR1_IRQn, API_SAFE_INTR_PRIO_0);
	NVIC_SetPriority(INT_IPC_TRGT2_INIT3_INTR2_IRQn, API_SAFE_INTR_PRIO_0);

	/* enable ipc irq in m3 side */
	NVIC_EnableIRQ(INT_IPC_TRGT2_INIT3_INTR1_IRQn);
	NVIC_EnableIRQ(INT_IPC_TRGT2_INIT3_INTR2_IRQn);

	prvSetupKas();

	DebugMsg( "end m3-kas ipc test\r\n" );

	return 0;
}

const CLI_Command_Definition_t ipcParameter =
{
	"ipc",
	"\r\nipc <...>:\r\n",
	ipccmd,
	0
};
const CLI_Command_Definition_t *P_IPC_CLI_Parameter = &ipcParameter;

const CLI_Command_Definition_t ipckasParameter =
{
	"ipckas",
	"\r\nipckas <...>:\r\n",
	ipckascmd,
	0
};
const CLI_Command_Definition_t *P_IPCKAS_CLI_Parameter = &ipckasParameter;
