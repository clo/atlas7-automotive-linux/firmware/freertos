/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __ATLAS7_STEP_B__
/* FreeRTOS include files. */
#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "soc_pwrc.h"
#include "io.h"
#include "pm.h"
#include "soc_timer.h"
#include "soc_cache.h"

#include "debug.h"
#include "soc_iobridge.h"
#include "soc_uart.h"
#include "misclib.h"
#include "loader.h"
#include "sirfdrive3.h"

static portBASE_TYPE prvLoaderCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	uint32_t uxBootMode;
	char *pcParameter1;
	uint32_t request;
	BaseType_t xParameter1StringLength;


	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(pcParameter1 == NULL) {
		DebugMsg( "failed: invalid input paramter\r\n" );
		goto error_exit;
	}
	request = (uint32_t)strtoul(pcParameter1, NULL, 0);
	if((request != 0) && (request != 1) ) {
		DebugMsg( "failed: invalid input paramter\r\n" );
		goto error_exit;
	}

	uxBootMode = uxPmCheckBootMode();
	if( PM_BOOT_INDEPENDENT != uxBootMode )
	{
		DebugMsg("This command is invalid for Dependent mode\r\n");
		goto error_exit;
	}

	if(0 == request)
	{
		SirfDrive3_DeInit();
		vLoaderStartup();

		xUartDeviceInitialize( UART0, UART_MODE_INTR, configCLI_BAUD_RATE, configSOC_IO_CLK );
		vUsWait(500000);
		DebugMsg("load u-boot done\r\n");
		vLoaderPerformance();
	}
	else
	{	/* show loader performance, invoke after command loader 0 */
		vLoaderPerformance();
	}

error_exit:
	return 0;
}

const CLI_Command_Definition_t loaderParameter =
{
    "loader",
    "\rloader <option>:\r\n"
	"    0: request to load uboot and request DRAM boot\r\n"
	"    1: print out the performance of the loader\r\n",
	prvLoaderCmd,
    -1
};
const CLI_Command_Definition_t *P_LOADER_CLI_Parameter = &loaderParameter;

#endif /* __ATLAS7_STEP_B__ */
