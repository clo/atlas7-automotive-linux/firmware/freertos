/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "debug.h"
#include "soc_clkc.h"
#include "soc_pwrc.h"
#include "soc_iobridge.h"
#include "io.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

#define	BYPASS			1
#define	FOUTPOSTDIV1PD	 	(1 << 15)
#define	FOUTPOSTDIV2PD	 	(1 << 16)
#define	FOUTVCOPD		(1 << 17)
#define	PD			(1 << 18)
#define	POSTDIV1		19
#define	POSTDIV2		23
#define	LOCK			(1 << 27)

/* switch M3 clock fast/slow test */
static int clockSwitchTest(void)
{
	unsigned int m3_clock_reg;

	/* for M3 clock test, it need A7 to switch the clock from fast to slow
	 * or slow to fast after M3 calling "WFI" to go to sleep. So when A7
	 * has finished the clock switch, A7 will trigger IPC interrupt to M3
	 * to wake up it.
	*/
	/* firstly, disable system interrupt to avoid other interrupts waking
	 * M3 up excluding the IPC interrupt from A7
	*/
	portDISABLE_INTERRUPTS();
	SOC_REG(A7DA_CLKC_CLKC_SW_REG4) = 0x1;
	__asm("wfi");
	m3_clock_reg = uxIoBridgeRead(A7DA_PWRC_M3_CLK_EN);
	DebugMsg("set m3 slow clock 0x%x!\r\n", m3_clock_reg);

	SOC_REG(A7DA_CLKC_CLKC_SW_REG4) = 0x1;
	__asm("wfi");
	m3_clock_reg = uxIoBridgeRead(A7DA_PWRC_M3_CLK_EN);
	DebugMsg("set m3 fast clock 0x%x!\r\n", m3_clock_reg);

	return 0;
}

/* VCO change, lower than 5% */
static int vcoChangeLowerTest(void)
{
	unsigned int pwr_rtc_pll_ctrl_old;
	unsigned int pwr_rtc_pll_ctrl;

	pwr_rtc_pll_ctrl_old = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);

	DebugMsg("set rtc-pll clock 168MHZ!\r\n");
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl_old;
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X1404 << 1); /* 168MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 168MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);
	DebugMsg("set rtc-pll clock 176MHZ!\r\n");
	pwr_rtc_pll_ctrl = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X14FB << 1); /* 176MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 174MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);
	DebugMsg("set rtc-pll clock 184MHZ!\r\n");
	pwr_rtc_pll_ctrl = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X15EC << 1); /* 184MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 183MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);
	DebugMsg("set rtc-pll clock 192MHZ!\r\n");
	pwr_rtc_pll_ctrl = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X16E0 << 1); /* 192MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 192MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);

	DebugMsg("restore rtc-pll clock 160MHZ!\r\n");
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl_old);
	vTaskDelay(100);

	return 0;
}

/* VCO change, higher than 5% */
static int vcoChangeHigherTest(void)
{
	unsigned int pwr_rtc_pll_ctrl_old;
	unsigned int pwr_rtc_pll_ctrl;

	pwr_rtc_pll_ctrl_old = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);

	DebugMsg("set rtc-pll clock 176MHZ!\r\n");
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl_old;
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X14FB << 1); /* 168MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 168MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);

	DebugMsg("set rtc-pll clock 192MHZ!\r\n");
	pwr_rtc_pll_ctrl = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl&(~0x7FFE);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl | (0X16E0 << 1); /* 192MHZ */
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	if(uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL) != pwr_rtc_pll_ctrl)
	{
		DebugMsg("clock test failed: 192MHZ failed!\r\n");
		return -1;
	}
	vTaskDelay(1000);

	DebugMsg("restore rtc-pll clock 160MHZ!\r\n");
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl_old);
	vTaskDelay(100);

	return 0;
}

/* VCO division change test */
static int vcoDivisionTest(void)
{
	int i;
	unsigned int tmp;
	unsigned int pwr_rtc_pll_ctrl;
	unsigned int pwr_rtc_pll_ctrl_old;

	pwr_rtc_pll_ctrl_old = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);
	pwr_rtc_pll_ctrl = pwr_rtc_pll_ctrl_old;
	pwr_rtc_pll_ctrl &= (~((0xF << POSTDIV1)|(0xF << POSTDIV2)));

	DebugMsg("first post divide test!\r\n");
	for(i=0; i<=15; i++)
	{
		tmp = pwr_rtc_pll_ctrl | (i << POSTDIV1);
		vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, tmp);
		vTaskDelay(100);
	}
	DebugMsg("second post divide test!\r\n");
	for(i=0; i<=15; i++)
	{
		tmp = pwr_rtc_pll_ctrl | (i << POSTDIV2);
		vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, tmp);
		vTaskDelay(100);
	}

	DebugMsg("restore rtc-pll clock 160MHZ!\r\n");
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl_old);
	vTaskDelay(100);

	return 0;
}

/* bypass mode test */
static int bypassTest(void)
{
	unsigned int pwr_rtc_pll_ctrl;

	pwr_rtc_pll_ctrl = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);

	DebugMsg("rtc-pll bypass mode test!\r\n");
	pwr_rtc_pll_ctrl = BYPASS;
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl);
	vTaskDelay(1000);

	return 0;
}

static portBASE_TYPE clockcmd(char *pcWriteBuffer,
				size_t xWriteBufferLen,
				const char *pcCommandString)
{
	int ret;
	unsigned int pwr_rtc_pll_ctrl_old;

	DebugMsg("CM3 clock test start!\r\n");

	pwr_rtc_pll_ctrl_old = uxIoBridgeRead(A7DA_PWRC_RTC_PLL_CTRL);

	/* switch M3 clock fast/slow test */
	ret = clockSwitchTest();
	if(ret != 0)
		return ret;
	/* VCO change, lower than 5% */
	ret = vcoChangeLowerTest();
	if(ret != 0)
		return ret;
	/* VCO change, higher than 5% */
	ret = vcoChangeHigherTest();
	if(ret != 0)
		return ret;
	/* VCO division change test */
	ret = vcoDivisionTest();
	if(ret != 0)
		return ret;
	/* bypass mode test */
	ret = bypassTest();
	if(ret != 0)
		return ret;

	DebugMsg("restore rtc-pll to default configuration!\r\n");
	vIoBridgeWrite(A7DA_PWRC_RTC_PLL_CTRL, pwr_rtc_pll_ctrl_old);

	DebugMsg("CM3 clock test pass!\r\n");

	return 0;

}
static const CLI_Command_Definition_t clockParameter =
{
	"clock",
	"\r\nclock\r\n",
	clockcmd,
	-1
};
const CLI_Command_Definition_t *P_CLOCK_CLI_Parameter = &clockParameter;
