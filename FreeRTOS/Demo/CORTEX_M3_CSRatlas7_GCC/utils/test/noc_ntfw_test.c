/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "io.h"
#include "misclib.h"
#include "soc_timer.h"

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define REG_M3_MODE_SET	0x18811050
#define REG_M3_MODE_CLR	0x18811054
#define REG_M3_MODE	0x18811058

static char *fw_name[] = {
	"dmac2", "dmac3", "dmac4", "gpio_vdifm", "gpio_mediam",
	"dmac0", "clkc", "pwm", "gpio_rtcm",
};

/*protected target registers base*/
static uint32_t fw_protected_base[] = {
	0x10d50000, 0x10d60000, 0x11002000,
	0x13300000, 0x17040000, 0x18000000, 0x18620000,
	0x18630000, 0x18890000,
};

/*protected target registers by fw vector*/
static uint32_t fw_protected_reg_offset[] = {
	0x0, 0x0, 0x0, 0x0,
	0x0, 0x0, 0x10, 0x0, 0x0,
};

/*protected target registers by default_access*/
static uint32_t fw_def_protected_reg_offset[] = {
	0x900, 0x900, 0x900, 0x8c,
	0x08c, 0x900, 0x568, -1, 0x8c,
};

static uint32_t reg_read(uint32_t address)
{
	uint32_t data;

	data = SOC_REG(address);
	vUsWait(100000);
	return data;
}

static void reg_write(uint32_t address, uint32_t val)
{
	SOC_REG(address) = val;
	vUsWait(100000);
}

static portBASE_TYPE ntfw_test(char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString)
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t i, ns, val, val_new;

	pcParameter1 = (char *)FreeRTOS_CLIGetParameter(pcCommandString,
						1, &xParameter1StringLength);

	if (NULL != pcParameter1) {
		pcParameter1[xParameter1StringLength] = 0x00;
		ns = (uint32_t)strtoul(pcParameter1, NULL, 0);
		if (ns) {
			reg_write(REG_M3_MODE_SET, 1);
			DebugMsg("***set m3 to ns mode***\r\n");
		} else {
			reg_write(REG_M3_MODE_CLR, 1);
			DebugMsg("***set m3 to s mode***\r\n");
		}
	}

	DebugMsg("***get m3 mode*** 0x%x: 0x%x(1=ns)\r\n",
		REG_M3_MODE, reg_read(REG_M3_MODE));
	/*m3 access target registers protected by vector and default_access*/
	for (i = 0; i < ARRAY_SIZE(fw_name); i++) {
		DebugMsg("*m3 test fw_%s \r\n", fw_name[i]);
		val = reg_read(
			fw_protected_base[i] + fw_protected_reg_offset[i]);
		reg_write(
			fw_protected_base[i] + fw_protected_reg_offset[i],
			val);
		val_new = reg_read(
			fw_protected_base[i] + fw_protected_reg_offset[i]);
		DebugMsg("0x%x: 0x%x -> 0x%x \r\n",
			fw_protected_base[i] + fw_protected_reg_offset[i],
			val, val_new);

		if (fw_def_protected_reg_offset[i] != -1) {
			val = reg_read(
				fw_protected_base[i] + \
				fw_def_protected_reg_offset[i]);
			reg_write(
				fw_protected_base[i] + \
				fw_def_protected_reg_offset[i],
				val);
			val_new = reg_read(
				fw_protected_base[i] + \
				fw_def_protected_reg_offset[i]);
			DebugMsg("0x%x: 0x%x -> 0x%x \r\n",
				fw_protected_base[i] + \
				fw_def_protected_reg_offset[i],
				val, val_new);
		} else
			DebugMsg("no regs protected by default_access in fw_%s \r\n",
			fw_name[i]);
	}

	return 0;
}


const CLI_Command_Definition_t ntfwParameter = {
	"ntfw",
	"\r\nntfw [ns]\r\n\
	ns	: 0:secure mode, 1: ns mode",
	ntfw_test,
	-1
};
const CLI_Command_Definition_t *P_NTFW_CLI_Parameter = &ntfwParameter;
