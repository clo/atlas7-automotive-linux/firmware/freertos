/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "debug.h"
#include "misclib.h"
#include "pulsec.h"
#include "soc_pulsec.h"
#include "io.h"
#include "gpt.h"
#include "soc_clkc.h"
#include "soc_ioc.h"
/* ================================================ [ MACRO ] ====================================================== */

/* ================================================ [ TYPE  ] ====================================================== */
/* ================================================ [ DECL  ] ====================================================== */
static portBASE_TYPE prvPulseCounterCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static portBASE_TYPE prvPulseCPwmCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
/* ================================================ [ DATA  ] ====================================================== */
const CLI_Command_Definition_t pulsecParameter =
{
    "pulsec",
    "\rpulsec < option >:\r\n    option: 0 - initialize; 1 - start increase; 2 - start drcrease; 3 - read pulsec counter;\r\n",
	prvPulseCounterCmd,
    -1
};
const CLI_Command_Definition_t *P_PULSEC_CLI_Parameter = &pulsecParameter;

const CLI_Command_Definition_t pulsecPwmParameter =
{
    "pulsecpwm",
    "\rpulsecpwm frequency duty:\r\n"
    "    frequency: 1 ~ 10K Hz, if 0 stop\r\n"
    "    duty:  form %1 to 100% output on gpio_3\r\n",
	prvPulseCPwmCmd,
    -1
};
const CLI_Command_Definition_t *P_PULSEC_PWM_CLI_Parameter = &pulsecPwmParameter;
/* ================================================ [ FUNC  ] ====================================================== */
static portBASE_TYPE prvPulseCounterCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    char *pcParameter1;
    uint32_t option;
    BaseType_t xParameter1StringLength;

    pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
    if((pcParameter1 == NULL)) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }

    option = (uint32_t)strtoul(pcParameter1, NULL, 0);
    if((option != 0) && (option != 1) && (option != 2) && (option != 3)) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }

    if(0 == option)
    {
    	vPulseCounterInit();
    	DebugMsg(" Initialize pulse counter done!\r\n");
    }
    else if(1 == option)
    {
    	vPulseCounterStart(SIRFSOC_PULSEC_FORWARD);
    	DebugMsg(" start pulse counter done increase!\r\n");
    }
    else if(2 == option)
    {
    	vPulseCounterStart(SIRFSOC_PULSEC_BACKWARD);
    	DebugMsg(" start pulse counter done decrease!\r\n");
    }
    else
    {
    	DebugMsg("A7DA_CLKC_GNSS_LEAF_CLK_EN_SET=%X\r\n",SOC_REG(A7DA_CLKC_GNSS_LEAF_CLK_EN_SET));
    	DebugMsg("A7DA_IOCTOP_FUNC_SEL_16_REG_SET=%X\r\n",SOC_REG(A7DA_IOCTOP_FUNC_SEL_16_REG_SET));

    	DebugMsg("A7DA_PULSEC_LWH_CNT=%X\r\n",SOC_REG(A7DA_PULSEC_LWH_CNT));
    	DebugMsg("A7DA_PULSEC_RWH_CNT=%X\r\n",SOC_REG(A7DA_PULSEC_RWH_CNT));
    	DebugMsg("A7DA_PULSEC_LWH_CNT=%X\r\n",SOC_REG(A7DA_PULSEC_LWH_CNT));
    	DebugMsg("A7DA_PULSEC_CTRL=%X\r\n",SOC_REG(A7DA_PULSEC_CTRL));
    	DebugMsg("A7DA_PULSEC_INT_STATUS=%X\r\n",SOC_REG(A7DA_PULSEC_INT_STATUS));
    	DebugMsg("A7DA_PULSEC_INT_EN=%X\r\n",SOC_REG(A7DA_PULSEC_INT_EN));
    	DebugMsg("A7DA_PULSEC_LWH_PRE_CNT=%X\r\n",SOC_REG(A7DA_PULSEC_LWH_PRE_CNT));
    	DebugMsg("A7DA_PULSEC_RWH_PRE_CNT=%X\r\n",SOC_REG(A7DA_PULSEC_RWH_PRE_CNT));
    }

error_exit:
    return 0;
}

static uint8_t l_duty;
static uint8_t l_progress;
static void prvDioInit(void)
{
	l_progress = 0;
	SOC_REG(0x10E40104)     = (7<<12);/* SW_TOP_FUNC_SEL_16_REG_CLR */
	SOC_REG(0x1330000C)= 0x60;	/* default high*/
}

static void prvDioSetState(uint8_t state)
{
	/* Use GPIO_3, detect J7 PIN31. */
	if(0 == state)
	{
		SOC_REG(0x1330000C)= 0x20;	/* output low */
	}
	else
	{
		SOC_REG(0x1330000C)= 0x60;	/* output high */
	}
}

static void prvPulseCPwmHandler(eGptChipId eChipId,uint8_t ucGptIdx,void *pxData)
{
	l_progress ++;

	if(l_progress < l_duty)
	{
		prvDioSetState(1);
	}
	else if(l_progress < 100)
	{
		prvDioSetState(0);
	}
	else
	{
		l_progress = 0;
		prvDioSetState(1);
	}
}

static portBASE_TYPE prvPulseCPwmCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    char *pcParameter1;
    char *pcParameter2;
    uint32_t freq,duty;
    int xRet;
    BaseType_t xParameter1StringLength;
    BaseType_t xParameter2StringLength;
    xGptParameter xParam;


    pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
    pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 2, &xParameter2StringLength );
    if( (pcParameter1 == NULL) || (pcParameter2 == NULL) ) {
        DebugMsg( "failed: invalid input paramter\r\n" );
        goto error_exit;
    }

    freq = (uint32_t)strtoul(pcParameter1, NULL, 0);
    if(freq > 10000) {
        DebugMsg( "failed: invalid input parameter frequency \r\n" );
        goto error_exit;
    }

    duty = (uint32_t)strtoul(pcParameter2, NULL, 0);
	if(duty >= 100) {
		DebugMsg( "failed: invalid input param3ter duty\r\n" );
		goto error_exit;
	}

	if(freq > 0)
	{
		/* Initialization */
		xRet = xGptInitialize(eGptOsTimerM3);
		if(xRet != 0)
		{
			DebugMsg("  >> Gpt Initialize failed with code %d\r\n",xRet);
			if ( -EEXIST != xRet )
			{
				goto error_exit;
			}
		}

		DebugMsg("  >> Test pulse counter simulation pulse: on %d Hz %d%% PWM on GPIO_3 to measure\r\n",freq,duty);

		vGptStopTimer(eGptOsTimerM3,1);	/* stop and then re-start */

		xRet = xGptRegisterInterrupt(prvPulseCPwmHandler,eGptOsTimerM3,1,NULL);
		if(xRet !=0) { DebugMsg("    # xGptRegisterInterrupt(%d,%d) failed with code %d\r\n",eGptOsTimerM3,1,xRet); }

		/* set up parameter */
		xParam.TIMER_DIV = 0;
		xParam.uxPeriod=configCPU_CLOCK_HZ/(freq*100);
		xParam.INTR_EN = 1;
		xParam.LOOP_EN = 1;

		prvDioInit();
		l_duty = duty;
		xRet=xGptStartTimer( eGptOsTimerM3,1, &xParam);
		if(xRet !=0) { DebugMsg("    # xGptStartTimer(%d,%d) failed with code %d\r\n",eGptOsTimerM3,1,xRet); }
	}
	else
	{

		vGptStopTimer(eGptOsTimerM3,1);
		DebugMsg("  stop the pulsec pwm\r\n");
	}

error_exit:
	return 0;
}
