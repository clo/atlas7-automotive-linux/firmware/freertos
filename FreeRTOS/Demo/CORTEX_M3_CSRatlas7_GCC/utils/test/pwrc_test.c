/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "pm.h"

#include "soc_pwrc.h"
#include "soc_iobridge.h"

#include "misclib.h"

static portBASE_TYPE pwrccmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t offset, val;

	pcParameter1 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
					1, &xParameter1StringLength);
	pcParameter2 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
					2, &xParameter2StringLength );
	if(NULL == pcParameter1)
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	if(NULL == pcParameter2)
	{
		offset = (uint32_t)strtoul(pcParameter1, NULL, 16);
		val = uxIoBridgeRead(offset);
		DebugMsg("offset:%x val:%x \r\n",offset, val);
		return 0;
	}

	pcParameter2[ xParameter2StringLength ] = 0x00;
	offset = (uint32_t)strtoul(pcParameter1, NULL, 16);
	val = (uint32_t)strtoul(pcParameter2, NULL, 16);
	vIoBridgeWrite(offset, val);
	return 0;
}

enum power_mode
{
	ACTIVE = 0,
	DEEPSLEEP_M3,
	HIBERNATE_M3,
	DEEPSLEEP,
	HIBERNATE,
};
static portBASE_TYPE powermodecmd( char *pcWriteBuffer,
				size_t xWriteBufferLen,
				const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	int type = 0;

	pcParameter1 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
					1, &xParameter1StringLength );

	if( NULL != pcParameter1 ) {
		pcParameter1[ xParameter1StringLength ] = 0x00;
		type = (uint32_t)strtoul(pcParameter1, NULL, 0);
	}

	DebugMsg("power mode is: %x\r\n", type);
	portDISABLE_INTERRUPTS();
	switch (type)
	{
	case ACTIVE:
		break;
	case DEEPSLEEP_M3:
		vGotoPowerSavingMode( PM_DEEP_SLEEP_M3 );
		break;
	case HIBERNATE_M3:
		vGotoPowerSavingMode( PM_HIBERNATION_M3 );
		break;
	case DEEPSLEEP:
		vGotoPowerSavingMode( PM_DEEP_SLEEP );
		break;
	case HIBERNATE:
		vGotoPowerSavingMode( PM_HIBERNATION );
		break;
	default:
		DebugMsg("cannot support such type.\r\n");
		return 0;
	}

	return 0;
}

static portBASE_TYPE prvPwrcCanWakeupCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t type = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		type = (uint32_t)strtoul(pcParameter1, NULL, 0);
	}

	if( /* type < PM_CAN_PARTIAL || */ type > PM_CAN_WAKEUP_TRANSCEIVER1 )
	{
		DebugMsg( "cannnot support this %d type!\r\n", type );
		return 1;
	}

	DebugMsg( "pwrc canbus wakeup type is %d\r\n", type );
	vPmEnableCanWakeup( type );
	return 0;
}

static const CLI_Command_Definition_t pwrcParameter =
{
	"pwrc",
	"\r\npwrc <offset> [val]\r\n",
	pwrccmd,
	-1
};
const CLI_Command_Definition_t *P_PWRC_CLI_Parameter = &pwrcParameter;

static const CLI_Command_Definition_t powermodeParameter =
{
	"power_mode",
	"\r\npower_mode <mode>\r\n \
	mode: \r\n \
	0(default) - active \r\n \
	1 - deep sleep m3 \r\n \
	2 - hibernate m3 \r\n \
	3 - deep sleep \r\n \
	4 - hibernate\r\n",
	powermodecmd,
	-1
};
const CLI_Command_Definition_t *P_PWRC_PowerMode_CLI_Parameter = &powermodeParameter;

const CLI_Command_Definition_t xPwrcCanWakeupParameter =
{
	"pm_can",
	"\r\npm_can <wakeup_source>\r\n \
	wakeup_source: \r\n \
	0 - partial-connect, CAN 0 is power on;\r\n \
	1 - CAN linstener0;\r\n \
	2 - CAN linstener1;\r\n \
	3 - CAN controller 0;\r\n \
	4 - CAN controller 1;\r\n \
	5 - CAN controller 0 or 1;\r\n \
	6 - CAN transceiver 0 interrupt;\r\n \
	7 - CAN transceiver 1 interrupt;\r\n",
	prvPwrcCanWakeupCommand,
	-1
};
const CLI_Command_Definition_t *P_PWRC_CanWakeup_CLI_Parameter = &xPwrcCanWakeupParameter;
