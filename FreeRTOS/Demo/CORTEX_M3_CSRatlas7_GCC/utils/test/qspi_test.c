/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "soc_qspi.h"
#include "soc_ioc.h"
#include "debug.h"
#include "misclib.h"
#include "soc_gpio.h"

#include "qspi.h"
#include "qspi_test_xip.h"
#include "pm.h"

static portBASE_TYPE prvQspiXipTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	char *pcParameter2;
	BaseType_t xParameter2StringLength;
	char *pcParameter3;
	BaseType_t xParameter3StringLength;
	char *pcParameter4;
	BaseType_t xParameter4StringLength;
	char *pcParameter5;
	BaseType_t xParameter5StringLength;
	char *pcParameter6;
	BaseType_t xParameter6StringLength;
	char *pcParameter7;
	BaseType_t xParameter7StringLength;
	char *pcParameter8;
	BaseType_t xParameter8StringLength;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				2, &xParameter2StringLength );
	pcParameter3 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				3, &xParameter3StringLength );
	pcParameter4 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				4, &xParameter4StringLength );
	pcParameter5 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				5, &xParameter5StringLength );
	pcParameter6 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				6, &xParameter6StringLength );
	pcParameter7 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				7, &xParameter7StringLength );
	pcParameter8 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				8, &xParameter8StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		xip_strc_var.clk_div = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
		clk_div = xip_strc_var.clk_div;
		DebugMsg( "clk_div:0x%x\r\n", clk_div );
	} else
		clk_div = 0;
	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		xip_strc_var.cpu_clk_hz = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
		cpu_clk_hz = xip_strc_var.cpu_clk_hz;
		vChangeCpuClock(cpu_clk_hz * 1000 * 1000);
		DebugMsg( "cpu_clk_hz:%d\r\n", cpu_clk_hz*1000*1000 );
	} else
		cpu_clk_hz = 192;
	if( NULL != pcParameter3 )
	{
		pcParameter3[ xParameter3StringLength ] = 0x00;
		read_op = ( uint32_t )strtoul( pcParameter3, NULL, 0 );
		DebugMsg( "read_op:%d\r\n", read_op );
	} else
		read_op = 0;
	if( NULL != pcParameter4 )
	{
		pcParameter4[ xParameter4StringLength ] = 0x00;
		read_times = ( uint32_t )strtoul( pcParameter4, NULL, 0 );
		DebugMsg( "read_times:%d\r\n", read_times );
	} else
		read_times = 0;
	if( NULL != pcParameter5 )
	{
		pcParameter5[ xParameter5StringLength ] = 0x00;
		qspi_delay_line = ( uint32_t )strtoul( pcParameter5, NULL, 0 );
		DebugMsg( "qspi_delay_line value:%d\r\n", qspi_delay_line );
	} else
		qspi_delay_line = 0;
	if( NULL != pcParameter6 )
	{
		pcParameter6[ xParameter6StringLength ] = 0x00;
		rd_delay = ( uint32_t )strtoul( pcParameter6, NULL, 0 );
		DebugMsg( "rd_delay value:%d\r\n", rd_delay );
	} else
		rd_delay = 2;
	if (NULL != pcParameter7) {
		pcParameter7[ xParameter7StringLength ] = 0x00;
		loop_times = ( uint32_t )strtoul( pcParameter7, NULL, 0 );
		DebugMsg( "loop_times value:%d\r\n", loop_times );
	} else
		loop_times = 1;
	if( NULL != pcParameter8 ) {
		pcParameter8[ xParameter8StringLength ] = 0x00;
		test_mode = ( uint32_t )strtoul( pcParameter8, NULL, 0 );
		DebugMsg( "test_mode value:%d\r\n", test_mode );
	} else
		test_mode = 1;
	vGpioSetDirectionOutput(eGpioRtcm, GPIO_RTC_0, 0);
	//DebugMsg("GPIO_RTC_0 value:%d\r\n", uxGpioGetValue(eGpioRtcm, GPIO_RTC_0));
	xTaskCreate( vQspiXipTestStart, "QXTS", configMINIMAL_STACK_SIZE + 128,
		(void *)(&xip_strc_var), tskIDLE_PRIORITY + 1 , NULL );
	return 0;
}

static portBASE_TYPE prvQspiInitCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	vQspiInit(0);
	return 0;
}

enum {
	DPM_LEAVE = 0,
	DPM_ENTER,
};
static portBASE_TYPE prvQspiDpmTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t type = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				1, &xParameter1StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		type = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	DebugMsg( "qspi %s dmp mode.\r\n", type == 0? "leave":"enter" );

	if( type == DPM_LEAVE )
		vQspiLeaveDpm();
	else
	{
		vQspiEnterDpm();
	}
	return 0;
}

static portBASE_TYPE prvQspiDpmPrintStatusCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	DebugMsg( "qspi status reg is 0x%x \r\n", (UINT32)SOC_REG(A7DA_QSPI_STAT) );
	/*DebugMsg( "qspi ctrl reg is 0x%x \r\n", (UINT32)SOC_REG(A7DA_QSPI_CTRL) );*/
	return 0;
}

static portBASE_TYPE prvQspiXIPInterfaceTestCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t clk_div, rd_op;

	clk_div = rd_op = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				1, &xParameter1StringLength );
	pcParameter2 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				2, &xParameter2StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		clk_div = ( uint32_t )strtoul( pcParameter1, NULL, 0 );
	}

	if( NULL != pcParameter2 )
	{
		pcParameter2[ xParameter2StringLength ] = 0x00;
		rd_op = ( uint32_t )strtoul( pcParameter2, NULL, 0 );
	}

	if( clk_div > 0xf )
		clk_div = 0xf;

	if( rd_op > 5 )
		rd_op = 5;

	DebugMsg( "clock divid is 0x%x, read opcode is %d.\r\n", clk_div, rd_op );

	vQspiXipInterfaceTestStart( clk_div, rd_op );

	return 0;
}


const CLI_Command_Definition_t xQspiXipTestParameter =
{
	"qspi_xip",
	"\r\nqspi_xip <clock_divisor> <cpu_clk_hz> <read_op> <read_times> <delay_line> <rd_delay> <loop_times> <test_mode>\r\n\
		clock_divisor: 0~15\r\n\
		cpu_clk_hz: maximum reach to 192\r\n\
		read_op: 0~4 read mode\r\n\
		read_times: 1~255 how many times continous read\r\n\
		delay_line: value for QSPI_DELAY_LINE\r\n\
		rd_delay: receive delay value will set\r\n\
		loop_times: >1 how many times test will do\r\n\
		test_mode: 1~3, 1 - only in loop way, 2 - only in random way, 3 - loop + random way\r\n",
	prvQspiXipTestCommand,
	-1
};

const CLI_Command_Definition_t *P_QSPI_XIPTest_CLI_Parameter = &xQspiXipTestParameter;
const CLI_Command_Definition_t xQspiInitParameter =
{
	"qspi_init",
	"\r\nqspi_init\r\n",
	prvQspiInitCommand,
	-1
};
const CLI_Command_Definition_t *P_QSPI_INIT_CLI_Parameter = &xQspiInitParameter;

const CLI_Command_Definition_t xQspiDpmTestParameter =
{
	"qspi_dpm",
	"\r\nqspi_dpm <type>\r\n \
	type: 0 - leave; 1 - enter\r\n",
	prvQspiDpmTestCommand,
	-1
};
const CLI_Command_Definition_t *P_QSPI_DpmTest_CLI_Parameter = &xQspiDpmTestParameter;

const CLI_Command_Definition_t xQspiDpmPrintStatusParameter =
{
	"qspi_print",
	"\r\nqspi_print: print out the status register.\r\n",
	prvQspiDpmPrintStatusCommand,
	0
};
const CLI_Command_Definition_t *P_QSPI_DpmPrintStatus_CLI_Parameter = &xQspiDpmPrintStatusParameter;

const CLI_Command_Definition_t xQspiXipInterfaceParameter =
{
	"xip",
	"\r\nxip <clk_div> <read_opcode> \r\n \
		clk_div: 0 - 0xf \r\n \
			clock = source_clock / (2 * (clk_div +  1)) \r\n \
		read_opcode: 0 - 5 \r\n \
			0 - FAST_READ \r\n \
			1 - READ2O \r\n \
			2 - READ2IO \r\n \
			3 - READ4O \r\n \
			4 - READ4IO \r\n",
	prvQspiXIPInterfaceTestCommand,
	-1
};
const CLI_Command_Definition_t *P_QSPI_XIPInterface_CLI_Parameter = &xQspiXipInterfaceParameter;
