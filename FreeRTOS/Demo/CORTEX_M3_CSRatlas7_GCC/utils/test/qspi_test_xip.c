/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"
#include "soc_mem.h"
#include "qspi.h"
#include "soc_cache.h"
#include "soc_timer.h"
#include "soc_gpio.h"

#include "qspi_test_xip.h"

#define NOR_24BIT_ADDRESS_SIZE		0x1000000

static struct qspi_test_nor_info qspi_nor_types[] =
{
	{ "MT25QL256ABA8ESF", 4096 * 4096 * 2, 5, { 0, 1, 2, 3, 4 } },
	{ "mx25l25635f", 4096 * 4096 * 2, 5, { 0, 1, 2, 3, 4 } },
	{ "mx25l12835f", 4096 * 4096, 5, { 0, 1, 2, 3, 4 } },
	{ "MX25L6445E", 4096 * 2048, 5, { 0, 1, 2, 3, 4 } },
	{ "s25fl164K", 4096 * 2048, 5, { 0, 1, 2, 3, 4 } },
	{ "S25FL116K", 4096 * 512, 5, { 0, 1, 2, 3, 4 } },
	{ "w25q80dv", 4096 * 256, 5, { 0, 1, 2, 3, 4 } },
	{ "W25Q32FV", 4096 * 1024, 5, { 0, 1, 2, 3, 4 } },
	{},
};

struct qspi_test_nor_info *pxInfo = NULL;

/* Function definitions. */

static INT32 prvCompareRegionsByDword( ulv *pxBufA, ulv *pxBufB, UINT32 uxCount )
{
	INT32 uxRet = 0;
	UINT32 uxN, times, equal_times;
	ulv *pxP1 = pxBufA;
	ulv *pxP2 = pxBufB;
	ulv temp1, temp2;

	for( uxN = 0; uxN < uxCount; uxN++, pxP1++, pxP2++ )
	{
		temp1 = *pxP1;
		temp2 = *pxP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "dowrd FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, *pxP1, *pxP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *pxP1;
					temp2 = *pxP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			uxRet = -1;
		}
	}
	return uxRet;
}

static INT32 prvCompareRegionsByWord( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	INT32 uxRet = 0;
	UINT32 uxN, times, equal_times;
	u16v *psP1 = ( u16v* )pxBufA;
	u16v *psP2 = ( u16v* )pxBufB;
	u16v temp1, temp2;

	for( uxN = 0; uxN < uxCount * 2; uxN++ )
	{
		temp1 = *psP1;
		temp2 = *psP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "Word FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, *psP1, *psP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *psP1;
					temp2 = *psP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			uxRet = -1;
		}
		psP1 += 1;
		psP2 += 1;
	}
	return uxRet;
}

static INT32 prvCompareRegionsByByte( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	INT32 uxRet = 0;
	UINT32 uxN, times, equal_times;
	u8v *pcP1 = ( u8v* )pxBufA;
	u8v *pcP2 = ( u8v* )pxBufB;
	u8v temp1, temp2;

	for( uxN = 0; uxN < uxCount * 4; uxN++ )
	{
		temp1 = *pcP1;
		temp2 = *pcP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "Byte FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, *pcP1, *pcP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *pcP1;
					temp2 = *pcP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			/* printf( "Skipping to next test..." ); */
			uxRet = -1;
		}
		pcP1 += 1;
		pcP2 += 1;
	}
	return uxRet;
}

static INT32 random_prvCompareRegionsByDword( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	INT32 uxRet = 0;
	UINT32 uxN, j, times, equal_times;
	ulv *pxP1 = pxBufA;
	ulv *pxP2 = pxBufB;
	ulv temp1, temp2;

	j = 0;
	do{
		uxN = rand_ul() % uxCount;
		pxP1 = pxBufA + uxN;
		pxP2 = pxBufB + uxN;
		temp1 = *pxP1;
		temp2 = *pxP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "Radom Dowrd FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, *pxP1, *pxP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *pxP1;
					temp2 = *pxP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pxP1, ( UINT32 )pxP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			uxRet = -1;
		}
	}while(j++ < uxCount );
	return uxRet;
}

static INT32 random_prvCompareRegionsByWord( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	INT32 uxRet = 0;
	size_t uxN, j, times, equal_times;
	u16v *psP1 = ( u16v* )pxBufA;
	u16v *psP2 = ( u16v* )pxBufB;
	u16v temp1, temp2;

	j = 0;
	do{
		uxN = rand_ul();
		uxN = uxN % ( uxCount * 2);
		psP1 = ( u16v* )pxBufA + uxN;
		psP2 = ( u16v* )pxBufB + uxN;
		temp1 = *psP1;
		temp2 = *psP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "Word FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, *psP1, *psP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *psP1;
					temp2 = *psP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )psP1, ( UINT32 )psP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			uxRet = -1;
		}
	}while(j++ < uxCount * 2);
	return uxRet;
}

static INT32 random_prvCompareRegionsByByte( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	INT32 uxRet = 0;
	size_t uxN, j, times, equal_times;
	u8v *pcP1 = ( u8v* )pxBufA;
	u8v *pcP2 = ( u8v* )pxBufB;
	u8v temp1, temp2;

	j = 0;
	do {
		uxN = rand_ul();
		uxN = uxN % ( uxCount * 4 );
		pcP1 = ( u8v* )pxBufA + uxN;
		pcP2 = ( u8v* )pxBufB + uxN;
		temp1 = *pcP1;
		temp2 = *pcP2;
		if( temp1 != temp2 )
		{
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
			if (read_times == 0)
				printf( "Byte FAILURE: addrss, 0x%08x, 0x%08x;value, in check(first read):0x%x != 0x%x in print(second read):0x%x != 0x%x\r\n",
					( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, *pcP1, *pcP2 );
			else {
				times = equal_times = 0;
				printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, times + 1);
				for (times = 1; times < read_times; times++) {
					temp1 = *pcP1;
					temp2 = *pcP2;
					printf ( "dowrd FAILURE: address, 0x%08x - 0x%08x, value 0x%x --- 0x%x in read times %d\r\n", ( UINT32 )pcP1, ( UINT32 )pcP2, temp1, temp2, times +1);
					if ( temp1 == temp2 )
						equal_times++;
				}
				printf ( "in %d times read, read equal times:%d read non-equal times:%d\r\n", read_times, equal_times, (read_times - equal_times) );
			}
			vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 0);
			uxRet = -1;
		}
	}while(j++ < uxCount * 4 );
	return uxRet;
}

static INT32 prvQspiTestCompareRegions( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	if (test_mode & 0x1) {
		if( prvCompareRegionsByDword( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "dword test success.\r\n" );
		if( prvCompareRegionsByWord( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "word test success.\r\n" );
		if( prvCompareRegionsByByte( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "byte test success.\r\n" );
	}
	if (test_mode & 0x2) {
		if( random_prvCompareRegionsByDword( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "random dword test success.\r\n" );
		if( random_prvCompareRegionsByWord( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "random word test success.\r\n" );
		if( random_prvCompareRegionsByByte( pxBufA, pxBufB, uxCount ) )
			return -1;
		printf( "random byte test success.\r\n" );
	}
	return 0;

}

static INT32 prvQspiTestStuckAddress( ulv *pxBufA, size_t uxCount, size_t uxXipBase )
{
	ulv *pxP1 = pxBufA;
	UINT32 uxM;
	UINT32 uxN;
	ul uxData;
	ul uxOff = ( ul )pxBufA - L2_MEMORY_QSPI + uxXipBase;

	for( uxM = 0; uxM < 2; uxM++ )
	{
		uxOff = ( ul )pxBufA - L2_MEMORY_QSPI + uxXipBase;
		pxP1 = ( ulv * ) ( pxBufA );
		vQspiSwitchHost();
		vQspiEraseFlash( uxOff,uxCount * 4 );
		for( uxN = 0; uxN < uxCount; uxN++ )
		{
			uxData = ( ( uxM + uxN) % 2 ) == 0 ? ( ul ) pxP1 : ~( ( ul ) pxP1 );
			vQspiIoWrite( uxOff, (const u32 * )&uxData, 4 );
			uxOff += 4;
			pxP1++;
		}

		vQspiSwitchXotf();
		pxP1 = ( ulv * ) ( pxBufA );
		for( uxN = 0; uxN < uxCount; uxN++, pxP1++ )
		{
			if( *pxP1 != ( ( ( uxM + uxN ) % 2 ) == 0 ?
				( ul ) pxP1 : ~( ( ul ) pxP1 ) ) )
			{
				printf( "xip address(0x%08x) uxData(0x%x) -- (0x%x).\n",
					( UINT32 )pxP1, ( UINT32 )( *pxP1 ),
					( ( uxM + uxN ) % 2) == 0 ? ( UINT32 )pxP1 : ~( ( UINT32 )pxP1 ) );
				printf( "FAILURE: possible bad address line at physical address 0x%08x.\n",( UINT32 )pxP1 );
				return -1;
			}
		}
	}
	return 0;
}

static INT32 prvQspiTestRandomValue( ulv *pxBufA, ulv *pxBufB, size_t uxCount, size_t uxXipBase )
{

	ul uxOffA = ( ul )pxBufA - L2_MEMORY_QSPI + uxXipBase;
	ul uxOffB = ( ul )pxBufB - L2_MEMORY_QSPI + uxXipBase;
	size_t uxN;
	ul uxData;
	UINT32 uxTimes;

	vQspiSwitchHost();
	vQspiEraseFlash( uxOffA, uxCount * 4 );
	vQspiEraseFlash( uxOffB, uxCount * 4 );
	for( uxN = 0; uxN < uxCount; uxN++ )
	{
		uxData = rand_ul();
		vQspiIoWrite( uxOffA, (const u32 * )&uxData, 4 );
		vQspiIoWrite( uxOffB, (const u32 * )&uxData, 4 );
		uxOffA += 4;
		uxOffB += 4;
	}

	vQspiSwitchXotf();
	for (uxTimes = 0; uxTimes < loop_times; uxTimes++) {
		if( 0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

#if 0
INT32 qspi_test_seqinc_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	size_t uxN;
	ul q = rand_ul();
	ul uxData;

	vQspiSwitchHost();
	vQspiEraseFlash(offa,uxCount*4 );
	vQspiEraseFlash(offb,uxCount*4 );
	for( uxN = 0; uxN < uxCount; uxN++ ) {

		uxData = uxN + q;
		vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
		vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
	}
	vQspiSwitchXotf();
	if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
		return -1;
	return 0;
}

INT32 qspi_test_solidbits_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	ul q;
	size_t uxN;
	ul uxData;

	for(j = 0; j < 64; j++ ) {
		q = (j % 2) == 0 ? UL_ONEBITS : 0;
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			uxData = ( uxN % 2) == 0 ? q : ~q;
			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		printf( "testing %d success\n", j);

		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_checkerboard_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	ul q;
	size_t uxN;
	ul uxData;

	for(j = 0; j < 64; j++ ) {
		q = (j % 2) == 0 ? CHECKERBOARD1 : CHECKERBOARD2;
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			uxData = ( uxN % 2) == 0 ? q : ~q;
			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_blockseq_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	size_t uxN;
	ul uxData;

	for(j = 0; j < 256; j++ ) {
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			uxData = ( ul ) UL_BYTE(j);
			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_walkbits0_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	size_t uxN;
	ul uxData;

	for(j = 0; j < UL_LEN * 2; j++ ) {
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			if(j < UL_LEN)	{/* Walk it up. */
				uxData = ONE << j;
			} else {	/* Walk it back down. */
				uxData = ONE << (UL_LEN * 2 - j - 1 );
			}
			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_walkbits1_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	size_t uxN;
	ul uxData;

	for(j = 0; j < UL_LEN * 2; j++ ) {
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			if(j < UL_LEN) {	/* Walk it up. */
				uxData = UL_ONEBITS ^ (ONE << j);
			} else	/* Walk it back down. */
				uxData = UL_ONEBITS ^
					(ONE << (UL_LEN * 2 - j - 1 ) );

			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_bitspread_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j;
	size_t uxN;
	ul uxData;

	for(j = 0; j < UL_LEN * 2; j++ ) {
		vQspiSwitchHost();
		vQspiEraseFlash(offa,uxCount*4 );
		vQspiEraseFlash(offb,uxCount*4 );
		for( uxN = 0; uxN < uxCount; uxN++ ) {
			if(j < UL_LEN) {	/* Walk it up. */
				uxData = ( uxN % 2 == 0 )
				    ? (ONE << j) | (ONE << (j + 2) )
				    : UL_ONEBITS ^ ( (ONE << j)
						    | (ONE << (j + 2) ));
			} else {	/* Walk it back down. */
				uxData = ( uxN % 2 == 0 )
				    ? (ONE << (UL_LEN * 2 - 1 - j) ) | (ONE <<
								       (UL_LEN *
									2 + 1 -
									j) )
				    : UL_ONEBITS ^ (ONE << (UL_LEN * 2 - 1 - j)
						    | (ONE <<
						       (UL_LEN * 2 + 1 - j) ));
			}
			vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
			vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
		}
		vQspiSwitchXotf();
		if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
			return -1;
	}
	return 0;
}

INT32 qspi_test_bitflip_comparison( ulv *pxBufA, ulv *pxBufB, size_t uxCount )
{
	ul offa = ( ul )pxBufA - L2_MEMORY_QSPI;
	ul offb = ( ul )pxBufB - L2_MEMORY_QSPI;
	unsigned UINT32 j, k;
	ul q;
	size_t uxN;
	ul uxData;

	for(k = 0; k < UL_LEN; k++ ) {
		q = ONE << k;
		for(j = 0; j < 8; j++ ) {
			q = ~q;
			vQspiSwitchHost();
			vQspiEraseFlash(offa,uxCount*4 );
			vQspiEraseFlash(offb,uxCount*4 );
			for( uxN = 0; uxN < uxCount; uxN++ ) {
				uxData = ( uxN % 2) == 0 ? q : ~q;
				vQspiIoWrite(offa, (const u32 * )&uxData, 4 );
				vQspiIoWrite(offb, (const u32 * )&uxData, 4 );
			}
			vQspiSwitchXotf();
			if(0 != prvQspiTestCompareRegions( pxBufA, pxBufB, uxCount ) )
				return -1;
		}
		printf( "testing %d success\n", j);
	}
	return 0;
}
#endif

const struct qspi_test tests[] = {
	{"Random Value", prvQspiTestRandomValue},
	{NULL, NULL}
};

static INT32 prvQspiXipTest( UINT32 uxAddr, UINT32 uxSize, UINT32 uxXipBase, UINT32 uxExtAddr, UINT32 clk_div, UINT32 read_op )
{
	ul uxN;
	UINT32  uxBufSize, uxHalfLen, uxCount;
	ulv *pxBufA, *pxBufB;
	INT32 xExitCode = 0;
	void volatile  *pAligned;
	UINT32 uxClk, uxMod, uxRdOpIndx, uxWrtOp;

	pAligned = ( void * )uxAddr;
	uxBufSize = uxSize;
	uxHalfLen = uxBufSize / 2;
	uxCount = uxHalfLen / sizeof( ul );
	pxBufA = ( ulv * )pAligned;
	pxBufB = ( ulv * )( ( UINT32 ) pAligned + uxHalfLen );

	vQspiSwitchHost();
	if( uxExtAddr == 0 )
		vQspiLeave32addr();
	else
		vQspiEnter32addr();

	vQspiSetXipBase( uxXipBase );

	{
		uxClk = clk_div;
		//for( uxMod = 0; uxMod < 2; uxMod++ )
		{
			uxMod = 0;
			//for( uxRdOpIndx = 0; uxRdOpIndx < pxInfo->uRxOpNum; uxRdOpIndx++ )
			{
				uxRdOpIndx = read_op;
				uxWrtOp = 0;
				printf( "clock div is 0x%x\r\n", uxClk );
				vQspiSwitchHost();
				vQspiSetClock( uxClk );
				/*
				if( uxClk < 2 )
					vQspiSetRxDelay( 2 );
				else if( 7 < uxClk )
					vQspiSetRxDelay( 7 );
				else
					vQspiSetRxDelay( uxClk );
				*/
				vQspiSetRxDelay( rd_delay );

				printf( "spi mode is 0x%x clk_div is 0x%x rd_delay:0x%x\r\n", uxMod, uxClk, rd_delay );
				vQspiSetMode( uxMod );
				printf( "read op is (0x%x), write op is (0x%x)\r\n",
						pxInfo->uRxOp[ uxRdOpIndx ], uxWrtOp );
				vQspiSetWorkMode( pxInfo->uRxOp[ uxRdOpIndx ], uxWrtOp, uxExtAddr);
				printf( "uxCount is 0x%x\r\n", uxCount );
				printf( "%s: ", "Stuck Address" );
				if(!prvQspiTestStuckAddress(pAligned, uxBufSize / sizeof( ul ), uxXipBase ) )
					printf( "ok\r\n" );
				else
					xExitCode |= QSPI_EXIT_FAIL;
				for( uxN = 0;; uxN++ )
				{
					if( !tests[ uxN ].name )
						break;
					/* If using a custom testmask, only run this test if the
					   bit corresponding to this test was set by the user.
					 */
					if( !tests[ uxN ].fp( pxBufA, pxBufB, uxCount, uxXipBase ) )
						printf( "=======%s: OK======\r\n", tests[ uxN ].name );
					else
						xExitCode |= QSPI_EXIT_FAIL;
				}
			}
		}
	}
	return xExitCode;
}

static struct qspi_test_nor_info * xQspiTestGetNorInfo( char * xNorName )
{
	struct qspi_test_nor_info *pInfo;

	for( pInfo = qspi_nor_types; pInfo->pcNorName; pInfo++ )
	{
		if( !strcmp( xNorName, pInfo->pcNorName ) )
			return pInfo;
	}
	return NULL;
}

#define QSPI_TEST_SIZE		0x100000
void vQspiXipTestStart( void *pvParameters )
{
	UINT32 result = 0;
	char* pcNorName;
	vDcacheDisable();

	vQspiInit(qspi_delay_line);
	pcNorName = cQspiGetNorName();

	DebugMsg( "nor name is %s! clk_div:0x%x\r\n", pcNorName, clk_div );
	pxInfo = xQspiTestGetNorInfo( pcNorName );
	if( NULL == pxInfo )
	{
		DebugMsg( "Find nor flash info failed.\r\n" );
		vTaskDelete(NULL);
	}
	DebugMsg( "nor size is 0x%x!\r\n", pxInfo->uNorSize );
	/* RETAIN_SRAM */
	DebugMsg( "begin Retain mem test!\r\n" );
	DebugMsg( "24 bit address test\r\n" );
	if( QSPI_EXIT_FAIL == prvQspiXipTest(L2_MEMORY_QSPI+SZ_1M*2, QSPI_TEST_SIZE, 0, 0, clk_div, read_op ) )
	{
		result |= QSPI_EXIT_FAIL;
		DebugMsg( "24 bit address test failed.\r\n" );
	}
	/*
	DebugMsg( "24 bit address xip base test\r\n" );
	if( QSPI_EXIT_FAIL == prvQspiXipTest(L2_MEMORY_QSPI, QSPI_TEST_SIZE, pxInfo->uNorSize / 2, 0 ) )
	{
		result |= QSPI_EXIT_FAIL;
		DebugMsg( "24 bit address xip base test failed.\r\n" );
	}
	*/

	if( pxInfo->uNorSize > NOR_24BIT_ADDRESS_SIZE )
	{
		DebugMsg( "32 bit address test\r\n" );
		if( QSPI_EXIT_FAIL == prvQspiXipTest(L2_MEMORY_QSPI+SZ_1M*2, QSPI_TEST_SIZE, 0, 1, clk_div, read_op ) )
		{
			result |= QSPI_EXIT_FAIL;
			DebugMsg( "32 bit address test failed.\r\n" );
		}
		/*
		DebugMsg( "32 bit address xip base test\r\n" );
		if( QSPI_EXIT_FAIL == prvQspiXipTest(L2_MEMORY_QSPI, QSPI_TEST_SIZE, pxInfo->uNorSize / 2, 1 ) )
		{
			result |= QSPI_EXIT_FAIL;
			DebugMsg( "32 bit address xip base test failed.\r\n" );
		}
		*/
	}
	if( QSPI_EXIT_FAIL & result )
		DebugMsg( "QSPI XIP test failed!\r\n" );
	else
		DebugMsg( "QSPI XIP test passed!\r\n" );
	/*
	vGpioSetValue(eGpioRtcm, GPIO_RTC_0, 1);
	DebugMsg("GPIO_RTC_0 value:%d\r\n", uxGpioGetValue(eGpioRtcm, GPIO_RTC_0));
	*/
	vDcacheEnable();
	vTaskDelete(NULL);
}

UINT32 data;
#define QSPI_INTERFACE_TEST_SIZE	0x100000
void vQspiXipInterfaceTestStart( UINT32 uxClk, UINT32 rd_op )
{
	UINT32 i;
	ulv *pxPD = (ulv *)L2_MEMORY_QSPI;

	vDcacheDisable();
	vQspiInit(0);

	vQspiSwitchHost();
	vQspiSetClock( uxClk );
	if( uxClk < 2 )
		vQspiSetRxDelay( 2 );
	else if( 7 < uxClk )
		vQspiSetRxDelay( 7 );
	else
		vQspiSetRxDelay( uxClk );

	vQspiSetWorkMode( rd_op, 0, 0 );
	vUsWait( 1 );

	vQspiSwitchXotf();

	for( i = 0; i < QSPI_INTERFACE_TEST_SIZE; pxPD++, i += 4 )
		data = *pxPD;

	vQspiSwitchHost();

	vDcacheEnable();
}
