/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __QSPI_TEST_XIP_H__
#define __QSPI_TEST_XIP_H__

#define QSPI_EXIT_FAIL	    0x01

struct qspi_test
{
	char *name;
	int (*fp) ();
};

struct qspi_test_nor_info
{
	char * pcNorName;
	uint32_t uNorSize;
	uint32_t uRxOpNum;
	uint32_t uRxOp[5];
};

/* Function declaration. */
void vQspiXipTestStart( void *pvParameters );
void vQspiXipInterfaceTestStart( UINT32 uxClk, UINT32 rd_op );

struct xip_strc {
	UINT32 clk_div;
	UINT32 cpu_clk_hz;
};
struct xip_strc xip_strc_var;
UINT32 clk_div;
UINT32 cpu_clk_hz;// =  192 * 1000 * 1000;
UINT32 read_op;
UINT32 read_times;
UINT32 qspi_delay_line;
UINT32 rd_delay;
UINT32 loop_times;
UINT32 test_mode;
#endif /* __QSPI_MEM_LIB_H__ */
