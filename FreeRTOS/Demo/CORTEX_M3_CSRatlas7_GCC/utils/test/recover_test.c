/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "soc_pwrc.h"
#include "soc_pwrc_retain.h"
#include "io.h"
#include "pm.h"
#include "soc_timer.h"
#include "soc_cache.h"

#include "debug.h"
#include "soc_iobridge.h"
#include "misclib.h"

#define RECOVER_FLAG_MAGIC    0x98921320
#define DEPENDEDNT_FLAG_MAGIC 0x334478AB

#define RECOVER_FLAG_MEM  MEM(PM_RECOVER_FLAG_MEM)

static void __on_retain EcuReset(void)
{
#if 0
	SOC_REG(A7DA_M3_TIMER_32COUNTER_WD_CTRL) = 0;	/* stop firstly */
	SOC_REG(A7DA_M3_TIMER_32COUNTER_2_CTRL) = 0;
	SOC_REG(A7DA_M3_TIMER_COUNTER_2) = 0;
	SOC_REG(A7DA_M3_TIMER_COUNTER_WD) = 0;
	SOC_REG(A7DA_M3_TIMER_MATCH_2)  = 10;
	SOC_REG(A7DA_M3_TIMER_MATCH_WD) = 10;

	SOC_REG(A7DA_M3_TIMER_WATCHDOG_EN) = 1;
	SOC_REG(A7DA_M3_TIMER_32COUNTER_WD_CTRL) = 1;
	SOC_REG(A7DA_M3_TIMER_32COUNTER_2_CTRL) = 3;
#else
	vPmClearWakeupSource(0xFFFFFFFF);
	vNocClkDisconnect();
	vNocClkReconnectAndNoWait();
	vIoBridgeWrite(A7DA_PWRC_RTC_SW_RSTC_CLR, PWRC_RESET_M3 );
#endif
}

void __on_retain vEnterRecoverMode(void)
{
	portDISABLE_INTERRUPTS();
	PWRC_CLEAR_CLOCK_INITIALIZED_FLAG();
	PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG();
	PWRC_CLEAR_DDR_INITIALIZED_FLAG();
	RECOVER_FLAG_MEM  = RECOVER_FLAG_MAGIC;
	vDcacheFlush();	/* make sure write to retain */
    vPmResetNorFlash();
	EcuReset();
	while(1);
	portENABLE_INTERRUPTS();
}

void __on_retain vEnterDependentMode(void)
{
	portDISABLE_INTERRUPTS();
    PWRC_CLEAR_CLOCK_INITIALIZED_FLAG();
    PWRC_CLEAR_CLOCK_LATE_INITIALIZED_FLAG();
    PWRC_CLEAR_DDR_INITIALIZED_FLAG();
    RECOVER_FLAG_MEM = DEPENDEDNT_FLAG_MAGIC;
    vDcacheFlush();	/* make sure write to retain */
    vPmResetNorFlash();
    EcuReset();
    while(1);
    portENABLE_INTERRUPTS();
}

static portBASE_TYPE prvRecoverCmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t uxOption = 0;


	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(NULL != pcParameter1)
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		uxOption = ( uint32_t )strtoul(pcParameter1, NULL, 0);
	}
    if(0 == uxOption)
    {
        uint32_t uxBootMode = uxPmCheckBootMode();
        if( PM_BOOT_INDEPENDENT == uxBootMode )
        {
            vEnterRecoverMode();
        }
        else
        {
            DebugMsg("This command is invalid for Dependent mode\r\n");
        }
    }
    else
    {
        vEnterDependentMode();
    }
	return 0;
}

const CLI_Command_Definition_t recoverParameter =
{
    "recover",
    "\rrecover <option>:\r\n    request to enter recover firmware\r\n"
    "\roptiron: 0 - CAN recover, 1 - dependent boot\r\n",
    prvRecoverCmd,
    -1
};
const CLI_Command_Definition_t *P_RECOVER_CLI_Parameter = &recoverParameter;

/*
devmem 0x188D001c 32  8
devmem 0x1324010c 32 1
devmem 0x18620478 32  0x44CC77DD
*/
