/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "debug.h"
#include "misclib.h"

static u32 reg_read(u32 address)
{
	u32 data;
	data = *((volatile u32 *)address);
	return data;
}

static void reg_write(u32 address, u32 val)
{
	*((volatile u32 *)address)=val;
}

static portBASE_TYPE regcmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	char *pcParameter1, *pcParameter2;
	BaseType_t xParameter1StringLength, xParameter2StringLength;
	uint32_t offset, val;

	pcParameter1 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
						1, &xParameter1StringLength);
	pcParameter2 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
						2, &xParameter2StringLength );
	if(NULL == pcParameter1)
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	if(NULL == pcParameter2) {
		offset = (uint32_t)strtoul(pcParameter1, NULL, 16);
		val = reg_read(offset);
		DebugMsg("addr:%x val:%x \r\n",offset, val);

		return 0;
	}

	pcParameter2[ xParameter2StringLength ] = 0x00;
	offset = (uint32_t)strtoul(pcParameter1, NULL, 16);
	val = (uint32_t)strtoul(pcParameter2, NULL, 16);
	reg_write(offset,val);

	return 0;
}


const CLI_Command_Definition_t regParameter =
{
	"reg",
	"\r\nreg <address> [val]\r\n",
	regcmd,
	-1
};
const CLI_Command_Definition_t *P_REGOPS_CLI_Parameter = &regParameter;
