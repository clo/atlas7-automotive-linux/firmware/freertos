/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "soc_tsadc.h"
#include "soc_ioc.h"
#include "debug.h"
#include "misclib.h"


static portBASE_TYPE prvTsadcEnableCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	vTsadcEnable();
	return 0;
}

static portBASE_TYPE prvTsadcDisableCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	vTsadcDisable();
	return 0;
}

static portBASE_TYPE prvTsadcInitCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	vTsadcInit();
	return 0;
}

static portBASE_TYPE prvTsadcGetVoltCommand( char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	ChannelIds eChannel = 0;
	uint32_t ulVolt = 0;

	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString,
				1, &xParameter1StringLength );
	if( NULL != pcParameter1 )
	{
		pcParameter1[ xParameter1StringLength ] = 0x00;
		eChannel = (ChannelIds)strtoul( pcParameter1, NULL, 0 );
	}

	ulVolt = ulTsadcGetVoltage( eChannel );

	DebugMsg("The channel voltage = %dmv \r\n", ulVolt);

	return 0;
}

const CLI_Command_Definition_t xTsadcGetVoltParameter =
{
	"tsadc_getvolt",
	"\r\ntsadc_getvolt <channel>\r\n \
	channel: 0 - 9 - enter\r\n",
	prvTsadcGetVoltCommand,
	-1
};
const CLI_Command_Definition_t *P_TSADC_GetVolt_CLI_Parameter = &xTsadcGetVoltParameter;

const CLI_Command_Definition_t xTsadcEnableParameter =
{
	"tsadc_enable",
	"\r\ntsadc_enable\r\n",
	prvTsadcEnableCommand,
	-1
};
const CLI_Command_Definition_t *P_TSADC_ENABLE_CLI_Parameter = &xTsadcEnableParameter;

const CLI_Command_Definition_t xTsadcDisableParameter =
{
	"tsadc_disable",
	"\r\ntsadc_disable\r\n",
	prvTsadcDisableCommand,
	-1
};
const CLI_Command_Definition_t *P_TSADC_DISABLE_CLI_Parameter = &xTsadcDisableParameter;

const CLI_Command_Definition_t xTsadcInitParameter =
{
	"tsadc_init",
	"\r\ntsadc_init\r\n",
	prvTsadcInitCommand,
	-1
};
const CLI_Command_Definition_t *P_TSADC_INIT_CLI_Parameter = &xTsadcInitParameter;
