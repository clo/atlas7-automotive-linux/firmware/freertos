/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"
#include "task.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "io.h"
#include "debug.h"
#include "misclib.h"
#include "canbus.h"

extern int virtio_can_send_frame(u32 chn, void *data, bool wait);

extern void virtio_can_show_statistic(void);
static portBASE_TYPE vcancmd( char *pcWriteBuffer,
			size_t xWriteBufferLen,
			const char *pcCommandString )
{
	CAN_MSGOBJECT cmsg;
	char *pcParameter1;
	BaseType_t xParameter1StringLength;
	uint32_t count, idx;

	DebugMsg( "START VIRTIO CAN test\r\n" );
	virtio_can_show_statistic();

	pcParameter1 = (char *)FreeRTOS_CLIGetParameter( pcCommandString,
						1, &xParameter1StringLength);
	if(NULL == pcParameter1)
		return 0;

	pcParameter1[ xParameter1StringLength ] = 0x00;
	count = (uint32_t)strtoul(pcParameter1, NULL, 10);

	for (idx = 0; idx < count; idx++) {
		cmsg.ide = true;
		cmsg.rtr = true;
		cmsg.id = 0x7AB;
		cmsg.dlc = 8;
		cmsg.dataHigh = 0xAABB1981;
		cmsg.dataLow = 0xCCDD6398;

		virtio_can_send_frame(3, &cmsg, true);
	}
	DebugMsg( "FINISH VIRTIO CAN test\r\n" );

	return 0;
}

const CLI_Command_Definition_t vcanParameter =
{
	"vcan",
	"\r\nvcan <count>\r\n",
	vcancmd,
	-1
};
const CLI_Command_Definition_t *P_VCAN_CLI_Parameter = &vcanParameter;
