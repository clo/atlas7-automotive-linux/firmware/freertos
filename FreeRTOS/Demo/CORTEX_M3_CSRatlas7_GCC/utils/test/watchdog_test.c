/*
 * Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "FreeRTOS.h"

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

/* Library include files. */
#include "ctypes.h"
#include "misclib.h"
#include "io.h"
#include "debug.h"
#include "soc_irq.h"
#include "soc_timer.h"
#include "core_cm3.h"

#define INT_WATCHDOG_IRQn INT_TIMER_5_IRQn
#define EN_INTERRUPT	(1<<1)
#define EN_COUTER	(1)

#define A7DA_WATCHDOG_CTRL	A7DA_TIMER_32COUNTER_5_CTRL
#define A7DA_WATCHDOG_MATCH	A7DA_TIMER_MATCH_5
#define A7DA_WATCHDOG_COUNTER	A7DA_TIMER_COUNTER_5

static portBASE_TYPE wdgcmd( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	char *pcParameter1;
	uint32_t request;
	BaseType_t xParameter1StringLength;

	DebugMsg( "start watchdog test\r\n" );

	/* enable watchdog interrupt */
	NVIC_EnableIRQ(INT_WATCHDOG_IRQn);
	NVIC_SetPriority(INT_WATCHDOG_IRQn, API_SAFE_INTR_PRIO_0);

	/* clear control register */
	SOC_REG(A7DA_WATCHDOG_CTRL) = 0;

	/* set match register */
	SOC_REG(A7DA_WATCHDOG_MATCH) = 0x100;
	/* set counter register*/
	SOC_REG(A7DA_WATCHDOG_COUNTER) = 0;

	/*
	 * 0 - Timer match register 5 matches cause an interrupt request.
	 * 1 - Timer match register 5 matches cause a second watchdog count
	 * 	which will be followed by reset in step B, but int step A just
	 *	cause system reset.
	 */
	/* cause an interrupt request as the default configuration */
	SOC_REG(A7DA_TIMER_WATCHDOG_EN) = 0;
	pcParameter1 = ( char * )FreeRTOS_CLIGetParameter( pcCommandString, 1, &xParameter1StringLength );
	if(pcParameter1 != NULL) {
		request = (uint32_t)strtoul(pcParameter1, NULL, 0);
		if((request != 0) && (request != 1)) {
			DebugMsg( "failed: invalid input paramter\r\n" );
			return -1;
		}
		SOC_REG(A7DA_TIMER_WATCHDOG_EN) = request;
	}
	/*
	 * generates interrupts if the counter matches the
	 * value of matches register
	 */
	SOC_REG(A7DA_WATCHDOG_CTRL) = EN_INTERRUPT | EN_COUTER;
	return 0;
}

const CLI_Command_Definition_t wdgParameter =
{
	"wdg",
	"\r\nwdg < flag >:\r\nflag: 0 - interrupt; 1 - reset\r\n",
	wdgcmd,
	-1
};
const CLI_Command_Definition_t *P_WATCHDOG_CLI_Parameter = &wdgParameter;
